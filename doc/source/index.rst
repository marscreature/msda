.. SWOT-MSDA documentation master file, created by
   sphinx-quickstart on Thu May  2 09:58:50 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SWOT-MSDA's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

:math:'$a=b \sigma$'

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
