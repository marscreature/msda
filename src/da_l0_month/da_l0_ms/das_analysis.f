      subroutine das_analysis(tile)
      implicit none
      integer tile
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=384,  MMmH=390, NnH=66)
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLmH,  MMm=MMmH,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer chunk_size_X,margin_X,chunk_size_E,margin_E
      parameter (chunk_size_X=(Lm+NSUB_X-1)/NSUB_X,
     &                     margin_X=(NSUB_X*chunk_size_X-Lm)/2,
     &           chunk_size_E=(Mm+NSUB_E-1)/NSUB_E,
     &                     margin_E=(NSUB_E*chunk_size_E-Mm)/2)
      integer Istr,Iend,Jstr,Jend, i_X,j_E
        j_E=tile/NSUB_X
        i_X=tile-j_E*NSUB_X
        Istr=1+i_X*chunk_size_X-margin_X
        Iend=Istr+chunk_size_X-1
        Istr=max(Istr,1)
        Iend=min(Iend,Lm)
        Jstr=1+j_E*chunk_size_E-margin_E
        Jend=Jstr+chunk_size_E-1
        Jstr=max(Jstr,1)
        Jend=min(Jend,Mm)
      call das_analysis_tile (Istr,Iend,Jstr,Jend)
      return
      end
      subroutine das_analysis_tile (Istr,Iend,Jstr,Jend)
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k,itrc
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=384,  MMmH=390, NnH=66)
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLmH,  MMm=MMmH,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+1)*(Mm+1)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_prf
      PARAMETER( max_prf=NDAS)
      integer max_sio, max_whoi, max_flight,
     &        max_hfradar,max_hfradar6,max_js1
      PARAMETER(max_sio=100,max_whoi=100,max_flight=30000,
     &         max_hfradar=30000,max_hfradar6=30000,max_js1=8000)
      integer max_ptsur
      PARAMETER(max_ptsur=100)
      integer max_martn
      PARAMETER(max_martn=100)
      integer max_moor
      PARAMETER(max_moor=280)
      real sio_ot,sio_os,whoi_ot,whoi_os,ptsur_ot,ptsur_os,
     &              martn_ot,martn_os,moor_ot,moor_os, hf_ouv,
     &              hf_ouv6,fl_ot,js1_ossh,swot_ossh,hcmin
      PARAMETER( sio_ot=1.0/(1.185*1.185),sio_os=1.0/(0.16*0.16),
     &          whoi_ot=1.0/(1.17*1.17),whoi_os=1.0/(0.16*0.16),
     &        ptsur_ot=1.0/(1.65*1.65),ptsur_os=1.0/(0.2*0.2),
     &        martn_ot=1.0/(1.65*1.65),martn_os=1.0/(0.2*0.2),
     &        moor_ot=1.0/(1.3*1.3),moor_os=1.0/(0.18*0.18),
     &        js1_ossh=1.0/(0.03*0.03),swot_ossh=1.0/(0.10*0.10),
     &        hcmin=50.0,
     &        fl_ot=1.0/(1.0*1.0),
     &        hf_ouv=1.0/(0.07*0.07),hf_ouv6=1.0/(0.2*0.2)
     &         )
      integer max_prf_cal,max_cal
      PARAMETER(max_prf_cal=NDAS,max_cal=100)
      integer max_prf_dor,max_dor
      PARAMETER(max_prf_dor=NDAS,max_dor=100)
      real cal_ot,cal_os,dor_ot,dor_os
      PARAMETER( cal_ot=1.0/(1.0*1.0),
     &           cal_os=1.0/(0.2*0.2),
     &           dor_ot=1.0/(1.0*1.0),
     &           dor_os=1.0/(0.2*0.2)
     &         )
      real geo_ratio, cross_tsp
      PARAMETER( geo_ratio = 1.0, cross_tsp=0.)
      integer sm_rad
      PARAMETER( sm_rad=5)
      integer Local_len
      PARAMETER( Local_len=20)
      real sz_rad
      PARAMETER( sz_rad=6.0)
      integer sz_rad_len
      PARAMETER( sz_rad_len=20)
      real regp, reguv
      PARAMETER( regp=1.0/(10.0*10.0) )
      PARAMETER( reguv=1.0/(0.6*0.6) )
      real h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hinv(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real f(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fomn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_h/h /grid_hinv/hinv /grid_f/f /grid_fomn/fomn
      real angler(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_angler/angler
      real latr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real lonr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_latr/latr /grid_lonr/lonr
      real pm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_pm/pm    /metrics_pn/pn
     &       /metrics_omu/om_u /metrics_on_u/on_u
     &       /metrics_omv/om_v /metrics_on_v/on_v
      real dmde(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real dndx(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_dmde/dmde    /metrics_dndx/dndx
      real on_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmon_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pnom_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real grdscl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_on_p/on_p /metrics_om_p/om_p
     &       /metrics_on_r/on_r /metrics_om_r/om_r
     &       /metrics_pmon_u/pmon_u /metrics_pnom_v/pnom_v
     &                              /metrics_grdscl/grdscl
      real rmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /mask_r/rmask /mask_p/pmask
     &       /mask_u/umask /mask_v/vmask
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      real  x_sponge,   v_sponge
       real  tauT_in, tauT_out, tauM_in, tauM_out
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &                      , x_sponge,   v_sponge
     &                      , tauT_in, tauT_out, tauM_in, tauM_out
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      real zeta(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real ubar(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vbar(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real rzeta_bak(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real rubar_bak(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real rvbar_bak(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_zeta/zeta  /rhs_rzeta/rzeta_bak
     &       /ocean_ubar/ubar  /rhs_rubar/rubar_bak
     &       /ocean_vbar/vbar  /rhs_rvbar/rvbar_bak
      real u(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real v(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real t(0:Lm+1+padd_X,0:Mm+1+padd_E,N,NT)
      common /ocean_u/u /ocean_v/v /ocean_t/t
      real Hz(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real Hz_bak(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real z_r(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real z_w(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      real Huon(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real Hvom(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real W(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      common /grid_Hz/Hz    /grid_zr/z_r  /grid_W/W
     &  /grid_Hz_bak/Hz_bak /grid_zw/z_w  /grid_Huon/Huon
     &                                    /grid_Hvom/Hvom
      real rho(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real rhop0(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      common /ocean_rho/rho /ocean_rhop0/rhop0
      real zeta_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_das/u_das /ocean_v_das/v_das
     &    /ocean_psi_das/psi_das /ocean_chi_das/chi_das
     &    /ocean_t_das/t_das /ocean_zeta_das/zeta_das
      real zeta_s(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real zeta_h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      real t_w(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s/u_s /ocean_v_s/v_s
     &      /ocean_psi_s/psi_s /ocean_chi_s/chi_s
     &      /ocean_rho_s/rho_s /ocean_p_s/p_s
     &     /ocean_t_s/t_s /ocean_zeta_s/zeta_s
     &     /ocean_zeta_h/zeta_h /ocean_t_w/t_w
      real zeta_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real psi_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common
     &     /ocean_psi_adj/psi_adj /ocean_chi_adj/chi_adj
     &     /ocean_t_adj/t_adj /ocean_zeta_adj/zeta_adj
      real zeta_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real zeta_h_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      real t_w_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s_adj/u_s_adj /ocean_v_s_adj/v_s_adj
     &   /ocean_psi_s_adj/psi_s_adj /ocean_chi_s_adj/chi_s_adj
     &     /ocean_rho_s_adj/rho_s_adj /ocean_p_s_adj/p_s_adj
     &     /ocean_t_s_adj/t_s_adj /ocean_zeta_s_adj/zeta_s_adj
     &     /ocean_zeta_h_adj/zeta_h_adj /ocean_t_w_adj/t_w_adj
      real rmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real pmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real umask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real vmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /mask_r_das/rmask_das /mask_p_das/pmask_das
     &       /mask_u_das/umask_das /mask_v_das/vmask_das
      integer nzr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nzr_das/nzr_das
     &       /ocean_nzu_das/nzu_das /ocean_nzv_das/nzv_das
      integer nsr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nsr_das/nsr_das
     &       /ocean_nsu_das/nsu_das /ocean_nsv_das/nsv_das
      real georatio(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /cgeoratio/georatio
      real z_das(ndas),tc1d_das(ndas),sc1d_das(ndas)
      common /grid_z_das/z_das,tc1d_das,sc1d_das
      real s1d(N),  z1d(NDAS)
      real xs1d(N), xz1d(NDAS)
      external das_spln1d
      integer IstrR,IendR,JstrR,JendR
      if (Istr.eq.1) then
        IstrR=Istr-1
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lm) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        JstrR=Jstr-1
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mm) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      do j=JstrR,JendR
        do i=IstrR,IendR
          zeta(i,j)=zeta(i,j) + zeta_s(i,j)
        enddo
      enddo
      do itrc=1,NT
        do j=JstrR,JendR
          do i=IstrR,IendR
            t(i,j,N,itrc)=t(i,j,N,itrc)+t_s(i,j,NDAS,itrc)
          enddo
        enddo
      enddo
      do itrc=1,NT
        do j=JstrR,JendR
          do i=IstrR,IendR
             do k=1,N
               s1d(k)=z_r(i,j,k)
             enddo
             do k=1,NDAS
               xz1d(k)=t_s(i,j,k,itrc)
             enddo
             call das_spln1d(NDAS,z_das,xz1d,N,s1d,xs1d)
             do k=1,N-1
               if (abs(s1d(k)) .lt. abs(z_das(1)) ) then
                 t(i,j,k,itrc)=t(i,j,k,itrc)+xs1d(k)
               endif
             enddo
          enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=Istr,IendR
          u(i,j,N)=u(i,j,N)+u_s(i,j,NDAS)
          ubar(i,j)=ubar(i,j) - u_s(i,j,NDAS)
     &                        *(Hz(i-1,j,N)+Hz(i,j,N))
     &                        /(z_w(i-1,j,0)+z_w(i,j,0))
        enddo
      enddo
      do j=JstrR,JendR
        do i=Istr,IendR
           do k=1,N
             s1d(k)=0.5*(z_r(i-1,j,k)+z_r(i,j,k))
           enddo
           do k=1,NDAS
             xz1d(k)=u_s(i,j,k)
           enddo
           call das_spln1d(NDAS,z_das,xz1d,N,s1d,xs1d)
           do k=1,N-1
             if (abs(s1d(k)) .lt. abs(z_das(1)) ) then
               u(i,j,k)=u(i,j,k) + xs1d(k)
               ubar(i,j)=ubar(i,j) - xs1d(k)
     &                        *(Hz(i-1,j,k)+Hz(i,j,k))
     &                        /(z_w(i-1,j,0)+z_w(i,j,0))
             endif
           enddo
        enddo
      enddo
      do j=Jstr,JendR
        do i=IstrR,IendR
          v(i,j,N)=v(i,j,N)+v_s(i,j,NDAS)
          vbar(i,j)=vbar(i,j) - v_s(i,j,NDAS)
     &                        *(Hz(i,j-1,N)+Hz(i,j,N))
     &                        /(z_w(i,j-1,0)+z_w(i,j,0))
        enddo
      enddo
      do j=Jstr,JendR
        do i=IstrR,IendR
           do k=1,N
             s1d(k)=0.5*(z_r(i,j-1,k)+z_r(i,j,k))
           enddo
           do k=1,NDAS
             xz1d(k)=v_s(i,j,k)
           enddo
           call das_spln1d(NDAS,z_das,xz1d,N,s1d,xs1d)
           do k=1,N-1
             if (abs(s1d(k)) .lt. abs(z_das(1)) ) then
               v(i,j,k)=v(i,j,k) + xs1d(k)
               vbar(i,j)=vbar(i,j) - xs1d(k)
     &                        *(Hz(i,j-1,k)+Hz(i,j,k))
     &                        /(z_w(i,j-1,0)+z_w(i,j,0))
             endif
           enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          zeta(i,j)=zeta(i,j) * rmask(i,j)
        enddo
      enddo
      do j=JstrR,JendR
        do i=Istr,IendR
          ubar(i,j)=ubar(i,j) *  umask(i,j)
        enddo
      enddo
      do j=Jstr,JendR
        do i=IstrR,IendR
          vbar(i,j)=vbar(i,j) *  vmask(i,j)
        enddo
      enddo
      do k=1,N
        do itrc=1,NT
          do j=JstrR,JendR
            do i=IstrR,IendR
              t(i,j,k,itrc)=t(i,j,k,itrc) * rmask(i,j)
            enddo
          enddo
        enddo
        do j=JstrR,JendR
          do i=Istr,IendR
            u(i,j,k)=u(i,j,k) *  umask(i,j)
          enddo
        enddo
        do j=Jstr,JendR
          do i=IstrR,IendR
            v(i,j,k)=v(i,j,k) *  vmask(i,j)
          enddo
        enddo
      enddo
      return
      end
