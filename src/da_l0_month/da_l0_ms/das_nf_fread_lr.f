      integer function das_nf_fread  (A, ncid, varid, record, type)
      implicit none
      include 'netcdf.inc'
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=384,  MMmH=390, NnH=66)
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLmH,  MMm=MMmH,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+1)*(Mm+1)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_prf
      PARAMETER( max_prf=NDAS)
      integer max_sio, max_whoi, max_flight,
     &        max_hfradar,max_hfradar6,max_js1
      PARAMETER(max_sio=100,max_whoi=100,max_flight=30000,
     &         max_hfradar=30000,max_hfradar6=30000,max_js1=8000)
      integer max_ptsur
      PARAMETER(max_ptsur=100)
      integer max_martn
      PARAMETER(max_martn=100)
      integer max_moor
      PARAMETER(max_moor=280)
      real sio_ot,sio_os,whoi_ot,whoi_os,ptsur_ot,ptsur_os,
     &              martn_ot,martn_os,moor_ot,moor_os, hf_ouv,
     &              hf_ouv6,fl_ot,js1_ossh,swot_ossh,hcmin
      PARAMETER( sio_ot=1.0/(1.185*1.185),sio_os=1.0/(0.16*0.16),
     &          whoi_ot=1.0/(1.17*1.17),whoi_os=1.0/(0.16*0.16),
     &        ptsur_ot=1.0/(1.65*1.65),ptsur_os=1.0/(0.2*0.2),
     &        martn_ot=1.0/(1.65*1.65),martn_os=1.0/(0.2*0.2),
     &        moor_ot=1.0/(1.3*1.3),moor_os=1.0/(0.18*0.18),
     &        js1_ossh=1.0/(0.03*0.03),swot_ossh=1.0/(0.10*0.10),
     &        hcmin=50.0,
     &        fl_ot=1.0/(1.0*1.0),
     &        hf_ouv=1.0/(0.07*0.07),hf_ouv6=1.0/(0.2*0.2)
     &         )
      integer max_prf_cal,max_cal
      PARAMETER(max_prf_cal=NDAS,max_cal=100)
      integer max_prf_dor,max_dor
      PARAMETER(max_prf_dor=NDAS,max_dor=100)
      real cal_ot,cal_os,dor_ot,dor_os
      PARAMETER( cal_ot=1.0/(1.0*1.0),
     &           cal_os=1.0/(0.2*0.2),
     &           dor_ot=1.0/(1.0*1.0),
     &           dor_os=1.0/(0.2*0.2)
     &         )
      real geo_ratio, cross_tsp
      PARAMETER( geo_ratio = 1.0, cross_tsp=0.)
      integer sm_rad
      PARAMETER( sm_rad=5)
      integer Local_len
      PARAMETER( Local_len=20)
      real sz_rad
      PARAMETER( sz_rad=6.0)
      integer sz_rad_len
      PARAMETER( sz_rad_len=20)
      real regp, reguv
      PARAMETER( regp=1.0/(10.0*10.0) )
      PARAMETER( reguv=1.0/(0.6*0.6) )
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLm+2-nhalf)/nratio-1,
     &           MMm_lr=(MMm+2-nhalf)/nratio-1)
      parameter ( Lm_lr=LLm_lr, Mm_lr=MMm_lr)
      real A(0:Lm_lr+1+padd_X,0:Mm_lr+1+padd_E,NDAS+1)
      integer ncid,  type,   vert_type,  imin, imax,  start(4)
     &      , varid, record, horiz_type, jmin, jmax,  count(4)
     &      , i,j,k, shift,  ierr,       trd,  omp_get_thread_num
      real buff((Lm+5)*(Mm+5)*(N+1),0:0)
      common /buffer/ buff
      vert_type=type/4
      horiz_type=type-4*vert_type
      jmin=horiz_type/2
      imin=horiz_type-2*jmin
      start(1)=1
      start(2)=1
      imax=Lm_lr+1
      jmax=Mm_lr+1
      count(1)=imax-imin+1
      count(2)=jmax-jmin+1
      if (vert_type.eq.0) then
        count(3)=1
        start(3)=record
      elseif (vert_type.eq.1) then
        count(3)=NDAS
        count(4)=1
        start(3)=1
        start(4)=record
      elseif (vert_type.eq.2) then
        count(3)=NDAS+1
        count(4)=1
        start(3)=1
        start(4)=record
      else
        write(*,'(/1x,2A,I3/)') 'NF_FREAD ERROR: ',
     &                    'illegal grid type', type
        das_nf_fread=nf_noerr+1
        return
      endif
      trd=omp_get_thread_num()
      ierr=nf_get_vara_double (ncid, varid, start, count, buff(1,trd))
      das_nf_fread=ierr
      if (ierr .ne. nf_noerr) then
        write(*,'(/1x,2A,I5/1x,3A,I4/)') 'NF_FREAD ERROR: ',
     &               'nf_get_vara netCDF error code =', ierr,
     &               'Cause of error: ', nf_strerror(ierr)
        return
      endif
      do k=1,count(3)
        do j=jmin,jmax
          shift=1-imin+count(1)*(j-jmin+(k-1)*count(2))
          do i=imin,imax
            A(i,j,k)=buff(i+shift,trd)
          enddo
        enddo
      enddo
      return
      end
      integer function das_nf_fwrite (A, ncid, varid, record, type)
      implicit none
      include 'netcdf.inc'
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=384,  MMmH=390, NnH=66)
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLmH,  MMm=MMmH,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+1)*(Mm+1)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_prf
      PARAMETER( max_prf=NDAS)
      integer max_sio, max_whoi, max_flight,
     &        max_hfradar,max_hfradar6,max_js1
      PARAMETER(max_sio=100,max_whoi=100,max_flight=30000,
     &         max_hfradar=30000,max_hfradar6=30000,max_js1=8000)
      integer max_ptsur
      PARAMETER(max_ptsur=100)
      integer max_martn
      PARAMETER(max_martn=100)
      integer max_moor
      PARAMETER(max_moor=280)
      real sio_ot,sio_os,whoi_ot,whoi_os,ptsur_ot,ptsur_os,
     &              martn_ot,martn_os,moor_ot,moor_os, hf_ouv,
     &              hf_ouv6,fl_ot,js1_ossh,swot_ossh,hcmin
      PARAMETER( sio_ot=1.0/(1.185*1.185),sio_os=1.0/(0.16*0.16),
     &          whoi_ot=1.0/(1.17*1.17),whoi_os=1.0/(0.16*0.16),
     &        ptsur_ot=1.0/(1.65*1.65),ptsur_os=1.0/(0.2*0.2),
     &        martn_ot=1.0/(1.65*1.65),martn_os=1.0/(0.2*0.2),
     &        moor_ot=1.0/(1.3*1.3),moor_os=1.0/(0.18*0.18),
     &        js1_ossh=1.0/(0.03*0.03),swot_ossh=1.0/(0.10*0.10),
     &        hcmin=50.0,
     &        fl_ot=1.0/(1.0*1.0),
     &        hf_ouv=1.0/(0.07*0.07),hf_ouv6=1.0/(0.2*0.2)
     &         )
      integer max_prf_cal,max_cal
      PARAMETER(max_prf_cal=NDAS,max_cal=100)
      integer max_prf_dor,max_dor
      PARAMETER(max_prf_dor=NDAS,max_dor=100)
      real cal_ot,cal_os,dor_ot,dor_os
      PARAMETER( cal_ot=1.0/(1.0*1.0),
     &           cal_os=1.0/(0.2*0.2),
     &           dor_ot=1.0/(1.0*1.0),
     &           dor_os=1.0/(0.2*0.2)
     &         )
      real geo_ratio, cross_tsp
      PARAMETER( geo_ratio = 1.0, cross_tsp=0.)
      integer sm_rad
      PARAMETER( sm_rad=5)
      integer Local_len
      PARAMETER( Local_len=20)
      real sz_rad
      PARAMETER( sz_rad=6.0)
      integer sz_rad_len
      PARAMETER( sz_rad_len=20)
      real regp, reguv
      PARAMETER( regp=1.0/(10.0*10.0) )
      PARAMETER( reguv=1.0/(0.6*0.6) )
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLm+2-nhalf)/nratio-1,
     &           MMm_lr=(MMm+2-nhalf)/nratio-1)
      parameter ( Lm_lr=LLm_lr, Mm_lr=MMm_lr)
      real A(0:Lm_lr+1+padd_X,0:Mm_lr+1+padd_E,NDAS+1)
      integer ncid,  type,   vert_type,  imin, imax,  start(4)
     &      , varid, record, horiz_type, jmin, jmax,  count(4)
     &      , i,j,k, shift,  ierr,       trd,  omp_get_thread_num
      real buff((Lm+5)*(Mm+5)*(N+1),0:0)
      common /buffer/ buff
      vert_type=type/4
      horiz_type=type-4*vert_type
      jmin=horiz_type/2
      imin=horiz_type-2*jmin
      start(1)=1
      start(2)=1
      imax=Lm_lr+1
      jmax=Mm_lr+1
      count(1)=imax-imin+1
      count(2)=jmax-jmin+1
      if (vert_type.eq.0) then
        count(3)=1
        start(3)=record
      elseif (vert_type.eq.1) then
        count(3)=NDAS
        count(4)=1
        start(3)=1
        start(4)=record
      elseif (vert_type.eq.2) then
        count(3)=NDAS+1
        count(4)=1
        start(3)=1
        start(4)=record
      else
        write(*,'(/1x,2A,I4/)') 'NF_FWRITE ERROR: ',
     &                    'illegal grid type', type
        das_nf_fwrite=nf_noerr+1
        return
      endif
      trd=omp_get_thread_num()
      do k=1,count(3)
        do j=jmin,jmax
          shift=1-imin+count(1)*(j-jmin+(k-1)*count(2))
          do i=imin,imax
            buff(i+shift,trd)=A(i,j,k)
          enddo
        enddo
      enddo
      ierr=nf_put_vara_double (ncid, varid, start, count, buff(1,trd))
      das_nf_fwrite=ierr
      if (ierr.ne.nf_noerr) then
        write(*,'(/1x,2A,I5/1x,3A,I4/)') 'NF_FWRITE ERROR: ',
     &               'nf_put_vara netCDF error code =', ierr,
     &               'Cause of error: ', nf_strerror(ierr)
      endif
      return
      end
