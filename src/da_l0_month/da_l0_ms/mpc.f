












!
! $Id: mpc.F,v 1.3 2004/03/26 15:26:07 pmarches Exp $
!
      program mpc
!
! Multifunctional precompiling processor designed to work between
! CPP and FORTRAN compiler.
!-------------------------------------------------------------------
! Objectives:
!------------ 
! (1) Eliminate blank lines left by CPP and fortran style comments,
!     but keep compiler directives like C$DOACROSS in place.
!
! (2) Usually Fortran syntax does not prohibit the practice when a
!     quotation is opened on one line and closes on the next line.
!     Doing so, however, may be considered as unsafe and sloppy from
!     stylistical point of view; mpc enforces the rule when every
!     string inside quotation mark must be closed within the line. 
!   --> Optional:: activated by TRAP_SINGLE_QUOTES CPP-switch.
!
! (3) Convert _default_ size fortran real type declarations into
!     real*8.  Any mixture of lower and upper cases latters is
!     allowed in the word real. 
!     Explicitly specified size declaration, such as real*4
!     or real*16 will be left unchanged.
!   --> Optional:: activated by REAL_TO_REAL8 CPP-switch.
!
! (4) Enforce double precision accuracy for the real type constants
!     appearing anywhere in the program, for example, 1. --> 1.D0;
!     .1 --> .1D0;    9.81 --> 9.81D0;   .5e-8 --> .5D-8; 
!     1.2e+14 --> 1.2D+14 etc.
!   --> Safe. Will distinguish real type constants from look alike
!     combinations like, say format descriptors F8.4 or e12.5; or
!     logical expressions like 1.eq.i etc. Will fold line of the
!     code automatically according to fortran syntax rules, when
!     this change causes line to increase beyond 72 characters. 
!   --> Optional: activated by DOUBLE_CONST CPP-switch.
!
!
! (5) Convert _default_ size fortran integer type declarations
!     into integer*4. Explicitly specified size declaration, such
!     as integer*2 or integer*8 will be left unchanged.
!   --> Optional: activated when _both_  INT_TO_INT4 _and_
!                 REAL_TO_REAL8 CPP-switches are defined.
!
!
! (6) Convert parallelized loops over subdomains (tiles) 
!
!          do tile=0,NSUB_X*NSUB_E-1
!
!     into two nested loops. The outer is loop over parallel threads
!     (to be parallelized), and the inner is over tiles to be
!     processed by the particular thread:
!
!          do trd=0,numthreads-1
!            do tile=forward_sweep
!     or
!          do trd=0,numthreads-1
!            do tile=backward_sweep
!
!     in such a way the inner loops in the subsequent parallel
!     regions are reversed, so that each thread "zig-zags" across
!     the tiles it is processing. Automatically append SHARED/PRIVATE
!     lists with the newly introduced variables.
!   --> Optional: activated by ZIG_ZAG CPP-switch. 
!
! More detailed description for each option is available below.
!
! In the case when after they were modified, lines of the fortran
! program became longer than the prescribed width of 72 characters, 
! they are automatically folded by mpc according to the fortran
! syntax rules. 
!
! Usage: 
!-------
! Similar to CPP
!                  mpc source.file target.file
!             or
!                  mpc source.file > target.file
!             or
!                  mpc < source.file > target.file
!
! mpc is smart enough to recognize how many arguments are given,
! TWO, ONE or NONE and act accordingly. If two arguments are present,
! the first one is input file name, while the second is output, if
! only one is present, it is input; output goes to standard output;
! if NONE is present, mpc expects input from standard input, while
! output goes to standard output. The last option allows mpc to work
! as receiver in pipe with CPP:
!
!                /lib/cpp -P file.F | mpc > file.f          
!
! WARNING: Insofar as manifestations of functional deficiencies are
! agreed upon by any and all concerned parties to be imperceivable,
! and are so stipulated, it is incumbent upon said heretofore
! mentioned parties to exercise the deferment of otherwise pertinent
! maintenance procedures.
!
! And therefore, all manifestations of functional deficiencies, such
! as bugs, evidence of nonrobust behaviour or any other problems of
! any kind shall be reported to Alexander Shchepetkin, (310)-206-9381
! or alex@atmos.ucla.edu.
!



      implicit none
      INTEGER*4 max_length, max_filename
      parameter(max_length=80, max_filename=32)
      character file_in*(max_filename), file_out*(max_filename),
     &          symbol(2*max_length),   buffr(max_length),
     &          scratch*(max_length),   quote,     double_quote,
     &          type, type_none, char_type, int_type, real_type
      parameter (type_none=' ', char_type='1',
     &           int_type='4',  real_type='8')
      INTEGER*4 last_arg, iargc, iin,iout, iocheck, line, istrt,
     &         length, i,j,k,m, is,ie, isft, ks1,ks2,ks3,ks4,ks5
      logical not_end_of_file, directive, dir_switch, lswtch
      logical par_region, lnest
      INTEGER*4 ip1,ip2, is1,is2
      lnest=.false.
      par_region=.false.
      k=0
      dir_switch=.false.
      quote=char(39)
      double_quote=char(34)

      file_in='                                '
      file_out='                                '
      last_arg=iargc()
      if (last_arg.gt.0) then
        iin=11
        call getarg(1,file_in)
        open(iin, file=file_in, form='formatted', status='old')
      else
        iin=5       !<-- read from standard input
      endif
      if (last_arg.eq.2) then
        iout=12
        call getarg(2,file_out)
        open(iout, file=file_out, form='formatted', status='unknown')
      else
        iout=6        !--> write to standard output
      endif

      not_end_of_file=.true.
      line=0
  1    line=line+1
        length=0                ! Reset the string length and blank
        directive=.false.
        do i=1,max_length       ! out the string itself. Then read
          symbol(i)=' '         ! in a new string.
        enddo
        read(iin,'(80A1)',iostat=iocheck,end=2)
     &                 (symbol(i), i=1,max_length)
        goto 3
   2    not_end_of_file=.false.
        if (symbol(1).eq.'#') goto 1 
   3    if (symbol(1).eq.'C' .or. symbol(1).eq.'c' .or.
     &                            symbol(1).eq.'!') then
          if (symbol(2).eq.'$' .or. symbol(3).eq.'$' .or.
     &        symbol(4).eq.'$' .or. symbol(5).eq.'$') then
            directive=.true.
          else                  ! if the first symbol indicates that 
            goto 1              ! this line is a fortran comment, but
          endif                 ! a dollar sign is present somewhere
        endif                   ! in positions 2,3,4,5, this line is
                                ! a directive and it should be pro-
        lswtch=.false.          ! cessed further. 
        do i=1,max_length
          if (symbol(i).eq.quote) then
            lswtch=.not.lswtch
          elseif (symbol(i).eq.'!' .and. .not.lswtch) then
            goto 4
          endif
          if (symbol(i).ne.' ') length=i
        enddo
   4    continue
        if (lswtch) write(iout,'(/6x,A,I4,1x,A/)')
     &    'ERROR: unmatched quote on line', line,'in .i file.'
        do while (length.gt.1 .and. symbol(length).eq.' ')
          length=length-1
        enddo
        istrt=7
        do while (istrt.lt.length .and. symbol(istrt).eq.' ')
          istrt=istrt+1 
        enddo
!
! Recognize REAL or real and turn it into REAL*8 or real*8,
! but do not convert REAL*4 or real*4 into real*8.
!
        i=istrt
        type=type_none
        if (type.ne.type_none) then     ! ...and j is index of the
          j=i+1
          do while (j.lt.length .and. symbol(j).eq.' ')
            j=j+1
          enddo                         ! first non-blanc symbol 
                                        ! after type declaration.
!
! Once it is eatablished that the line contains an "obsolecent" from
! Intel IFC compiler point of view style of explicit size declaration
! like real*16 or character*128, change it to FORTRAN 95 style,
!
!                    real*16 --> real(16)
!              character*128 --> character(len=128)
!
! which is then accepted by the compiler without warning message.
! To do so, first extract the size itself and save it into "buffr"
! (strip outer braces if any), and set initial value "isft" to be
! the number of deleted characters (negative), which is asterick
! '*' itself and all blank spaces occurring between FORTRAN type
! specifier and size specifier.
!
          if (symbol(j).eq.'*') then
!
! The following code segment deals with declaration of default-size
! real and INTEGER*4 type variables:  if so directed, insert explicit
! specification of the size.  Once again, two versions, F77 and F90
! are provided. The logical condition here is explained as follows:
! at first, reject declaration where the first symbol after type
! declaration is opening bracket (this is possible only if it is
! already a F90/95 style declaration with explicit size specification
! ==> no further action is required). If the symbol is not opening
! bracket, then three possibilities may occur: (i) it is a comma,
! separating type and F90 attribute (like pointer, dimension, etc.,);
! (ii) it is still a blanc character, which means that the line
! contains just one word, say REAL, and contunues on the next line
! (typical situation in  code); or (iii) there symbols other
! that '*' (already considered above) and '(', separated by at least
! one blank space: these are etther variable names, or F90 double
! colon :: separater. In the all three cases (i -- iii) the line is
! considered as default type declaration, and should be processed.
!
          elseif (symbol(j).ne.'(' .and. (symbol(j).eq.' ' .or.
     &                      j.gt.i+1 .or. symbol(j).eq.',')) then

          endif
        endif



!
! Recognize and transform parallel loops:
!---------- --- --------- -------- ------
! The following code segment rearranges direction of consecutive
! 'do tile=my_first,my_last' loops into zig-zag order. This version
! is designed to work in conjuction with the OpenMP version of main.F
! (or any other code), so that no parallel directive is required in
! front of the loop in order for the rearrangement to occur: it is
! simpy triggered by the occurrence of 'do tile=my_***' with possible
! blank spaces between 'tile', '=' and 'my_***', where 'my_***' may
! be anything like handcoded 'my_first,my_last' or 'my_tile_range'. 
!
! Compared to the straightforward sequence, this measure eliminates
! secondary cache misses, because after passing a synchronization
! point, each thread proceeds with the same tile it was processing
! just before the synchronization point. It also reduces the
! probability of mutual cache_line invalidation (by factor of two)
! in a multiprocessor machine, if multiprocessing is allowed for the
! subdomains adjacent in the XI direction. This is because after a
! subdomain has been processed by a processor and a synchronization
! point has been reached, all cache_lines which are going across the
! subdivision partitioning are coherent with the cache of _that_
! processor (and invalid with respect to the one working on the
! adjacent subdomain). And it is _that_ processor (and not the
! adjacent one), who proceeds with this subdomain, and therefore
! enjoys all its cache_lines coherent with its cache at this moment.
!
        i=istrt
        if (symbol(i).eq.'d' .and. symbol(i+1).eq.'o') then
          i=i+2
          do while (symbol(i).eq.' ' .and. i.lt.length)
            i=i+1
          enddo
          if (symbol(i  ).eq.'t' .and. symbol(i+1).eq.'i' .and.
     &        symbol(i+2).eq.'l' .and. symbol(i+3).eq.'e') then
            i=i+4
            do while (symbol(i).eq.' ' .and. i.lt.length)
              i=i+1
            enddo
            if (symbol(i).eq.'=') then
              i=i+1
              do while (symbol(i).eq.' ' .and. i.lt.length)
                i=i+1
              enddo
     
              if (symbol(i).eq.'m' .and. symbol(i+1).eq.'y' .and.
     &                                   symbol(i+2).eq.'_') then
                dir_switch=.not.dir_switch
                if (dir_switch) then
                  scratch='my_first,my_last,+1'
                else
                  scratch='my_last,my_first,-1'
                endif
                do j=1,19
                  symbol(i+j-1)=scratch(j:j)
                enddo
                length=i+18
              endif
            endif
          endif
        endif
!
! Similarly as above, but designed to work for version of main.F,
! where parallel directives are inserted in front of each parallel 
! loop over subdomains "tiles", i.e., loops like
!
!      C$DOACROSS LOCAL (tile)
!          do tile=0,NSUB_X*NSUB_E-1
!
! are converted into two nested loops. The outer is loop over
! parallel threads (to be parallelized), and the inner is over
! tiles to be processed by the particular thread:
!
!          do trd=0,numthreads-1
!            do tile=forward_sweep
! or 
!          do trd=0,numthreads-1
!            do tile=backward_sweep
!
! in such a way the inner loops in the subsequent parallel regions
! are reversed, so that each thread zig-zags across the tiles it is
! processing.
!                             ! If the line is a parallel directive
        if (directive) then   ! containing attribute LOCAL(..) or 
          ip1=0               ! PRIVATE(..), and perhaps, SHARE(..),
          ip2=0               ! identify indices of the first and
          is1=0               ! the last symbol inside each bracket,
          is2=0               ! which are then used below to append
          i=1                                        ! either list. 
          do while (i.lt.length)
            if (symbol(i  ).eq.'L' .or. symbol(i  ).eq.'l') then
              if (symbol(i+1).eq.'O' .or. symbol(i+1).eq.'o') then
               if (symbol(i+2).eq.'C' .or. symbol(i+2).eq.'c') then
                if (symbol(i+3).eq.'A' .or. symbol(i+3).eq.'a') then
                 if (symbol(i+4).eq.'L' .or. symbol(i+4).eq.'l') then
                   i=i+5
                   do while (symbol(i).ne.'(' .and. i.lt.length)
                     i=i+1
                   enddo
                   if (symbol(i).eq.'(') then
                     ip1=i+1
                     do while (symbol(i).ne.')' .and. i.lt.length)
                       i=i+1
                     enddo
                     if (symbol(i).eq.')') ip2=i-1
                   endif
                 endif
                endif
               endif
              endif
            elseif(symbol(i).eq.'P' .or. symbol(i  ).eq.'p') then
              if (symbol(i+1).eq.'R' .or. symbol(i+1).eq.'r') then
               if (symbol(i+2).eq.'I' .or. symbol(i+2).eq.'i') then
                if (symbol(i+3).eq.'V' .or. symbol(i+3).eq.'v') then
                 if (symbol(i+4).eq.'A' .or. symbol(i+4).eq.'a') then
                  if (symbol(i+5).eq.'T'.or. symbol(i+5).eq.'t') then
                   if (symbol(i+6).eq.'E'.or.symbol(i+6).eq.'e') then
                     i=i+5
                     do while (symbol(i).ne.'(' .and. i.lt.length)
                       i=i+1
                     enddo
                     if (symbol(i).eq.'(') then
                       ip1=i+1
                       do while (symbol(i).ne.')' .and. i.lt.length)
                         i=i+1
                       enddo
                       if (symbol(i).eq.')') ip2=i-1
                     endif
                   endif
                  endif
                 endif
                endif
               endif
              endif
            elseif(symbol(i).eq.'S' .or. symbol(i  ).eq.'s') then
              if (symbol(i+1) .eq.'H' .or. symbol(i+1).eq.'h') then
               if (symbol(i+2) .eq.'A' .or. symbol(i+2).eq.'a') then
                if (symbol(i+3) .eq.'R'.or. symbol(i+3).eq.'r') then
                 if (symbol(i+4).eq.'E'.or. symbol(i+4).eq.'e') then
                   i=i+5
                   do while (symbol(i).ne.'(' .and. i.lt.length)
                     i=i+1
                   enddo
                   if (symbol(i).eq.'(') then
                     is1=i+1
                     do while (symbol(i).ne.')' .and. i.lt.length)
                       i=i+1
                     enddo
                     if (symbol(i).eq.')') is2=i-1
                   endif
                 endif    ! Skip to the beginning of the next word
                endif     ! for further checking. NOTE that 'PRIVATE'
               endif      ! in context of 'DO PRIVATE' is recognized 
              endif       ! (as it should be), but 'PRIVATE' within 
            endif         ! 'THREADPRIVATE' is disregarded because 
                          !  it not a complete word.
            i=i+1
            do while (symbol(i).ne.' ' .and. symbol(i).ne.',')
              i=i+1
            enddo
            do while (symbol(i).eq.' ' .and. i.lt.length)
              i=i+1
            enddo
          enddo

          if (ip1.gt.0 .and. ip2.eq.0) write(iout,'(A)')
     &      '### ERROR: No closing bracket in local/private list.' 
          if (is1.gt.0 .and. is2.eq.0) write(iout,'(A)')
     &             '### ERROR: No closing bracket in shared list.'
!
! It should be noted that a valid DOACROSS / DO PARALEL /DO ALL
! directive must have at least one valiable in its LOCAL/PRIVATE
! list to privatize the index of do-loop which follows it. Hence, 
! the presence of non-empty local list triggers further processing 
! in the following code segment: paste additional variables "trd"
! and subs into private list and "numthreads" into shared, in order
! to allow loop rearrangement. There are three possibilities here:
! SHARE attribute goes beforeLOCAL; vise versa; and and SHARE
! attribute is absent.
!                                           ! Save the whole string 
          if (ip1.gt.0 .and. ip2.gt.0) then ! into buffer before
            do i=1,length                   ! start messing with it.
              buffr(i)=symbol(i)
            enddo
            if (ip1.gt.is2 .and. is2.ge.is1 .and. is1.gt.0) then
              scratch = 'numthreads, '
              do i=1,12
                symbol(is1+i-1)=scratch(i:i)
              enddo
              do i=is1,ip1-1
                symbol(i+12)=buffr(i)
              enddo
              scratch = 'trd,subs, '
              do i=1,10
                symbol(ip1+i+11)=scratch(i:i)
              enddo
              do i=ip1,length
                symbol(i+22)=buffr(i)
              enddo
              length=length+22
            else
              scratch = 'trd,subs, '
              do i=1,10
                symbol(ip1+i-1)=scratch(i:i)
              enddo
              if (is1.gt.0 .and. is2.gt.0) then
                do i=ip1,is1-1
                  symbol(i+10)=buffr(i)
                enddo
                scratch = 'numthreads, '
                do i=1,12
                  symbol(is1+i+9)=scratch(i:i)
                enddo
                do i=is1,length
                  symbol(i+22)=buffr(i) 
                enddo
                length=length+22
              else
                do i=ip1,ip2+1
                  symbol(i+10)=buffr(i)
                enddo
                scratch = ', SHARED(numthreads)'
                do i=1,20
                  symbol(ip2+i+11)=scratch(i:i)
                enddo
                do i=ip2+2,length
                  symbol(i+30)=buffr(i)
                enddo
                length=length+30
              endif
            endif
!
! Write out the modified compiler directive, which is obviously
! longer than the original one, check its length and if it exceeds
! 72 symbols, split it into two. For the estetic purposes the split
! goes along a natural divider symbol, such as a intentionally
! placed blank character or a comma.
!
            if (length.le.72) then
              write(iout,'(72A1)') (symbol(i),i=1,length)
            else
              ks1=72
              do while (symbol(ks1).ne.' ' .and. ks1.gt.0)
                ks1=ks1-1
              enddo
              ks2=72
              do while (symbol(ks2).ne.',' .and. ks2.gt.0)
                ks2=ks2-1
              enddo
              if (ks1.gt.54) then
                k=ks1
              elseif (ks2.gt.6) then
                k=ks2
              else 
                write(iout,*) 'MPC ERROR: Cannot split directive.'
              endif
              write(iout,'(72A1)') (symbol(i),i=1,k)
              m=3
              symbol(m)='&'
              do i=m+1,k
                symbol(i)=' '
              enddo
              m=length-k
              do i=k+1,length
                symbol(i-m)=symbol(i)
              enddo
              write(iout,'(72A1)') (symbol(i),i=1,k)
            endif
            par_region=.true.
            goto 1
          endif
        endif
!
! Once a parallel region is detected and the compiler directive above
! was modified, transform the parallel loop over subdomains (tiles)
! into a set of nested loops over threads (outer loop) and subdomains
! within the work zone of each thread (inner loop), in such a way
! that the direction of the inner loop is always reversed with respect
! to the direction of similar loop in the previous parallel region
! (zig-zag sequence).  
!
        if (par_region) then
          is=7
          do while (symbol(is).eq.' ' .and. is.lt.length)
            is=is+1
          enddo
          if (symbol(is).eq.'d' .and. symbol(is+1).eq.'o') then
            is=is+2
            do while (symbol(is).eq.' ' .and. is.lt.length)
              is=is+1
            enddo
            if (symbol(is ).eq.'t' .and. symbol(is+1).eq.'i' .and.
     &          symbol(is+2).eq.'l' .and. symbol(is+3).eq.'e' .and.
     &                                   symbol(is+4).eq.'=') then
              scratch='0,NSUB_X*NSUB_E-1'
              lswtch=.true.
              do i=1,17 
                if (symbol(is+4+i).ne.scratch(i:i)) lswtch=.false.
              enddo
              if (lswtch) then
                lnest=.true.
                write(iout,'(7x,A/8x,A)') 'do trd=0,numthreads-1',
     &                             'subs=NSUB_X*NSUB_E/numthreads'
                dir_switch=.not.dir_switch
                if (dir_switch) then
                  write(iout,'(9x,A)')
     &               'do tile=subs*trd,subs*(trd+1)-1,+1'
                else
                  write(iout,'(9x,A)')
     &               'do tile=subs*(trd+1)-1,subs*trd,-1'
                endif
                goto 1
              endif
            endif
          elseif (lnest) then
            if (symbol(is).eq.'e' .and. symbol(is+1).eq.'n' .and.
     &          symbol(is+2).eq.'d' .and. symbol(is+3).eq.'d' .and.
     &                                     symbol(is+4).eq.'o') then
              lnest=.false.
              par_region=.false.
              write(iout,'(9x,A/7x,A)') 'enddo', 'enddo'
              goto 1
            endif
          endif
        endif
!
! Write the modified line of code into the output file.
!------ --- -------- ---- -- ---- ---- --- ------ -----
! Because its length after modification may exceed the standard of
! 72 symbols, it may be necessary to split the line into two. In this
! case several attempts are made to find a good (from estetic point
! of view) splitting point. First attempt is made by searching for
! the first blank symbol starting from position 72 and moving to the
! left; if such is not found, then the search starts from the
! beginning and attempt is made to split along a comma; if this is
! not successful, an assignment operator is being searched; then
! a mathematical operation. 
!
        if (length.gt.6 .and. length.le.72) then
           write(iout,'(72A1)') (symbol(i),i=1,length)
        elseif (length.gt.72) then

          ks1=0        ! Find appropriate places in the line where it
          ks2=0        ! may be split into two. This is just a purely
          ks3=0        ! estetic matter: line split may be done if
          ks4=0        ! there is a natural divider, such as a blank
          ks5=0        ! haracter in the middle; comma or a symbol of
                       ! of mathematical operation.
          do k=7,72
            if (symbol(k).eq.' ') then
              ks1=k
            elseif (symbol(k).eq.',') then
              ks2=k
            elseif (symbol(k).eq.'=') then
              ks3=k
            elseif (symbol(k).eq.'/') then
              ks4=k 
            elseif (symbol(k).eq.'+' .or.
     &              symbol(k).eq.'-'  .or.
     &              symbol(k).ne.'*') then
              ks5=k
            endif
          enddo
            
          if (length-ks1.gt.66) ks1=0
          if (length-ks2.gt.66) ks2=0
          if (length-ks3.gt.66) ks3=0
          if (length-ks4.gt.66) ks4=0
          if (length-ks5.gt.66) ks5=0
!
! Making final decision about the line split: there is nothing
! special here, it is just a matter of estetics to decide which of
! the possible breaking points (if more than one are available) is
! the most appropriate; the logical sequence below is designed as
! hierarchy of preferences. 
!
          if (ks1.gt.34) then
            k=ks1
          elseif (ks4.gt.6) then
            k=ks4-1
          elseif (ks2.gt.54) then
            k=ks2
          elseif (ks3.gt.60) then
            k=ks3
          elseif (ks5.gt.6) then
            k=ks5-1
          else
            write(iout,*) 'MPC ERROR: Cannot split line'
          endif
!
! Write out the line. First write the first part of the line; then
! create a continuation line resetting the starting symbols to blank,
! and them moving the tail to the left in such a way that its end
! will be in the position 72. Also put a continuation character into
! position 6. 
!
          write(iout,'(72A1)') (symbol(i),i=1,k) 
          do i=1,k
            symbol(i)=' '
          enddo
          symbol(6)='&'
          m=length-72
          do i=k+1,length
            symbol(i-m)=symbol(i)
          enddo
          write(iout,'(72A1)') (symbol(i),i=1,72)
        endif
       if (not_end_of_file) go to 1
      if (iout.gt.6) close(iout)
      if (iin.gt.5) close(iin)
      stop
      end
       
