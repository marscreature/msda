      implicit none
#include "cppdefs.h"
#include "param.h"
#include "das_param.h"
#include "das_param_lr.h"
#include "scalars.h"
#include "ncscrum.h"
#include "grid.h"
#include "ocean3d.h"
#include "ocean2d.h"
#include "das_ocean.h"
#include "das_ocean_lr.h"
#include "das_ocean9.h"
#include "das_covar.h"
#include "das_lbgfs.h"
#include "das_innov.h"
      integer tile, subs, trd, ierr, ios, nsyn
      integer i,j,k,max_tile
!
! minimization
!
      REAL EPS,GTOL
      INTEGER LP,IPRINT(2),IFLAG ,POINT,ITER,MP,ICALL
      INTEGER MAXITER
      LOGICAL DIAGCO
      EXTERNAL VA15CD
      COMMON /VA15DD/MP,LP, GTOL
      real costpp
!
! ... observation files
!
#ifdef MPI
      include 'mpif.h'
      real*8 start_time2, start_time1, exe_time
      call MPI_Init (ierr)
      start_time1=MPI_Wtime()
      call MPI_Setup (ierr)
      if (ierr.ne.0) goto 100                            !--> ERROR
c**   call MPI_Test
c**   goto 100
#endif

#define CR
#define SINGLE NSUB_X*NSUB_E,NSUB_X*NSUB_E !!!!

      call read_inp (ierr)           ! Read in tunable model
      if (ierr.ne.0) goto 100        ! parameters.
      call init_scalars (ierr)       ! Also initialize global
      if (ierr.ne.0) goto 100        ! scalar variables.
      call das_init_scalars          ! 

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! Create parallel threads;
        call start_timers()          ! start timers for each thread;
        call init_arrays (tile)      ! initialize (FIRST-TOUCH) model 
        call das_init_arrays (tile)  ! initialize (FIRST-TOUCH) DAS
      enddo                          ! global arrays (most of them 
CR      write(*,*) '-12' MYID        ! are just set to to zero).
!
      call get_grid                  ! from GRID NetCDF file). 
      if (may_day_flag.ne.0) goto 99     !-->  EXIT

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! Compute various metric
        call setup_grid1 (tile)      ! term combinations.
      enddo
CR      write(*,*) '-11' MYID
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call setup_grid2 (tile)
      enddo
CR      write(*,*) '-10' MYID
                                     ! Set up vertical S-coordinate
#ifdef SOLVE3D
      call set_scoord                ! and fast-time averaging  
#endif                               
CR      write(*,*) ' -9' MYID 

#ifdef SOLVE3D
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! S-coordinate system, which
        call set_depth (tile)        ! may be neded by ana_ninitial
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call grid_stiffness (tile) 
      enddo                          ! (here it is assumed that free
CR      write(*,*) ' -7' MYID        ! surface zeta=0).
#endif
!
#ifdef ANA_INITIAL
      if (nrrec.eq.0) then
C$OMP PARALLEL DO PRIVATE(tile)
        do tile=0,NSUB_X*NSUB_E-1    ! for primitive variables
          call ana_initial (tile)    ! (analytically or read
        enddo                        ! from initial conditions
      else                           ! NetCDF file).
#endif
        call get_initial
        iic=0        ! required by set_depth
#ifdef ANA_INITIAL
      endif
#endif

CR      write(*,*) ' -6' MYID 
      if (may_day_flag.ne.0) goto 99     !-->  EXIT
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! S-coordinate system: at this
        call set_depth (tile)        ! time free surface is set to
      enddo                          ! a non-zero field, either 
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_setup1 (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_setup2 (tile)
      enddo
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! Interpolation from S-coordinate system
        call das_s2z_3d (tile)          ! to z-coordinates
      enddo

      call das_get_initial_lr        ! read the increment from the low resolution DA

!      print*, 'lat_lr', Lm_lr, Mm_lr
!      print*, (lat_lr(i,Mm_Lr+1),i=0,Lm_lr+1)
!      print*, (lon_lr(i,Mm_Lr+1),i=0,Lm_lr+1)
!      print*, (lat_lr(i,0),i=0,Lm_lr+1)
!      print*, (lon_lr(i,0),i=0,Lm_lr+1)
!      print*, 'T'
!      print*, (t_lr(i,Mm_Lr+1,ndas,1),i=0,Lm_lr+1)
!      print*, 'S'
!      print*, (t_lr(i,Mm_Lr+1,ndas,2),i=0,Lm_lr+1)
!      print*, 'u'
!      print*, (u_lr(i,Mm_Lr+1,ndas),i=0,Lm_lr+1)
!      print*, 'v'
!      print*, (v_lr(i,Mm_Lr+1,ndas),i=0,Lm_lr+1)
!      print*, 'Z'
!      print*, (zeta_lr(i,Mm_Lr+1),i=0,Lm_lr+1)

      if (flag_lr_da .eq. 0 ) then
C$OMP PARALLEL DO PRIVATE(tile)
        do tile=0,NSUB_X*NSUB_E-1
          call das_interp_lr (tile)
          call das_add_lr (tile)
        enddo
      endif
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! Interpolation from S-coordinate system
        call das_fill9 (tile)
      enddo

!      print*, 'T'
!      print*, (t_lr_s(i,Mm+1,ndas,1),i=0,Lm+1)
!      print*, 'S'
!      print*, (t_lr_s(i,Mm+1,ndas,2),i=0,Lm+1)
!      print*, 'u'
!      print*, (u_lr_s(i,Mm+1,ndas),i=0,Lm+1)
!      print*, 'v'
!      print*, (v_lr_s(i,Mm+1,ndas),i=0,Lm+1)
!      print*, 'Z'
!      print*, (zeta_lr_s(i,Mm+1),i=0,Lm+1)

!C$OMP PARALLEL DO PRIVATE(tile)
!      do tile=0,NSUB_X*NSUB_E-1      ! Interpolation from S-coordinate system
!        call das_z2s (tile)          ! to z-coordinates
!      enddo      

!
!... prepare some basic variables
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1   
        call das_extrapolate9 (tile)
        call das_rho_eos9 (tile)   
      enddo
!
! ... get the file names of observations and error covariances
!
      call das_read_inp
      write(*,'(6x, A, 2x, I3)') 'das time level', time_level
!
! ... get the forecast error covariance
!
# ifdef DAS_ANA_BVAR
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_ana_bvar(tile)
      enddo
# else
      call das_get_bvar
# endif

      do k=1,ndas
        do j=0,Mm+1
          do i=0,Lm+1
!good             bt_das(i,j,k,itemp)=1.0*bt_das(i,j,k,itemp)+0.03
!good             bt_das(i,j,k,isalt)=0.6*bt_das(i,j,k,isalt)+0.01
!             bt_das(i,j,k,itemp)=1.0*bt_das(i,j,k,itemp)  !v1
!             bt_das(i,j,k,isalt)=0.6*bt_das(i,j,k,isalt)  !v1
!             bt_das(i,j,k,itemp)=0.85*bt_das(i,j,k,itemp)+0.35
!             bt_das(i,j,k,isalt)=0.25*bt_das(i,j,k,isalt)+0.05
             bt_das(i,j,k,itemp)=0.9*bt_das(i,j,k,itemp)
             bt_das(i,j,k,isalt)=0.65*bt_das(i,j,k,isalt)

!             bt_das(i,j,k,isalt)=0.65*bt_das(i,j,k,isalt)
!             bpsi_das(i,j,k)=1.00*bpsi_das(i,j,k)
!             bchi_das(i,j,k)=0.25*bchi_das(i,j,k)
! HF2
!             bpsi_das(i,j,k)=0.5*bpsi_das(i,j,k)
!             bchi_das(i,j,k)=0.25*bchi_das(i,j,k)
!bench        bpsi_das(i,j,k)=0.35*10.*bpsi_das(i,j,k)
!             bchi_das(i,j,k)=0.35*5.*bchi_das(i,j,k)
             bpsi_das(i,j,k)=0.28*10.*bpsi_das(i,j,k)
             bchi_das(i,j,k)=0.28*5.*bchi_das(i,j,k)
          enddo
        enddo
      enddo
      do j=0,Mm+1
        do i=0,Lm+1
!           bz_das(i,j)=1.5*bz_das(i,j)
!           bz_das(i,j)=0.5*bz_das(i,j)  !v1
           bz_das(i,j)=1.0*(0.6*exp(-h(i,j)*h(i,j)/(150.*150.))+1.0)
     &                 * bz_das(i,j) 
        enddo
      enddo
      call das_set_geo_ratio

!      write(*,*) 'geo_ratio'
!      write(*,*) (georatio(i,50),i=1,LLm)
!      write(*,*) 'geo_ratio'
!      write(*,*) (georatio(200,j),j=1,MMm)

      write(*,*) 'bt_var'
      write(*,*) (bt_das(50,50,k,itemp),k=1,ndas)
      write(*,*) 'bs_var'
      write(*,*) (bt_das(30,40,k,isalt),k=1,ndas)
      write(*,*) 'bpsi_var'
      write(*,*) (bpsi_das(30,40,k),k=1,ndas)
      write(*,*) 'bchi_var'
      write(*,*) (bchi_das(30,40,k),k=1,ndas)
      write(*,*) 'bz_var'
      write(*,*) (bz_das(30,k), k=20,40)
!
# ifdef DAS_BVAR_CORR
      call das_get_corr
# endif
!
#if defined DAS_JASONSSH || defined DAS_TPSSH\
 || defined DAS_ERS2SSH\
 || defined DAS_GFOSSH || defined DAS_SWOTSSH
      call das_get_ssh_ref
#endif
!       write(*,*) 'ssh_ref'
!       write(*,*) (ssh_ref(200,j),j=1,Mm)
!
! ... read in all the observational data
!
                       ! IN-SITU, whoi, sio, ptsur, martin vertical profiles
                       !          NPS flight sst
      call das_get_insitu
                       ! HF radar
      call das_get_hfradar
!
!
! ... get CalPoly & DORADO auv
!
      call das_get_auv
!      print*,'calpoly auv ', num_cal
!      print*,'dorado  auv ', num_dor
!
                       ! SST from TMI and dist_coast
# ifdef DAS_TMISST
      call das_get_tmi
# endif

!      write(*,*) 'dist_coast'
!      write(*,*) (dist_coast(i,20),i=0,Lm+1)
                       ! SST from AVHRR
# ifdef DAS_MCSST
      call das_get_mc
# endif
# ifdef DAS_GOES_SST
      call das_get_goes
# endif
                       ! SSH from JASON-1
# ifdef DAS_SWOTSSH
      call das_get_swot
# endif
# ifdef DAS_JASONSSH
      call das_get_js1
# endif
                       ! SSH from TOPEX/POSAIDON
# ifdef DAS_TPSSH
      call das_get_tp
# endif
!
      write (*,'(9(2x,A))') 'obs num:', 'hfradar','hfradar6','mooring',
     &                       'flight','whoi','sio',
     &                       'ptsur','martin'
      write (*,'(10x,8I7)') num_hfradar,num_hfradar6,prf_num_moor,
     &                      num_flight,prf_num_whoi,prf_num_sio,
     &                      prf_num_ptsur,prf_num_martn

                                          ! when no any observation
                                          ! data assimilation not executed
      if(.not. flag_tmi .and. .not. flag_mc .and. .not. flag_goes
     &   .and. .not. flag_swot
     &   .and. .not. flag_js1 .and. .not. flag_tp 
     &   .and. .not. flag_sio .and. .not. flag_whoi
     &   .and. .not. flag_ptsur .and. .not. flag_martn 
     &   .and. .not. flag_moor .and. .not. flag_prof 
     &   .and. .not. flag_ship
     &   .and. .not. flag_cal .and. .not. flag_dor
     &   .and. .not. flag_flight
     &   .and. .not. flag_hfradar.and. .not. flag_hfradar6) then
        write(*,'(6x, A, 2(/6x, A))') 
     &              '!!! NO ANY OBSERVATION AVAILABLE',
     &              '!!! DATA ASSIMILATION NOT EXECUTED',
     &              '!!! NO ANALYSIS FILE'
        goto 99
      endif

!      if (flag_mc) then
!        do i=1,Lm
!        do j=1,Mm
!        if (mc_mask(i,j) .gt. 0.0 ) then
!          write (*,*) sst_mc(i,j)
!        endif
!        enddo
!        enddo
!      endif
!
! ... compute the innovation vectors
! ... required by the incremental formulation
!
      max_tile=max(numthreads,prf_num_ptsur)
C$OMP PARALLEL DO PRIVATE(k), SHARED(max_tile)
      do k=1,max_tile
        call DAS_INNOV_PTSUR(k)
      enddo
!
      max_tile=max(numthreads,prf_num_martn)
C$OMP PARALLEL DO PRIVATE(k), SHARED(max_tile)
      do k=1,max_tile
        call DAS_INNOV_MARTN(k)
      enddo
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_innov_sio(tile)
        call das_innov_whoi(tile)
        call das_innov_moor(tile)
        call das_innov_flight(tile)
        call das_innov_calauv(tile)
        call das_innov_dorauv(tile)
# if defined DAS_JASONSSH || defined DAS_SWOTSSH
        call das_innov_swot(tile)
        call das_innov_js1(tile)
# endif
#if defined DAS_MCSST || defined DAS_TMISST
        call das_innov_sst(tile)
# endif
#if defined DAS_HFRADAR
        call das_innov_hfradar (tile)
#endif
      enddo


!
! print out innovations
!

!
! START MINIMIZATION
!
      ICALL=0
      IFLAG=0
      MAXITER=30
      IPRINT(1)= 1
      IPRINT(2)= 0
      DIAGCO= .FALSE.
      EPS= 1.0D-5     ! minimization criterion
                      ! |grad|/max(1, |x|) < EPS, terminated
      LP = 6
      MP = 6

!
! ... initialize the first guess of increment to be zero
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_CA2ZERO (tile)
      enddo
      CALL DAS_CA2VEC    ! control variable vector
!
! ... minimization interation starts
!
  10  CONTINUE
!
! ... initialize the cost function
!
      COSTF=0.0
!
! ... compute the matrix-vector product 
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL das_cross_ts(tile)
      enddo

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_MATVEC(tile)  
      enddo

!
! ... terms of the SSH
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_VARVEC (tile)      ! t_s
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_REG(tile)
      enddo

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_LIN_RHO_EOS (tile) ! rho_s
        CALL DAS_DIAG_SSH_STERIC (tile)    ! zeta_h, steric SSHs
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_SWOT(tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
#if defined DAS_JASONSSH
        CALL DAS_COST_JS1(tile)   ! cost function SSH
#endif
        CALL DAS_DIAG_PRESSURE (tile)     ! geostrophic p, T/S are geostrophic?
      enddo

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_PRESSURE_SMOOTH (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COPY_PRESSURE_SM (tile)
      enddo

       costpp=costf
!      write(*,*) 'zeta_s+H'
!      write(*,*) (zeta_s(53,j),j=53,Mm)
#if defined DAS_HFRADAR
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_GEOS_UV_P (tile)      !u_s, v_s before psichi
        CALL DAS_GEO_RATIO (tile)
        CALL DAS_PSICHI_UV (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_HFRADAR (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_REG_UV(tile)   ! cost function SST
      enddo
#endif
      costpp=costf-costpp
      write(*,*) '     HF COST=', costpp
!
! ... term of the SST
!
! flight
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_FLIGHT(tile)   ! cost function SST
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_CALAUV(tile)   
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_DORAUV(tile)   
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_MOOR(tile)   
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_GLIDER_SIO(tile)   
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_GLIDER_WHOI(tile)   
      enddo
!
! satellite
!
#if defined DAS_MCSST || defined DAS_TMISST
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_SST(tile)   ! cost function SST
      enddo
#endif
!
! ... gliders
!
      CALL DAS_COST_GLIDER
!
! ... term of the background
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COSTB(tile)   ! cost function background
      enddo

!
! ... compute the gradient with respect to u, v, t, s
!
! ... clean the adjoint arrays
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_ZERO(tile)
      enddo
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_COSTB(tile)
      enddo
!
! glider
!
      CALL DAS_ADJ_CGLIDER
!
#if defined DAS_MCSST || defined DAS_TMISST
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_CSST(tile)
      enddo
#endif
!
! CalPoly AUV
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_GLIDER_WHOI(tile)
        CALL DAS_ADJ_GLIDER_SIO(tile)
        CALL DAS_ADJ_MOOR(tile)
        CALL DAS_ADJ_DAUV(tile)
        CALL DAS_ADJ_CAUV(tile)
      enddo
!
! flight
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_CFLIGHT(tile)
      enddo
!
#if defined DAS_HFRADAR
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_COST_REG_UV(tile)
      enddo

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_CHFRADAR (tile)
      enddo

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_PSICHI_UV (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_GEO_RATIO (TILE)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_GEOS_UV_P (tile)
      enddo
#endif   /* DAS_HFRADAR */

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_COPY_PRESSURE_SM (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_PRESSURE_SM (tile)
      enddo

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_DIAG_P (tile)
      enddo

!      write(*,*) 'zeta_s_adj'
!      write(*,*) (zeta_s_adj(10,j),j=50,Mm)
!      write(*,*) 'zeta_s_adj'
!      write(*,*) (zeta_s_adj(50,j),j=50,Mm)
      
!
#if defined DAS_JASONSSH 
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_CJS1(tile)   ! cost function SSH
      enddo
#endif
#if defined DAS_SWOTSSH
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_CSWOT(tile)   ! cost function SSH
      enddo
#endif
!      write(*,*) 'zeta_s 50'
!      write(*,*) (zeta_s(53,j),j=50,Mm)
!
!      write(*,*) 'ssh_js1 53'
!      write(*,*) (ssh_js1(53,j),j=50,Mm)
!
!      write(*,*) 'zeta_s_adj cjs1'
!      write(*,*) (zeta_s_adj(53,j),j=50,Mm)

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_DIAG_SSH_STERIC (tile)
        CALL DAS_ADJ_RHO_EOS (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_COST_REG (tile)
        CALL DAS_ADJ_VARVEC (tile)
      enddo
!
! ... compute the matrix-vector product
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_MATVEC(tile)
      enddo

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_CROSS_TS(tile)
      enddo
!
! ... clean the intermediate adjoint arrays
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_ZERO_S(tile)
      enddo
!
      CALL DAS_GA2VEC    ! gradient vector
!
      CALL mbfgs(NDIM,MSAVE,COSTF,DIAGCO,IPRINT,EPS,POINT,IFLAG)

      CALL DAS_VEC2CA        !vector to control variables

      IF(IFLAG.LE.0) GO TO 50
      ICALL=ICALL + 1
      IF(ICALL.GT.MAXITER) GO TO 50
  
      GOTO 10
 
  50  CONTINUE
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!   minimization done
!
      print*, '     FLAG=', IFLAG
      if (IFLAG .lt. 0) then
        write(*, '(6x, A, (/6x, A))')
     &   '!!! MINIMIZATION NOT COMPLETED',
     &   '!!! NO ASSIMILATED ANALYSIS GENERATED'
        goto 99
      else
        write(*, '(6x, A)')
     &   ' MINIMIZATION COMPLETED'
      endif
!
! ... compute incremental analysis
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL das_cross_ts(tile)
      enddo

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1   !temp and salt
        call das_matvec (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_varvec (tile)       ! t_s
      enddo

!      write(*,*) 'zeta_s'
!      write(*,*) (zeta_s(53,j),j=50,Mm)
!
#define DAS_SMOOTH
#ifdef DAS_SMOOTH
                    ! smooth t_s to t_sm
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_innov_smooth (tile)
      enddo
                    ! then t_sm to t_s
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_copy_smooth (tile)
      enddo
#endif
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_lin_rho_eos (tile)  ! rho_s
        call das_diag_ssh_steric (tile)     ! zeta_s -> zeta_s
      enddo
!C$OMP PARALLEL DO PRIVATE(tile)
!      do tile=0,NSUB_X*NSUB_E-1
!        call das_zetah_sm (tile)     ! smooth zeta_h
!      enddo
!C$OMP PARALLEL DO PRIVATE(tile)
!      do tile=0,NSUB_X*NSUB_E-1       
!        call das_copy_zetah_sm (tile)     ! smooth zeta_h           
!      enddo 
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_zeta_smooth (tile)  ! zeta_sm
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_copy_zeta_smooth (tile)   !zeta_sm -> zeta_s
        call das_diag_pressure (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL das_pressure_smooth (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL das_copy_pressure_sm (tile)
      enddo

!!!!!!!!!!!!!!!!!!!!!!!!
#undef PRINF_GEO

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_geos_uv_p (tile)      ! u_s and v_s
        call das_geo_ratio (tile)
#if !defined PRINF_GEO
        call das_psichi_uv (tile)      ! u_s,vs -> us,vs
#endif    
      enddoo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_innov_uv_smooth (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1                          
        call das_copy_uv_smooth (tile)                  
      enddo

#ifdef PRINF_GEO
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_diag_ssh (tile)     ! zeta
      enddo
#endif   
      
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_add_lr_s (tile)  !the large scale component, bias
        call das_analysis (tile)
      enddo

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call rho_eos (tile)
      enddo

      write(*,*) 'rho 80 '
      write(*,*) (rho(50,50,k), k=1,n)
      write(*,*) 'rho 120 '
      write(*,*) (rho(80,80,k), k=1,n)
!
! ... create new restart file
! ... with the analysis from 3DVAR
!
      iic=ntstart
      call wrt_rst
!      call das_wrt_rst   
!...
!
      write(*,*) '     iic==', iic

      write(*,*) 't_s j=20 '
      write(*,*) (t_s(i,20,ndas,1), i=0,Lm+1)

!      write(*,*) 'psi 80 '
!      write(*,*) (psi_s(15,j,ndas), j=1,Mm)
!
!      write(*,*) 'chi 80 '
!      write(*,*) (chi_s(15,j,ndas), j=1,Mm)
!
                                     ! JOB DONE  
  99  continue                       ! SHUTDOWN:
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! close netCDF files.
        call stop_timers()
      enddo
      call closecdf

 100  continue
#ifdef MPI
      call MPI_Barrier(MPI_COMM_WORLD, ierr)
      start_time2=MPI_Wtime()
      exe_time = start_time2 - start_time1
      if(mynode.eq.0) print*,'exe_time =',exe_time
      call MPI_Finalize (ierr)
#endif
      stop
      end

