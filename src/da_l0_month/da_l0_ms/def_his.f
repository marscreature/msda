      subroutine def_his (ncid, total_rec, ierr)
      implicit none
      logical create_new_file
      integer ncid, total_rec, ierr, rec, lstr,lvar,lenstr, timedim
     &      , r2dgrd(3),  u2dgrd(3), v2dgrd(3), auxil(2), checkdims
     &      , b3dgrd(4)
     &      , r3dgrd(4),  u3dgrd(4), v3dgrd(4), w3dgrd(4), itrc
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=384,  MMmH=390, NnH=66)
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLmH,  MMm=MMmH,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      real  x_sponge,   v_sponge
       real  tauT_in, tauT_out, tauM_in, tauM_out
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &                      , x_sponge,   v_sponge
     &                      , tauT_in, tauT_out, tauM_in, tauM_out
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
      integer indxU, indxV, indxT
      parameter (indxU=5, indxV=6, indxT=7)
      integer indxS
      parameter (indxS=indxT+1)
      integer indxO, indxW, indxR, indxVisc, indxAkv, indxAkt
      parameter (indxO=indxT+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed
     &                                  +ntrc_diats+ntrc_diauv+1,
     &           indxW=indxO+1, indxR=indxO+2,
     &           indxVisc=indxR+1,
     &           indxAkv=indxR+2, indxAkt=indxAkv+1)
      integer indxAks
      parameter (indxAks=indxAkt+1)
      integer indxSSH
      parameter (indxSSH=indxAkt+4)
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
      integer indxSHFl, indxSWRad
      parameter (indxSHFl=indxSSH+3)
      integer indxSSFl
      parameter (indxSSFl=indxSHFl+1, indxSWRad=indxSHFl+2)
      integer indxSST, indxdQdSST
      parameter (indxSST=indxSWRad+1, indxdQdSST=indxSWRad+2)
      integer indxSSS
      parameter (indxSSS=indxSST+2)
      integer indxBostr
      parameter (indxBostr=indxSUSTR+16)
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     &                u3dvar, v3dvar, p3dvar, w3dvar, b3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8,b3dvar=12)
      integer xi_rho,xi_u, eta_rho,eta_v
      parameter (xi_rho=LLm+2,  xi_u=xi_rho-1,
     &           eta_rho=MMm+2, eta_v=eta_rho-1)
      integer ncidfrc, ncidbulk, ncidclm,  ntsms
     &      , ntsrf,  ntssh,  ntsst, ntsss, ntuclm, ntww,
     &        ntbulk
      integer nttclm(NT),    ntstf(NT)
      integer ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                  ,rstR  , rstU,    rstV
      integer rstT(NT)
      integer  ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,  hisV,  hisR,    hisHbl, hisHbbl
     &      , hisO,  hisW,  hisVisc,  hisAkv,  hisAkt, hisAks
     &      , hisBostr
      integer hisT(NT)
      integer   ncidavg, nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,  avgV,  avgR,    avgHbl, avgHbbl
     &      , avgO,  avgW,  avgVisc, avgAkv,  avgAkt, avgAks
      integer avgT(NT)
      integer avgBostr
      logical wrthis(60+NT)
     &      , wrtavg(60+NT)
      common/incscrum/
     &        ncidfrc, ncidbulk,ncidclm, ntsms, ntsrf, ntssh, ntsst
     &      , ntuclm, ntsss, ntww, ntbulk
     &                        ,  nttclm,          ntstf
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                  ,rstR  , rstU,    rstV,   rstT
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT,    hisR
     &      , hisO,    hisW,     hisVisc, hisAkv,  hisAkt, hisAks
     &      , hisHbl,  hisHbbl,  hisBostr
     &      , ncidavg,  nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT,    avgR
     &      , avgO,    avgW,     avgVisc,  avgAkv,  avgAkt, avgAks
     &      , avgHbl,  avgHbbl
     &      , avgBostr
     &      , wrthis
     &      , wrtavg
      character*80 date_str, title
      character*80 ininame,  grdname,  hisname
     &         ,   rstname,  frcname,  bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
      character*52  vname(4, 150)
      common /cncscrum/       date_str,   title
     &         ,   ininame,  grdname, hisname
     &         ,   rstname,  frcname, bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
     &                      ,  vname
      integer NF_BYTE
      integer NF_INT1
      integer NF_CHAR
      integer NF_SHORT
      integer NF_INT2
      integer NF_INT
      integer NF_FLOAT
      integer NF_REAL
      integer NF_DOUBLE
      parameter (NF_BYTE = 1)
      parameter (NF_INT1 = NF_BYTE)
      parameter (NF_CHAR = 2)
      parameter (NF_SHORT = 3)
      parameter (NF_INT2 = NF_SHORT)
      parameter (NF_INT = 4)
      parameter (NF_FLOAT = 5)
      parameter (NF_REAL = NF_FLOAT)
      parameter (NF_DOUBLE = 6)
      integer           NF_FILL_BYTE
      integer           NF_FILL_INT1
      integer           NF_FILL_CHAR
      integer           NF_FILL_SHORT
      integer           NF_FILL_INT2
      integer           NF_FILL_INT
      real              NF_FILL_FLOAT
      real              NF_FILL_REAL
      doubleprecision   NF_FILL_DOUBLE
      parameter (NF_FILL_BYTE = -127)
      parameter (NF_FILL_INT1 = NF_FILL_BYTE)
      parameter (NF_FILL_CHAR = 0)
      parameter (NF_FILL_SHORT = -32767)
      parameter (NF_FILL_INT2 = NF_FILL_SHORT)
      parameter (NF_FILL_INT = -2147483647)
      parameter (NF_FILL_FLOAT = 9.9692099683868690e+36)
      parameter (NF_FILL_REAL = NF_FILL_FLOAT)
      parameter (NF_FILL_DOUBLE = 9.9692099683868690e+36)
      integer NF_NOWRITE
      integer NF_WRITE
      integer NF_CLOBBER
      integer NF_NOCLOBBER
      integer NF_FILL
      integer NF_NOFILL
      integer NF_LOCK
      integer NF_SHARE
      parameter (NF_NOWRITE = 0)
      parameter (NF_WRITE = 1)
      parameter (NF_CLOBBER = 0)
      parameter (NF_NOCLOBBER = 4)
      parameter (NF_FILL = 0)
      parameter (NF_NOFILL = 256)
      parameter (NF_LOCK = 1024)
      parameter (NF_SHARE = 2048)
      integer NF_UNLIMITED
      parameter (NF_UNLIMITED = 0)
      integer NF_GLOBAL
      parameter (NF_GLOBAL = 0)
      integer NF_MAX_DIMS
      integer NF_MAX_ATTRS
      integer NF_MAX_VARS
      integer NF_MAX_NAME
      integer NF_MAX_VAR_DIMS
      parameter (NF_MAX_DIMS = 100)
      parameter (NF_MAX_ATTRS = 2000)
      parameter (NF_MAX_VARS = 2000)
      parameter (NF_MAX_NAME = 128)
      parameter (NF_MAX_VAR_DIMS = NF_MAX_DIMS)
      integer NF_NOERR
      integer NF_EBADID
      integer NF_EEXIST
      integer NF_EINVAL
      integer NF_EPERM
      integer NF_ENOTINDEFINE
      integer NF_EINDEFINE
      integer NF_EINVALCOORDS
      integer NF_EMAXDIMS
      integer NF_ENAMEINUSE
      integer NF_ENOTATT
      integer NF_EMAXATTS
      integer NF_EBADTYPE
      integer NF_EBADDIM
      integer NF_EUNLIMPOS
      integer NF_EMAXVARS
      integer NF_ENOTVAR
      integer NF_EGLOBAL
      integer NF_ENOTNC
      integer NF_ESTS
      integer NF_EMAXNAME
      integer NF_EUNLIMIT
      integer NF_ENORECVARS
      integer NF_ECHAR
      integer NF_EEDGE
      integer NF_ESTRIDE
      integer NF_EBADNAME
      integer NF_ERANGE
      parameter (NF_NOERR = 0)
      parameter (NF_EBADID = -33)
      parameter (NF_EEXIST = -35)
      parameter (NF_EINVAL = -36)
      parameter (NF_EPERM = -37)
      parameter (NF_ENOTINDEFINE = -38)
      parameter (NF_EINDEFINE = -39)
      parameter (NF_EINVALCOORDS = -40)
      parameter (NF_EMAXDIMS = -41)
      parameter (NF_ENAMEINUSE = -42)
      parameter (NF_ENOTATT = -43)
      parameter (NF_EMAXATTS = -44)
      parameter (NF_EBADTYPE = -45)
      parameter (NF_EBADDIM = -46)
      parameter (NF_EUNLIMPOS = -47)
      parameter (NF_EMAXVARS = -48)
      parameter (NF_ENOTVAR = -49)
      parameter (NF_EGLOBAL = -50)
      parameter (NF_ENOTNC = -51)
      parameter (NF_ESTS = -52)
      parameter (NF_EMAXNAME = -53)
      parameter (NF_EUNLIMIT = -54)
      parameter (NF_ENORECVARS = -55)
      parameter (NF_ECHAR = -56)
      parameter (NF_EEDGE = -57)
      parameter (NF_ESTRIDE = -58)
      parameter (NF_EBADNAME = -59)
      parameter (NF_ERANGE = -60)
      integer  NF_FATAL
      integer NF_VERBOSE
      parameter (NF_FATAL = 1)
      parameter (NF_VERBOSE = 2)
      character*80   nf_inq_libvers
      external       nf_inq_libvers
      character*80   nf_strerror
      external       nf_strerror
      logical        nf_issyserr
      external       nf_issyserr
      integer         nf_create
      external        nf_create
      integer         nf_open
      external        nf_open
      integer         nf_set_fill
      external        nf_set_fill
      integer         nf_redef
      external        nf_redef
      integer         nf_enddef
      external        nf_enddef
      integer         nf_sync
      external        nf_sync
      integer         nf_abort
      external        nf_abort
      integer         nf_close
      external        nf_close
      integer         nf_delete
      external        nf_delete
      integer         nf_inq
      external        nf_inq
      integer         nf_inq_ndims
      external        nf_inq_ndims
      integer         nf_inq_nvars
      external        nf_inq_nvars
      integer         nf_inq_natts
      external        nf_inq_natts
      integer         nf_inq_unlimdim
      external        nf_inq_unlimdim
      integer         nf_def_dim
      external        nf_def_dim
      integer         nf_inq_dimid
      external        nf_inq_dimid
      integer         nf_inq_dim
      external        nf_inq_dim
      integer         nf_inq_dimname
      external        nf_inq_dimname
      integer         nf_inq_dimlen
      external        nf_inq_dimlen
      integer         nf_rename_dim
      external        nf_rename_dim
      integer         nf_inq_att
      external        nf_inq_att
      integer         nf_inq_attid
      external        nf_inq_attid
      integer         nf_inq_atttype
      external        nf_inq_atttype
      integer         nf_inq_attlen
      external        nf_inq_attlen
      integer         nf_inq_attname
      external        nf_inq_attname
      integer         nf_copy_att
      external        nf_copy_att
      integer         nf_rename_att
      external        nf_rename_att
      integer         nf_del_att
      external        nf_del_att
      integer         nf_put_att_text
      external        nf_put_att_text
      integer         nf_get_att_text
      external        nf_get_att_text
      integer         nf_put_att_int1
      external        nf_put_att_int1
      integer         nf_get_att_int1
      external        nf_get_att_int1
      integer         nf_put_att_int2
      external        nf_put_att_int2
      integer         nf_get_att_int2
      external        nf_get_att_int2
      integer         nf_put_att_int
      external        nf_put_att_int
      integer         nf_get_att_int
      external        nf_get_att_int
      integer         nf_put_att_real
      external        nf_put_att_real
      integer         nf_get_att_real
      external        nf_get_att_real
      integer         nf_put_att_double
      external        nf_put_att_double
      integer         nf_get_att_double
      external        nf_get_att_double
      integer         nf_def_var
      external        nf_def_var
      integer         nf_inq_var
      external        nf_inq_var
      integer         nf_inq_varid
      external        nf_inq_varid
      integer         nf_inq_varname
      external        nf_inq_varname
      integer         nf_inq_vartype
      external        nf_inq_vartype
      integer         nf_inq_varndims
      external        nf_inq_varndims
      integer         nf_inq_vardimid
      external        nf_inq_vardimid
      integer         nf_inq_varnatts
      external        nf_inq_varnatts
      integer         nf_rename_var
      external        nf_rename_var
      integer         nf_copy_var
      external        nf_copy_var
      integer         nf_put_var_text
      external        nf_put_var_text
      integer         nf_get_var_text
      external        nf_get_var_text
      integer         nf_put_var_int1
      external        nf_put_var_int1
      integer         nf_get_var_int1
      external        nf_get_var_int1
      integer         nf_put_var_int2
      external        nf_put_var_int2
      integer         nf_get_var_int2
      external        nf_get_var_int2
      integer         nf_put_var_int
      external        nf_put_var_int
      integer         nf_get_var_int
      external        nf_get_var_int
      integer         nf_put_var_real
      external        nf_put_var_real
      integer         nf_get_var_real
      external        nf_get_var_real
      integer         nf_put_var_double
      external        nf_put_var_double
      integer         nf_get_var_double
      external        nf_get_var_double
      integer         nf_put_var1_text
      external        nf_put_var1_text
      integer         nf_get_var1_text
      external        nf_get_var1_text
      integer         nf_put_var1_int1
      external        nf_put_var1_int1
      integer         nf_get_var1_int1
      external        nf_get_var1_int1
      integer         nf_put_var1_int2
      external        nf_put_var1_int2
      integer         nf_get_var1_int2
      external        nf_get_var1_int2
      integer         nf_put_var1_int
      external        nf_put_var1_int
      integer         nf_get_var1_int
      external        nf_get_var1_int
      integer         nf_put_var1_real
      external        nf_put_var1_real
      integer         nf_get_var1_real
      external        nf_get_var1_real
      integer         nf_put_var1_double
      external        nf_put_var1_double
      integer         nf_get_var1_double
      external        nf_get_var1_double
      integer         nf_put_vara_text
      external        nf_put_vara_text
      integer         nf_get_vara_text
      external        nf_get_vara_text
      integer         nf_put_vara_int1
      external        nf_put_vara_int1
      integer         nf_get_vara_int1
      external        nf_get_vara_int1
      integer         nf_put_vara_int2
      external        nf_put_vara_int2
      integer         nf_get_vara_int2
      external        nf_get_vara_int2
      integer         nf_put_vara_int
      external        nf_put_vara_int
      integer         nf_get_vara_int
      external        nf_get_vara_int
      integer         nf_put_vara_real
      external        nf_put_vara_real
      integer         nf_get_vara_real
      external        nf_get_vara_real
      integer         nf_put_vara_double
      external        nf_put_vara_double
      integer         nf_get_vara_double
      external        nf_get_vara_double
      integer         nf_put_vars_text
      external        nf_put_vars_text
      integer         nf_get_vars_text
      external        nf_get_vars_text
      integer         nf_put_vars_int1
      external        nf_put_vars_int1
      integer         nf_get_vars_int1
      external        nf_get_vars_int1
      integer         nf_put_vars_int2
      external        nf_put_vars_int2
      integer         nf_get_vars_int2
      external        nf_get_vars_int2
      integer         nf_put_vars_int
      external        nf_put_vars_int
      integer         nf_get_vars_int
      external        nf_get_vars_int
      integer         nf_put_vars_real
      external        nf_put_vars_real
      integer         nf_get_vars_real
      external        nf_get_vars_real
      integer         nf_put_vars_double
      external        nf_put_vars_double
      integer         nf_get_vars_double
      external        nf_get_vars_double
      integer         nf_put_varm_text
      external        nf_put_varm_text
      integer         nf_get_varm_text
      external        nf_get_varm_text
      integer         nf_put_varm_int1
      external        nf_put_varm_int1
      integer         nf_get_varm_int1
      external        nf_get_varm_int1
      integer         nf_put_varm_int2
      external        nf_put_varm_int2
      integer         nf_get_varm_int2
      external        nf_get_varm_int2
      integer         nf_put_varm_int
      external        nf_put_varm_int
      integer         nf_get_varm_int
      external        nf_get_varm_int
      integer         nf_put_varm_real
      external        nf_put_varm_real
      integer         nf_get_varm_real
      external        nf_get_varm_real
      integer         nf_put_varm_double
      external        nf_put_varm_double
      integer         nf_get_varm_double
      external        nf_get_varm_double
      integer nccre
      integer ncopn
      integer ncddef
      integer ncdid
      integer ncvdef
      integer ncvid
      integer nctlen
      integer ncsfil
      external nccre
      external ncopn
      external ncddef
      external ncdid
      external ncvdef
      external ncvid
      external nctlen
      external ncsfil
      integer NCRDWR
      integer NCCREAT
      integer NCEXCL
      integer NCINDEF
      integer NCNSYNC
      integer NCHSYNC
      integer NCNDIRTY
      integer NCHDIRTY
      integer NCLINK
      integer NCNOWRIT
      integer NCWRITE
      integer NCCLOB
      integer NCNOCLOB
      integer NCGLOBAL
      integer NCFILL
      integer NCNOFILL
      integer MAXNCOP
      integer MAXNCDIM
      integer MAXNCATT
      integer MAXNCVAR
      integer MAXNCNAM
      integer MAXVDIMS
      integer NCNOERR
      integer NCEBADID
      integer NCENFILE
      integer NCEEXIST
      integer NCEINVAL
      integer NCEPERM
      integer NCENOTIN
      integer NCEINDEF
      integer NCECOORD
      integer NCEMAXDS
      integer NCENAME
      integer NCENOATT
      integer NCEMAXAT
      integer NCEBADTY
      integer NCEBADD
      integer NCESTS
      integer NCEUNLIM
      integer NCEMAXVS
      integer NCENOTVR
      integer NCEGLOB
      integer NCENOTNC
      integer NCFOOBAR
      integer NCSYSERR
      integer NCFATAL
      integer NCVERBOS
      integer NCENTOOL
      integer NCBYTE
      integer NCCHAR
      integer NCSHORT
      integer NCLONG
      integer NCFLOAT
      integer NCDOUBLE
      parameter(NCBYTE = 1)
      parameter(NCCHAR = 2)
      parameter(NCSHORT = 3)
      parameter(NCLONG = 4)
      parameter(NCFLOAT = 5)
      parameter(NCDOUBLE = 6)
      parameter(NCRDWR = 1)
      parameter(NCCREAT = 2)
      parameter(NCEXCL = 4)
      parameter(NCINDEF = 8)
      parameter(NCNSYNC = 16)
      parameter(NCHSYNC = 32)
      parameter(NCNDIRTY = 64)
      parameter(NCHDIRTY = 128)
      parameter(NCFILL = 0)
      parameter(NCNOFILL = 256)
      parameter(NCLINK = 32768)
      parameter(NCNOWRIT = 0)
      parameter(NCWRITE = NCRDWR)
      parameter(NCCLOB = NF_CLOBBER)
      parameter(NCNOCLOB = NF_NOCLOBBER)
      integer NCUNLIM
      parameter(NCUNLIM = 0)
      parameter(NCGLOBAL  = 0)
      parameter(MAXNCOP = 32)
      parameter(MAXNCDIM = 100)
      parameter(MAXNCATT = 2000)
      parameter(MAXNCVAR = 2000)
      parameter(MAXNCNAM = 128)
      parameter(MAXVDIMS = MAXNCDIM)
      parameter(NCNOERR = 0)
      parameter(NCEBADID = 1)
      parameter(NCENFILE = 2)
      parameter(NCEEXIST = 3)
      parameter(NCEINVAL = 4)
      parameter(NCEPERM = 5)
      parameter(NCENOTIN = 6)
      parameter(NCEINDEF = 7)
      parameter(NCECOORD = 8)
      parameter(NCEMAXDS = 9)
      parameter(NCENAME = 10)
      parameter(NCENOATT = 11)
      parameter(NCEMAXAT = 12)
      parameter(NCEBADTY = 13)
      parameter(NCEBADD = 14)
      parameter(NCEUNLIM = 15)
      parameter(NCEMAXVS = 16)
      parameter(NCENOTVR = 17)
      parameter(NCEGLOB = 18)
      parameter(NCENOTNC = 19)
      parameter(NCESTS = 20)
      parameter (NCENTOOL = 21)
      parameter(NCFOOBAR = 32)
      parameter(NCSYSERR = -1)
      parameter(NCFATAL = 1)
      parameter(NCVERBOS = 2)
      integer FILBYTE
      integer FILCHAR
      integer FILSHORT
      integer FILLONG
      real FILFLOAT
      doubleprecision FILDOUB
      parameter (FILBYTE = -127)
      parameter (FILCHAR = 0)
      parameter (FILSHORT = -32767)
      parameter (FILLONG = -2147483647)
      parameter (FILFLOAT = 9.9692099683868690e+36)
      parameter (FILDOUB = 9.9692099683868690e+36)
      ierr=0
      lstr=lenstr(hisname)
      if (nrpfhis.gt.0) then
        lvar=total_rec-(1+mod(total_rec-1, nrpfhis))
        call insert_time_index (hisname, lstr, lvar, ierr)
        if (ierr .ne. 0) goto 99
      endif
      create_new_file=ldefhis
      if (ncid.ne.-1) create_new_file=.false.
  10  if (create_new_file) then
        ierr=nf_create(hisname(1:lstr), nf_clobber, ncid)
        if (ierr .ne. nf_noerr) then
          write(stdout,'(/3(1x,A)/)') 'ERROR in def_his/avg:',
     &           'Cannot create netCDF file:', hisname(1:lstr)
          goto 99
        endif
        if (nrpfhis.eq.0) total_rec=0
        call put_global_atts (ncid, ierr)
        ierr=nf_def_dim (ncid, 'xi_rho',   xi_rho,   r2dgrd(1))
        ierr=nf_def_dim (ncid, 'xi_u',     xi_u,     u2dgrd(1))
        ierr=nf_def_dim (ncid, 'eta_rho',  eta_rho,  r2dgrd(2))
        ierr=nf_def_dim (ncid, 'eta_v',    eta_v,    v2dgrd(2))
        ierr=nf_def_dim (ncid, 's_rho',    N,        r3dgrd(3))
        ierr=nf_def_dim (ncid, 's_w',      N+1,      w3dgrd(3))
        ierr=nf_def_dim (ncid, 'time', nf_unlimited, timedim)
        ierr=nf_def_dim (ncid, 'auxil',    4,        auxil(1))
        auxil(2)=timedim
        r2dgrd(3)=timedim
        u2dgrd(2)=r2dgrd(2)
        u2dgrd(3)=timedim
        v2dgrd(1)=r2dgrd(1)
        v2dgrd(3)=timedim
        b3dgrd(1)=r2dgrd(1)
        b3dgrd(2)=r2dgrd(2)
        b3dgrd(4)=timedim
        r3dgrd(1)=r2dgrd(1)
        r3dgrd(2)=r2dgrd(2)
        r3dgrd(4)=timedim
        u3dgrd(1)=u2dgrd(1)
        u3dgrd(2)=r2dgrd(2)
        u3dgrd(3)=r3dgrd(3)
        u3dgrd(4)=timedim
        v3dgrd(1)=r2dgrd(1)
        v3dgrd(2)=v2dgrd(2)
        v3dgrd(3)=r3dgrd(3)
        v3dgrd(4)=timedim
        w3dgrd(1)=r2dgrd(1)
        w3dgrd(2)=r2dgrd(2)
        w3dgrd(4)=timedim
        if (total_rec.le.1) call def_grid (ncid, r2dgrd)
        ierr=nf_def_var (ncid, 'time_step', nf_int, 2, auxil,
     &                                                 hisTstep)
        ierr=nf_put_att_text (ncid, hisTstep, 'long_name', 48,
     &       'time step and record numbers from initialization')
        lvar=lenstr(vname(1,indxTime))
        ierr=nf_def_var (ncid, vname(1,indxTime)(1:lvar),
     &                            NF_REAL, 1, timedim, hisTime)
        lvar=lenstr(vname(2,indxTime))
        ierr=nf_put_att_text (ncid, hisTime, 'long_name', lvar,
     &                                vname(2,indxTime)(1:lvar))
        lvar=lenstr(vname(3,indxTime))
        ierr=nf_put_att_text (ncid, hisTime, 'units',  lvar,
     &                                vname(3,indxTime)(1:lvar))
        lvar=lenstr(vname(4,indxTime))
        ierr=nf_put_att_text (ncid, hisTime, 'field',  lvar,
     &                                vname(4,indxTime)(1:lvar))
        if (wrthis(indxZ)) then
          lvar=lenstr(vname(1,indxZ))
          ierr=nf_def_var (ncid, vname(1,indxZ)(1:lvar),
     &                              NF_REAL, 3, r2dgrd, hisZ)
          lvar=lenstr(vname(2,indxZ))
          ierr=nf_put_att_text (ncid, hisZ, 'long_name', lvar,
     &                                  vname(2,indxZ)(1:lvar))
          lvar=lenstr(vname(3,indxZ))
          ierr=nf_put_att_text (ncid, hisZ, 'units',     lvar,
     &                                  vname(3,indxZ)(1:lvar))
          lvar=lenstr(vname(4,indxZ))
          ierr=nf_put_att_text (ncid, hisZ, 'field',     lvar,
     &                                  vname(4,indxZ)(1:lvar))
        endif
        if (wrthis(indxUb)) then
          lvar=lenstr(vname(1,indxUb))
          ierr=nf_def_var (ncid, vname(1,indxUb)(1:lvar),
     &                             NF_REAL, 3, u2dgrd, hisUb)
          lvar=lenstr(vname(2,indxUb))
          ierr=nf_put_att_text (ncid, hisUb, 'long_name', lvar,
     &                                  vname(2,indxUb)(1:lvar))
          lvar=lenstr(vname(3,indxUb))
          ierr=nf_put_att_text (ncid, hisUb, 'units',     lvar,
     &                                  vname(3,indxUb)(1:lvar))
          lvar=lenstr(vname(4,indxUb))
          ierr=nf_put_att_text (ncid, hisUb, 'field',    lvar,
     &                                  vname(4,indxUb)(1:lvar))
        endif
        if (wrthis(indxVb)) then
          lvar=lenstr(vname(1,indxVb))
          ierr=nf_def_var (ncid, vname(1,indxVb)(1:lvar),
     &                              NF_REAL, 3, v2dgrd, hisVb)
          lvar=lenstr(vname(2,indxVb))
          ierr=nf_put_att_text (ncid, hisVb, 'long_name', lvar,
     &                                  vname(2,indxVb)(1:lvar))
          lvar=lenstr(vname(3,indxVb))
          ierr=nf_put_att_text (ncid, hisVb, 'units',     lvar,
     &                                  vname(3,indxVb)(1:lvar))
          lvar=lenstr(vname(4,indxVb))
          ierr=nf_put_att_text (ncid, hisVb, 'field',     lvar,
     &                                  vname(4,indxVb)(1:lvar))
        endif
        if (wrthis(indxU)) then
          lvar=lenstr(vname(1,indxU))
          ierr=nf_def_var (ncid, vname(1,indxU)(1:lvar),
     &                             NF_REAL, 4, u3dgrd, hisU)
          lvar=lenstr(vname(2,indxU))
          ierr=nf_put_att_text (ncid, hisU, 'long_name', lvar,
     &                                  vname(2,indxU)(1:lvar))
          lvar=lenstr(vname(3,indxU))
          ierr=nf_put_att_text (ncid, hisU, 'units',     lvar,
     &                                  vname(3,indxU)(1:lvar))
          lvar=lenstr(vname(4,indxU))
          ierr=nf_put_att_text (ncid, hisU, 'field',     lvar,
     &                                  vname(4,indxU)(1:lvar))
        endif
        if (wrthis(indxV)) then
          lvar=lenstr(vname(1,indxV))
          ierr=nf_def_var (ncid, vname(1,indxV)(1:lvar),
     &                             NF_REAL, 4, v3dgrd, hisV)
          lvar=lenstr(vname(2,indxV))
          ierr=nf_put_att_text (ncid, hisV, 'long_name', lvar,
     &                                  vname(2,indxV)(1:lvar))
          lvar=lenstr(vname(3,indxV))
          ierr=nf_put_att_text (ncid, hisV, 'units',     lvar,
     &                                  vname(3,indxV)(1:lvar))
          lvar=lenstr(vname(4,indxV))
          ierr=nf_put_att_text (ncid, hisV, 'field',     lvar,
     &                                  vname(4,indxV)(1:lvar))
        endif
        do itrc=1,NT
          if (wrthis(indxT+itrc-1)) then
            lvar=lenstr(vname(1,indxT+itrc-1))
            ierr=nf_def_var (ncid, vname(1,indxT+itrc-1)(1:lvar),
     &                             NF_REAL, 4, r3dgrd, hisT(itrc))
            lvar=lenstr(vname(2,indxT+itrc-1))
            ierr=nf_put_att_text (ncid, hisT(itrc), 'long_name',
     &                         lvar, vname(2,indxT+itrc-1)(1:lvar))
            lvar=lenstr(vname(3,indxT+itrc-1))
            ierr=nf_put_att_text (ncid, hisT(itrc), 'units', lvar,
     &                               vname(3,indxT+itrc-1)(1:lvar))
            lvar=lenstr(vname(4,indxT+itrc-1))
            ierr=nf_put_att_text (ncid, hisT(itrc), 'field', lvar,
     &                               vname(4,indxT+itrc-1)(1:lvar))
          endif
        enddo
        if (wrthis(indxR)) then
          lvar=lenstr(vname(1,indxR))
          ierr=nf_def_var (ncid, vname(1,indxR)(1:lvar),
     &                             NF_REAL, 4, r3dgrd, hisR)
          lvar=lenstr(vname(2,indxR))
          ierr=nf_put_att_text (ncid, hisR, 'long_name', lvar,
     &                                  vname(2,indxR)(1:lvar))
          lvar=lenstr(vname(3,indxR))
          ierr=nf_put_att_text (ncid, hisR, 'units',     lvar,
     &                                  vname(3,indxR)(1:lvar))
          lvar=lenstr(vname(4,indxR))
          ierr=nf_put_att_text (ncid, hisR, 'field',     lvar,
     &                                  vname(4,indxR)(1:lvar))
        endif
        if (wrthis(indxO)) then
          lvar=lenstr(vname(1,indxO))
          ierr=nf_def_var (ncid, vname(1,indxO)(1:lvar),
     &                             NF_REAL, 4, w3dgrd, hisO)
          lvar=lenstr(vname(2,indxO))
          ierr=nf_put_att_text (ncid, hisO, 'long_name', lvar,
     &                                  vname(2,indxO)(1:lvar))
          lvar=lenstr(vname(3,indxO))
          ierr=nf_put_att_text (ncid, hisO, 'units',     lvar,
     &                                  vname(3,indxO)(1:lvar))
          lvar=lenstr(vname(4,indxO))
          ierr=nf_put_att_text (ncid, hisO, 'field',     lvar,
     &                                  vname(4,indxO)(1:lvar))
        endif
        if (wrthis(indxW)) then
          lvar=lenstr(vname(1,indxW))
          ierr=nf_def_var (ncid, vname(1,indxW)(1:lvar),
     &                               NF_REAL, 4, r3dgrd, hisW)
          lvar=lenstr(vname(2,indxW))
          ierr=nf_put_att_text (ncid, hisW, 'long_name', lvar,
     &                                  vname(2,indxW)(1:lvar))
          lvar=lenstr(vname(3,indxW))
          ierr=nf_put_att_text (ncid, hisW, 'units',     lvar,
     &                                  vname(3,indxW)(1:lvar))
          lvar=lenstr(vname(4,indxW))
          ierr=nf_put_att_text (ncid, hisW, 'field',     lvar,
     &                                  vname(4,indxW)(1:lvar))
        endif
        if (wrthis(indxAkv)) then
          lvar=lenstr(vname(1,indxAkv))
          ierr=nf_def_var (ncid, vname(1,indxAkv)(1:lvar),
     &                             NF_REAL, 4, w3dgrd, hisAkv)
          lvar=lenstr(vname(2,indxAkv))
          ierr=nf_put_att_text (ncid, hisAkv, 'long_name', lvar,
     &                                  vname(2,indxAkv)(1:lvar))
          lvar=lenstr(vname(3,indxAkv))
          ierr=nf_put_att_text (ncid, hisAkv, 'units',     lvar,
     &                                  vname(3,indxAkv)(1:lvar))
          lvar=lenstr(vname(4,indxAkv))
          ierr=nf_put_att_text (ncid, hisAkv, 'field',     lvar,
     &                                  vname(4,indxAkv)(1:lvar))
        endif
        if (wrthis(indxAkt)) then
          lvar=lenstr(vname(1,indxAkt))
          ierr=nf_def_var (ncid, vname(1,indxAkt)(1:lvar),
     &                              NF_REAL, 4, w3dgrd, hisAkt)
          lvar=lenstr(vname(2,indxAkt))
          ierr=nf_put_att_text (ncid, hisAkt, 'long_name', lvar,
     &                                  vname(2,indxAkt)(1:lvar))
          lvar=lenstr(vname(3,indxAkt))
          ierr=nf_put_att_text (ncid, hisAkt, 'units',     lvar,
     &                                  vname(3,indxAkt)(1:lvar))
          lvar=lenstr(vname(4,indxAkt))
          ierr=nf_put_att_text (ncid, hisAkt, 'field',     lvar,
     &                                  vname(4,indxAkt)(1:lvar))
        endif
        if (wrthis(indxAks)) then
          lvar=lenstr(vname(1,indxAks))
          ierr=nf_def_var (ncid, vname(1,indxAks)(1:lvar),
     &                              NF_REAL, 4, w3dgrd, hisAks)
          lvar=lenstr(vname(2,indxAks))
          ierr=nf_put_att_text (ncid, hisAks, 'long_name', lvar,
     &                                  vname(2,indxAks)(1:lvar))
          lvar=lenstr(vname(3,indxAks))
          ierr=nf_put_att_text (ncid, hisAks, 'units',     lvar,
     &                                  vname(3,indxAks)(1:lvar))
          lvar=lenstr(vname(4,indxAks))
          ierr=nf_put_att_text (ncid, hisAks, 'field',     lvar,
     &                                  vname(4,indxAks)(1:lvar))
        endif
        if (wrthis(indxBostr)) then
          lvar=lenstr(vname(1,indxBostr))
          ierr=nf_def_var (ncid, vname(1,indxBostr)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, hisBostr)
          lvar=lenstr(vname(2,indxBostr))
          ierr=nf_put_att_text (ncid, hisBostr, 'long_name', lvar,
     &                                 vname(2,indxBostr)(1:lvar))
          lvar=lenstr(vname(3,indxBostr))
          ierr=nf_put_att_text (ncid, hisBostr, 'units',     lvar,
     &                                 vname(3,indxBostr)(1:lvar))
        endif
        ierr=nf_enddef(ncid)
        write(stdout,'(6x,4A,1x,A,i4)') 'DEF_HIS/AVG - Created ',
     &                'new netCDF file ''', hisname(1:lstr), '''.'
      elseif (ncid.eq.-1) then
        ierr=nf_open (hisname(1:lstr), nf_write, ncid)
        if (ierr. eq. nf_noerr) then
          ierr=checkdims (ncid, hisname(1:lstr), lstr, rec)
          if (ierr .eq. nf_noerr) then
            if (nrpfhis.eq.0) then
              ierr=rec+1 - total_rec
            else
              ierr=rec+1 - (1+mod(total_rec-1, nrpfhis))
            endif
            if (ierr.gt.0) then
               write( stdout,
     &                 '(/1x,A,I5,1x,A/8x,3A,I5,/8x,A,I5,1x,A/)'
     &            ) 'DEF_HIS/AVG WARNING: Actual number of records',
     &               rec,  'in netCDF file',  '''',  hisname(1:lstr),
     &             ''' exceeds the record number from restart data',
     &             rec+1-ierr,'/', total_rec,', restart is assumed.'
              rec=rec-ierr
            elseif (nrpfhis.eq.0) then
              total_rec=rec+1
            endif
            ierr=nf_noerr
          endif
        endif
        if (ierr. ne. nf_noerr) then
          create_new_file=.true.
          goto 10
        endif
        ierr=nf_inq_varid (ncid, 'time_step', hisTstep)
        if (ierr .ne. nf_noerr) then
          write(stdout,1) 'time_step', hisname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxTime))
        ierr=nf_inq_varid (ncid,vname(1,indxTime)(1:lvar),hisTime)
        if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxTime)(1:lvar), hisname(1:lstr)
          goto 99
        endif
        if (wrthis(indxZ)) then
          lvar=lenstr(vname(1,indxZ))
          ierr=nf_inq_varid (ncid, vname(1,indxZ)(1:lvar), hisZ)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxZ)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxUb)) then
          lvar=lenstr(vname(1,indxUb))
          ierr=nf_inq_varid (ncid, vname(1,indxUb)(1:lvar), hisUb)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxUb)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxVb)) then
          lvar=lenstr(vname(1,indxVb))
          ierr=nf_inq_varid (ncid, vname(1,indxVb)(1:lvar), hisVb)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxVb)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxU)) then
          lvar=lenstr(vname(1,indxU))
          ierr=nf_inq_varid (ncid, vname(1,indxU)(1:lvar), hisU)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxU)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxV)) then
          lvar=lenstr(vname(1,indxV))
          ierr=nf_inq_varid (ncid, vname(1,indxV)(1:lvar), hisV)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxV)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        do itrc=1,NT
          if (wrthis(indxT+itrc-1)) then
            lvar=lenstr(vname(1,indxT+itrc-1))
            ierr=nf_inq_varid (ncid, vname(1,indxT+itrc-1)(1:lvar),
     &                                                 hisT(itrc))
            if (ierr .ne. nf_noerr) then
              write(stdout,1) vname(1,indxT+itrc-1)(1:lvar),
     &                                       hisname(1:lstr)
              goto 99
            endif
          endif
        enddo
        if (wrthis(indxR)) then
          lvar=lenstr(vname(1,indxR))
          ierr=nf_inq_varid (ncid, vname(1,indxR)(1:lvar), hisR)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxR)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxO)) then
          lvar=lenstr(vname(1,indxO))
          ierr=nf_inq_varid (ncid, vname(1,indxO)(1:lvar), hisO)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxO)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxW)) then
          lvar=lenstr(vname(1,indxW))
          ierr=nf_inq_varid (ncid, vname(1,indxW)(1:lvar), hisW)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxW)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxAkv)) then
          lvar=lenstr(vname(1,indxAkv))
          ierr=nf_inq_varid (ncid, vname(1,indxAkv)(1:lvar), hisAkv)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxAkv)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxAkt)) then
          lvar=lenstr(vname(1,indxAkt))
          ierr=nf_inq_varid (ncid,vname(1,indxAkt)(1:lvar), hisAkt)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxAkt)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxAks)) then
          lvar=lenstr(vname(1,indxAks))
          ierr=nf_inq_varid (ncid,vname(1,indxAks)(1:lvar), hisAks)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxAks)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxBostr)) then
          lvar=lenstr(vname(1,indxBostr))
          ierr=nf_inq_varid (ncid,vname(1,indxBostr)(1:lvar),
     &                                                   hisBostr)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxBostr)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        write(*,'(6x,2A,i4,1x,A,i4)') 'DEF_HIS/AVG -- Opened ',
     &                     'existing file  from record =', rec
      endif
      ierr=nf_set_fill (ncid, nf_nofill, lvar)
      if (ierr .ne. nf_noerr) then
        write(*,'(6x,2A,i4,1x,A,i4)') 'DEF_HIS/AVG ERROR: Cannot ',
     &    'switch to ''nf_nofill'' more; netCDF error code =', ierr
      endif
   1  format(/1x,'DEF_HIS/AVG ERROR: Cannot find variable ''',
     &                   A, ''' in netCDF file ''', A, '''.'/)
        if (total_rec.le.1) call wrt_grid (ncid, hisname, lstr)
  99  return
      end
      subroutine def_avg (ncid, total_rec, ierr)
      implicit none
      logical create_new_file
      integer ncid, total_rec, ierr, rec, lstr,lvar,lenstr, timedim
     &      , r2dgrd(3),  u2dgrd(3), v2dgrd(3), auxil(2), checkdims
     &      , b3dgrd(4)
     &      , r3dgrd(4),  u3dgrd(4), v3dgrd(4), w3dgrd(4), itrc
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=384,  MMmH=390, NnH=66)
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLmH,  MMm=MMmH,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      real  x_sponge,   v_sponge
       real  tauT_in, tauT_out, tauM_in, tauM_out
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &                      , x_sponge,   v_sponge
     &                      , tauT_in, tauT_out, tauM_in, tauM_out
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
      integer indxU, indxV, indxT
      parameter (indxU=5, indxV=6, indxT=7)
      integer indxS
      parameter (indxS=indxT+1)
      integer indxO, indxW, indxR, indxVisc, indxAkv, indxAkt
      parameter (indxO=indxT+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed
     &                                  +ntrc_diats+ntrc_diauv+1,
     &           indxW=indxO+1, indxR=indxO+2,
     &           indxVisc=indxR+1,
     &           indxAkv=indxR+2, indxAkt=indxAkv+1)
      integer indxAks
      parameter (indxAks=indxAkt+1)
      integer indxSSH
      parameter (indxSSH=indxAkt+4)
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
      integer indxSHFl, indxSWRad
      parameter (indxSHFl=indxSSH+3)
      integer indxSSFl
      parameter (indxSSFl=indxSHFl+1, indxSWRad=indxSHFl+2)
      integer indxSST, indxdQdSST
      parameter (indxSST=indxSWRad+1, indxdQdSST=indxSWRad+2)
      integer indxSSS
      parameter (indxSSS=indxSST+2)
      integer indxBostr
      parameter (indxBostr=indxSUSTR+16)
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     &                u3dvar, v3dvar, p3dvar, w3dvar, b3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8,b3dvar=12)
      integer xi_rho,xi_u, eta_rho,eta_v
      parameter (xi_rho=LLm+2,  xi_u=xi_rho-1,
     &           eta_rho=MMm+2, eta_v=eta_rho-1)
      integer ncidfrc, ncidbulk, ncidclm,  ntsms
     &      , ntsrf,  ntssh,  ntsst, ntsss, ntuclm, ntww,
     &        ntbulk
      integer nttclm(NT),    ntstf(NT)
      integer ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                  ,rstR  , rstU,    rstV
      integer rstT(NT)
      integer  ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,  hisV,  hisR,    hisHbl, hisHbbl
     &      , hisO,  hisW,  hisVisc,  hisAkv,  hisAkt, hisAks
     &      , hisBostr
      integer hisT(NT)
      integer   ncidavg, nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,  avgV,  avgR,    avgHbl, avgHbbl
     &      , avgO,  avgW,  avgVisc, avgAkv,  avgAkt, avgAks
      integer avgT(NT)
      integer avgBostr
      logical wrthis(60+NT)
     &      , wrtavg(60+NT)
      common/incscrum/
     &        ncidfrc, ncidbulk,ncidclm, ntsms, ntsrf, ntssh, ntsst
     &      , ntuclm, ntsss, ntww, ntbulk
     &                        ,  nttclm,          ntstf
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                  ,rstR  , rstU,    rstV,   rstT
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT,    hisR
     &      , hisO,    hisW,     hisVisc, hisAkv,  hisAkt, hisAks
     &      , hisHbl,  hisHbbl,  hisBostr
     &      , ncidavg,  nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT,    avgR
     &      , avgO,    avgW,     avgVisc,  avgAkv,  avgAkt, avgAks
     &      , avgHbl,  avgHbbl
     &      , avgBostr
     &      , wrthis
     &      , wrtavg
      character*80 date_str, title
      character*80 ininame,  grdname,  hisname
     &         ,   rstname,  frcname,  bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
      character*52  vname(4, 150)
      common /cncscrum/       date_str,   title
     &         ,   ininame,  grdname, hisname
     &         ,   rstname,  frcname, bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
     &                      ,  vname
      integer NF_BYTE
      integer NF_INT1
      integer NF_CHAR
      integer NF_SHORT
      integer NF_INT2
      integer NF_INT
      integer NF_FLOAT
      integer NF_REAL
      integer NF_DOUBLE
      parameter (NF_BYTE = 1)
      parameter (NF_INT1 = NF_BYTE)
      parameter (NF_CHAR = 2)
      parameter (NF_SHORT = 3)
      parameter (NF_INT2 = NF_SHORT)
      parameter (NF_INT = 4)
      parameter (NF_FLOAT = 5)
      parameter (NF_REAL = NF_FLOAT)
      parameter (NF_DOUBLE = 6)
      integer           NF_FILL_BYTE
      integer           NF_FILL_INT1
      integer           NF_FILL_CHAR
      integer           NF_FILL_SHORT
      integer           NF_FILL_INT2
      integer           NF_FILL_INT
      real              NF_FILL_FLOAT
      real              NF_FILL_REAL
      doubleprecision   NF_FILL_DOUBLE
      parameter (NF_FILL_BYTE = -127)
      parameter (NF_FILL_INT1 = NF_FILL_BYTE)
      parameter (NF_FILL_CHAR = 0)
      parameter (NF_FILL_SHORT = -32767)
      parameter (NF_FILL_INT2 = NF_FILL_SHORT)
      parameter (NF_FILL_INT = -2147483647)
      parameter (NF_FILL_FLOAT = 9.9692099683868690e+36)
      parameter (NF_FILL_REAL = NF_FILL_FLOAT)
      parameter (NF_FILL_DOUBLE = 9.9692099683868690e+36)
      integer NF_NOWRITE
      integer NF_WRITE
      integer NF_CLOBBER
      integer NF_NOCLOBBER
      integer NF_FILL
      integer NF_NOFILL
      integer NF_LOCK
      integer NF_SHARE
      parameter (NF_NOWRITE = 0)
      parameter (NF_WRITE = 1)
      parameter (NF_CLOBBER = 0)
      parameter (NF_NOCLOBBER = 4)
      parameter (NF_FILL = 0)
      parameter (NF_NOFILL = 256)
      parameter (NF_LOCK = 1024)
      parameter (NF_SHARE = 2048)
      integer NF_UNLIMITED
      parameter (NF_UNLIMITED = 0)
      integer NF_GLOBAL
      parameter (NF_GLOBAL = 0)
      integer NF_MAX_DIMS
      integer NF_MAX_ATTRS
      integer NF_MAX_VARS
      integer NF_MAX_NAME
      integer NF_MAX_VAR_DIMS
      parameter (NF_MAX_DIMS = 100)
      parameter (NF_MAX_ATTRS = 2000)
      parameter (NF_MAX_VARS = 2000)
      parameter (NF_MAX_NAME = 128)
      parameter (NF_MAX_VAR_DIMS = NF_MAX_DIMS)
      integer NF_NOERR
      integer NF_EBADID
      integer NF_EEXIST
      integer NF_EINVAL
      integer NF_EPERM
      integer NF_ENOTINDEFINE
      integer NF_EINDEFINE
      integer NF_EINVALCOORDS
      integer NF_EMAXDIMS
      integer NF_ENAMEINUSE
      integer NF_ENOTATT
      integer NF_EMAXATTS
      integer NF_EBADTYPE
      integer NF_EBADDIM
      integer NF_EUNLIMPOS
      integer NF_EMAXVARS
      integer NF_ENOTVAR
      integer NF_EGLOBAL
      integer NF_ENOTNC
      integer NF_ESTS
      integer NF_EMAXNAME
      integer NF_EUNLIMIT
      integer NF_ENORECVARS
      integer NF_ECHAR
      integer NF_EEDGE
      integer NF_ESTRIDE
      integer NF_EBADNAME
      integer NF_ERANGE
      parameter (NF_NOERR = 0)
      parameter (NF_EBADID = -33)
      parameter (NF_EEXIST = -35)
      parameter (NF_EINVAL = -36)
      parameter (NF_EPERM = -37)
      parameter (NF_ENOTINDEFINE = -38)
      parameter (NF_EINDEFINE = -39)
      parameter (NF_EINVALCOORDS = -40)
      parameter (NF_EMAXDIMS = -41)
      parameter (NF_ENAMEINUSE = -42)
      parameter (NF_ENOTATT = -43)
      parameter (NF_EMAXATTS = -44)
      parameter (NF_EBADTYPE = -45)
      parameter (NF_EBADDIM = -46)
      parameter (NF_EUNLIMPOS = -47)
      parameter (NF_EMAXVARS = -48)
      parameter (NF_ENOTVAR = -49)
      parameter (NF_EGLOBAL = -50)
      parameter (NF_ENOTNC = -51)
      parameter (NF_ESTS = -52)
      parameter (NF_EMAXNAME = -53)
      parameter (NF_EUNLIMIT = -54)
      parameter (NF_ENORECVARS = -55)
      parameter (NF_ECHAR = -56)
      parameter (NF_EEDGE = -57)
      parameter (NF_ESTRIDE = -58)
      parameter (NF_EBADNAME = -59)
      parameter (NF_ERANGE = -60)
      integer  NF_FATAL
      integer NF_VERBOSE
      parameter (NF_FATAL = 1)
      parameter (NF_VERBOSE = 2)
      character*80   nf_inq_libvers
      external       nf_inq_libvers
      character*80   nf_strerror
      external       nf_strerror
      logical        nf_issyserr
      external       nf_issyserr
      integer         nf_create
      external        nf_create
      integer         nf_open
      external        nf_open
      integer         nf_set_fill
      external        nf_set_fill
      integer         nf_redef
      external        nf_redef
      integer         nf_enddef
      external        nf_enddef
      integer         nf_sync
      external        nf_sync
      integer         nf_abort
      external        nf_abort
      integer         nf_close
      external        nf_close
      integer         nf_delete
      external        nf_delete
      integer         nf_inq
      external        nf_inq
      integer         nf_inq_ndims
      external        nf_inq_ndims
      integer         nf_inq_nvars
      external        nf_inq_nvars
      integer         nf_inq_natts
      external        nf_inq_natts
      integer         nf_inq_unlimdim
      external        nf_inq_unlimdim
      integer         nf_def_dim
      external        nf_def_dim
      integer         nf_inq_dimid
      external        nf_inq_dimid
      integer         nf_inq_dim
      external        nf_inq_dim
      integer         nf_inq_dimname
      external        nf_inq_dimname
      integer         nf_inq_dimlen
      external        nf_inq_dimlen
      integer         nf_rename_dim
      external        nf_rename_dim
      integer         nf_inq_att
      external        nf_inq_att
      integer         nf_inq_attid
      external        nf_inq_attid
      integer         nf_inq_atttype
      external        nf_inq_atttype
      integer         nf_inq_attlen
      external        nf_inq_attlen
      integer         nf_inq_attname
      external        nf_inq_attname
      integer         nf_copy_att
      external        nf_copy_att
      integer         nf_rename_att
      external        nf_rename_att
      integer         nf_del_att
      external        nf_del_att
      integer         nf_put_att_text
      external        nf_put_att_text
      integer         nf_get_att_text
      external        nf_get_att_text
      integer         nf_put_att_int1
      external        nf_put_att_int1
      integer         nf_get_att_int1
      external        nf_get_att_int1
      integer         nf_put_att_int2
      external        nf_put_att_int2
      integer         nf_get_att_int2
      external        nf_get_att_int2
      integer         nf_put_att_int
      external        nf_put_att_int
      integer         nf_get_att_int
      external        nf_get_att_int
      integer         nf_put_att_real
      external        nf_put_att_real
      integer         nf_get_att_real
      external        nf_get_att_real
      integer         nf_put_att_double
      external        nf_put_att_double
      integer         nf_get_att_double
      external        nf_get_att_double
      integer         nf_def_var
      external        nf_def_var
      integer         nf_inq_var
      external        nf_inq_var
      integer         nf_inq_varid
      external        nf_inq_varid
      integer         nf_inq_varname
      external        nf_inq_varname
      integer         nf_inq_vartype
      external        nf_inq_vartype
      integer         nf_inq_varndims
      external        nf_inq_varndims
      integer         nf_inq_vardimid
      external        nf_inq_vardimid
      integer         nf_inq_varnatts
      external        nf_inq_varnatts
      integer         nf_rename_var
      external        nf_rename_var
      integer         nf_copy_var
      external        nf_copy_var
      integer         nf_put_var_text
      external        nf_put_var_text
      integer         nf_get_var_text
      external        nf_get_var_text
      integer         nf_put_var_int1
      external        nf_put_var_int1
      integer         nf_get_var_int1
      external        nf_get_var_int1
      integer         nf_put_var_int2
      external        nf_put_var_int2
      integer         nf_get_var_int2
      external        nf_get_var_int2
      integer         nf_put_var_int
      external        nf_put_var_int
      integer         nf_get_var_int
      external        nf_get_var_int
      integer         nf_put_var_real
      external        nf_put_var_real
      integer         nf_get_var_real
      external        nf_get_var_real
      integer         nf_put_var_double
      external        nf_put_var_double
      integer         nf_get_var_double
      external        nf_get_var_double
      integer         nf_put_var1_text
      external        nf_put_var1_text
      integer         nf_get_var1_text
      external        nf_get_var1_text
      integer         nf_put_var1_int1
      external        nf_put_var1_int1
      integer         nf_get_var1_int1
      external        nf_get_var1_int1
      integer         nf_put_var1_int2
      external        nf_put_var1_int2
      integer         nf_get_var1_int2
      external        nf_get_var1_int2
      integer         nf_put_var1_int
      external        nf_put_var1_int
      integer         nf_get_var1_int
      external        nf_get_var1_int
      integer         nf_put_var1_real
      external        nf_put_var1_real
      integer         nf_get_var1_real
      external        nf_get_var1_real
      integer         nf_put_var1_double
      external        nf_put_var1_double
      integer         nf_get_var1_double
      external        nf_get_var1_double
      integer         nf_put_vara_text
      external        nf_put_vara_text
      integer         nf_get_vara_text
      external        nf_get_vara_text
      integer         nf_put_vara_int1
      external        nf_put_vara_int1
      integer         nf_get_vara_int1
      external        nf_get_vara_int1
      integer         nf_put_vara_int2
      external        nf_put_vara_int2
      integer         nf_get_vara_int2
      external        nf_get_vara_int2
      integer         nf_put_vara_int
      external        nf_put_vara_int
      integer         nf_get_vara_int
      external        nf_get_vara_int
      integer         nf_put_vara_real
      external        nf_put_vara_real
      integer         nf_get_vara_real
      external        nf_get_vara_real
      integer         nf_put_vara_double
      external        nf_put_vara_double
      integer         nf_get_vara_double
      external        nf_get_vara_double
      integer         nf_put_vars_text
      external        nf_put_vars_text
      integer         nf_get_vars_text
      external        nf_get_vars_text
      integer         nf_put_vars_int1
      external        nf_put_vars_int1
      integer         nf_get_vars_int1
      external        nf_get_vars_int1
      integer         nf_put_vars_int2
      external        nf_put_vars_int2
      integer         nf_get_vars_int2
      external        nf_get_vars_int2
      integer         nf_put_vars_int
      external        nf_put_vars_int
      integer         nf_get_vars_int
      external        nf_get_vars_int
      integer         nf_put_vars_real
      external        nf_put_vars_real
      integer         nf_get_vars_real
      external        nf_get_vars_real
      integer         nf_put_vars_double
      external        nf_put_vars_double
      integer         nf_get_vars_double
      external        nf_get_vars_double
      integer         nf_put_varm_text
      external        nf_put_varm_text
      integer         nf_get_varm_text
      external        nf_get_varm_text
      integer         nf_put_varm_int1
      external        nf_put_varm_int1
      integer         nf_get_varm_int1
      external        nf_get_varm_int1
      integer         nf_put_varm_int2
      external        nf_put_varm_int2
      integer         nf_get_varm_int2
      external        nf_get_varm_int2
      integer         nf_put_varm_int
      external        nf_put_varm_int
      integer         nf_get_varm_int
      external        nf_get_varm_int
      integer         nf_put_varm_real
      external        nf_put_varm_real
      integer         nf_get_varm_real
      external        nf_get_varm_real
      integer         nf_put_varm_double
      external        nf_put_varm_double
      integer         nf_get_varm_double
      external        nf_get_varm_double
      integer nccre
      integer ncopn
      integer ncddef
      integer ncdid
      integer ncvdef
      integer ncvid
      integer nctlen
      integer ncsfil
      external nccre
      external ncopn
      external ncddef
      external ncdid
      external ncvdef
      external ncvid
      external nctlen
      external ncsfil
      integer NCRDWR
      integer NCCREAT
      integer NCEXCL
      integer NCINDEF
      integer NCNSYNC
      integer NCHSYNC
      integer NCNDIRTY
      integer NCHDIRTY
      integer NCLINK
      integer NCNOWRIT
      integer NCWRITE
      integer NCCLOB
      integer NCNOCLOB
      integer NCGLOBAL
      integer NCFILL
      integer NCNOFILL
      integer MAXNCOP
      integer MAXNCDIM
      integer MAXNCATT
      integer MAXNCVAR
      integer MAXNCNAM
      integer MAXVDIMS
      integer NCNOERR
      integer NCEBADID
      integer NCENFILE
      integer NCEEXIST
      integer NCEINVAL
      integer NCEPERM
      integer NCENOTIN
      integer NCEINDEF
      integer NCECOORD
      integer NCEMAXDS
      integer NCENAME
      integer NCENOATT
      integer NCEMAXAT
      integer NCEBADTY
      integer NCEBADD
      integer NCESTS
      integer NCEUNLIM
      integer NCEMAXVS
      integer NCENOTVR
      integer NCEGLOB
      integer NCENOTNC
      integer NCFOOBAR
      integer NCSYSERR
      integer NCFATAL
      integer NCVERBOS
      integer NCENTOOL
      integer NCBYTE
      integer NCCHAR
      integer NCSHORT
      integer NCLONG
      integer NCFLOAT
      integer NCDOUBLE
      parameter(NCBYTE = 1)
      parameter(NCCHAR = 2)
      parameter(NCSHORT = 3)
      parameter(NCLONG = 4)
      parameter(NCFLOAT = 5)
      parameter(NCDOUBLE = 6)
      parameter(NCRDWR = 1)
      parameter(NCCREAT = 2)
      parameter(NCEXCL = 4)
      parameter(NCINDEF = 8)
      parameter(NCNSYNC = 16)
      parameter(NCHSYNC = 32)
      parameter(NCNDIRTY = 64)
      parameter(NCHDIRTY = 128)
      parameter(NCFILL = 0)
      parameter(NCNOFILL = 256)
      parameter(NCLINK = 32768)
      parameter(NCNOWRIT = 0)
      parameter(NCWRITE = NCRDWR)
      parameter(NCCLOB = NF_CLOBBER)
      parameter(NCNOCLOB = NF_NOCLOBBER)
      integer NCUNLIM
      parameter(NCUNLIM = 0)
      parameter(NCGLOBAL  = 0)
      parameter(MAXNCOP = 32)
      parameter(MAXNCDIM = 100)
      parameter(MAXNCATT = 2000)
      parameter(MAXNCVAR = 2000)
      parameter(MAXNCNAM = 128)
      parameter(MAXVDIMS = MAXNCDIM)
      parameter(NCNOERR = 0)
      parameter(NCEBADID = 1)
      parameter(NCENFILE = 2)
      parameter(NCEEXIST = 3)
      parameter(NCEINVAL = 4)
      parameter(NCEPERM = 5)
      parameter(NCENOTIN = 6)
      parameter(NCEINDEF = 7)
      parameter(NCECOORD = 8)
      parameter(NCEMAXDS = 9)
      parameter(NCENAME = 10)
      parameter(NCENOATT = 11)
      parameter(NCEMAXAT = 12)
      parameter(NCEBADTY = 13)
      parameter(NCEBADD = 14)
      parameter(NCEUNLIM = 15)
      parameter(NCEMAXVS = 16)
      parameter(NCENOTVR = 17)
      parameter(NCEGLOB = 18)
      parameter(NCENOTNC = 19)
      parameter(NCESTS = 20)
      parameter (NCENTOOL = 21)
      parameter(NCFOOBAR = 32)
      parameter(NCSYSERR = -1)
      parameter(NCFATAL = 1)
      parameter(NCVERBOS = 2)
      integer FILBYTE
      integer FILCHAR
      integer FILSHORT
      integer FILLONG
      real FILFLOAT
      doubleprecision FILDOUB
      parameter (FILBYTE = -127)
      parameter (FILCHAR = 0)
      parameter (FILSHORT = -32767)
      parameter (FILLONG = -2147483647)
      parameter (FILFLOAT = 9.9692099683868690e+36)
      parameter (FILDOUB = 9.9692099683868690e+36)
      character*70 text
      ierr=0
      lstr=lenstr(avgname)
      if (nrpfavg.gt.0) then
        lvar=total_rec-(1+mod(total_rec-1, nrpfavg))
        call insert_time_index (avgname, lstr, lvar, ierr)
        if (ierr .ne. 0) goto 99
      endif
      create_new_file=ldefhis
      if (ncid.ne.-1) create_new_file=.false.
  10  if (create_new_file) then
        ierr=nf_create(avgname(1:lstr), nf_clobber, ncid)
        if (ierr .ne. nf_noerr) then
          write(stdout,'(/3(1x,A)/)') 'ERROR in def_his/avg:',
     &           'Cannot create netCDF file:', avgname(1:lstr)
          goto 99
        endif
        if (nrpfavg.eq.0) total_rec=0
        call put_global_atts (ncid, ierr)
        ierr=nf_def_dim (ncid, 'xi_rho',   xi_rho,   r2dgrd(1))
        ierr=nf_def_dim (ncid, 'xi_u',     xi_u,     u2dgrd(1))
        ierr=nf_def_dim (ncid, 'eta_rho',  eta_rho,  r2dgrd(2))
        ierr=nf_def_dim (ncid, 'eta_v',    eta_v,    v2dgrd(2))
        ierr=nf_def_dim (ncid, 's_rho',    N,        r3dgrd(3))
        ierr=nf_def_dim (ncid, 's_w',      N+1,      w3dgrd(3))
        ierr=nf_def_dim (ncid, 'time', nf_unlimited, timedim)
        ierr=nf_def_dim (ncid, 'auxil',    4,        auxil(1))
        auxil(2)=timedim
        r2dgrd(3)=timedim
        u2dgrd(2)=r2dgrd(2)
        u2dgrd(3)=timedim
        v2dgrd(1)=r2dgrd(1)
        v2dgrd(3)=timedim
        b3dgrd(1)=r2dgrd(1)
        b3dgrd(2)=r2dgrd(2)
        b3dgrd(4)=timedim
        r3dgrd(1)=r2dgrd(1)
        r3dgrd(2)=r2dgrd(2)
        r3dgrd(4)=timedim
        u3dgrd(1)=u2dgrd(1)
        u3dgrd(2)=r2dgrd(2)
        u3dgrd(3)=r3dgrd(3)
        u3dgrd(4)=timedim
        v3dgrd(1)=r2dgrd(1)
        v3dgrd(2)=v2dgrd(2)
        v3dgrd(3)=r3dgrd(3)
        v3dgrd(4)=timedim
        w3dgrd(1)=r2dgrd(1)
        w3dgrd(2)=r2dgrd(2)
        w3dgrd(4)=timedim
        if (total_rec.le.1) call def_grid (ncid, r2dgrd)
        ierr=nf_def_var (ncid, 'time_step', nf_int, 2, auxil,
     &                                                 avgTstep)
        ierr=nf_put_att_text (ncid, avgTstep, 'long_name', 48,
     &       'time step and record numbers from initialization')
        lvar=lenstr(vname(1,indxTime))
        ierr=nf_def_var (ncid, vname(1,indxTime)(1:lvar),
     &                            NF_REAL, 1, timedim, avgTime)
        text='averaged '/ /vname(2,indxTime)
        lvar=lenstr(text)
        ierr=nf_put_att_text (ncid, avgTime, 'long_name', lvar,
     &                                             text(1:lvar))
        lvar=lenstr(vname(3,indxTime))
        ierr=nf_put_att_text (ncid, avgTime, 'units',  lvar,
     &                                vname(3,indxTime)(1:lvar))
        lvar=lenstr(vname(4,indxTime))
        ierr=nf_put_att_text (ncid, avgTime, 'field',  lvar,
     &                                vname(4,indxTime)(1:lvar))
        if (wrtavg(indxZ)) then
          lvar=lenstr(vname(1,indxZ))
          ierr=nf_def_var (ncid, vname(1,indxZ)(1:lvar),
     &                              NF_REAL, 3, r2dgrd, avgZ)
          text='averaged '/ /vname(2,indxZ)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgZ, 'long_name', lvar,
     &                                            text(1:lvar))
          lvar=lenstr(vname(3,indxZ))
          ierr=nf_put_att_text (ncid, avgZ, 'units',     lvar,
     &                                  vname(3,indxZ)(1:lvar))
          lvar=lenstr(vname(4,indxZ))
          ierr=nf_put_att_text (ncid, avgZ, 'field',     lvar,
     &                                  vname(4,indxZ)(1:lvar))
        endif
        if (wrtavg(indxUb)) then
          lvar=lenstr(vname(1,indxUb))
          ierr=nf_def_var (ncid, vname(1,indxUb)(1:lvar),
     &                             NF_REAL, 3, u2dgrd, avgUb)
          text='averaged '/ /vname(2,indxUb)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgUb, 'long_name', lvar,
     &                                             text(1:lvar))
          lvar=lenstr(vname(3,indxUb))
          ierr=nf_put_att_text (ncid, avgUb, 'units',     lvar,
     &                                  vname(3,indxUb)(1:lvar))
          lvar=lenstr(vname(4,indxUb))
          ierr=nf_put_att_text (ncid, avgUb, 'field',    lvar,
     &                                  vname(4,indxUb)(1:lvar))
        endif
        if (wrtavg(indxVb)) then
          lvar=lenstr(vname(1,indxVb))
          ierr=nf_def_var (ncid, vname(1,indxVb)(1:lvar),
     &                              NF_REAL, 3, v2dgrd, avgVb)
          text='averaged '/ /vname(2,indxVb)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgVb, 'long_name', lvar,
     &                                             text(1:lvar))
          lvar=lenstr(vname(3,indxVb))
          ierr=nf_put_att_text (ncid, avgVb, 'units',     lvar,
     &                                  vname(3,indxVb)(1:lvar))
          lvar=lenstr(vname(4,indxVb))
          ierr=nf_put_att_text (ncid, avgVb, 'field',     lvar,
     &                                  vname(4,indxVb)(1:lvar))
        endif
        if (wrtavg(indxU)) then
          lvar=lenstr(vname(1,indxU))
          ierr=nf_def_var (ncid, vname(1,indxU)(1:lvar),
     &                             NF_REAL, 4, u3dgrd, avgU)
          text='averaged '/ /vname(2,indxU)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgU, 'long_name', lvar,
     &                                            text(1:lvar))
          lvar=lenstr(vname(3,indxU))
          ierr=nf_put_att_text (ncid, avgU, 'units',     lvar,
     &                                  vname(3,indxU)(1:lvar))
          lvar=lenstr(vname(4,indxU))
          ierr=nf_put_att_text (ncid, avgU, 'field',     lvar,
     &                                  vname(4,indxU)(1:lvar))
        endif
        if (wrtavg(indxV)) then
          lvar=lenstr(vname(1,indxV))
          ierr=nf_def_var (ncid, vname(1,indxV)(1:lvar),
     &                             NF_REAL, 4, v3dgrd, avgV)
          text='averaged '/ /vname(2,indxV)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgV, 'long_name', lvar,
     &                                            text(1:lvar))
          lvar=lenstr(vname(3,indxV))
          ierr=nf_put_att_text (ncid, avgV, 'units',     lvar,
     &                                  vname(3,indxV)(1:lvar))
          lvar=lenstr(vname(4,indxV))
          ierr=nf_put_att_text (ncid, avgV, 'field',     lvar,
     &                                  vname(4,indxV)(1:lvar))
        endif
        do itrc=1,NT
          if (wrtavg(indxT+itrc-1)) then
            lvar=lenstr(vname(1,indxT+itrc-1))
            ierr=nf_def_var (ncid, vname(1,indxT+itrc-1)(1:lvar),
     &                             NF_REAL, 4, r3dgrd, avgT(itrc))
            text='averaged '/ /vname(2,indxT+itrc-1)
            lvar=lenstr(text)
            ierr=nf_put_att_text (ncid, avgT(itrc), 'long_name',
     &                                          lvar, text(1:lvar))
            lvar=lenstr(vname(3,indxT+itrc-1))
            ierr=nf_put_att_text (ncid, avgT(itrc), 'units', lvar,
     &                               vname(3,indxT+itrc-1)(1:lvar))
            lvar=lenstr(vname(4,indxT+itrc-1))
            ierr=nf_put_att_text (ncid, avgT(itrc), 'field', lvar,
     &                               vname(4,indxT+itrc-1)(1:lvar))
          endif
        enddo
        if (wrtavg(indxR)) then
          lvar=lenstr(vname(1,indxR))
          ierr=nf_def_var (ncid, vname(1,indxR)(1:lvar),
     &                             NF_REAL, 4, r3dgrd, avgR)
          text='averaged '/ /vname(2,indxR)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgR, 'long_name', lvar,
     &                                            text(1:lvar))
          lvar=lenstr(vname(3,indxR))
          ierr=nf_put_att_text (ncid, avgR, 'units',     lvar,
     &                                  vname(3,indxR)(1:lvar))
          lvar=lenstr(vname(4,indxR))
          ierr=nf_put_att_text (ncid, avgR, 'field',     lvar,
     &                                  vname(4,indxR)(1:lvar))
        endif
        if (wrtavg(indxO)) then
          lvar=lenstr(vname(1,indxO))
          ierr=nf_def_var (ncid, vname(1,indxO)(1:lvar),
     &                             NF_REAL, 4, w3dgrd, avgO)
          text='averaged '/ /vname(2,indxO)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgO, 'long_name', lvar,
     &                                            text(1:lvar))
          lvar=lenstr(vname(3,indxO))
          ierr=nf_put_att_text (ncid, avgO, 'units',     lvar,
     &                                  vname(3,indxO)(1:lvar))
          lvar=lenstr(vname(4,indxO))
          ierr=nf_put_att_text (ncid, avgO, 'field',     lvar,
     &                                  vname(4,indxO)(1:lvar))
        endif
        if (wrtavg(indxW)) then
          lvar=lenstr(vname(1,indxW))
          ierr=nf_def_var (ncid, vname(1,indxW)(1:lvar),
     &                               NF_REAL, 4, r3dgrd, avgW)
          text='averaged '/ /vname(2,indxW)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgW, 'long_name', lvar,
     &                                            text(1:lvar))
          lvar=lenstr(vname(3,indxW))
          ierr=nf_put_att_text (ncid, avgW, 'units',     lvar,
     &                                  vname(3,indxW)(1:lvar))
          lvar=lenstr(vname(4,indxW))
          ierr=nf_put_att_text (ncid, avgW, 'field',     lvar,
     &                                  vname(4,indxW)(1:lvar))
        endif
        if (wrtavg(indxAkv)) then
          lvar=lenstr(vname(1,indxAkv))
          ierr=nf_def_var (ncid, vname(1,indxAkv)(1:lvar),
     &                             NF_REAL, 4, w3dgrd, avgAkv)
          text='averaged '/ /vname(2,indxAkv)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgAkv, 'long_name', lvar,
     &                                              text(1:lvar))
          lvar=lenstr(vname(3,indxAkv))
          ierr=nf_put_att_text (ncid, avgAkv, 'units',     lvar,
     &                                  vname(3,indxAkv)(1:lvar))
          lvar=lenstr(vname(4,indxAkv))
          ierr=nf_put_att_text (ncid, avgAkv, 'field',     lvar,
     &                                  vname(4,indxAkv)(1:lvar))
        endif
        if (wrtavg(indxAkt)) then
          lvar=lenstr(vname(1,indxAkt))
          ierr=nf_def_var (ncid, vname(1,indxAkt)(1:lvar),
     &                              NF_REAL, 4, w3dgrd, avgAkt)
          text='averaged '/ /vname(2,indxAkt)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgAkt, 'long_name', lvar,
     &                                              text(1:lvar))
          lvar=lenstr(vname(3,indxAkt))
          ierr=nf_put_att_text (ncid, avgAkt, 'units',     lvar,
     &                                  vname(3,indxAkt)(1:lvar))
          lvar=lenstr(vname(4,indxAkt))
          ierr=nf_put_att_text (ncid, avgAkt, 'field',     lvar,
     &                                  vname(4,indxAkt)(1:lvar))
        endif
        if (wrtavg(indxAks)) then
          lvar=lenstr(vname(1,indxAks))
          ierr=nf_def_var (ncid, vname(1,indxAks)(1:lvar),
     &                              NF_REAL, 4, w3dgrd, avgAks)
          text='averaged '/ /vname(2,indxAks)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgAks, 'long_name', lvar,
     &                                              text(1:lvar))
          lvar=lenstr(vname(3,indxAks))
          ierr=nf_put_att_text (ncid, avgAks, 'units',     lvar,
     &                                  vname(3,indxAks)(1:lvar))
          lvar=lenstr(vname(4,indxAks))
          ierr=nf_put_att_text (ncid, avgAks, 'field',     lvar,
     &                                  vname(4,indxAks)(1:lvar))
        endif
        if (wrtavg(indxBostr)) then
          lvar=lenstr(vname(1,indxBostr))
          ierr=nf_def_var (ncid, vname(1,indxBostr)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, avgBostr)
          text='averaged '/ /vname(2,indxBostr)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgBostr, 'long_name', lvar,
     &                                               text(1:lvar))
          lvar=lenstr(vname(3,indxBostr))
          ierr=nf_put_att_text (ncid, avgBostr, 'units',     lvar,
     &                                 vname(3,indxBostr)(1:lvar))
        endif
        ierr=nf_enddef(ncid)
        write(stdout,'(6x,4A,1x,A,i4)') 'DEF_HIS/AVG - Created ',
     &                'new netCDF file ''', avgname(1:lstr), '''.'
      elseif (ncid.eq.-1) then
        ierr=nf_open (avgname(1:lstr), nf_write, ncid)
        if (ierr. eq. nf_noerr) then
          ierr=checkdims (ncid, avgname(1:lstr), lstr, rec)
          if (ierr .eq. nf_noerr) then
            if (nrpfavg.eq.0) then
              ierr=rec+1 - total_rec
            else
              ierr=rec+1 - (1+mod(total_rec-1, nrpfavg))
            endif
            if (ierr.gt.0) then
               write( stdout,
     &                 '(/1x,A,I5,1x,A/8x,3A,I5,/8x,A,I5,1x,A/)'
     &            ) 'DEF_HIS/AVG WARNING: Actual number of records',
     &               rec,  'in netCDF file',  '''',  avgname(1:lstr),
     &             ''' exceeds the record number from restart data',
     &             rec+1-ierr,'/', total_rec,', restart is assumed.'
              rec=rec-ierr
            elseif (nrpfavg.eq.0) then
              total_rec=rec+1
            endif
            ierr=nf_noerr
          endif
        endif
        if (ierr. ne. nf_noerr) then
          create_new_file=.true.
          goto 10
        endif
        ierr=nf_inq_varid (ncid, 'time_step', avgTstep)
        if (ierr .ne. nf_noerr) then
          write(stdout,1) 'time_step', avgname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxTime))
        ierr=nf_inq_varid (ncid,vname(1,indxTime)(1:lvar),avgTime)
        if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxTime)(1:lvar), avgname(1:lstr)
          goto 99
        endif
        if (wrtavg(indxZ)) then
          lvar=lenstr(vname(1,indxZ))
          ierr=nf_inq_varid (ncid, vname(1,indxZ)(1:lvar), avgZ)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxZ)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxUb)) then
          lvar=lenstr(vname(1,indxUb))
          ierr=nf_inq_varid (ncid, vname(1,indxUb)(1:lvar), avgUb)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxUb)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxVb)) then
          lvar=lenstr(vname(1,indxVb))
          ierr=nf_inq_varid (ncid, vname(1,indxVb)(1:lvar), avgVb)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxVb)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxU)) then
          lvar=lenstr(vname(1,indxU))
          ierr=nf_inq_varid (ncid, vname(1,indxU)(1:lvar), avgU)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxU)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxV)) then
          lvar=lenstr(vname(1,indxV))
          ierr=nf_inq_varid (ncid, vname(1,indxV)(1:lvar), avgV)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxV)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        do itrc=1,NT
          if (wrtavg(indxT+itrc-1)) then
            lvar=lenstr(vname(1,indxT+itrc-1))
            ierr=nf_inq_varid (ncid, vname(1,indxT+itrc-1)(1:lvar),
     &                                                 avgT(itrc))
            if (ierr .ne. nf_noerr) then
              write(stdout,1) vname(1,indxT+itrc-1)(1:lvar),
     &                                       avgname(1:lstr)
              goto 99
            endif
          endif
        enddo
        if (wrtavg(indxR)) then
          lvar=lenstr(vname(1,indxR))
          ierr=nf_inq_varid (ncid, vname(1,indxR)(1:lvar), avgR)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxR)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxO)) then
          lvar=lenstr(vname(1,indxO))
          ierr=nf_inq_varid (ncid, vname(1,indxO)(1:lvar), avgO)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxO)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxW)) then
          lvar=lenstr(vname(1,indxW))
          ierr=nf_inq_varid (ncid, vname(1,indxW)(1:lvar), avgW)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxW)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxAkv)) then
          lvar=lenstr(vname(1,indxAkv))
          ierr=nf_inq_varid (ncid, vname(1,indxAkv)(1:lvar), avgAkv)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxAkv)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxAkt)) then
          lvar=lenstr(vname(1,indxAkt))
          ierr=nf_inq_varid (ncid,vname(1,indxAkt)(1:lvar), avgAkt)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxAkt)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxAks)) then
          lvar=lenstr(vname(1,indxAks))
          ierr=nf_inq_varid (ncid,vname(1,indxAks)(1:lvar), hisAks)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxAks)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxBostr)) then
          lvar=lenstr(vname(1,indxBostr))
          ierr=nf_inq_varid (ncid,vname(1,indxBostr)(1:lvar),
     &                                                   avgBostr)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxBostr)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        write(*,'(6x,2A,i4,1x,A,i4)') 'DEF_HIS/AVG -- Opened ',
     &                     'existing file  from record =', rec
      endif
      ierr=nf_set_fill (ncid, nf_nofill, lvar)
      if (ierr .ne. nf_noerr) then
        write(*,'(6x,2A,i4,1x,A,i4)') 'DEF_HIS/AVG ERROR: Cannot ',
     &    'switch to ''nf_nofill'' more; netCDF error code =', ierr
      endif
   1  format(/1x,'DEF_HIS/AVG ERROR: Cannot find variable ''',
     &                   A, ''' in netCDF file ''', A, '''.'/)
        if (total_rec.le.1) call wrt_grid (ncid, avgname, lstr)
      if (ntsavg.eq.1) then
        time_avg=time-0.5*float(navg)*dt
      else
        time_avg=time-0.5*float(navg)*dt+float(ntsavg)*dt
      endif
  99  return
      end
