      subroutine das_get_insitu
      implicit none
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=384,  MMmH=390, NnH=66)
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLmH,  MMm=MMmH,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+1)*(Mm+1)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_prf
      PARAMETER( max_prf=NDAS)
      integer max_sio, max_whoi, max_flight,
     &        max_hfradar,max_hfradar6,max_js1
      PARAMETER(max_sio=100,max_whoi=100,max_flight=30000,
     &         max_hfradar=30000,max_hfradar6=30000,max_js1=8000)
      integer max_ptsur
      PARAMETER(max_ptsur=100)
      integer max_martn
      PARAMETER(max_martn=100)
      integer max_moor
      PARAMETER(max_moor=280)
      real sio_ot,sio_os,whoi_ot,whoi_os,ptsur_ot,ptsur_os,
     &              martn_ot,martn_os,moor_ot,moor_os, hf_ouv,
     &              hf_ouv6,fl_ot,js1_ossh,swot_ossh,hcmin
      PARAMETER( sio_ot=1.0/(1.185*1.185),sio_os=1.0/(0.16*0.16),
     &          whoi_ot=1.0/(1.17*1.17),whoi_os=1.0/(0.16*0.16),
     &        ptsur_ot=1.0/(1.65*1.65),ptsur_os=1.0/(0.2*0.2),
     &        martn_ot=1.0/(1.65*1.65),martn_os=1.0/(0.2*0.2),
     &        moor_ot=1.0/(1.3*1.3),moor_os=1.0/(0.18*0.18),
     &        js1_ossh=1.0/(0.03*0.03),swot_ossh=1.0/(0.10*0.10),
     &        hcmin=50.0,
     &        fl_ot=1.0/(1.0*1.0),
     &        hf_ouv=1.0/(0.07*0.07),hf_ouv6=1.0/(0.2*0.2)
     &         )
      integer max_prf_cal,max_cal
      PARAMETER(max_prf_cal=NDAS,max_cal=100)
      integer max_prf_dor,max_dor
      PARAMETER(max_prf_dor=NDAS,max_dor=100)
      real cal_ot,cal_os,dor_ot,dor_os
      PARAMETER( cal_ot=1.0/(1.0*1.0),
     &           cal_os=1.0/(0.2*0.2),
     &           dor_ot=1.0/(1.0*1.0),
     &           dor_os=1.0/(0.2*0.2)
     &         )
      real geo_ratio, cross_tsp
      PARAMETER( geo_ratio = 1.0, cross_tsp=0.)
      integer sm_rad
      PARAMETER( sm_rad=5)
      integer Local_len
      PARAMETER( Local_len=20)
      real sz_rad
      PARAMETER( sz_rad=6.0)
      integer sz_rad_len
      PARAMETER( sz_rad_len=20)
      real regp, reguv
      PARAMETER( regp=1.0/(10.0*10.0) )
      PARAMETER( reguv=1.0/(0.6*0.6) )
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      real  x_sponge,   v_sponge
       real  tauT_in, tauT_out, tauM_in, tauM_out
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &                      , x_sponge,   v_sponge
     &                      , tauT_in, tauT_out, tauM_in, tauM_out
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      real h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hinv(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real f(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fomn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_h/h /grid_hinv/hinv /grid_f/f /grid_fomn/fomn
      real angler(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_angler/angler
      real latr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real lonr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_latr/latr /grid_lonr/lonr
      real pm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_pm/pm    /metrics_pn/pn
     &       /metrics_omu/om_u /metrics_on_u/on_u
     &       /metrics_omv/om_v /metrics_on_v/on_v
      real dmde(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real dndx(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_dmde/dmde    /metrics_dndx/dndx
      real on_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmon_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pnom_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real grdscl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_on_p/on_p /metrics_om_p/om_p
     &       /metrics_on_r/on_r /metrics_om_r/om_r
     &       /metrics_pmon_u/pmon_u /metrics_pnom_v/pnom_v
     &                              /metrics_grdscl/grdscl
      real rmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /mask_r/rmask /mask_p/pmask
     &       /mask_u/umask /mask_v/vmask
      real zeta_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_das/u_das /ocean_v_das/v_das
     &    /ocean_psi_das/psi_das /ocean_chi_das/chi_das
     &    /ocean_t_das/t_das /ocean_zeta_das/zeta_das
      real zeta_s(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real zeta_h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      real t_w(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s/u_s /ocean_v_s/v_s
     &      /ocean_psi_s/psi_s /ocean_chi_s/chi_s
     &      /ocean_rho_s/rho_s /ocean_p_s/p_s
     &     /ocean_t_s/t_s /ocean_zeta_s/zeta_s
     &     /ocean_zeta_h/zeta_h /ocean_t_w/t_w
      real zeta_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real psi_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common
     &     /ocean_psi_adj/psi_adj /ocean_chi_adj/chi_adj
     &     /ocean_t_adj/t_adj /ocean_zeta_adj/zeta_adj
      real zeta_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real zeta_h_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      real t_w_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s_adj/u_s_adj /ocean_v_s_adj/v_s_adj
     &   /ocean_psi_s_adj/psi_s_adj /ocean_chi_s_adj/chi_s_adj
     &     /ocean_rho_s_adj/rho_s_adj /ocean_p_s_adj/p_s_adj
     &     /ocean_t_s_adj/t_s_adj /ocean_zeta_s_adj/zeta_s_adj
     &     /ocean_zeta_h_adj/zeta_h_adj /ocean_t_w_adj/t_w_adj
      real rmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real pmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real umask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real vmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /mask_r_das/rmask_das /mask_p_das/pmask_das
     &       /mask_u_das/umask_das /mask_v_das/vmask_das
      integer nzr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nzr_das/nzr_das
     &       /ocean_nzu_das/nzu_das /ocean_nzv_das/nzv_das
      integer nsr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nsr_das/nsr_das
     &       /ocean_nsu_das/nsu_das /ocean_nsv_das/nsv_das
      real georatio(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /cgeoratio/georatio
      real z_das(ndas),tc1d_das(ndas),sc1d_das(ndas)
      common /grid_z_das/z_das,tc1d_das,sc1d_das
      real sst_mc(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real mc_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real mc_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_sst_mc/sst_mc
     &       /innov_mask_mc/mc_mask
     &       /innov_oin_mc/mc_oin
      real sst_goes(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real goes_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real goes_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_sst_goes/sst_goes
     &       /innov_mask_goes/goes_mask
     &       /innov_oin_goes/goes_oin
      real ssh_swot(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real swot_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real swot_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_ssh_swot/ssh_swot
     &       /innov_mask_swot/swot_mask
     &       /innov_oin_swot/swot_oin
      real ssh_js1(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real js1_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real js1_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_ssh_js1/ssh_js1
     &       /innov_mask_js1/js1_mask
     &       /innov_oin_js1/js1_oin
      real lon_js1(max_js1),lat_js1(max_js1),
     &      ssh_js1_raw(max_js1)
      common /innov_ssh_js1_raw/lon_js1,lat_js1,ssh_js1_raw
      real ssh_ref(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real ref_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ssh_ref_yavg/ssh_ref
     &       /ssh_ref_mask/ref_mask
      real sst_fl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fl_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_sst_fl/sst_fl
     &       /innov_mask_fl/fl_mask
      real lon_fl(max_flight),lat_fl(max_flight),
     &      sst_fl_raw(max_flight)
      common /innov_sst_fl_raw/lon_fl,lat_fl,sst_fl_raw
      real u_hf(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real v_hf(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_u_hf/u_hf
     &       /innov_v_hf/v_hf
     &       /innov_umask_hf/hf_umask
     &       /innov_vmask_hf/hf_vmask
      real lon_hf(max_hfradar),lat_hf(max_hfradar),
     &      amperr_hf(max_hfradar),fitdif_hf(max_hfradar),
     &      u_hf_raw(max_hfradar),v_hf_raw(max_hfradar)
      common /innov_uv_hf_raw/lon_hf,lat_hf,
     &       amperr_hf,fitdif_hf,u_hf_raw,v_hf_raw
      real u_hf6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real v_hf6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_umask6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_vmask6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_uierr6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_vierr6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_u_hf6/u_hf6
     &       /innov_v_hf6/v_hf6
     &       /innov_umask_hf6/hf_umask6
     &       /innov_vmask_hf6/hf_vmask6
     &       /innov_uierr_hf6/hf_uierr6
     &       /innov_vierr_hf6/hf_vierr6
      real lon_hf6(max_hfradar6),lat_hf6(max_hfradar6),
     &      amperr_hf6(max_hfradar6),fitdif_hf6(max_hfradar6),
     &      u_hf_raw6(max_hfradar6),v_hf_raw6(max_hfradar6)
      common /innov_uv_hf_raw6/lon_hf6,lat_hf6,
     &       amperr_hf6,fitdif_hf6,u_hf_raw6,v_hf_raw6
      real t_cal(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_cal),
     &     s_cal(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_cal)
      real cal_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_cal)
      common /innov_temp_cal/t_cal
     &       /innov_salt_cal/s_cal
     &       /innov_mask_cal/cal_mask
      real lon_cal(max_cal,max_prf_cal),
     &     lat_cal(max_cal,max_prf_cal),
     &     t_cal_raw(max_cal,max_prf_cal),
     &     s_cal_raw(max_cal,max_prf_cal)
      common /innov_cal_raw/
     &     lon_cal,lat_cal,t_cal_raw,s_cal_raw
      real t_dor(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_dor),
     &     s_dor(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_dor)
      real dor_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_dor)
      common /innov_temp_dor/t_dor
     &       /innov_salt_dor/s_dor
     &       /innov_mask_dor/dor_mask
      real lon_dor(max_dor,max_prf_dor),
     &     lat_dor(max_dor,max_prf_dor),
     &     t_dor_raw(max_dor,max_prf_dor),
     &     s_dor_raw(max_dor,max_prf_dor)
      common /innov_dor_raw/
     &     lon_dor,lat_dor,t_dor_raw,s_dor_raw
      real t_sio(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real s_sio(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real sio_t_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real sio_s_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      common /innov_t_sio/t_sio
     &       /innov_s_sio/s_sio
     &       /innov_mask_tsio/sio_t_mask
     &       /innov_mask_ssio/sio_s_mask
      real   t_sio_raw(max_sio,max_prf),
     &       s_sio_raw(max_sio,max_prf)
      real lon_sio(max_sio), lat_sio(max_sio)
      common /sio_glides_st_raw/t_sio_raw, s_sio_raw,
     &                        lon_sio, lat_sio
      real t_whoi(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real s_whoi(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real whoi_t_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real whoi_s_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      common /innov_t_whoi/t_whoi
     &       /innov_s_whoi/s_whoi
     &       /innov_mask_twhoi/whoi_t_mask
     &       /innov_mask_swhoi/whoi_s_mask
      real   t_whoi_raw(max_whoi,max_prf),
     &       s_whoi_raw(max_whoi,max_prf)
      real lon_whoi(max_whoi), lat_whoi(max_whoi)
      common /whoi_glides_st_raw/t_whoi_raw, s_whoi_raw,
     &                        lon_whoi, lat_whoi
      real s_ptsur(max_ptsur,max_prf),
     &     t_ptsur(max_ptsur,max_prf),
     &     mask_ptsur(max_ptsur,max_prf)
      real lon_ptsur(max_ptsur), lat_ptsur(max_ptsur)
      common /ptsur_cdt_st/s_ptsur,t_ptsur,
     &                      lon_ptsur,lat_ptsur,
     &                      mask_ptsur
      integer Iptsur(max_ptsur),Jptsur(max_ptsur)
      common /ptsur_cdt_loc/Iptsur,Jptsur
      real s_martn(max_martn,max_prf),
     &     t_martn(max_martn,max_prf),
     &     mask_martn(max_martn,max_prf)
      real lon_martn(max_martn), lat_martn(max_martn)
      common /martn_cdt_st/s_martn,t_martn,
     &                      lon_martn,lat_martn,
     &                      mask_martn
      integer Imartn(max_martn),Jmartn(max_martn)
      common /martn_cdt_loc/Imartn,Jmartn
      real t_moor(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real s_moor(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real moor_t_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real moor_s_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      common /innov_t_moor/t_moor
     &       /innov_s_moor/s_moor
     &       /innov_mask_tmoor/moor_t_mask
     &       /innov_mask_smoor/moor_s_mask
      real   t_moor_raw(max_moor,max_prf),
     &       s_moor_raw(max_moor,max_prf)
      real lon_moor(max_moor), lat_moor(max_moor)
      common /moor_ctd_raw/t_moor_raw, s_moor_raw,
     &                        lon_moor, lat_moor
      integer prf_num_sio,prf_num_whoi,prf_num_moor,
     &            prf_num_ptsur,prf_num_martn,num_flight,
     &            num_hfradar,num_hfradar6,num_js1
      common /insitu_num/prf_num_sio,prf_num_whoi,prf_num_moor,
     &            prf_num_ptsur,prf_num_martn,num_flight,
     &            num_hfradar,num_hfradar6,num_js1
      integer num_cal(max_prf_cal),num_dor(max_prf_dor)
      common /insitu_num_cal/num_cal,num_dor
      logical flag_tmi, flag_mc, flag_goes,flag_js1,
     &        flag_swot,flag_tp,flag_sio,flag_whoi,flag_ptsur,
     &        flag_martn,flag_flight,flag_cal,flag_dor,
     &        flag_moor,flag_prof,flag_ship,flag_hfradar,
     &        flag_hfradar6, flag_ssh_ref
      common /innov_flags/flag_tmi, flag_mc, flag_goes,
     &       flag_js1,flag_swot,flag_tp,flag_sio,flag_whoi,flag_ptsur,
     &        flag_martn,flag_flight,flag_cal,flag_dor,
     &        flag_moor,flag_prof,flag_ship,flag_hfradar,
     &        flag_hfradar6, flag_ssh_ref
      character*99 file_tmi,file_mc,file_goes
      common /innov_file_sst/file_tmi,file_mc,file_goes
      character*99 file_js1, file_swot, file_tp
      common /innov_file_ssh/file_js1, file_swot, file_tp
      character*99 file_sio_glider,
     &             file_whoi_glider
      common /innov_file_glider/file_sio_glider, file_whoi_glider
      character*99 file_ptsur_ctd,file_martn_ctd,
     &           file_moor_ctd,file_prof_ctd
      common /innov_file_ctd/file_ptsur_ctd,
     &          file_martn_ctd,file_moor_ctd,file_prof_ctd
      character*99 file_flight_sst,file_cal_auv,
     &            file_dor_auv,file_ship_sst
      common /innov_file_flight/file_flight_sst,
     &              file_cal_auv,file_dor_auv,file_ship_sst
      character*99 file_hfradar_uv,file_hfradar_uv6
      common /innov_file_HF/file_hfradar_uv,file_hfradar_uv6
      character*99 file_dist_coast
      common /dist_coast_file/file_dist_coast
      character*99 file_ssh_ref
      common /ssh_ref_file/file_ssh_ref
      character*99 file_ssh_ref_js1
      common /ssh_ref_file_js1/file_ssh_ref_js1
      integer time_level
      common /das_time_level/time_level
      character*99 file_innov_sio, file_innov_whoi
      common /innov_print_file/file_innov_sio,
     &                         file_innov_whoi
      character*99 file_innov_ana_sio, file_innov_ana_whoi
      common /innov_print_ana_file/file_innov_ana_sio,
     &                         file_innov_ana_whoi
      real anc(Lm+2,Mm+2)
      common /temp_nc/anc
      integer i,j,k,ios,ios1,ios2,lenstr,len_file,it,num,itp
      real hh
      flag_sio=.true.
      flag_whoi=.true.
      flag_ptsur=.true.
      flag_martn=.true.
      flag_moor=.true.
      flag_prof=.true.
      flag_flight=.true.
      flag_ship=.true.
      prf_num_sio=0
      prf_num_whoi=0
      prf_num_ptsur=0
      prf_num_martn=0
      prf_num_moor=0
      num_flight=0
      do i=1,max_sio
        lon_sio(i)=-9999.0
        lat_sio(i)=-9999.0
      enddo
      do j=1,max_prf
        do i=1,max_sio
          t_sio_raw(i,j)=-9999.0
          s_sio_raw(i,j)=-9999.0
        enddo
      enddo
      do i=1,max_whoi
        lon_whoi(i)=-9999.0
        lat_whoi(i)=-9999.0
      enddo
      do j=1,max_prf
        do i=1,max_whoi
          t_whoi_raw(i,j)=-9999.0
          s_whoi_raw(i,j)=-9999.0
        enddo
      enddo
      do i=1,max_moor
        lon_moor(i)=-9999.0
        lat_moor(i)=-9999.0
      enddo
      do j=1,max_prf
        do i=1,max_moor
          t_moor_raw(i,j)=-9999.0
          s_moor_raw(i,j)=-9999.0
        enddo
      enddo
      do i=1,max_ptsur
        lon_ptsur(i)=-9999.0
        lat_ptsur(i)=-9999.0
        Iptsur(i)=-9999
        Jptsur(i)=-9999
      enddo
      do j=1,max_prf
        do i=1,max_ptsur
          mask_ptsur(i,j)=0.0
          t_ptsur(i,j)=-9999.0
          s_ptsur(i,j)=-9999.0
        enddo
      enddo
      do i=1,max_martn
        lon_martn(i)=-9999.0
        lat_martn(i)=-9999.0
        Imartn(i)=-9999
        Jmartn(i)=-9999
      enddo
      do j=1,max_prf
        do i=1,max_martn
          mask_martn(i,j)=0.0
          t_martn(i,j)=-9999.0
          s_martn(i,j)=-9999.0
        enddo
      enddo
      do i=1,max_flight
        lon_fl(i)=-9999.0
        lat_fl(i)=-9999.0
        sst_fl_raw(i)=-9999.0
      enddo
      do i=1,max_flight
        lon_fl(i)=-9999.0
        lat_fl(i)=-9999.0
        sst_fl_raw(i)=-9999.0
      enddo
      len_file=lenstr(file_sio_glider)
      open(81, file=file_sio_glider(1:len_file),form='formatted',
     &         STATUS='old',IOSTAT=ios)
      if(ios.ne.0) then
        flag_sio=.false.
        write(*,'(6x,A,(/8x,A))') '??? WARNING: SIO NOT FOUND ',
     &          file_sio_glider
        goto 10
      endif
      do it=1,max_sio
        do k=1,max_prf
          read(81,900,end=100)
     &     lon_sio(it),lat_sio(it),hh,
     &     t_sio_raw(it,k),s_sio_raw(it,k)
        enddo
      enddo
100   continue
      prf_num_sio=it-1
      write(*,'(6x,A,I5,(/8x,A))')  'OK: SIO GLIDER READ IN',
     &         prf_num_sio, file_sio_glider
10    continue
      close(81)
      len_file=lenstr(file_whoi_glider)
      open(81, file=file_whoi_glider(1:len_file),form='formatted',
     &         STATUS='old',IOSTAT=ios)
      if(ios.ne.0) then
        flag_whoi=.false.
        write(*,'(6x,A,(/8x,A))') '??? WARNING: WHOI NOT FOUND ',
     &          file_whoi_glider
        goto 20
      endif
      do it=1,max_whoi
        do k=1,max_prf
          read(81,900,end=200)
     &     lon_whoi(it),lat_whoi(it),hh,
     &     t_whoi_raw(it,k),s_whoi_raw(it,k)
        enddo
      enddo
200   continue
      prf_num_whoi=it-1
      write(*,'(6x,A,(/8x,A))')  'OK: WHOI GLIDER READ IN',
     &         file_whoi_glider
20    continue
      close(81)
      len_file=lenstr(file_moor_ctd)
      open(81, file=file_moor_ctd(1:len_file),form='formatted',
     &         STATUS='old',IOSTAT=ios)
      if(ios.ne.0) then
        flag_moor=.false.
        write(*,'(6x,A,(/8x,A))') '??? WARNING: MOOR NOT FOUND ',
     &         file_moor_ctd
        goto 150
      endif
      do it=1,max_moor
        do k=1,max_prf
          read(81,900,end=1500)
     &     lon_moor(it),lat_moor(it),hh,
     &     t_moor_raw(it,k),s_moor_raw(it,k)
        enddo
      enddo
1500  continue
      prf_num_moor=it-1
      write(*,'(6x,A,I3,(/8x,A))')
     &         'OK: MOORING CDT READ IN',
     &          prf_num_moor,file_moor_ctd
150    continue
      close(81)
      len_file=lenstr(file_prof_ctd)
      open(81, file=file_prof_ctd(1:len_file),form='formatted',
     &         STATUS='old',IOSTAT=ios)
      if(ios.ne.0) then
        flag_prof=.false.
        write(*,'(6x,A,(/8x,A))') '??? WARNING: PROF NOT FOUND ',
     &         file_prof_ctd
        goto 160
      endif
      do it=1,max_moor-prf_num_moor
          itp=prf_num_moor+it
        do k=1,max_prf
          read(81,900,end=1600)
     &     lon_moor(itp),lat_moor(itp),hh,t_moor_raw(itp,k),
     &     s_moor_raw(itp,k)
        enddo
      enddo
1600  continue
      prf_num_moor=itp-1
      write(*,'(6x,A,(/8x,A))')
     &         'OK: PROFILE CDT READ IN',file_prof_ctd
160    continue
      close(81)
      len_file=lenstr(file_ptsur_ctd)
      open(81, file=file_ptsur_ctd(1:len_file),form='formatted',
     &         STATUS='old',IOSTAT=ios)
      if(ios.ne.0) then
        flag_ptsur=.false.
        write(*,'(6x,A,(/8x,A))') '??? WARNING: PTSUR NOT FOUND ',
     &         file_ptsur_ctd
        goto 40
      endif
      do it=1,max_ptsur
        do k=1,max_prf
          read(81,900,end=400)
     &     lon_ptsur(it),lat_ptsur(it),hh,t_ptsur(it,k),
     &     s_ptsur(it,k)
        enddo
      enddo
400   continue
      prf_num_ptsur=it-1
      write(*,'(6x,A,(/8x,A))')
     &         'OK: PTSUR CDT READ IN',file_ptsur_ctd
40    continue
      close(81)
      len_file=lenstr(file_martn_ctd)
      open(81, file=file_martn_ctd(1:len_file),form='formatted',
     &         STATUS='old',IOSTAT=ios)
      if(ios.ne.0) then
        flag_martn=.false.
        write(*,'(6x,A,(/8x,A))') '??? WARNING: NOT FOUND ',
     &         file_martn_ctd
        goto 50
      endif
      do it=1,max_martn
        do k=1,max_prf
          read(81,900,end=500)
     &     lon_martn(it),lat_martn(it),hh,t_martn(it,k),
     &     s_martn(it,k)
        enddo
      enddo
500   continue
      prf_num_martn=it-1
      write(*,'(6x,A,(/8x,A))')
     &         'OK: MARTIN CDT READ IN',file_martn_ctd
50    continue
      close(81)
      len_file=lenstr(file_ship_sst)
      open(81, file=file_ship_sst(1:len_file),form='formatted',
     &         STATUS='old',IOSTAT=ios)
      if(ios.ne.0) then
        flag_ship=.false.
        write(*,'(6x,A,(/8x,A))') '??? WARNING: SHIP NOT FOUND ',
     &          file_ship_sst
        goto 130
      endif
      do it=1,max_flight
        read(81,800,end=1300)
     &   lon_fl(it),lat_fl(it),sst_fl_raw(it)
      enddo
1300  continue
      num_flight=it-1
      write(*,'(6x,A,(/8x,A))')
     &         'OK: SHIP SST READ IN',file_ship_sst
130   continue
      close(81)
      do it=1,num_flight
        if ( lon_fl(it) .lt. 0.0)
     &       lon_fl(it)=360.0+lon_fl(it)
      enddo
      len_file=lenstr(file_flight_sst)
      open(81, file=file_flight_sst(1:len_file),form='formatted',
     &         STATUS='old',IOSTAT=ios)
      if(ios.ne.0) then
        flag_flight=.false.
        write(*,'(6x,A,(/8x,A))') '??? WARNING: FLIGHT NOT FOUND ',
     &          file_flight_sst
        goto 30
      endif
      do it=1,max_flight-num_flight
        itp=num_flight+it
        read(81,800,end=300)
     &   lon_fl(itp),lat_fl(itp),sst_fl_raw(itp)
      enddo
300   continue
      num_flight=itp-1
      write(*,'(6x,A,I5, (/8x,A))')
     &         'OK: FLIGHT SST READ IN',
     &         num_flight, file_flight_sst
30    continue
      close(81)
800   format(3f10.4)
900   format(5f11.4)
      return
      end
