      implicit none
#include "cppdefs.h"
#include "param.h"
#include "das_param.h"
#include "scalars.h"
#include "ncscrum.h"
#include "ocean3d.h"
#include "ocean2d.h"
#include "das_ocean.h"
#include "das_ocean9.h"
#include "das_covar.h"
#include "das_lbgfs.h"
#include "das_innov.h"
      real time_start
      integer next_kstp, tile, subs, trd, ierr, ios, nsyn
      integer i,j,k
!
! adjoint test
      real rright,rleft
!
! minimization
!
      REAL EPS,GTOL
      INTEGER LP,IPRINT(2),IFLAG ,POINT,ITER,MP,M,N,ICALL
      INTEGER MAXITER
      LOGICAL DIAGCO
      EXTERNAL VA15CD
      COMMON /VA15DD/MP,LP, GTOL
!
! ... observation files
!
#ifdef MPI
      include 'mpif.h'
      real*8 start_time2, start_time1, exe_time
      call MPI_Init (ierr)
      start_time1=MPI_Wtime()
      call MPI_Setup (ierr)
      if (ierr.ne.0) goto 100                            !--> ERROR
c**   call MPI_Test
c**   goto 100
#endif

#define CR
#define SINGLE NSUB_X*NSUB_E,NSUB_X*NSUB_E !!!!

      call read_inp (ierr)           ! Read in tunable model
      if (ierr.ne.0) goto 100        ! parameters.
      call init_scalars (ierr)       ! Also initialize global
      if (ierr.ne.0) goto 100        ! scalar variables.
      call das_init_scalars          ! 

CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! Create parallel threads;
        call start_timers()          ! start timers for each thread;
        call init_arrays (tile)      ! initialize (FIRST-TOUCH) model 
        call das_init_arrays (tile)  ! initialize (FIRST-TOUCH) DAS
      enddo                          ! global arrays (most of them 
CR      write(*,*) '-12' MYID        ! are just set to to zero).
!
      call get_grid                  ! from GRID NetCDF file). 
      if (may_day_flag.ne.0) goto 99     !-->  EXIT

CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! Compute various metric
        call setup_grid1 (tile)      ! term combinations.
      enddo
CR      write(*,*) '-11' MYID
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call setup_grid2 (tile)
      enddo
CR      write(*,*) '-10' MYID
                                     ! Set up vertical S-coordinate
#ifdef SOLVE3D
      call set_scoord                ! and fast-time averaging  
#endif                               ! split-explicit baroropic mode.
CR      write(*,*) ' -9' MYID 

#if defined VIS_GRID || defined DIF_GRID
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! Rescale horizontal mixing
        call visc_rescale (tile)     ! coefficients according to
      enddo                          ! local grid size.
CR      write(*,*) ' -8' MYID 
#endif
#ifdef SOLVE3D
CSDOACROSS LOCAL(tile)               ! Create three-dimensional
      do tile=0,NSUB_X*NSUB_E-1      ! S-coordinate system, which
        call set_depth (tile)        ! may be neded by ana_ninitial
      enddo
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call grid_stiffness (tile) 
      enddo                          ! (here it is assumed that free
CR      write(*,*) ' -7' MYID        ! surface zeta=0).
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_setup (tile)
      enddo
#endif

#ifdef ANA_INITIAL
      if (nrrec.eq.0) then
CSDOACROSS LOCAL(tile)               ! Set initial conditions
        do tile=0,NSUB_X*NSUB_E-1    ! for primitive variables
          call ana_initial (tile)    ! (analytically or read
        enddo                        ! from initial conditions
      else                           ! NetCDF file).
#endif
        call get_initial
        iic=0        ! required by set_depth
#ifdef ANA_INITIAL
      endif
#endif

CR      write(*,*) ' -6' MYID 
      if (may_day_flag.ne.0) goto 99     !-->  EXIT
CSDOACROSS LOCAL(tile)               ! Create three-dimensional
      do tile=0,NSUB_X*NSUB_E-1      ! S-coordinate system: at this
        call set_depth (tile)        ! time free surface is set to
      enddo                          ! a non-zero field, either 
!
CSDOACROSS LOCAL(tile)               ! Create three-dimensional
      do tile=0,NSUB_X*NSUB_E-1      ! Interpolation from S-coordinate system
        call das_s2z_3d (tile)          ! to z-coordinates
      enddo

!CSDOACROSS LOCAL(tile)               ! Create three-dimensional
!      do tile=0,NSUB_X*NSUB_E-1      ! Interpolation from S-coordinate system
!        call das_z2s (tile)          ! to z-coordinates
!      enddo      

!
! prepare some basic variables
!
CSDOACROSS LOCAL(tile)             
      do tile=0,NSUB_X*NSUB_E-1   
        call das_copy9 (tile)   
        call das_rho_eos9 (tile)   
      enddo
!
!
! ... get the file names of observations and error covariances
!
      call das_read_inp
!
! ... get the forecast error covariance
!
# ifdef DAS_ANA_BVAR
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_ana_bvar(tile)
      enddo
# else
      call das_get_bvar
# endif
# ifdef DAS_BVAR_CORR
      call das_get_corr
# endif
!
!
! ... read in all the observational data
!
                       ! SST from TMI
# ifdef DAS_TMISST
      call das_get_tmi
# endif
                       ! SST from AVHRR
# ifdef DAS_MCSST
      call das_get_mc
# endif
                       ! SSH from JASON-1
# ifdef DAS_JASONSSH
      call das_get_js1
# endif
                       ! SSH from TOPEX/POSAIDON
# ifdef DAS_TPSSH
      call das_get_tp
# endif
!
# ifdef DAS_JASONSSH
      print*, 'flag_js1=', flag_js1
      do j=0,MMm+1
        do i=0,LLm+1
          if (js1_mask(i,j) .ge. 1.0 ) then
            print*, 'ssh_js1= ', i, j, ssh_js1(i,j)
          endif
        enddo
      enddo
# endif
                                          ! when no any observation
                                          ! data assimilation not executed
      if(.not. flag_tmi .and. .not. flag_mc
     &   .and. .not. flag_js1 .and. .not. flag_tp ) then
        write(*,'(6x, A, 2(/6x, A))') 
     &              '!!! NO ANY OBSERVATION AVAILABLE',
     &              '!!! DATA ASSIMILATION NOT EXECUTED',
     &              '!!! NO ANALYSIS FILE'
        goto 99
      endif
!
! ... compute the innovation vectors
! ... required by the incremental formulation
!
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
# if defined DAS_JASONSSH || defined DAS_TPSSH
        call das_innov_ssh(tile)
# endif
#if defined DAS_MCSST || defined DAS_TMISST
        call das_innov_sst(tile)
# endif
      enddo

!      write(*,*) 'innov tmi'
!      write(*,*) (sst_tmi(10,j), j=0,Mm+1)
!      write(*,*) 'innov mc'
!      write(*,*) (sst_mc(10,j), j=0,Mm+1)

!
! START MINIMIZATION
!
      ICALL=0
      IFLAG=0
      MAXITER=15
      IPRINT(1)= 1
      IPRINT(2)= 0
      DIAGCO= .FALSE.
      EPS= 1.0D-5     ! minimization criterion
!      EPS= 1.0D-6     ! minimization criterion
                      ! |grad|/max(1, |x|) < EPS, terminated
      LP = 6
      MP = 6

!
! ... initialize the first guess of increment to be zero
!
!CSDOACROSS LOCAL(tile)
!      do tile=0,NSUB_X*NSUB_E-1
!        CALL DAS_CA2ZERO (tile)
!      enddo
!      CALL DAS_CA2VEC    ! control variable vector
!
! ... minimization interation starts
!
  10  CONTINUE
!
! ... initialize the cost function
!
      COSTF=0.0
!
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_MASK(tile)
      enddo
!
! ... compute the matrix-vector product 
!
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_MATVEC(tile)  
      enddo

#  if defined TEST_MATVEC_VARVEC
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_VARVEC (tile)      ! t_s
      enddo

      rright=0.0
      rleft=0.0
      do k=1,ndas
        do j=0,Mm+1
          do i=0,Lm+1
           rright=rright
     &            +t_s(i,j,k,itemp)*t_s(i,j,k,itemp)
     &            +t_s(i,j,k,isalt)*t_s(i,j,k,isalt)
          enddo
        enddo
      enddo
      do k=1,ndas
        do j=0,Mm+1
          do i=0,Lm+1
            t_s_adj(i,j,k,itemp)=t_s(i,j,k,itemp)
            t_s_adj(i,j,k,isalt)=t_s(i,j,k,isalt)
          enddo
        enddo
      enddo
      do k=1,ndas
        do j=0,Mm+1
          do i=0,Lm+1
            t_adj(i,j,k,itemp)=0.0
            t_adj(i,j,k,isalt)=0.0
          enddo
        enddo
      enddo

CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_VARVEC (tile)
      enddo
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_MATVEC(tile)
      enddo
      do k=1,ndas
        do j=0,Mm+1
          do i=0,Lm+1
           rleft=rleft
     &            +t_adj(i,j,k,itemp)*t_das(i,j,k,itemp)
     &            +t_adj(i,j,k,isalt)*t_das(i,j,k,isalt)
          enddo
        enddo
      enddo
      print*,  rleft, rright
#  endif  /* TEST_MATVEC_VARVEC */

!
! ... terms of the SSH
!
#if defined DAS_JASONSSH || defined DAS_TPSSH\
  || defined DAS_TPSSH || defined DAS_ERS2SSH\
  || defined DAS_GFOSSH
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_VARVEC (tile)      ! t_s
      enddo

CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_LIN_RHO_EOS (tile) ! rho_s
      enddo

      rright=0.0
      rleft=0.0
      do k=1,ndas
        do j=0,Mm+1
          do i=0,Lm+1
           rright=rright
     &            +rho_s(i,j,k)*rho_s(i,j,k)
          enddo
        enddo
      enddo

      do k=1,ndas
        do j=0,Mm+1
          do i=0,Lm+1
            rho_s_adj(i,j,k)=rho_s(i,j,k)
          enddo
        enddo
      enddo
      do k=1,ndas
        do j=0,Mm+1
          do i=0,Lm+1
            t_s_adj(i,j,k,itemp)=0.0
            t_s_adj(i,j,k,isalt)=0.0
          enddo
        enddo
      enddo

CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_RHO_EOS (tile)
      enddo

      do k=1,ndas
        do j=0,Mm+1
          do i=0,Lm+1
           rleft=rleft
     &            +t_s_adj(i,j,k,itemp)*t_s(i,j,k,itemp)
     &            +t_s_adj(i,j,k,isalt)*t_s(i,j,k,isalt)
          enddo
        enddo
      enddo

      print*,  rleft, rright

      STOP 9999

CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_DIAG_SSH (tile)    ! zeta_s
        CALL DAS_COST_SSH(tile)   ! cost function SSH
      enddo
#endif

!
! ... term of the SST
!
#if defined DAS_MCSST || defined DAS_TMISST
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_SST(tile)   ! cost function SST
      enddo
#endif
!
! ... term of the background
!
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COSTB(tile)   ! cost function background
      enddo
      print*, costf

!
! ... compute the gradient with respect to u, v, t, s
!
! ... clean the adjoint arrays
!
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_ZERO(tile)
      enddo
!
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_COSTB(tile)
      enddo
#if defined DAS_MCSST || defined DAS_TMISST
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_CSST(tile)
      enddo
#endif
!
#if defined DAS_JASONSSH || defined DAS_TPSSH\
  || defined DAS_TPSSH || defined DAS_ERS2SSH\
  || defined DAS_GFOSSH
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_CSSH(tile)   ! cost function SSH
        CALL DAS_ADJ_DIAG_SSH (tile)
        CALL DAS_ADJ_RHO_EOS (tile)
        CALL DAS_ADJ_VARVEC (tile)
      enddo
#endif
!
! ... mask the adjoint variables
!
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_MASK(tile)
      enddo
!
! ... compute the matrix-vector product
!
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_MATVEC(tile)
      enddo
!
! ... clean the intermediate adjoint arrays
!
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_ZERO_S(tile)
      enddo
!
      CALL DAS_GA2VEC    ! gradient vector
!
      CALL mbfgs(NDIM,MSAVE,COSTF,DIAGCO,IPRINT,EPS,POINT,IFLAG)

      CALL DAS_VEC2CA        !vector to control variables

      IF(IFLAG.LE.0) GO TO 50
      ICALL=ICALL + 1
      IF(ICALL.GT.MAXITER) GO TO 50
  
      GOTO 10
 
  50  CONTINUE

        
      write(*,*) 't j k=NDAS'
      write(*,*) (t_das(10,j,NDAS,itemp), j=0,Mm+1)
      write(*,*) 't j k=15'
      write(*,*) (t_das(10,j,15,itemp), j=0,Mm+1)
!      write(*,*) 't j k=1'
!      write(*,*) (t_das(10,j,1,itemp), j=0,Mm+1)
!      write(*,*) 's2'
!      write(*,*) (t_das(50,j,NDAS,isalt), j=0,Mm+1)
!      write(*,*) 'u2'
!      write(*,*) (u_das(50,j,NDAS), j=1,Mm)
!      write(*,*) 'v2'
!      write(*,*) (v_das(50,j,NDAS), j=1,Mm)
!      write(*,*) 'zeta2'
!      write(*,*) (zeta_das(50,j), j=1,Lm)

      write(*,*) 't j=10 k=NDAS'
      write(*,*) (t_das(i,10,NDAS,itemp), i=1,Lm)
      write(*,*) 't j=10 k=NDAS-1'
      write(*,*) (t_das(i,10,NDAS-1,itemp), i=1,Lm)
      write(*,*) 't j=10 k=10'
      write(*,*) (t_das(i,10,10,itemp), i=1,Lm)
      write(*,*) 't j=10 k=5'
      write(*,*) (t_das(i,10,5,itemp), i=1,Lm)
      write(*,*) 't j=10 k=1'
      write(*,*) (t_das(i,10,1,itemp), i=1,Lm)

!
      print*, 'IFLAG=', IFLAG
      if (IFLAG .lt. 0) then
        write(*, '(6x, A, (/6x, A))')
     &   '!!! Minimization is not completed',
     &   '!!! There is no assimilated analysis generated'
        goto 99
      endif
!
! ... compute incremental analysis
!
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1   !temp and salt
        call das_matvec (tile)
      enddo
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_lin_rho_eos (tile)  ! rho
        call das_diag_ssh (tile)     ! zeta
      enddo
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_geos_uv (tile)      ! u and v
        call das_analysis (tile)
      enddo
CSDOACROSS LOCAL(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call rho_eos (tile)
      enddo
!
! ... create new restart file
! ... with the analysis from 3DVAR
!
      iic=ntstart
      call wrt_rst   
!...
!
      write(*,*) 'iic==', iic

      write(*,*) 'zeta_s j '
      write(*,*) (zeta_s(10,j), j=0,Mm+1)

      write(*,*) 'ts j k=NDAS'
      write(*,*) (t_s(10,j,NDAS,itemp), j=0,Mm+1)
      write(*,*) 'ts j k=15'
      write(*,*) (t_s(10,j,15,itemp), j=0,Mm+1)
      write(*,*) 'ts j k=1'
      write(*,*) (t_s(10,j,1,itemp), j=0,Mm+1)
      write(*,*) 's2'
      write(*,*) (t_s(50,j,NDAS,isalt), j=0,Mm+1)

      write(*,*) 'ts j=50 k=NDAS'
      write(*,*) (t_s(i,50,NDAS,itemp), i=1,Lm)
      write(*,*) 'ts j=50 k=NDAS-1'
      write(*,*) (t_s(i,50,NDAS-1,itemp), i=1,Lm)
      write(*,*) 'ts j=50 k=10'
      write(*,*) (t_s(i,50,10,itemp), i=1,Lm)
      write(*,*) 'ts j=50 k=5'
      write(*,*) (t_s(i,50,5,itemp), i=1,Lm)
      write(*,*) 'ts j=50 k=1'
      write(*,*) (t_s(i,50,1,itemp), i=1,Lm)

      write(*,*) 'rho_s j k=NDAS'
      write(*,*) (rho_s(10,j,NDAS), j=0,Mm+1)
      write(*,*) 'rho_s j k=15'
      write(*,*) (rho_s(10,j,15), j=0,Mm+1)

      write(*,*) 'rho 10 '
      write(*,*) (rho(10,10,k), k=1,n)
      write(*,*) 'rho 20 '
      write(*,*) (rho(10,20,k), k=1,n)
      write(*,*) 'rho 30 '
      write(*,*) (rho(10,30,k), k=1,n)
      write(*,*) 'rho 50 '
      write(*,*) (rho(10,50,k), k=1,n)
      write(*,*) 'rho 80 '
      write(*,*) (rho(10,80,k), k=1,n)
      write(*,*) 'rho 120 '
      write(*,*) (rho(10,120,k), k=1,n)
                                     ! JOB DONE  
  99  continue                       ! SHUTDOWN:
CSDOACROSS LOCAL(tile)               ! Stop timers and 
      do tile=0,NSUB_X*NSUB_E-1      ! close netCDF files.
        call stop_timers()
      enddo
      call closecdf

 100  continue
#ifdef MPI
      call MPI_Barrier(MPI_COMM_WORLD, ierr)
      start_time2=MPI_Wtime()
      exe_time = start_time2 - start_time1
      if(mynode.eq.0) print*,'exe_time =',exe_time
      call MPI_Finalize (ierr)
#endif
      stop
      end

