#include "cppdefs.h"
      subroutine das_get_mc

      implicit none

# include "param.h"
# include "das_param.h"
# include "grid.h"
# include "das_innov.h"

      integer status, nc_id, v_id, i, j
     
      include 'netcdf.inc'
!
! READ mc sst
!
      flag_mc=.true.

      status = NF_OPEN(file_mc,0,nc_id)
      if (status.ne.NF_NOERR) then
        write(*,'(6x,A,(/8x,A))') '??? WARNING: NOT FOUND OR OPENED',
     &          file_mc
        flag_mc=.false.
        goto 999
      endif
!
! sst
!
      status = NF_INQ_VARID(nc_id, 'sst',v_id)
      if (status.ne.NF_NOERR) then
        print*, '??? WARNING: error in inquiring mc sst'
        flag_mc=.false.
        goto 999
      endif
      status = NF_GET_VAR_DOUBLE(nc_id, v_id, anc)
      if (status.ne.NF_NOERR) then
        print*, '??? WARNING: error in reading mc sst'
        flag_mc=.false.
        goto 999
      endif
      do j=0,Mm+1
        do i=0,Lm+1
          sst_mc(i,j)=anc(i+1,j+1)
        enddo
      enddo
!
! mask
!
      do j=0,Mm+1
        do i=0,Lm+1
          if (sst_mc(i,j) .gt. 0.5 ) then
            mc_mask(i,j)=1.0
          else
            mc_mask(i,j)=0.0
          endif
        enddo
      enddo

      status = NF_CLOSE(nc_id)

999   continue

      if (flag_mc) then
        write(*, '(6x, A,(/8x,A))') 'OK: MC SST READ IN',
     &         file_mc
      endif
    
      return
      end

