      subroutine das_get_swot
      implicit none
      integer  LLmH,  MMmH, NnH
      parameter (LLmH=384,  MMmH=390, NnH=66)
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLmH+2-nhalf)/nratio-1,
     &           MMm_lr=(MMmH+2-nhalf)/nratio-1)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLm_lr,  MMm=MMm_lr,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+1)*(Mm+1)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_prf
      PARAMETER( max_prf=NDAS)
      integer max_sio, max_whoi, max_flight,
     &        max_hfradar,max_hfradar6,max_js1
      PARAMETER(max_sio=100,max_whoi=100,max_flight=30000,
     &         max_hfradar=39000,max_hfradar6=40000,max_js1=8000)
      integer max_ptsur
      PARAMETER(max_ptsur=100)
      integer max_martn
      PARAMETER(max_martn=100)
      integer max_moor
      PARAMETER(max_moor=3520)
      real sio_ot_raw,sio_os_raw,whoi_ot_raw,whoi_os_raw,ptsur_ot,
     &                                                         ptsur_os,
     &                   martn_ot,martn_os,moor_ot_raw,moor_os_raw,
     &                   hf_ouv,hf_ouv6,js1_ossh,swot_ossh,hcmin,
     &                                                         sats_osss
      PARAMETER( sio_ot_raw=1.0/(1.5*1.5),sio_os_raw=1.0/(0.21*0.21),
     &        whoi_ot_raw=1.0/(1.2*1.2),whoi_os_raw=1.0/(0.21*0.21),
     &        moor_ot_raw=1.0/(0.6*0.6),moor_os_raw=1.0/(0.05*0.05),
     &        ptsur_ot=1.0/(1.00*1.00),ptsur_os=1.0/(0.15*0.15),
     &        martn_ot=1.0/(1.00*1.00),martn_os=1.0/(0.15*0.15),
     &        js1_ossh=1.0/(0.03*0.03), swot_ossh=1.0/(0.06*0.06),
     &        hcmin=200.0, sats_osss=1.0/(0.13*0.13),
     &        hf_ouv=1.0/(0.09*0.09),hf_ouv6=1.0/(0.10*0.10)
     &         )
      integer max_prf_cal,max_cal
      PARAMETER(max_prf_cal=NDAS,max_cal=100)
      integer max_prf_dor,max_dor
      PARAMETER(max_prf_dor=NDAS,max_dor=180)
      real cal_ot,cal_os,dor_ot,dor_os
      PARAMETER( cal_ot=1.0/(1.5*1.5),
     &           cal_os=1.0/(0.25*0.25),
     &           dor_ot=1.0/(1.5*1.5),
     &           dor_os=1.0/(0.25*0.25)
     &         )
      real geo_ratio, cross_tsp
      PARAMETER( geo_ratio = 1.0, cross_tsp=0.)
      integer sm_rad
      PARAMETER( sm_rad=4)
      integer Local_len
      PARAMETER( Local_len=30)
      real sz_rad
      PARAMETER( sz_rad=4)
      integer sz_rad_len
      PARAMETER( sz_rad_len=15)
      real regp, reguv
      PARAMETER( regp=0.4/(10.*10.) )
      PARAMETER( reguv=0.5/(0.6*0.6)  )
      real h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hinv(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real f(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fomn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_h/h /grid_hinv/hinv /grid_f/f /grid_fomn/fomn
      real angler(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_angler/angler
      real latr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real lonr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_latr/latr /grid_lonr/lonr
      real pm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_pm/pm    /metrics_pn/pn
     &       /metrics_omu/om_u /metrics_on_u/on_u
     &       /metrics_omv/om_v /metrics_on_v/on_v
      real dmde(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real dndx(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_dmde/dmde    /metrics_dndx/dndx
      real on_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmon_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pnom_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real grdscl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_on_p/on_p /metrics_om_p/om_p
     &       /metrics_on_r/on_r /metrics_om_r/om_r
     &       /metrics_pmon_u/pmon_u /metrics_pnom_v/pnom_v
     &                              /metrics_grdscl/grdscl
      real rmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /mask_r/rmask /mask_p/pmask
     &       /mask_u/umask /mask_v/vmask
      real sst_mc(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real mc_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real mc_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_sst_mc/sst_mc
     &       /innov_mask_mc/mc_mask
     &       /innov_oin_mc/mc_oin
      real sst_goes(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real goes_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real goes_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_sst_goes/sst_goes
     &       /innov_mask_goes/goes_mask
     &       /innov_oin_goes/goes_oin
      real ssh_swot(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real swot_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real swot_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_ssh_swot/ssh_swot
     &       /innov_mask_swot/swot_mask
     &       /innov_oin_swot/swot_oin
      real sss_sats(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real sats_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real sats_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_sss_sats/sss_sats
     &       /innov_mask_sats/sats_mask
     &       /innov_oin_sats/sats_oin
      real ssh_js1(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real js1_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real js1_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_ssh_js1/ssh_js1
     &       /innov_mask_js1/js1_mask
     &       /innov_oin_js1/js1_oin
      real lon_js1(max_js1),lat_js1(max_js1),
     &      ssh_js1_raw(max_js1)
      common /innov_ssh_js1_raw/lon_js1,lat_js1,ssh_js1_raw
      real ssh_ref(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real ref_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ssh_ref_yavg/ssh_ref
     &       /ssh_ref_mask/ref_mask
      real sst_fl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fl_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_sst_fl/sst_fl
     &       /innov_mask_fl/fl_mask
      real lon_fl(max_flight),lat_fl(max_flight),
     &      sst_fl_raw(max_flight)
      common /innov_sst_fl_raw/lon_fl,lat_fl,sst_fl_raw
      real u_hf(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real v_hf(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_u_hf/u_hf
     &       /innov_v_hf/v_hf
     &       /innov_umask_hf/hf_umask
     &       /innov_vmask_hf/hf_vmask
      real lon_hf(max_hfradar),lat_hf(max_hfradar),
     &      amperr_hf(max_hfradar),fitdif_hf(max_hfradar),
     &      u_hf_raw(max_hfradar),v_hf_raw(max_hfradar)
      common /innov_uv_hf_raw/lon_hf,lat_hf,
     &       amperr_hf,fitdif_hf,u_hf_raw,v_hf_raw
      real u_hf6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real v_hf6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_umask6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_vmask6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_uierr6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_vierr6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_u_hf6/u_hf6
     &       /innov_v_hf6/v_hf6
     &       /innov_umask_hf6/hf_umask6
     &       /innov_vmask_hf6/hf_vmask6
     &       /innov_uierr_hf6/hf_uierr6
     &       /innov_vierr_hf6/hf_vierr6
      real lon_hf6(max_hfradar6),lat_hf6(max_hfradar6),
     &      amperr_hf6(max_hfradar6),fitdif_hf6(max_hfradar6),
     &      u_hf_raw6(max_hfradar6),v_hf_raw6(max_hfradar6)
      common /innov_uv_hf_raw6/lon_hf6,lat_hf6,
     &       amperr_hf6,fitdif_hf6,u_hf_raw6,v_hf_raw6
      real t_cal(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_cal),
     &     s_cal(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_cal)
      real cal_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_cal)
      common /innov_temp_cal/t_cal
     &       /innov_salt_cal/s_cal
     &       /innov_mask_cal/cal_mask
      real lon_cal(max_cal,max_prf_cal),
     &     lat_cal(max_cal,max_prf_cal),
     &     t_cal_raw(max_cal,max_prf_cal),
     &     s_cal_raw(max_cal,max_prf_cal)
      common /innov_cal_raw/
     &     lon_cal,lat_cal,t_cal_raw,s_cal_raw
      real t_dor(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_dor),
     &     s_dor(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_dor)
      real dor_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_dor)
      common /innov_temp_dor/t_dor
     &       /innov_salt_dor/s_dor
     &       /innov_mask_dor/dor_mask
      real lon_dor(max_dor,max_prf_dor),
     &     lat_dor(max_dor,max_prf_dor),
     &     t_dor_raw(max_dor,max_prf_dor),
     &     s_dor_raw(max_dor,max_prf_dor)
      common /innov_dor_raw/
     &     lon_dor,lat_dor,t_dor_raw,s_dor_raw
      real t_sio(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real s_sio(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real sio_t_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real sio_s_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      common /innov_t_sio/t_sio
     &       /innov_s_sio/s_sio
     &       /innov_mask_tsio/sio_t_mask
     &       /innov_mask_ssio/sio_s_mask
      real   t_sio_raw(max_sio,max_prf),
     &       s_sio_raw(max_sio,max_prf)
      real lon_sio(max_sio), lat_sio(max_sio)
      real sio_ot(max_prf), sio_os(max_prf)
      common /sio_glides_st_raw/t_sio_raw, s_sio_raw,
     &                   lon_sio, lat_sio, sio_ot, sio_os
      real t_whoi(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real s_whoi(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real whoi_t_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real whoi_s_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      common /innov_t_whoi/t_whoi
     &       /innov_s_whoi/s_whoi
     &       /innov_mask_twhoi/whoi_t_mask
     &       /innov_mask_swhoi/whoi_s_mask
      real   t_whoi_raw(max_whoi,max_prf),
     &       s_whoi_raw(max_whoi,max_prf)
      real lon_whoi(max_whoi), lat_whoi(max_whoi)
      real whoi_ot(max_prf), whoi_os(max_prf)
      common /whoi_glides_st_raw/t_whoi_raw, s_whoi_raw,
     &             lon_whoi, lat_whoi,whoi_ot, whoi_os
      real s_ptsur(max_ptsur,max_prf),
     &     t_ptsur(max_ptsur,max_prf),
     &     mask_ptsur(max_ptsur,max_prf)
      real lon_ptsur(max_ptsur), lat_ptsur(max_ptsur)
      common /ptsur_cdt_st/s_ptsur,t_ptsur,
     &                      lon_ptsur,lat_ptsur,
     &                      mask_ptsur
      integer Iptsur(max_ptsur),Jptsur(max_ptsur)
      common /ptsur_cdt_loc/Iptsur,Jptsur
      real s_martn(max_martn,max_prf),
     &     t_martn(max_martn,max_prf),
     &     mask_martn(max_martn,max_prf)
      real lon_martn(max_martn), lat_martn(max_martn)
      common /martn_cdt_st/s_martn,t_martn,
     &                      lon_martn,lat_martn,
     &                      mask_martn
      integer Imartn(max_martn),Jmartn(max_martn)
      common /martn_cdt_loc/Imartn,Jmartn
      real t_moor(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real s_moor(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real moor_t_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real moor_s_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      common /innov_t_moor/t_moor
     &       /innov_s_moor/s_moor
     &       /innov_mask_tmoor/moor_t_mask
     &       /innov_mask_smoor/moor_s_mask
      real   t_moor_raw(max_moor,max_prf),
     &       s_moor_raw(max_moor,max_prf)
      real lon_moor(max_moor), lat_moor(max_moor)
      real moor_ot(max_prf), moor_os(max_prf)
      common /moor_ctd_raw/t_moor_raw, s_moor_raw,
     &         lon_moor, lat_moor,moor_ot,moor_os
      integer prf_num_sio,prf_num_whoi,prf_num_moor,
     &            prf_num_ptsur,prf_num_martn,num_flight,
     &            num_hfradar,num_hfradar6,num_js1
      common /insitu_num/prf_num_sio,prf_num_whoi,prf_num_moor,
     &            prf_num_ptsur,prf_num_martn,num_flight,
     &            num_hfradar,num_hfradar6,num_js1
      integer num_cal(max_prf_cal),num_dor(max_prf_dor)
      common /insitu_num_cal/num_cal,num_dor
      logical flag_tmi, flag_mc, flag_goes,flag_js1,
     &        flag_swot,flag_tp,flag_sio,flag_whoi,flag_ptsur,
     &        flag_martn,flag_flight,flag_cal,flag_dor,
     &        flag_moor,flag_prof,flag_ship,flag_hfradar,
     &        flag_hfradar6, flag_ssh_ref,
     &        flag_sats
      common /innov_flags/flag_tmi, flag_mc, flag_goes,
     &       flag_js1,flag_swot,flag_tp,flag_sio,flag_whoi,flag_ptsur,
     &        flag_martn,flag_flight,flag_cal,flag_dor,
     &        flag_moor,flag_prof,flag_ship,flag_hfradar,
     &        flag_hfradar6, flag_ssh_ref,
     &        flag_sats
      character*99 file_tmi,file_mc,file_goes
      common /innov_file_sst/file_tmi,file_mc,file_goes
      character*99 file_js1, file_swot,file_tp, file_sats
      common /innov_file_ssh/file_js1,file_swot, file_tp, file_sats
      character*99 file_sio_glider,
     &             file_whoi_glider
      common /innov_file_glider/file_sio_glider, file_whoi_glider
      character*99 file_ptsur_ctd,file_martn_ctd,
     &           file_moor_ctd,file_prof_ctd
      common /innov_file_ctd/file_ptsur_ctd,
     &          file_martn_ctd,file_moor_ctd,file_prof_ctd
      character*99 file_flight_sst,file_cal_auv,
     &            file_dor_auv,file_ship_sst
      common /innov_file_flight/file_flight_sst,
     &              file_cal_auv,file_dor_auv,file_ship_sst
      character*99 file_hfradar_uv,file_hfradar_uv6
      common /innov_file_HF/file_hfradar_uv,file_hfradar_uv6
      character*99 file_dist_coast
      common /dist_coast_file/file_dist_coast
      character*99 file_ssh_ref
      common /ssh_ref_file/file_ssh_ref
      character*99 file_ssh_ref_js1
      common /ssh_ref_file_js1/file_ssh_ref_js1
      integer time_level
      common /das_time_level/time_level
      character*99 file_innov_sio, file_innov_whoi
      common /innov_print_file/file_innov_sio,
     &                         file_innov_whoi
      character*99 file_innov_ana_sio, file_innov_ana_whoi
      common /innov_print_ana_file/file_innov_ana_sio,
     &                         file_innov_ana_whoi
      real anc(Lm+2,Mm+2)
      common /temp_nc/anc
      integer status, nc_id, v_id, i, j
      include 'netcdf.inc'
      flag_swot=.true.
      status = NF_OPEN(file_swot,0,nc_id)
      if (status.ne.NF_NOERR) then
        write(*,'(6x,A,(/8x,A))') '??? WARNING: GOES NOT FOUND',
     &          file_swot
        flag_swot=.false.
        goto 999
      endif
      status = NF_INQ_VARID(nc_id, 'ssh',v_id)
      if (status.ne.NF_NOERR) then
        print*, '??? WARNING: error in inquiring swot ssh'
        flag_swot=.false.
        goto 999
      endif
      status = NF_GET_VAR_DOUBLE(nc_id, v_id, anc)
      if (status.ne.NF_NOERR) then
        print*, '??? WARNING: error in reading swot ssh'
        flag_swot=.false.
        goto 999
      endif
      do j=0,Mm+1
        do i=0,Lm+1
          ssh_swot(i,j)=anc(i+1,j+1)
        enddo
      enddo
      do j=0,Mm+1
        do i=0,Lm+1
          if (ssh_swot(i,j) .gt. -900
     &        .and. h(i,j) .gt. hcmin) then
            swot_mask(i,j)=1.0
          else
            swot_mask(i,j)=0.0
          endif
        enddo
      enddo
      status = NF_CLOSE(nc_id)
999   continue
      if (flag_swot) then
        write(*, '(6x, A,(/8x,A))') 'OK: SWOT SSH READ IN',
     &         file_swot
      endif
      return
      end
