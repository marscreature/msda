#include "cppdefs.h"
#if defined MPI && defined SOLVE3D

      subroutine MessPass3D_tile (Istr,Iend,Jstr,Jend, A, nmax)
!
! This subroutine is designed for ROMS-MPI code. It exchanges domain
! boundary information, including 2 ghost-cells in each direction.
! Ping Wang 9/20/99.
!
      implicit none
# include "param.h"
# include "scalars.h"
      include 'mpif.h'

      integer nmax
      real A(GLOBAL_2D_ARRAY,nmax)
CSDISTRIBUTE_RESHAPE A(BLOCK_PATTERN) BLOCK_CLAUSE
      integer Istr,Iend,Jstr,Jend, i,j,k, isize,jsize,ksize,
     &        req(8), status(MPI_STATUS_SIZE,8), ierr

      integer sub_X,size_X, sub_E,size_E, size_Z
      parameter (size_Z=4*(N+1),
     &     sub_X=(Lm+NSUB_X-1)/NSUB_X,  size_X=2*(N+1)*(4+sub_X),
     &     sub_E=(Mm+NSUB_E-1)/NSUB_E,  size_E=2*(N+1)*(4+sub_E))

      real buf_snd4(size_Z),  ibuf_sndN(size_X),  buf_snd2(size_Z),
     &     buf_rev4(size_Z),  ibuf_revN(size_X),  buf_rev2(size_Z),

     &    jbuf_sndW(size_E),                      jbuf_sndE(size_E),
     &    jbuf_revW(size_E),                      jbuf_revE(size_E),

     &     buf_snd1(size_Z),  ibuf_sndS(size_X),  buf_snd3(size_Z),
     &     buf_rev1(size_Z),  ibuf_revS(size_X),  buf_rev3(size_Z)

c**
      common /buffers_3D/
     &     buf_snd4,     ibuf_sndN,     buf_snd2,
     &     buf_rev4,     ibuf_revN,     buf_rev2,

     &    jbuf_sndW,                    jbuf_sndE,
     &    jbuf_revW,                    jbuf_revE,

     &     buf_snd1,     ibuf_sndS,     buf_snd3,
     &     buf_rev1,     ibuf_revS,     buf_rev3
c**
!
#include "compute_message_bounds.h"
! 
      ksize=4*nmax                    ! message sizes for
      isize=2*ishft*nmax              ! corner messages and sides
      jsize=2*jshft*nmax              ! in XI and ETA directions 
!
! Prepare to receive and send: sides....
!
      if (WEST_INTER) then
        do k=1,nmax
          do j=jmin,jmax
            jbuf_sndW(k+nmax*(j-jmin      ))=A(1,j,k)
            jbuf_sndW(k+nmax*(j-jmin+jshft))=A(2,j,k)
          enddo
        enddo
        call MPI_Irecv (jbuf_revW, jsize, MPI_DOUBLE_PRECISION,
     &                     p_W, 2, MPI_COMM_WORLD, req(1), ierr)
        call MPI_Send  (jbuf_sndW, jsize, MPI_DOUBLE_PRECISION,
     &                     p_W, 1, MPI_COMM_WORLD,         ierr)
      endif

      if (EAST_INTER) then
        do k=1,nmax
          do j=jmin,jmax
            jbuf_sndE(k+nmax*(j-jmin      ))=A(Lm-1,j,k)
            jbuf_sndE(k+nmax*(j-jmin+jshft))=A(Lm  ,j,k)
          enddo
        enddo
        call MPI_Irecv (jbuf_revE, jsize, MPI_DOUBLE_PRECISION,
     &                     p_E, 1, MPI_COMM_WORLD, req(2), ierr)
        call MPI_Send  (jbuf_sndE, jsize, MPI_DOUBLE_PRECISION,
     &                     p_E, 2, MPI_COMM_WORLD,         ierr)
      endif

      if (SOUTH_INTER) then
        do k=1,nmax
          do i=imin,imax
            ibuf_sndS(k+nmax*(i-imin      ))=A(i,1,k)
            ibuf_sndS(k+nmax*(i-imin+ishft))=A(i,2,k)
          enddo
        enddo
        call MPI_Irecv (ibuf_revS, isize, MPI_DOUBLE_PRECISION,
     &                     p_S, 4, MPI_COMM_WORLD, req(3), ierr)
        call MPI_Send  (ibuf_sndS, isize, MPI_DOUBLE_PRECISION,
     &                     p_S, 3, MPI_COMM_WORLD,         ierr)
      endif

      if (NORTH_INTER) then
        do k=1,nmax
          do i=imin,imax
            ibuf_sndN(k+nmax*(i-imin      ))=A(i,Mm-1,k)
            ibuf_sndN(k+nmax*(i-imin+ishft))=A(i,Mm  ,k)
          enddo
        enddo
        call MPI_Irecv (ibuf_revN, isize, MPI_DOUBLE_PRECISION,
     &                     p_N, 3, MPI_COMM_WORLD, req(4), ierr)
        call MPI_Send  (ibuf_sndN, isize, MPI_DOUBLE_PRECISION,
     &                     p_N, 4, MPI_COMM_WORLD,         ierr)
      endif
!
! ...corners:
!
      if (SOUTH_INTER .and. WEST_INTER) then
        do k=1,nmax
          buf_snd1(k       )=A(1,1,k)
          buf_snd1(k+nmax  )=A(2,1,k)
          buf_snd1(k+2*nmax)=A(1,2,k)
          buf_snd1(k+3*nmax)=A(2,2,k)
        enddo
        call MPI_Irecv (buf_rev1, ksize, MPI_DOUBLE_PRECISION, p_SW,
     &                                6, MPI_COMM_WORLD, req(5),ierr)
        call MPI_Send  (buf_snd1, ksize, MPI_DOUBLE_PRECISION, p_SW,
     &                                5, MPI_COMM_WORLD,        ierr)
      endif

      if (NORTH_INTER .and. EAST_INTER) then
        do k=1,nmax
          buf_snd2(k       )=A(Lm-1,Mm-1,k)
          buf_snd2(k+nmax  )=A(Lm  ,Mm-1,k)
          buf_snd2(k+2*nmax)=A(Lm-1,Mm  ,k)
          buf_snd2(k+3*nmax)=A(Lm  ,Mm  ,k)
        enddo
        call MPI_Irecv (buf_rev2, ksize, MPI_DOUBLE_PRECISION, p_NE,
     &                                5, MPI_COMM_WORLD, req(6),ierr)
        call MPI_Send  (buf_snd2, ksize, MPI_DOUBLE_PRECISION, p_NE,
     &                                6, MPI_COMM_WORLD,        ierr)
      endif

      if (SOUTH_INTER .and. EAST_INTER) then
        do k=1,nmax
          buf_snd3(k       )=A(Lm-1,1,k)
          buf_snd3(k+nmax  )=A(Lm  ,1,k)
          buf_snd3(k+2*nmax)=A(Lm-1,2,k)
          buf_snd3(k+3*nmax)=A(Lm  ,2,k)
        enddo
        call MPI_Irecv (buf_rev3, ksize, MPI_DOUBLE_PRECISION, p_SE,
     &                                8, MPI_COMM_WORLD, req(7),ierr)
        call MPI_Send  (buf_snd3, ksize, MPI_DOUBLE_PRECISION, p_SE,
     &                                7, MPI_COMM_WORLD,        ierr)
      endif

      if (NORTH_INTER .and. WEST_INTER) then
        do k=1,nmax
          buf_snd4(k       )=A(1,Mm-1,k)
          buf_snd4(k+nmax  )=A(2,Mm-1,k)
          buf_snd4(k+2*nmax)=A(1,Mm  ,k)
          buf_snd4(k+3*nmax)=A(2,Mm  ,k)
        enddo
        call MPI_Irecv (buf_rev4, ksize, MPI_DOUBLE_PRECISION, p_NW,
     &                                7, MPI_COMM_WORLD, req(8),ierr)
        call MPI_Send  (buf_snd4, ksize, MPI_DOUBLE_PRECISION, p_NW,
     &                                8, MPI_COMM_WORLD,        ierr)
      endif
!
! Wait for completion of receive and fill ghost points: sides...
!
      if (WEST_INTER) then
        call MPI_Wait (req(1),status(1,1),ierr)
        do k=1,nmax
          do j=jmin,jmax
            A(-1,j,k)=jbuf_revW(k+nmax*(j-jmin      ))
            A( 0,j,k)=jbuf_revW(k+nmax*(j-jmin+jshft))
          enddo
        enddo
      endif

      if (EAST_INTER) then
        call MPI_Wait (req(2),status(1,2),ierr)
        do k=1,nmax
          do j=jmin,jmax
            A(Lm+1,j,k)=jbuf_revE(k+nmax*(j-jmin      ))
            A(Lm+2,j,k)=jbuf_revE(k+nmax*(j-jmin+jshft))
          enddo
        enddo
      endif

      if (SOUTH_INTER) then
        call MPI_Wait (req(3),status(1,3),ierr)
        do k=1,nmax
          do i=imin,imax
            A(i,-1,k)=ibuf_revS(k+nmax*(i-imin      ))
            A(i, 0,k)=ibuf_revS(k+nmax*(i-imin+ishft))
          enddo
        enddo
      endif

      if (NORTH_INTER) then
        call MPI_Wait (req(4),status(1,4),ierr)
        do k=1,nmax
          do i=imin,imax
            A(i,Mm+1,k)=ibuf_revN(k+nmax*(i-imin      ))
            A(i,Mm+2,k)=ibuf_revN(k+nmax*(i-imin+ishft))
          enddo
        enddo
      endif
!
! ...corners:
!
      if (SOUTH_INTER .and. WEST_INTER) then
        call MPI_Wait (req(5),status(1,5),ierr)
        do k=1,nmax
          A(-1,-1,k)=buf_rev1(k   )
          A( 0,-1,k)=buf_rev1(k+nmax )
          A(-1, 0,k)=buf_rev1(k+2*nmax)
          A( 0, 0,k)=buf_rev1(k+3*nmax)
        enddo
      endif

      if (NORTH_INTER.and.EAST_INTER) then
        call MPI_Wait (req(6),status(1,6),ierr)
        do k=1,nmax
          A(Lm+1,Mm+1,k)=buf_rev2(k   )
          A(Lm+2,Mm+1,k)=buf_rev2(k+nmax )
          A(Lm+1,Mm+2,k)=buf_rev2(k+2*nmax)
          A(Lm+2,Mm+2,k)=buf_rev2(k+3*nmax)
        enddo
      endif

      if (SOUTH_INTER .and. EAST_INTER) then
        call MPI_Wait (req(7),status(1,7),ierr)
        do k=1,nmax
          A(Lm+1,-1,k)=buf_rev3(k   )
          A(Lm+2,-1,k)=buf_rev3(k+nmax )
          A(Lm+1, 0,k)=buf_rev3(k+2*nmax)
          A(Lm+2, 0,k)=buf_rev3(k+3*nmax)
        enddo
      endif

      if (NORTH_INTER .and. WEST_INTER) then
        call MPI_Wait (req(8),status(1,8),ierr)
        do k=1,nmax
          A(-1,Mm+1,k)=buf_rev4(k   )
          A( 0,Mm+1,k)=buf_rev4(k+nmax )
          A(-1,Mm+2,k)=buf_rev4(k+2*nmax)
          A( 0,Mm+2,k)=buf_rev4(k+3*nmax)
        enddo
      endif

      return
      end
#else
      subroutine MessPass3D_empty
      return
      end
#endif

