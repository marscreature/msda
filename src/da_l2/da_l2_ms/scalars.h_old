! This is include file "scalars.h"
!---------------------------------
!
!  The following common block contains time variables and indices
! for 2D (k-indices) and 3D (n-indices) computational engines. Since
! they are changed together, they are placed into the same cache line
! despite their mixed type, so that only one cachene is being
! invalidated and has to be propagated accross the cluster.
!
! Note that the real values are placed first into the common block
! before the integer variables. This is done to prevent the
! possibility of misallignment of the 8-byte objects in the case
! when an uneven number of 4-byte integers is placed before a 8-byte
! real (in the case when default real size is set to 8bytes).
! Thought misallignment is not formally a violation of fortran
! standard, it may cause performance degradation and/or make compiler
! issue a warning message (Sun, DEC Alpha) or even crash (Alpha).
!
! time      Time since initialization [seconds];
! tdays     Time since initialization [days];
! dt        Time step for 3D primitive equations [seconds];
! dtfast    Time step for 2D (barotropic) mode [seconds];
!
      real dt, dtfast, time, tdays
      integer iic, kstp, krhs, knew
#ifdef SOLVE3D
     &      , iif, nstp, nrhs, nnew
#endif
      logical PREDICTOR_2D_STEP
      common /time_indices/ dt,dtfast, time,tdays, 
     &                       iic, kstp, krhs, knew,
#ifdef SOLVE3D
     &                       iif, nstp, nrhs, nnew,
#endif
     &                           PREDICTOR_2D_STEP 

!
! Slowly changing variables: these are typically set in the beginning
! of the run and either remain unchanged, or are changing only in
! association with the I/0. 
!
! xl, el   Physical size (m) of domain box in the XI-,ETA-directions.
!
! Tcline   Width (m) of surface or bottom boundary layer in which
!          higher vertical resolution is required during stretching.
! theta_s  S-coordinate surface control parameter, [0<theta_s<20].
! theta_b  S-coordinate bottom control parameter, [0<theta_b<1].
! hc       S-coordinate parameter, hc=min(hmin,Tcline).
!
! sc_r     S-coordinate independent variable, [-1 < sc < 0] at
!             vertical RHO-points
! sc_w     S-coordinate independent variable, [-1 < sc < 0] at
!             vertical W-points.
! Cs_r     Set of S-curves used to stretch the vertical coordinate
!             lines that follow the topography at vertical RHO-points.
! Cs_w     Set of S-curves used to stretch the vertical coordinate
!             lines that follow the topography at vertical W-points.
!
! rho0     Boussinesque Approximation Mean density [kg/m^3].
! R0       Background constant density anomaly [kg/m^3] used in
!                                      linear equation of state.
! T0,S0    Background temperature (Celsius) and salinity [PSU]
!                          values used in analytical fields;
! Tcoef    Thermal expansion coefficient in linear EOS;
! Scoef    Saline contraction coefficient in linear EOS;
!
! rdrg     Linear bottom drag coefficient.
! rdrg2    Quadratic bottom drag coefficient.
! gamma2   Slipperiness parameter, either 1. (free-slip)
!
! ntstart  Starting timestep in evolving the 3D primitive equations;
!                              usually 1, if not a restart run.
! ntimes   Number of timesteps for the 3D primitive equations in
!                                                    the current run.
! ndtfast  Number of timesteps for 2-D equations between each "dt".
!
! nrst     Number of timesteps between storage of restart fields.
! nwrt     Number of timesteps between writing of fields into
!                                                     history file.
! ninfo    Number of timesteps between print of single line
!                                   information to standard output.
! nsta     Number of timesteps between storage of station data.
! navg     Number of timesteps between storage of time-averaged
!                                                           fields.
! ntsavg   Starting timestep for accumulation of output time-
!                                                 averaged fields.
! nrrec    Counter of restart time records to read from disk,
!                   the last is used as the initial conditions.
!
! ldefhis  Logical switch used to create the history file.
!             If TRUE, a new history file is created. If FALSE,
!             data is appended to an existing history file.
! levsfrc  Deepest level to apply surface momentum stress as
!                                                 bodyforce.
! levbfrc  Shallowest level to apply bottom momentum stress as
!                                                 bodyforce.
!
      real time_avg, rho0,    rdrg,      rdrg2
     &           , xl, el,    visc2,     visc4,   gamma2
#ifdef SOLVE3D
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
     &           , rx0,       rx1,       tnu2(NT),tnu4(NT)
# if !defined LMD_MIXING && !defined BVF_MIXING\
  && !defined MY2_MIXING && !defined MY25_MIXING\
                         && !defined PP_MIXING
     &                      , Akv_bak,   Akt_bak(NT)
# endif
# ifdef MY25_MIXING
     &                      , Akq_bak,   q2nu2,   q2nu4
# endif
# ifndef NONLIN_EOS
     &                      , R0,T0,S0,  Tcoef,   Scoef
# endif
     &                      , weight(2,0:127)
#endif
#ifdef SPONGE
     &                    , x_sponge,   v_sponge
#endif
#ifdef TNUDGING
     &          , TauT_in, Taut_out, TauM_in, TauM_out
#endif   

      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
#ifdef AVERAGES
     &                                 , ntsavg,  navg
#endif
#ifdef STATIONS
     &                                 , nsta
#endif
#ifdef BODYFORCE
     &                     , levbfrc,   levsfrc
#endif 
      logical         ldefhis


      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , xl, el,    visc2,     visc4,   gamma2
#ifdef SOLVE3D
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
# if !defined LMD_MIXING && !defined BVF_MIXING\
  && !defined MY2_MIXING && !defined MY25_MIXING\
                         && !defined PP_MIXING
     &                      , Akv_bak,   Akt_bak
# endif
# ifdef MY25_MIXING
     &                      , Akq_bak,   q2nu2,   q2nu4
# endif
# ifndef NONLIN_EOS
     &                      , R0,T0,S0,  Tcoef,   Scoef
# endif
     &                      , weight
#endif
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
#ifdef AVERAGES
     &                                 , ntsavg,  navg
#endif
#ifdef STATIONS
     &                      , nsta
#endif
#ifdef BODYFORCE
     &                      , levbfrc,   levsfrc
#endif
#ifdef SPONGE
     &                    , x_sponge,   v_sponge
#endif
#ifdef TNUDGING
     &          , TauT_in, Taut_out, TauM_in, TauM_out
#endif   
     &      ,         ldefhis


!
! This following common block contains a set of globally accessable
! variables in order to allow information exchange between parallel
! threads working on different subdomains.
!
! Global summation variables are declared with 16 byte precision
! to avoid accumulation of roundoff errors, since roundoff  error
! depends on the order of summation, which is undeterministic in
! the case of summation between the parallel threads; not doing so
! would make itimpossible to pass an ETALON CHECK test if there is
! a feedback of these sums into the dynamics of the model, such as
! in the case when global mass conservation is enforced. 
!
!  One sunny sping day, somewhen in 1989 an american tourist, who
! happened to be an attorney, was walking along a Moscow street.
! Because it was the period of 'Perestroika' (which literally means
! 'remodelling'), so that a lot of construction was going on in
! Moscow, dozens of holes and trenches were open on the street. He
! felt into one of them, broke his leg, ended up in a hospital and
! complaining: In my country if a construction firm would not place
! little red flags around the construction zone to warn passers-by
! about the danger, I will sue em for their negligence! The doctror,
! who was performing surgery on his leg replied to him: Did not you
! see the one big red flag above the whole country at the first time?
!
! WARNING: FRAGILE ALIGNMENT SEQUENCE: In the following common block:
! since real objects are grouped in pairs and integer*4 are grouped
! in quartets, it is guaranteed that 16 Byte objects are aligned
! in 16 Byte boundaries and 8 Byte objects are aligned in 8 Byte
! boundaries. Removing or introduction of variables with violation
! of parity, as well as changing the sequence of variables in the
! common block may cause violation of alignment.
!
      logical synchro_flag
      integer tile_count, first_time, may_day_flag, bc_count
      real hmin, hmax,    grdmin, grdmax,      Cu_min, Cu_max
      real*QUAD volume,     avgke,      avgpe,        avgkp
     &                  , bc_crss
#ifdef OBC_VOLCONS
     &                  , bc_flux,      ubar_xs
#endif
#ifdef BIOLOGY 
     &                  , bio_count,    global_sum(0:30)
#endif
      common /communicators/
     &        volume,     avgke,      avgpe,        avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,      
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &                    bc_crss,
#ifdef OBC_VOLCONS
     &                    bc_flux,       ubar_xs,
#endif
#ifdef BIOLOGY
     &                    global_sum,
#endif
     &        synchro_flag           
!
!  The following common block contains process counters and model
! timers. These are used to measure CPU time consumed by different
! parallel threads during the whole run, as well as in various
! parallel regions, if so is needed. These variables are used purely
! for diagnostic/performance measurements purposes and do not affect
! the model results.
!
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count

#ifdef MPI
!
! MPI rlated variables
! === ====== =========
!
      logical EAST_INTER, WEST_INTER, NORTH_INTER, SOUTH_INTER
      integer mynode, ii,jj, p_W,p_E,p_S,p_N, p_SW,p_SE, p_NW,p_NE
      common /comm_setup/ mynode, ii,jj, p_W,p_E,p_S,p_N, p_SW,p_SE,
     &  p_NW,p_NE, EAST_INTER, WEST_INTER, NORTH_INTER, SOUTH_INTER
#endif




!
! Physical constants:
! ======== ==========

      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
!
! Earth radius [m]; Aceleration of gravity [m/s^2], duration
! of the day in seconds and its inverse; Julian offset day.

      real Eradius, g, day2sec,sec2day, jul_off
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.)
!
! Acceleration of gravity (nondimensional for Soliton problem)
!
#ifdef SOLITON
      parameter (g=1.)
#else
      parameter (g=9.81)
#endif
!
!  Specific heat [Joules/kg/degC] for seawater, it is approximately
!  4000, and varies only slightly (see Gill, 1982, Appendix 3).
!
      real Cp
      parameter (Cp=3985.0)

      real vonKar
      parameter (vonKar=0.41)


