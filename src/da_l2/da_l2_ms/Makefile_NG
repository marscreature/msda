# Universal machine independent makefile for SCRUM/ROMS model 
#====================================================================
# Set machine dependent definitions and rules.

include Makedefs

# Configuration for SCRUM/ROMS model:
#====================================================================
# All source codes files are sorted into eight groups, separated by
# blanc lines:
#  1) main driving part;          5) vertical mixing schemes; 
#  2) 2D time stepping engine;    6) on-fly model diagnostic routines;
#  3) 3D time stepping engine;    7) netCDF I/O routines;
#  4) sea-water EOS routines;     8) model forcing routines;
#  5) biology routines;
#--------------------------------------------------------------------

 SRCS = main.F	check_switches1.F	check_switches2.F	 check_srcs.F\
	read_inp.F	sta_par.F	setup_kwds.F	check_kwds.F\
	timers.F	init_scalars.F	init_arrays.F\
	set_scoord.F	setup_grid1.F	setup_grid2.F\
	visc_rescale.F	ana_initial.F	analytical.F\
\
	das_init_arrays.F	das_init_scalars.F\
	das_setup1.F das_setup2.F das_spline1d.F das_exchange.F\
	das_MessPass2D.F	das_s2z.F das_z2s.F das_analysisNG.F\
	das_ana_bvar.F das_costb.F das_vec.F das_adj_costb.F\
	das_get_tmi.F  das_get_mc.F das_lbfgs.F das_innov_sst.F\
	das_cost_sst.F das_adj_csst.F das_ca2zero.F das_adj_zero.F\
	das_adj_zero_s.F das_read_inp.F das_matvec.F das_adj_matvec.F\
	das_get_corr.F das_mask.F das_adj_mask.F das_varvec.F das_adj_varvec.F\
	das_rho_eos9.F das_copy9.F das_geos_uv.F das_get_bvar.F\
	das_get_zeta_ref.F das_innov_print.F das_innov_ana_print.F\
\
	das_get_js1.F das_innov_ssh.F das_rho_eos_lin.F\
	das_diag_ssh.F das_cost_ssh.F\
	das_adj_rho_eos.F das_adj_diag_ssh.F das_adj_cssh.F\
	das_adj_geos_uv.F\
\
	das_get_insitu.F das_innov_sio.F das_cost_glider.F das_adj_cglider.F\
	das_innov_whoi.F das_innov_flight.F das_cost_flight.F das_adj_cflight.F\
	das_innov_ptsur.F das_innov_martn.F\
	das_get_auv.F das_innov_calauv.F das_cost_calauv.F das_adj_cauv.F\
	das_innov_dorauv.F das_cost_dorauv.F das_adj_dauv.F\
\
	das_copy_smooth.F das_innov_smooth.F\
\
	set_depth.F	omega.F\
\
	rho_eos.F\
\
	diag.F	wvlcty.F	checkdims.F	grid_stiffness.F\
\
	get_date.F	lenstr.F	closecdf.F\
	insert_node.F	nf_fread.F	get_grid.F	get_initial.F\
	put_global_atts.F		def_grid.F	wrt_grid.F\
	def_his.F	def_rst.F	def_station.F\
	wrt_his.F	wrt_rst.F	wrt_avg.F	wrt_station.F\
\
	set_cycle.F	get_uclima.F\
	get_vbc.F	get_tclima.F	get_ssh.F	get_wwave.F\
	get_smflux.F	get_stflux.F	get_srflux.F	get_sst.F\
\
	MPI_Setup.F	MessPass2D.F	MessPass3D.F	exchange.F\
\
	biology.F	bio_diag.F
#
#	das_cost_ssh_lin.F das_adj_cssh_lin.F das_cost_sst_lin.F das_adj_csst_lin.F\
#	das_costb_lin.F das_adj_costb_lin.F\
 
#
# non-parallelized/old/obsolete stuff
#
#	ri_number.F	sg_bbl96.F	sg_ubab.F\
#	my25_q.F	my25_vmix.F	my2_vmix.F\
#	smol_adv.F	smol_ups.F	smol_adiff.F\
#	get_rhosxe.F	copy.F		get_bsedim.F
#
#--------------------------------------------------------------------

 RCS = $(SRCS:.F=.f)

 OBJS = $(RCS:.f=.o)

# SBIN = /alhena/tmp/ourocean/bin/reanalysis/das_l3_insitu.exe

 SBIN = /aosn1/zhijin/aosn/bin/das_l3NG.exe

#
# Eecutable file.
# ========= =====
#
$(SBIN): $(OBJS)
	$(LDR) $(FFLAGS) $(LDFLAGS) -o a.out $(OBJS) $(LCDF) $(LMPI) 
	mv a.out $(SBIN)
#
# Everything
# ==========
all: tools depend $(SBIN)
#
# Special treatment of main: include LDFLAGS into compilation,
#                             since it may contain directives.
main.o: main.f
	$(CFT) $(FFLAGS) $(LDFLAGS) -c main.f
#
# Auxiliary utility programs and List of Dependecies:
# ========= ======= ======== === ==== == ============
#
  TOOLS = mpc cross_matrix cppcheck srcscheck checkkwds partit ncjoin ncrename

tools: $(TOOLS)

mpc: mpc.F
	$(CPP) -P mpc.F > mpc.f
	$(LDR) $(FFLAGS) -o mpc mpc.f
cross_matrix: cross_matrix.o
	$(LDR) $(FFLAGS) -o cross_matrix cross_matrix.o
cppcheck: cppcheck.o
	$(LDR) $(FFLAGS) -o cppcheck cppcheck.o
srcscheck: srcscheck.o
	$(LDR) $(FFLAGS) -o srcscheck srcscheck.o
checkkwds: checkkwds.o
	$(LDR) -o checkkwds checkkwds.o
checkdefs: check_switches1.F setup_kwds.F

check_switches1.F: cppcheck cppdefs.h
	cppcheck
check_srcs.F: srcscheck Makefile
	srcscheck
setup_kwds.F: checkkwds read_inp.F
	checkkwds
partit: partit.o insert_node.o lenstr.o
	$(LDR) $(FFLAGS) -o partit partit.F insert_node.o lenstr.o $(LCDF)
ncjoin: ncjoin.o lenstr.o
	$(LDR) $(FFLAGS) -o ncjoin ncjoin.o lenstr.o $(LCDF)
ncrename: ncrename.o lenstr.o
	$(LDR) $(FFLAGS) -o ncrename ncrename.o lenstr.o $(LCDF)

depend: checkdefs cross_matrix
	cross_matrix *.F
#
# Target to create tar file.
# ====== == ====== === =====
#
tarfile:
	tar cvf roms.tar Make* *.h *.F *.README README.*  *.in *.in.*
#
# Cleaning:
# =========
#
rmtools:
	/bin/rm -f $(TOOLS)
clean:
	/bin/rm -f core core.* *.o *.i *.s *.f *.trace

clobber: clean
	/bin/rm -rf $(SBIN) $(TOOLS) ./rii_files


plotter: plotter.F
	f77 -n32 -o plotter plotter.F $(LIBNCAR)

#
# Special treatment for barrier function:
# THERE SHALL BE NO OPTIMIZATION HERE!!!!
#
my_barrier.o: my_barrier.f
	$(CFT) -c -O0 my_barrier.f
#
# Include automatically generated dependency list:
#
include Make.depend

