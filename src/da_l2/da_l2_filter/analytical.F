#include "cppdefs.h"
!
!  ANALYTICAL PACKAGE:
!--------------------------------------------------------------------
!
!  This package is used to provide various analytical fields to the
!  model when appropriate.
!
!  Routines:
!
!  ana_bmflux_tile   Analytical kinematic bottom momentum flux.
!  ana_btflux_tile   Analytical kinematic bottom flux of tracer
!                          type variables.
!  ana_bsedim_tile   Analytical bottom sediment grain size
!                          and density.
!  ana_meanRHO_tile  Analytical mean density anomaly.                
!  ana_smflux_tile   Analytical kinematic surface momentum flux
!                          (wind stress).
!  ana_srflux_tile   Analytical kinematic surface shortwave
!                          radiation.
!  ana_ssh_tile      Analytical sea surface height climatology.      
!  ana_sst_tile      Analytical sea surface temperature and dQdSST  
!                         which are used during heat flux correction.
!  ana_stflux_tile   Analytical kinematic surface flux of tracer type
!                          variables.
!  ana_tclima_tile   Analytical tracer climatology fields.  
!  ana_uclima_tile   Analytical tracer climatology fields.  
!  ana_wwave_tile    Analytical wind induced wave amplitude,
!                         direction and period.
!-------------------------------------------------------------------
!
# if !defined OPENMP
      integer function omp_get_thread_num()
      omp_get_thread_num=0
      return
      end
      integer function omp_get_num_threads()
      omp_get_num_threads=1
      return
      end
      subroutine mp_setlock ()
      return
      end
      subroutine mp_unsetlock ()
      return
      end
#endif

#ifdef ANA_BMFLUX
      subroutine ana_bmflux_tile (Istr,Iend,Jstr,Jend)
!
!---------------------------------------------------------------------
!  This routine sets kinematic bottom momentum flux (bottom stress)
! "bustr" and "bvstr" [m^2/s^2] using an analytical expression.
!---------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j
# include "param.h"
# include "grid.h"
# include "forces.h"
# include "scalars.h"
!
# include "compute_auxiliary_bounds.h"
!
      do j=JstrR,JendR
        do i=Istr,IendR
          bustr(i,j)=???
        enddo
      enddo
      do j=Jstr,JendR
        do i=IstrR,IendR
          bvstr(i,j)=???
        enddo
      enddo
      return
      end
#endif /* ANA_BMFLUX */
#ifdef ANA_BTFLUX
      subroutine ana_btflux_tile (Istr,Iend,Jstr,Jend, itrc)
!
!---------------------------------------------------------------------
!  This routine sets kinematic bottom flux of tracer type variables
!  [tracer units m/s].
!
!  On Input:
!     itrc      Tracer type array index.
!---------------------------------------------------------------------
!
      implicit none
      integer itrc, Istr,Iend,Jstr,Jend, i,j
# include "param.h"
# include "grid.h"
# include "forces.h"
# include "scalars.h"
!
# include "compute_auxiliary_bounds.h"
!
      if (itrc.eq.itemp) then
!
! Set kinematic bottom heat flux [degC m/s] at horizontal RHO-points.
!--------------------------------------------------------------------
!
# if defined BASIN     || defined CANYON_A  || defined CANYON_B   \
  || defined DAMEE_B   || defined DAMEE_S   || defined GRAV_ADJ   \
  || defined NJ_BIGHT  || defined NPACIFIC  || defined OVERFLOW   \
  || defined SEAMOUNT  || defined SHELFRONT || defined TASMAN_SEA \
                       || defined UPWELLING || defined USWEST
        do j=JstrR,JendR
          do i=IstrR,IendR
            btflx(i,j,itemp)=0.
          enddo
        enddo
# else
        do j=JstrR,JendR
          do i=IstrR,IendR
            btflx(i,j,itemp)=???
          enddo
        enddo
# endif
# ifdef SALINITY
      elseif (itrc.eq.isalt) then
!
!  Set kinematic bottom salt flux (m/s) at horizontal RHO-points,
!  scaling by bottom salinity is done in STEP3D.
!---------------------------------------------------------------------
!
#  if defined DAMEE_B   || defined DAMEE_S   || defined NJ_BIGHT   \
   || defined NPACIFIC  || defined SHELFRONT || defined TASMAN_SEA \
                        || defined UPWELLING || defined USWEST
        do j=JstrR,JendR
          do i=IstrR,IendR
            btflx(i,j,isalt)=0.
          enddo
        enddo
#  else
        do j=JstrR,JendR
          do i=IstrR,IendR
            btflx(i,j,isalt)=???
          enddo
        enddo
#  endif
# endif /* SALINITY */
      else
!
!  Set kinematic surface flux of additional tracers, if any.
!---------------------------------------------------------------------
!
      endif
      return
      end
#endif /* ANA_BTFLUX */
#if defined ANA_BSEDIM && defined SG_BBL96
      subroutine ana_bsedim_tile (Istr,Iend,Jstr,Jend)
!
!---------------------------------------------------------------------
!  This routine sets initial bottom sediment grain diameter size [m]
!  and density used in the bottom boundary formulation [kg/m^3].
!---------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j
# include "param.h"
# include "bblm.h"
# include "grid.h"
# include "scalars.h"
!
# include "compute_auxiliary_bounds.h"
!
      do j=JstrR,JendR
        do i=IstrR,IendR
          Ssize(i,j)=???
          Sdens(i,j)=???
        enddo
      enddo
      return
      end
#endif /* ANA_BSEDIM && SG_BBL96 */
#if defined SOLVE3D && defined ANA_MEANRHO
      subroutine ana_meanRHO_tile (Istr,Iend,Jstr,Jend)
!
!--------------------------------------------------------------------
!  This subroutine sets mean density anomaly [kg/m^3] using an
!  analytical expression.
!--------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k
# include "param.h"
# include "grid.h"
# include "ocean3d.h"
# include "scalars.h"
!
# include "compute_auxiliary_bounds.h"
!
!  Set mean density anomaly (kg/m^3) at horizontal and vertical
!  RHO-points.
!
# if defined BASIN
c     cff1=(44.690/39.382)**2
c     cff2=cff1*(rho0*800./g)*(5.0e-5/((42.689/44.690)**2))
c-song:
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
c           rhobar(i,j,k)=R0-cff2*exp(z_r(i,j,k)/800.)*
            rhobar(i,j,k)=R0-
     &  (44.690/39.382)**2
     &  *(rho0*800./g)*(5.0e-5/((42.689/44.690)**2))
     &                           *exp(z_r(i,j,k)/800.)*
     &                    (0.6-0.4*tanh(z_r(i,j,k)/800.))
           enddo
        enddo
      enddo
# elif defined CANYON_A || defined GRAV_ADJ || defined OVERFLOW
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            rhobar(i,j,k)=R0
          enddo
        enddo
      enddo
# elif defined CANYON_B
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            rhobar(i,j,k)=R0-3.488*exp(z_w(i,j,k)/800.)*
     &                  (1.-(2./3.)*tanh(z_r(i,j,k)/800.))
          enddo
        enddo
      enddo
# elif defined DAMEE_B || defined DAMEE_S
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            rhobar(i,j,k)=30.5-0.004*z_r(i,j,k)
     &                     -4.*exp(z_r(i,j,k)/2000.)
          enddo
        enddo
      enddo
# elif defined NJ_BIGHT
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            if (z_r(i,j,k).ge.-15.0 ) then
              cff1=2.049264257728403e+01-z_r(i,j,k)*(
     &               2.640850848793918e-01+z_r(i,j,k)*(
     &               2.751125328535212e-01+z_r(i,j,k)*(
     &               9.207489761648872e-02+z_r(i,j,k)*(
     &               1.449075725742839e-02+z_r(i,j,k)*(
     &               1.078215685912076e-03+z_r(i,j,k)*(
     &               3.240318053903974e-05+
     &                1.262826857690271e-07*z_r(i,j,k)
     &                                           ))))))
              cff2=3.066489149193135e+01-z_r(i,j,k)*(
     &               1.476725262946735e-01+z_r(i,j,k)*(
     &               1.126455760313399e-01+z_r(i,j,k)*(
     &               3.900923281871022e-02+z_r(i,j,k)*(
     &               6.939014937447098e-03+z_r(i,j,k)*(
     &               6.604436696792939e-04+z_r(i,j,k)*(
     &               3.191792361954220e-05+
     &               6.177352634409320e-07*z_r(i,j,k)
     &                                           ))))))
            else    
               cff1=14.6+6.7 *tanh(1.1*z_r(i,j,k)+15.9)
               cff2=31.3-0.55*tanh(1.1*z_r(i,j,k)+15.9)
            endif 
            rhobar(i,j,k)=R0+Tcoef*cff1+Scoef*cff2
          enddo
        enddo
      enddo
# elif defined NPACIFIC || defined USWEST
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            rhobar(i,j,k)=30.5-0.004*z_r(i,j,k)
     &                         -4.*exp(z_r(i,j,k)/2000.)
          enddo
        enddo
      enddo
# elif defined SEAMOUNT
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            rhobar(i,j,k)=28.-2.*exp(z_r(i,j,k)/1000.)
          enddo
        enddo
      enddo
# elif defined SHELFRONT
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            rhobar(i,j,k)=R0
          enddo
        enddo
      enddo
# elif defined UPWELLING
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            rhobar(i,j,k)=26.459472-2.24*exp(z_r(i,j,k)/50.)
          enddo
        enddo
      enddo
# else
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            rhobar(i,j,k)=???
          enddo
        enddo
      enddo
# endif
# ifdef MASKING
!
!  Apply Land/Sea mask.
!
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            rhobar(i,j,k)=rhobar(i,j,k)*rmask(i,j)
          enddo
        enddo
      enddo
# endif
      return
      end       
#endif /* ANA_MEANRHO && SOLVE3D */

#ifdef ANA_SMFLUX
      subroutine ana_smflux_tile (Istr,Iend,Jstr,Jend)
!
!  Sets kinematic surface momentum flux (wind stress) "sustr" and "svstr"
!  [m^2/s^2] using an analytical expression.
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j
      real Ewind, Nwind, dircoef, windamp
# include "param.h"
# include "grid.h"
# include "forces.h"
# include "scalars.h"
      data windamp /0./ 
      data Ewind, Nwind, dircoef /0., 0., 0./
      save windamp
!
# include "compute_auxiliary_bounds.h"
!
!  Set kinematic surface momentum flux (wind stress) component in the
!  XI-direction (m^2/s^2) at horizontal U-points.
!
# ifdef BASIN
      cff1=0.0001 * 0.5*(1.+tanh((time-6.*86400.)/(3.*86400.)))
      cff2=2.*pi/el
      do j=JstrR,JendR
        do i=Istr,IendR
          sustr(i,j)=-cff1*cos(cff2*yr(i,j))
        enddo
      enddo
# elif defined CANYON_A || defined CANYON_B
      do j=JstrR,JendR
        do i=Istr,IendR
          sustr(i,j)=0.0001*0.5*sin(2.*pi*tdays/10.)*
     &               (1.-tanh((yr(i,j)-0.5*el)/10000.))
        enddo
      enddo
# elif defined DAMEE_B   || defined DAMEE_S  || defined GRAV_ADJ ||\
       defined NPACIFIC  || defined OVERFLOW || defined SEAMOUNT ||\
       defined SHELFRONT || defined SOLITON
      do j=JstrR,JendR
        do i=Istr,IendR
          sustr(i,j)=0.
        enddo
      enddo
# elif defined NJ_BIGHT
      windamp=0.086824313
      dircoef=0.5714286
      if (tdays.le.0.5) then
        Ewind=windamp*dircoef*sin(pi*tdays)/rho0
        Nwind=windamp*sin(pi*tdays)/rho0
      else
        Ewind=windamp*dircoef/rho0
        Nwind=windamp/rho0
      endif
      do j=JstrR,JendR
        do i=Istr,IendR
          cff1=0.5*(angler(i-1,j)+angler(i,j))
          sustr(i,j)=Ewind*cos(cff1)+Nwind*sin(cff1)
        enddo
      enddo
# elif defined TASMAN_SEA
      windamp=0.5*(1.+tanh((time-6.*86400.)/(3.*86400.)))
      do j=JstrR,JendR
        do i=Istr,IendR
          cff1=yr(i,j)/el
          sustr(i,j)=windamp*cff1*(1.-cff1)*
     &               (1.7580e-3+cff1*(-2.7907e-3+cff1*
     &            (-4.0764e-3+cff1*(9.1903e-3-cff1*4.7915e-3))))
        enddo
      enddo
# elif defined UPWELLING
      if (tdays.le.2.) then
        windamp=-0.1*sin(pi*tdays/4.)/rho0
      else
        windamp=-0.1/rho0
      endif
c??
c??
c      windamp=0.
c??
c??

      do j=JstrR,JendR
        do i=Istr,IendR
          sustr(i,j)=windamp
        enddo
      enddo
# elif defined USWEST
      do j=JstrR,JendR
        do i=Istr,IendR
c         cff1=(latr(i,j)-latr(Lm/2,Mm/2))/20.
c         sustr(i,j)=1.d-4.*cff1
c         sustr(i,j)=-1.d-4
          sustr(i,j)=0.
        enddo
      enddo
# else
      do j=JstrR,JendR
        do i=Istr,IendR
          sustr(i,j)=???
        enddo
      enddo
# endif
!
!  Set kinematic surface momentum flux (wind stress) component in the
!  ETA-direction (m^2/s^2) at horizontal V-points.
!
# if defined BASIN     || defined DAMEE_B  || defined DAMEE_S    || \
     defined CANYON_A  || defined CANYON_B || defined GRAV_ADJ   || \
     defined NPACIFIC  || defined OVERFLOW || defined SEAMOUNT   || \
     defined SHELFRONT || defined SOLITON  || defined TASMAN_SEA || \
     defined UPWELLING
      do j=Jstr,JendR
        do i=IstrR,IendR
          svstr(i,j)=0.
        enddo
      enddo
# elif defined NJ_BIGHT
      do j=Jstr,JendR
        do i=IstrR,IendR
          cff1=0.5*(angler(i,j)+angler(i,j-1)) 
          svstr(i,j)=-Ewind*sin(cff1)+Nwind*cos(cff1)
        enddo
      enddo
# elif defined USWEST
      do j=Jstr,JendR
        do i=IstrR,IendR
c          svstr(i,j)=-1.0d-4
          svstr(i,j)=0.
        enddo
      enddo
# else
      do j=Jstr,JendR
        do i=IstrR,IendR
          svstr(i,j)=???
        enddo
      enddo
# endif
      return
      end
#endif /* ANA_SMFLUX */
#ifdef ANA_SRFLUX
      subroutine ana_srflux_tile (Istr,Iend,Jstr,Jend)
!
!---------------------------------------------------------------------
!  This subroutine sets kinematic surface solar shortwave radiation
!  flux "srflx" (degC m/s) using an analytical expression.
!---------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j
# include "param.h"
# include "grid.h"
# include "forces.h"
# include "scalars.h"
!
# include "compute_auxiliary_bounds.h"
!
!  Set kinematic surface solar shortwave radiation [degC m/s] at
!  horizontal RHO-points.
!
# if defined DAMEE_B  || defined DAMEE_S  || defined NJ_BIGHT ||\
     defined NPACIFIC || defined OVERFLOW || defined USWEST || \
     defined UPWELLING
      do j=JstrR,JendR
        do i=IstrR,IendR
          srflx(i,j)=0.
        enddo
      enddo
# else
      do j=JstrR,JendR
        do i=IstrR,IendR
          srflx(i,j)=???
        enddo
      enddo
# endif
      return
      end
#endif /* ANA_SRFLUX */
#if defined ANA_SSH && defined ZNUDGING 
      subroutine ana_ssh_tile (Istr,Iend,Jstr,Jend)
!
!---------------------------------------------------------------------
!  This routine sets analytical sea surface height climatology [m].
!---------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j
# include "param.h"
# include "grid.h"
# include "climat.h"
# include "scalars.h"
!
# include "compute_auxiliary_bounds.h"
!
!  Set sea surface height (meters).
!
      do j=JstrR,JendR
        do i=IstrR,IendR
          ssh(i,j)=???
        enddo
      enddo
      return
      end
#endif /* ANA_SSH && ZNUDGING */
#if defined ANA_SST && defined QCORRECTION
      subroutine ana_sst_tile (Istr,Iend,Jstr,Jend)
!
!--------------------------------------------------------------------
!  This routine sets sea surface temperature SST[Celsius] and surface
!  net heat flux sensitivity dQdSTT to sea surface temperature using
!  analytical expressions. dQdSTT is usually computed in units of
!  [Watts/m^2/degC]. It needs to be scaled to [m/s] by dividing by
!  rho0*Cp.  These forcing fields are used when the heat flux
!  correction is activated:
!
!       Q_model ~ Q + dQdSST * (T_model - SST)
!--------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j
# include "param.h"
# include "grid.h"
# include "forces.h"
# include "scalars.h"
!
# include "compute_auxiliary_bounds.h"
!
      do j=JstrR,JendR
        do i=IstrR,IendR
          sst(i,j)=???
          dqdt(i,j)=???
        enddo
      enddo
      return
      end
#endif /* ANA_SST && QCORRECTION */
#if defined ANA_STFLUX || defined ANA_SSFLUX
      subroutine ana_stflux_tile (Istr,Iend,Jstr,Jend, itrc)
!
!--------------------------------------------------------------------
!  This routine sets kinematic surface flux of tracer type variables
!  "stflx" (tracer units m/s) using analytical expressions.
!
!  On Input:
!     itrc      Tracer type array index.
!--------------------------------------------------------------------
!
      implicit none
      integer itrc, Istr,Iend,Jstr,Jend, i,j 
# include "param.h"
# include "grid.h"
# include "forces.h"
# include "scalars.h"
c
# include "compute_auxiliary_bounds.h"
c
      if (itrc.eq.itemp) then
!
!  Set kinematic surface heat flux [degC m/s] at horizontal
!  RHO-points.
!
#if defined BASIN     || defined DAMEE_B   || defined DAMEE_S    ||\
    defined CANYON_A  || defined CANYON_B  || defined GRAV_ADJ   ||\
    defined NJ_BIGHT  || defined NPACIFIC  || defined OVERFLOW   ||\
    defined SEAMOUNT  || defined SHELFRONT || defined TASMAN_SEA ||\
    defined UPWELLING || defined USWEST
        do j=JstrR,JendR
          do i=IstrR,IendR
            stflx(i,j,itemp)=0.
          enddo
        enddo
#else
        do j=JstrR,JendR
          do i=IstrR,IendR
            stflx(i,j,itemp)=???
          enddo
        enddo
#endif
#ifdef SALINITY
      elseif (itrc.eq.isalt) then
!
!  Set kinematic surface freshwater flux (m/s) at horizontal
!  RHO-points, scaling by surface salinity is done in STEP3D.
!
# if defined DAMEE_B   || defined DAMEE_S   || defined NJ_BIGHT   ||\
     defined NPACIFIC  || defined SHELFRONT || defined TASMAN_SEA ||\
     defined UPWELLING || defined USWEST
        do j=JstrR,JendR
          do i=IstrR,IendR
            stflx(i,j,isalt)=0.
          enddo
        enddo
# else
        do j=JstrR,JendR
          do i=IstrR,IendR
            stflx(i,j,isalt)=???
          enddo
        enddo
# endif
#endif /* SALINITY */
      else
!
!  Set kinematic surface flux of additional tracers, if any.
!
      endif
      return
      end
#endif /* ANA_STFLUX || ANA_SSFLUX */
!
#if defined ANA_TCLIMA && defined TCLIMATOLOGY
      subroutine ana_tclima (tile)
      implicit none
      integer tile
#include "param.h"
#include "compute_tile_bounds.h"
      call ana_tclima_tile   (Istr,Iend,Jstr,Jend)
      return
      end
      subroutine ana_tclima_tile (Istr,Iend,Jstr,Jend)
!
!---------------------------------------------------------------------
!  This routine sets analytical tracer climatology fields.
!---------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k
# include "param.h"
# include "climat.h"
# include "grid.h"
# include "ocean3d.h"
# include "scalars.h"
!
# include "compute_auxiliary_bounds.h"
!
!  Set climatology fields for tracer type variables.
!---------------------------------------------------------------------
!
# if defined NJ_BIGHT
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            if (z_r(i,j,k).ge.-15.0) then
              tclm(i,j,k,itemp)=2.049264257728403e+01-z_r(i,j,k)*(
     &                          2.640850848793918e-01+z_r(i,j,k)*(
     &                          2.751125328535212e-01+z_r(i,j,k)*(
     &                          9.207489761648872e-02+z_r(i,j,k)*(
     &                          1.449075725742839e-02+z_r(i,j,k)*(
     &                          1.078215685912076e-03+z_r(i,j,k)*(
     &                          3.240318053903974e-05+
     &                          1.262826857690271e-07*z_r(i,j,k)
     &                                                      ))))))
              tclm(i,j,k,isalt)=3.066489149193135e+01-z_r(i,j,k)*(
     &                          1.476725262946735e-01+z_r(i,j,k)*(
     &                          1.126455760313399e-01+z_r(i,j,k)*(
     &                          3.900923281871022e-02+z_r(i,j,k)*(
     &                          6.939014937447098e-03+z_r(i,j,k)*(
     &                          6.604436696792939e-04+z_r(i,j,k)*(
     &                          3.191792361954220e-05+
     &                          6.177352634409320e-07*z_r(i,j,k)
     &                                                       ))))))
            else
               tclm(i,j,k,itemp)=14.6+6.7 *tanh(1.1*z_r(i,j,k)+15.9)
               tclm(i,j,k,isalt)=31.3-0.55*tanh(1.1*z_r(i,j,k)+15.9)
            endif
          enddo
        enddo
      enddo
# else
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            tclm(i,j,k,itemp)=???
#  ifdef SALINITY
            tclm(i,j,k,isalt)=???
#  endif /* SALINITY */
          enddo
        enddo
      enddo
# endif
      return
      end
#endif /* ANA_TCLIMA && TCLIMATOLOGY */
!
#if defined ANA_UCLIMA && defined UCLIMATOLOGY
      subroutine ana_uclima (tile)
      implicit none
      integer tile
#include "param.h"
#include "compute_tile_bounds.h"
      call ana_uclima_tile   (Istr,Iend,Jstr,Jend)
      return
      end
!
      subroutine ana_uclima_tile (Istr,Iend,Jstr,Jend)
!
!---------------------------------------------------------------------
!  This routine sets analytical momentum climatology fields.
!---------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k
# include "param.h"
# include "climat.h"
# include "grid.h"
# include "ocean3d.h"
# include "scalars.h"
!
# include "compute_auxiliary_bounds.h"
!
!  Set climatology fields for tracer type variables.
!---------------------------------------------------------------------
!
# if defined USWEST
      do j=JstrR,JendR
        do i=IstrR,IendR
          ubclm(i,j)=0.
          vbclm(i,j)=0.
        enddo
      enddo
#  ifdef SOLVE3D
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            uclm(i,j,k)=0.
            vclm(i,j,k)=0.
          enddo
        enddo
      enddo
#  endif
# else
      do j=JstrR,JendR
        do i=IstrR,IendR
          ubclm(i,j)=???
          vbclm(i,j)=???
        enddo
      enddo
#  ifdef SOLVE3D
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            uclm(i,j,k)=???
            vclm(i,j,k)=???
          enddo
        enddo
      enddo
#  endif
# endif
      return
      end
#endif /* ANA_UCLIMA && UCLIMATOLOGY */
!
#if defined ANA_WWAVE && defined SG_BBL96
      subroutine ana_wwave_tile (Istr,Iend,Jstr,Jend)
!
!---------------------------------------------------------------------
!  This routine sets wind induced wave amplitude, direction
!  and period used in the bottom boundary layer formulation.
!---------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j
# include "param.h"
# include "bblm.h"
# include "grid.h"
# include "scalars.h"
!
# include "compute_auxiliary_bounds.h" 
!
!  Set wind induced wave amplitude (m), direction (radians) and
!  period (s) at RHO-points.
!
      do j=JstrR,JendR
        do i=IstrR,IendR
          Awave(i,j)=???
          Dwave(i,j)=???
          Pwave(i,j)=???
        enddo
      enddo
      return
      end
#endif /* ANA_WWAVE && SG_BBL96 */

