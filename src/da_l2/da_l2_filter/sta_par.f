      subroutine sta_par
      implicit none
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=588,  MMm=624,  N=66
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp
     &          , isalt
       parameter (itemp=1,
     &            isalt=2,
     &            NT=2
     &           )
      integer NS
      parameter (NS=5)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      integer nsta, nrpfsta
      logical ldefhis
      logical got_tini(NT)
      logical ldefsta
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , nsta, nrpfsta
     &                      , ldefhis
     &                      , got_tini
     &                      , ldefsta
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      real h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hinv(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real f(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fomn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_h/h /grid_hinv/hinv /grid_f/f /grid_fomn/fomn
      real angler(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_angler/angler
      real latr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real lonr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_latr/latr /grid_lonr/lonr
      real pm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_pm/pm    /metrics_pn/pn
     &       /metrics_omu/om_u /metrics_on_u/on_u
     &       /metrics_omv/om_v /metrics_on_v/on_v
      real dmde(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real dndx(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_dmde/dmde    /metrics_dndx/dndx
      real on_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmon_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pnom_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real grdscl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_on_p/on_p /metrics_om_p/om_p
     &       /metrics_on_r/on_r /metrics_om_r/om_r
     &       /metrics_pmon_u/pmon_u /metrics_pnom_v/pnom_v
     &                              /metrics_grdscl/grdscl
      real rmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /mask_r/rmask /mask_p/pmask
     &       /mask_u/umask /mask_v/vmask
      real visc2_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real visc2_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /mixing_visc2_r/visc2_r /mixing_visc2_p/visc2_p
      real diff2(0:Lm+1+padd_X,0:Mm+1+padd_E,NT)
      common /mixing_diff2/diff2
      real Akv(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      real Akt(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N,NT)
      common /mixing_Akv/Akv /mixing_Akt/Akt
      real bvf(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      common /mixing_bvf/ bvf
      integer kbl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hbl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real ustar(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /lmd_kpp_kbl/kbl     /lmd_kpp_hbl/hbl
     &       /lmd_kpp_ustar/ustar
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
      integer indxU, indxV, indxT
      parameter (indxU=5, indxV=6, indxT=7)
      integer indxS
      parameter (indxS=indxT+1)
      integer indxO, indxW, indxR, indxAkv, indxAkt
      parameter (indxO=indxT+NT, indxW=indxO+1, indxR=indxO+2,
     &                     indxAkv=indxR+1, indxAkt=indxAkv+1)
      integer indxAks
      parameter (indxAks=indxAkt+1)
      integer indxHbl
      parameter (indxHbl=indxAks+1)
      integer indxSSH
      parameter (indxSSH=indxAkt+3)
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
      integer indxSHFl, indxSWRad
      parameter (indxSHFl=indxSSH+3)
      integer indxSSFl
      parameter (indxSSFl=indxSHFl+1, indxSWRad=indxSHFl+2)
      integer indxSST, indxdQdSST
      parameter (indxSST=indxSWRad+1, indxdQdSST=indxSWRad+2)
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     &                u3dvar, v3dvar, p3dvar, w3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8)
      integer xi_rho,xi_u, eta_rho,eta_v
      parameter (xi_rho=LLm+2,  xi_u=xi_rho-1,
     &           eta_rho=MMm+2, eta_v=eta_rho-1)
      integer ncidfrc, ncidclm,  ntsms,   ntsrf,  ntssh,  ntsst
     &                        ,  nttclm(NT+1),    ntstf(NT+1)
     &                        ,  ntuclm
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV,   rstT(NT+1)
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT(NT+1),      hisR
     &      , hisO,    hisW,     hisAkv,  hisAkt, hisAks
     &                                          , hisHbl
     &      , ncidavg, nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT(NT+1),      avgR
     &      , avgO,    avgW,     avgAkv,  avgAkt, avgAks
     &                                          , avgHbl
     &      , ncidsta, nrecsta,  stadid,   stazid
     &      , staubid, stavbid,  nstation,ispos(NS),jspos(NS)
     &      , stauid,  stavid,   statid(NT+1),     starid
     &      , stawid,  staakvid, staaktid,         staaksid
     &      , stahblid
      logical wrthis(16+NT-2)
     &      , wrtavg(16+NT-2)
     &      , wsta(16+NT-2)
      common/incscrum/
     &        ncidfrc, ncidclm, ntsms,   ntsrf,   ntssh,   ntsst
     &                        ,  nttclm,          ntstf
     &                        ,  ntuclm
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV,   rstT
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT,            hisR
     &      , hisO,    hisW,     hisAkv,  hisAkt, hisAks
     &                                          , hisHbl
     &      , ncidavg,  nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT,            avgR
     &      , avgO,    avgW,     avgAkv,  avgAkt, avgAks
     &                                          , avgHbl
     &      , ncidsta, nrecsta,  stadid, stazid
     &      , staubid, stavbid,  nstation, ispos,  jspos
     &      , stauid,  stavid,   statid,           starid
     &      , stawid,  staakvid, staaktid,         staaksid
     &      , stahblid
     &      , wrthis
     &      , wrtavg
     &      , wsta
      character date_str*44, title*80
      character*80 ininame,    grdname,   hisname
     &         ,   rstname,    frcname,   usrname
     &                                ,   avgname
     &                      ,  staname,   sposnam
      character*42  vname(4,
     &                          39)
      common /cncscrum/       date_str,   title
     &         ,   ininame,    grdname,   hisname
     &         ,   rstname,    frcname,   usrname
     &                                ,   avgname
     &                      ,  staname,   sposnam
     &                      ,  vname
      integer iunit, icard, itrc, k, lstr, lenstr
      parameter (iunit=40)
      lstr=lenstr(sposnam)
      open(iunit,file=sposnam(1:lstr),form='formatted',status='old')
       write(stdout,'(/,A20,/)') ' STATION parameters:'
   1  continue
       read(iunit,*,err=97) icard
        if (icard.eq.99) goto 99
        if (icard.eq.1) then
          read(iunit,*,err=97) nstation
          if (NS.lt.nstation) then
            write(stdout,2) NS, nstation
   2        format(/,' INP_PAR - too small dimension parameter, NS: '
     &                   ,2i4,' change file  param.h and recompile.')
            may_day_flag=5
            return
          endif
           write(stdout,3) nstation
   3      format(4x,i6,2x,'nstation    Number of stations to write',
     &                                    ' out into stations file.')
        elseif (icard.eq.2) then
          read(iunit,*,err=97) wsta(indxU), wsta(indxV), wsta(indxW),
     &          wsta(indxO), wsta(indxUb), wsta(indxVb), wsta(indxZ)
           write(stdout,4) wsta(indxU), wsta(indxV),
     &                                    wsta(indxW), wsta(indxO)
   4      format(9x,l1,2x,'wstaU       ',
     &                 'Write out 3D U-momentum component (T/F).',/,
     &           9x,l1,2x,'wstaV       ',
     &                 'Write out 3D V-momentum component (T/F).',/,
     &           9x,l1,2x,'wstaW       ',
     &                    'Write out W-momentum component (T/F).',/,
     &           9x,l1,2x,'wstaO       ',
     &                   'Write out omega vertical velocity (T/F).')
           write(stdout,5) wsta(indxUb),
     &                             wsta(indxVb), wsta(indxZ)
   5      format(9x,l1,2x,'wstaUBAR    ',
     &                 'Write out 2D U-momentum component (T/F).',/,
     &           9x,l1,2x,'wstaVBAR    ',
     &                 'Write out 2D V-momentum component (T/F).',/,
     &           9x,l1,2x,'wstaZ       ',
     &                              'Write out free-surface (T/F).')
        elseif (icard.eq.3) then
          read(iunit,*,err=97) (wsta(itrc+indxT-1), itrc=1,NT)
          do itrc=1,NT
             write(stdout,6) wsta(itrc+indxT-1),
     &                  itrc,itrc
   6        format(9x,l1,2x,'wstaT(',i1,')    Write out tracer ',
     &                                              i1,' (T/F).')
          enddo
        elseif (icard.eq.4) then
          read(iunit,*,err=97) wsta(indxR), wsta(indxAkv)
     &                                    , wsta(indxAkt)
     &                                    , wsta(indxAks)
     &                                    , wsta(indxHbl)
           write(stdout,7) wsta(indxR),
     &                       wsta(indxAkv), wsta(indxAkt)
     &                                    , wsta(indxAks)
     &                                    , wsta(indxHbl)
   7      format(9x,l1,2x,'wstaRHO     ',
     &          'Write out density anomaly (T/F).'
     &          /9x,l1,2x,'wstaAKV     ',
     &          'Write out vertical viscosity coefficient (T/F).'
     &          /9x,l1,2x,'wstaAKT     ',
     &          'Write out vertical T-diffusion coefficient (T/F).'
     &          /,9x,l1,2x,'wstaAKS     ',
     &          'Write out vertical S-diffusion coefficient (T/F).'
     &          /9x,l1,2x,'wstaHBL      ',
     &          'Write out depth of mixed layer (T/F).'
     &                  )
        elseif (icard.eq.5) then
           write(stdout,*) ' '
          do k=1,nstation
            read(iunit,*,err=97) ispos(k), jspos(k)
            if (ispos(k).lt.2 .or. ispos(k).gt.Lm) then
               write(stdout,9) ' ISPOS = ', ispos(k)
              may_day_flag=5
              return
            endif
            if (jspos(k).lt.2 .or. jspos(k).gt.Mm) then
               write(stdout,9) ' JSPOS = ', jspos(k)
   9          format(/,' INP_PAR - out of range station index',
     &                                        ' position',a,i4)
              may_day_flag=5
              return
            endif
             write(stdout,10) k, ispos(k), jspos(k)
  10        format(12x,'(I,J) for station ',i3.3,':',2i6)
          enddo
        endif
      goto 1
  97   write(stdout,98) icard, sposnam(1:lstr)
  98  format(/,' STA_PAR - error while reading input card: ',i2,
     &       /,12x,'from stations file: ',a)
      may_day_flag=5
  99  continue
      return
      end
