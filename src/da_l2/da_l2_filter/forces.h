!  This is include file "forces.h"
!--------------------------------------------------------------------
!  SURFACE MOMENTUM FLUX (WIND STRESS):
!--------------------------------------------------------------------
!  sustr |  XI- and ETA-components of kinematic surface momentum flux
!  svstr |  (wind stresses) defined at horizontal U- and V-points.
!            dimensioned as [m^2/s^2].
!
      real sustr(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE sustr(BLOCK_PATTERN) BLOCK_CLAUSE
      real svstr(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE svstr(BLOCK_PATTERN) BLOCK_CLAUSE
      common /forces_sustr/sustr /forces_svstr/svstr
#ifndef ANA_SMFLUX
# if defined SMFLUX_DATA || defined ALL_DATA
!
!  tsms      Time of surface momentum stresses.
!
!  sustrg |  Two-time level gridded data for XI- and ETA-componets
!  svstrg |  of kinematic surface momentum flux (wind stess).
!
!  sustrp |  Two-time level point data for XI- and ETA-componets 
!  svstrp |  of kinematic surface momentum flux (wind stess).
!
      real sustrg(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE sustrg(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real svstrg(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE svstrg(BLOCK_PATTERN,*) BLOCK_CLAUSE
      common /smsdat_sustrg/sustrg /smsdat_svstrg/svstrg

      real sustrp(2), svstrp(2),  sms_time(2), sms_cycle, sms_scale 
      integer itsms,  sms_ncycle, sms_rec,     lsusgrd,   lsvsgrd, 
     &                            sms_tid,     susid,     svsid
      common /smsdat/
     &        sustrp, svstrp,     sms_time,    sms_cycle, sms_scale,
     &        itsms,  sms_ncycle, sms_rec,     lsusgrd,   lsvsgrd,
     &                            sms_tid,     susid,     svsid
#  undef SMFLUX_DATA
# endif /* SMFLUX_DATA */
#endif /* !ANA_SMFLUX */
!
!  BOTTOM MOMENTUM FLUX:
!--------------------------------------------------------------------
!  bustr |  XI- and ETA-components of kinematic bottom momentum flux
!  bvstr |  (drag) defined at horizontal U- and V-points [m^2/s^2].
!
      real bustr(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE bustr(BLOCK_PATTERN) BLOCK_CLAUSE
      real bvstr(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE bvstr(BLOCK_PATTERN) BLOCK_CLAUSE
      common /forces_bustr/bustr /forces_bvstr/bvstr
#ifndef ANA_BMFLUX
# if defined BMFLUX_DATA || defined ALL_DATA
!
!  tbms      Time of surface momentum stresses.
!
!  bustrg |  Two-time level gridded data for XI- and ETA-componets 
!  bvstrg |  of kinematic bottom momentum flux.
!
!  bustrp |  Two-time level point data for XI- and ETA-componets   
!  bvstrp |  of kinematic bottom momentum flux.
!
      real bustrg(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE bustrg(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real bvstrg(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE bvstrg(BLOCK_PATTERN,*) BLOCK_CLAUSE
      common /bmsdat_bustrg/bustrg /bmsdat_bvstrg/bvstrg

      real bms_tintrp(2), bustrp(2),    bvstrp(2), tbms(2),
     &        bmsclen,    bms_tstart,   bms_tend,  tsbms,   sclbms
      integer itbms,      bmstid,busid, bvsid,     tbmsindx
      logical bmscycle,   bms_onerec,   lbusgrd,   lbvsgrd
      common /bmsdat/
     &        bms_tintrp, bustrp,       bvstrp,    tbms,
     &        bmsclen,    bms_tstart,   bms_tend,  tsbms,   sclbms,
     &        itbms,      bmstid,busid, bvsid,     tbmsindx,
     &        bmscycle,   bms_onerec,   lbusgrd,   lbvsgrd

#  undef BMFLUX_DATA
# endif /* BMFLUX_DATA */
#endif /* !ANA_BMFLUX */
#ifdef SOLVE3D
!
!  SURFACE TRACER FLUXES: 
!--------------------------------------------------------------------
!  stflx   Kinematic surface fluxes of tracer type variables at
!          horizontal RHO-points. Physical dimensions [degC m/s] -
!          temperature; [PSU m/s] - salinity.
!
      real stflx(GLOBAL_2D_ARRAY,NT)
CSDISTRIBUTE_RESHAPE stflx(BLOCK_PATTERN,*) BLOCK_CLAUSE
      common /forces_stflx/stflx
# if !defined ANA_STFLUX || !defined ANA_SSFLUX
#  if defined STFLUX_DATA || defined ALL_DATA
!
!  stflxg   Two-time level surface tracer flux grided data.
!  stflxp   Two-time level surface tracer flux point  data.
!  tstflx   Time of surface tracer flux.
!
      real stflxg(GLOBAL_2D_ARRAY,2,NT)
CSDISTRIBUTE_RESHAPE stflxg(BLOCK_PATTERN,*,*) BLOCK_CLAUSE
      common /stfdat_stflxg/stflxg

      real stflxp(2,NT), stf_time(2,NT), stf_cycle(NT), stf_scale(NT)
      integer itstf(NT), stf_ncycle(NT), stf_rec(NT),   lstfgrd(NT),
     &                                   stf_tid(NT),   stf_id(NT)
      common /stfdat/ stflxp,  stf_time, stf_cycle,     stf_scale,
     &        itstf,     stf_ncycle,     stf_rec,       lstfgrd,
     &                                   stf_tid,       stf_id
#   undef STFLUX_DATA
#  endif /*  STFLUX_DATA */
# endif /* !ANA_STFLUX || !ANA_SSFLUX */
!
!  BOTTOM TRACER FLUXES:
!--------------------------------------------------------------------
!  btflx  Kinematic bottom fluxes of tracer type variables at
!         horizontal RHO-points. Physical dimensions [degC m/s] -
!         temperature; [PSU m/s] - salinity.
!
      real btflx(GLOBAL_2D_ARRAY,NT)
CSDISTRIBUTE_RESHAPE btflx(BLOCK_PATTERN,*) BLOCK_CLAUSE
      common /forces_btflx/btflx
# ifndef ANA_BTFLUX
#  if defined BTFLUX_DATA || defined ALL_DATA
!
!  btflxg   Two-time level bottom tracer flux grided data.
!  btflxp   Two-time level bottom tracer flux point data.
!  tbtflx   Time of bottom tracer flux.
!
      real btflxg (GLOBAL_2D_ARRAY,2,NT)
CSDISTRIBUTE_RESHAPE btflxg(BLOCK_PATTERN,*,*) BLOCK_CLAUSE 
      common /btfdat_btflxg/btflxg

      real sclbtf(NT), btf_tstart(NT),   btf_tend(NT),  btfclen(NT),
     &     tsbtf(NT),  btf_tintrp(2,NT), btflxp(2,NT),  tbtflx(2,NT)
      integer itbtf(NT),   btfid(NT),    btftid(NT),    tbtfindx(NT)
      logical lbtfgrd(NT), btfcycle(NT), btf_onerec(NT)
      common /btfdat/
     &        sclbtf,      btf_tstart,   btf_tend,      btfclen,
     &        tsbtf,       btf_tintrp,   btflxp,        tbtflx,
     &        itbtf,       btfid,        btftid,        tbtfindx,
     &        lbtfgrd,     btfcycle,     btf_onerec

#   undef BTFLUX_DATA
#  endif /* BTFLUX_DATA */
# endif /* !ANA_BTFLUX */
#ifdef QCORRECTION
!
!  HEAT FLUX CORRECTION
!--------------------------------------------------------------------
!  dqdt     Kinematic surface net heat flux sensitivity to SST [m/s].
!  sst      Current sea surface temperature [degree Celsius].
!
      real dqdt(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE  dqdt(BLOCK_PATTERN) BLOCK_CLAUSE
      real sst(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE   sst(BLOCK_PATTERN) BLOCK_CLAUSE
      common /forces_dqdt/dqdt /forces_sst/sst
#  ifndef ANA_SST
#   if defined SST_DATA || defined ALL_DATA
!
!  dqdtg |  Two-time-level grided data for net surface heat flux
!  sstg  |  sensitivity to SST grided data [Watts/m^2/Celsius] and
!              sea surface temperature [degree Celsius].
!  dqdtp |  Two-time-level point data for net surface heat flux
!  sstp  |  sensitivity to SST grided data [Watts/m^2/Celsius] and
!              sea surface temperature [degree Celsius].
!  tsst     Time of sea surface temperature data.
!
      real dqdtg(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE dqdtg(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real sstg(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE  sstg(BLOCK_PATTERN,*) BLOCK_CLAUSE
      common /sstdat_dqdtg/dqdtg /sstdat_sstg/sstg

      real sstp(2),  dqdtp(2),   sst_time(2), sst_cycle, scldqdt
      integer itsst, sst_ncycle, sst_rec,     sst_tid,   sst_id,
     &                           dqdt_id,     lsstgrd,   sstunused
      common /sstdat/
     &        sstp,  dqdtp,      sst_time,    sst_cycle, scldqdt,
     &        itsst, sst_ncycle, sst_rec,     sst_tid,   sst_id,
     &                           dqdt_id,     lsstgrd,   sstunused

#    undef SST_DATA
#   endif /* SST_DATA */
#  endif /* !ANA_SST */
# endif /* QCORRECTION */
!
!  SOLAR SHORT WAVE RADIATION FLUX.
!--------------------------------------------------------------------
!  srflx  Kinematic surface shortwave solar radiation flux
!         [degC m/s] at horizontal RHO-points
!
      real srflx(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE srflx(BLOCK_PATTERN) BLOCK_CLAUSE
      common /forces_srflx/srflx
# ifndef ANA_SRFLUX
#  if defined SRFLUX_DATA || defined ALL_DATA
!
!  srflxg | Two-time-level grided and point data for surface 
!  srflxp |      solar shortwave radiation flux grided data.
!  tsrflx   Time of solar shortwave radiation flux.
!
      real srflxg(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE srflxg(BLOCK_PATTERN,*) BLOCK_CLAUSE
      common /srfdat_srflxg/srflxg

      real srflxp(2),srf_time(2),srf_cycle, srf_scale
      integer itsrf, srf_ncycle, srf_rec,   lsrfgrd, srf_tid, srf_id 
      common /srfdat/
     &        srflxp,srf_time,   srf_cycle, srf_scale,
     &        itsrf, srf_ncycle, srf_rec,   lsrfgrd, srf_tid, srf_id

#   undef SRFLUX_DATA
#  endif /* SRFLUX_DATA */
# endif /* !ANA_SRFLUX */
# if defined SG_BBL96 && !defined ANA_WWAVE
#  if defined WWAVE_DATA || defined ALL_DATA
!
!  WIND INDUCED WAVES:
!--------------------------------------------------------------------
!  wwag  |  Two-time-level       | wave amplitude [m]
!  wwdg  |  gridded data         | wave direction [radians]
!  wwpg  |  for wind induced     ! wave period [s]
!
!  wwap  |  Two-time-level       | wave amplitude [m]
!  wwdp  |  point data           | wave direction [radians]
!  wwpp  |  for wind induced     ! wave period [s]
!
!  tww      Time of wind induced waves.
!
      real wwag(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE wwag(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real wwdg(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE wwdg(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real wwpg(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE wwpg(BLOCK_PATTERN,*) BLOCK_CLAUSE
      common /wwf_wwag/wwag /wwf_wwdg/wwdg /wwf_wwpg/wwpg

      real ww_tintrp(2), wwap(2), wwdp(2),  wwpp(2), tww(2), tsww,
     &        ww_tstart, ww_tend, sclwwa,   sclwwd,  sclwwp, wwclen
      integer itww,      twwindx, wwaid,    wwdid,   wwpid,  wwtid
      logical lwwgrd,    wwcycle, ww_onerec
      common /wwfdat/
     &        ww_tintrp, wwap,    wwdp,     wwpp,    tww,    tsww,
     &        ww_tstart, ww_tend, sclwwa,   sclwwd,  sclwwp, wwclen,
     &        itww,      twwindx, wwaid,    wwdid,   wwpid,  wwtid,
     &        lwwgrd,    wwcycle, ww_onerec

#   undef WWAVE_DATA
#  endif /* WWAVE_DATA */
# endif /* SG_BBL96 && !ANA_WWAVE */
#endif /* SOLVE3D */

