#include "cppdefs.h"


      subroutine check_switches1 (ierr)

!!!!!! WARNING: THIS IS A MACHINE GENERATED CODE, DO NOT EDIT! !!!!!!
!!!!!! This file needs to be updated only if new CPP-switches  !!!!!!
!!!!!! were introduced into "cppdefs.h".  NO ACTION IS NEEDED  !!!!!!
!!!!!! if changes in "cppdefs.h" are limited to activation or  !!!!!!
!!!!!! deactivation of previously known switches.              !!!!!!
!!!!!! To refresh this file compile and execute "cppcheck.F"   !!!!!!
!!!!!! as an independent program, or use commands              !!!!!!
!!!!!! "make checkdefs" or "make depend".                      !!!!!!
!!!!!! Number of Configuration Choices:                     14 !!!!!!
!!!!!! Total number of CPP-switches:                       114 !!!!!!

      implicit none
      integer ierr, is,ie, iexample
#include "param.h"
#include "strings.h"
#ifdef MPI
# include "scalars.h"
#endif
      MPI_master_only write(stdout,'(/1x,A/)')
     &      'Activated C-preprocessing Options:'
      do is=1,max_opt_size
        Coptions(is:is)=' '
      enddo
      iexample=0
      is=1
#ifdef BASIN
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'BASIN'
      ie=is + 4
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='BASIN'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef CANYON_A
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'CANYON_A'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='CANYON_A'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef CANYON_B
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'CANYON_B'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='CANYON_B'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAMEE_B
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'DAMEE_B'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAMEE_B'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAMEE_S
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'DAMEE_S'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAMEE_S'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef GRAV_ADJ
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'GRAV_ADJ'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='GRAV_ADJ'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef NJ_BIGHT
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'NJ_BIGHT'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='NJ_BIGHT'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef NPACIFIC
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'NPACIFIC'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='NPACIFIC'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OVERFLOW
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'OVERFLOW'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OVERFLOW'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef SEAMOUNT
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'SEAMOUNT'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SEAMOUNT'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef SOLITON
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'SOLITON'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SOLITON'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef TASMAN_SEA
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'TASMAN_SEA'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='TASMAN_SEA'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef UPWELLING
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'UPWELLING'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='UPWELLING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef USWEST
      iexample=iexample+1
      MPI_master_only write(stdout,'(10x,A)') 'USWEST'
      ie=is + 5
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='USWEST'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef SOLVE3D
      MPI_master_only write(stdout,'(10x,A)') 'SOLVE3D'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SOLVE3D'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef UV_ADV
      MPI_master_only write(stdout,'(10x,A)') 'UV_ADV'
      ie=is + 5
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='UV_ADV'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef UV_COR
      MPI_master_only write(stdout,'(10x,A)') 'UV_COR'
      ie=is + 5
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='UV_COR'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef UV_VIS4
      MPI_master_only write(stdout,'(10x,A)') 'UV_VIS4'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='UV_VIS4'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef MIX_GP_UV
      MPI_master_only write(stdout,'(10x,A)') 'MIX_GP_UV'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='MIX_GP_UV'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef SALINITY
      MPI_master_only write(stdout,'(10x,A)') 'SALINITY'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SALINITY'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef NONLIN_EOS
      MPI_master_only write(stdout,'(10x,A)') 'NONLIN_EOS'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='NONLIN_EOS'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef TS_DIF2
      MPI_master_only write(stdout,'(10x,A)') 'TS_DIF2'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='TS_DIF2'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef TS_DIF4
      MPI_master_only write(stdout,'(10x,A)') 'TS_DIF4'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='TS_DIF4'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef MIX_GP_TS
      MPI_master_only write(stdout,'(10x,A)') 'MIX_GP_TS'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='MIX_GP_TS'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef BODYFORCE
      MPI_master_only write(stdout,'(10x,A)') 'BODYFORCE'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='BODYFORCE'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_GRID
      MPI_master_only write(stdout,'(10x,A)') 'ANA_GRID'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_GRID'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_INITIAL
      MPI_master_only write(stdout,'(10x,A)') 'ANA_INITIAL'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_INITIAL'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_SMFLUX
      MPI_master_only write(stdout,'(10x,A)') 'ANA_SMFLUX'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_SMFLUX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_STFLUX
      MPI_master_only write(stdout,'(10x,A)') 'ANA_STFLUX'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_STFLUX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_BTFLUX
      MPI_master_only write(stdout,'(10x,A)') 'ANA_BTFLUX'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_BTFLUX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_VMIX
      MPI_master_only write(stdout,'(10x,A)') 'ANA_VMIX'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_VMIX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef UV_VIS2
      MPI_master_only write(stdout,'(10x,A)') 'UV_VIS2'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='UV_VIS2'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_EAST
      MPI_master_only write(stdout,'(10x,A)') 'OBC_EAST'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_EAST'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_WEST
      MPI_master_only write(stdout,'(10x,A)') 'OBC_WEST'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_WEST'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_NORTH
      MPI_master_only write(stdout,'(10x,A)') 'OBC_NORTH'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_NORTH'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_SOUTH
      MPI_master_only write(stdout,'(10x,A)') 'OBC_SOUTH'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_SOUTH'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef EW_PERIODIC
      MPI_master_only write(stdout,'(10x,A)') 'EW_PERIODIC'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='EW_PERIODIC'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef NS_PERIODIC
      MPI_master_only write(stdout,'(10x,A)') 'NS_PERIODIC'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='NS_PERIODIC'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_MEANRHO
      MPI_master_only write(stdout,'(10x,A)') 'ANA_MEANRHO'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_MEANRHO'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef STATIONS
      MPI_master_only write(stdout,'(10x,A)') 'STATIONS'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='STATIONS'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef TCLIMATOLOGY
      MPI_master_only write(stdout,'(10x,A)') 'TCLIMATOLOGY'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='TCLIMATOLOGY'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef TNUDGING
      MPI_master_only write(stdout,'(10x,A)') 'TNUDGING'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='TNUDGING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef QCORRECTION
      MPI_master_only write(stdout,'(10x,A)') 'QCORRECTION'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='QCORRECTION'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_SSFLUX
      MPI_master_only write(stdout,'(10x,A)') 'ANA_SSFLUX'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_SSFLUX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_SST
      MPI_master_only write(stdout,'(10x,A)') 'ANA_SST'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_SST'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_SRFLUX
      MPI_master_only write(stdout,'(10x,A)') 'ANA_SRFLUX'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_SRFLUX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_BSFLUX
      MPI_master_only write(stdout,'(10x,A)') 'ANA_BSFLUX'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_BSFLUX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef VIS_GRID
      MPI_master_only write(stdout,'(10x,A)') 'VIS_GRID'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='VIS_GRID'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef MIX_S_UV
      MPI_master_only write(stdout,'(10x,A)') 'MIX_S_UV'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='MIX_S_UV'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DIF_GRID
      MPI_master_only write(stdout,'(10x,A)') 'DIF_GRID'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DIF_GRID'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef MIX_S_TS
      MPI_master_only write(stdout,'(10x,A)') 'MIX_S_TS'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='MIX_S_TS'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef MIX_EN_TS
      MPI_master_only write(stdout,'(10x,A)') 'MIX_EN_TS'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='MIX_EN_TS'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef BVF_MIXING
      MPI_master_only write(stdout,'(10x,A)') 'BVF_MIXING'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='BVF_MIXING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef PP_MIXING
      MPI_master_only write(stdout,'(10x,A)') 'PP_MIXING'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='PP_MIXING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef LMD_MIXING
      MPI_master_only write(stdout,'(10x,A)') 'LMD_MIXING'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='LMD_MIXING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef LMD_RIMIX
      MPI_master_only write(stdout,'(10x,A)') 'LMD_RIMIX'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='LMD_RIMIX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef LMD_CONVEC
      MPI_master_only write(stdout,'(10x,A)') 'LMD_CONVEC'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='LMD_CONVEC'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef LMD_KPP
      MPI_master_only write(stdout,'(10x,A)') 'LMD_KPP'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='LMD_KPP'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef CURVGRID
      MPI_master_only write(stdout,'(10x,A)') 'CURVGRID'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='CURVGRID'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef SPHERICAL
      MPI_master_only write(stdout,'(10x,A)') 'SPHERICAL'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SPHERICAL'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef MASKING
      MPI_master_only write(stdout,'(10x,A)') 'MASKING'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='MASKING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef EASTERN_WALL
      MPI_master_only write(stdout,'(10x,A)') 'EASTERN_WALL'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='EASTERN_WALL'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef WESTERN_WALL
      MPI_master_only write(stdout,'(10x,A)') 'WESTERN_WALL'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='WESTERN_WALL'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef SOUTHERN_WALL
      MPI_master_only write(stdout,'(10x,A)') 'SOUTHERN_WALL'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SOUTHERN_WALL'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef NORTHERN_WALL
      MPI_master_only write(stdout,'(10x,A)') 'NORTHERN_WALL'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='NORTHERN_WALL'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_FSORLANSKI
      MPI_master_only write(stdout,'(10x,A)') 'OBC_FSORLANSKI'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_FSORLANSKI'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_FSPRESCRIBE
      MPI_master_only write(stdout,'(10x,A)') 'OBC_FSPRESCRIBE'
      ie=is +14
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_FSPRESCRIBE'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_M2ORLANSKI
      MPI_master_only write(stdout,'(10x,A)') 'OBC_M2ORLANSKI'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_M2ORLANSKI'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_M2PRESCRIBE
      MPI_master_only write(stdout,'(10x,A)') 'OBC_M2PRESCRIBE'
      ie=is +14
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_M2PRESCRIBE'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_M3ORLANSKI
      MPI_master_only write(stdout,'(10x,A)') 'OBC_M3ORLANSKI'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_M3ORLANSKI'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_M3PRESCRIBE
      MPI_master_only write(stdout,'(10x,A)') 'OBC_M3PRESCRIBE'
      ie=is +14
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_M3PRESCRIBE'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_TCLIMA
      MPI_master_only write(stdout,'(10x,A)') 'OBC_TCLIMA'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_TCLIMA'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_TORLANSKI
      MPI_master_only write(stdout,'(10x,A)') 'OBC_TORLANSKI'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_TORLANSKI'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_TPRESCRIBE
      MPI_master_only write(stdout,'(10x,A)') 'OBC_TPRESCRIBE'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_TPRESCRIBE'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef LMD_DDMIX
      MPI_master_only write(stdout,'(10x,A)') 'LMD_DDMIX'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='LMD_DDMIX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_TCLIMA
      MPI_master_only write(stdout,'(10x,A)') 'ANA_TCLIMA'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_TCLIMA'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef SMOLARKIEWICZ
      MPI_master_only write(stdout,'(10x,A)') 'SMOLARKIEWICZ'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SMOLARKIEWICZ'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef AVERAGES
      MPI_master_only write(stdout,'(10x,A)') 'AVERAGES'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='AVERAGES'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_INFLOW
      MPI_master_only write(stdout,'(10x,A)') 'OBC_INFLOW'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_INFLOW'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ZNUDGING
      MPI_master_only write(stdout,'(10x,A)') 'ZNUDGING'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ZNUDGING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_VOLCONS
      MPI_master_only write(stdout,'(10x,A)') 'OBC_VOLCONS'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_VOLCONS'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef LMD_VMIX
      MPI_master_only write(stdout,'(10x,A)') 'LMD_VMIX'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='LMD_VMIX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OPENMP
      MPI_master_only write(stdout,'(10x,A)') 'OPENMP'
      ie=is + 5
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OPENMP'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef UCLIMATOLOGY
      MPI_master_only write(stdout,'(10x,A)') 'UCLIMATOLOGY'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='UCLIMATOLOGY'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef M3NUDGING
      MPI_master_only write(stdout,'(10x,A)') 'M3NUDGING'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='M3NUDGING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef M2NUDGING
      MPI_master_only write(stdout,'(10x,A)') 'M2NUDGING'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='M2NUDGING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef SPONGE
      MPI_master_only write(stdout,'(10x,A)') 'SPONGE'
      ie=is + 5
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SPONGE'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef SFLX_CORR
      MPI_master_only write(stdout,'(10x,A)') 'SFLX_CORR'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SFLX_CORR'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ROBUST_DIAG
      MPI_master_only write(stdout,'(10x,A)') 'ROBUST_DIAG'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ROBUST_DIAG'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ANA_UCLIMA
      MPI_master_only write(stdout,'(10x,A)') 'ANA_UCLIMA'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_UCLIMA'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef CLIMAT_TS_MIXH
      MPI_master_only write(stdout,'(10x,A)') 'CLIMAT_TS_MIXH'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='CLIMAT_TS_MIXH'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef LMD_NONLOCAL
      MPI_master_only write(stdout,'(10x,A)') 'LMD_NONLOCAL'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='LMD_NONLOCAL'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_M2FLATHER
      MPI_master_only write(stdout,'(10x,A)') 'OBC_M2FLATHER'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_M2FLATHER'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_TSPECIFIED
      MPI_master_only write(stdout,'(10x,A)') 'OBC_TSPECIFIED'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_TSPECIFIED'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_M2SPECIFIED
      MPI_master_only write(stdout,'(10x,A)') 'OBC_M2SPECIFIED'
      ie=is +14
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_M2SPECIFIED'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef OBC_M3SPECIFIED
      MPI_master_only write(stdout,'(10x,A)') 'OBC_M3SPECIFIED'
      ie=is +14
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_M3SPECIFIED'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef BIOLOGY
      MPI_master_only write(stdout,'(10x,A)') 'BIOLOGY'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='BIOLOGY'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_GEOS_STRONG
      MPI_master_only write(stdout,'(10x,A)') 'DAS_GEOS_STRONG'
      ie=is +14
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_GEOS_STRONG'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_GEOS_WEAK
      MPI_master_only write(stdout,'(10x,A)') 'DAS_GEOS_WEAK'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_GEOS_WEAK'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_HYDRO_STRONG
      MPI_master_only write(stdout,'(10x,A)') 'DAS_HYDRO_STRONG'
      ie=is +15
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_HYDRO_STRONG'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_HYDRO_WEAK
      MPI_master_only write(stdout,'(10x,A)') 'DAS_HYDRO_WEAK'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_HYDRO_WEAK'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_BVAR_CORR
      MPI_master_only write(stdout,'(10x,A)') 'DAS_BVAR_CORR'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_BVAR_CORR'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_ANA_BVAR
      MPI_master_only write(stdout,'(10x,A)') 'DAS_ANA_BVAR'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_ANA_BVAR'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_MCSST
      MPI_master_only write(stdout,'(10x,A)') 'DAS_MCSST'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_MCSST'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_GOES_SST
      MPI_master_only write(stdout,'(10x,A)') 'DAS_GOES_SST'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_GOES_SST'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_AMSR_SST
      MPI_master_only write(stdout,'(10x,A)') 'DAS_AMSR_SST'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_AMSR_SST'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_TMISST
      MPI_master_only write(stdout,'(10x,A)') 'DAS_TMISST'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_TMISST'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_JASONSSH
      MPI_master_only write(stdout,'(10x,A)') 'DAS_JASONSSH'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_JASONSSH'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_TPSSH
      MPI_master_only write(stdout,'(10x,A)') 'DAS_TPSSH'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_TPSSH'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_ERS2SSH
      MPI_master_only write(stdout,'(10x,A)') 'DAS_ERS2SSH'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_ERS2SSH'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_GFOSSH
      MPI_master_only write(stdout,'(10x,A)') 'DAS_GFOSSH'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_GFOSSH'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_NPSFLIGHT
      MPI_master_only write(stdout,'(10x,A)') 'DAS_NPSFLIGHT'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_NPSFLIGHT'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_MOORING
      MPI_master_only write(stdout,'(10x,A)') 'DAS_MOORING'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_MOORING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef DAS_CURRENT_UV
      MPI_master_only write(stdout,'(10x,A)') 'DAS_CURRENT_UV'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_CURRENT_UV'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
      MPI_master_only write(stdout,'(/)')
      if (iexample.eq.0) then
        MPI_master_only write(stdout,'(1x,A)')
     & 'ERROR in "cppdefs.h": no configuration is specified.'
        ierr=ierr+1
      elseif (iexample.gt.1) then
        MPI_master_only write(stdout,'(1x,A)')
     & 'ERROR: more than one configuration in "cppdefs.h".'
        ierr=ierr+1
      endif
      return
  99  MPI_master_only write(stdout,'(/1x,A,A/14x,A)')
     &  'CHECKDEFS -- ERROR: Unsufficient size of string Coptions',
     &  'in file "strings.h".', 'Increase the size it and recompile.'
      ierr=ierr+1
      return
      end
