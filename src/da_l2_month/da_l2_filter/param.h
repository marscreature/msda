! Dimensions of Physical Grid and array dimensions: 
! =========== == ======== ==== === ===== ============
! LLm,MMm  Number of the internal points of the PHYSICAL grid.
!          in the XI- and ETA-directions [physical side boundary
!          points and peroodic ghost points (if any) are excluded].
!
! Lm,Mm    Number of the internal points [see above] of array
!          covering a Message Passing subdomain. In the case when
!          no Message Passing partitioning is used, these two are
!          the same as LLm,MMm. 
!
! N        Number of vertical levels.
!
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
# if defined BASIN
     &               LLm=180, MMm=140, N=12
# elif defined CANYON_A
     &               LLm=65,  MMm=48,  N=10
# elif defined CANYON_B
     &               LLm=66,  MMm=48,  N=12
# elif defined DAMEE_S
c**  &               LLm=192, MMm=96,  N=20
     &               LLm=113, MMm=58,  N=20
# elif defined DAMEE_B
     &               LLm=128, MMm=128, N=20
c**  &               LLm=256, MMm=256, N=20
# elif defined GRAV_ADJ
     &               LLm=128, MMm=4,   N=20
# elif defined NJ_BIGHT
c*   &               LLm=98,  MMm=206, N=12
     &               LLm=24,  MMm=34,  N=10
# elif defined NPACIFIC
     &               LLm=174, MMm=94,  N=20
# elif defined OVERFLOW
     &               LLm=4,   MMm=128, N=20
# elif defined SEAMOUNT
     &               LLm=48,  MMm=48,  N=11
c*   &               LLm=97,  MMm=96,  N=20
# elif defined SHELFRONT
     &               LLm=4,   MMm=40,  N=12 
# elif defined SOLITON
     &               LLm=96,  MMm=32,  N=1
c*   &               LLm=192, MMm=64,  N=1
c*   &               LLm=384, MMm=128, N=1
# elif defined TASMAN_SEA
     &               LLm=128, MMm=128, N=4
# elif defined UPWELLING
c     &               LLm=10,  MMm=12,  N=3
     &               LLm=16,  MMm=64,  N=16
c*   &               LLm=16,  MMm=128, N=32
# elif defined USWEST
c*     &               LLm=384,  MMm=256,  N=40 ! SCB
     &               LLm=588,  MMm=624,  N=66 ! SWOT CAL
# else
     &                LLm=??, MMm=??, N=??
# endif
     &                                      )
      

!
! Domain subdivision parameters:
! ====== =========== ===========
! NPP            Maximum allowed number of parallel threads;
! NSUB_X,NSUB_E  Number of SHARED memory subdomains in XI- and
!                                                ETA-directions;
! NNODES        Total number of MPI processes (nodes);
! NP_XI,NP_ETA  Number of MPI subdomains in XI- and ETA-directions;
!
      integer NSUB_X, NSUB_E, NPP
# ifdef MPI
      integer NP_XI, NP_ETA, NNODES
      parameter (NP_XI=2, NP_ETA=2,  NNODES=NP_XI*NP_ETA,
     &                               Lm=LLm/NP_XI, Mm=MMm/NP_ETA)
      parameter (NSUB_X=1, NSUB_E=1, NPP=1)
# else
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
# endif
!
! Number of tracers and tracer identification indices:
! ====== == ======= === ====== ============== ========
!
# ifdef SOLVE3D
      integer NT, itemp
#  ifdef SALINITY
     &          , isalt
#  endif
#  ifdef BIOLOGY
     &          , iNO3_, iNH4_, iDet_, iPhyt, iZoo_
#  endif
       parameter (itemp=1,
#  ifdef SALINITY 
     &            isalt=2,
#   ifdef BIOLOGY
     &            NT=7, iNO3_=3, iNH4_=4, iDet_=5, iPhyt=6, iZoo_=7
#   else
     &            NT=2
#   endif
#  else
#   ifdef BIOLOGY
     &            NT=6, iNO3_=2, iNH4_=3, iDet_=4, iPhyt=5, iZoo_=6
#   else
     &            NT=1
#   endif
#  endif
     &           ) 
# endif /*SOLVE3D */
# ifdef STATIONS
      integer NS           ! Number of output stations (if any).
      parameter (NS=5)     ! ====== == ====== ======== === =====
# endif
!
! Derived dimension parameters.
!
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
!
! Number maximum of weights for the barotropic mode
!
      integer NWEIGHT
      parameter (NWEIGHT=137)
