      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
!
! minimization lbfgs
!
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
#if !defined DAS_GEOS_STRONG 
     &        +NDAS*((Lm+1)*(Mm+2)+(Lm+2)*(Mm+1))
#endif
#if !defined DAS_HYDRO_STRONG
     &        +(Lm+2)*(Mm+2)
#endif
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
!
! in-situ observation
!
      integer max_sio, max_whoi, max_flight, max_prf
      PARAMETER(max_sio=64,max_whoi=192,max_flight=1500,
     &         max_prf=NDAS)
      integer max_moor
      PARAMETER(max_moor=68)
!
! inversion of observational error variance
!
      real sio_ot,sio_os,whoi_ot,whoi_os,moor_ot,moor_os
      PARAMETER( sio_ot=1.0/(1.5*1.5),sio_os=1.0/(0.3*0.3),
     &           whoi_ot=1.0/(1.5*1.5),whoi_os=1.0/(0.3*0.3),
     &           moor_ot=1.0/(1.8*1.8),moor_os=1.0/(0.25*0.25)
     &         )

      real regp                !  Regular parameter                                                                                
      PARAMETER( regp=1./(10.*10.) ) 
!!!!!!!!!!!!!!!!!!  Lower resolution
      integer nratio
      PARAMETER( nratio=3 ) 
