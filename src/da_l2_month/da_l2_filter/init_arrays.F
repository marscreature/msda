#include "cppdefs.h"

      subroutine init_arrays (tile)
      implicit none
      integer tile
#include "param.h"
#include "compute_tile_bounds.h"
      call init_arrays_tile (Istr,Iend,Jstr,Jend)
      return
      end

      subroutine init_arrays_tile (Istr,Iend,Jstr,Jend)
!
! Initialize (first touch) globally accessable arrays. Most of them
! are assigned to zeros, vertical mixing coefficients are assinged
! to their background values. These will remain unchenged if no
! vertical mixing scheme is applied. Because of the "first touch"
! default data distribution policy, this operation actually performs
! distribution of the shared arrays accross the cluster, unless
! another distribution policy is specified to override the default.
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k,itrc, omp_get_thread_num
#define ALL_DATA
#include "param.h"
#include "grid.h"
#include "ocean2d.h"
#include "ocean3d.h"
#include "coupling.h"
#include "averages.h"
#include "forces.h"
#include "mixing.h"
#include "climat.h"
#include "scalars.h"
#undef ALL_DATA
      real init    !!!!  0xFFFA5A5A ==> NaN

      parameter (init=0.)

# ifdef MPI
      include 'mpif.h'
      integer ierr
# endif
!
# include "compute_extended_bounds.h"
!
#ifdef NOT_
# ifdef MPI
      i=mynode
      do j=0,mynode
        call MPI_Barrier (MPI_COMM_WORLD, ierr)
      enddo
# else
      i=omp_get_thread_num()
# endif
      write(*,*)
      write(*,'(I4,2(6x,A6,I3,3x,A6,I3))')i, 'Istr =',Istr,
     &         'Iend =',Iend, 'Jstr =',Jstr, 'Jend =',Jend
      write(*,'(4x2(6x,A6,I3,3x,A6,I3))')    'IstrR=',IstrR,
     &         'IendR=',IendR,'JstrR=',JstrR,'JendR=',JendR
      write(*,*)
# ifdef MPI
      do j=mynode+1,NNODES
         call MPI_Barrier (MPI_COMM_WORLD, ierr)
      enddo
# endif
#endif
!
!  Initialize 2-D primitive variables.
!
      do j=JstrR,JendR
        do i=IstrR,IendR
          zeta(i,j)=0. ! init
!-
          ubar(i,j)=init

          vbar(i,j)=init
#ifdef AVERAGES
          zeta_avg(i,j)=init 
          ubar_avg(i,j)=init
          vbar_avg(i,j)=init
#endif
          rzeta_bak(i,j)=init
          rubar_bak(i,j)=init
          rvbar_bak(i,j)=init
#ifdef SOLVE3D
          rufrc(i,j)=init
          rufrc(i,j)=init

          Zt_avg1(i,j)=0. !init
          DU_avg1(i,j,1)=0. !init
          DV_avg1(i,j,1)=0. !init
          DU_avg1(i,j,2)=0. !init
          DV_avg1(i,j,2)=0. !init
          DU_avg2(i,j)=0. !init
          DV_avg2(i,j)=0. !init
#endif
        enddo
      enddo
#ifdef SOLVE3D
!
!  Initialize 3-D primitive variables.
!

      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            u(i,j,k)=init

            v(i,j,k)=init

            rho(i,j,k) =init
# ifdef ANA_MEANRHO
            rhobar(i,j,k)=init
# endif
# ifdef AVERAGES
            rho_avg(i,j,k)=init
            u_avg(i,j,k)=init
            v_avg(i,j,k)=init
            w_avg(i,j,k)=init
# endif
          enddo
        enddo
      enddo
      do k=0,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            w(i,j,k)=init
          enddo
        enddo
      enddo
      do itrc=1,NT
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              t(i,j,k,itrc)=init
# ifdef AVERAGES
              t_avg(i,j,k,itrc)=init
# endif
            enddo
          enddo
        enddo
      enddo
#endif /* SOLVE3D */
!
!  Initialize forcing arrays (see "forces.h").
!
      do j=JstrR,JendR
        do i=IstrR,IendR
          sustr(i,j)=init
          svstr(i,j)=init
#ifndef ANA_SMFLUX
          sustrg(i,j,1)=init
          svstrg(i,j,1)=init
          sustrg(i,j,2)=init
          svstrg(i,j,2)=init
#endif
          bustr(i,j)=init
          bvstr(i,j)=init
#ifndef ANA_BMFLUX
          bustrg(i,j,1)=init
          bvstrg(i,j,1)=init
          bustrg(i,j,2)=init
          bvstrg(i,j,2)=init
#endif
        enddo
      enddo
#ifdef SOLVE3D
      do itrc=1,NT
        do j=JstrR,JendR
          do i=IstrR,IendR
            stflx(i,j,itrc)=init
# if !defined ANA_STFLUX || !defined ANA_SSFLUX
            stflxg(i,j,1,itrc)=init
            stflxg(i,j,2,itrc)=init
# endif
            btflx(i,j,itrc)=init
# ifndef ANA_BTFLUX
            btflxg(i,j,1,itrc)=init
            btflxg(i,j,2,itrc)=init
# endif
          enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
# ifdef QCORRECTION
          dqdt(i,j)=init
          sst (i,j)=init
#  ifndef ANA_SST
          dqdtg(i,j,1)=init
          sstg (i,j,1)=init
          dqdtg(i,j,2)=init
          sstg (i,j,2)=init
#  endif
# endif
          srflx(i,j)=init
# ifndef ANA_SRFLUX
          srflxg(i,j,1)=init
          srflxg(i,j,2)=init
# endif
# if defined SG_BBL96 && !defined ANA_WWAVE
          wwag(i,j,1)=init
          wwdg(i,j,1)=init
          wwpg(i,j,1)=init
          wwag(i,j,2)=init
          wwdg(i,j,2)=init
          wwpg(i,j,2)=init
# endif
        enddo
      enddo
#endif /* SOLVE3D */
!
! Initialize climatology arrays (see "climat.h").
!
#if defined ZNUDGING
      do j=JstrR,JendR
        do i=IstrR,IendR
          Znudgcof(i,j)=init
          ssh(i,j)=init
# ifndef ANA_SSH
          sshg(i,j,1)=init
          sshg(i,j,2)=init
# endif
        enddo
      enddo
#endif
#if defined TNUDGING && defined TCLIMATOLOGY
      do itrc=1,NT
        do j=JstrR,JendR
          do i=IstrR,IendR
            Tnudgcof(i,j,itrc)=init
          enddo
        enddo
      enddo
#endif
#ifdef TCLIMATOLOGY
# ifndef ANA_TCLIMA
      do itrc=1,NT
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              tclm(i,j,k,itrc)=init
              tclima(i,j,k,1,itrc)=init
              tclima(i,j,k,2,itrc)=init
            enddo
          enddo
        enddo
      enddo
# endif
#endif
#ifdef UCLIMATOLOGY
# ifndef ANA_UCLIMA
      do j=JstrR,JendR
        do i=IstrR,IendR
          ubclm(i,j)=init
          vbclm(i,j)=init
          ubclima(i,j,1)=init
          ubclima(i,j,2)=init
          vbclima(i,j,1)=init
          vbclima(i,j,2)=init
        enddo
      enddo
#  ifdef SOLVE3D
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            uclm(i,j,k)=init
            vclm(i,j,k)=init
            uclima(i,j,k,1)=init
            uclima(i,j,k,2)=init
            vclima(i,j,k,1)=init
            vclima(i,j,k,2)=init
          enddo
        enddo
      enddo
#  endif
# endif
#endif
!
! Set variable horizontal viscosities and tracer diffusion
! coefficients (see "mixing.h") to their background values.
!
#ifdef UV_VIS2
        do j=JstrR,JendR
          do i=IstrR,IendR
            visc2_r(i,j)=visc2
            visc2_p(i,j)=visc2
          enddo
        enddo
#endif
#ifdef UV_VIS4
        do j=JstrR,JendR
          do i=IstrR,IendR
            visc4_r(i,j)=visc4
            visc4_p(i,j)=visc4
          enddo
        enddo
#endif
#ifdef TS_DIF2
        do itrc=1,NT
          do j=JstrR,JendR
            do i=IstrR,IendR
              diff2(i,j,itrc)=tnu2(itrc)
            enddo
          enddo
        enddo
#endif
#ifdef TS_DIF4
        do itrc=1,NT
          do j=JstrR,JendR
            do i=IstrR,IendR
              diff4(i,j,itrc)=tnu4(itrc)
            enddo
          enddo
        enddo
#endif
#ifdef SOLVE3D
!
!  Initialize vertical mixing coefficients (see "mixing.h") to their
!  background values. If no vertical closure scheme is selected, the
!  vertical mixing coefficients are those specified by the background
!  values.
!
      do k=0,N
        do j=JstrR,JendR
          do i=IstrR,IendR
# if !defined LMD_MIXING && !defined BVF_MIXING\
  && !defined MY2_MIXING && !defined MY25_MIXING\
                         && !defined PP_MIXING
            Akv(i,j,k)=Akv_bak
# else
            Akv(i,j,k)=init
# endif
# if defined MIX_EN_TS || defined MIX_EN_UV
            rhosx(i,j,k)=init
            rhose(i,j,k)=init
# endif
# if defined BVF_MIXING || defined LMD_MIXING  || defined LMD_KPP \
  || defined MY2_MIXING || defined MY25_MIXING || defined PP_MIXING
            bvf(i,j,k)=init
# endif
          enddo
        enddo
        do itrc=1,NT
          do j=JstrR,JendR
            do i=IstrR,IendR
# if !defined LMD_MIXING && !defined BVF_MIXING\
  && !defined MY2_MIXING && !defined MY25_MIXING\
                         && !defined PP_MIXING
              Akt(i,j,k,itrc)=Akt_bak(itrc)
# else
              Akt(i,j,k,itrc)=init
# endif
            enddo
          enddo
        enddo
      enddo

# if defined LMD_KPP && defined LMD_NONLOCAL
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            ghats(i,j,k)=init
          enddo
        enddo
      enddo
# endif
# ifdef LMD_KPP
!
!  Initialize depth of planetary boundary layer.
!
      do j=JstrR,JendR
        do i=IstrR,IendR
          hbl(i,j)=0.  ! init
        enddo
      enddo
# endif /* LMD_KPP */
#endif /* SOLVE3D */
      return
      end
