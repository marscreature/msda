/* This is include file "das_covar.h". 
  --------------------------------------------
*/
! z-coord
      real bz_das(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE bz_das(BLOCK_PATTERN) BLOCK_CLAUSE
!if !defined DAS_GEOS_STRONG & !defined DAS_GEOS_WEAK
      real bu_das(GLOBAL_2D_ARRAY,ndas)
CSDISTRIBUTE_RESHAPE bu_ads(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real bv_das(GLOBAL_2D_ARRAY,ndas)
!endif
CSDISTRIBUTE_RESHAPE bv_das(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real bt_das(GLOBAL_2D_ARRAY,ndas,NT)
CSDISTRIBUTE_RESHAPE bt_das(BLOCK_PATTERN,*,*) BLOCK_CLAUSE
      common 
!if !defined DAS_GEOS_STRONG & !defined DAS_GEOS_WEAK
     &     /ocean_bu_das/bu_das /ocean_bv_das/bv_das
!endif
     &     /ocean_bt_das/bt_das /ocean_bz_das/bz_das
!
!ifdef DAS_BVAR_CORR
      real czE_das(GLOBAL_1D_ETA,GLOBAL_1D_ETA)
CSDISTRIBUTE_RESHAPE czE_das(block,block) BLOCK_CLAUSE
      real czX_das(GLOBAL_1D_XI,GLOBAL_1D_XI)
CSDISTRIBUTE_RESHAPE czX_das(block) BLOCK_CLAUSE
! if !defined DAS_GEOS_STRONG & !defined DAS_GEOS_WEAK
      real cuE_das(GLOBAL_1D_ETA,GLOBAL_1D_ETA)
CSDISTRIBUTE_RESHAPE cuE_ads(block) BLOCK_CLAUSE
      real cuX_das(GLOBAL_1D_XI,GLOBAL_1D_XI)
CSDISTRIBUTE_RESHAPE cuX_ads(block) BLOCK_CLAUSE
      real cuZ_das(NDAS,NDAS)
CSDISTRIBUTE_RESHAPE cuZ_ads(block) BLOCK_CLAUSE
      real cvE_das(GLOBAL_1D_ETA,GLOBAL_1D_ETA)
CSDISTRIBUTE_RESHAPE cvE_ads(block) BLOCK_CLAUSE
      real cvX_das(GLOBAL_1D_XI,GLOBAL_1D_XI)
CSDISTRIBUTE_RESHAPE cvX_ads(block) BLOCK_CLAUSE
      real cvZ_das(NDAS,NDAS)
CSDISTRIBUTE_RESHAPE cvZ_ads(block) BLOCK_CLAUSE
! endif
      real ctE_das(GLOBAL_1D_ETA,GLOBAL_1D_ETA,NT)
CSDISTRIBUTE_RESHAPE ctE_ads(block,*) BLOCK_CLAUSE
      real ctX_das(GLOBAL_1D_XI,GLOBAL_1D_XI,NT)
CSDISTRIBUTE_RESHAPE ctX_ads(block,*) BLOCK_CLAUSE
      real ctZ_das(ndas,ndas,NT)
CSDISTRIBUTE_RESHAPE ctZ_ads(block,*,*) BLOCK_CLAUSE
      common /ocean_cze_das/czE_das /ocean_czX_das/czX_das
! if !defined DAS_GEOS_STRONG & !defined DAS_GEOS_WEAK
     &       /ocean_cue_das/cuE_das /ocean_cuX_das/cuX_das
     &       /ocean_cuz_das/cuZ_das
     &       /ocean_cve_das/cvE_das /ocean_cvX_das/cvX_das
     &       /ocean_cvz_das/cvZ_das
! endif
     &       /ocean_cte_das/ctE_das /ocean_ctX_das/ctX_das
     &       /ocean_ctz_das/ctZ_das
!
! variance and correlation files
      character*118 file_corr
      common /corr_file/file_corr
      character*118 file_corr_vert
      common /corr_file_vert/file_corr_vert
      character*118 file_bvar
      common /bvar_file_vert/file_bvar
!endif
