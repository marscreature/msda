/* This is include file "ocean2d.h".
--------------------------------------------------------------------
 zeta,rheta     Free surface elevation [m] and its time tendency;
 ubar,rubar     Vertically integrated  2D velocity components in 
 vbar,rvbar     XI- and ETA-directions and their time tendencies;
*/
      real zeta(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE zeta(BLOCK_PATTERN) BLOCK_CLAUSE
      real ubar(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE ubar(BLOCK_PATTERN) BLOCK_CLAUSE
      real vbar(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE vbar(BLOCK_PATTERN) BLOCK_CLAUSE
      real rzeta_bak(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE rzeta_bak(BLOCK_PATTERN) BLOCK_CLAUSE
      real rubar_bak(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE rubar_bak(BLOCK_PATTERN) BLOCK_CLAUSE
      real rvbar_bak(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE rvbar_bak(BLOCK_PATTERN) BLOCK_CLAUSE
      common /ocean_zeta/zeta  /rhs_rzeta/rzeta_bak
     &       /ocean_ubar/ubar  /rhs_rubar/rubar_bak
     &       /ocean_vbar/vbar  /rhs_rvbar/rvbar_bak

/*
Arrays zeta,ubar,vbar need to be communicated.

rzeta_bak,rubar_bak,rvbar_bak are used only locally.
*/


