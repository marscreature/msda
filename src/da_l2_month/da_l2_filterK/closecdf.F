#include "cppdefs.h"
                                !
      subroutine closecdf       ! Close output netCDF files. 
                                !
      implicit none
#include "param.h"
#include "scalars.h"
#include "ncscrum.h"
      include 'netcdf.inc'
      integer ierr 
      if (ncidrst.ne.-1) ierr=nf_close(ncidrst)
      if (ncidhis.ne.-1) ierr=nf_close(ncidhis)
#ifdef AVERAGES
      if (ncidavg.ne.-1) ierr=nf_close(ncidavg)
#endif
#ifdef FLOATS
      if (ncidflt.ne.-1) ierr=nf_close(ncidflt)
#endif

      write(stdout,'(/1x,2A,I5,3(/8x,A,I5)/)') 'MAIN - '
     &  ,'number of records written into history  file(s):', nrechis
     &  ,'number of records written into restart  file(s):', nrecrst
#ifdef AVERAGES
     &  ,'number of records written into averages file(s):', nrecavg
#endif
#ifdef STATIONS
     &  ,'number of records written into averages file(s):', nrecsta
#endif

      if (may_day_flag.eq.0) then
        write(stdout,'(/1x,A/)') 'MAIN: DONE'
      elseif (may_day_flag.eq.1) then
        write(stdout,'(/1x,A/)') 'MAIN: Abnormal termination: BLOWUP'
      elseif (may_day_flag.eq.2) then
        write(stdout,'(/1x,A/)')
     &                    'ERROR: Abnormal termination: netCDF INPUT'
      elseif (may_day_flag.eq.3) then
        write(stdout,'(/1x,A/)')
     &                   'ERROR: Abnormal termination: netCDF OUTPUT'
      elseif (may_day_flag.eq.4) then
        write(stdout,'(/1x,A/)') 'ERROR: Cannot open netCDF file'
      elseif (may_day_flag.eq.5) then
        write(stdout,'(/1x,A/)')
     &                         'ERROR: READ_INP: Error in input file'
      elseif (may_day_flag.eq.6) then
        write(stdout,'(/1x,A/)')
     &              'ERROR: READ_INP: An input file is not available'
      elseif (may_day_flag.eq.7) then
        write(stdout,'(/1x,A/)') 'ERROR: KPP algoritm failure'
      endif
      return
      end
