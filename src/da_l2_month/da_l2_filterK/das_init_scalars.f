      subroutine das_init_scalars
      implicit none
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=588,  MMm=624,  N=66
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp
     &          , isalt
       parameter (itemp=1,
     &            isalt=2,
     &            NT=2
     &           )
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_sio, max_whoi, max_flight, max_prf
      PARAMETER(max_sio=64,max_whoi=192,max_flight=1500,
     &         max_prf=NDAS)
      integer max_moor
      PARAMETER(max_moor=68)
      real sio_ot,sio_os,whoi_ot,whoi_os,moor_ot,moor_os
      PARAMETER( sio_ot=1.0/(1.5*1.5),sio_os=1.0/(0.3*0.3),
     &           whoi_ot=1.0/(1.5*1.5),whoi_os=1.0/(0.3*0.3),
     &           moor_ot=1.0/(1.8*1.8),moor_os=1.0/(0.25*0.25)
     &         )
      real regp
      PARAMETER( regp=1./(10.*10.) )
      integer nratio
      PARAMETER( nratio=3 )
      real zeta_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_das/u_das /ocean_v_das/v_das
     &     /ocean_t_das/t_das /ocean_zeta_das/zeta_das
      real zeta_s(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s/u_s /ocean_v_s/v_s
     &      /ocean_rho_s/rho_s /ocean_p_s/p_s
     &     /ocean_t_s/t_s /ocean_zeta_s/zeta_s
      real rmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real umask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real vmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /mask_r_das/rmask_das
     &       /mask_u_das/umask_das /mask_v_das/vmask_das
      integer nzr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nzr_das/nzr_das
     &       /ocean_nzu_das/nzu_das /ocean_nzv_das/nzv_das
      integer nsr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nsr_das/nsr_das
     &       /ocean_nsu_das/nsu_das /ocean_nsv_das/nsv_das
      real z_das(ndas),tc1d_das(ndas),sc1d_das(ndas)
      common /grid_z_das/z_das,tc1d_das,sc1d_das
      real z_dep(ndas), tclm1d(ndas), sclm1d(ndas)
      data z_dep/ 0., 5., 10., 15., 20., 25., 30., 35., 40., 45.,
     &            50.,  55.,  60.,  70.,  80.,
     &            90.,  100., 115., 135., 155.,
     &            180., 210., 250., 300., 360.,
     &            430., 510., 600., 700., 810., 930., 1060/
      integer k
      data tclm1d/12.6276, 12.5463, 12.4700, 12.3822, 11.6455,
     &            10.5014, 10.2175,  9.9360,  9.6180,  9.3822,
     &             9.1843,  8.9994,  8.8468,  8.5720,  8.3710,
     &             8.1834,  7.9598,  7.7484,  7.5485,  7.3754,
     &             7.1150,  6.8014,  6.4242,  6.1920,  5.9076,
     &             5.3877,  4.9600,  4.7265,  4.4328,  4.1766,
     &             3.7578,  3.4098 /
      data sclm1d/ 33.5314, 33.5269, 33.5189, 33.5164, 33.4538,
     &             33.3863, 33.4071, 33.4331, 33.4630, 33.4968,
     &             33.5384, 33.5711, 33.6004, 33.6625, 33.7249,
     &             33.7786, 33.8104, 33.8627, 33.9167, 33.9608,
     &             33.9932, 34.0209, 34.0493, 34.0945, 34.1535,
     &             34.1685, 34.2147, 34.2901, 34.3503, 34.4153,
     &             34.4420, 34.4778 /
      do k=1,ndas
        z_das(k)=-z_dep(ndas-k+1)
        tc1d_das(k) = tclm1d(ndas-k+1)-0.5
        sc1d_das(k) = sclm1d(ndas-k+1)-0.3
      enddo
      return
      end
