#include "cppdefs.h"
      subroutine das_init_scalars
!
! prepare all the variables that are initialized
! by one single processor
!

      implicit none
#include "param.h"
#include "das_param.h"
#include "das_ocean.h"
#include "das_innov.h"
#include "das_lbgfs.h"
      real LCT,LCS, alpha, exphT,exphS
      real z_dep(ndas), tclm1d(ndas), sclm1d(ndas)

      data z_dep/ 0., 5., 10., 15., 20., 25., 30., 35., 40., 45.,
     &            50.,  55.,  60.,  70.,  80.,
     &            90.,  100., 115., 135., 155.,
     &            180., 210., 250., 300., 360.,
     &            430., 510., 600., 700., 810., 930., 1060/
!      data z_dep/ 0.,5., 10., 15., 20., 25., 30., 35., 40., 50., 60.,
!     &            75., 100., 125., 150., 200., 250., 300., 400., 500.,
!     &           600., 800., 1000., 1200. /

      integer k
                         ! One-dimesional profiles of T/S are need
                         ! for extrapolating the background to every
                         ! model grid. Note: 1) data is given from surface
                         ! to bottom, but the DA grid from bottom to surface;
                         ! 2) the DA result is little influenced by them.
      data tclm1d/12.6276, 12.5463, 12.4700, 12.3822, 11.6455,
     &            10.5014, 10.2175,  9.9360,  9.6180,  9.3822,
     &             9.1843,  8.9994,  8.8468,  8.5720,  8.3710,
     &             8.1834,  7.9598,  7.7484,  7.5485,  7.3754,
     &             7.1150,  6.8014,  6.4242,  6.1920,  5.9076,
     &             5.3877,  4.9600,  4.7265,  4.4328,  4.1766,
     &             3.7578,  3.4098 /
      data sclm1d/ 33.5314, 33.5269, 33.5189, 33.5164, 33.4538,
     &             33.3863, 33.4071, 33.4331, 33.4630, 33.4968,
     &             33.5384, 33.5711, 33.6004, 33.6625, 33.7249,
     &             33.7786, 33.8104, 33.8627, 33.9167, 33.9608,
     &             33.9932, 34.0209, 34.0493, 34.0945, 34.1535,
     &             34.1685, 34.2147, 34.2901, 34.3503, 34.4153,
     &             34.4420, 34.4778 /

!      data tclm1d/30.125, 30.097, 29.970, 29.744, 29.428, 29.004,
!     &            28.456, 27.886, 27.393, 26.551, 26.037, 25.706,
!     &            25.590, 25.523, 25.410, 24.432, 23.079, 21.792,
!     &            19.600, 17.615, 15.506, 11.818, 7.813, 6.255/
!      data sclm1d/36.546, 36.505, 36.472, 36.455, 36.443, 36.432,
!     &            36.419, 36.409, 36.406, 36.425, 36.440, 36.456,
!     &            36.446, 36.467, 36.505, 36.592, 36.633, 36.574,
!     &            36.243, 36.009, 35.773, 35.287, 35.024, 34.896/
!

      do k=1,ndas
        z_das(k)=-z_dep(ndas-k+1)
        tc1d_das(k) = tclm1d(ndas-k+1)-0.5   ! help stabalize the stratification
        sc1d_das(k) = sclm1d(ndas-k+1)-0.3
      enddo

      costf=0.
      cost0=0.

!      LC=50.0
      LCT=30.0
      LCS=35.0
      alpha=0.5
      do k=1,max_prf
        exphT=(1.0+ alpha*exp(-z_dep(k)*z_dep(k)/(LCT*LCT)))
        exphS=(1.0+ alpha*exp(-z_dep(k)*z_dep(k)/(LCS*LCS)))
        sio_ot(k) = sio_ot_raw/(exphT*exphT)
        sio_os(k) = sio_os_raw/(exphS*exphS)
        whoi_ot(k) = whoi_ot_raw/(exphT*exphT)
        whoi_os(k) = whoi_os_raw/(exphS*exphS)
        moor_ot(k) = moor_ot_raw/(exphT*exphT)
        moor_os(k) = moor_os_raw/(exphS*exphS)
      enddo

      return
      end
