      integer NSA, N2d,N3d, size_XI,size_ETA
      parameter (NSA=16)
c---#define ALLOW_SINGLE_BLOCK_MODE
#ifdef  ALLOW_SINGLE_BLOCK_MODE
      parameter (size_XI=6+Lm, size_ETA=6+Mm)
#else
      parameter (size_XI=7+(Lm+NSUB_X-1)/NSUB_X,
     &           size_ETA=7+(Mm+NSUB_E-1)/NSUB_E)
#endif
      integer se,sse, sz,ssz
      parameter (sse=size_ETA/Np, ssz=Np/size_ETA)
      parameter (se=sse/(sse+ssz),   sz=1-se)

      parameter (N2d=size_XI*(se*size_ETA+sz*Np),
     &                  N3d=size_XI*size_ETA*Np)
#ifdef SGI
      real A2d(N2d,NSA,0:NPP-1), A3d(N3d,6,0:NPP-1)
CSDISTRIBUTE_RESHAPE A2d(*,*,cyclic), A3d(*,*,cyclic)
      common /private_scratch_A2d/A2d
     &       /private_scratch_A3d/A3d
#else
      real A2d(N2d,NSA,0:0), A3d(N3d,6,0:0)
# ifdef CRAY
      task common /private_scratch/ A2d,A3d
# else
      common /private_scratch/ A2d,A3d
# endif
#endif
