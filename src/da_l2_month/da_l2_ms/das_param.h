      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
!
! minimization lbfgs
!
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)     !T
     &        +NDAS*(Lm+2)*(Mm+2)     !S
#if !defined DAS_GEOS_STRONG || defined DAS_HFRADAR
     &        +NDAS*(Lm+1)*(Mm+1)     !psi
     &        +NDAS*(Lm+2)*(Mm+2)     !chi
#endif
#if !defined DAS_HYDRO_STRONG
     &        +(Lm+2)*(Mm+2)
#endif
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
!
! in-situ observation
!
      integer max_prf
      PARAMETER( max_prf=NDAS)   !fixed
!
      integer max_sio, max_whoi, max_flight,
     &        max_hfradar,max_hfradar6,max_js1
      PARAMETER(max_sio=100,max_whoi=100,max_flight=30000,
     &         max_hfradar=30000,max_hfradar6=30000,max_js1=8000)
! ctd
!
      integer max_ptsur
      PARAMETER(max_ptsur=100)
      integer max_martn
      PARAMETER(max_martn=100)
      integer max_moor
      PARAMETER(max_moor=280)
!
! inversion of observational error variance
!
      real sio_ot,sio_os,whoi_ot,whoi_os,ptsur_ot,ptsur_os,
     &              martn_ot,martn_os,moor_ot,moor_os, hf_ouv,
     &              hf_ouv6,fl_ot,js1_ossh,swot_ossh,hcmin
      PARAMETER( sio_ot=1.0/(1.185*1.185),sio_os=1.0/(0.16*0.16),
     &          whoi_ot=1.0/(1.17*1.17),whoi_os=1.0/(0.16*0.16),
     &        ptsur_ot=1.0/(1.65*1.65),ptsur_os=1.0/(0.2*0.2),
     &        martn_ot=1.0/(1.65*1.65),martn_os=1.0/(0.2*0.2),
     &        moor_ot=1.0/(1.2*1.2),moor_os=1.0/(0.15*0.15),
     &        js1_ossh=1.0/(0.03*0.03),swot_ossh=1.0/(0.10*0.10),
     &        hcmin=50.0,
     &        fl_ot=1.0/(1.0*1.0),
     &        hf_ouv=1.0/(0.07*0.07),hf_ouv6=1.0/(0.2*0.2)
     &         )
!
! auv
!
! CalPoly
      integer max_prf_cal,max_cal
      PARAMETER(max_prf_cal=NDAS,max_cal=100)
! Dorado
      integer max_prf_dor,max_dor
      PARAMETER(max_prf_dor=NDAS,max_dor=100)
!
! inversion of observational error variance
!
      real cal_ot,cal_os,dor_ot,dor_os
      PARAMETER( cal_ot=1.0/(1.0*1.0),
     &           cal_os=1.0/(0.2*0.2),
     &           dor_ot=1.0/(1.0*1.0),
     &           dor_os=1.0/(0.2*0.2)
     &         )
!
! geostrophic ratio
!
      real geo_ratio, cross_tsp
      PARAMETER( geo_ratio = 1.0, cross_tsp=0.)
      integer sm_rad   ! for smoothing radius
      PARAMETER( sm_rad=5)

      integer Local_len
      PARAMETER( Local_len=20)   ! correlation set zero beyond this length

      real sz_rad                ! smoothing steric SSHs for geostrophic SSHs
      PARAMETER( sz_rad=6.0)
      integer sz_rad_len                ! smoothing steric SSHs for geostrophic SSHs
      PARAMETER( sz_rad_len=20)         ! sz_rad_len should be 3 times larger that sz_rad

      real regp, reguv              !  Regular parameter                                                                                      
      PARAMETER( regp=1.0/(10.0*10.0) )
      PARAMETER( reguv=1.0/(0.6*0.6) )
