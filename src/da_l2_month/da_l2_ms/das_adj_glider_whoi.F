#include "cppdefs.h"

      subroutine das_adj_glider_whoi (tile)
      implicit none
      integer tile
#include "param.h"
#include "das_param.h"
#include "compute_tile_bounds.h"
      call das_adj_glider_whoi_tile (Istr,Iend,Jstr,Jend) 
      return
      end

      subroutine das_adj_glider_whoi_tile (Istr,Iend,Jstr,Jend) 
      implicit none
      integer Istr,Iend,Jstr,Jend,    i,j,k
      real cft, cfs, kdas
#include "param.h"
# include "das_param.h"
#include "grid.h"
#include "scalars.h"
# include "das_ocean.h"
# include "das_covar.h"
# include "das_lbgfs.h"
# include "das_innov.h"
!
# include "compute_auxiliary_bounds.h"

!
      do k=1,max_prf
        kdas=NDAS-k+1
        do j=JstrR,JendR
          do i=IstrR,IendR
            cft=t_s(i,j,kdas,itemp)-t_whoi(i,j,k)
            cfs=t_s(i,j,kdas,isalt)-s_whoi(i,j,k)
            t_s_adj(i,j,kdas,itemp)=t_s_adj(i,j,kdas,itemp)
     &                       +cft*whoi_t_mask(i,j,k)*whoi_ot
            t_s_adj(i,j,kdas,isalt)=t_s_adj(i,j,kdas,isalt)
     &                       +cfs*whoi_s_mask(i,j,k)*whoi_os
          enddo
        enddo
      enddo
!
!
      return
      end
