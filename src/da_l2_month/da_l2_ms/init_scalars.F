!
! $Id: init_scalars.F,v 1.15 2005/10/11 12:37:02 pmarches Exp $
!
#include "cppdefs.h"

       subroutine init_scalars (ierr)
!
! Set initial values for  globally accessable (stored in common
! blocks) scalar variables. Typically this is associated with
! model utility switches, i/o contlol, time indices, global
! summation counters, etc; NONE of the physical parameters of
! the model is set here.
!
      implicit none
      integer ierr, i,j,itrc, lvar, lenstr
      character*20 nametrc, unitt
      character*60 vname1, vname3      
      integer omp_get_num_threads
#include "param.h"
#include "scalars.h"
#include "ncscrum.h"
#if defined DIAGNOSTICS_TS || defined DIAGNOSTICS_UV
# include "diagnostics.h"
#endif
#include "boundary.h"

      call system('uname -prs')
      
!
! Find out how many threads are created and check whether the number
! of threads exceeds maximum allowed, as well as check if the number
! of tiles is divisible by the number of threads, so the job can be
! evenly partitioned, complain and signal to terminate, if something
! is wrong. 
! 
C$OMP PARALLEL
C$OMP CRITICAL (isca_cr_rgn)
      numthreads=omp_get_num_threads()
C$OMP END CRITICAL (isca_cr_rgn)
C$OMP END PARALLEL
      MPI_master_only write(stdout,'(1x,A,3(1x,A,I3),A)') 'NUMBER',
     &    'OF THREADS:',numthreads,'BLOCKING:',NSUB_X,'x',NSUB_E,'.'

      if (numthreads.gt.NPP) then
        MPI_master_only write(stdout,'(/1x,A,I3/)')
     &    'ERROR: Requested number of threads exceeds NPP =', NPP
        ierr=ierr+1                                      !--> ERROR
      elseif (mod(NSUB_X*NSUB_E,numthreads).ne.0) then
        MPI_master_only write(stdout,
     &                '(/1x,A,1x,A,I3,4x,A,I3,4x,A,I4,A)') 'ERROR:',
     &                'wrong choice of numthreads =', numthreads,
     &                'NSUB_X =', NSUB_X, 'NSUB_E =', NSUB_E, '.'
        ierr=ierr+1                                      !--> ERROR
      endif
!
! Set evolving time indices to their default initial values.
!
      time=0.
      tdays=0.
      PREDICTOR_2D_STEP=.FALSE.
      iic=0
      kstp=1
      krhs=1
      knew=1

      ntstart=1
#ifdef SOLVE3D
      nstp=1
      nrhs=1
      nnew=1
#endif
      nfast=1

#ifdef FLOATS
      nfp1=0     ! set time step indices for
      nf=3       ! 5th order AB4AM4 corrected
      nfm1=2     ! scheme
      nfm2=1
      nfm3=0
#endif
!
! Reset intra-thread communication flags and counters.
!
      synchro_flag=.true.
      first_time=0
      may_day_flag=0
!
! Reset process ID and CPU_time clocks (needed by timers).
!
      do j=0,NPP
        do i=0,31
          proc(i,j)=0
          CPU_time(i,j)=0.E0
        enddo
      enddo
      trd_count=0
!
! Reset record counters for averages/history output.
!
      nrecrst=0
      nrechis=0
#ifdef AVERAGES
      nrecavg=0
#endif
#if defined DIAGNOSTICS_TS || defined DIAGNOSTICS_UV
      nrecdia=0
# ifdef AVERAGES
      nrecdia_avg=0
# endif
#endif 
!
! Reset global sums and counters needed by volume/energy
! diagnostics and volume conservation constraint.
!
      tile_count=0
      bc_count=0
      avgke=0.
      avgpe=0.
      avgkp=0.
      volume=0.

      hmin=+1.E+20      ! Set these
      hmax=-1.E+20      ! to large,
      grdmin=+1.E+20    ! positive or 
      grdmax=-1.E+20    ! negative
      Cu_min=+1.E+20    ! unrealistic
      Cu_max=-1.E+20    ! values

      bc_crss=QuadZero
#ifdef OBC_VOLCONS
      bc_flux=QuadZero
      ubar_xs=QuadZero
#endif
#ifdef SOLVE3D
      rx0=-1.E+20
      rx1=-1.E+20
#endif
#ifdef BIOLOGY
      bio_count=0
      do i=0,2*NT+1
        global_sum(i)=QuadZero
      enddo
#endif
!
! Initialize netCDF files IDs to closed status.
!
      ncidrst=-1
      ncidhis=-1
#ifdef AVERAGES
      ncidavg=-1
#endif
      ncidfrc=-1
      ncidbulk=-1
      ncidclm=-1
#if defined DIAGNOSTICS_TS || defined DIAGNOSTICS_UV
      nciddia=-1
# ifdef AVERAGES
      nciddia_avg=-1
# endif 
#endif 
#ifndef ANA_BRY
       bry_id=-1
#endif
      call get_date (date_str)
!
!---------------------------------------------------------------
! Define names of variables in NetCDF output files.
! Inner dimension is for variable type and outer is for variable
! attributes (name, long-name, units, field).
!---------------------------------------------------------------
!
c--#define CR  
CR      write(stdout,'(1x,A,I3)') 'indxTime =',indxTime
      vname(1,indxTime)='scrum_time                               '
      vname(2,indxTime)='time since initialization                '
      vname(3,indxTime)='second                                   '
      vname(4,indxTime)='time, scalar, series                     '

CR      write(stdout,'(1x,A,I3)') 'indxZ =',indxZ
      vname(1,indxZ)='zeta                                        '
      vname(2,indxZ)='free-surface                                '
      vname(3,indxZ)='meter                                       '
      vname(4,indxZ)='free-surface, scalar, series                '

CR      write(stdout,'(1x,A,I3)') 'indxUb =',indxUb
      vname(1,indxUb)='ubar                                       '
      vname(2,indxUb)='vertically integrated u-momentum component '
      vname(3,indxUb)='meter second-1                             '
      vname(4,indxUb)='ubar-velocity, scalar, series              '

CR      write(stdout,'(1x,A,I3)') 'indxVb = ',indxVb
      vname(1,indxVb)='vbar                                       '
      vname(2,indxVb)='vertically integrated v-momentum component '
      vname(3,indxVb)='meter second-1                             '
      vname(4,indxVb)='vbar-velocity, scalar, series              '

#ifdef SOLVE3D
CR      write(stdout,'(1x,A,I3)') 'indxU = ',indxU
      vname(1,indxU)='u                                           '
      vname(2,indxU)='u-momentum component                        '
      vname(3,indxU)='meter second-1                              '
      vname(4,indxU)='u-velocity, scalar, series                  '

CR      write(stdout,'(1x,A,I3)') 'indxV = ',indxV
      vname(1,indxV)='v                                           '
      vname(2,indxV)='v-momentum component                        '
      vname(3,indxV)='meter second-1                              '
      vname(4,indxV)='v-velocity, scalar, series                  '

CR      write(stdout,'(1x,A,I3)') 'indxT = ',indxT
      vname(1,indxT)='temp                                        '
      vname(2,indxT)='potential temperature                       '
      vname(3,indxT)='Celsius                                     '
      vname(4,indxT)='temperature, scalar, series                 '

# ifdef SALINITY
CR      write(stdout,'(1x,A,I3)') 'indxS = ',indxS
      vname(1,indxS)='salt                                        '
      vname(2,indxS)='salinity                                    '
      vname(3,indxS)='PSU                                         '
      vname(4,indxS)='salinity, scalar, series                    '
# endif

# ifdef PASSIVE_TRACER
CR      write(stdout,'(1x,A,I3)') 'indxTPAS = ',indxTPAS
      vname(1,indxTPAS)='tpas                                     '
      vname(2,indxTPAS)='passive tracer                           '
      vname(3,indxTPAS)='no unit                                  '
      vname(4,indxTPAS)='passive tracer, scalar, series           '
# endif

# ifdef BIOLOGY
#  ifdef BIO_NChlPZD
CR      write(stdout,'(1x,A,I3)') 'indxNO3 =',indxNO3
      vname(1,indxNO3)='NO3                                       '
      vname(2,indxNO3)='NO3 Nutrient                              '
      vname(3,indxNO3)='mMol N m-3                                '
      vname(4,indxNO3)='NO3, scalar, series                       '

CR      write(stdout,'(1x,A,I3)') 'indxChla =',indxChla
      vname(1,indxChla)='CHLA                                     '
      vname(2,indxChla)='Chlorophyll A                            '
      vname(3,indxChla)='mg Chla m-3                              '
      vname(4,indxChla)='Chlorophyl, scalar, series               '

CR      write(stdout,'(1x,A,I3)') 'indxPhy1 =',indxPhy1
      vname(1,indxPhy1)='PHYTO                                    '
      vname(2,indxPhy1)='Phytoplankton                            '
      vname(3,indxPhy1)='mMol N m-3                               '
      vname(4,indxPhy1)='Phytoplankton, scalar, series            '

CR      write(stdout,'(1x,A,I3)') 'indxZoo1 =',indxZoo1
      vname(1,indxZoo1)='ZOO                                      '
      vname(2,indxZoo1)='Zooplankton                              '
      vname(3,indxZoo1)='mMol N m-3                               '
      vname(4,indxZoo1)='Zooplankton, scalar, series              '

CR      write(stdout,'(1x,A,I3)') 'indxDet1 =',indxDet1
      vname(1,indxDet1)='DET                                      '
      vname(2,indxDet1)='Detritus Nutrient                        '
      vname(3,indxDet1)='mMol N m-3                               '
      vname(4,indxDet1)='Detritus, scalar, series                 '

#  elif defined BIO_N2ChlPZD2

CR      write(stdout,'(1x,A,I3)') 'indxNO3 =',indxNO3
      vname(1,indxNO3)='NO3                                       '
      vname(2,indxNO3)='NO3 Nutrient                              '
      vname(3,indxNO3)='mMol N m-3                                '
      vname(4,indxNO3)='NO3, scalar, series                       '

CR      write(stdout,'(1x,A,I3)') 'indxNH4 =',indxNH4
      vname(1,indxNH4)='NH4                                       '
      vname(2,indxNH4)='NH4 Nutrient                              '
      vname(3,indxNH4)='mMol N m-3                                '
      vname(4,indxNH4)='NH4, scalar, series                       '

CR      write(stdout,'(1x,A,I3)') 'indxChla =',indxChla
      vname(1,indxChla)='CHLA                                     '
      vname(2,indxChla)='Chlorophyll A                            '
      vname(3,indxChla)='mg Chla m-3                              '
      vname(4,indxChla)='Chlorophyl, scalar, series               '

CR      write(stdout,'(1x,A,I3)') 'indxPhy1 =',indxPhy1
      vname(1,indxPhy1)='PHYTO                                    '
      vname(2,indxPhy1)='Phytoplankton                            '
      vname(3,indxPhy1)='mMol N m-3                               '
      vname(4,indxPhy1)='Phytoplankton, scalar, series            '

CR      write(stdout,'(1x,A,I3)') 'indxZoo1 =',indxZoo1
      vname(1,indxZoo1)='ZOO                                      '
      vname(2,indxZoo1)='Zooplankton                              '
      vname(3,indxZoo1)='mMol N m-3                               '
      vname(4,indxZoo1)='Zooplankton, scalar, series              '

CR      write(stdout,'(1x,A,I3)') 'indxDet1 =',indxDet1
      vname(1,indxDet1)='SDET                                     '
      vname(2,indxDet1)='Small Detritus Nutrient                  '
      vname(3,indxDet1)='mMol N m-3                               '
      vname(4,indxDet1)='Small Detritus, scalar, series           '

CR      write(stdout,'(1x,A,I3)') 'indxDet2 =',indxDet2
      vname(1,indxDet2)='LDET                                     '
      vname(2,indxDet2)='Large Detritus Nutrient                  '
      vname(3,indxDet2)='mMol N m-3                               '
      vname(4,indxDet2)='Large Detritus, scalar, series           '

#  elif defined BIO_N2P2Z2D2

CR      write(stdout,'(1x,A,I3)') 'indxNO3 =',indxNO3
      vname(1,indxNO3)='NO3                                       '
      vname(2,indxNO3)='NO3 Nutrient                              '
      vname(3,indxNO3)='mMol N m-3                                '
      vname(4,indxNO3)='NO3, scalar, series                       '

CR      write(stdout,'(1x,A,I3)') 'indxNH4 =',indxNH4
      vname(1,indxNH4)='NH4                                       '
      vname(2,indxNH4)='NH4 Nutrient                              '
      vname(3,indxNH4)='mMol N m-3                                '
      vname(4,indxNH4)='NH4, scalar, series                       '

CR      write(stdout,'(1x,A,I3)') 'indxPhy1 =',indxPhy1
      vname(1,indxPhy1)='SPHYTO                                   '
      vname(2,indxPhy1)='Small Phytoplankton                      '
      vname(3,indxPhy1)='mMol N m-3                               '
      vname(4,indxPhy1)='Small Phytoplankton, scalar, series      '

CR      write(stdout,'(1x,A,I3)') 'indxPhy2 =',indxPhy2
      vname(1,indxPhy2)='LPHYTO                                   '
      vname(2,indxPhy2)='Large Phytoplankton                      '
      vname(3,indxPhy2)='mMol N m-3                               '
      vname(4,indxPhy2)='Large Phytoplankton, scalar, series      '

CR      write(stdout,'(1x,A,I3)') 'indxZoo1 =',indxZoo1
      vname(1,indxZoo1)='SZOO                                     '
      vname(2,indxZoo1)='Small Zooplankton                        '
      vname(3,indxZoo1)='mMol N m-3                               '
      vname(4,indxZoo1)='Small Zooplankton, scalar, series        '

CR      write(stdout,'(1x,A,I3)') 'indxZoo2 =',indxZoo2
      vname(1,indxZoo2)='LZOO                                     '
      vname(2,indxZoo2)='Large Zooplankton                        '
      vname(3,indxZoo2)='mMol N m-3                               '
      vname(4,indxZoo2)='Large Zooplankton, scalar, series        '

CR      write(stdout,'(1x,A,I3)') 'indxDet1 =',indxDet1
      vname(1,indxDet1)='SDET                                     '
      vname(2,indxDet1)='Small Detritus Nutrient                  '
      vname(3,indxDet1)='mMol N m-3                               '
      vname(4,indxDet1)='Small Detritus, scalar, series           '

CR      write(stdout,'(1x,A,I3)') 'indxDet2 =',indxDet2
      vname(1,indxDet2)='LDET                                     '
      vname(2,indxDet2)='Large Detritus Nutrient                  '
      vname(3,indxDet2)='mMol N m-3                               '
      vname(4,indxDet2)='Large Detritus, scalar, series           '
#  endif
# endif
# ifdef SEDIMENT
CR      write(stdout,'(1x,A,I3)') 'indxsand =',indxsand
      vname(1,indxsand)='sand                                     '
      vname(2,indxsand)='sand sediment                            '
      vname(3,indxsand)='mg/l                                     '
      vname(4,indxsand)='sand, scalar, series                     '

CR      write(stdout,'(1x,A,I3)') 'indxsilt =',indxsilt
      vname(1,indxsilt)='silt                                     '
      vname(2,indxsilt)='silt sediment                            '
      vname(3,indxsilt)='mg/l                                     '
      vname(4,indxsilt)='silt, scalar, series                     '

CR      write(stdout,'(1x,A,I3)') 'indxBTHK =',indxBTHK
      vname(1,indxBTHK)  ='bed_thick                              '
      vname(2,indxBTHK)  ='Thickness of sediment bed layer        '
      vname(3,indxBTHK)  ='m                                      '
CR      write(stdout,'(1x,A,I3)') 'indxBPOR =',indxBPOR
      vname(1,indxBPOR)  ='bed_poros                              '
      vname(2,indxBPOR)  ='Porosity of sediment bed layer         '
      vname(3,indxBPOR)  ='no units                               '
CR      write(stdout,'(1x,A,I3)') 'indxBFRA1 =',indxBFRA
      vname(1,indxBFRA)  ='bed_frac_sand                          '
      vname(2,indxBFRA)  ='volume fraction of sand in bed layer   '
      vname(3,indxBFRA)  ='no units                               '
CR      write(stdout,'(1x,A,I3)') 'indxBFRA2 =',indxBFRA+1
      vname(1,indxBFRA+1)='bed_frac_silt                          '
      vname(2,indxBFRA+1)='Volume fraction of silt in bed layer   '
      vname(3,indxBFRA+1)='no units                               '
# endif
# ifdef BBL
CR      write(stdout,'(1x,A,I3)') 'indxAbed=',indxBBL
      vname(1,indxBBL)  ='Abed                                    '
      vname(2,indxBBL)  ='Bed wave excursion amplitude            '
      vname(3,indxBBL)  ='m                                       '
CR      write(stdout,'(1x,A,I3)') 'indxHrip =',indxBBL+1
      vname(1,indxHrip)='Hripple                                  '
      vname(2,indxHrip)='Bed ripple height                        '
      vname(3,indxHrip)='m                                        '
CR      write(stdout,'(1x,A,I3)') 'indxLrip =',indxBBL+2
      vname(1,indxLrip)='Lripple                                  '
      vname(2,indxLrip)='Bed ripple length                        '
      vname(3,indxLrip)='m                                        '
CR      write(stdout,'(1x,A,I3)') 'indxZbnot=',indxBBL+3
      vname(1,indxZbnot)='Zbnot                                   '
      vname(2,indxZbnot)='Physical hydraulic bottom roughness     '
      vname(3,indxZbnot)='m                                       '
CR      write(stdout,'(1x,A,I3)') 'indxZbapp=',indxBBL+4
      vname(1,indxZbapp)='Zbapp                                   '
      vname(2,indxZbapp)='Apparent hydraulic bottom roughness     '
      vname(3,indxZbapp)='m                                       '
CR      write(stdout,'(1x,A,I3)') 'indxBostrw=',indxBBL+5
      vname(1,indxBostrw)='bostrw                                 '
      vname(2,indxBostrw)='Wave-induced kinematic bottom stress   '
      vname(3,indxBostrw)='N/m2                                   '
# endif

CR      write(stdout,'(1x,A,I3)') 'indxBostr =',indxBostr
      vname(1,indxBostr)='bostr                                   '
      vname(2,indxBostr)='Kinematic bottom stress                 '
      vname(3,indxBostr)='N/m2                                    '

CR      write(stdout,'(1x,A,I3)') 'indxO =',indxO
      vname(1,indxO)='omega                                       '
      vname(2,indxO)='S-coordinate vertical momentum component    '
      vname(3,indxO)='meter second-1                              '
      vname(4,indxO)='omega, scalar, series                       '

CR      write(stdout,'(1x,A,I3)') 'indxW =',indxW
      vname(1,indxW)='w                                           '
      vname(2,indxW)='vertical momentum component                 '
      vname(3,indxW)='meter second-1                              '
      vname(4,indxW)='w-velocity, scalar, series                  '

CR      write(stdout,'(1x,A,I3)') 'indxR =',indxR
      vname(1,indxR)='rho                                         '
      vname(2,indxR)='density anomaly                             '
      vname(3,indxR)='kilogram meter-3                            '
      vname(4,indxR)='density, scalar, series                     '

# if defined UV_VIS2 && defined SMAGORINSKY
CR      write(stdout,'(1x,A,I3)') 'indxVisc =',indxVisc
      vname(1,indxVisc)='visc3d                                   '
      vname(2,indxVisc)='horizontal viscosity coefficient         '
      vname(3,indxVisc)='meter2 second-1                          '
      vname(4,indxVisc)='vis3d, scalar, series                    '
# endif

CR      write(stdout,'(1x,A,I3)') 'indxAkv =',indxAkv
      vname(1,indxAkv)='AKv                                       '
      vname(2,indxAkv)='vertical viscosity coefficient            '
      vname(3,indxAkv)='meter2 second-1                           '
      vname(4,indxAkv)='AKv, scalar, series                       '

CR      write(stdout,'(1x,A,I3)') 'indxAkt =',indxAkt
      vname(1,indxAkt)='AKt                                       '
      vname(2,indxAkt)='temperature vertical diffusion coefficient'
      vname(3,indxAkt)='meter2 second-1                           '
      vname(4,indxAkt)='AKt, scalar, series                       '
# ifdef SALINITY
CR      write(stdout,'(1x,A,I3)') 'indxAks =',indxAks
      vname(1,indxAks)='AKs                                       '
      vname(2,indxAks)='salinity vertical diffusion coefficient   '
      vname(3,indxAks)='meter2 second-1                           '
      vname(4,indxAks)='AKs, scalar, series                       '
# endif
# ifdef LMD_SKPP
CR      write(stdout,'(1x,A,I3)') 'indxHbl =',indxHbl
      vname(1,indxHbl)='hbl                                       '
      vname(2,indxHbl)='depth of planetary boundary layer         '
      vname(3,indxHbl)='meter                                     '
      vname(4,indxHbl)='hbl, scalar, series                       '
# endif
# ifdef LMD_BKPP
CR      write(stdout,'(1x,A,I3)') 'indxHbbl =',indxHbbl
      vname(1,indxHbbl)='hbbl                                     '
      vname(2,indxHbbl)='depth of bottom boundary layer           '
      vname(3,indxHbbl)='meter                                    '
      vname(4,indxHbbl)='hbbl, scalar, series                     '
# endif
#endif
CR      write(stdout,'(1x,A,I3)') 'indxSSH =',indxSSH
      vname(1,indxSSH)='SSH                                       '
      vname(2,indxSSH)='sea surface height                        '
      vname(3,indxSSH)='meter                                     '
      vname(4,indxSSH)='SSH, scalar, series                       '

CR      write(stdout,'(1x,A,I3)') 'indxSUSTR =',indxSUSTR
      vname(1,indxSUSTR)='sustr                                   '
      vname(2,indxSUSTR)='surface u-momentum stress               '
      vname(3,indxSUSTR)='Newton meter-2                          '
      vname(4,indxSUSTR)='surface u-mom stress, scalar, series    '

CR      write(stdout,'(1x,A,I3)') 'indxSVSTR =',indxSVSTR
      vname(1,indxSVSTR)='svstr                                   '
      vname(2,indxSVSTR)='surface v-momentum stress               '
      vname(3,indxSVSTR)='Newton meter-2                          '
      vname(4,indxSVSTR)='surface v-mom stress, scalar, series    '

#ifdef SOLVE3D
CR      write(stdout,'(1x,A,I3)') 'indxSHFl =',indxSHFl
      vname(1,indxSHFl)='shflux                                   '
      vname(2,indxSHFl)='surface net heat flux                    '
      vname(3,indxSHFl)='Watts meter-2                            '
      vname(4,indxSHFl)='surface heat flux, scalar, series        '
# ifdef SALINITY
CR      write(stdout,'(1x,A,I3)') 'indxSSFl=',indxSSFl
      vname(1,indxSSFl)='swflux                                   '
      vname(2,indxSSFl)='surface freshwater flux (E-P)            '
      vname(3,indxSSFl)='centimeter day-1                         '
      vname(4,indxSSFl)='surface freshwater flux, scalar, series  '
# endif
CR      write(stdout,'(1x,A,I3)') 'indxSWRad =',indxSWRad
      vname(1,indxSWRad)='swrad                                   '
      vname(2,indxSWRad)='solar shortwave radiation               '
      vname(3,indxSWRad)='Watts meter-2                           '
      vname(4,indxSWRad)='shortwave radiation, scalar, series     '

CR      write(stdout,'(1x,A,I3)') 'indxSST =',indxSST
      vname(1,indxSST)='SST                                       '
      vname(2,indxSST)='sea surface temperature                   '
      vname(3,indxSST)='Celsius                                   '
      vname(4,indxSST)='SST, scalar, series                       '

CR      write(stdout,'(1x,A,I3)') 'indxdQdSST =',indxdQdSST
      vname(1,indxdQdSST)='dQdSST                                 '
      vname(2,indxdQdSST)='surface heat flux sensitivity to SST   '
      vname(3,indxdQdSST)='Watts meter-2 Celsius-1                '
      vname(4,indxdQdSST)='dQdSST, scalar, series                 '
# if defined SALINITY && defined QCORRECTION && defined SFLX_CORR
CR      write(stdout,'(1x,A,I3)') 'indxSSS =',indxSSS
      vname(1,indxSSS)='SSS                                       '
      vname(2,indxSSS)='sea surface salinity                      '
      vname(3,indxSSS)='PSU                                       '
      vname(4,indxSSS)='SSS, scalar, series                       '
# endif
# ifdef BBL
#  ifndef ANA_WWAVE
CR      write(stdout,'(1x,A,I3)') 'indxWWA',indxWWA
      vname(1,indxWWA)='Awave                                     '
      vname(2,indxWWA)='wind induced wave amplitude               '
      vname(3,indxWWA)='meter                                     '
      vname(4,indxWWA)='Awave, scalar, series                     '

CR      write(stdout,'(1x,A,I3)') 'indxWWD =',indxWWD
      vname(1,indxWWD)='Dwave                                     '
      vname(2,indxWWD)='wind induced wave direction               '
      vname(3,indxWWD)='degrees                                   '
      vname(4,indxWWD)='Dwave, scalar, series                     '

CR      write(stdout,'(1x,A,I3)') 'indxWWP =',indxWWP
      vname(1,indxWWP)='Pwave                                     '
      vname(2,indxWWP)='wind induced wave Period                  '
      vname(3,indxWWP)='second                                    '
      vname(4,indxWWP)='Pwave, scalar, series                     '
#  endif
#  if (!defined ANA_BSEDIM && !defined SEDIMENT)
CR      write(stdout,'(1x,A,I3)') 'indxBSS =',indxBSS
      vname(1,indxBSS)='Ssize                                     '
      vname(2,indxBSS)='bottom sediment grain diameter size       '
      vname(3,indxBSS)='meter                                     '
      vname(4,indxBSS)='Ssize, scalar, series                     '

CR      write(stdout,'(1x,A,I3)') 'indxBSD =',indxBSD
      vname(1,indxBSD)='Sdens                                     '
      vname(2,indxBSD)='bottom sediment grain density             '
      vname(3,indxBSD)='kilogram meter-3                          '
      vname(4,indxBSD)='Sdens, scalar, series                     '
#  endif
# endif
#endif
#ifdef ICE
      vname(1,indxAi)='aice                                       '
      vname(2,indxAi)='fraction of cell covered by ice            '
      vname(3,indxAi)='nondimensional                             '
      vname(4,indxAi)='aice, scalar, series                       '

      vname(1,indxUi)='uice                                       '
      vname(2,indxUi)='u-component of ice velocity                '
      vname(3,indxUi)='meter sec-1                                '
      vname(4,indxUi)='uice, scalar, series                       '

      vname(1,indxVi)='vice                                       '
      vname(2,indxVi)='v-component of ice velocity                '
      vname(3,indxVi)='meter sec-1                                '
      vname(4,indxVi)='vice, scalar, series                       '

      vname(1,indxHi)='hice                                       '
      vname(2,indxHi)='depth of ice cover                         '
      vname(3,indxHi)='meter                                      '
      vname(4,indxHi)='hice, scalar, series                       '

      vname(1,indxHS)='snow_thick                                 '
      vname(2,indxHS)='depth of snow cover                        '
      vname(3,indxHS)='meter                                      '
      vname(4,indxHS)='snow_thick, scalar, series                 '

      vname(1,indxTIsrf)='tisrf                                   '
      vname(2,indxTIsrf)='temperature of ice surface              '
      vname(3,indxTIsrf)='Kelvin                                  '
      vname(4,indxTIsrf)='tsrf, scalar, series                    '
#endif
#ifdef BULK_FLUX
CR      write(stdout,'(1x,A,I3)') 'indxWSPD =',indxWSPD
      vname(1,indxWSPD)='wspd                                     '
      vname(2,indxWSPD)='surface wind speed 10 m                  '
      vname(3,indxWSPD)='meter second-1                           '
      vname(4,indxWSPD)='surface wind speed, scalar, series       '

CR      write(stdout,'(1x,A,I3)') 'indxTAIR =',indxTAIR
      vname(1,indxTAIR)='tair                                     '
      vname(2,indxTAIR)='surface air temperature 2m               '
      vname(3,indxTAIR)='Celsius                                  '
      vname(4,indxTAIR)='surface air temperature, scalar, series  '

CR      write(stdout,'(1x,A,I3)') 'indxRHUM =',indxRHUM
      vname(1,indxRHUM)='rhum                                     '
      vname(2,indxRHUM)='surface air relative humidity 2m         '
      vname(3,indxRHUM)='fraction                                 '
      vname(4,indxRHUM)='surface relative humidity, scalar, series'

CR      write(stdout,'(1x,A,I3)') 'indxRADLW =',indxRADLW
      vname(1,indxRADLW)='radlw                                   '
      vname(2,indxRADLW)='net terrestrial longwave radiation      '
      vname(3,indxRADLW)='Watts meter-2                           '
      vname(4,indxRADLW)='terrestrial longwave, scalar, series    '

CR      write(stdout,'(1x,A,I3)') 'indxRADSW =',indxRADSW
      vname(1,indxRADSW)='radsw                                   '
      vname(2,indxRADSW)='net solar shortwave radiation           '
      vname(3,indxRADSW)='Watts meter-2                           '
      vname(4,indxRADSW)='solar shortwave, scalar, series         '

CR      write(stdout,'(1x,A,I3)') 'indxPRATE =',indxPRATE
      vname(1,indxPRATE)='prate                                   '
      vname(2,indxPRATE)='surface precipitation rate              '
      vname(3,indxPRATE)='Kg meter-2 second-1                     '
      vname(4,indxPRATE)='precipitation rate, scalar, series      '
#endif /* BULK_FLUX */

# ifdef DIAGNOSTICS_TS
      do itrc=1,NT
CR       write(stdout,'(1x,A,I3)') 'indxTXadv = ',indxTXadv+itrc-1
       lvar=lenstr(vname(1,indxT+itrc-1))
       nametrc=vname(1,indxT+itrc-1)(1:lvar)
       lvar=lenstr(nametrc)
       unitt=vname(3,indxT+itrc-1)
       write(vname1,*) trim(nametrc),'_xadv                       '
       vname(1,indxTXadv+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxTXadv+itrc-1)
       vname(2,indxTXadv+itrc-1)='Horizontal (xi) advection term  '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxTXadv+itrc-1)=vname3

CR       write(stdout,'(1x,A,I3)') 'indxTYadv = ',indxTYadv+itrc-1
       nametrc=vname(1,indxT+itrc-1)
       unitt=vname(3,indxT+itrc-1)
       write(vname1,*) trim(nametrc),'_yadv                       '
       vname(1,indxTYadv+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxTYadv+itrc-1)
       vname(2,indxTYadv+itrc-1)='Horizontal (eta) advection term '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxTYadv+itrc-1)=vname3

CR       write(stdout,'(1x,A,I3)') 'indxTVadv = ',indxTVadv+itrc-1
       nametrc=vname(1,indxT+itrc-1)
       unitt=vname(3,indxT+itrc-1)
       write(vname1,*) trim(nametrc),'_vadv                       ' 
       vname(1,indxTVadv+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxTVadv+itrc-1)
       vname(2,indxTVadv+itrc-1)='Vertical advection term         '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxTVadv+itrc-1)=vname3

CR       write(stdout,'(1x,A,I3)') 'indxTHmix = ',indxTHmix+itrc-1
       nametrc=vname(1,indxT+itrc-1)
       unitt=vname(3,indxT+itrc-1)
       write(vname1,*) trim(nametrc),'_hmix                       '
       vname(1,indxTHmix+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxTHmix+itrc-1)
       vname(2,indxTHmix+itrc-1)='Horizontal mixing term          '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxTHmix+itrc-1)=vname3

CR       write(stdout,'(1x,A,I3)') 'indxTVmix = ',indxTVmix+itrc-1
       nametrc=vname(1,indxT+itrc-1)
       unitt=vname(3,indxT+itrc-1)
       write(vname1,*) trim(nametrc),'_vmix                       ' 
       vname(1,indxTVmix+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxTVmix+itrc-1)
       vname(2,indxTVmix+itrc-1)='Vertical mixing term            '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxTVmix+itrc-1)=vname3
       
CR       write(stdout,'(1x,A,I3)') 'indxTbody = ',indxTbody+itrc-1
       nametrc=vname(1,indxT+itrc-1)
       unitt=vname(3,indxT+itrc-1)
       write(vname1,*) trim(nametrc),'_body                       '
       vname(1,indxTbody+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxTbody+itrc-1)
       vname(2,indxTbody+itrc-1)='Body Force term (Nudging & SWRAD)'
       write(vname3,*) trim(unitt),' second-1                      '
       vname(3,indxTbody+itrc-1)=vname3

CR       write(stdout,'(1x,A,I3)') 'indxTrate = ',indxTrate+itrc-1
       nametrc=vname(1,indxT+itrc-1)
       unitt=vname(3,indxT+itrc-1)
       write(vname1,*) trim(nametrc),'_rate                       '
       vname(1,indxTrate+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxTrate+itrc-1)
       vname(2,indxTrate+itrc-1)='Time rate of change             '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxTrate+itrc-1)=vname3
      enddo
# endif  /* DIAGNOSTICS_TS */

# ifdef DIAGNOSTICS_UV
      do itrc=1,2
CR       write(stdout,'(1x,A,I3)') 'indxMXadv = ',indxMXadv+itrc-1
       lvar=lenstr(vname(1,indxU+itrc-1))
       nametrc=vname(1,indxU+itrc-1)(1:lvar)
       lvar=lenstr(nametrc)
       unitt=vname(3,indxU+itrc-1)
       write(vname1,*) trim(nametrc),'_xadv                       '
       vname(1,indxMXadv+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxMxadv+itrc-1)
       vname(2,indxMXadv+itrc-1)='Horizontal (xi) advection term  '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxMXadv+itrc-1)=vname3

CR       write(stdout,'(1x,A,I3)') 'indxMYadv = ',indxMYadv+itrc-1
       nametrc=vname(1,indxU+itrc-1)
       unitt=vname(3,indxU+itrc-1)
       write(vname1,*) trim(nametrc),'_yadv                       '
       vname(1,indxMYadv+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxMYadv+itrc-1)
       vname(2,indxMYadv+itrc-1)='Horizontal (eta) advection term '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxMYadv+itrc-1)=vname3

CR       write(stdout,'(1x,A,I3)') 'indxMVadv = ',indxMVadv+itrc-1
       nametrc=vname(1,indxU+itrc-1)
       unitt=vname(3,indxU+itrc-1)
       write(vname1,*) trim(nametrc),'_vadv                       ' 
       vname(1,indxMVadv+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxMVadv+itrc-1)
       vname(2,indxMVadv+itrc-1)='Vertical advection term         '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxMVadv+itrc-1)=vname3

CR       write(stdout,'(1x,A,I3)') 'indxMCor = ',indxMCor+itrc-1
       nametrc=vname(1,indxU+itrc-1)
       unitt=vname(3,indxU+itrc-1)
       write(vname1,*) trim(nametrc),'_cor                        '
       vname(1,indxMCor+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxMcor+itrc-1)
       vname(2,indxMCor+itrc-1)='Coriolis term                    '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxMCor+itrc-1)=vname3

CR       write(stdout,'(1x,A,I3)') 'indxMPrsgrd = ',indxMPrsgrd+itrc-1
       nametrc=vname(1,indxU+itrc-1)
       unitt=vname(3,indxU+itrc-1)
       write(vname1,*) trim(nametrc),'_Prsgrd                     '
       vname(1,indxMPrsgrd+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxMPrsgrd+itrc-1)
       vname(2,indxMPrsgrd+itrc-1)='Pressure gradient term        '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxMPrsgrd+itrc-1)=vname3

CR       write(stdout,'(1x,A,I3)') 'indxMHmix = ',indxMHmix+itrc-1
       nametrc=vname(1,indxU+itrc-1)
       unitt=vname(3,indxU+itrc-1)
       write(vname1,*) trim(nametrc),'_hmix                       '
       vname(1,indxMHmix+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxMHmix+itrc-1)
       vname(2,indxMHmix+itrc-1)='Horizontal mixing term          '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxMHmix+itrc-1)=vname3

CR       write(stdout,'(1x,A,I3)') 'indxMVmix = ',indxMVmix+itrc-1
       nametrc=vname(1,indxU+itrc-1)
       unitt=vname(3,indxU+itrc-1)
       write(vname1,*) trim(nametrc),'_vmix                       ' 
       vname(1,indxMVmix+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxMVmix+itrc-1)
       vname(2,indxMVmix+itrc-1)='Vertical mixing term            '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxMVmix+itrc-1)=vname3
       
CR       write(stdout,'(1x,A,I3)') 'indxMrate = ',indxMrate+itrc-1
       nametrc=vname(1,indxU+itrc-1)
       unitt=vname(3,indxU+itrc-1)
       write(vname1,*) trim(nametrc),'_rate                       '
       vname(1,indxMrate+itrc-1)=vname1
CR       write(stdout,'(20x,A)') vname(1,indxMrate+itrc-1)
       vname(2,indxMrate+itrc-1)='Time rate of change             '
       write(vname3,*) trim(unitt),' second-1                     '
       vname(3,indxMrate+itrc-1)=vname3
      enddo
# endif /* DIAGNOSTICS_UV */

      return
      end
