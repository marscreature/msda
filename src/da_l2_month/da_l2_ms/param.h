! Dimensions of Physical Grid and array dimensions: 
! =========== == ======== ==== === ===== ============
! LLm,MMm  Number of the internal points of the PHYSICAL grid.
!          in the XI- and ETA-directions [physical side boundary
!          points and peroodic ghost points (if any) are excluded].
!
! Lm,Mm    Number of the internal points [see above] of array
!          covering a Message Passing subdomain. In the case when
!          no Message Passing partitioning is used, these two are
!          the same as LLm,MMm. 
!
! N        Number of vertical levels.
!
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=588,  MMmH=624, NnH=66)   !   <--  high resolution
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)

#if defined DAS_IN_MBFGS
      integer  LLm,Lm,  MMm,Mm
      parameter (LLm=LLmH,  MMm=MMmH)      !   <--  SPURS2
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
#else
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
# if defined BASIN
     &               LLm=180, MMm=140, N=12
# elif defined CANYON_A
     &               LLm=65,  MMm=48,  N=10
# elif defined CANYON_B
     &               LLm=66,  MMm=48,  N=12
# elif defined DAMEE_S
c**  &               LLm=192, MMm=96,  N=20
     &               LLm=113, MMm=58,  N=20
# elif defined DAMEE_B
     &               LLm=128, MMm=128, N=20
c**  &               LLm=256, MMm=256, N=20
# elif defined GRAV_ADJ
     &               LLm=128, MMm=4,   N=20
# elif defined NJ_BIGHT
c*   &               LLm=98,  MMm=206, N=12
     &               LLm=24,  MMm=34,  N=10
# elif defined NPACIFIC
     &               LLm=174, MMm=94,  N=20
# elif defined OVERFLOW
     &               LLm=4,   MMm=128, N=20
# elif defined SEAMOUNT
     &               LLm=48,  MMm=48,  N=11
c*   &               LLm=97,  MMm=96,  N=20
# elif defined SHELFRONT
     &               LLm=4,   MMm=40,  N=12 
# elif defined SOLITON
     &               LLm=96,  MMm=32,  N=1
c*   &               LLm=192, MMm=64,  N=1
c*   &               LLm=384, MMm=128, N=1
# elif defined TASMAN_SEA
     &               LLm=128, MMm=128, N=4
# elif defined UPWELLING
c     &               LLm=10,  MMm=12,  N=3
     &               LLm=16,  MMm=64,  N=16
c*   &               LLm=16,  MMm=128, N=32
# elif defined USWEST0
c*     &               LLm=83,  MMm=168, N=20 ! <-- MB_L1 nested
c*     &               LLm=93,  MMm=189, N=20 ! <-- MB_L2 nested
     &               LLm=LLmH,  MMm=MMmH, N=NnH   ! <-- MB_L3 nested
# elif defined USWEST
     &               LLm=LLmH,  MMm=MMmH,  N=NnH ! PWS
# else
     &                LLm=??, MMm=??, N=??
# endif
     &                                      )
      

!
! Domain subdivision parameters:
! ====== =========== ===========
! NPP            Maximum allowed number of parallel threads;
! NSUB_X,NSUB_E  Number of SHARED memory subdomains in XI- and
!                                                ETA-directions;
! NNODES        Total number of MPI processes (nodes);
! NP_XI,NP_ETA  Number of MPI subdomains in XI- and ETA-directions;
!
      integer NSUB_X, NSUB_E, NPP
# ifdef MPI
      integer NP_XI, NP_ETA, NNODES
      parameter (NP_XI=1, NP_ETA=4,  NNODES=NP_XI*NP_ETA,
     &                               Lm=LLm/NP_XI, Mm=MMm/NP_ETA)
      parameter (NSUB_X=1, NSUB_E=1, NPP=1)
# else
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
# endif
!
! Number of tracers and tracer identification indices:
! ====== == ======= === ====== ============== ========
!
#ifdef SOLVE3D                                                                                                                 
      integer NT, itemp,                                                                                                       
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed                                                                          
     &        , ntrc_diats, ntrc_diauv                                                                                         
# ifdef BIOLOGY                                                                                                                
     &        ,itrc_bio                                                                                                        
# endif                                                                                                                        
# ifdef SEDIMENT                                                                                                               
     &        ,itrc_sed                                                                                                        
# endif                                                                                                                        
# ifdef SALINITY                                                                                                               
     &          , isalt                                                                                                        
# endif                                                                                                                        
# ifdef PASSIVE_TRACER                                                                                                         
     &          , itpas                                                                                                        
# endif                                                                                                                        
# ifdef BIOLOGY                                                                                                                
#  ifdef BIO_NChlPZD                                                                                                           
     &          , iNO3_, iChla, iPhy1, iZoo1                                                                                   
     &          , iDet1                                                                                                        
#  elif defined BIO_N2ChlPZD2                                                                                                  
     &          , iNO3_, iNH4_, iChla, iPhy1, iZoo1                                                                            
     &          , iDet1, iDet2                                                                                                 
#  elif defined BIO_N2P2Z2D2                                                                                                   
     &          , iNO3_, iNH4_, iPhy1, iPhy2, iZoo1, iZoo2                                                                     
     &          , iDet1, iDet2                                                                                                 
#  endif                                                                                                                       
# endif                                                                                                                        
                                                                                                                               
# ifdef SEDIMENT                                                                                                               
     &          , isand, isilt                                                                                                 
     &          , NST, NLAY                                                                                                    
# endif                                                                                                                        
                                                                                                                               
      parameter (itemp=1)                                                                                                      
# ifdef SALINITY                                                                                                               
      parameter (ntrc_salt=1)                                                                                                  
      parameter (isalt=itemp+1)                                                                                                
# else                                                                                                                         
      parameter (ntrc_salt=0)                                                                                                  
#endif                                                                                                                         
             !SOLVE3D
# ifdef PASSIVE_TRACER                                                                                                         
      parameter (ntrc_pas=1)                                                                                                   
      parameter (itpas=itemp+ntrc_salt+1)                                                                                      
# else
      parameter (ntrc_pas=0)
# endif
# ifdef BIOLOGY
#  ifdef BIO_NChlPZD
      parameter (ntrc_bio=5,itrc_bio=itemp+ntrc_salt+ntrc_pas+1)
      parameter (iNO3_=itrc_bio, iChla=iNO3_+1,
     &           iPhy1=iNO3_+2,
     &           iZoo1=iNO3_+3,
     &           iDet1=iNO3_+4)
#  elif defined BIO_N2ChlPZD2
      parameter (ntrc_bio=7,itrc_bio=itemp+ntrc_salt+ntrc_pas+1)
      parameter (iNO3_=itrc_bio, iNH4_=iNO3_+1, iChla=iNO3_+2,
     &           iPhy1=iNO3_+3,
     &           iZoo1=iNO3_+4,
     &           iDet1=iNO3_+5, iDet2=iNO3_+6)
#  elif defined BIO_N2P2Z2D2
      parameter (ntrc_bio=8,itrc_bio=itemp+ntrc_salt+ntrc_pas+1)
      parameter (iNO3_=itrc_bio, iNH4_=iNO3_+1,
     &           iPhy1=iNO3_+2, iPhy2=iNO3_+3,
     &           iZoo1=iNO3_+4, iZoo2=iNO3_+5,
     &           iDet1=iNO3_+6, iDet2=iNO3_+7)
#  endif
# else
      parameter (ntrc_bio=0)
# endif
# ifdef SEDIMENT
! NST            Number of sediment (tracer) size classes
! NLAY           Number of layers in sediment bed
      parameter (NST=2, NLAY=2)
      parameter (ntrc_sed=NST,
     &             itrc_sed=itemp+ntrc_salt+ntrc_pas+ntrc_bio+1)
      parameter (isand=itrc_sed, isilt=isand+1)
# else
      parameter (ntrc_sed=0)
# endif
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
#ifdef DIAGNOSTICS_TS
      parameter (ntrc_diats=7*NT)
#else
      parameter (ntrc_diats=0)
#endif
#ifdef DIAGNOSTICS_UV
      parameter (ntrc_diauv=16)
#else 
      parameter (ntrc_diauv=0)
#endif
#endif /*SOLVE3D */
#ifdef STATIONS 
      integer NS           ! Number of output stations (if any).
      parameter (NS=5)     ! ====== == ====== ======== === =====
#endif           
#ifdef PSOURCE   
      integer Msrc         ! Number of point sources, if any
      parameter (Msrc=10)  ! ====== == ====== ======== === =
#endif
#ifdef FLOATS    
       integer Mfloats          ! Maximum number of floats
       parameter (Mfloats=32000)! ====== == ====== ========
#endif
#ifdef STATIONS 
       integer Msta          ! Maximum of stations
       parameter (Msta=1000) ! ======= == ========
#endif           
#if defined SSH_TIDES || defined UV_TIDES
      integer Ntides
      parameter (Ntides=8)
#endif

!
! Derived dimension parameters.
!
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)

#endif    /* DAS_IN_MBFGS */
!
! Number maximum of weights for the barotropic mode
!
      integer NWEIGHT
      parameter (NWEIGHT=137)
