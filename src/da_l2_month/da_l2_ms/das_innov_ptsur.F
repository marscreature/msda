#include "cppdefs.h"

      subroutine das_innov_ptsur (tile)
      implicit none
# include "param.h"
# include "das_param.h"
# include "scalars.h"
# include "grid.h"
# include "das_ocean.h"
# include "das_covar.h"
# include "das_innov.h"

      integer i,j,ii,jj,k,tile,kdas
      real srad,rmin,rr,crs,crt,cfs,cft,ro
!
!

      if (tile .gt. prf_num_ptsur) goto 99
!
! 1  map to model grid
!
      ro =      (lonr(Lm/2,Mm/2)-lonr(Lm/2+1,Mm/2+1))
     &          *(lonr(Lm/2,Mm/2)-lonr(Lm/2+1,Mm/2+1))
     &          +(latr(Lm/2,Mm/2)-latr(Lm/2+1,Mm/2+1))
     &          *(latr(Lm/2,Mm/2)-latr(Lm/2+1,Mm/2+1))

      srad=2.0*ro

! obs at > srad, not used
      rmin=srad
      ii=-999
      jj=-999
!
      do j=0,Mm+1
        do i=0,Lm+1
          rr = (lonr(i,j)-lon_ptsur(tile))
     &        *(lonr(i,j)-lon_ptsur(tile))
     &        +(latr(i,j)-lat_ptsur(tile))
     &        *(latr(i,j)-lat_ptsur(tile))
          if (rr .le. rmin) then
            rmin=rr
            ii=i
            jj=j
          endif
        enddo
      enddo
      Iptsur(tile)=ii
      Jptsur(tile)=jj
!
! mask
!
      if (ii .ge. 0 ) then
        do k=1,max_prf
          if ( t_ptsur(tile,k) .gt. 0.0 .and.
     &         s_ptsur(tile,k) .gt. 0.0 ) then
            mask_ptsur(tile,k)=1.0
          else
            mask_ptsur(tile,k)=0.0
          endif
        enddo
      else
        do k=1,max_prf
          mask_ptsur(tile,k)=0.0
        enddo
        Iptsur(tile)=0
        Jptsur(tile)=0
      endif
!
! rmask_das
!
! note: t_ptsur k=1:max_prf, k=1 => ndas
!
      do k=1,max_prf
        kdas=ndas-k+1
        mask_ptsur(tile,k)=mask_ptsur(tile,k)
     &        *rmask_das(Iptsur(tile),Jptsur(tile),kdas)
      enddo
!
! implement simple QC and compute innovations
!
      crt=2.5
      crs=0.5
      do k=1,max_prf
        kdas=ndas-k+1
        cft=t_ptsur(tile,k)
     &       - t_das(Iptsur(tile),Jptsur(tile),kdas,itemp)
        if (abs(cft) .gt. crt ) mask_ptsur(tile,k)=0.0
        t_ptsur(tile,k)=cft*mask_ptsur(tile,k)
!
        cfs=s_ptsur(tile,k)
     &       - t_das(Iptsur(tile),Jptsur(tile),kdas,isalt)
        if (abs(cfs) .gt. crs ) mask_ptsur(tile,k)=0.0
        s_ptsur(tile,k)=cfs*mask_ptsur(tile,k)
      enddo
!
99    continue
      return
      end
