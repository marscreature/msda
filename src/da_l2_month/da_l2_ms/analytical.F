#include "cppdefs.h"
!
!  ANALYTICAL PACKAGE:
!--------------------------------------------------------------------
!
!  This package is used to provide various analytical fields to the
!  model when appropriate.
!
!  Routines:
!
!  ana_bmflux_tile   Analytical kinematic bottom momentum flux.
!  ana_btflux_tile   Analytical kinematic bottom flux of tracer
!                          type variables.
!  ana_bsedim_tile   Analytical bottom sediment grain size
!                          and density.
!  ana_meanRHO_tile  Analytical mean density anomaly.                
!  ana_smflux_tile   Analytical kinematic surface momentum flux
!                          (wind stress).
!  ana_srflux_tile   Analytical kinematic surface shortwave
!                          radiation.
!  ana_ssh_tile      Analytical sea surface height climatology.      
!  ana_sst_tile      Analytical sea surface temperature and dQdSST  
!                         which are used during heat flux correction.
!  ana_stflux_tile   Analytical kinematic surface flux of tracer type
!                          variables.
!  ana_tclima_tile   Analytical tracer climatology fields.  
!  ana_uclima_tile   Analytical tracer climatology fields.  
!  ana_wwave_tile    Analytical wind induced wave amplitude,
!                         direction and period.
!-------------------------------------------------------------------
!
# if !defined OPENMP
      integer function omp_get_thread_num()
      omp_get_thread_num=0
      return
      end
      integer function omp_get_num_threads()
      omp_get_num_threads=1
      return
      end
      subroutine mp_setlock ()
      return
      end
      subroutine mp_unsetlock ()
      return
      end
#else
      subroutine omp_get_empty
      return
      end
#endif

