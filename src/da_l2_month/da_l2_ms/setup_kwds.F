#include "cppdefs.h"

      subroutine setup_kwds (ierr)

!!!!!! WARNING: THIS IS A MACHINE GENERATED CODE, DO NOT EDIT! !!!!!!
!!!!!! This file needs to be updated only if new keywords were !!!!!!
!!!!!! introduced into "read_inp.F". To create or refresh this !!!!!!
!!!!!! file use compile and execute "checkkwds.F" as an        !!!!!!
!!!!!! independent program, or use commands "make checkkwds"   !!!!!!
!!!!!! or "make depend".                                       !!!!!!

      implicit none
      integer ierr, is,ie
#include "param.h"
#include "strings.h"
#ifdef MPI
# include "scalars.h"
#endif
      do is=1,max_opt_size
        Coptions(is:is)=' '
      enddo
      is=1
#ifdef MPI
#endif
#ifdef SOLVE3D
#endif
#if defined SOLITON
#elif defined SEAMOUNT
#elif defined UPWELLING
#elif defined USWEST
#elif defined BASIN
#elif defined DAMEE_B
#else
#endif
#ifdef MPI
#else
#endif
#ifdef AVERAGES
#endif
      ie=is + 5
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='title'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='time_stepping'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#ifdef SOLVE3D
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='S-coord'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='initial'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#ifdef ANA_INITIAL
#endif
#if defined MPI && defined PARALLEL_FILES
#endif
#ifdef ANA_INITIAL
#endif
#ifndef ANA_GRID
      ie=is + 4
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='grid'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# if defined MPI && defined PARALLEL_FILES
# endif
#endif
#if !defined ANA_SMFLUX || defined SOLVE3D && (\
    !defined ANA_STFLUX || !defined ANA_BTFLUX  \
  ||(defined SG_BBL96    && !defined ANA_BSEDIM) \
  ||(defined SG_BBL96    && !defined ANA_WWAVE)  \
  ||(defined QCORRECTION && !defined ANA_SST)    \
  ||(defined SALINITY    && !defined ANA_SSFLUX) \
  ||(defined LMD_SKPP     && !defined ANA_SRFLUX))
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='forcing'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# if defined MPI && defined PARALLEL_FILES
# endif
#endif
#if (defined TCLIMATOLOGY && !defined ANA_TCLIMA) || \
    (defined ZNUDGING && !defined ANA_SSH)
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='climatology'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# if defined MPI && defined PARALLEL_FILES
# endif
#endif
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='restart'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# if defined MPI && defined PARALLEL_FILES
# endif
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='history'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# if defined MPI && defined PARALLEL_FILES
# endif
#ifdef AVERAGES
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='averages'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# if defined MPI && defined PARALLEL_FILES
# endif
#endif
#ifdef STATIONS
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='stations'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef FLOATS
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='floats'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef ASSIMILATION
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='assimilation'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
      ie=is +22
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='primary_history_fields'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#ifdef SOLVE3D
#endif
#ifdef SOLVE3D
#endif
#ifdef SOLVE3D
      ie=is +24
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='auxiliary_history_fields'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# ifdef SALINITY
#  ifdef LMD_SKPP
#  endif
# endif
# ifdef SALINITY
#  ifdef LMD_SKPP
#  endif
# endif
# ifdef SALINITY
#  ifdef LMD_SKPP
#  endif
# endif
#endif /* SOLVE3D */
#ifdef AVERAGES
      ie=is +16
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='primary_averages'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# ifdef SOLVE3D
# endif
# ifdef SOLVE3D
# endif
# ifdef SOLVE3D
      ie=is +18
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='auxiliary_averages'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#  ifdef SALINITY
#   ifdef LMD_SKPP
#   endif
#  endif
#  ifdef SALINITY
#   ifdef LMD_SKPP
#   endif
#  endif
#  ifdef SALINITY
#   ifdef LMD_SKPP
#   endif
#  endif
# endif /* SOLVE3D */
#endif /* AVERAGES */
      ie=is + 4
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='rho0'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#if defined UV_VIS2 || defined UV_VIS4
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='lateral_visc'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#endif
#ifdef UV_VIS2
#endif
#ifdef UV_VIS4
#endif
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='bottom_drag'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='gamma2'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#ifdef SOLVE3D
# ifdef TS_DIF2
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='tracer_diff2'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# endif
# ifdef TS_DIF4
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='tracer_diff4'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# endif
# if !defined LMD_MIXING && !defined BVF_MIXING\
  && !defined MY2_MIXING && !defined MY25_MIXING\
                         && !defined PP_MIXING
      ie=is +15
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='vertical_mixing'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# endif
# ifdef MY25_MIXING
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='MY_bak_mixing'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
#  ifdef Q_DIF2
#  endif
#  ifdef Q_DIF4
#  endif
# endif
# ifdef BODYFORCE
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='bodyforce'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# endif
# ifdef SPONGE
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='sponge'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# endif
# ifdef TNUDGING
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='nudg_cof'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# endif
# ifndef NONLIN_EOS
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='lin_EOS_cff'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
# endif
#endif
#ifdef STATIONS
#endif
#ifdef MPI
#endif
      return
  99  MPI_master_only write(stdout,'(/1x,A,A/14x,A)')
     &  'SETUP_KWDS ERROR: Unsufficient size of string Coptions',
     &  'in file "strings.h".', 'Increase the size it and recompile.'
      ierr=ierr+1
      return
      end
