      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
!
! minimization lbfgs
!
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)     !T
     &        +NDAS*(Lm+2)*(Mm+2)     !S
#if !defined DAS_GEOS_STRONG || defined DAS_HFRADAR
     &        +NDAS*(Lm+1)*(Mm+1)     !psi
     &        +NDAS*(Lm+2)*(Mm+2)     !chi
#endif
#if !defined DAS_HYDRO_STRONG
     &        +(Lm+2)*(Mm+2)
#endif
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
!
! in-situ observation
!
      integer max_prf
      PARAMETER( max_prf=NDAS)   !!! fixed
!
      integer max_sio, max_whoi, max_flight,
     &        max_hfradar,max_hfradar6,max_js1
      PARAMETER(max_sio=100,max_whoi=100,max_flight=30000,
     &         max_hfradar=39000,max_hfradar6=40000,max_js1=8000)
! ctd
!
      integer max_ptsur
      PARAMETER(max_ptsur=100)
      integer max_martn
      PARAMETER(max_martn=100)
      integer max_moor
      PARAMETER(max_moor=5000)
!
! inversion of observational error variance
! hcmin, h<hcmin, no ssh assimilated
!
      real sio_ot_raw,sio_os_raw,whoi_ot_raw,whoi_os_raw,ptsur_ot,ptsur_os,
     &                   martn_ot,martn_os,moor_ot_raw,moor_os_raw,
     &                   hf_ouv,hf_ouv6,js1_ossh,swot_ossh,hcmin,sats_osss
      PARAMETER( sio_ot_raw=1.0/(1.5*1.5),sio_os_raw=1.0/(0.21*0.21),
     &        whoi_ot_raw=1.0/(1.2*1.2),whoi_os_raw=1.0/(0.21*0.21),
     &        moor_ot_raw=1.0/(0.8*0.8),moor_os_raw=1.0/(0.07*0.07),
     &        ptsur_ot=1.0/(1.00*1.00),ptsur_os=1.0/(0.15*0.15),
     &        martn_ot=1.0/(1.00*1.00),martn_os=1.0/(0.15*0.15),
     &        js1_ossh=1.0/(0.03*0.03), swot_ossh=1.0/(0.1*0.1),
     &        hcmin=200.0, sats_osss=1.0/(0.13*0.13),
     &        hf_ouv=1.0/(0.09*0.09),hf_ouv6=1.0/(0.10*0.10)
     &         )
!
! auv
!
! CalPoly
      integer max_prf_cal,max_cal
      PARAMETER(max_prf_cal=NDAS,max_cal=100)
! Dorado
      integer max_prf_dor,max_dor
      PARAMETER(max_prf_dor=NDAS,max_dor=180)
!
! inversion of observational error variance
!
      real cal_ot,cal_os,dor_ot,dor_os
      PARAMETER( cal_ot=1.0/(1.5*1.5),
     &           cal_os=1.0/(0.25*0.25),
     &           dor_ot=1.0/(1.5*1.5),
     &           dor_os=1.0/(0.25*0.25)
     &         )
!
! geostrophic ratio
!
      real geo_ratio, cross_tsp
      PARAMETER( geo_ratio = 1.0, cross_tsp=0.)
      integer sm_rad   ! for smoothing radius
      PARAMETER( sm_rad=4)

      integer Local_len
      PARAMETER( Local_len=36)   ! correlation set zero beyond this length

      real sz_rad                ! smoothing steric SSHs for geostrophic SSHs
      PARAMETER( sz_rad=4)
      integer sz_rad_len                ! smoothing steric SSHs for geostrophic SSHs
      PARAMETER( sz_rad_len=15)         ! sz_rad_len should be 3 times larger that sz_rad

      real regp, reguv               !  Regular parameter                                                       
      PARAMETER( regp=0.4/(10.*10.) )
      PARAMETER( reguv=0.5/(0.6*0.6)  )   ! uv error 0.55 m/s
