      integer function checkdims (ncid, ncname, lstr, recsize)
      implicit none
      character*(*)  ncname,     dimname*16
      integer ncid,    lstr,     ndims,  nvars, ngatts, recdim,
     &        recsize, ierr,     dimsize,    i,   ldim,  lenstr
      integer  LLmH,  MMmH, NnH
      parameter (LLmH=606,  MMmH=438, NnH=66)
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLmH+2-nhalf)/nratio-1,
     &           MMm_lr=(MMmH+2-nhalf)/nratio-1)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLm_lr,  MMm=MMm_lr,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
      integer indxU, indxV, indxT
      parameter (indxU=5, indxV=6, indxT=7)
      integer indxS
      parameter (indxS=indxT+1)
      integer indxO, indxW, indxR, indxVisc, indxAkv, indxAkt
      parameter (indxO=indxT+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed
     &                                  +ntrc_diats+ntrc_diauv+1,
     &           indxW=indxO+1, indxR=indxO+2,
     &           indxVisc=indxR+1,
     &           indxAkv=indxR+2, indxAkt=indxAkv+1)
      integer indxAks
      parameter (indxAks=indxAkt+1)
      integer indxSSH
      parameter (indxSSH=indxAkt+4)
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
      integer indxSHFl, indxSWRad
      parameter (indxSHFl=indxSSH+3)
      integer indxSSFl
      parameter (indxSSFl=indxSHFl+1, indxSWRad=indxSHFl+2)
      integer indxSST, indxdQdSST
      parameter (indxSST=indxSWRad+1, indxdQdSST=indxSWRad+2)
      integer indxSSS
      parameter (indxSSS=indxSST+2)
      integer indxBostr
      parameter (indxBostr=indxSUSTR+16)
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     &                u3dvar, v3dvar, p3dvar, w3dvar, b3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8,b3dvar=12)
      integer xi_rho,xi_u, eta_rho,eta_v
      parameter (xi_rho=LLm+2,  xi_u=xi_rho-1,
     &           eta_rho=MMm+2, eta_v=eta_rho-1)
      integer ncidfrc, ncidbulk, ncidclm,  ntsms
     &      , ntsrf,  ntssh,  ntsst, ntsss, ntuclm, ntww,
     &        ntbulk
      integer nttclm(NT),    ntstf(NT)
      integer ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV
      integer rstT(NT)
      integer  ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,  hisV,  hisR,    hisHbl, hisHbbl
     &      , hisO,  hisW,  hisVisc,  hisAkv,  hisAkt, hisAks
     &      , hisBostr
      integer hisT(NT)
      integer   ncidavg, nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,  avgV,  avgR,    avgHbl, avgHbbl
     &      , avgO,  avgW,  avgVisc, avgAkv,  avgAkt, avgAks
      integer avgT(NT)
      integer avgBostr
      logical wrthis(60+NT)
     &      , wrtavg(60+NT)
      common/incscrum/
     &        ncidfrc, ncidbulk,ncidclm, ntsms, ntsrf, ntssh, ntsst
     &      , ntuclm, ntsss, ntww, ntbulk
     &                        ,  nttclm,          ntstf
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV,   rstT
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT,    hisR
     &      , hisO,    hisW,     hisVisc, hisAkv,  hisAkt, hisAks
     &      , hisHbl,  hisHbbl,  hisBostr
     &      , ncidavg,  nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT,    avgR
     &      , avgO,    avgW,     avgVisc,  avgAkv,  avgAkt, avgAks
     &      , avgHbl,  avgHbbl
     &      , avgBostr
     &      , wrthis
     &      , wrtavg
      character*80 date_str, title
      character*80 ininame,  grdname,  hisname
     &         ,   rstname,  frcname,  bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
      character*52  vname(4, 150)
      common /cncscrum/       date_str,   title
     &         ,   ininame,  grdname, hisname
     &         ,   rstname,  frcname, bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
     &                      ,  vname
      include 'netcdf.inc'
      recsize=0
      ierr=nf_inq (ncid, ndims, nvars, ngatts, recdim)
      if (ierr .ne. nf_noerr) then
        write(stdout,'(/1x,4A/)') 'CHECKDIMS ERROR while inquiring ',
     &                  'about netCDF file ''', ncname(1:lstr), '''.'
      else
        do i=1,ndims
          ierr=nf_inq_dim (ncid, i, dimname, dimsize)
          if (ierr .ne. nf_noerr) then
            write(stdout,'(/1x,2A,I3/8x,3A/)') 'CHECKDIMS ERROR ',
     &                  'while inquiring about dimension ID =', i,
     &                  'in netCDF file ''', ncname(1:lstr), '''.'
            goto 99
          endif
          ldim=lenstr(dimname)
          if ((ldim.eq.6 .and. dimname(1:ldim).eq.'xi_rho') .or.
     &        (ldim.eq.4 .and. dimname(1:ldim).eq.'xi_v'  )) then
            if (dimsize.ne.xi_rho) then
              write(stdout,1) dimname(1:ldim), dimsize, xi_rho
              goto 99
            endif
          elseif ((ldim.eq.4 .and. dimname(1:ldim).eq.'xi_u'  ) .or.
     &            (ldim.eq.6 .and. dimname(1:ldim).eq.'xi_psi')) then
            if (dimsize.ne.xi_u) then
              write(stdout,1) dimname(1:ldim), dimsize, xi_u
              goto 99
            endif
          elseif ((ldim.eq.7 .and. dimname(1:ldim).eq.'eta_rho') .or.
     &            (ldim.eq.5 .and. dimname(1:ldim).eq.'eta_u' )) then
            if (dimsize.ne.eta_rho) then
              write(stdout,1) dimname(1:ldim), dimsize, eta_rho
              goto 99
            endif
          elseif ((ldim.eq.7 .and. dimname(1:ldim).eq.'eta_v'  ) .or.
     &            (ldim.eq.5 .and. dimname(1:ldim).eq.'eta_psi'))then
            if (dimsize.ne.eta_v) then
              write(stdout,1) dimname(1:ldim), dimsize, eta_v
              goto 99
            endif
          elseif (ldim.eq.5 .and. dimname(1:ldim).eq.'s_rho') then
            if (dimsize.ne.N) then
              write(stdout,1) dimname(1:ldim), dimsize, N
              goto 99
            endif
          elseif (ldim.eq.3 .and. dimname(1:ldim).eq.'s_w') then
            if (dimsize.ne.N+1) then
              write(stdout,1) dimname(1:ldim), dimsize, N+1
              goto 99
            endif
          elseif (i.eq.recdim) then
            recsize=dimsize
          endif
        enddo
        checkdims=nf_noerr
        return
      endif
  1   format(/' CHECKDIMS ERROR: inconsistent size of dimension ''',
     &                      A, ''':', i5, 1x, '(must be', i5, ').'/)
  99  checkdims=nf_noerr+1
      return
      end
