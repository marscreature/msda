      implicit none
      integer  LLmH,  MMmH, NnH
      parameter (LLmH=606,  MMmH=438, NnH=66)
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLmH+2-nhalf)/nratio-1,
     &           MMm_lr=(MMmH+2-nhalf)/nratio-1)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLm_lr,  MMm=MMm_lr,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+1)*(Mm+1)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_prf
      PARAMETER( max_prf=NDAS)
      integer max_sio, max_whoi, max_flight,
     &        max_hfradar,max_hfradar6,max_js1
      PARAMETER(max_sio=100,max_whoi=100,max_flight=30000,
     &         max_hfradar=39000,max_hfradar6=40000,max_js1=8000)
      integer max_ptsur
      PARAMETER(max_ptsur=100)
      integer max_martn
      PARAMETER(max_martn=100)
      integer max_moor
      PARAMETER(max_moor=5000)
      real sio_ot_raw,sio_os_raw,whoi_ot_raw,whoi_os_raw,ptsur_ot,
     &                                                         ptsur_os,
     &                   martn_ot,martn_os,moor_ot_raw,moor_os_raw,
     &                   hf_ouv,hf_ouv6,js1_ossh,swot_ossh,hcmin,
     &                                                         sats_osss
      PARAMETER( sio_ot_raw=1.0/(1.5*1.5),sio_os_raw=1.0/(0.21*0.21),
     &        whoi_ot_raw=1.0/(1.2*1.2),whoi_os_raw=1.0/(0.21*0.21),
     &        moor_ot_raw=1.0/(0.8*0.8),moor_os_raw=1.0/(0.07*0.07),
     &        ptsur_ot=1.0/(1.00*1.00),ptsur_os=1.0/(0.15*0.15),
     &        martn_ot=1.0/(1.00*1.00),martn_os=1.0/(0.15*0.15),
     &        js1_ossh=1.0/(0.03*0.03), swot_ossh=1.0/(0.1*0.1),
     &        hcmin=200.0, sats_osss=1.0/(0.13*0.13),
     &        hf_ouv=1.0/(0.09*0.09),hf_ouv6=1.0/(0.10*0.10)
     &         )
      integer max_prf_cal,max_cal
      PARAMETER(max_prf_cal=NDAS,max_cal=100)
      integer max_prf_dor,max_dor
      PARAMETER(max_prf_dor=NDAS,max_dor=180)
      real cal_ot,cal_os,dor_ot,dor_os
      PARAMETER( cal_ot=1.0/(1.5*1.5),
     &           cal_os=1.0/(0.25*0.25),
     &           dor_ot=1.0/(1.5*1.5),
     &           dor_os=1.0/(0.25*0.25)
     &         )
      real geo_ratio, cross_tsp
      PARAMETER( geo_ratio = 1.0, cross_tsp=0.)
      integer sm_rad
      PARAMETER( sm_rad=4)
      integer Local_len
      PARAMETER( Local_len=36)
      real sz_rad
      PARAMETER( sz_rad=4)
      integer sz_rad_len
      PARAMETER( sz_rad_len=15)
      real regp, reguv
      PARAMETER( regp=0.4/(10.*10.) )
      PARAMETER( reguv=0.5/(0.6*0.6)  )
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      real  x_sponge,   v_sponge
       real  tauT_in, tauT_out, tauM_in, tauM_out
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &                      , x_sponge,   v_sponge
     &                      , tauT_in, tauT_out, tauM_in, tauM_out
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
      integer indxU, indxV, indxT
      parameter (indxU=5, indxV=6, indxT=7)
      integer indxS
      parameter (indxS=indxT+1)
      integer indxO, indxW, indxR, indxVisc, indxAkv, indxAkt
      parameter (indxO=indxT+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed
     &                                  +ntrc_diats+ntrc_diauv+1,
     &           indxW=indxO+1, indxR=indxO+2,
     &           indxVisc=indxR+1,
     &           indxAkv=indxR+2, indxAkt=indxAkv+1)
      integer indxAks
      parameter (indxAks=indxAkt+1)
      integer indxSSH
      parameter (indxSSH=indxAkt+4)
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
      integer indxSHFl, indxSWRad
      parameter (indxSHFl=indxSSH+3)
      integer indxSSFl
      parameter (indxSSFl=indxSHFl+1, indxSWRad=indxSHFl+2)
      integer indxSST, indxdQdSST
      parameter (indxSST=indxSWRad+1, indxdQdSST=indxSWRad+2)
      integer indxSSS
      parameter (indxSSS=indxSST+2)
      integer indxBostr
      parameter (indxBostr=indxSUSTR+16)
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     &                u3dvar, v3dvar, p3dvar, w3dvar, b3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8,b3dvar=12)
      integer xi_rho,xi_u, eta_rho,eta_v
      parameter (xi_rho=LLm+2,  xi_u=xi_rho-1,
     &           eta_rho=MMm+2, eta_v=eta_rho-1)
      integer ncidfrc, ncidbulk, ncidclm,  ntsms
     &      , ntsrf,  ntssh,  ntsst, ntsss, ntuclm, ntww,
     &        ntbulk
      integer nttclm(NT),    ntstf(NT)
      integer ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV
      integer rstT(NT)
      integer  ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,  hisV,  hisR,    hisHbl, hisHbbl
     &      , hisO,  hisW,  hisVisc,  hisAkv,  hisAkt, hisAks
     &      , hisBostr
      integer hisT(NT)
      integer   ncidavg, nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,  avgV,  avgR,    avgHbl, avgHbbl
     &      , avgO,  avgW,  avgVisc, avgAkv,  avgAkt, avgAks
      integer avgT(NT)
      integer avgBostr
      logical wrthis(60+NT)
     &      , wrtavg(60+NT)
      common/incscrum/
     &        ncidfrc, ncidbulk,ncidclm, ntsms, ntsrf, ntssh, ntsst
     &      , ntuclm, ntsss, ntww, ntbulk
     &                        ,  nttclm,          ntstf
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV,   rstT
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT,    hisR
     &      , hisO,    hisW,     hisVisc, hisAkv,  hisAkt, hisAks
     &      , hisHbl,  hisHbbl,  hisBostr
     &      , ncidavg,  nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT,    avgR
     &      , avgO,    avgW,     avgVisc,  avgAkv,  avgAkt, avgAks
     &      , avgHbl,  avgHbbl
     &      , avgBostr
     &      , wrthis
     &      , wrtavg
      character*80 date_str, title
      character*80 ininame,  grdname,  hisname
     &         ,   rstname,  frcname,  bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
      character*52  vname(4, 150)
      common /cncscrum/       date_str,   title
     &         ,   ininame,  grdname, hisname
     &         ,   rstname,  frcname, bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
     &                      ,  vname
      real h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hinv(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real f(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fomn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_h/h /grid_hinv/hinv /grid_f/f /grid_fomn/fomn
      real angler(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_angler/angler
      real latr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real lonr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_latr/latr /grid_lonr/lonr
      real pm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_pm/pm    /metrics_pn/pn
     &       /metrics_omu/om_u /metrics_on_u/on_u
     &       /metrics_omv/om_v /metrics_on_v/on_v
      real dmde(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real dndx(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_dmde/dmde    /metrics_dndx/dndx
      real on_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmon_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pnom_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real grdscl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_on_p/on_p /metrics_om_p/om_p
     &       /metrics_on_r/on_r /metrics_om_r/om_r
     &       /metrics_pmon_u/pmon_u /metrics_pnom_v/pnom_v
     &                              /metrics_grdscl/grdscl
      real rmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /mask_r/rmask /mask_p/pmask
     &       /mask_u/umask /mask_v/vmask
      real u(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real v(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real t(0:Lm+1+padd_X,0:Mm+1+padd_E,N,NT)
      common /ocean_u/u /ocean_v/v /ocean_t/t
      real Hz(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real Hz_bak(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real z_r(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real z_w(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      real Huon(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real Hvom(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real W(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      common /grid_Hz/Hz    /grid_zr/z_r  /grid_W/W
     &  /grid_Hz_bak/Hz_bak /grid_zw/z_w  /grid_Huon/Huon
     &                                    /grid_Hvom/Hvom
      real rho(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real rhop0(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      common /ocean_rho/rho /ocean_rhop0/rhop0
      real zeta(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real ubar(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vbar(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real rzeta_bak(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real rubar_bak(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real rvbar_bak(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_zeta/zeta  /rhs_rzeta/rzeta_bak
     &       /ocean_ubar/ubar  /rhs_rubar/rubar_bak
     &       /ocean_vbar/vbar  /rhs_rvbar/rvbar_bak
      real zeta_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_das/u_das /ocean_v_das/v_das
     &    /ocean_psi_das/psi_das /ocean_chi_das/chi_das
     &    /ocean_t_das/t_das /ocean_zeta_das/zeta_das
      real zeta_s(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real zeta_h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      real t_w(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s/u_s /ocean_v_s/v_s
     &      /ocean_psi_s/psi_s /ocean_chi_s/chi_s
     &      /ocean_rho_s/rho_s /ocean_p_s/p_s
     &     /ocean_t_s/t_s /ocean_zeta_s/zeta_s
     &     /ocean_zeta_h/zeta_h /ocean_t_w/t_w
      real zeta_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real psi_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common
     &     /ocean_psi_adj/psi_adj /ocean_chi_adj/chi_adj
     &     /ocean_t_adj/t_adj /ocean_zeta_adj/zeta_adj
      real zeta_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real zeta_h_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      real t_w_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s_adj/u_s_adj /ocean_v_s_adj/v_s_adj
     &   /ocean_psi_s_adj/psi_s_adj /ocean_chi_s_adj/chi_s_adj
     &     /ocean_rho_s_adj/rho_s_adj /ocean_p_s_adj/p_s_adj
     &     /ocean_t_s_adj/t_s_adj /ocean_zeta_s_adj/zeta_s_adj
     &     /ocean_zeta_h_adj/zeta_h_adj /ocean_t_w_adj/t_w_adj
      real rmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real pmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real umask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real vmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /mask_r_das/rmask_das /mask_p_das/pmask_das
     &       /mask_u_das/umask_das /mask_v_das/vmask_das
      integer nzr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nzr_das/nzr_das
     &       /ocean_nzu_das/nzu_das /ocean_nzv_das/nzv_das
      integer nsr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nsr_das/nsr_das
     &       /ocean_nsu_das/nsu_das /ocean_nsv_das/nsv_das
      real georatio(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /cgeoratio/georatio
      real z_das(ndas),tc1d_das(ndas),sc1d_das(ndas)
      common /grid_z_das/z_das,tc1d_das,sc1d_das
      real zeta_das9(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real t_das9(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      real rho_das9(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /ocean_t_das9/t_das9 /ocean_zeta_das9/zeta_das9
     &       /ocean_rho_das9/rho_das9
      real bz_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real bu_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real bv_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real bpsi_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real bchi_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real bt_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common
     &     /ocean_bu_das/bu_das /ocean_bv_das/bv_das
     &     /ocean_bpsi_das/bpsi_das /ocean_bchi_das/bchi_das
     &     /ocean_bt_das/bt_das /ocean_bz_das/bz_das
      real czE_das(0:Mm+1+padd_E,0:Mm+1+padd_E)
      real czX_das(0:Lm+1+padd_X,0:Lm+1+padd_X)
      real cuE_das(0:Mm+1+padd_E,0:Mm+1+padd_E)
      real cuX_das(0:Lm+1+padd_X,0:Lm+1+padd_X)
      real cuZ_das(NDAS,NDAS)
      real cvE_das(0:Mm+1+padd_E,0:Mm+1+padd_E)
      real cvX_das(0:Lm+1+padd_X,0:Lm+1+padd_X)
      real cvZ_das(NDAS,NDAS)
      real cpsiE_das(0:Mm+1+padd_E,0:Mm+1+padd_E)
      real cpsiX_das(0:Lm+1+padd_X,0:Lm+1+padd_X)
      real cpsiZ_das(NDAS,NDAS)
      real cchiE_das(0:Mm+1+padd_E,0:Mm+1+padd_E)
      real cchiX_das(0:Lm+1+padd_X,0:Lm+1+padd_X)
      real cchiZ_das(NDAS,NDAS)
      real ctE_das(0:Mm+1+padd_E,0:Mm+1+padd_E,NT)
      real ctX_das(0:Lm+1+padd_X,0:Lm+1+padd_X,NT)
      real ctZ_das(ndas,ndas,NT)
      common /ocean_cze_das/czE_das /ocean_czX_das/czX_das
     &       /ocean_cue_das/cuE_das /ocean_cuX_das/cuX_das
     &       /ocean_cuz_das/cuZ_das
     &       /ocean_cve_das/cvE_das /ocean_cvX_das/cvX_das
     &       /ocean_cvz_das/cvZ_das
     &       /ocean_cpsie_das/cpsiE_das
     &       /ocean_cpsix_das/cpsiX_das
     &       /ocean_cpsiz_das/cpsiZ_das
     &       /ocean_cchie_das/cchiE_das
     &       /ocean_cchix_das/cchiX_das
     &       /ocean_cchiz_das/cchiZ_das
     &       /ocean_cte_das/ctE_das /ocean_ctX_das/ctX_das
     &       /ocean_ctz_das/ctZ_das
      real corr_ts(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /ocean_cross_ts/corr_ts
      character*99 file_corr
      common /corr_file/file_corr
      character*99 file_corr_vert
      common /corr_file_vert/file_corr_vert
      character*99 file_bvar
      common /bvar_file_vert/file_bvar
      real costf, cost0, costf_adj
      real xmin(NDIM)
      real xgrd(NDIM)
      real smin(NXM)
      real ymin(NXM)
      real diag(NDIM)
      real wmin(NWORK)
      common /das_miniz/xmin,xgrd,smin,ymin,diag,wmin
      common /das_cost/costf, cost0, costf_adj
      real sst_mc(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real mc_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real mc_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_sst_mc/sst_mc
     &       /innov_mask_mc/mc_mask
     &       /innov_oin_mc/mc_oin
      real sst_goes(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real goes_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real goes_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_sst_goes/sst_goes
     &       /innov_mask_goes/goes_mask
     &       /innov_oin_goes/goes_oin
      real ssh_swot(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real swot_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real swot_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_ssh_swot/ssh_swot
     &       /innov_mask_swot/swot_mask
     &       /innov_oin_swot/swot_oin
      real sss_sats(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real sats_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real sats_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_sss_sats/sss_sats
     &       /innov_mask_sats/sats_mask
     &       /innov_oin_sats/sats_oin
      real ssh_js1(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real js1_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real js1_oin(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_ssh_js1/ssh_js1
     &       /innov_mask_js1/js1_mask
     &       /innov_oin_js1/js1_oin
      real lon_js1(max_js1),lat_js1(max_js1),
     &      ssh_js1_raw(max_js1)
      common /innov_ssh_js1_raw/lon_js1,lat_js1,ssh_js1_raw
      real ssh_ref(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real ref_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ssh_ref_yavg/ssh_ref
     &       /ssh_ref_mask/ref_mask
      real sst_fl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fl_mask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_sst_fl/sst_fl
     &       /innov_mask_fl/fl_mask
      real lon_fl(max_flight),lat_fl(max_flight),
     &      sst_fl_raw(max_flight)
      common /innov_sst_fl_raw/lon_fl,lat_fl,sst_fl_raw
      real u_hf(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real v_hf(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_u_hf/u_hf
     &       /innov_v_hf/v_hf
     &       /innov_umask_hf/hf_umask
     &       /innov_vmask_hf/hf_vmask
      real lon_hf(max_hfradar),lat_hf(max_hfradar),
     &      amperr_hf(max_hfradar),fitdif_hf(max_hfradar),
     &      u_hf_raw(max_hfradar),v_hf_raw(max_hfradar)
      common /innov_uv_hf_raw/lon_hf,lat_hf,
     &       amperr_hf,fitdif_hf,u_hf_raw,v_hf_raw
      real u_hf6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real v_hf6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_umask6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_vmask6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_uierr6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hf_vierr6(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /innov_u_hf6/u_hf6
     &       /innov_v_hf6/v_hf6
     &       /innov_umask_hf6/hf_umask6
     &       /innov_vmask_hf6/hf_vmask6
     &       /innov_uierr_hf6/hf_uierr6
     &       /innov_vierr_hf6/hf_vierr6
      real lon_hf6(max_hfradar6),lat_hf6(max_hfradar6),
     &      amperr_hf6(max_hfradar6),fitdif_hf6(max_hfradar6),
     &      u_hf_raw6(max_hfradar6),v_hf_raw6(max_hfradar6)
      common /innov_uv_hf_raw6/lon_hf6,lat_hf6,
     &       amperr_hf6,fitdif_hf6,u_hf_raw6,v_hf_raw6
      real t_cal(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_cal),
     &     s_cal(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_cal)
      real cal_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_cal)
      common /innov_temp_cal/t_cal
     &       /innov_salt_cal/s_cal
     &       /innov_mask_cal/cal_mask
      real lon_cal(max_cal,max_prf_cal),
     &     lat_cal(max_cal,max_prf_cal),
     &     t_cal_raw(max_cal,max_prf_cal),
     &     s_cal_raw(max_cal,max_prf_cal)
      common /innov_cal_raw/
     &     lon_cal,lat_cal,t_cal_raw,s_cal_raw
      real t_dor(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_dor),
     &     s_dor(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_dor)
      real dor_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf_dor)
      common /innov_temp_dor/t_dor
     &       /innov_salt_dor/s_dor
     &       /innov_mask_dor/dor_mask
      real lon_dor(max_dor,max_prf_dor),
     &     lat_dor(max_dor,max_prf_dor),
     &     t_dor_raw(max_dor,max_prf_dor),
     &     s_dor_raw(max_dor,max_prf_dor)
      common /innov_dor_raw/
     &     lon_dor,lat_dor,t_dor_raw,s_dor_raw
      real t_sio(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real s_sio(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real sio_t_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real sio_s_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      common /innov_t_sio/t_sio
     &       /innov_s_sio/s_sio
     &       /innov_mask_tsio/sio_t_mask
     &       /innov_mask_ssio/sio_s_mask
      real   t_sio_raw(max_sio,max_prf),
     &       s_sio_raw(max_sio,max_prf)
      real lon_sio(max_sio), lat_sio(max_sio)
      real sio_ot(max_prf), sio_os(max_prf)
      common /sio_glides_st_raw/t_sio_raw, s_sio_raw,
     &                   lon_sio, lat_sio, sio_ot, sio_os
      real t_whoi(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real s_whoi(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real whoi_t_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real whoi_s_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      common /innov_t_whoi/t_whoi
     &       /innov_s_whoi/s_whoi
     &       /innov_mask_twhoi/whoi_t_mask
     &       /innov_mask_swhoi/whoi_s_mask
      real   t_whoi_raw(max_whoi,max_prf),
     &       s_whoi_raw(max_whoi,max_prf)
      real lon_whoi(max_whoi), lat_whoi(max_whoi)
      real whoi_ot(max_prf), whoi_os(max_prf)
      common /whoi_glides_st_raw/t_whoi_raw, s_whoi_raw,
     &             lon_whoi, lat_whoi,whoi_ot, whoi_os
      real s_ptsur(max_ptsur,max_prf),
     &     t_ptsur(max_ptsur,max_prf),
     &     mask_ptsur(max_ptsur,max_prf)
      real lon_ptsur(max_ptsur), lat_ptsur(max_ptsur)
      common /ptsur_cdt_st/s_ptsur,t_ptsur,
     &                      lon_ptsur,lat_ptsur,
     &                      mask_ptsur
      integer Iptsur(max_ptsur),Jptsur(max_ptsur)
      common /ptsur_cdt_loc/Iptsur,Jptsur
      real s_martn(max_martn,max_prf),
     &     t_martn(max_martn,max_prf),
     &     mask_martn(max_martn,max_prf)
      real lon_martn(max_martn), lat_martn(max_martn)
      common /martn_cdt_st/s_martn,t_martn,
     &                      lon_martn,lat_martn,
     &                      mask_martn
      integer Imartn(max_martn),Jmartn(max_martn)
      common /martn_cdt_loc/Imartn,Jmartn
      real t_moor(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real s_moor(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real moor_t_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      real moor_s_mask(0:Lm+1+padd_X,0:Mm+1+padd_E,max_prf)
      common /innov_t_moor/t_moor
     &       /innov_s_moor/s_moor
     &       /innov_mask_tmoor/moor_t_mask
     &       /innov_mask_smoor/moor_s_mask
      real   t_moor_raw(max_moor,max_prf),
     &       s_moor_raw(max_moor,max_prf)
      real lon_moor(max_moor), lat_moor(max_moor)
      real moor_ot(max_prf), moor_os(max_prf)
      common /moor_ctd_raw/t_moor_raw, s_moor_raw,
     &         lon_moor, lat_moor,moor_ot,moor_os
      integer prf_num_sio,prf_num_whoi,prf_num_moor,
     &            prf_num_ptsur,prf_num_martn,num_flight,
     &            num_hfradar,num_hfradar6,num_js1
      common /insitu_num/prf_num_sio,prf_num_whoi,prf_num_moor,
     &            prf_num_ptsur,prf_num_martn,num_flight,
     &            num_hfradar,num_hfradar6,num_js1
      integer num_cal(max_prf_cal),num_dor(max_prf_dor)
      common /insitu_num_cal/num_cal,num_dor
      logical flag_tmi, flag_mc, flag_goes,flag_js1,
     &        flag_swot,flag_tp,flag_sio,flag_whoi,flag_ptsur,
     &        flag_martn,flag_flight,flag_cal,flag_dor,
     &        flag_moor,flag_prof,flag_ship,flag_hfradar,
     &        flag_hfradar6, flag_ssh_ref,
     &        flag_sats
      common /innov_flags/flag_tmi, flag_mc, flag_goes,
     &       flag_js1,flag_swot,flag_tp,flag_sio,flag_whoi,flag_ptsur,
     &        flag_martn,flag_flight,flag_cal,flag_dor,
     &        flag_moor,flag_prof,flag_ship,flag_hfradar,
     &        flag_hfradar6, flag_ssh_ref,
     &        flag_sats
      character*99 file_tmi,file_mc,file_goes
      common /innov_file_sst/file_tmi,file_mc,file_goes
      character*99 file_js1, file_swot,file_tp, file_sats
      common /innov_file_ssh/file_js1,file_swot, file_tp, file_sats
      character*99 file_sio_glider,
     &             file_whoi_glider
      common /innov_file_glider/file_sio_glider, file_whoi_glider
      character*99 file_ptsur_ctd,file_martn_ctd,
     &           file_moor_ctd,file_prof_ctd
      common /innov_file_ctd/file_ptsur_ctd,
     &          file_martn_ctd,file_moor_ctd,file_prof_ctd
      character*99 file_flight_sst,file_cal_auv,
     &            file_dor_auv,file_ship_sst
      common /innov_file_flight/file_flight_sst,
     &              file_cal_auv,file_dor_auv,file_ship_sst
      character*99 file_hfradar_uv,file_hfradar_uv6
      common /innov_file_HF/file_hfradar_uv,file_hfradar_uv6
      character*99 file_dist_coast
      common /dist_coast_file/file_dist_coast
      character*99 file_ssh_ref
      common /ssh_ref_file/file_ssh_ref
      character*99 file_ssh_ref_js1
      common /ssh_ref_file_js1/file_ssh_ref_js1
      integer time_level
      common /das_time_level/time_level
      character*99 file_innov_sio, file_innov_whoi
      common /innov_print_file/file_innov_sio,
     &                         file_innov_whoi
      character*99 file_innov_ana_sio, file_innov_ana_whoi
      common /innov_print_ana_file/file_innov_ana_sio,
     &                         file_innov_ana_whoi
      real anc(Lm+2,Mm+2)
      common /temp_nc/anc
      integer tile, subs, trd, ierr, ios, nsyn
      integer i,j,k,max_tile
      REAL EPS,GTOL
      INTEGER LP,IPRINT(2),IFLAG ,POINT,ITER,MP,ICALL
      INTEGER MAXITER
      LOGICAL DIAGCO
      EXTERNAL VA15CD
      COMMON /VA15DD/MP,LP, GTOL
      real costpp
      call read_inp (ierr)
      if (ierr.ne.0) goto 100
      call init_scalars (ierr)
      if (ierr.ne.0) goto 100
      call das_init_scalars
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call start_timers()
        call init_arrays (tile)
        call das_init_arrays (tile)
         enddo
       enddo
      write(*,*) '-12'
      call get_grid
      if (may_day_flag.ne.0) goto 99
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call setup_grid1 (tile)
         enddo
       enddo
      write(*,*) '-11'
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call setup_grid2 (tile)
         enddo
       enddo
      write(*,*) '-10'
      call set_scoord
      write(*,*) ' -9'
        call DAS_GET_INITIAL
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call das_setup (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call das_copy (tile)
        call das_fill9 (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call das_extrapolate9 (tile)
        call das_rho_eos9 (tile)
         enddo
       enddo
      call das_read_inp
      write(*,'(6x, A, 2x, I3)') 'das time level', time_level
      call das_get_bvar
      do k=1,ndas
        do j=0,Mm+1
          do i=0,Lm+1
             bt_das(i,j,k,itemp)=1.3*bt_das(i,j,k,itemp)+0.05
             bt_das(i,j,k,isalt)=0.5*bt_das(i,j,k,isalt)+0.0005
             bpsi_das(i,j,k)=0.1*10.*bpsi_das(i,j,k)
             bchi_das(i,j,k)=0.1*5.*bchi_das(i,j,k)
          enddo
        enddo
      enddo
      do j=0,Mm+1
        do i=0,Lm+1
           bz_das(i,j)=6.0*bz_das(i,j)
        enddo
      enddo
      call das_set_geo_ratio
      write(*,*) 'geo_ratio'
      write(*,*) (georatio(50,j),j=1,MMm)
      write(*,*) 'bt_var'
      write(*,*) (bt_das(30,30,k,itemp),k=1,ndas)
      write(*,*) 'bs_var'
      write(*,*) (bt_das(30,30,k,isalt),k=1,ndas)
      write(*,*) 'bpsi_var'
      write(*,*) (bpsi_das(30,30,k),k=1,ndas)
      write(*,*) 'bchi_var'
      write(*,*) (bchi_das(30,30,k),k=1,ndas)
      write(*,*) 'bz_var'
      write(*,*) (bz_das(30,k), k=20,30)
      call das_get_corr
      call das_get_insitu
      call das_get_hfradar
      call das_get_auv
      call das_get_mc
      call das_get_goes
      call das_get_swot
      call das_get_js1
      call das_get_sats
      write (*,'(9(2x,A))') 'obs num:', 'hfradar','hfradar6','mooring',
     &                       'flight','whoi','sio',
     &                       'ptsur','martin'
      write (*,'(10x,8I7)') num_hfradar,num_hfradar6,prf_num_moor,
     &                      num_flight,prf_num_whoi,prf_num_sio,
     &                      prf_num_ptsur,prf_num_martn
      if(.not. flag_tmi .and. .not. flag_mc .and. .not. flag_goes
     &   .and. .not. flag_swot .and. .not. flag_sats
     &   .and. .not. flag_js1 .and. .not. flag_tp
     &   .and. .not. flag_sio .and. .not. flag_whoi
     &   .and. .not. flag_ptsur .and. .not. flag_martn
     &   .and. .not. flag_moor .and. .not. flag_prof
     &   .and. .not. flag_ship
     &   .and. .not. flag_cal .and. .not. flag_dor
     &   .and. .not. flag_flight
     &   .and. .not. flag_hfradar.and. .not. flag_hfradar6) then
        write(*,'(6x, A, 2(/6x, A))')
     &              '!!! NO ANY OBSERVATION AVAILABLE',
     &              '!!! DATA ASSIMILATION NOT EXECUTED',
     &              '!!! NO ANALYSIS FILE'
        goto 99
      endif
      max_tile=max(numthreads,prf_num_ptsur)
C$OMP PARALLEL DO PRIVATE(trd,subs, k), SHARED(numthreads, max_tile)
      do k=1,max_tile
        call DAS_INNOV_PTSUR(k)
      enddo
      max_tile=max(numthreads,prf_num_martn)
C$OMP PARALLEL DO PRIVATE(trd,subs, k), SHARED(numthreads, max_tile)
      do k=1,max_tile
        call DAS_INNOV_MARTN(k)
      enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call das_innov_sio(tile)
        call das_innov_moor(tile)
         call das_innov_whoi(tile)
        call das_innov_flight(tile)
        call das_innov_calauv(tile)
        call das_innov_dorauv(tile)
        call das_innov_swot(tile)
        call das_innov_js1(tile)
        call das_innov_sats(tile)
        call das_innov_sst(tile)
        call das_innov_hfradar (tile)
         enddo
       enddo
      ICALL=0
      IFLAG=0
      MAXITER=36
      IPRINT(1)= 1
      IPRINT(2)= 0
      DIAGCO= .FALSE.
      EPS= 1.0D-4
      LP = 6
      MP = 6
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_CA2ZERO (tile)
         enddo
       enddo
      CALL DAS_CA2VEC
  10  CONTINUE
      COSTF=0.0
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL das_cross_ts(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_MATVEC(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_VARVEC (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_COST_REG(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_LIN_RHO_EOS (tile)
        CALL DAS_DIAG_SSH_STERIC (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_COST_SWOT(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_COST_JS1(tile)
        CALL DAS_DIAG_PRESSURE (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_PRESSURE_SMOOTH (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_COPY_PRESSURE_SM (tile)
         enddo
       enddo
       costpp=costf
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_GEOS_UV_P (tile)
        CALL DAS_GEO_RATIO (tile)
        CALL DAS_PSICHI_UV (tile)
        CALL DAS_COST_HFRADAR (tile)
         enddo
       enddo
      costpp=costf-costpp
      write(*,*) '  HF COST=', costpp, costf
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_COST_FLIGHT(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_COST_CALAUV(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_COST_DORAUV(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_COST_MOOR(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_COST_GLIDER_SIO(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_COST_GLIDER_WHOI(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_COST_SATS(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_COST_SST(tile)
         enddo
       enddo
      CALL DAS_COST_GLIDER
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_COSTB(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_ADJ_ZERO(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_ADJ_COSTB(tile)
         enddo
       enddo
      CALL DAS_ADJ_CGLIDER
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_ADJ_CSST(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_ADJ_CSATS(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_ADJ_GLIDER_WHOI(tile)
        CALL DAS_ADJ_GLIDER_SIO(tile)
        CALL DAS_ADJ_MOOR(tile)
        CALL DAS_ADJ_DAUV(tile)
        CALL DAS_ADJ_CAUV(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_ADJ_CFLIGHT(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_ADJ_CHFRADAR (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_ADJ_PSICHI_UV (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_ADJ_GEO_RATIO (TILE)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_ADJ_GEOS_UV_P (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_ADJ_COPY_PRESSURE_SM (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_ADJ_PRESSURE_SM (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_ADJ_DIAG_P (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_ADJ_CJS1(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_ADJ_CSWOT(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_ADJ_DIAG_SSH_STERIC (tile)
        CALL DAS_ADJ_RHO_EOS (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_ADJ_COST_REG (tile)
        CALL DAS_ADJ_VARVEC (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_ADJ_MATVEC(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL DAS_ADJ_CROSS_TS(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL DAS_ADJ_ZERO_S(tile)
         enddo
       enddo
      CALL DAS_GA2VEC
      CALL mbfgs(NDIM,MSAVE,COSTF,DIAGCO,IPRINT,EPS,POINT,IFLAG)
      CALL DAS_VEC2CA
      IF(IFLAG.LE.0) GO TO 50
      ICALL=ICALL + 1
      IF(ICALL.GT.MAXITER) GO TO 50
      GOTO 10
  50  CONTINUE
      print*, '     FLAG=', IFLAG
      if (IFLAG .lt. 0) then
        write(*, '(6x, A, (/6x, A))')
     &   '!!! MINIMIZATION NOT COMPLETED',
     &   '!!! NO ASSIMILATED ANALYSIS GENERATED'
        goto 99
      else
        write(*, '(6x, A)')
     &   ' MINIMIZATION COMPLETED'
      endif
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL das_cross_ts(tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call das_matvec (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call das_varvec (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call das_innov_smooth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call das_copy_smooth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call das_lin_rho_eos (tile)
        call das_diag_ssh_steric (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call das_zeta_smooth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call das_copy_zeta_smooth (tile)
        call das_diag_pressure (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        CALL das_pressure_smooth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        CALL das_copy_pressure_sm (tile)
         enddo
       enddo
      print*, 'p_s'
      print*, (p_s(15,15,k), k=1,NDAS)
      print*, 'rho'
      print*, (rho_s(15,15,k), k=1,NDAS)
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call das_geos_uv_p (tile)
        call das_geo_ratio (tile)
        call das_psichi_uv (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call das_innov_uv_smooth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call das_copy_uv_smooth (tile)
         enddo
       enddo
      write(*,*) '     iic==', iic
      write(*,*) 't_s j=20 '
      write(*,*) (t_s(i,20,ndas,1), i=0,100)
      call das_wrt
  99  continue
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call stop_timers()
         enddo
       enddo
      call closecdf
 100  continue
      stop
      end
