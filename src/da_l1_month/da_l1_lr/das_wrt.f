      subroutine das_wrt
      implicit none
      include 'netcdf.inc'
      integer  LLmH,  MMmH, NnH
      parameter (LLmH=606,  MMmH=438, NnH=66)
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLmH+2-nhalf)/nratio-1,
     &           MMm_lr=(MMmH+2-nhalf)/nratio-1)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLm_lr,  MMm=MMm_lr,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+1)*(Mm+1)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_prf
      PARAMETER( max_prf=NDAS)
      integer max_sio, max_whoi, max_flight,
     &        max_hfradar,max_hfradar6,max_js1
      PARAMETER(max_sio=100,max_whoi=100,max_flight=30000,
     &         max_hfradar=39000,max_hfradar6=40000,max_js1=8000)
      integer max_ptsur
      PARAMETER(max_ptsur=100)
      integer max_martn
      PARAMETER(max_martn=100)
      integer max_moor
      PARAMETER(max_moor=5000)
      real sio_ot_raw,sio_os_raw,whoi_ot_raw,whoi_os_raw,ptsur_ot,
     &                                                         ptsur_os,
     &                   martn_ot,martn_os,moor_ot_raw,moor_os_raw,
     &                   hf_ouv,hf_ouv6,js1_ossh,swot_ossh,hcmin,
     &                                                         sats_osss
      PARAMETER( sio_ot_raw=1.0/(1.5*1.5),sio_os_raw=1.0/(0.21*0.21),
     &        whoi_ot_raw=1.0/(1.2*1.2),whoi_os_raw=1.0/(0.21*0.21),
     &        moor_ot_raw=1.0/(0.8*0.8),moor_os_raw=1.0/(0.07*0.07),
     &        ptsur_ot=1.0/(1.00*1.00),ptsur_os=1.0/(0.15*0.15),
     &        martn_ot=1.0/(1.00*1.00),martn_os=1.0/(0.15*0.15),
     &        js1_ossh=1.0/(0.03*0.03), swot_ossh=1.0/(0.1*0.1),
     &        hcmin=200.0, sats_osss=1.0/(0.13*0.13),
     &        hf_ouv=1.0/(0.09*0.09),hf_ouv6=1.0/(0.10*0.10)
     &         )
      integer max_prf_cal,max_cal
      PARAMETER(max_prf_cal=NDAS,max_cal=100)
      integer max_prf_dor,max_dor
      PARAMETER(max_prf_dor=NDAS,max_dor=180)
      real cal_ot,cal_os,dor_ot,dor_os
      PARAMETER( cal_ot=1.0/(1.5*1.5),
     &           cal_os=1.0/(0.25*0.25),
     &           dor_ot=1.0/(1.5*1.5),
     &           dor_os=1.0/(0.25*0.25)
     &         )
      real geo_ratio, cross_tsp
      PARAMETER( geo_ratio = 1.0, cross_tsp=0.)
      integer sm_rad
      PARAMETER( sm_rad=4)
      integer Local_len
      PARAMETER( Local_len=36)
      real sz_rad
      PARAMETER( sz_rad=4)
      integer sz_rad_len
      PARAMETER( sz_rad_len=15)
      real regp, reguv
      PARAMETER( regp=0.4/(10.*10.) )
      PARAMETER( reguv=0.5/(0.6*0.6)  )
      real zeta_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_das/u_das /ocean_v_das/v_das
     &    /ocean_psi_das/psi_das /ocean_chi_das/chi_das
     &    /ocean_t_das/t_das /ocean_zeta_das/zeta_das
      real zeta_s(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real zeta_h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      real t_w(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s/u_s /ocean_v_s/v_s
     &      /ocean_psi_s/psi_s /ocean_chi_s/chi_s
     &      /ocean_rho_s/rho_s /ocean_p_s/p_s
     &     /ocean_t_s/t_s /ocean_zeta_s/zeta_s
     &     /ocean_zeta_h/zeta_h /ocean_t_w/t_w
      real zeta_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real psi_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common
     &     /ocean_psi_adj/psi_adj /ocean_chi_adj/chi_adj
     &     /ocean_t_adj/t_adj /ocean_zeta_adj/zeta_adj
      real zeta_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real zeta_h_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      real t_w_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s_adj/u_s_adj /ocean_v_s_adj/v_s_adj
     &   /ocean_psi_s_adj/psi_s_adj /ocean_chi_s_adj/chi_s_adj
     &     /ocean_rho_s_adj/rho_s_adj /ocean_p_s_adj/p_s_adj
     &     /ocean_t_s_adj/t_s_adj /ocean_zeta_s_adj/zeta_s_adj
     &     /ocean_zeta_h_adj/zeta_h_adj /ocean_t_w_adj/t_w_adj
      real rmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real pmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real umask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real vmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /mask_r_das/rmask_das /mask_p_das/pmask_das
     &       /mask_u_das/umask_das /mask_v_das/vmask_das
      integer nzr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nzr_das/nzr_das
     &       /ocean_nzu_das/nzu_das /ocean_nzv_das/nzv_das
      integer nsr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nsr_das/nsr_das
     &       /ocean_nsu_das/nsu_das /ocean_nsv_das/nsv_das
      real georatio(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /cgeoratio/georatio
      real z_das(ndas),tc1d_das(ndas),sc1d_das(ndas)
      common /grid_z_das/z_das,tc1d_das,sc1d_das
      real h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hinv(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real f(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fomn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_h/h /grid_hinv/hinv /grid_f/f /grid_fomn/fomn
      real angler(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_angler/angler
      real latr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real lonr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_latr/latr /grid_lonr/lonr
      real pm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_pm/pm    /metrics_pn/pn
     &       /metrics_omu/om_u /metrics_on_u/on_u
     &       /metrics_omv/om_v /metrics_on_v/on_v
      real dmde(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real dndx(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_dmde/dmde    /metrics_dndx/dndx
      real on_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmon_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pnom_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real grdscl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_on_p/on_p /metrics_om_p/om_p
     &       /metrics_on_r/on_r /metrics_om_r/om_r
     &       /metrics_pmon_u/pmon_u /metrics_pnom_v/pnom_v
     &                              /metrics_grdscl/grdscl
      real rmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /mask_r/rmask /mask_p/pmask
     &       /mask_u/umask /mask_v/vmask
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
      integer indxU, indxV, indxT
      parameter (indxU=5, indxV=6, indxT=7)
      integer indxS
      parameter (indxS=indxT+1)
      integer indxO, indxW, indxR, indxVisc, indxAkv, indxAkt
      parameter (indxO=indxT+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed
     &                                  +ntrc_diats+ntrc_diauv+1,
     &           indxW=indxO+1, indxR=indxO+2,
     &           indxVisc=indxR+1,
     &           indxAkv=indxR+2, indxAkt=indxAkv+1)
      integer indxAks
      parameter (indxAks=indxAkt+1)
      integer indxSSH
      parameter (indxSSH=indxAkt+4)
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
      integer indxSHFl, indxSWRad
      parameter (indxSHFl=indxSSH+3)
      integer indxSSFl
      parameter (indxSSFl=indxSHFl+1, indxSWRad=indxSHFl+2)
      integer indxSST, indxdQdSST
      parameter (indxSST=indxSWRad+1, indxdQdSST=indxSWRad+2)
      integer indxSSS
      parameter (indxSSS=indxSST+2)
      integer indxBostr
      parameter (indxBostr=indxSUSTR+16)
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     &                u3dvar, v3dvar, p3dvar, w3dvar, b3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8,b3dvar=12)
      integer xi_rho,xi_u, eta_rho,eta_v
      parameter (xi_rho=LLm+2,  xi_u=xi_rho-1,
     &           eta_rho=MMm+2, eta_v=eta_rho-1)
      integer ncidfrc, ncidbulk, ncidclm,  ntsms
     &      , ntsrf,  ntssh,  ntsst, ntsss, ntuclm, ntww,
     &        ntbulk
      integer nttclm(NT),    ntstf(NT)
      integer ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV
      integer rstT(NT)
      integer  ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,  hisV,  hisR,    hisHbl, hisHbbl
     &      , hisO,  hisW,  hisVisc,  hisAkv,  hisAkt, hisAks
     &      , hisBostr
      integer hisT(NT)
      integer   ncidavg, nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,  avgV,  avgR,    avgHbl, avgHbbl
     &      , avgO,  avgW,  avgVisc, avgAkv,  avgAkt, avgAks
      integer avgT(NT)
      integer avgBostr
      logical wrthis(60+NT)
     &      , wrtavg(60+NT)
      common/incscrum/
     &        ncidfrc, ncidbulk,ncidclm, ntsms, ntsrf, ntssh, ntsst
     &      , ntuclm, ntsss, ntww, ntbulk
     &                        ,  nttclm,          ntstf
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV,   rstT
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT,    hisR
     &      , hisO,    hisW,     hisVisc, hisAkv,  hisAkt, hisAks
     &      , hisHbl,  hisHbbl,  hisBostr
     &      , ncidavg,  nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT,    avgR
     &      , avgO,    avgW,     avgVisc,  avgAkv,  avgAkt, avgAks
     &      , avgHbl,  avgHbbl
     &      , avgBostr
     &      , wrthis
     &      , wrtavg
      character*80 date_str, title
      character*80 ininame,  grdname,  hisname
     &         ,   rstname,  frcname,  bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
      character*52  vname(4, 150)
      common /cncscrum/       date_str,   title
     &         ,   ininame,  grdname, hisname
     &         ,   rstname,  frcname, bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
     &                      ,  vname
      character start_day*20, vnts*4
      integer i,j,k,lstr, lenstr, das_nf_fwrite, indx,varid,itrc
* error status return
      integer  iret
* netCDF id
      integer  ncid
* dimension ids
      integer  xi_rho_dim
      integer  xi_u_dim
      integer  eta_rho_dim
      integer  eta_v_dim
      integer  s_rho_dim
      integer  time_dim
* dimension lengths
      integer  xi_rho_len
      integer  xi_u_len
      integer  eta_rho_len
      integer  eta_v_len
      integer  s_rho_len
      integer  time_len
      parameter (time_len = NF_UNLIMITED)
* variable ids
      integer  depth_id
      integer  u_da_id
      integer  v_da_id
      integer  t_da_id
      integer  s_da_id
      integer  zeta_da_id
      integer  mask_da_id
      integer  lat_da_id
      integer  lon_da_id
* rank (number of dimensions) for each variable
      integer  depth_rank
      integer  u_da_rank
      integer  v_da_rank
      integer  t_da_rank
      integer  s_da_rank
      integer  mask_da_rank
      integer  lat_da_rank
      integer  lon_da_rank
      integer  zeta_da_rank
      parameter (depth_rank = 1)
      parameter (u_da_rank = 3)
      parameter (v_da_rank = 3)
      parameter (t_da_rank = 3)
      parameter (s_da_rank = 3)
      parameter (mask_da_rank = 3)
      parameter (lat_da_rank = 2)
      parameter (lon_da_rank = 2)
      parameter (zeta_da_rank = 2)
* variable shapes
      integer  depth_dims(depth_rank)
      integer  u_da_dims(u_da_rank)
      integer  v_da_dims(v_da_rank)
      integer  t_da_dims(t_da_rank)
      integer  s_da_dims(s_da_rank)
      integer  mask_da_dims(mask_da_rank)
      integer  lat_da_dims(lat_da_rank)
      integer  lon_da_dims(lon_da_rank)
      integer  zeta_da_dims(zeta_da_rank)
      real doubleval(1)
      integer nc_start(3)
      integer nc_count(3)
      integer nc_start2(2)
      integer nc_count2(2)
      lstr=lenstr(rstname)
* enter define mode
      iret = nf_create(rstname(1:lstr), NF_CLOBBER, ncid)
      call check_err(iret)
* define dimensions
      iret = nf_def_dim(ncid, 'xi_rho', xi_rho, xi_rho_dim)
      call check_err(iret)
      iret = nf_def_dim(ncid, 'xi_u', xi_u, xi_u_dim)
      call check_err(iret)
      iret = nf_def_dim(ncid, 'eta_rho', eta_rho, eta_rho_dim)
      call check_err(iret)
      iret = nf_def_dim(ncid, 'eta_v', eta_v, eta_v_dim)
      call check_err(iret)
      iret = nf_def_dim(ncid, 'ndas', ndas, s_rho_dim)
      call check_err(iret)
* define variables
      u_da_dims(3) =  s_rho_dim
      u_da_dims(2) =  eta_rho_dim
      u_da_dims(1) = xi_u_dim
      iret = nf_def_var(ncid, 'u_da', NF_DOUBLE, u_da_rank,
     &       u_da_dims, u_da_id)
      call check_err(iret)
      v_da_dims(3) =  s_rho_dim
      v_da_dims(2) =  eta_v_dim
      v_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 'v_da', NF_DOUBLE, v_da_rank,
     &       v_da_dims, v_da_id)
      call check_err(iret)
      t_da_dims(3) =  s_rho_dim
      t_da_dims(2) =  eta_rho_dim
      t_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 't_da', NF_DOUBLE, t_da_rank,
     &       t_da_dims, t_da_id)
      call check_err(iret)
      s_da_dims(3) =  s_rho_dim
      s_da_dims(2) =  eta_rho_dim
      s_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 's_da', NF_DOUBLE, s_da_rank,
     &       s_da_dims, s_da_id)
      call check_err(iret)
      mask_da_dims(3) =  s_rho_dim
      mask_da_dims(2) =  eta_rho_dim
      mask_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 'mask_da', NF_DOUBLE, mask_da_rank,
     &       mask_da_dims, mask_da_id)
      call check_err(iret)
      zeta_da_dims(2) =  eta_rho_dim
      zeta_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 'zeta_da', NF_DOUBLE, zeta_da_rank,
     &       zeta_da_dims, zeta_da_id)
      call check_err(iret)
      lat_da_dims(2) =  eta_rho_dim
      lat_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 'lat_da', NF_DOUBLE, lat_da_rank,
     &       lat_da_dims, lat_da_id)
      call check_err(iret)
      lon_da_dims(2) =  eta_rho_dim
      lon_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 'lon_da', NF_DOUBLE, lon_da_rank,
     &       lon_da_dims, lon_da_id)
      call check_err(iret)
* assign attributes
      iret = nf_put_att_text(ncid, u_da_id, 'long_name', 1, 'u')
      call check_err(iret)
      doubleval(1) = 0.0
      call check_err(iret)
      iret = nf_put_att_text(ncid, v_da_id, 'long_name', 1, 'v')
      call check_err(iret)
      iret = nf_put_att_text(ncid, t_da_id, 'long_name', 4, 'temp')
      call check_err(iret)
      iret = nf_put_att_text(ncid, s_da_id, 'long_name', 4, 'salt')
      call check_err(iret)
      iret = nf_put_att_text(ncid, mask_da_id, 'long_name', 4, 'mask')
      call check_err(iret)
      iret = nf_put_att_text(ncid, zeta_da_id, 'long_name', 4, 'zeta')
      call check_err(iret)
      iret = nf_put_att_text(ncid, lat_da_id, 'long_name', 8,
     &    'latitude')
      call check_err(iret)
      iret = nf_put_att_text(ncid, lon_da_id, 'long_name', 9,
     &      'longitude')
      call check_err(iret)
      iret = nf_put_att_text(ncid, NF_GLOBAL, 'type', 11, 'ROMS SCB LR')
      call check_err(iret)
      iret = nf_put_att_text(ncid, NF_GLOBAL, 'data', 16,
     &     'ROMS vert interp')
      call check_err(iret)
* leave define mode
      iret = nf_enddef(ncid)
      call check_err(iret)
      iret = nf_close(ncid)
      call check_err(iret)
      indx=1
      iret = NF_OPEN(rstname(1:lstr),NF_WRITE,ncid)
      iret=nf_inq_varid (ncid, 'zeta_da', varid)
      if (iret .eq. nf_noerr) then
        iret=das_nf_fwrite (zeta_s(0,0), ncid, varid,
     &                                         indx, r2dvar)
        if (iret .ne. nf_noerr) then
          write(stdout,2) 'zeta_da',indx,rstname(1:lstr)
          goto 99
        endif
      else
        write(stdout,1) 'zeta_da', rstname(1:lstr)
        goto 99
      endif
      iret=nf_inq_varid (ncid, 'lat_da', varid)
      if (iret .eq. nf_noerr) then
        iret=das_nf_fwrite (latr(0,0), ncid, varid,
     &                                         indx, r2dvar)
        if (iret .ne. nf_noerr) then
          write(stdout,2) 'lat_da',indx,rstname(1:lstr)
          goto 99
        endif
      else
        write(stdout,1) 'lat_da', rstname(1:lstr)
        goto 99
      endif
      iret=nf_inq_varid (ncid, 'lon_da', varid)
      if (iret .eq. nf_noerr) then
        iret=das_nf_fwrite (lonr(0,0), ncid, varid,
     &                                         indx, r2dvar)
        if (iret .ne. nf_noerr) then
          write(stdout,2) 'lon_da',indx,rstname(1:lstr)
          goto 99
        endif
      else
        write(stdout,1) 'lon_da', rstname(1:lstr)
        goto 99
      endif
      iret=nf_inq_varid (ncid, 'u_da', varid)
      if (iret .eq. nf_noerr) then
        iret=das_nf_fwrite (u_s(0,0,1), ncid, varid,
     &                                        indx, u3dvar)
        if (iret .ne. nf_noerr) then
          write(stdout,2) 'u_da', indx,
     &                                     rstname(1:lstr)
          goto 99
        endif
      else
        write(stdout,1) 'u_da', rstname(1:lstr)
        goto 99
      endif
      iret=nf_inq_varid (ncid, 'v_da', varid)
      if (iret .eq. nf_noerr) then
        iret=das_nf_fwrite (v_s(0,0,1), ncid, varid,
     &                                        indx, v3dvar)
        if (iret .ne. nf_noerr) then
          write(stdout,2) 'v_da', indx,
     &                                       rstname(1:lstr)
          goto 99
        endif
      else
        write(stdout,1) 'v_da', rstname(1:lstr)
        goto 99
      endif
      do itrc=1,NT
        if (itrc .eq. 1) then
           vnts='t_da'
        else if (itrc .eq. 2) then
           vnts='s_da'
        else
           vnts='BIO-VARIABLES'
        endif
        iret=nf_inq_varid (ncid, vnts, varid)
        if (iret .eq. nf_noerr) then
          iret=das_nf_fwrite (t_s(0,0,1,itrc), ncid,  varid,
     &                                               indx, r3dvar)
          if (iret .ne. nf_noerr) then
            write(stdout,2) vnts, indx,
     &                                              rstname(1:lstr)
            goto 99
          endif
        else
          write(stdout,1) vnts, rstname(1:lstr)
          goto 99
        endif
      enddo
      iret=nf_inq_varid (ncid, 'mask_da', varid)
      if (iret .eq. nf_noerr) then
        iret=das_nf_fwrite (rmask_das(0,0,1), ncid,  varid,
     &                                               indx, r3dvar)
        if (iret .ne. nf_noerr) then
          write(stdout,2) 'mask_da', indx,
     &                                              rstname(1:lstr)
          goto 99
        endif
      else
        write(stdout,1) 'mask_da', rstname(1:lstr)
        goto 99
      endif
  1   format(/1x,'WRT_INC - unable to find variable:',    1x,A,
     &                            /15x,'in input NetCDF file:',1x,A/)
  2   format(/1x,'WRT_INC - error while writing variable:',1x, A,
     &    2x,'at time record =',i4/15x,'in input NetCDF file:',1x,A/)
      iret=nf_close(ncid)
      return
  99  stop 819
      return
      end
      subroutine check_err(iret)
      integer iret
      include 'netcdf.inc'
      if (iret .ne. NF_NOERR) then
      print *, nf_strerror(iret)
      stop
      endif
      end
