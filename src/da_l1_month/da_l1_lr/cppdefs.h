!  This is "cppdefs.h": MODEL CONFIGURATION FILE
!  ==== == ============ ===== ============= ====
!
#undef BASIN           /* Big Bad Basin Example */
#undef CANYON_A        /* Canyon_A Example */
#undef CANYON_B        /* Canyon_B Example */
#undef DAMEE_B         /* North Atlantic DAMEE, Big Domain */
#undef DAMEE_S         /* North Atlantic DAMEE Small Domain */
#undef GRAV_ADJ        /* Graviational Adjustment Example */
#undef NJ_BIGHT        /* New Jersey Bight Application */
#undef NPACIFIC        /* North Pacific Application */
#undef OVERFLOW        /* Graviational/Overflow Example */
#undef SEAMOUNT        /* Seamount Example */
#undef SOLITON         /* Equatorial Rossby Wave Example */
#undef TASMAN_SEA      /* Tasman Sea Example */
#undef UPWELLING       /* Upwelling Example */
#define USWEST          /* US West Coast Application */
!
!--------------------------------------------------------------------
!
#if defined BASIN
!
!  Options for Big Bad Basin Example:
!
# define SOLVE3D

# define UV_ADV
# define UV_COR
# define UV_VIS4
# define MIX_GP_UV

# undef  SALINITY
# undef  NONLIN_EOS

# undef  TS_DIF2
# undef  TS_DIF4
# define MIX_GP_TS

# define BODYFORCE
# define ANA_GRID
# define ANA_INITIAL
c--# define ANA_MEANRHO
# define ANA_SMFLUX
# define ANA_STFLUX
# define ANA_BTFLUX
# undef  ANA_VMIX
!

#elif defined CANYON_A
!
!  Options for Canyon A Example.
!
#define SOLVE3D

#define UV_ADV
#define UV_COR
#define UV_VIS2
#define MIX_GP_UV

#undef SALINITY
#undef  NONLIN_EOS

#define TS_DIF2
#define MIX_GP_TS

#undef TS_DIF2
#undef  TS_DIF4
#undef  OBC_EAST
#undef  OBC_WEST
#undef  OBC_NORTH
#undef  OBC_SOUTH
#define EW_PERIODIC
#undef  NS_PERIODIC
#define BODYFORCE
#define ANA_GRID
#define ANA_INITIAL
#define ANA_MEANRHO
#define ANA_SMFLUX
#define ANA_STFLUX
#define ANA_BTFLUX
!
#elif defined CANYON_B
!
!  Options for Canyon B Example.
!
#define SOLVE3D

#define UV_ADV
#define UV_COR
#define UV_VIS2
#define MIX_GP_UV

#undef  NONLIN_EOS
#undef  SALINITY

#define TS_DIF2
#define MIX_GP_TS

#undef OBC_EAST
#undef OBC_WEST
#undef OBC_NORTH
#undef	OBC_SOUTH
#define	EW_PERIODIC
#undef	NS_PERIODIC
#define ANA_GRID
#define ANA_INITIAL
#define ANA_MEANRHO
#define ANA_SMFLUX
#define ANA_STFLUX
#define ANA_BTFLUX
#define ANA_VMIX
!
#elif defined DAMEE_B || defined DAMEE_S
!
!                       North Atlantic (DAMEE) Configuration
!                       ===== ======== ======= ============= 
# define SOLVE3D
c--# define AVERAGES
# undef  STATIONS
# define UV_COR
# define UV_ADV
!                       Equation of State 
# define SALINITY
# define NONLIN_EOS
c----# define ANA_MEANRHO
!                       Forcing and Climatology
# define TCLIMATOLOGY
# define TNUDGING
# define QCORRECTION
# undef  ANA_SMFLUX
# undef  ANA_SSFLUX
# undef  ANA_STFLUX
# undef  ANA_SST
# undef  ANA_SRFLUX
# define ANA_BSFLUX
# define ANA_BTFLUX
!                       Lateral Mixing
# define VIS_GRID
# undef	UV_VIS2
# undef	UV_VIS4    /* <-- changed: was previously defined */
# undef MIX_S_UV
# define MIX_GP_UV  /* <-- changed: was previously undef */
# define DIF_GRID
# undef	TS_DIF2
# undef	TS_DIF4   /* <-- changed: was previously defined */
# undef	MIX_S_TS
# define MIX_GP_TS
# undef	MIX_EN_TS
!                       Vertical Mixing
# undef BVF_MIXING
# undef PP_MIXING
# define LMD_MIXING
#  define LMD_RIMIX
#  define LMD_CONVEC
c--#  define LMD_DDMIX

#  define LMD_KPP
c--#  define LMD_NONLOCAL

!                       Grid Configuration and Boundaries

# define CURVGRID
# define SPHERICAL
# define MASKING

# define EASTERN_WALL
# define WESTERN_WALL
# define SOUTHERN_WALL
# define NORTHERN_WALL
# undef OBC_EAST
# undef OBC_WEST
# undef OBC_NORTH
# undef OBC_SOUTH
# undef EW_PERIODIC
# undef NS_PERIODIC
# undef OBC_FSORLANSKI
# undef OBC_FSPRESCRIBE
# undef OBC_M2ORLANSKI
# undef OBC_M2PRESCRIBE
# undef OBC_M3ORLANSKI
# undef OBC_M3PRESCRIBE
# undef OBC_TCLIMA
# undef OBC_TORLANSKI
# undef OBC_TPRESCRIBE
!
#elif defined GRAV_ADJ
!
!  Options for Gravitational Adjustment Example.
!
# define SOLVE3D

# define UV_ADV
# undef UV_COR
# define UV_VIS2
# define MIX_GP_UV

# define TS_DIF2
# define MIX_GP_TS

# define ANA_GRID
# define ANA_INITIAL
# define ANA_MEANRHO
# define ANA_SMFLUX
# define ANA_STFLUX
# define ANA_BTFLUX
!
#elif defined NJ_BIGHT
!
!  Options for New Jersey Bight Application.
!
# define SOLVE3D

# define UV_ADV
# define UV_COR
# define UV_VIS2
# define MIX_GP_UV

# define SALINITY
# undef  NONLIN_EOS

# define TS_DIF2

# define CURVGRID
# define SPHERICAL

# define STATIONS
# define OBC_EAST
# undef	OBC_WEST
# define OBC_NORTH
# define OBC_SOUTH
# undef	EW_PERIODIC
# undef	NS_PERIODIC
# define OBC_FSORLANSKI
# define OBC_M2ORLANSKI
# define OBC_M3ORLANSKI
# undef  OBC_TCLIMA
# undef  OBC_TORLANSKI
# define LMD_MIXING
# define LMD_RIMIX
# undef  LMD_CONVEC
# undef  LMD_DDMIX
# define LMD_KPP
# undef  TCLIMATOLOGY
# undef  ANA_TCLIMA
# define ANA_INITIAL
# define ANA_MEANRHO
# define ANA_SMFLUX
# define ANA_SRFLUX
# define ANA_SSFLUX
# define ANA_STFLUX
# define ANA_BSFLUX
# define ANA_BTFLUX
# undef  ANA_VMIX
!
#elif defined NPACIFIC
!
!  Options for North Pacific Application.
!
# define SOLVE3D

# define UV_ADV
# define UV_COR
# define UV_VIS2
# define MIX_GP_UV

# define NONLIN_EOS
# define SALINITY

# define TS_DIF2
# define MIX_GP_TS

# undef	SMOLARKIEWICZ
# define QCORRECTION

# define CURVGRID
# define SPHERICAL
# define MASKING

# define AVERAGES
# undef	STATIONS
# undef	OBC_EAST
# undef	OBC_WEST
# undef	OBC_NORTH
# define OBC_SOUTH
# undef	EW_PERIODIC
# undef	NS_PERIODIC
# undef	OBC_INFLOW
# undef	OBC_FSORLANSKI
# undef	OBC_FSPRESCRIBE
# undef	OBC_M2ORLANSKI
# undef	OBC_M2PRESCRIBE
# undef	OBC_M3ORLANSKI
# undef	OBC_M3PRESCRIBE
# undef	OBC_TCLIMA
# undef	OBC_TORLANSKI
# undef	OBC_TPRESCRIBE
# define BODYFORCE
# undef	BVF_MIXING
# undef	PP_MIXING
# define LMD_MIXING
# define LMD_RIMIX
# define LMD_CONVEC
# undef	LMD_DDMIX
# define LMD_KPP
# define TCLIMATOLOGY
# define TNUDGING
# undef	ZNUDGING
# undef	ANA_INITIAL
# define ANA_MEANRHO
# undef	ANA_SMFLUX
# undef	ANA_SSFLUX
# undef	ANA_SST
# undef	ANA_STFLUX
# define ANA_BSFLUX
# define ANA_BTFLUX
!
#elif defined OVERFLOW
!
!  Options for Gravitational/Overflow Example.
!
#define SOLVE3D

#define UV_ADV
#undef UV_COR
#define UV_VIS2
#define MIX_GP_UV

#undef  NONLIN_EOS
#undef  SALINITY

#define TS_DIF2
#define MIX_GP_TS

#define ANA_GRID
#define ANA_INITIAL
#define ANA_MEANRHO
#define ANA_SMFLUX
#define ANA_STFLUX
#define ANA_BTFLUX
!
#elif defined SEAMOUNT
!
!  Options for Seamount Example.
!
#define SOLVE3D

#define UV_ADV
#define UV_COR

c#define UV_VIS2
c#undef MIX_S_UV
c#define MIX_GP_UV

c#define TS_DIF2
c#undef  TS_DIF4
c#define MIX_GP_TS

#define	EW_PERIODIC

#define ANA_GRID
#define ANA_INITIAL
#define ANA_MEANRHO
#define ANA_SMFLUX
#define ANA_STFLUX
#define ANA_BTFLUX
!
#elif defined SOLITON
!
!  Options for Equatorial Rossby Example:
!
# undef  SOLVE3D

# define ANA_GRID
# define ANA_INITIAL
# define ANA_SMFLUX

# define UV_COR
# define UV_ADV
# undef  UV_VIS2
c--# define UV_VIS4
c--# define EW_PERIODIC
c--# define NS_PERIODIC

# define OBC_WEST
# define OBC_EAST
# define OBC_NORTH
# define OBC_SOUTH
# define OBC_M2ORLANSKI
# define OBC_VOLCONS
!
#elif defined TASMAN_SEA
!
!  Options for Tasman Sea Example:
!
#define SOLVE3D

#define UV_ADV
#define UV_COR
#define UV_VIS2
#define MIX_GP_UV

#define NONLIN_EOS
#define SALINITY

#define TS_DIF2
#define MIX_GP_TS

#define	MASKING

#define BODYFORCE
#define ANA_INITIAL
#undef  ANA_MEANRHO
#define ANA_SMFLUX
#define ANA_SSFLUX
#define ANA_STFLUX
#define ANA_BSFLUX
#define ANA_BTFLUX
#undef  ANA_VMIX
!
#elif defined UPWELLING
!
!  Options for Upwelling Example:
!
#define SOLVE3D
#define UV_ADV
#define UV_COR
c--#define UV_VIS2
#define MIX_GP_UV

#undef NONLIN_EOS
#undef SALINITY
#undef LMD_VMIX

c--#define TS_DIF2
#define MIX_GP_TS

#define EW_PERIODIC

c-#define OBC_WEST
c-#define OBC_EAST

c--#define BODYFORCE

#define ANA_GRID
#define ANA_INITIAL
c--#define ANA_MEANRHO
#define ANA_SMFLUX
#define ANA_STFLUX
#define ANA_SSFLUX
#define ANA_BTFLUX
#define ANA_BSFLUX
#define ANA_SRFLUX
#define ANA_VMIX

c--#undef BODYFORCE
c--#undef ANA_VMIX
c--#define LMD_MIXING
c--#define LMD_KPP

!--------------------------------------------
#elif defined USWEST
!                       US West Coast Configuration
!                       == ==== ===== =============
# define OPENMP
!
# define SOLVE3D
# define UV_COR
# define UV_ADV
!                       Equation of State
# define SALINITY
# define NONLIN_EOS
# undef  ANA_MEANRHO
!                       Forcing and Climatology
# undef  BODYFORCE

# ifdef SOLVE3D
#   define QCORRECTION
#   define TCLIMATOLOGY
#   define UCLIMATOLOGY
#   define TNUDGING
#   define M3NUDGING
#   define M2NUDGING
#   define ZNUDGING
#   define SPONGE
#   define SFLX_CORR
#   undef ROBUST_DIAG
!
#   undef  ANA_SSFLUX
#   undef  ANA_SST
#   undef  ANA_SRFLUX
#   undef  ANA_STFLUX
#   define ANA_BSFLUX
#   define ANA_BTFLUX
# endif
# undef  ANA_SMFLUX
# undef  ANA_TCLIMA
# undef  ANA_UCLIMA
!                       Lateral Mixing
# undef VIS_GRID
# define UV_VIS2
# define MIX_GP_TS
# undef UV_VIS4

# ifdef SOLVE3D
#   undef DIF_GRID
#   define TS_DIF2
#   define MIX_GP_UV
#   undef TS_DIF4
#   ifdef TCLIMATOLOGY
#     define CLIMAT_TS_MIXH
#   endif
# endif
!                       Vertical Mixing
# ifdef SOLVE3D
#   undef BVF_MIXING
#   undef PP_MIXING
#   define LMD_MIXING
# endif
# if defined LMD_MIXING
#   define LMD_RIMIX
#   define LMD_CONVEC
#   undef LMD_DDMIX
#   define LMD_KPP
#   undef LMD_NONLOCAL
# else
#   undef LMD_RIMIX
#   undef LMD_CONVEC
#   undef LMD_DDMIX
#   undef LMD_KPP
#   undef LMD_NONLOCAL
# endif
!                       Grid Configuration
# define CURVGRID
# define SPHERICAL
# define MASKING
!                       Open Boundary Conditions
# undef OBC_EAST
# define OBC_WEST
# define OBC_NORTH
# define OBC_SOUTH
# define OBC_VOLCONS
!
# define OBC_TORLANSKI
# define OBC_M2ORLANSKI
# define OBC_M3ORLANSKI
!
# undef OBC_M2FLATHER
!
# undef OBC_TSPECIFIED
# undef OBC_M2SPECIFIED
# undef OBC_M3SPECIFIED
!
# define AVERAGES
!                       Biology
# undef BIOLOGY
!
!                       ROMS-DAS
# undef DAS_GEOS_STRONG
# undef DAS_HYDRO_STRONG
!                      error corelations and variances
# define  DAS_BVAR_CORR 
# undef DAS_ANA_BVAR
!                       observations
# define DAS_MCSST
# define DAS_GOES_SST
# undef  DAS_TMISST
# define  DAS_JASONSSH
# define  DAS_SWOTSSH
# define  DAS_SATSSSS
# undef  DAS_TPSSH
# undef  DAS_ERS2SSH
# undef  DAS_GFOSSH
!                      in-situ
# define DAS_WHOIGLIDER
# define DAS_SIOGLIDER
# define DAS_PTSURCDT
# define DAS_MARTINCDT
# define DAS_MOORINGNCDT
# define DAS_NPSFLIGHT
# define DAS_CALPOLYAUV
# define DAS_DORADOAUV
# define DAS_MOORING
# define  DAS_HFRADAR
# undef  DAS_CURRENT_UV
!
#endif
!
#include "set_global_definitions.h"
#include "das_set_global_def.h"
