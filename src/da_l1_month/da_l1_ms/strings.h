! A long character string to hold activated cpp-switches.
! Basically it is used to keep track of cpp-switches by placing
! them together and writing into history file.
!                                    !
      integer max_opt_size           ! NOTE: Parameter max_opt_size
      parameter (max_opt_size=2880)  ! must be equal to the length
      character*2880 Coptions,srcs   ! of character string. 
      common /strings/ Coptions,srcs !
