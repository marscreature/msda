#include "cppdefs.h"

      subroutine das_geos_uv (tile)
      implicit none
      integer tile,trd, omp_get_thread_num
# include "param.h"
# include "das_param.h"
# include "das_private_scratch.h"
# include "compute_tile_bounds.h"
      trd=omp_get_thread_num()
      call das_geos_uv_tile (Istr,Iend,Jstr,Jend,
     &                     A2d(1,1,trd),A2d(1,2,trd))
      return
      end

      subroutine das_geos_uv_tile (Istr,Iend,Jstr,Jend,ug,vg)
!
!--------------------------------------------------------------------
! compute geostropic current from sea surface height increments
! zeta_s and density increments rho_s. called by ROMS-DAS
!
! It is not applicable near the equator(-1.5 - 1.5). 
!
! In consistence with ROMS that computes XI-component (ETA-component)
! of pressure at u-grids (v-grids), v-component (u-component) of
! geostrophic flow is thus computed at u-grids (v-grids) and then interpolated
! to v-grids (u-grids). 
!
! In the geometry vertical coordinates, there is a possibilty of
! a single column or row. The increment in those areas is zero in
! the following pragram.
!
! Boundary condition: sliding, and velocity normal to coastline zero
! 
!--------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k
      real cff, cff1, crit_lat, crit
# include "param.h"
# include "das_param.h"
# include "scalars.h"
# include "grid.h"
# include "das_ocean.h"
      real ug(PRIVATE_2D_SCRATCH_ARRAY),
     &     vg(PRIVATE_2D_SCRATCH_ARRAY)
!
# include "compute_auxiliary_bounds.h"
!
!------------------------------------------------------------------
!
      cff=0.5*g/rho0    ! 0.5, two vertical levels
      cff1=1000.0*g/rho0    ! g
!
      crit_lat=1.5
      crit=2.0*1.e-5*sin(crit_lat*3.1415926/180.0)
!
! surface u
!
      do j=Jstr,Jend+1
        do i=Istr-1,IendR
          ug(i,j)=-cff1*(zeta_s(i,j) -zeta_s(i,j-1))
     &            *(pn(i,j-1)+pn(i,j))/(f(i,j-1)+f(i,j))
     &            *vmask_das(i,j,NDAS)  ! vg on v-grids
        enddo
      enddo
      do j=Jstr,Jend
        do i=Istr,IendR
          u_s(i,j,NDAS)=0.25*(
     &        ug(i-1,j)+ug(i-1,j+1)+ug(i,j)+ug(i,j+1)  )
     &        *umask_das(i,j,NDAS)
        enddo
      enddo
      if (SOUTHERN_EDGE) then
        do i=Istr,IendR
          u_s(i,0,NDAS) = 0.5*(ug(i-1,1)+ug(i,1))
     &                         * umask_das(i,0,NDAS)
        enddo
      endif
      if (NORTHERN_EDGE) then
        do i=Istr,IendR
          u_s(i,Mm+1,NDAS) = 0.5*(ug(i-1,Mm+1)+ug(i,Mm+1))
     &                          * umask_das(i,Mm+1,NDAS)
        enddo
      endif
!
! surface v
!
      do j=Jstr-1,JendR
        do i=Istr,Iend+1
          vg(i,j)= cff1 * (zeta_s(i,j) -zeta_s(i-1,j))
     &            *(pm(i-1,j)+pm(i,j))/(f(i-1,j)+f(i,j))
     &            *umask_das(i,j,NDAS)   ! vg on u-grids
        enddo
      enddo
      do j=Jstr,JendR
        do i=Istr,Iend
          v_s(i,j,NDAS)=0.25* (
     &        vg(i,j-1)+vg(i,j)+vg(i+1,j-1)+vg(i+1,j)  )
     &        *vmask_das(i,j,NDAS)
        enddo
      enddo
      if (WESTERN_EDGE) then
        do j=Jstr,JendR
          v_s(0,j,NDAS) = 0.5*(vg(1,j-1)+vg(1,j))
     &                         * vmask_das(0,j,NDAS)
        enddo
      endif
      if (EASTERN_EDGE) then
        do j=Jstr,JendR
          v_s(Lm+1,j,NDAS) = 0.5*(vg(Lm+1,j-1)+vg(Lm+1,j))
     &                          * vmask_das(Lm+1,j,NDAS)
        enddo
      endif
!
!!!!!!!!!!!!!!!!      
!
      do k=NDAS-1,1,-1
!
! u
!
        do j=Jstr,Jend+1
          do i=Istr-1,IendR
            ug(i,j)=ug(i,j)
     &             -cff*( rho_s(i,j,k)-rho_s(i,j-1,k)
     &                   +rho_s(i,j,k+1)-rho_s(i,j-1,k+1)  )
     &                *(pn(i,j-1)+pn(i,j))/(f(i,j-1)+f(i,j))
     &                *(z_das(k+1)-z_das(k))
     &                *vmask_das(i,j,k)
          enddo
        enddo
        do j=Jstr,Jend
          do i=Istr,IendR
            u_s(i,j,k)=0.25*(
     &        ug(i-1,j)+ug(i-1,j+1)+ug(i,j)+ug(i,j+1)  )
     &        *umask_das(i,j,k)
          enddo
        enddo
        if (SOUTHERN_EDGE) then
          do i=Istr,IendR
            u_s(i,0,k) = 0.5*(ug(i-1,1)+ug(i,1))
     &                         * umask_das(i,0,k)
          enddo
        endif
        if (NORTHERN_EDGE) then
          do i=Istr,IendR
            u_s(i,Mm+1,k) = 0.5*(ug(i-1,Mm+1)+ug(i,Mm+1))
     &                          * umask_das(i,Mm+1,k)
          enddo
        endif
!
! v
!
        do j=Jstr-1,JendR
          do i=Istr,Iend+1
            vg(i,j)=vg(i,j)
     &         +cff*(  rho_s(i,j,k)-rho_s(i-1,j,k)
     &                +rho_s(i,j,k+1)-rho_s(i-1,j,k+1) )
     &            *(pm(i-1,j)+pm(i,j))/(f(i-1,j)+f(i,j))
     &            *(z_das(k+1)-z_das(k))
     &            *umask_das(i,j,k)
          enddo
        enddo
        do j=Jstr,JendR
          do i=Istr,Iend
            v_s(i,j,k)=0.25* (
     &          vg(i,j-1)+vg(i,j)+vg(i+1,j-1)+vg(i+1,j)  )
     &          *vmask_das(i,j,k)
          enddo
        enddo
        if (WESTERN_EDGE) then
          do j=Jstr,JendR
            v_s(0,j,k) = 0.5*(vg(1,j-1)+vg(1,j))
     &                         * vmask_das(0,j,k)
          enddo
        endif
        if (EASTERN_EDGE) then
          do j=Jstr,JendR
            v_s(Lm+1,j,k) = 0.5*(vg(Lm+1,j-1)+vg(Lm+1,j))
     &                          * vmask_das(Lm+1,j,k)
          enddo
        endif
      enddo  !k
!          
      return
      end
