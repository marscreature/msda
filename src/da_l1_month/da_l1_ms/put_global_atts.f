      subroutine put_global_atts (ncid, ierr)
      implicit none
      include 'netcdf.inc'
      integer ncid, ierr, nf_ftype, lvar,lenstr
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=606,  MMmH=438, NnH=66)
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLmH,  MMm=MMmH,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      real  x_sponge,   v_sponge
       real  tauT_in, tauT_out, tauM_in, tauM_out
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &                      , x_sponge,   v_sponge
     &                      , tauT_in, tauT_out, tauM_in, tauM_out
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
      integer indxU, indxV, indxT
      parameter (indxU=5, indxV=6, indxT=7)
      integer indxS
      parameter (indxS=indxT+1)
      integer indxO, indxW, indxR, indxVisc, indxAkv, indxAkt
      parameter (indxO=indxT+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed
     &                                  +ntrc_diats+ntrc_diauv+1,
     &           indxW=indxO+1, indxR=indxO+2,
     &           indxVisc=indxR+1,
     &           indxAkv=indxR+2, indxAkt=indxAkv+1)
      integer indxAks
      parameter (indxAks=indxAkt+1)
      integer indxSSH
      parameter (indxSSH=indxAkt+4)
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
      integer indxSHFl, indxSWRad
      parameter (indxSHFl=indxSSH+3)
      integer indxSSFl
      parameter (indxSSFl=indxSHFl+1, indxSWRad=indxSHFl+2)
      integer indxSST, indxdQdSST
      parameter (indxSST=indxSWRad+1, indxdQdSST=indxSWRad+2)
      integer indxSSS
      parameter (indxSSS=indxSST+2)
      integer indxBostr
      parameter (indxBostr=indxSUSTR+16)
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     &                u3dvar, v3dvar, p3dvar, w3dvar, b3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8,b3dvar=12)
      integer xi_rho,xi_u, eta_rho,eta_v
      parameter (xi_rho=LLm+2,  xi_u=xi_rho-1,
     &           eta_rho=MMm+2, eta_v=eta_rho-1)
      integer ncidfrc, ncidbulk, ncidclm,  ntsms
     &      , ntsrf,  ntssh,  ntsst, ntsss, ntuclm, ntww,
     &        ntbulk
      integer nttclm(NT),    ntstf(NT)
      integer ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                  ,rstR  , rstU,    rstV
      integer rstT(NT)
      integer  ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,  hisV,  hisR,    hisHbl, hisHbbl
     &      , hisO,  hisW,  hisVisc,  hisAkv,  hisAkt, hisAks
     &      , hisBostr
      integer hisT(NT)
      integer   ncidavg, nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,  avgV,  avgR,    avgHbl, avgHbbl
     &      , avgO,  avgW,  avgVisc, avgAkv,  avgAkt, avgAks
      integer avgT(NT)
      integer avgBostr
      logical wrthis(60+NT)
     &      , wrtavg(60+NT)
      common/incscrum/
     &        ncidfrc, ncidbulk,ncidclm, ntsms, ntsrf, ntssh, ntsst
     &      , ntuclm, ntsss, ntww, ntbulk
     &                        ,  nttclm,          ntstf
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                  ,rstR  , rstU,    rstV,   rstT
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT,    hisR
     &      , hisO,    hisW,     hisVisc, hisAkv,  hisAkt, hisAks
     &      , hisHbl,  hisHbbl,  hisBostr
     &      , ncidavg,  nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT,    avgR
     &      , avgO,    avgW,     avgVisc,  avgAkv,  avgAkt, avgAks
     &      , avgHbl,  avgHbbl
     &      , avgBostr
     &      , wrthis
     &      , wrtavg
      character*80 date_str, title
      character*80 ininame,  grdname,  hisname
     &         ,   rstname,  frcname,  bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
      character*52  vname(4, 150)
      common /cncscrum/       date_str,   title
     &         ,   ininame,  grdname, hisname
     &         ,   rstname,  frcname, bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
     &                      ,  vname
      integer max_opt_size
      parameter (max_opt_size=2880)
      character*2880 Coptions,srcs
      common /strings/ Coptions,srcs
      if (ncid.eq.ncidrst) then
        nf_ftype=NF_DOUBLE
      else
        nf_ftype=NF_REAL
      endif
      if (ncid.eq.ncidrst) then
        ierr=nf_put_att_text (ncid, nf_global, 'type',  17,
     &                                 'ROMS restart file')
      elseif (ncid.eq.ncidhis) then
        ierr=nf_put_att_text (ncid, nf_global, 'type',  17,
     &                                 'ROMS history file')
      elseif (ncid.eq.ncidavg) then
        ierr=nf_put_att_text (ncid, nf_global, 'type',  18,
     &                                'ROMS averages file')
      endif
      lvar=lenstr(title)
      ierr=nf_put_att_text(ncid, nf_global, 'title',   lvar,
     &                                         title(1:lvar))
      lvar=lenstr(date_str)
      ierr=nf_put_att_text(ncid, nf_global, 'date',    lvar,
     &                                      date_str(1:lvar))
      lvar=lenstr(rstname)
      ierr=nf_put_att_text(ncid, nf_global, 'rst_file',lvar,
     &                                       rstname(1:lvar))
      lvar=lenstr(hisname)
      ierr=nf_put_att_text(ncid, nf_global, 'his_file',lvar,
     &                                       hisname(1:lvar))
      lvar=lenstr(avgname)
      ierr=nf_put_att_text(ncid, nf_global, 'avg_file',lvar,
     &                                       avgname(1:lvar))
      lvar=lenstr(grdname)
      ierr=nf_put_att_text(ncid, nf_global, 'grd_file',lvar,
     &                                       grdname(1:lvar))
      lvar=lenstr(ininame)
      ierr=nf_put_att_text(ncid, nf_global, 'ini_file',lvar,
     &                                       ininame(1:lvar))
      lvar=lenstr(frcname)
      ierr=nf_put_att_text(ncid, nf_global,'frc_file', lvar,
     &                                       frcname(1:lvar))
      ierr=nf_put_att_double(ncid, nf_global,'theta_s',nf_ftype,
     &                                            1,  theta_s)
      ierr=nf_put_att_text (ncid, nf_global,'theta_s_expl',38,
     &                   'S-coordinate surface control parameter')
      ierr=nf_put_att_double(ncid,nf_global,'theta_b',nf_ftype, 1,
     &                                                       theta_b)
      ierr=nf_put_att_text (ncid,nf_global,'theta_b_expl',37,
     &                       'S-coordinate bottom control parameter')
      ierr=nf_put_att_double(ncid,nf_global,'Tcline', nf_ftype, 1,
     &                                                        Tcline)
      ierr=nf_put_att_text (ncid,nf_global,'Tcline_expl',39,
     &                     'S-coordinate surface/bottom layer width')
      ierr=nf_put_att_text (ncid, nf_global,'Tcline_units',5,'meter')
      ierr=nf_put_att_double(ncid, nf_global, 'hc',nf_ftype, 1, hc)
      ierr=nf_put_att_text (ncid, nf_global, 'hc_expl',38,
     &                      'S-coordinate parameter, critical depth')
      ierr=nf_put_att_text (ncid, nf_global, 'hc_units', 5, 'meter')
      ierr=nf_put_att_double(ncid, nf_global,'sc_w',nf_ftype, N+1,
     &                                                          sc_w)
      ierr=nf_put_att_text (ncid, nf_global,'sc_w_expl', 24,
     &                                    'S-coordinate at W-points')
      ierr=nf_put_att_double(ncid, nf_global,'Cs_w',nf_ftype, N+1,
     &                                                          Cs_w)
      ierr=nf_put_att_text (ncid, nf_global,'Cs_w_expl',42,
     &                  'S-coordinate stretching curves at W-points')
      ierr=nf_put_att_double(ncid,nf_global,'sc_r',nf_ftype,N,sc_r)
      ierr=nf_put_att_text (ncid, nf_global,'sc_r_expl', 24,
     &                                    'S-coordinate at W-points')
      ierr=nf_put_att_double(ncid,nf_global,'Cs_r',nf_ftype,N,Cs_r)
      ierr=nf_put_att_text (ncid, nf_global,'Cs_r_expl',44,
     &                'S-coordinate stretching curves at RHO-points')
      ierr=nf_put_att_int(ncid,nf_global,'ntimes',  nf_int,1,ntimes)
      ierr=nf_put_att_int(ncid,nf_global,'ndtfast', nf_int,1,ndtfast)
      ierr=nf_put_att_double(ncid,nf_global,'dt',    nf_ftype, 1,  dt)
      ierr=nf_put_att_double(ncid,nf_global,'dtfast',nf_ftype, 1,
     &                                                        dtfast)
      ierr=nf_put_att_int  (ncid,nf_global,'nwrt',  nf_int,  1, nwrt)
      ierr=nf_put_att_int  (ncid,nf_global,'ntsavg',nf_int, 1,ntsavg)
      ierr=nf_put_att_text (ncid,nf_global,'ntsavg_expl',59,
     & 'starting time-step for accumulation of time-averaged fields')
      ierr=nf_put_att_int  (ncid,nf_global,'navg',  nf_int, 1,  navg)
      ierr=nf_put_att_text (ncid,nf_global,'navg_expl',50,
     &          'number of time-steps between time-averaged records')
      ierr=nf_put_att_double(ncid,nf_global,'visc2',nf_ftype,1,visc2)
      ierr=nf_put_att_text (ncid,nf_global,'visc2_expl',41,
     &                   'Laplacian mixing coefficient for momentum')
      ierr=nf_put_att_text (ncid,nf_global,'visc2_units',15,
     &                                             'meter2 second-1')
      ierr=nf_put_att_double(ncid,nf_global,'tnu2',nf_ftype, 1,tnu2)
      ierr=nf_put_att_text (ncid,nf_global,'tnu2_expl',40,
     &                    'Laplacian mixing coefficient for tracers')
      ierr=nf_put_att_text (ncid,nf_global,'tnu2_units',15,
     &                                             'meter2 second-1')
      ierr=nf_put_att_double(ncid,nf_global,'rdrg',nf_ftype,1,rdrg)
      ierr=nf_put_att_text (ncid,nf_global,'rdrg_expl',23,
     &                                     'linear drag coefficient')
      ierr=nf_put_att_text (ncid,nf_global,'rdrg_units',14,
     &                                              'meter second-1')
      ierr=nf_put_att_double(ncid,nf_global,'rdrg2',nf_ftype,1,rdrg2)
      ierr=nf_put_att_text (ncid,nf_global,'rdrg2_expl',26,
     &                                  'quadratic drag coefficient')
      ierr=nf_put_att_text (ncid,nf_global,'rdrg2_units',14,
     &                                              'nondimensional')
      ierr=nf_put_att_double(ncid,nf_global,'rho0',nf_ftype, 1,rho0)
      ierr=nf_put_att_text (ncid,nf_global,'rho0_expl', 45,
     &               'Mean density used in Boussinesq approximation')
      ierr=nf_put_att_text (ncid,nf_global,'rho0_units', 16,
     &                                            'kilogram meter-3')
      ierr=nf_put_att_double(ncid,nf_global,'gamma2',nf_ftype, 1,
     &                                                       gamma2)
      ierr=nf_put_att_text (ncid,nf_global,'gamma2_expl', 22,
     &                                    'Slipperiness parameter')
      lvar=lenstr(srcs)
      ierr=nf_put_att_text (ncid,nf_global, 'SRCS', lvar,
     &                                        srcs(1:lvar))
      lvar=lenstr(Coptions)
      ierr=nf_put_att_text(ncid,nf_global, 'CPP-options',
     &                              lvar, Coptions(1:lvar))
      return
      end
