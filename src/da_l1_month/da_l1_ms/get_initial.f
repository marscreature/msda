      subroutine get_initial
      implicit none
      include 'netcdf.inc'
      real time_scale
      integer ncid, indx, varid,  ierr, lstr, lvar, latt, lenstr,
     &        start(2), count(2), ibuff(4),   nf_fread, checkdims
     &                                      , itrc
      character units*64
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=606,  MMmH=438, NnH=66)
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLmH,  MMm=MMmH,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      real  x_sponge,   v_sponge
       real  tauT_in, tauT_out, tauM_in, tauM_out
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &                      , x_sponge,   v_sponge
     &                      , tauT_in, tauT_out, tauM_in, tauM_out
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
      integer indxU, indxV, indxT
      parameter (indxU=5, indxV=6, indxT=7)
      integer indxS
      parameter (indxS=indxT+1)
      integer indxO, indxW, indxR, indxVisc, indxAkv, indxAkt
      parameter (indxO=indxT+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed
     &                                  +ntrc_diats+ntrc_diauv+1,
     &           indxW=indxO+1, indxR=indxO+2,
     &           indxVisc=indxR+1,
     &           indxAkv=indxR+2, indxAkt=indxAkv+1)
      integer indxAks
      parameter (indxAks=indxAkt+1)
      integer indxSSH
      parameter (indxSSH=indxAkt+4)
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
      integer indxSHFl, indxSWRad
      parameter (indxSHFl=indxSSH+3)
      integer indxSSFl
      parameter (indxSSFl=indxSHFl+1, indxSWRad=indxSHFl+2)
      integer indxSST, indxdQdSST
      parameter (indxSST=indxSWRad+1, indxdQdSST=indxSWRad+2)
      integer indxSSS
      parameter (indxSSS=indxSST+2)
      integer indxBostr
      parameter (indxBostr=indxSUSTR+16)
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     &                u3dvar, v3dvar, p3dvar, w3dvar, b3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8,b3dvar=12)
      integer xi_rho,xi_u, eta_rho,eta_v
      parameter (xi_rho=LLm+2,  xi_u=xi_rho-1,
     &           eta_rho=MMm+2, eta_v=eta_rho-1)
      integer ncidfrc, ncidbulk, ncidclm,  ntsms
     &      , ntsrf,  ntssh,  ntsst, ntsss, ntuclm, ntww,
     &        ntbulk
      integer nttclm(NT),    ntstf(NT)
      integer ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                  ,rstR  , rstU,    rstV
      integer rstT(NT)
      integer  ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,  hisV,  hisR,    hisHbl, hisHbbl
     &      , hisO,  hisW,  hisVisc,  hisAkv,  hisAkt, hisAks
     &      , hisBostr
      integer hisT(NT)
      integer   ncidavg, nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,  avgV,  avgR,    avgHbl, avgHbbl
     &      , avgO,  avgW,  avgVisc, avgAkv,  avgAkt, avgAks
      integer avgT(NT)
      integer avgBostr
      logical wrthis(60+NT)
     &      , wrtavg(60+NT)
      common/incscrum/
     &        ncidfrc, ncidbulk,ncidclm, ntsms, ntsrf, ntssh, ntsst
     &      , ntuclm, ntsss, ntww, ntbulk
     &                        ,  nttclm,          ntstf
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                  ,rstR  , rstU,    rstV,   rstT
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT,    hisR
     &      , hisO,    hisW,     hisVisc, hisAkv,  hisAkt, hisAks
     &      , hisHbl,  hisHbbl,  hisBostr
     &      , ncidavg,  nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT,    avgR
     &      , avgO,    avgW,     avgVisc,  avgAkv,  avgAkt, avgAks
     &      , avgHbl,  avgHbbl
     &      , avgBostr
     &      , wrthis
     &      , wrtavg
      character*80 date_str, title
      character*80 ininame,  grdname,  hisname
     &         ,   rstname,  frcname,  bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
      character*52  vname(4, 150)
      common /cncscrum/       date_str,   title
     &         ,   ininame,  grdname, hisname
     &         ,   rstname,  frcname, bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
     &                      ,  vname
      real zeta(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real ubar(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vbar(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real rzeta_bak(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real rubar_bak(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real rvbar_bak(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_zeta/zeta  /rhs_rzeta/rzeta_bak
     &       /ocean_ubar/ubar  /rhs_rubar/rubar_bak
     &       /ocean_vbar/vbar  /rhs_rvbar/rvbar_bak
      real u(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real v(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real t(0:Lm+1+padd_X,0:Mm+1+padd_E,N,NT)
      common /ocean_u/u /ocean_v/v /ocean_t/t
      real Hz(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real Hz_bak(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real z_r(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real z_w(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      real Huon(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real Hvom(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real W(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      common /grid_Hz/Hz    /grid_zr/z_r  /grid_W/W
     &  /grid_Hz_bak/Hz_bak /grid_zw/z_w  /grid_Huon/Huon
     &                                    /grid_Hvom/Hvom
      real rho(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real rhop0(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      common /ocean_rho/rho /ocean_rhop0/rhop0
      if (may_day_flag.ne.0) return
      lstr=lenstr(ininame)
      ierr=nf_open(ininame(1:lstr), nf_nowrite, ncid)
      if (ierr .eq. nf_noerr) then
        ierr=checkdims (ncid, ininame(1:lstr), lstr, indx)
        if (ierr. ne. nf_noerr) then
          goto 99
        elseif (indx.eq.0) then
          indx=1
        elseif (indx.gt.0 .and. nrrec.gt.0 .and. nrrec.le.indx) then
          indx=nrrec
        elseif (indx.gt.0 .and. nrrec.gt.indx) then
          write(stdout,'(/1x,A,I4,A/16x,A,I4,A/16x,3A/)')
     &            'GET_INITIAL ERROR: requested restart time record',
     &             nrrec, ' exceeds',  'number of available records',
     &             indx,'in netCDF file', '''',ininame(1:lstr),'''.'
          goto 99
        endif
      else
        write(stdout,'(/1x,2A/15x,3A)') 'GET_INITIAL ERROR: Cannot ',
     &               'open netCDF file', '''', ininame(1:lstr) ,'''.'
        goto 99
      endif
      lvar=lenstr(vname(1,indxTime))
      ierr=nf_inq_varid (ncid, vname(1,indxTime)(1:lvar), varid)
      if (ierr .eq. nf_noerr) then
        ierr=nf_get_var1_double (ncid, varid, indx, time)
        if (ierr .eq. nf_noerr) then
          ierr=nf_get_att_text(ncid, varid, 'units', units)
          if (ierr .eq. nf_noerr) then
            latt=lenstr(units)
            if (units(1:6).eq.'second') then
               time_scale=1.
            elseif (units(1:3).eq.'day') then
              time_scale=day2sec
            else
              write (stdout,'(/1x,4A/8x,3A/)') 'GET_INITIAL ',
     &              'ERROR: unknown units of for variable ''',
     &               vname(1,indxTime)(1:lvar), '''',
     &              'in netCDF file ''', ininame(1:lstr),'''.'
              goto 99
            endif
          else
            write (stdout,'(/1x,2A/8x,5A/)') 'GET_INITIAL ERROR: ',
     &             'cannot read attribute ''units'' for variable',
     &             '''', vname(1,indxTime)(1:lvar),
     &             ''' in netCDF file ''',  ininame(1:lstr), '''.'
            goto 99
          endif
        else
          write(stdout,2) vname(1,indxTime)(1:lvar), indx,
     &                                        ininame(1:lstr)
          goto 99
        endif
      else
        write(stdout,1) vname(1,indxTime)(1:lvar),ininame(1:lstr)
        goto 99
      endif
      time=time*time_scale
      tdays=time*sec2day
      ierr=nf_inq_varid (ncid, 'time_step', varid)
      if (ierr .eq. nf_noerr) then
        start(1)=1
        start(2)=indx
        count(1)=4
        count(2)=1
        ierr=nf_get_vara_int (ncid, varid, start, count, ibuff)
        if (ierr .eq. nf_noerr) then
          ntstart=ibuff(1)
          nrecrst=ibuff(2)
          nrechis=ibuff(3)
          nrecavg=ibuff(4)
        write(stdout,'(6x,A,G12.4,A,I2,A,I6,A,I3,A,I3,A)')
     &     'GET_INITIAL: Restarted from day =', tdays, ' rec =',
     &      indx, '(', ntstart, ',', nrecrst, ',', nrechis, ').'
        else
          write(stdout,'(/1x,2A/)') 'GET_INITIAL ERROR: Cannot ',
     &                            'read time and record indices.'
          goto 99
        endif
      else
        ntstart=1
        nrecrst=0
        nrechis=0
        nrecavg=0
        write(stdout,'(6x,2A,G12.4,1x,A,I4)') 'GET_INITIAL -- ',
     &          'Processing data for time =', tdays, 'record =', indx
      endif
      if (ntstart.lt.1) then
        ntstart=1
      elseif (ntstart.gt.1) then
        ntimes=ntstart+ntimes
      endif
      lvar=lenstr(vname(1,indxZ))
      ierr=nf_inq_varid (ncid, vname(1,indxZ)(1:lvar), varid)
      if (ierr .eq. nf_noerr) then
        ierr=nf_fread (zeta(0,0), ncid, varid,
     &                                         indx, r2dvar)
        if (ierr .ne. nf_noerr) then
          write(stdout,2) vname(1,indxZ)(1:lvar),indx,ininame(1:lstr)
          goto 99
        endif
      else
        write(stdout,1) vname(1,indxZ)(1:lvar), ininame(1:lstr)
        goto 99
      endif
      lvar=lenstr(vname(1,indxUb))
      ierr=nf_inq_varid (ncid, vname(1,indxUb)(1:lvar), varid)
      if (ierr .eq. nf_noerr) then
        ierr=nf_fread (ubar(0,0), ncid, varid,
     &                                         indx, u2dvar)
        if (ierr .ne. nf_noerr) then
          write(stdout,2) vname(1,indxUb)(1:lvar), indx,
     &                                      ininame(1:lstr)
          goto 99
        endif
      else
        write(stdout,1) vname(1,indxUb)(1:lvar), ininame(1:lstr)
        goto 99
      endif
      lvar=lenstr(vname(1,indxVb))
      ierr=nf_inq_varid (ncid, vname(1,indxVb)(1:lvar), varid)
      if (ierr .eq. nf_noerr) then
        ierr=nf_fread (vbar(0,0), ncid, varid,
     &                                         indx, v2dvar)
        if (ierr .ne. nf_noerr) then
          write(stdout,2) vname(1,indxVb)(1:lvar), indx,
     &                                       ininame(1:lstr)
          goto 99
        endif
      else
        write(stdout,1) vname(1,indxVb)(1:lvar), ininame(1:lstr)
        goto 99
      endif
      lvar=lenstr(vname(1,indxU))
      ierr=nf_inq_varid (ncid, vname(1,indxU)(1:lvar), varid)
      if (ierr .eq. nf_noerr) then
        ierr=nf_fread (u(0,0,1), ncid, varid,
     &                                        indx, u3dvar)
        if (ierr .ne. nf_noerr) then
          write(stdout,2) vname(1,indxU)(1:lvar), indx,
     &                                     ininame(1:lstr)
          goto 99
        endif
      else
        write(stdout,1) vname(1,indxU)(1:lvar), ininame(1:lstr)
        goto 99
      endif
      lvar=lenstr(vname(1,indxV))
      ierr=nf_inq_varid (ncid, vname(1,indxV)(1:lvar), varid)
      if (ierr .eq. nf_noerr) then
        ierr=nf_fread (v(0,0,1), ncid, varid,
     &                                        indx, v3dvar)
        if (ierr .ne. nf_noerr) then
          write(stdout,2) vname(1,indxV)(1:lvar), indx,
     &                                       ininame(1:lstr)
          goto 99
        endif
      else
        write(stdout,1) vname(1,indxV)(1:lvar), ininame(1:lstr)
        goto 99
      endif
      do itrc=1,NT
        lvar=lenstr(vname(1,indxT+itrc-1))
        ierr=nf_inq_varid (ncid, vname(1,indxT+itrc-1)(1:lvar),
     &                                                    varid)
        if (ierr .eq. nf_noerr) then
          ierr=nf_fread (t(0,0,1,itrc), ncid,  varid,
     &                                               indx, r3dvar)
          if (ierr .ne. nf_noerr) then
            write(stdout,2) vname(1,indxT+itrc-1)(1:lvar), indx,
     &                                              ininame(1:lstr)
            goto 99
          endif
        else
          write(stdout,1) vname(1,indxT+itrc-1)(1:lvar),
     &                                    ininame(1:lstr)
          goto 99
        endif
      enddo
  1   format(/1x,'GET_INITIAL - unable to find variable:',    1x,A,
     &                            /15x,'in input NetCDF file:',1x,A/)
  2   format(/1x,'GET_INITIAL - error while reading variable:',1x, A,
     &    2x,'at time record =',i4/15x,'in input NetCDF file:',1x,A/)
      ierr=nf_close(ncid)
      return
  99  may_day_flag=2
      return
      end
