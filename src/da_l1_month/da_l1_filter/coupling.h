/* This is include file "coupling.h":
  ----------------------------------------------------
  Variables responsible for communication between two-
  and three-dimensional parts of the model.
*/
#ifdef SOLVE3D
      real grA(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE grA(BLOCK_PATTERN) BLOCK_CLAUSE
      real grS(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE grS(BLOCK_PATTERN) BLOCK_CLAUSE
      real rufrc(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE rufrc(BLOCK_PATTERN) BLOCK_CLAUSE
      real rvfrc(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE rvfrc(BLOCK_PATTERN) BLOCK_CLAUSE
      real rufrc_bak(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE rufrc_bak(BLOCK_PATTERN,2) BLOCK_CLAUSE
      real rvfrc_bak(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE rvfrc_bak(BLOCK_PATTERN,2) BLOCK_CLAUSE
      common /coup_grA/grA             /coup_grS/grS
     &       /coup_rufrc/rufrc         /coup_rvfrc/rvfrc
     &       /coup_rufrc_bak/rufrc_bak /coup_rvfrc_bak/rvfrc_bak



      real Zt_avg1(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE Zt_avg1(BLOCK_PATTERN) BLOCK_CLAUSE
      real DU_avg1(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE DU_avg1(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real DV_avg1(GLOBAL_2D_ARRAY,2)
CSDISTRIBUTE_RESHAPE DV_avg1(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real DU_avg2(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE DU_avg2(BLOCK_PATTERN) BLOCK_CLAUSE
      real DV_avg2(GLOBAL_2D_ARRAY)
CSDISTRIBUTE_RESHAPE DV_avg2(BLOCK_PATTERN) BLOCK_CLAUSE
      common /ocean_Zt_avg1/Zt_avg1
     &     /coup_DU_avg1/DU_avg1 /coup_DV_avg1/DV_avg1
     &     /coup_DU_avg2/DU_avg2 /coup_DV_avg2/DV_avg2
#endif
