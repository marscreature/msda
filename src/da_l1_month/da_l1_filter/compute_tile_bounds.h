/* Auxiliary module "compute_tile_bounds.h":
------------------------------------------------------
 Bounds designed to cover interior points of an array for
 the purpose of shared memory subdomain partitioning (tiling.) 
                
 Input: tile -- usually from 0 to NSUB_X*NSUB_E-1 -- indicates
                the specified subdomain. tile=NSUB_X*NSUB_E
                corresponds to the whole domain of RHO points
                treated as a single block.
 Output: Istr,Iend -- starting and ending computational indices
         Jstr,Jend    in XI-    and ETA-directions.
*/
      integer chunk_size_X,margin_X,chunk_size_E,margin_E

      parameter (chunk_size_X=(Lm+NSUB_X-1)/NSUB_X,
     &                     margin_X=(NSUB_X*chunk_size_X-Lm)/2,
     &           chunk_size_E=(Mm+NSUB_E-1)/NSUB_E,
     &                     margin_E=(NSUB_E*chunk_size_E-Mm)/2)
      integer Istr,Iend,Jstr,Jend, i_X,j_E
  
c---#define ALLOW_SINGLE_BLOCK_MODE
#ifdef  ALLOW_SINGLE_BLOCK_MODE
      if (tile.eq.NSUB_X*NSUB_E) then
        Istr=1
        Iend=Lm       ! MONOBLOCK VERSION:
        Jstr=1        ! DO NOT DO THE PARTITION 
        Jend=Mm
      else
#endif

        j_E=tile/NSUB_X
        i_X=tile-j_E*NSUB_X

        Istr=1+i_X*chunk_size_X-margin_X
        Iend=Istr+chunk_size_X-1
        Istr=max(Istr,1)
        Iend=min(Iend,Lm)

        Jstr=1+j_E*chunk_size_E-margin_E
        Jend=Jstr+chunk_size_E-1
        Jstr=max(Jstr,1)
        Jend=min(Jend,Mm)

#ifdef  ALLOW_SINGLE_BLOCK_MODE
      endif
#endif
