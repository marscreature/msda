#include "cppdefs.h"
#if defined DAS_JASONSSH || defined DAS_TPSSH

      subroutine das_innov_ssh (tile)
      implicit none
      integer tile
# include "param.h"
# include "compute_tile_bounds.h"
      call das_innov_ssh_tile (Istr,Iend,Jstr,Jend)
      return
      end

      subroutine das_innov_ssh_tile (Istr,Iend,Jstr,Jend)
!
!--------------------------------------------------------------------
!  Set initial conditions for momentum and tracer variables using
!  analytical expressions.
!--------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k, itrc
      real ojs1, otp
# include "param.h"
# include "das_param.h"
# include "scalars.h"
# include "grid.h"
# include "das_ocean.h"
# include "das_covar.h"
# include "das_innov.h"
!
!
# include "compute_auxiliary_bounds.h"
!
# ifdef EW_PERIODIC
#  define IR_RANGE Istr,Iend
#  define IU_RANGE Istr,Iend
# else
#  define IR_RANGE IstrR,IendR
#  define IU_RANGE  Istr,IendR
# endif

# ifdef NS_PERIODIC
#  define JR_RANGE Jstr,Jend
#  define JV_RANGE Jstr,Jend
# else
#  define JR_RANGE JstrR,JendR
#  define JV_RANGE  Jstr,JendR
# endif
!
!  forecast error variance
!------------------------------------------------------------------
!
      ojs1=0.04*0.04
      otp=0.04*0.04
!
! IMPORTANT WARNING: 
!       zeta_ref is an averaged field. Satellite SSH 
!       MUST be subtracted by an equivalent averaged 
!       field. Here it is assumed that the equivalent 
!       averaged field has been subtracted during the 
!       satellite SSH preprocessing. 
!
# ifdef DAS_JASONSSH
      do j=JR_RANGE
        do i=IR_RANGE
          ssh_js1(i,j)=
     &              ( (ssh_js1(i,j) -zeta_ref_js1(i,j))
     &               -(zeta_das(i,j)-zeta_ref(i,j)) )
     &                       *js1_mask(i,j)*mask_ref_js1(i,j)
          js1_oin(i,j)=1.0/ojs1
        enddo
      enddo
# endif
# ifdef DAS_TPSSH
      do j=JR_RANGE
        do i=IR_RANGE
          ssh_tp(i,j)=( (ssh_tp(i,j)  -zeta_ref_tp(i,j))
     &                 -(zeta_das(i,j)-zeta_ref(i,j)))
     &                       *tp_mask(i,j)*mask_ref_tp(i,j)
          tp_oin(i,j)=1.0/otp
        enddo
      enddo
# endif
!
#if defined EW_PERIODIC || defined NS_PERIODIC || defined  MPI
      call exchange_r2d_tile (Istr,Iend,Jstr,Jend,
     &                        ssh_js1(START_2D_ARRAY))
      call exchange_r2d_tile (Istr,Iend,Jstr,Jend,
     &                        ssh_tp(START_2D_ARRAY))
#endif
!
# undef IR_RANGE
# undef IU_RANGE
# undef JR_RANGE
# undef JV_RANGE
      return
      end
#else
      subroutine das_innov_ssh_empty
      return
      end
#endif /* DAS_JS1SSH DAS_TPSSH */
