      implicit none
#include "cppdefs.h"
#include "param.h"
#include "das_param.h"
#include "scalars.h"
#include "ncscrum.h"
#include "ocean3d.h"
#include "ocean2d.h"
#include "das_ocean.h"
#include "das_ocean9.h"
#include "das_covar.h"
#include "das_lbgfs.h"
#include "das_innov.h"
      integer tile, subs, trd, ierr, ios, nsyn
      integer i,j,k,max_tile
!
! minimization
!
      REAL EPS,GTOL
      INTEGER LP,IPRINT(2),IFLAG ,POINT,ITER,MP,ICALL
      INTEGER MAXITER
      LOGICAL DIAGCO
      EXTERNAL VA15CD
      COMMON /VA15DD/MP,LP, GTOL
!
! ... observation files
!
#ifdef MPI
      include 'mpif.h'
      real*8 start_time2, start_time1, exe_time
      call MPI_Init (ierr)
      start_time1=MPI_Wtime()
      call MPI_Setup (ierr)
      if (ierr.ne.0) goto 100                            !--> ERROR
c**   call MPI_Test
c**   goto 100
#endif

#define CR
#define SINGLE NSUB_X*NSUB_E,NSUB_X*NSUB_E !!!!

      call read_inp (ierr)           ! Read in tunable model
      if (ierr.ne.0) goto 100        ! parameters.
      call init_scalars (ierr)       ! Also initialize global
      if (ierr.ne.0) goto 100        ! scalar variables.
      call das_init_scalars          ! 

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! Create parallel threads;
        call start_timers()          ! start timers for each thread;
        call init_arrays (tile)      ! initialize (FIRST-TOUCH) model 
        call das_init_arrays (tile)  ! initialize (FIRST-TOUCH) DAS
      enddo                          ! global arrays (most of them 
CR      write(*,*) '-12' MYID        ! are just set to to zero).
!
      call get_grid                  ! from GRID NetCDF file). 
      if (may_day_flag.ne.0) goto 99     !-->  EXIT

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! Compute various metric
        call setup_grid1 (tile)      ! term combinations.
      enddo
CR      write(*,*) '-11' MYID
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call setup_grid2 (tile)
      enddo
CR      write(*,*) '-10' MYID
                                     ! Set up vertical S-coordinate
#ifdef SOLVE3D
      call set_scoord                ! and fast-time averaging  
#endif                               
CR      write(*,*) ' -9' MYID 

#if defined VIS_GRID || defined DIF_GRID
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! Rescale horizontal mixing
        call visc_rescale (tile)     ! coefficients according to
      enddo                          ! local grid size.
CR      write(*,*) ' -8' MYID 
#endif
#ifdef SOLVE3D
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! S-coordinate system, which
        call set_depth (tile)        ! may be neded by ana_ninitial
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call grid_stiffness (tile) 
      enddo                          ! (here it is assumed that free
CR      write(*,*) ' -7' MYID        ! surface zeta=0).
#endif
!
#ifdef ANA_INITIAL
      if (nrrec.eq.0) then
C$OMP PARALLEL DO PRIVATE(tile)
        do tile=0,NSUB_X*NSUB_E-1    ! for primitive variables
          call ana_initial (tile)    ! (analytically or read
        enddo                        ! from initial conditions
      else                           ! NetCDF file).
#endif
        call get_initial
        iic=0        ! required by set_depth
#ifdef ANA_INITIAL
      endif
#endif

CR      write(*,*) ' -6' MYID 
      if (may_day_flag.ne.0) goto 99     !-->  EXIT
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! S-coordinate system: at this
        call set_depth (tile)        ! time free surface is set to
      enddo                          ! a non-zero field, either 
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_setup1 (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_setup2 (tile)
      enddo
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! Interpolation from S-coordinate system
        call das_s2z_3d (tile)          ! to z-coordinates
      enddo

!C$OMP PARALLEL DO PRIVATE(tile)
!      do tile=0,NSUB_X*NSUB_E-1      ! Interpolation from S-coordinate system
!        call das_z2s (tile)          ! to z-coordinates
!      enddo      

!
!... prepare some basic variables
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1   
        call das_copy9 (tile)   
        call das_rho_eos9 (tile)   
      enddo
!
! ... get the file names of observations and error covariances
!
      call das_read_inp
      write(*,'(6x, A, 2x, I3)') 'das time level', time_level
!
! ... get the forecast error covariance
!
# ifdef DAS_ANA_BVAR
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_ana_bvar(tile)
      enddo
# else
      call das_get_bvar
# endif

      do k=1,ndas
        do j=0,Mm+1
          do i=0,Lm+1
!             bt_das(i,j,k,isalt)=0.5*bt_das(i,j,k,isalt)
             bt_das(i,j,k,isalt)=0.6*bt_das(i,j,k,isalt)
             bpsi_das(i,j,k)=0.35*bpsi_das(i,j,k)
             bchi_das(i,j,k)=0.15*bchi_das(i,j,k)
          enddo
        enddo
      enddo
        do j=0,Mm+1
          do i=0,Lm+1
             bz_das(i,j)=0.75*bz_das(i,j)
          enddo
        enddo


      write(*,*) 'bs_var'
      write(*,*) (bt_das(50,50,k,isalt),k=1,ndas)
      write(*,*) 'bs_var'
      write(*,*) (bt_das(30,40,k,isalt),k=1,ndas)
      write(*,*) 'bpsi_var'
      write(*,*) (bpsi_das(30,40,k),k=1,ndas)
      write(*,*) 'bchi_var'
      write(*,*) (bchi_das(30,40,k),k=1,ndas)
      write(*,*) 'bz_var'
      write(*,*) (bz_das(30,k), k=20,40)
!
# ifdef DAS_BVAR_CORR
      call das_get_corr
# endif
!
!if defined DAS_JASONSSH || defined DAS_TPSSH\
! || defined DAS_ERS2SSH\
! || defined DAS_GFOSSH
!     call das_get_zeta_ref
!endif
!
! ... read in all the observational data
!
                       ! IN-SITU, whoi, sio, ptsur, martin vertical profiles
                       !          NPS flight sst
      call das_get_insitu
                       ! HF radar
      call das_get_hfradar
!
      print*,'obs num:    ','sio    ', 'whoi    ','flight    ',
     &                     'num_hfradar'
      print*,'   ', prf_num_sio,prf_num_whoi,num_flight
     &            , num_hfradar
      print*,'obs num:    ', 'ptsur  ','num_martn', ' num_moor'
      print*,'   ',prf_num_ptsur,prf_num_martn,prf_num_moor
!
! ... get CalPoly & DORADO auv
!
      call das_get_auv
!      print*,'calpoly auv ', num_cal
!      print*,'dorado  auv ', num_dor
!
                       ! SST from TMI and dist_coast
# ifdef DAS_TMISST
      call das_get_tmi
# endif

!      write(*,*) 'dist_coast'
!      write(*,*) (dist_coast(i,20),i=0,Lm+1)
                       ! SST from AVHRR
# ifdef DAS_MCSST
      call das_get_mc
# endif
# ifdef DAS_GOES_SST
      call das_get_goes
# endif
                       ! SSH from JASON-1
# ifdef DAS_JASONSSH
      call das_get_js1
# endif
                       ! SSH from TOPEX/POSAIDON
# ifdef DAS_TPSSH
      call das_get_tp
# endif
!
                                          ! when no any observation
                                          ! data assimilation not executed
      if(.not. flag_tmi .and. .not. flag_mc .and. .not. flag_goes
     &   .and. .not. flag_js1 .and. .not. flag_tp 
     &   .and. .not. flag_sio .and. .not. flag_whoi
     &   .and. .not. flag_ptsur .and. .not. flag_martn 
     &   .and. .not. flag_moor .and. .not. flag_prof 
     &   .and. .not. flag_ship
     &   .and. .not. flag_cal .and. .not. flag_dor
     &   .and. .not. flag_flight
     &   .and. .not. flag_hfradar) then
        write(*,'(6x, A, 2(/6x, A))') 
     &              '!!! NO ANY OBSERVATION AVAILABLE',
     &              '!!! DATA ASSIMILATION NOT EXECUTED',
     &              '!!! NO ANALYSIS FILE'
        goto 99
      endif
!
! ... compute the innovation vectors
! ... required by the incremental formulation
!
!                  t_sio(64,17), mask_sio(64,17)
!
      max_tile=max(numthreads,prf_num_sio)
C$OMP PARALLEL DO PRIVATE(k), SHARED(max_tile)
      do k=1,max_tile
        CALL DAS_INNOV_SIO(k)
      enddo
!
      max_tile=max(numthreads,prf_num_whoi)
C$OMP PARALLEL DO PRIVATE(k), SHARED(max_tile)
      do k=1,max_tile
        call DAS_INNOV_WHOI(k)
      enddo
!
      max_tile=max(numthreads,prf_num_ptsur)
C$OMP PARALLEL DO PRIVATE(k), SHARED(max_tile)
      do k=1,max_tile
        call DAS_INNOV_PTSUR(k)
      enddo
!
      max_tile=max(numthreads,prf_num_martn)
C$OMP PARALLEL DO PRIVATE(k), SHARED(max_tile)
      do k=1,max_tile
        call DAS_INNOV_MARTN(k)
      enddo
!
      call DAS_INNOV_MOOR

!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_innov_flight(tile)
        call das_innov_calauv(tile)
        call das_innov_dorauv(tile)
# if defined DAS_JASONSSH || defined DAS_TPSSH
        call das_innov_js1(tile)
# endif
#if defined DAS_MCSST || defined DAS_TMISST
        call das_innov_sst(tile)
# endif
#if defined DAS_HFRADAR
        call das_innov_hfradar (tile)
#endif
      enddo
!
! print out innovations
!

!      call das_innov_print
!
!
!      write(*,*) 'innov whoi t and s'
!      do k=1,max_prf
!        write(*,*) 'k=',k
!      do i=1,prf_num_whoi
!        if (mask_whoi(i,k) .gt. 0.5 ) then
!        write(*,*) 't=',t_whoi(i,k),'s=',s_whoi(i,k)
!        endif
!      enddo
!      enddo
!
!      write(*,*) 'innov dorado auv t and s'
!      do k=1,max_prf_dor
!        write(*,*) 'k=',k
!      do j=30,140
!      do i=30,Lm
!        if (dor_mask(i,j,k) .gt. 0.5 ) then
!        write(*,*) 't=',t_dor(i,j,k),'s=',s_dor(i,j,k)
!        endif
!      enddo
!      enddo
!      enddo
!
! START MINIMIZATION
!
      ICALL=0
      IFLAG=0
      MAXITER=38
      IPRINT(1)= 1
      IPRINT(2)= 0
      DIAGCO= .FALSE.
!      EPS= 1.0D-5     ! minimization criterion
      EPS= 1.0D-6     ! minimization criterion
                      ! |grad|/max(1, |x|) < EPS, terminated
      LP = 6
      MP = 6

!
! ... initialize the first guess of increment to be zero
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_CA2ZERO (tile)
      enddo
      CALL DAS_CA2VEC    ! control variable vector
!
! ... minimization interation starts
!
  10  CONTINUE
!
! ... initialize the cost function
!
      COSTF=0.0
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_MASK(tile)
      enddo
!
! ... compute the matrix-vector product 
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL das_cross_ts(tile)
      enddo

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_MATVEC(tile)  
      enddo
!
! ... terms of the SSH
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_VARVEC (tile)      ! t_s
        CALL DAS_LIN_RHO_EOS (tile) ! rho_s
        CALL DAS_DIAG_SSH (tile)    ! zeta_s
#if defined DAS_JASONSSH
        CALL DAS_COST_JS1(tile)   ! cost function SSH
#endif
        CALL DAS_DIAG_PRESSURE (tile)
      enddo
#if defined DAS_HFRADAR
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_GEOS_UV_P (tile)      !u_s, v_s before psichi
        CALL DAS_GEO_RATIO (tile)
        CALL DAS_PSICHI_UV (tile)
        CALL DAS_COST_HFRADAR (tile)
      enddo
#endif
!
! ... term of the SST
!
! flight
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_FLIGHT(tile)   ! cost function SST
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_CALAUV(tile)   
        CALL DAS_COST_DORAUV(tile)   
      enddo
!
! satellite
!
#if defined DAS_MCSST || defined DAS_TMISST
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COST_SST(tile)   ! cost function SST
      enddo
#endif
!
! ... gliders
!
      CALL DAS_COST_GLIDER
!
! ... term of the background
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_COSTB(tile)   ! cost function background
      enddo

!
! ... compute the gradient with respect to u, v, t, s
!
! ... clean the adjoint arrays
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_ZERO(tile)
      enddo
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_COSTB(tile)
      enddo
!
! glider
!
      CALL DAS_ADJ_CGLIDER
!
#if defined DAS_MCSST || defined DAS_TMISST
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_CSST(tile)
      enddo
#endif
!
! CalPoly AUV
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_DAUV(tile)
        CALL DAS_ADJ_CAUV(tile)
      enddo
!
! flight
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_CFLIGHT(tile)
      enddo
!
#if defined DAS_HFRADAR
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_CHFRADAR (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_PSICHI_UV (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_GEO_RATIO (TILE)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_GEOS_UV_P (tile)
      enddo
#endif   /* DAS_HFRADAR */
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_DIAG_P (tile)
      enddo
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
#if defined DAS_JASONSSH || defined DAS_TPSSH\
  || defined DAS_TPSSH || defined DAS_ERS2SSH\
  || defined DAS_GFOSSH
        CALL DAS_ADJ_CJS1(tile)   ! cost function SSH
#endif
        CALL DAS_ADJ_DIAG_SSH (tile)
        CALL DAS_ADJ_RHO_EOS (tile)
        CALL DAS_ADJ_VARVEC (tile)
      enddo
!
! ... mask the adjoint variables
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_MASK(tile)
      enddo
!
! ... compute the matrix-vector product
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_MATVEC(tile)
      enddo

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_CROSS_TS(tile)
      enddo
!
! ... clean the intermediate adjoint arrays
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL DAS_ADJ_ZERO_S(tile)
      enddo
!
      CALL DAS_GA2VEC    ! gradient vector
!
      CALL mbfgs(NDIM,MSAVE,COSTF,DIAGCO,IPRINT,EPS,POINT,IFLAG)

      CALL DAS_VEC2CA        !vector to control variables

      IF(IFLAG.LE.0) GO TO 50
      ICALL=ICALL + 1
      IF(ICALL.GT.MAXITER) GO TO 50
  
      GOTO 10
 
  50  CONTINUE
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!   minimization done
!
      print*, '     FLAG=', IFLAG
      if (IFLAG .lt. 0) then
        write(*, '(6x, A, (/6x, A))')
     &   '!!! MINIMIZATION NOT COMPLETED',
     &   '!!! NO ASSIMILATED ANALYSIS GENERATED'
        goto 99
      else
        write(*, '(6x, A)')
     &   ' MINIMIZATION COMPLETED'
      endif
!
! ... compute incremental analysis
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        CALL das_cross_ts(tile)
      enddo

C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1   !temp and salt
        call das_matvec (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_varvec (tile)       ! t_s
      enddo
!
#define DAS_SMOOTH
#ifdef DAS_SMOOTH
                    ! smooth t_s to t_sm
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_innov_smooth (tile)
      enddo
                    ! then t_sm to t_s
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_copy_smooth (tile)
      enddo
#endif
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_lin_rho_eos (tile)  ! rho
        call das_diag_ssh (tile)     ! zeta
        call das_diag_pressure (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_zeta_p_smooth (tile)
      enddo
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_zeta_p_smooth2 (tile)
      enddo
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call das_geos_uv_p (tile)      ! u and v
!        call das_geo_ratio (tile)
         call das_psichi_uv (tile)
        call das_analysis (tile)
      enddo
!
! print
!
      call das_innov_ana_print
!
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1
        call rho_eos (tile)
      enddo
!
! ... create new restart file
! ... with the analysis from 3DVAR
!
      iic=ntstart
      call wrt_rst   
!...
!
      write(*,*) '     iic==', iic

      write(*,*) 't_s j=20 '
      write(*,*) (t_s(i,20,ndas,1), i=0,Lm+1)

      write(*,*) 'psi 80 '
      write(*,*) (psi_s(15,j,ndas), j=1,Mm)

      write(*,*) 'chi 80 '
      write(*,*) (chi_s(15,j,ndas), j=1,Mm)

      write(*,*) 'rho 80 '
      write(*,*) (rho(10,80,k), k=1,n)
      write(*,*) 'rho 120 '
      write(*,*) (rho(70,112,k), k=1,n)
                                     ! JOB DONE  
  99  continue                       ! SHUTDOWN:
C$OMP PARALLEL DO PRIVATE(tile)
      do tile=0,NSUB_X*NSUB_E-1      ! close netCDF files.
        call stop_timers()
      enddo
      call closecdf

 100  continue
#ifdef MPI
      call MPI_Barrier(MPI_COMM_WORLD, ierr)
      start_time2=MPI_Wtime()
      exe_time = start_time2 - start_time1
      if(mynode.eq.0) print*,'exe_time =',exe_time
      call MPI_Finalize (ierr)
#endif
      stop
      end

