      subroutine set_cycle (ncid, varid, ntime, cycle_length,
     &                                        icycle, trecord)
      implicit none
      real              cycle_length,    tstart,  tend,     cff
      integer ncid,     icycle,          tshift,  vartype,  lvar,
     &        varid,    trecord,         irec,    nvatts,   latt,
     &                  ntime,           i,ierr,  nvdims,   ldim,
     &                                   size,    vdims(5), lenstr
      character*16 varname, dimname, attname
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=384,  MMmH=390, NnH=66)
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLmH,  MMm=MMmH,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      real  x_sponge,   v_sponge
       real  tauT_in, tauT_out, tauM_in, tauM_out
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &                      , x_sponge,   v_sponge
     &                      , tauT_in, tauT_out, tauM_in, tauM_out
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      include 'netcdf.inc'
      ierr=nf_inq_var (ncid, varid, varname, vartype,
     &                       nvdims,  vdims,  nvatts)
      if (ierr .eq. nf_noerr) then
        lvar=lenstr(varname)
        ntime=0
        do i=1,nvdims
          ierr=nf_inq_dim (ncid, vdims(i), dimname, size)
          if (ierr .eq. nf_noerr) then
            ldim=lenstr(dimname)
            if (dimname(ldim-4:ldim) .eq. '_time') then
              if (ntime.eq.0) then
                ntime=size
              else
                write (stdout,'(/1x,4A/18x,A)') 'SET_CYCLE ERROR: ',
     &                     'variable ''', varname(1:lvar), ''' has',
     &                     'more than one _time dimension.'
                goto 99
              endif
            endif
          else
            write(stdout,'(/1x,4A/)') 'SET_CYCLE ERROR while ',
     &                  'inquiring dimensions for variable ''',
     &                                   varname(1:lvar), '''.'
            goto 99
          endif
        enddo
        if (ntime.gt.1) then
          cycle_length=0.
          do i=1,nvatts
            ierr=nf_inq_attname (ncid, varid, i, attname)
            if (ierr .eq. nf_noerr) then
              latt=lenstr(attname)
              if (attname(1:latt) .eq. 'cycle_length') then
                ierr=nf_get_att_double (ncid, varid, attname(1:latt),
     &                                                 cycle_length)
                if (ierr .eq. nf_noerr) then
                  cycle_length=cycle_length*day2sec
                else
                  write(stdout,'(/1x,4A/)') 'SET_CYCLE ERROR while ',
     &                 'reading attribute ''', attname(1:latt), '''.'
                  goto 99
                endif
              endif
            else
              write(stdout,'(/1x,4A/)') 'SET_CYCLE ERROR while ',
     &                    'inquiring attributes for variable ''',
     &                                     varname(1:lvar), '''.'
              goto 99
            endif
          enddo
        else
          cycle_length=-1.
        endif
      else
        write(stdout,'(/1x,2A,I4/18x,A,I4/)') 'SET_CYCLE ERROR: ',
     &           'Cannot inquire about variable with ID =', varid,
     &           'in input file; netCDF error code =', ierr
        goto 99
      endif
      ierr=nf_get_var1_double(ncid, varid, 1, tstart)
      if (ierr .ne. nf_noerr) goto 10
      tstart=tstart*day2sec
      if (cycle_length.gt.0.) then
        cff=time-tstart
        icycle=int(abs(cff)/cycle_length)
        if (cff.lt.0.) icycle=-1-icycle
        tstart=tstart+icycle*cycle_length
      else
        icycle=0
      endif
      tshift=icycle
      trecord=0
      i=1
  1    irec=i+1
        if (cycle_length.gt.0. .and. irec.gt.ntime) then
          irec=1
          tshift=tshift+1
        endif
        if (irec.le.ntime) then
          ierr=nf_get_var1_double(ncid, varid, irec, tend)
          if (ierr .ne. nf_noerr) goto 10
          tend=tend*day2sec+tshift*cycle_length
          if (tstart.le.time .and. time.lt.tend) then
            trecord=i
          elseif (irec.ne.1) then
            i=irec
            tstart=tend
            goto 1
          endif
        endif
      if (trecord.ne.0) then
        trecord=trecord-1
        if (trecord.lt.1 .and. cycle_length.gt.0.) then
          trecord=trecord+ntime
          icycle=icycle-1
        endif
        return
      endif
      if (cycle_length.gt.0.) then
        write(stdout,2) varname(1:lvar)
  2     format(/1x, 'SET_CYCLE ERROR: Algorithm failure',
     &                ' while processing variable: ',A,'.'/)
      else
        write(stdout,3) varname(1:lvar), tdays, tend*sec2day
  3     format(/1x, 'SET_CYCLE ERROR: non-cycling regime,',
     &         ' but model time exeeds'  /18x,  'time of the',
     &         ' last available data record for variable: ',A/
     &         18x, 'TDAYS = ', G12.4, 2x, 'TLAST = ', G12.4/)
      endif
      goto 99
  10  write(stdout,'(1x,4A)') 'SET_CYCLE ERROR while reading ',
     &                    'variable ''', varname(1:lvar), '''.'
  99  may_day_flag=2
      return
      end
      integer function advance_cycle (cycle_length, ntime,
     &                                      icycle, trecord)
      implicit none
      real cycle_length
      integer ntime, icycle, trecord, ierr
      ierr=0
      trecord=trecord+1
      if (trecord.gt.ntime) then
        if (cycle_length.gt.0.) then
          trecord=1
          icycle=icycle+1
        else
          ierr=1
        endif
      endif
      advance_cycle=ierr
      return
      end
