      subroutine check_switches1 (ierr)
      implicit none
      integer ierr, is,ie, iexample
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=384,  MMmH=390, NnH=66)
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLmH,  MMm=MMmH,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer max_opt_size
      parameter (max_opt_size=2880)
      character*2880 Coptions,srcs
      common /strings/ Coptions,srcs
       write(stdout,'(/1x,A/)')
     &      'Activated C-preprocessing Options:'
      do is=1,max_opt_size
        Coptions(is:is)=' '
      enddo
      iexample=0
      is=1
      iexample=iexample+1
       write(stdout,'(10x,A)') 'USWEST'
      ie=is + 5
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='USWEST'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'SOLVE3D'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SOLVE3D'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'UV_ADV'
      ie=is + 5
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='UV_ADV'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'UV_COR'
      ie=is + 5
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='UV_COR'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'MIX_GP_UV'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='MIX_GP_UV'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'SALINITY'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SALINITY'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'NONLIN_EOS'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='NONLIN_EOS'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'TS_DIF2'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='TS_DIF2'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'MIX_GP_TS'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='MIX_GP_TS'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'ANA_BTFLUX'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_BTFLUX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'UV_VIS2'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='UV_VIS2'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'OBC_WEST'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_WEST'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'OBC_NORTH'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_NORTH'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'OBC_SOUTH'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_SOUTH'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'TCLIMATOLOGY'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='TCLIMATOLOGY'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'TNUDGING'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='TNUDGING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'QCORRECTION'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='QCORRECTION'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'ANA_BSFLUX'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ANA_BSFLUX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'LMD_MIXING'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='LMD_MIXING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'LMD_RIMIX'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='LMD_RIMIX'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'LMD_CONVEC'
      ie=is + 9
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='LMD_CONVEC'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'LMD_KPP'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='LMD_KPP'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'CURVGRID'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='CURVGRID'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'SPHERICAL'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SPHERICAL'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'MASKING'
      ie=is + 6
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='MASKING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'OBC_M2ORLANSKI'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_M2ORLANSKI'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'OBC_M3ORLANSKI'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_M3ORLANSKI'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'OBC_TORLANSKI'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_TORLANSKI'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'AVERAGES'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='AVERAGES'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'ZNUDGING'
      ie=is + 7
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='ZNUDGING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'OBC_VOLCONS'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OBC_VOLCONS'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'OPENMP'
      ie=is + 5
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='OPENMP'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'UCLIMATOLOGY'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='UCLIMATOLOGY'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'M3NUDGING'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='M3NUDGING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'M2NUDGING'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='M2NUDGING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'SPONGE'
      ie=is + 5
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SPONGE'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'SFLX_CORR'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='SFLX_CORR'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'CLIMAT_TS_MIXH'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='CLIMAT_TS_MIXH'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_BVAR_CORR'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_BVAR_CORR'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_MCSST'
      ie=is + 8
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_MCSST'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_GOES_SST'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_GOES_SST'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_JASONSSH'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_JASONSSH'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_SWOTSSH'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_SWOTSSH'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_WHOIGLIDER'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_WHOIGLIDER'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_SIOGLIDER'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_SIOGLIDER'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_PTSURCDT'
      ie=is +11
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_PTSURCDT'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_MARTINCDT'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_MARTINCDT'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_MOORINGNCDT'
      ie=is +14
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_MOORINGNCDT'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_NPSFLIGHT'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_NPSFLIGHT'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_CALPOLYAUV'
      ie=is +13
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_CALPOLYAUV'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_DORADOAUV'
      ie=is +12
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_DORADOAUV'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_MOORING'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_MOORING'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(10x,A)') 'DAS_HFRADAR'
      ie=is +10
      if (ie.ge.max_opt_size) goto 99
      Coptions(is:ie)='DAS_HFRADAR'
      Coptions(ie+1:ie+1)=' '
      is=ie+2
       write(stdout,'(/)')
      if (iexample.eq.0) then
         write(stdout,'(1x,A)')
     & 'ERROR in "cppdefs.h": no configuration is specified.'
        ierr=ierr+1
      elseif (iexample.gt.1) then
         write(stdout,'(1x,A)')
     & 'ERROR: more than one configuration in "cppdefs.h".'
        ierr=ierr+1
      endif
      return
  99   write(stdout,'(/1x,A,A/14x,A)')
     &  'CHECKDEFS -- ERROR: Unsufficient size of string Coptions',
     &  'in file "strings.h".', 'Increase the size it and recompile.'
      ierr=ierr+1
      return
      end
