      SUBROUTINE mbfgs(N,M,F,DIAGCO,IPRINT,EPS,POINT,IFLAG)
      integer  LLmH,  MMmH, NnH, NSUB_XH, NSUB_EH,NPPH
      parameter (LLmH=384,  MMmH=390, NnH=66)
      parameter (NSUB_XH=2, NSUB_EH=28, NPPH=56)
      integer  LLm,Lm,  MMm,Mm
      parameter (LLm=LLmH,  MMm=MMmH)
      parameter (NSUB_X=NSUB_XH,NSUB_E=NSUB_EH,NPP=NPPH,Lm=LLm,Mm=MMm)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+1)*(Mm+1)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_prf
      PARAMETER( max_prf=NDAS)
      integer max_sio, max_whoi, max_flight,
     &        max_hfradar,max_hfradar6,max_js1
      PARAMETER(max_sio=100,max_whoi=100,max_flight=30000,
     &         max_hfradar=30000,max_hfradar6=30000,max_js1=8000)
      integer max_ptsur
      PARAMETER(max_ptsur=100)
      integer max_martn
      PARAMETER(max_martn=100)
      integer max_moor
      PARAMETER(max_moor=280)
      real sio_ot,sio_os,whoi_ot,whoi_os,ptsur_ot,ptsur_os,
     &              martn_ot,martn_os,moor_ot,moor_os, hf_ouv,
     &              hf_ouv6,fl_ot,js1_ossh,swot_ossh,hcmin
      PARAMETER( sio_ot=1.0/(1.185*1.185),sio_os=1.0/(0.16*0.16),
     &          whoi_ot=1.0/(1.17*1.17),whoi_os=1.0/(0.16*0.16),
     &        ptsur_ot=1.0/(1.65*1.65),ptsur_os=1.0/(0.2*0.2),
     &        martn_ot=1.0/(1.65*1.65),martn_os=1.0/(0.2*0.2),
     &        moor_ot=1.0/(1.3*1.3),moor_os=1.0/(0.18*0.18),
     &        js1_ossh=1.0/(0.03*0.03),swot_ossh=1.0/(0.07*0.07),
     &        hcmin=50.0,
     &        fl_ot=1.0/(1.0*1.0),
     &        hf_ouv=1.0/(0.07*0.07),hf_ouv6=1.0/(0.2*0.2)
     &         )
      integer max_prf_cal,max_cal
      PARAMETER(max_prf_cal=NDAS,max_cal=100)
      integer max_prf_dor,max_dor
      PARAMETER(max_prf_dor=NDAS,max_dor=100)
      real cal_ot,cal_os,dor_ot,dor_os
      PARAMETER( cal_ot=1.0/(1.0*1.0),
     &           cal_os=1.0/(0.2*0.2),
     &           dor_ot=1.0/(1.0*1.0),
     &           dor_os=1.0/(0.2*0.2)
     &         )
      real geo_ratio, cross_tsp
      PARAMETER( geo_ratio = 1.0, cross_tsp=0.)
      integer sm_rad
      PARAMETER( sm_rad=5)
      integer Local_len
      PARAMETER( Local_len=20)
      real sz_rad
      PARAMETER( sz_rad=6.0)
      integer sz_rad_len
      PARAMETER( sz_rad_len=20)
      real regp, reguv
      PARAMETER( regp=1.0/(10.0*10.0) )
      PARAMETER( reguv=1.0/(0.6*0.6) )
      DOUBLE PRECISION X(NDIM),G(NDIM),S(NXM),Y(NXM),DIAG(NDIM),
     &                      W(NWORK)
      common /das_miniz/X,G,S,Y,DIAG,W
      DOUBLE PRECISION FTOL,GTOL,XTOL,STPMIN,STPMAX,STP,F,YS,SQ,
     *              YR,BETA,ONE,ZERO,EPS,XNORM,GNORM,YY,DDOT,STP1
      INTEGER BOUND,LP,ITER,NFUN,NFEV,IPRINT(2),POINT,CP,IFLAG
      LOGICAL FINISH,DIAGCO
      COMMON /VA15DD/MP,LP, GTOL
      SAVE
      DATA ONE,ZERO/1.0D+0,0.0D+0/
      IF(IFLAG.EQ.0) GO TO 1
      GO TO (72,10) IFLAG
   1  ITER= 0
      IF(N.LE.0.OR.M.LE.0) GO TO 96
      IF(GTOL.LE.1.D-04) THEN
        IF(LP.GT.0) WRITE(LP,145)
        GTOL=1.D-02
      ENDIF
      NFUN= 1
      POINT= 0
      FINISH= .FALSE.
      IF(DIAGCO) THEN
         DO 3 I=1,N
 3       IF (DIAG(I).LE.ZERO) GO TO 95
      ELSE
         DO 4 I=1,N
 4       DIAG(I)= 1.0D0
      ENDIF
      DO 5 I=1,N
 5    S(I)= -G(I)*DIAG(I)
      GNORM= DSQRT(DDOT(N,G,1,G,1))
      STP1= ONE/GNORM
      FTOL= 1.0D-4
      XTOL= 1.0D-17
      STPMIN= 1.0D-20
      STPMAX= 1.0D+20
      MAXFEV= 20
       CALL VA15BD(IPRINT,ITER,NFUN,
     *                     N,M,X,F,G,STP,FINISH)
 8    ITER= ITER+1
      INFO=0
      BOUND=ITER-1
      IF (ITER .GT. M)BOUND=M
      IF(ITER.EQ.1) GO TO 65
      IF(.NOT.DIAGCO) THEN
         DO 9 I=1,N
   9     DIAG(I)= YS/YY
      ELSE
         IFLAG=2
         RETURN
      ENDIF
  10  CONTINUE
      DO 11 I=1,N
  11  IF (DIAG(I).LE.ZERO) GO TO 95
      CP= POINT
      IF (POINT.EQ.0) CP=M
      W(N+CP)= ONE/YS
      DO 12 I=1,N
  12  W(I)= -G(I)
      CP= POINT
      DO 25 II= 1,BOUND
         CP=CP-1
         IF (CP.EQ. -1)CP=M-1
         SQ= DDOT(N,S(CP*N+1),1,W,1)
         W(N+M+CP+1)= W(N+CP+1)*SQ
         DO 20 K=1,N
  20     W(K)= W(K)-W(N+M+CP+1)*Y(CP*N+K)
  25  CONTINUE
      DO 30 I=1,N
  30  W(I)=DIAG(I)*W(I)
      DO 45 II=1,BOUND
         YR= DDOT(N,Y(CP*N+1),1,W,1)
         BETA= W(N+CP+1)*YR
         DO 40 K=1,N
  40     W(K)= W(K)+S(CP*N+K)*(W(N+M+CP+1)-BETA)
         CP=CP+1
         IF (CP.EQ.M)CP=0
  45  CONTINUE
       DO 60 J=1,N
  60   S(POINT*N+J)= W(J)
  65  NFEV=0
      STP=ONE
      IF (ITER.EQ.1) STP=STP1
      DO 70 I=1,N
  70  W(I)=G(I)
  72  CONTINUE
      CALL VD05AD(N,X,F,G,S(POINT*N+1),STP,FTOL,GTOL,
     *            XTOL,STPMIN,STPMAX,MAXFEV,INFO,NFEV,DIAG)
      IF (INFO .EQ. -1) THEN
        IFLAG=1
        RETURN
      ENDIF
      IF (INFO .NE. 1) GO TO 90
      NFUN= NFUN + NFEV
      NPT=POINT*N
      DO 75 I=1,N
      S(NPT+I)= STP*S(NPT+I)
  75  Y(NPT+I)= G(I)-W(I)
      YS= DDOT(N,Y(NPT+1),1,S(NPT+1),1)
      YY= DDOT(N,Y(NPT+1),1,Y(NPT+1),1)
      POINT=POINT+1
      IF (POINT.EQ.M)POINT=0
      GNORM= DDOT(N,G,1,G,1)
      GNORM=DSQRT(GNORM)
      XNORM= DDOT(N,X,1,X,1)
      XNORM=DSQRT(XNORM)
      XNORM= DMAX1(1.0D0,XNORM)
      IF (GNORM/XNORM .LE. EPS) FINISH=.TRUE.
      CALL VA15BD(IPRINT,ITER,NFUN,
     *               N,M,X,F,G,STP,FINISH)
      IF (FINISH) THEN
         IFLAG=0
         RETURN
      ENDIF
      GO TO 8
  90  IF(LP.LE.0) RETURN
      IF (INFO.EQ.0) THEN
           IFLAG= -1
           WRITE(LP,100)IFLAG
      ELSE IF (INFO.EQ.2) THEN
           IFLAG= -2
           WRITE(LP,105)IFLAG
      ELSE IF (INFO.EQ.3) THEN
           IFLAG= -3
           WRITE(LP,110)IFLAG
      ELSE IF (INFO.EQ.4) THEN
           IFLAG= -4
           WRITE(LP,115)IFLAG
      ELSE IF (INFO.EQ.5) THEN
           IFLAG= -5
           WRITE(LP,120)IFLAG
      ELSE IF (INFO.EQ.6) THEN
           IFLAG= -6
           WRITE(LP,125)IFLAG
      ENDIF
      RETURN
  95  IFLAG= -7
      IF(LP.GT.0) WRITE(LP,135)IFLAG,I
      RETURN
  96  IFLAG= -8
      IF(LP.GT.0) WRITE(LP,140)IFLAG
 100  FORMAT(/' IFLAG= ',I2,/' IMPROPER INPUT PARAMETERS DURING',
     .       ' THE LINE SEARCH.')
 105  FORMAT(/' IFLAG= ',I2,/' RELATIVE WIDTH OF THE INTERVAL OF',
     .       ' UNCERTAINTY IN THE LINE SEARCH'/'IS OF THE ORDER OF',
     .         'MACHINE ROUNDOFF.')
 110  FORMAT(/' IFLAG= ',I2,/' NUMBER OF CALLS TO FUNCTION IN THE',
     .       ' LINE SEARCH HAS REACHED 20.')
 115  FORMAT(/' IFLAG= ',I2,/' THE STEP IN THE LINE SEARCH IS',
     .       ' TOO SMALL.')
 120  FORMAT(/' IFLAG= ',I2,/' THE STEP IN THE LINE SEARCH IS',
     .       ' TOO LARGE.')
 125  FORMAT(/' IFLAG= ',I2,/' ROUNDING ERRORS PREVENT FURTHER',
     .       ' PROGRESS IN THE LINE SEARCH.')
 135  FORMAT(/' IFLAG= ',I2,/' THE',I5,'-TH DIAGONAL ELEMENT OF THE',
     .       ' INVERSE HESSIAN APPROXIMATION IS NOT POSITIVE')
 140  FORMAT(/' IFLAG= ',I2,/' IMPROPER INPUT PARAMETERS (N OR M',
     .       ' ARE NOT POSITIVE)')
 145  FORMAT(/'  GTOL IS LESS THAN OR EQUAL TO 1.D-04',
     .       / 'IT HAS BEEN RESET TO 1.D-02')
      RETURN
      END
      SUBROUTINE VA15BD(IPRINT,ITER,NFUN,
     *                     N,M,X,F,G,STP,FINISH)
      DOUBLE PRECISION X(N),G(N),F,GNORM,STP,FACTOR,DDOT,GTOL
      INTEGER IPRINT(2),ITER,NFUN,PROB,LP
      LOGICAL FINISH
      COMMON /SET/ FACTOR,PROB
      COMMON /VA15DD/MP,LP, GTOL
      IF (IPRINT(1).LT.0)RETURN
      GNORM= DDOT(N,G,1,G,1)
      GNORM= DSQRT(GNORM)
      IF (ITER.EQ.0)THEN
           WRITE(MP,10)
           WRITE(MP,20) PROB,N,M
           WRITE(MP,30)F,GNORM
                 IF (IPRINT(2).GE.1)THEN
                     WRITE(MP,40)
                     WRITE(MP,50) (X(I),I=1,N)
                     WRITE(MP,60)
                     WRITE(MP,50) (G(I),I=1,N)
                  ENDIF
           WRITE(MP,10)
           WRITE(MP,70)
      ELSE
          IF ((IPRINT(1).EQ.0).AND.(ITER.NE.1.AND..NOT.FINISH))RETURN
              IF (IPRINT(1).NE.0)THEN
                   IF(MOD(ITER-1,IPRINT(1)).EQ.0.OR.FINISH)THEN
                         WRITE(MP,80)ITER,NFUN,F,GNORM,STP
                   ELSE
                         RETURN
                   ENDIF
              ELSE
                   WRITE(MP,80)ITER,NFUN,F,GNORM,STP
              ENDIF
              IF (IPRINT(2).EQ.2.OR.IPRINT(2).EQ.3)THEN
                    IF (FINISH)THEN
                        WRITE(MP,90)
               WRITE(MP,50) (X(I),I=1,N)
        WRITE(MP,60)
        WRITE(MP,50) (G(I),I=1,N)
                    ENDIF
              ENDIF
            IF (FINISH) WRITE(MP,100)
      ENDIF
 10   FORMAT('*************************************************')
 20   FORMAT(' PROB=',I3,'   N=',I9,'   NUMBER OF CORRECTIONS=',I2)
 30   FORMAT('    ',E15.8,'          ',E15.8)
 40   FORMAT(' VECTOR X= ')
 50   FORMAT(6(2X,1E15.8))
 60   FORMAT(' GRADIENT VECTOR G= ')
 70   FORMAT(/'     I     NFN',4X,'FUNC',8X,
     &            '     GNORM',7X,'     STEPLENGTH'/)
 80   FORMAT(2(I6,1X),3X,3(E15.8,2X))
 90   FORMAT(' FINAL POINT X= ')
 100  FORMAT(/' THE MINIMIZATION TERMINATED WITHOUT DETECTING ERRORS.',
     .       /' IFLAG = 0')
      RETURN
      END
      BLOCK DATA VA15CD
      COMMON /VA15DD/MP,LP, GTOL
      INTEGER LP
      DOUBLE PRECISION GTOL
      DATA MP,LP,GTOL/6,6,9.0D-01/
      END
      SUBROUTINE VD05AD(N,X,F,G,S,STP,FTOL,GTOL,XTOL,
     *                  STPMIN,STPMAX,MAXFEV,INFO,NFEV,WA)
      INTEGER N,MAXFEV,INFO,NFEV
      DOUBLE PRECISION F,STP,FTOL,GTOL,XTOL,STPMIN,STPMAX
      DOUBLE PRECISION X(N),G(N),S(N),WA(N)
      SAVE
      INTEGER INFOC,J
      LOGICAL BRACKT,STAGE1
      DOUBLE PRECISION DG,DGM,DGINIT,DGTEST,DGX,DGXM,DGY,DGYM,
     *       FINIT,FTEST1,FM,FX,FXM,FY,FYM,P5,P66,STX,STY,
     *       STMIN,STMAX,WIDTH,WIDTH1,XTRAPF,ZERO
      DATA P5,P66,XTRAPF,ZERO /0.5D0,0.66D0,4.0D0,0.0D0/
      IF(INFO.EQ.-1) GO TO 45
      INFOC = 1
      IF (N .LE. 0 .OR. STP .LE. ZERO .OR. FTOL .LT. ZERO .OR.
     *    GTOL .LT. ZERO .OR. XTOL .LT. ZERO .OR. STPMIN .LT. ZERO
     *    .OR. STPMAX .LT. STPMIN .OR. MAXFEV .LE. 0) RETURN
      DGINIT = ZERO
      DO 10 J = 1, N
         DGINIT = DGINIT + G(J)*S(J)
   10    CONTINUE
      IF (DGINIT .GE. ZERO) RETURN
      BRACKT = .FALSE.
      STAGE1 = .TRUE.
      NFEV = 0
      FINIT = F
      DGTEST = FTOL*DGINIT
      WIDTH = STPMAX - STPMIN
      WIDTH1 = WIDTH/P5
      DO 20 J = 1, N
         WA(J) = X(J)
   20    CONTINUE
      STX = ZERO
      FX = FINIT
      DGX = DGINIT
      STY = ZERO
      FY = FINIT
      DGY = DGINIT
   30 CONTINUE
         IF (BRACKT) THEN
            STMIN = MIN(STX,STY)
            STMAX = MAX(STX,STY)
         ELSE
            STMIN = STX
            STMAX = STP + XTRAPF*(STP - STX)
            END IF
         STP = MAX(STP,STPMIN)
         STP = MIN(STP,STPMAX)
         IF ((BRACKT .AND. (STP .LE. STMIN .OR. STP .GE. STMAX))
     *      .OR. NFEV .GE. MAXFEV-1 .OR. INFOC .EQ. 0
     *      .OR. (BRACKT .AND. STMAX-STMIN .LE. XTOL*STMAX)) STP = STX
         DO 40 J = 1, N
            X(J) = WA(J) + STP*S(J)
   40       CONTINUE
         INFO=-1
         RETURN
   45    INFO=0
         NFEV = NFEV + 1
         DG = ZERO
         DO 50 J = 1, N
            DG = DG + G(J)*S(J)
   50       CONTINUE
         FTEST1 = FINIT + STP*DGTEST
         IF ((BRACKT .AND. (STP .LE. STMIN .OR. STP .GE. STMAX))
     *      .OR. INFOC .EQ. 0) INFO = 6
         IF (STP .EQ. STPMAX .AND.
     *       F .LE. FTEST1 .AND. DG .LE. DGTEST) INFO = 5
         IF (STP .EQ. STPMIN .AND.
     *       (F .GT. FTEST1 .OR. DG .GE. DGTEST)) INFO = 4
         IF (NFEV .GE. MAXFEV) INFO = 3
         IF (BRACKT .AND. STMAX-STMIN .LE. XTOL*STMAX) INFO = 2
         IF (F .LE. FTEST1 .AND. ABS(DG) .LE. GTOL*(-DGINIT)) INFO = 1
         IF (INFO .NE. 0) RETURN
         IF (STAGE1 .AND. F .LE. FTEST1 .AND.
     *       DG .GE. MIN(FTOL,GTOL)*DGINIT) STAGE1 = .FALSE.
         IF (STAGE1 .AND. F .LE. FX .AND. F .GT. FTEST1) THEN
            FM = F - STP*DGTEST
            FXM = FX - STX*DGTEST
            FYM = FY - STY*DGTEST
            DGM = DG - DGTEST
            DGXM = DGX - DGTEST
            DGYM = DGY - DGTEST
            CALL VD05BD(STX,FXM,DGXM,STY,FYM,DGYM,STP,FM,DGM,
     *                 BRACKT,STMIN,STMAX,INFOC)
            FX = FXM + STX*DGTEST
            FY = FYM + STY*DGTEST
            DGX = DGXM + DGTEST
            DGY = DGYM + DGTEST
         ELSE
            CALL VD05BD(STX,FX,DGX,STY,FY,DGY,STP,F,DG,
     *                 BRACKT,STMIN,STMAX,INFOC)
            END IF
         IF (BRACKT) THEN
            IF (ABS(STY-STX) .GE. P66*WIDTH1)
     *         STP = STX + P5*(STY - STX)
            WIDTH1 = WIDTH
            WIDTH = ABS(STY-STX)
            END IF
         GO TO 30
      END
      SUBROUTINE VD05BD(STX,FX,DX,STY,FY,DY,STP,FP,DP,BRACKT,
     *                 STPMIN,STPMAX,INFO)
      INTEGER INFO
      DOUBLE PRECISION STX,FX,DX,STY,FY,DY,STP,FP,DP,STPMIN,STPMAX
      LOGICAL BRACKT,BOUND
      DOUBLE PRECISION GAMMA,P,Q,R,S,SGND,STPC,STPF,STPQ,THETA
      INFO = 0
      IF ((BRACKT .AND. (STP .LE. MIN(STX,STY) .OR.
     *    STP .GE. MAX(STX,STY))) .OR.
     *    DX*(STP-STX) .GE. 0.0 .OR. STPMAX .LT. STPMIN) RETURN
      SGND = DP*(DX/ABS(DX))
      IF (FP .GT. FX) THEN
         INFO = 1
         BOUND = .TRUE.
         THETA = 3*(FX - FP)/(STP - STX) + DX + DP
         S = MAX(ABS(THETA),ABS(DX),ABS(DP))
         GAMMA = S*SQRT((THETA/S)**2 - (DX/S)*(DP/S))
         IF (STP .LT. STX) GAMMA = -GAMMA
         P = (GAMMA - DX) + THETA
         Q = ((GAMMA - DX) + GAMMA) + DP
         R = P/Q
         STPC = STX + R*(STP - STX)
         STPQ = STX + ((DX/((FX-FP)/(STP-STX)+DX))/2)*(STP - STX)
         IF (ABS(STPC-STX) .LT. ABS(STPQ-STX)) THEN
            STPF = STPC
         ELSE
           STPF = STPC + (STPQ - STPC)/2
           END IF
         BRACKT = .TRUE.
      ELSE IF (SGND .LT. 0.0) THEN
         INFO = 2
         BOUND = .FALSE.
         THETA = 3*(FX - FP)/(STP - STX) + DX + DP
         S = MAX(ABS(THETA),ABS(DX),ABS(DP))
         GAMMA = S*SQRT((THETA/S)**2 - (DX/S)*(DP/S))
         IF (STP .GT. STX) GAMMA = -GAMMA
         P = (GAMMA - DP) + THETA
         Q = ((GAMMA - DP) + GAMMA) + DX
         R = P/Q
         STPC = STP + R*(STX - STP)
         STPQ = STP + (DP/(DP-DX))*(STX - STP)
         IF (ABS(STPC-STP) .GT. ABS(STPQ-STP)) THEN
            STPF = STPC
         ELSE
            STPF = STPQ
            END IF
         BRACKT = .TRUE.
      ELSE IF (ABS(DP) .LT. ABS(DX)) THEN
         INFO = 3
         BOUND = .TRUE.
         THETA = 3*(FX - FP)/(STP - STX) + DX + DP
         S = MAX(ABS(THETA),ABS(DX),ABS(DP))
         GAMMA = S*SQRT(MAX(0.0D0,(THETA/S)**2 - (DX/S)*(DP/S)))
         IF (STP .GT. STX) GAMMA = -GAMMA
         P = (GAMMA - DP) + THETA
         Q = (GAMMA + (DX - DP)) + GAMMA
         R = P/Q
         IF (R .LT. 0.0 .AND. GAMMA .NE. 0.0) THEN
            STPC = STP + R*(STX - STP)
         ELSE IF (STP .GT. STX) THEN
            STPC = STPMAX
         ELSE
            STPC = STPMIN
            END IF
         STPQ = STP + (DP/(DP-DX))*(STX - STP)
         IF (BRACKT) THEN
            IF (ABS(STP-STPC) .LT. ABS(STP-STPQ)) THEN
               STPF = STPC
            ELSE
               STPF = STPQ
               END IF
         ELSE
            IF (ABS(STP-STPC) .GT. ABS(STP-STPQ)) THEN
               STPF = STPC
            ELSE
               STPF = STPQ
               END IF
            END IF
      ELSE
         INFO = 4
         BOUND = .FALSE.
         IF (BRACKT) THEN
            THETA = 3*(FP - FY)/(STY - STP) + DY + DP
            S = MAX(ABS(THETA),ABS(DY),ABS(DP))
            GAMMA = S*SQRT((THETA/S)**2 - (DY/S)*(DP/S))
            IF (STP .GT. STY) GAMMA = -GAMMA
            P = (GAMMA - DP) + THETA
            Q = ((GAMMA - DP) + GAMMA) + DY
            R = P/Q
            STPC = STP + R*(STY - STP)
            STPF = STPC
         ELSE IF (STP .GT. STX) THEN
            STPF = STPMAX
         ELSE
            STPF = STPMIN
            END IF
         END IF
      IF (FP .GT. FX) THEN
         STY = STP
         FY = FP
         DY = DP
      ELSE
         IF (SGND .LT. 0.0) THEN
            STY = STX
            FY = FX
            DY = DX
            END IF
         STX = STP
         FX = FP
         DX = DP
         END IF
      STPF = MIN(STPMAX,STPF)
      STPF = MAX(STPMIN,STPF)
      STP = STPF
      IF (BRACKT .AND. BOUND) THEN
         IF (STY .GT. STX) THEN
            STP = MIN(STX+0.66*(STY-STX),STP)
         ELSE
            STP = MAX(STX+0.66*(STY-STX),STP)
            END IF
         END IF
      RETURN
      END
       DOUBLE PRECISION FUNCTION DDOT(N,D,I1,S,I2)
       DOUBLE PRECISION D(N),S(N),PROD
       INTEGER I1,I2
        PROD=0.0D0
        DO 10 I=1,N
 10     PROD= PROD+D(I)*S(I)
        DDOT= PROD
       RETURN
       END
