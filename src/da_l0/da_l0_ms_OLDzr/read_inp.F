#include "cppdefs.h"
                                        ! Read/report model input
      subroutine read_inp (ierr)        ! parameters  from  startup 
                                        ! script file using keywords
      implicit none                     ! to recognize variables.
#include "param.h"
#include "scalars.h"
#include "ncscrum.h"
#ifdef MPI
      include 'mpif.h'
#endif
      integer kwsize, testunit, input
      parameter (kwsize=32, testunit=40, input=15)
      character end_signal*3, keyword*32, fname*98
      parameter (end_signal='end')
      integer ierr, iargc, is,ie, kwlen, lstr, lenstr
#ifdef SOLVE3D
     &                                       , k, itrc
#endif
!
! Use pre-set default startup filename for known applications,
! or get it as an argument from command line via iargc-getarg
! (override default). NOTE: The usage of the executable should
! be either 
!           scrum
!        or
!           scrum startup_file_name
!
! WITHOUT the UNIX redirection (<): scrum<startup_file like it
! used to be.
!
#if defined SOLITON
      fname='scrum.in.Soliton'
#elif defined SEAMOUNT
      fname='scrum.in.Seamount'
#elif defined UPWELLING
      fname='scrum.in.Upwelling'
#elif defined USWEST
      fname='scrum.in.US_West'
#elif defined BASIN
      fname='scrum.in.BBB'
#elif defined DAMEE_B
      fname='scrum.in.Atl4'
#else
      fname='no_startup_file'
#endif
#ifdef MPI
      if (mynode.eq.0 .and. iargc().eq.2) call getarg(1,fname) 
      call MPI_Bcast(fname,64,MPI_BYTE, 0, MPI_COMM_WORLD,ierr)
#else
      if (iargc().eq.2) call getarg(1,fname)
#endif
      wrthis(indxTime)=.false.
#ifdef AVERAGES
      wrtavg(indxTime)=.false.
#endif
!
! Read in keyword: keep trying, until keyword is found.
! ==== == ======== ==== ======= ===== ======= == ======
!
      ierr=0    ! <-- reset error counrter
      call setup_kwds (ierr)
      open (input,file=fname,status='old',form='formatted',err=97)
   1   keyword='                                '
       read(input,'(A)',err=1,end=99) keyword
       if (ichar(keyword(1:1)).eq.33) goto 1
       is=1
   2   if (is.eq.kwsize) then
         goto 1
       elseif (keyword(is:is).eq.' ') then
         is=is+1
         goto 2
       endif
       ie=is
   3   if (keyword(ie:ie).eq.':') then
         keyword(ie:ie)=' '
         goto 4           !--> recognized keyword.               
       elseif (keyword(ie:ie).ne.' ' .and. ie.lt.kwsize) then 
         ie=ie+1
         goto 3
       endif
       goto 1
   4   kwlen=ie-is
       if (is.gt.1) keyword(1:kwlen)=keyword(is:is+kwlen-1)
!
! Read input parameters according to the keyword:
! ==== ===== ========== ========= == === ========
!
! Title
!
        if (keyword(1:kwlen).eq.'title') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,'(A)',err=95) title
          lstr=lenstr(title)
          MPI_master_only write(stdout,'(/1x,A)') title(1:lstr)
!
! Time-stepping parameters
!
        elseif (keyword(1:kwlen).eq.'time_stepping') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) ntimes,dt,ndtfast, ninfo
          MPI_master_only write(stdout,
     &   '(I10,2x,A,1x,A /F10.2,2x,A,2(/I10,2x,A,1x,A)/F10.4,2x,A)'
     &    ) ntimes,  'ntimes   Total number of timesteps for',
     &                                              '3D equations.',
     &      dt,      'dt       Timestep [sec] for 3D equations',
     &      ndtfast, 'ndtfast  Number of 2D timesteps within each',
     &                                                   '3D step.',
     &      ninfo,   'ninfo    Number of timesteps between',
     &                                       'runtime diagnostics.'
          dtfast=dt/float(ndtfast)     ! set barotropic time step.

#ifdef SOLVE3D
!
! Vertical S-coordinates parameters.
!
        elseif (keyword(1:kwlen).eq.'S-coord') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) theta_s, theta_b, Tcline
          MPI_master_only write(stdout,
     &                          '(3(1pe10.3,2x,A,1x,A/),32x,A)')
     &      theta_s, 'theta_s  S-coordinate surface control',
     &                                                 'parameter.',
     &      theta_b, 'theta_b  S-coordinate bottom control',
     &                                                 'parameter.',
     &      Tcline,  'Tcline   S-coordinate surface/bottom layer',
     &    'width used in', 'vertical coordinate stretching, meters.'
#endif
!
! Initial conditions file name. Check its availability (in the case
! of analytical initial conditions and nrrec=0 initial conditions are
! created internally and no file is needed).
!
        elseif (keyword(1:kwlen).eq.'initial') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) nrrec
#ifdef ANA_INITIAL
          if (nrrec.gt.0) then 
#endif
            read(input,'(A)',err=95) fname
            lstr=lenstr(fname)
#if defined MPI && defined PARALLEL_FILES
            call insert_node (fname, lstr, mynode, NNODES, ierr)
#endif
            open (testunit, file=fname(1:lstr), status='old', err=97)
            close(testunit)
            ininame=fname(1:lstr)
            MPI_master_only write(stdout,'(1x,A,2x,A,4x,A,I3)')
     &       'Initial State File:', ininame(1:lstr), 'Record:',nrrec
#ifdef ANA_INITIAL
          endif
#endif
#ifndef ANA_GRID
!
! Grid file name. Check its availability.
!
        elseif (keyword(1:kwlen).eq.'grid') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,'(A)',err=95) fname 
          lstr=lenstr(fname)
# if defined MPI && defined PARALLEL_FILES
          call insert_node (fname, lstr, mynode, NNODES, ierr)
# endif
          open(testunit,file=fname(1:lstr), status='old', err=97)
          close(testunit)
          grdname=fname(1:lstr)
          MPI_master_only write(stdout,'(10x,A,2x,A)')
     &                     'Grid File:', grdname(1:lstr)
#endif
#if !defined ANA_SMFLUX || defined SOLVE3D && (\
    !defined ANA_STFLUX || !defined ANA_BTFLUX  \
  ||(defined SG_BBL96    && !defined ANA_BSEDIM) \
  ||(defined SG_BBL96    && !defined ANA_WWAVE)  \
  ||(defined QCORRECTION && !defined ANA_SST)    \
  ||(defined SALINITY    && !defined ANA_SSFLUX) \
  ||(defined LMD_SKPP     && !defined ANA_SRFLUX))

!
! Forcing file name. Check its availability.
!
        elseif (keyword(1:kwlen).eq.'forcing') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,'(A)',err=95) fname
          lstr=lenstr(fname)
# if defined MPI && defined PARALLEL_FILES
          call insert_node (fname, lstr, mynode, NNODES, ierr)
# endif
          open (testunit, file=fname(1:lstr), status='old', err=97)
          close(testunit)
          frcname=fname(1:lstr)
          MPI_master_only write(stdout,'(2x,A,2x,A)')
     &               'Forcing Data File:', frcname(1:lstr)
#endif
#if (defined TCLIMATOLOGY && !defined ANA_TCLIMA) || \
    (defined ZNUDGING && !defined ANA_SSH)
!
! Climatology file name. Check availability.
!
        elseif (keyword(1:kwlen).eq.'climatology') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,'(A)',err=95) fname 
          lstr=lenstr(fname)
# if defined MPI && defined PARALLEL_FILES
          call insert_node (fname, lstr, mynode, NNODES, ierr)
# endif
!LI          open (testunit, file=fname(1:lstr), status='old', err=97)
!LI          close(testunit)
          clmname=fname(1:lstr)
          MPI_master_only write(stdout,'(3x,A,2x,A)')
     &              'Climatology File:', clmname(1:lstr)
#endif
!
! Restart file name.
!
        elseif (keyword(1:kwlen).eq.'restart') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) nrst, nrpfrst
          read(input,'(A)',err=95)  fname 
          lstr=lenstr(fname)
# if defined MPI && defined PARALLEL_FILES
          call insert_node (fname, lstr, mynode, NNODES, ierr)
# endif
          rstname=fname(1:lstr)
          MPI_master_only write(stdout,
     &               '(7x,A,2x,A,4x,A,I6,4x,A,I4)')
     &               'Restart File:', rstname(1:lstr),
     &               'nrst =', nrst, 'rec/file: ', nrpfrst 
!
! History file name.
!
        elseif (keyword(1:kwlen).eq.'history') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) ldefhis, nwrt, nrpfhis
          read(input,'(A)',err=95) fname
          lstr=lenstr(fname)
# if defined MPI && defined PARALLEL_FILES
          call insert_node (fname, lstr, mynode, NNODES, ierr)
# endif
          hisname=fname(1:lstr)
          MPI_master_only write(stdout,
     &               '(7x,A,2x,A,2x,A,1x,L1,2x,A,I4,2x,A,I3)')
     &         'History File:', hisname(1:lstr),  'Create new:',
     &         ldefhis, 'nwrt =', nwrt, 'rec/file =', nrpfhis
#ifdef AVERAGES
!
! Averages file name.
!
        elseif (keyword(1:kwlen).eq.'averages') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) ntsavg, navg, nrpfavg
          read(input,'(A)',err=95) fname
          lstr=lenstr(fname)
# if defined MPI && defined PARALLEL_FILES
          call insert_node (fname, lstr, mynode, NNODES, ierr)
# endif
          avgname=fname(1:lstr)
          MPI_master_only write(stdout,
     &           '(2(I10,2x,A,1x,A/32x,A/),6x,A,2x,A,1x,A,I3)')
     &      ntsavg, 'ntsavg      Starting timestep for the',
     &           'accumulation of output', 'time-averaged data.',
     &      navg,   'navg        Number of timesteps between',
     &     'writing of time-averaged','data into averages file.',
     &     'Averages File:', avgname(1:lstr),
     &     'rec/file =', nrpfavg
#endif
#ifdef STATIONS
!
! Stations file name.
!
        elseif (keyword(1:kwlen).eq.'stations') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) nsta
          read(input,'(A)',err=95) sposnam
          read(input,'(A)',err=95) staname

          fname=sposnam
          lstr=lenstr(fname)
          open (testunit,file=fname(1:lstr),status='old',err=97)
          close(testunit)
          MPI_master_only write(stdout,
     &       '(I10,2x,A,1x,A/32x,A/1x,A,2x,A)')
     &         nsta,  'nsta        Number of timesteps',
     &        'between writing of data into',   'stations file.',
     &        'Station Positions File:',   sposnam(1:lstr)
          lstr=lenstr(staname)
          MPI_master_only write(stdout,'(2x,A,2x,A)')
     &                  'Stations History File:', staname(1:lstr)
#endif
#ifdef FLOATS
!
! Floats file name.
!
        elseif (keyword(1:kwlen).eq.'floats') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,'(A)',err=95) fposnam
          read(input,'(A)',err=95) fltname
          fname=fposnam
          lstr=lenstr(fposnam)
          open (testunit,file=fposnam(1:lstr),status='old',err=97)
          close(testunit)
          MPI_master_only write(stdout,'(1x,A,2x,A)')
     &           'Floats Initial Positions File:', fposnam(1:lstr)
          lstr=lenstr(fltname)
          MPI_master_only write(stdout,'(11x,A,2x,A)')
     &                     'Floats History File:', fltname(1:lstr)
#endif
#ifdef ASSIMILATION
!
! Assimilation input/output file names.
!
        elseif (keyword(1:kwlen).eq.'assimilation') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,'(A)',err=95) aparnam
          read(input,'(A)',err=95) assname
          fname=aparnam
          lstr=lenstr(aparnam)
          open (testunit,file=aparnam(1:lstr),status='old',err=97)
          close(testunit)
          MPI_master_only write(stdout,'(1x,A,2x,A)')
     &           'Assimilation Parameters File:', aparnam(1:lstr)
          fname=assname
          lstr=lenstr(assname)
          open (testunit,file=assname(1:lstr),status='old',err=97)
          close(testunit)
          MPI_master_only write(stdout,'(12x,A,2x,A)')
     &                      'Assimilation File:', assname(1:lstr)
#endif
!
! Switches for fields to be saved into history file.
!
        elseif (keyword(1:kwlen).eq.'primary_history_fields') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) wrthis(indxZ),  wrthis(indxUb)
     &                                         ,  wrthis(indxVb)
#ifdef SOLVE3D
     &                      ,  wrthis(indxU),  wrthis(indxV)
     &                      , (wrthis(itrc), itrc=indxT,indxT+NT-1) 
#endif
          if ( wrthis(indxZ) .or. wrthis(indxUb) .or. wrthis(indxVb)
#ifdef SOLVE3D
     &                          .or. wrthis(indxU) .or. wrthis(indxV)
#endif
     &       ) wrthis(indxTime)=.true.

          MPI_master_only write(stdout,'(/1x,A,5(/6x,l1,2x,A,1x,A))')
     &    'Fields to be saved in history file: (T/F)'
     &    , wrthis(indxZ),  'write zeta ', 'free-surface.'
     &    , wrthis(indxUb), 'write UBAR ', '2D U-momentum component.'
     &    , wrthis(indxVb), 'write VBAR ', '2D V-momentum component.'
#ifdef SOLVE3D
     &    , wrthis(indxU),  'write U    ', '3D U-momentum component.'
     &    , wrthis(indxU),  'write U    ', '3D U-momentum component.'
          do itrc=1,NT
            if (wrthis(indxT+itrc-1)) wrthis(indxTime)=.true.
            MPI_master_only write(stdout, '(6x,L1,2x,A,I1,A,I2,A)')
     &                       wrthis(indxT+itrc-1), 'write T(', itrc,
     &                                ')  Tracer of index', itrc,'.'
          enddo

        elseif (keyword(1:kwlen).eq.'auxiliary_history_fields') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) wrthis(indxR), wrthis(indxO)
     &          ,  wrthis(indxW),  wrthis(indxAkv),  wrthis(indxAkt)
# ifdef SALINITY
     &                                            ,  wrthis(indxAks)
#  ifdef LMD_SKPP
     &                                            ,  wrthis(indxHbl)
#  endif
# endif
          if ( wrthis(indxR) .or. wrthis(indxO) .or. wrthis(indxW)
     &                     .or. wrthis(indxAkv) .or. wrthis(indxAkt)
# ifdef SALINITY
     &                                          .or. wrthis(indxAks)
#  ifdef LMD_SKPP
     &                                          .or. wrthis(indxHbl)
#  endif
# endif
     &       ) wrthis(indxTime)=.true.

          MPI_master_only write(stdout,'(7(/6x,l1,2x,A,1x,A))')
     &      wrthis(indxR),  'write RHO  ', 'Density anomaly'
     &    , wrthis(indxO),  'write Omega', 'Omega vertical velocity.'
     &    , wrthis(indxW),  'write W    ', 'True vertical velocity.'
     &    , wrthis(indxAkv),'write Akv  ', 'Vertical viscosity'
     &    , wrthis(indxAkt),'write Akt  ',
     &                        'Vertical diffusivity for temperature.'
# ifdef SALINITY
     &    , wrthis(indxAks),'write Aks  ',
     &                           'Vertical diffusivity for salinity.'
#  ifdef LMD_SKPP
     &    , wrthis(indxHbl),'write Hbl  ',
     &                            'Depth of KPP-model boundary layer'
#  endif
# endif
#endif /* SOLVE3D */
#ifdef AVERAGES
!
! Switches for fields to be saved into averages file.
!
        elseif (keyword(1:kwlen).eq.'primary_averages') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) wrtavg(indxZ),  wrtavg(indxUb)
     &                                         ,  wrtavg(indxVb)
# ifdef SOLVE3D
     &                      ,  wrtavg(indxU),  wrtavg(indxV)
     &                      , (wrtavg(itrc), itrc=indxT,indxT+NT-1)
# endif
          if ( wrtavg(indxZ) .or. wrtavg(indxUb) .or. wrtavg(indxVb)
# ifdef SOLVE3D
     &                          .or. wrtavg(indxU) .or. wrtavg(indxV)
# endif
     &       ) wrtavg(indxTime)=.true.

          MPI_master_only write(stdout,'(/1x,A,5(/6x,l1,2x,A,1x,A))')
     &    'Fields to be saved in averages file: (T/F)'
     &    , wrtavg(indxZ),  'write zeta ', 'free-surface.'
     &    , wrtavg(indxUb), 'write UBAR ', '2D U-momentum component.'
     &    , wrtavg(indxVb), 'write VBAR ', '2D V-momentum component.'
# ifdef SOLVE3D
     &    , wrtavg(indxU),  'write U    ', '3D U-momentum component.'
     &    , wrtavg(indxU),  'write U    ', '3D U-momentum component.'
          do itrc=1,NT
            if (wrtavg(indxT+itrc-1)) wrtavg(indxTime)=.true.
            MPI_master_only write(stdout,
     &                       '(6x,L1,2x,A,I1,A,2x,A,I2,A)')
     &                        wrtavg(indxT+itrc-1), 'write T(',
     &                        itrc,')', 'Tracer of index', itrc,'.'
          enddo

        elseif (keyword(1:kwlen).eq.'auxiliary_averages') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) wrtavg(indxR), wrtavg(indxO)
     &          ,  wrtavg(indxW),  wrtavg(indxAkv),  wrtavg(indxAkt)
#  ifdef SALINITY
     &                                            ,  wrtavg(indxAks)
#   ifdef LMD_SKPP
     &                                            ,  wrtavg(indxHbl)
#   endif
#  endif
          if ( wrtavg(indxR) .or. wrtavg(indxO) .or. wrtavg(indxW)
     &                     .or. wrtavg(indxAkv) .or. wrtavg(indxAkt)
#  ifdef SALINITY
     &                                          .or. wrtavg(indxAks)
#   ifdef LMD_SKPP
     &                                          .or. wrtavg(indxHbl)
#   endif
#  endif
     &       ) wrtavg(indxTime)=.true.

          MPI_master_only write(stdout,'(7(/6x,l1,2x,A,1x,A))')
     &      wrtavg(indxR),  'write RHO  ', 'Density anomaly'
     &    , wrtavg(indxO),  'write Omega', 'Omega vertical velocity.'
     &    , wrtavg(indxW),  'write W    ', 'True vertical velocity.'
     &    , wrtavg(indxAkv),'write Akv  ', 'Vertical viscosity'
     &    , wrtavg(indxAkt),'write Akt  ',
     &                        'Vertical diffusivity for temperature.'
#  ifdef SALINITY
     &    , wrtavg(indxAks),'write Aks  ',
     &                           'Vertical diffusivity for salinity.'
#   ifdef LMD_SKPP
     &    , wrtavg(indxHbl),'write Hbl  ',
     &                            'Depth of KPP-model boundary layer'
#   endif
#  endif
# endif /* SOLVE3D */
#endif /* AVERAGES */
!
! Boussinesque Approximation mean density.
!
        elseif (keyword(1:kwlen).eq.'rho0') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) rho0
          MPI_master_only write(stdout,'(F10.4,2x,A,1x,A)')
     &          rho0, 'rho0     Boussinesque approximation',
     &                                'mean density, kg/m3.'
#if defined UV_VIS2 || defined UV_VIS4
!
! Horizontal viscosity coefficients.
!
        elseif (keyword(1:kwlen).eq.'lateral_visc') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) visc2, visc4
#endif
#ifdef UV_VIS2
          MPI_master_only write(stdout,9) visc2
   9      format(1pe10.3,2x,'visc2       Horizontal Laplacian ',
     &         'mixing coefficient [m2/s]',/,32x,'for momentum.')
#endif
#ifdef UV_VIS4
          MPI_master_only write(stdout,10) visc4
  10      format(1pe10.3,2x,'visc4       Horizontal biharmonic ',
     &         'mixing coefficient [m4/s]',/,32x,'for momentum.')
#endif
!
! Bottom drag coefficients.
!
        elseif (keyword(1:kwlen).eq.'bottom_drag') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) rdrg, rdrg2
          MPI_master_only write(stdout,'((1pe10.3,2x,A))')
     &      rdrg, 'rdrg     Linear bottom drag coefficient, m/s.',
     &      rdrg2,'rdrg2    Quadratic bottom drag coefficient.'
!
! Lateral boundary slipperness.
!
        elseif (keyword(1:kwlen).eq.'gamma2') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) gamma2
          MPI_master_only write(stdout,'(f10.2,2x,A,1x,A)')
     &       gamma2, 'gamma2   Slipperiness parameter:',
     &                       'free-slip +1, or no-slip -1.'
#ifdef SOLVE3D
# ifdef TS_DIF2
!
! Horizontal Laplacian mixing coefficients for tracers.
!
        elseif (keyword(1:kwlen).eq.'tracer_diff2') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) (tnu2(itrc),itrc=1,NT)
          do itrc=1,NT
            MPI_master_only write(stdout,7) tnu2(itrc), itrc, itrc
   7        format(1pe10.3,'  tnu2(',i1,')     Horizontal Laplacian '
     &       ,'mixing coefficient (m2/s)',/,32x,'for tracer ',i1,'.')
          enddo
# endif
# ifdef TS_DIF4
!
! Horizontal biharmonic mixing coefficients for tracer.
!
        elseif (keyword(1:kwlen).eq.'tracer_diff4') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) (tnu4(itrc),itrc=1,NT)
          do itrc=1,NT
            MPI_master_only write(stdout,8) tnu4(itrc), itrc, itrc
   8        format(1pe10.3,'  tnu4(',i1,')     Horizontal biharmonic'
     &      ,' mixing coefficient [m4/s]',/,32x,'for tracer ',i1,'.')
          enddo

# endif
# if !defined LMD_MIXING && !defined BVF_MIXING\
  && !defined MY2_MIXING && !defined MY25_MIXING\
                         && !defined PP_MIXING
!
! Background vertical viscosity and mixing coefficients for tracers.
!
        elseif (keyword(1:kwlen).eq.'vertical_mixing') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) Akv_bak,(Akt_bak(itrc),itrc=1,NT)
          MPI_master_only write(stdout,'(1pe10.3,2x,A,1x,A)')
     &        Akv_bak, 'Akv_bak    Background vertical viscosity',
     &                                       'coefficient, m2/s.'
          do itrc=1,NT
            MPI_master_only write(stdout,
     &             '(1pe10.3,2x,A,I1,A,1x,A/32x,A,I3,A)')
     &              Akt_bak(itrc), 'Akt_bak(', itrc, ')',
     &             'Background vertical mixing coefficient, m2/s,',
     &                                    'for tracer ', itrc, '.' 
          enddo
# endif
# ifdef MY25_MIXING
!
! Mellor-Yamada Level 2.5 turbulent closure parameters.
!
        elseif (keyword(1:kwlen).eq.'MY_bak_mixing') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) Akq_bak, q2nu2, q2nu4
          MPI_master_only write(stdout,13) Akq_bak
  13      format(1pe10.3,2x,'Akq_bak     Background vertical mixing',
     &          ' coefficient [m2/s]',/,32x,'for turbulent energy.')
#  ifdef Q_DIF2
          MPI_master_only write(stdout,14) q2nu2
  14      format(1pe10.3,2x,'q2nu2       Horizontal Laplacian ',
     &    'mixing coefficient [m2/s]',/,32x,'for turbulent energy.')
#  endif
#  ifdef Q_DIF4
          MPI_master_only write(stdout,15) q2nu4
  15      format(1pe10.3,2x,'q2nu4       Horizontal, biharmonic ',
     &    'mixing coefficient, m2/s,',/,32x,'for turbulent energy.')
#  endif
# endif
# ifdef BODYFORCE
        elseif (keyword(1:kwlen).eq.'bodyforce') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) levsfrc,levbfrc
          if (levsfrc.lt.1 .or. levsfrc.gt.N) then
            MPI_master_only write(stdout,19) 'LEVSFRC = ',levsfrc
  19        format(' READ_INP - Illegal bodyforce level, ',A,i4)
            ierr=ierr+1
          endif
          if (levbfrc.lt.1 .or. levbfrc.gt.N) then
            MPI_master_only write(stdout,19) 'LEVBFRC = ',levbfrc
            ierr=ierr+1
          endif
          MPI_master_only write(stdout,20) levsfrc, levbfrc
  20      format(4x,i6,2x,'levsfrc     ',
     &             'Deepest level to apply surface stress as a ',
     &             'bodyfroce.',/,
     &           4x,i6,2x,'levbfrc     ',
     &             'Shallowest level to apply bottom stress as a ',
     &             'bodyfroce.')
# endif
# ifdef SPONGE
!
! Parameters for the sponge layers
!
        elseif (keyword(1:kwlen).eq.'sponge') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) x_sponge, v_sponge
          MPI_master_only write(stdout,'(1pe10.2,2x,A)')
     &       x_sponge,'x_sponge Thickness of sponge layer (m)'
          MPI_master_only write(stdout,'(f10.2,2x,A)')
     &       v_sponge,'v_sponge Viscosity in sponge layer (m2/s)'
# endif
# ifdef TNUDGING
!
! Parameters for OBC nudging and nudging layers 
! (converted from [days] to [sec^-1]
!
        elseif (keyword(1:kwlen).eq.'nudg_cof') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) TauT_in,TauT_out,TauM_in,TauM_out
          TauT_in =1./(TauT_in *86400.)
          TauT_out=1./(TauT_out*86400.)
          TauM_in =1./(TauM_in *86400.)
          TauM_out=1./(TauM_out*86400.)
          MPI_master_only write(stdout,'(1pe10.3,2x,A)') 
     &       TauT_in,'TauT_in  Nudging coefficients [sec^-1]'
          MPI_master_only write(stdout,'(1pe10.3,2x,A)') 
     &       TauT_out,'TauT_out  Nudging coefficients [sec^-1]'
          MPI_master_only write(stdout,'(1pe10.3,2x,A)') 
     &       TauM_in,'TauM_in  Nudging coefficients [sec^-1]'
          MPI_master_only write(stdout,'(1pe10.3,2x,A)') 
     &       TauM_out,'TauM_out  Nudging coefficients [sec^-1]'
# endif

# ifndef NONLIN_EOS
!
! Parameters for linear equations of state.
!
        elseif (keyword(1:kwlen).eq.'lin_EOS_cff') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) R0, T0, S0, Tcoef, Scoef
          MPI_master_only write(stdout,'(5(f10.4,2x,A,1x,A/))')
     &       T0, 'T0       Background value for potential',
     &                                     'temperature (Celsius).',
     &       S0, 'S0       Background salinity (PSU),', 'constant.',
     &       R0, 'R0       Background density (kg/m3) used in',
     &                                                'linear EOS.',
     &    Tcoef, 'Tcoef    Thermal expansion coefficient',
     &                                               '(1/Celsius).',
     &    Scoef, 'Scoef    Saline contraction coefficient (1/PSU).'
# endif
#endif

        else
          MPI_master_only write(stdout,'(/3(1x,A)/)')
     &                    'WARNING: Urecognized keyword:',
     &                     keyword(1:kwlen),' --> DISREGARDED.'
        endif
       if (keyword(1:kwlen) .eq. end_signal) goto 99
      goto 1
!
! Error while reading input parameters.
!
  95  write(stdout,'(/1x,4A/)') 'READ_INP ERROR while reading block',
     &                    ' with keyword ''', keyword(1:kwlen), '''.' 
      ierr=ierr+1
      goto 99 
  97  write(stdout,'(/1x,4A/)') 'READ_INP ERROR: Cannot find input ',
     &                                'file ''', fname(1:lstr), '''.'
      ierr=ierr+1
  99  close (input) 
!
! Check that all keywords were canceled, complain about the error,
! if some of them left.
!
      if (ierr.eq.0) then
        call check_kwds (ierr)

#ifdef STATIONS
!
! Read station positions.
!
        call sta_inp (ierr)
#endif

!
! Check CPP-switches for consistency. This operation is split into
! two stages because the first subroutine, "check_switches1", is
! generated by special program cppcheck (file cppcheck.F) by
! examining and documention all available switches in cppdefs.h.
! This subroutine creates log of all switches defined in "cppdefs.h",
! as well as traps multiply defined global configurations (project
! switches, such as DAMEE_B, USWEST, etc).
! The second routine, "check_switches2" is hand written and it
! contains traps for mutually exclussive definition of all other
! CPP-switches (i.e. those which are NOT project selection switches,
! for example, it traps multiply defined vertical mixing schemes or
! lateral boundary conditions).
!
! Both codes are written in transparent mode: they assumed that error
! variable (ierr) is initialized at entry and they add 1 for each
! error discovered. 
!
        call check_srcs
        call check_switches1 (ierr)
        call check_switches2 (ierr)
      endif
      if (ierr.ne.0) then
        write(stdout,'(/1x,2A,I3,1x,A/)') 'READ_INP ERROR: ',
     & 'A total of', ierr, 'configuration errors discovered.'
       return
      endif
#ifdef MPI
      call MPI_Barrier (MPI_COMM_WORLD, ierr)
#endif
      return
      end

c
