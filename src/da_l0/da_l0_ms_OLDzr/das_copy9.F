#include "cppdefs.h"

      subroutine das_copy9 (tile)
      implicit none
      integer tile
#include "param.h"
#include "das_param.h"
#include "compute_tile_bounds.h"
      call das_copy9_tile (Istr,Iend,Jstr,Jend)
      return
      end

      subroutine das_copy9_tile (Istr,Iend,Jstr,Jend)
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k
#include "param.h"
#include "das_param.h"
#include "scalars.h"
#include "das_ocean.h"
#include "das_ocean9.h"
!
# include "compute_extended_bounds.h"
!
! 2D
!
        do j=JstrR,JendR
          do i=IstrR,IendR
            zeta_das9(i,j)=zeta_das(i,j)
          enddo
        enddo
!
!  Initialize 3-D primitive variables.
!
        do k=1,NDAS
          do j=JstrR,JendR
            do i=IstrR,IendR
              t_das9(i,j,k,itemp)=t_das(i,j,k,itemp)
            enddo
          enddo
        enddo
        do k=1,NDAS
          do j=JstrR,JendR
            do i=IstrR,IendR
              t_das9(i,j,k,isalt)=t_das(i,j,k,isalt)
            enddo
          enddo
        enddo
      return
      end
