      subroutine das_filter (tile)
      implicit none
      integer tile
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=384,  MMm=390,  N=66
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp
     &          , isalt
       parameter (itemp=1,
     &            isalt=2,
     &            NT=2
     &           )
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer chunk_size_X,margin_X,chunk_size_E,margin_E
      parameter (chunk_size_X=(Lm+NSUB_X-1)/NSUB_X,
     &                     margin_X=(NSUB_X*chunk_size_X-Lm)/2,
     &           chunk_size_E=(Mm+NSUB_E-1)/NSUB_E,
     &                     margin_E=(NSUB_E*chunk_size_E-Mm)/2)
      integer Istr,Iend,Jstr,Jend, i_X,j_E
        j_E=tile/NSUB_X
        i_X=tile-j_E*NSUB_X
        Istr=1+i_X*chunk_size_X-margin_X
        Iend=Istr+chunk_size_X-1
        Istr=max(Istr,1)
        Iend=min(Iend,Lm)
        Jstr=1+j_E*chunk_size_E-margin_E
        Jend=Jstr+chunk_size_E-1
        Jstr=max(Jstr,1)
        Jend=min(Jend,Mm)
      call das_filter_tile (Istr,Iend,Jstr,Jend)
      return
      end
      subroutine das_filter_tile (Istr,Iend,Jstr,Jend)
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k
      integer is,ie,js,je,i0,j0
      real rr,rc,ro, ppm, Lcorr
      integer count,num,n0
      integer srad,sdim
      parameter (srad=3, sdim=(2*srad+1)*(2*srad+1))
      real tt(sdim),ss(sdim),dis(sdim)
      real su(sdim),sv(sdim),zz(sdim)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=384,  MMm=390,  N=66
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp
     &          , isalt
       parameter (itemp=1,
     &            isalt=2,
     &            NT=2
     &           )
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_sio, max_whoi, max_flight, max_prf
      PARAMETER(max_sio=64,max_whoi=192,max_flight=1500,
     &         max_prf=NDAS)
      integer max_moor
      PARAMETER(max_moor=68)
      real sio_ot,sio_os,whoi_ot,whoi_os,moor_ot,moor_os
      PARAMETER( sio_ot=1.0/(1.5*1.5),sio_os=1.0/(0.3*0.3),
     &           whoi_ot=1.0/(1.5*1.5),whoi_os=1.0/(0.3*0.3),
     &           moor_ot=1.0/(1.8*1.8),moor_os=1.0/(0.25*0.25)
     &         )
      real regp
      PARAMETER( regp=1./(10.*10.) )
      integer nratio
      PARAMETER( nratio=3 )
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      real h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hinv(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real f(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fomn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_h/h /grid_hinv/hinv /grid_f/f /grid_fomn/fomn
      real angler(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_angler/angler
      real latr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real lonr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_latr/latr /grid_lonr/lonr
      real pm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_pm/pm    /metrics_pn/pn
     &       /metrics_omu/om_u /metrics_on_u/on_u
     &       /metrics_omv/om_v /metrics_on_v/on_v
      real dmde(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real dndx(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_dmde/dmde    /metrics_dndx/dndx
      real on_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmon_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pnom_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real grdscl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_on_p/on_p /metrics_om_p/om_p
     &       /metrics_on_r/on_r /metrics_om_r/om_r
     &       /metrics_pmon_u/pmon_u /metrics_pnom_v/pnom_v
     &                              /metrics_grdscl/grdscl
      real rmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /mask_r/rmask /mask_p/pmask
     &       /mask_u/umask /mask_v/vmask
      real zeta_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_das/u_das /ocean_v_das/v_das
     &     /ocean_t_das/t_das /ocean_zeta_das/zeta_das
      real zeta_s(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s/u_s /ocean_v_s/v_s
     &      /ocean_rho_s/rho_s /ocean_p_s/p_s
     &     /ocean_t_s/t_s /ocean_zeta_s/zeta_s
      real rmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real umask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real vmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /mask_r_das/rmask_das
     &       /mask_u_das/umask_das /mask_v_das/vmask_das
      integer nzr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nzr_das/nzr_das
     &       /ocean_nzu_das/nzu_das /ocean_nzv_das/nzv_das
      integer nsr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nsr_das/nsr_das
     &       /ocean_nsu_das/nsu_das /ocean_nsv_das/nsv_das
      real z_das(ndas),tc1d_das(ndas),sc1d_das(ndas)
      common /grid_z_das/z_das,tc1d_das,sc1d_das
      real bz_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real bu_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real bv_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real bt_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common
     &     /ocean_bu_das/bu_das /ocean_bv_das/bv_das
     &     /ocean_bt_das/bt_das /ocean_bz_das/bz_das
      real czE_das(0:Mm+1+padd_E,0:Mm+1+padd_E)
      real czX_das(0:Lm+1+padd_X,0:Lm+1+padd_X)
      real cuE_das(0:Mm+1+padd_E,0:Mm+1+padd_E)
      real cuX_das(0:Lm+1+padd_X,0:Lm+1+padd_X)
      real cuZ_das(NDAS,NDAS)
      real cvE_das(0:Mm+1+padd_E,0:Mm+1+padd_E)
      real cvX_das(0:Lm+1+padd_X,0:Lm+1+padd_X)
      real cvZ_das(NDAS,NDAS)
      real ctE_das(0:Mm+1+padd_E,0:Mm+1+padd_E,NT)
      real ctX_das(0:Lm+1+padd_X,0:Lm+1+padd_X,NT)
      real ctZ_das(ndas,ndas,NT)
      common /ocean_cze_das/czE_das /ocean_czX_das/czX_das
     &       /ocean_cue_das/cuE_das /ocean_cuX_das/cuX_das
     &       /ocean_cuz_das/cuZ_das
     &       /ocean_cve_das/cvE_das /ocean_cvX_das/cvX_das
     &       /ocean_cvz_das/cvZ_das
     &       /ocean_cte_das/ctE_das /ocean_ctX_das/ctX_das
     &       /ocean_ctz_das/ctZ_das
      character*118 file_corr
      common /corr_file/file_corr
      character*118 file_corr_vert
      common /corr_file_vert/file_corr_vert
      character*118 file_bvar
      common /bvar_file_vert/file_bvar
      integer IstrR,IendR,JstrR,JendR
      integer IstrU
      integer JstrV
      if (Istr.eq.1) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (Iend.eq.Lm) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (Jend.eq.Mm) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      Lcorr=0.15
      ro = (Lcorr*Lcorr)* (
     &           (lonr(Lm/2,Mm/2)-lonr(Lm/2+1,Mm/2+1))
     &          *(lonr(Lm/2,Mm/2)-lonr(Lm/2+1,Mm/2+1))
     &          +(latr(Lm/2,Mm/2)-latr(Lm/2+1,Mm/2+1))
     &          *(latr(Lm/2,Mm/2)-latr(Lm/2+1,Mm/2+1)) )
      do k=1,NDAS
        do j=JstrR,JendR
          do i=IstrR,IendR
            if (rmask_das(i,j,NDAS) .gt. 0.5) then
              count=0
              js=max(j-srad, 0)
              je=min(j+srad, Mm+1)
              is=max(i-srad,0)
              ie=min(i+srad, Lm+1)
              do j0=js,je
                do i0=is,ie
                  if (rmask_das(i0,j0,k) .gt. 0.5) then
                    rr=      (lonr(i,j)-lonr(i0,j0))
     &                      *(lonr(i,j)-lonr(i0,j0))
     &                      +(latr(i,j)-latr(i0,j0))
     &                      *(latr(i,j)-latr(i0,j0))
                    count=count+1
                    tt(count)=t_das(i0,j0,k,itemp)
                    ss(count)=t_das(i0,j0,k,isalt)
                    dis(count)=exp(-rr/ro)
                  endif
                enddo
              enddo
              rc=0.0
              t_s(i,j,k,itemp)=0.0
              t_s(i,j,k,isalt)=0.0
              if ( count .gt. 0) then
                do n0=1,count
                  rc=rc+dis(n0)
                  t_s(i,j,k,itemp)=t_s(i,j,k,itemp)+tt(n0)*dis(n0)
                  t_s(i,j,k,isalt)=t_s(i,j,k,isalt)+ss(n0)*dis(n0)
                enddo
                t_s(i,j,k,itemp)=t_s(i,j,k,itemp)/rc
                t_s(i,j,k,isalt)=t_s(i,j,k,isalt)/rc
               else
                t_s(i,j,k,itemp)=0.0
                t_s(i,j,k,isalt)=0.0
              endif
            endif
          enddo
        enddo
        do j=JstrR,JendR
          do i=Istr,IendR
            if (umask_das(i,j,NDAS) .gt. 0.5) then
              count=0
              js=max(j-srad, 1)
              je=min(j+srad, Mm)
              is=max(i-srad,1)
              ie=min(i+srad, Lm)
              do j0=js,je
                do i0=is,ie
                  ppm=umask_das(i0,j0,k)
                  if (ppm .gt. 0.5) then
                    rr=      (lonr(i,j)-lonr(i0,j0))
     &                      *(lonr(i,j)-lonr(i0,j0))
     &                      +(latr(i,j)-latr(i0,j0))
     &                      *(latr(i,j)-latr(i0,j0))
                    count=count+1
                    su(count)=u_das(i0,j0,k)
                    dis(count)=exp(-rr/ro)
                  endif
                enddo
              enddo
              rc=0.0
              u_s(i,j,k)=0.0
              if ( count .gt. 0) then
                do n0=1,count
                  rc=rc+dis(n0)
                  u_s(i,j,k)=u_s(i,j,k)+su(n0)*dis(n0)
                enddo
                u_s(i,j,k)=u_s(i,j,k)/rc
              endif
            else
              u_s(i,j,k)=0.0
            endif
          enddo
        enddo
        do j=Jstr,JendR
          do i=IstrR,IendR
            if (vmask_das(i,j,NDAS) .gt. 0.5) then
              count=0
              js=max(j-srad, 1)
              je=min(j+srad, Mm)
              is=max(i-srad,1)
              ie=min(i+srad, Lm)
              do j0=js,je
                do i0=is,ie
                  ppm=vmask_das(i0,j0,k)
                  if ( ppm .gt. 0.5) then
                    rr=      (lonr(i,j)-lonr(i0,j0))
     &                      *(lonr(i,j)-lonr(i0,j0))
     &                      +(latr(i,j)-latr(i0,j0))
     &                      *(latr(i,j)-latr(i0,j0))
                    count=count+1
                    sv(count)=v_das(i0,j0,k)
                    dis(count)=exp(-rr/ro)
                  endif
                enddo
              enddo
              rc=0.0
              v_s(i,j,k)=0.0
              if ( count .gt. 0) then
                do n0=1,count
                  rc=rc+dis(n0)
                  v_s(i,j,k)=v_s(i,j,k)+sv(n0)*dis(n0)
                enddo
                v_s(i,j,k)=v_s(i,j,k)/rc
              endif
            else
              v_s(i,j,k)=0.0
            endif
          enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          if (rmask_das(i,j,NDAS) .gt. 0.5) then
            count=0
            js=max(j-srad, 0)
            je=min(j+srad, Mm+1)
            is=max(i-srad,0)
            ie=min(i+srad, Lm+1)
            do j0=js,je
              do i0=is,ie
                if (rmask_das(i0,j0,NDAS) .gt. 0.5) then
                  rr=      (lonr(i,j)-lonr(i0,j0))
     &                    *(lonr(i,j)-lonr(i0,j0))
     &                    +(latr(i,j)-latr(i0,j0))
     &                    *(latr(i,j)-latr(i0,j0))
                  count=count+1
                  zz(count)=zeta_das(i0,j0)
                  dis(count)=exp(-rr/ro)
                endif
              enddo
            enddo
            rc=0.0
            zeta_s(i,j)=0.0
            do n0=1,count
              rc=rc+dis(n0)
              zeta_s(i,j)=zeta_s(i,j)+zz(n0)*dis(n0)
            enddo
            zeta_s(i,j)=zeta_s(i,j)/rc
          else
            zeta_s(i,j)=0.0
          endif
        enddo
      enddo
      return
      end
