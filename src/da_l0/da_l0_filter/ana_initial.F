#include "cppdefs.h"
#ifdef ANA_INITIAL

      subroutine ana_initial (tile)
      implicit none
      integer tile
# include "param.h"
# include "compute_tile_bounds.h"
      call ana_initial_tile (Istr,Iend,Jstr,Jend)
      return
      end

      subroutine ana_initial_tile (Istr,Iend,Jstr,Jend)
!
!--------------------------------------------------------------------
!  Set initial conditions for momentum and tracer variables using
!  analytical expressions.
!--------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k, itrc
      real fac, x,y, x0,y0, cff1,cff2,cff3
#ifdef BIOLOGY
      real temp, SiO4
#endif
# include "param.h"
# include "grid.h"
# include "ocean2d.h"
# include "ocean3d.h"
# include "scalars.h"
!
# include "compute_auxiliary_bounds.h"
!
# ifdef EW_PERIODIC
#  define IR_RANGE Istr,Iend
#  define IU_RANGE Istr,Iend
# else
#  define IR_RANGE IstrR,IendR
#  define IU_RANGE  Istr,IendR
# endif

# ifdef NS_PERIODIC
#  define JR_RANGE Jstr,Jend
#  define JV_RANGE Jstr,Jend
# else
#  define JR_RANGE JstrR,JendR
#  define JV_RANGE  Jstr,JendR
# endif

!  Initial conditions for free surface and 2D momentum components.
!------------------------------------------------------------------
!
# if defined BASIN      || defined CANYON_A  || defined CANYON_B   \
   || defined DAMEE_B   || defined DAMEE_S   || defined GRAV_ADJ   \
   || defined NJ_BIGHT  || defined NPACIFIC  || defined OVERFLOW   \
   || defined SEAMOUNT  || defined SHELFRONT || defined TASMAN_SEA \
                        || defined UPWELLING || defined USWEST
      do j=JR_RANGE
        do i=IR_RANGE
          zeta(i,j)=0.
          ubar(i,j)=0.
          vbar(i,j)=0.
        enddo
      enddo
# elif defined SOLITON
      x0= 2.*xl/3.
      y0=el/2.
      cff1=0.395
      cff2=0.771*(cff1*cff1)
      do j=JR_RANGE
        do i=IR_RANGE
          x=xr(i,j)-x0
          y=yr(i,j)-y0
          cff3=exp(-cff1*x)
          fac=cff2*(2.*cff3/(1.+cff3*cff3))**2
          zeta(i,j)=0.25*fac*(6.*y*y+3.)*exp(-0.5*y*y)
        enddo
      enddo
      do j=JR_RANGE
        do i=IU_RANGE
          x=0.5*(xr(i-1,j)+xr(i,j))-x0
          y=0.5*(yr(i-1,j)+yr(i,j))-y0
          cff3=exp(-cff1*x)
          fac=cff2 * (2.*cff3/(1.+cff3*cff3))**2
          ubar(i,j)=0.25*fac*(6.*y*y-9.)*exp(-0.5*y*y)
        enddo
      enddo
      do j=JV_RANGE
        do i=IR_RANGE
          x=0.5*(xr(i,j-1)+xr(i,j))-x0
          y=0.5*(yr(i,j-1)+yr(i,j))-y0
          cff3=exp(-cff1*x)
          fac=cff2 * (2.*cff3/(1.+cff3*cff3))**2
          vbar(i,j)=2.*fac*y*(-2.*cff1*tanh(cff1*x))
     &                                  *exp(-0.5*y*y)
        enddo
      enddo
# else
      ERROR: ANA_INITIAL: no initials for ubar,vbar,zeta
# endif

# if defined EW_PERIODIC || defined NS_PERIODIC || defined  MPI
      call exchange_r2d_tile (Istr,Iend,Jstr,Jend,
     &                        zeta(START_2D_ARRAY))
      call exchange_u2d_tile (Istr,Iend,Jstr,Jend,
     &                        ubar(START_2D_ARRAY))
      call exchange_v2d_tile (Istr,Iend,Jstr,Jend,
     &                        vbar(START_2D_ARRAY))
# endif

# ifdef SOLVE3D
!
!  Initial conditions for momentum components [m/s].
!--------------------------------------------------------------------
!
#  if defined BASIN     || defined CANYON_A  || defined CANYON_B   \
   || defined DAMEE_B   || defined DAMEE_S   || defined GRAV_ADJ   \
   || defined NJ_BIGHT  || defined NPACIFIC  || defined OVERFLOW   \
   || defined SEAMOUNT  || defined SHELFRONT || defined TASMAN_SEA \
                        || defined UPWELLING || defined USWEST
      do k=1,N
        do j=JR_RANGE
          do i=IR_RANGE
            u(i,j,k)=0.
            v(i,j,k)=0.
          enddo
        enddo
      enddo
#  else
        ERROR: ANA_INITIAL: no initial conditions for u,v.
#  endif
!
!  Initial conditions for tracer type variables.
!--------------------------------------------------------------------
!  Set initial conditions for potential temperature [degC] and
!  salinity [PSU].
!
#  ifdef BASIN
      cff1=(44.690/39.382)**2
      cff2=cff1*(rho0*800./g)*(5.0e-5/((42.689/44.690)**2))
      do k=1,N
        do j=JR_RANGE
          do i=IR_RANGE
            t(i,j,k,itemp)=cff2*exp(z_r(i,j,k)/800.)
     &                 *(0.6-0.4*tanh(z_r(i,j,k)/800.))
          enddo
        enddo
      enddo
#  elif defined CANYON_A
      do k=1,N
        do j=JR_RANGE
          do i=IR_RANGE
            t(i,j,k,itemp)=T0
          enddo
        enddo
      enddo
#  elif defined CANYON_B
      do k=1,N
        do j=JR_RANGE
          do i=IR_RANGE
            t(i,j,k,itemp)=3.488*exp(z_r(i,j,k)/800.)
     &        *(1.-0.666666666666*tanh(z_r(i,j,k)/800.))
          enddo
        enddo
      enddo
#  elif defined GRAV_ADJ
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,min(L/2,IendR)  <-- incompatible with MPI
            t(i,j,k,itemp)=T0+5.
          enddo
          do i=max(L/2+1,IstrR),IendR
            t(i,j,k,itemp)=T0
          enddo
        enddo
      enddo
#  elif defined OVERFLOW
      do k=1,N
        do j=JR_RANGE
          do i=IR_RANGE

            t(i,j,k,itemp)=T0*(0.5-0.5*tanh( yr(i,j)/1000.-25.))

          enddo
        enddo
      enddo
#  elif defined NJ_BIGHT
      do k=1,N
        do j=JR_RANGE
          do i=IR_RANGE
            if (z_r(i,j,k).ge.-15.0) then
              t(i,j,k,itemp)=2.049264257728403e+01-z_r(i,j,k)*(
     &                         2.640850848793918e-01+z_r(i,j,k)*(
     &                         2.751125328535212e-01+z_r(i,j,k)*(
     &                         9.207489761648872e-02+z_r(i,j,k)*(
     &                         1.449075725742839e-02+z_r(i,j,k)*(
     &                         1.078215685912076e-03+z_r(i,j,k)*(
     &                         3.240318053903974e-05+
     &                         1.262826857690271e-07*z_r(i,j,k)
     &                                                     ))))))
              t(i,j,k,isalt)=3.066489149193135e+01-z_r(i,j,k)*(
     &                         1.476725262946735e-01+z_r(i,j,k)*(
     &                         1.126455760313399e-01+z_r(i,j,k)*(
     &                         3.900923281871022e-02+z_r(i,j,k)*(
     &                         6.939014937447098e-03+z_r(i,j,k)*(
     &                         6.604436696792939e-04+z_r(i,j,k)*(
     &                         3.191792361954220e-05+
     &                         6.177352634409320e-07*z_r(i,j,k)
     &                                                     ))))))
            else
               t(i,j,k,itemp)=14.6+6.7 *tanh(1.1*z_r(i,j,k)+15.9)
               t(i,j,k,isalt)=31.3-0.55*tanh(1.1*z_r(i,j,k)+15.9)
            endif
          enddo
        enddo
      enddo
#  elif defined SEAMOUNT
      do k=1,N
        do j=JR_RANGE
          do i=IR_RANGE
#define AMPL 3.
#define STRAT 500.

            t(i,j,k,itemp)=AMPL*STRAT*( exp(z_w(i,j,k)/STRAT)
     &                                   -exp(z_w(i,j,k-1)/STRAT)
     &                                )/(z_w(i,j,k)-z_w(i,j,k-1))
#undef STRAT
#undef AMPL

          enddo
        enddo
      enddo
#  elif defined SHELFRONT
      do k=1,N
        do j=JR_RANGE
          do i=IR_RANGE
            t(i,j,k,itemp)=T0+2.5*tanh((yr(i,j)-50000.0)/20000.0)
            t(i,j,k,isalt)=S0  +  tanh((yr(i,j)-50000.0)/20000.0)
          enddo
        enddo
      enddo
#  elif defined TASMAN_SEA
      do k=1,N
        do j=JR_RANGE
          do i=IR_RANGE
            t(i,j,k,itemp)=17.4+z_r(i,j,k)*(1.727e-2+z_r(i,j,k)*
     &                           (5.707e-06+z_r(i,j,k)*(5.921e-10)))
            t(i,j,k,isalt)=35.08+z_r(i,j,k)*(7.56e-4+z_r(i,j,k)*
     &                           (3.185e-07+z_r(i,j,k)*(3.702e-11)))
          enddo
        enddo
      enddo
#  elif defined UPWELLING
      do k=1,N
        do j=JR_RANGE
          do i=IR_RANGE

#define Z0 (-35.)
#define THKNSS 6.5
#define Z1 (-75.)
#define STRAT 150.

           t(i,j,k,itemp)=14.+4.*THKNSS*log(
     &                          cosh((z_w(i,j,k )-Z0)/THKNSS)
     &                         /cosh((z_w(i,j,k-1)-Z0)/THKNSS)
     &                             )/(z_w(i,j,k)-z_w(i,j,k-1))
     &               +((z_w(i,j,k)+z_w(i,j,k-1))/2.-Z1)/STRAT

#undef Z0
#undef THKNSS
#undef Z1
#undef STRAT



#   ifdef SALINITY
c*            t(i,j,k,1,isalt)=1.E-4*yr(i,j)-S0 +0.1
c            t(i,j,k,1,isalt)=1.

            if (j.lt.Mm/2) then
              t(i,j,k,isalt)=0.
            elseif (j.eq.Mm/2) then
              t(i,j,k,isalt)=0.5
            elseif (j.gt.Mm/2) then
              t(i,j,k,isalt)=1.
            endif
#   endif
          enddo
        enddo
      enddo
#  elif defined USWEST
      do k=1,N
        do j=JR_RANGE
          do i=IR_RANGE
            t(i,j,k,itemp)=2.+13.*exp(z_r(i,j,k)/450.)
            t(i,j,k,isalt)=35.
# ifdef BIOLOGY
            temp=t(i,j,k,itemp)
            if (temp.lt.8.) then
               SiO4=30.
            elseif (temp.ge.8. .and. temp.le.11.) then
               SiO4=30.-((temp-8.)*(20./3.))
            elseif (temp.gt.11. .and. temp.le.13.) then
               SiO4=10.-((temp-11.)*(8./2.))
            elseif (temp.gt.13. .and. temp.le.16.) then
               SiO4=2.-((temp-13.)*(2./3.))
            elseif (temp.gt.16.) then
              SiO4=0.
            endif
            t(i,j,k,iNO3_)=1.67+0.5873*SiO4+0.0144*SiO4**2
     &                               +0.0003099*SiO4**3
            t(i,j,k,iNH4_)=0.10
            t(i,j,k,iChla)=0.08
            t(i,j,k,iPhyt)=0.06
            t(i,j,k,iZoo_)=0.04
            t(i,j,k,iSDet)=0.02
            t(i,j,k,iLDet)=0.02
# endif
          enddo
        enddo
      enddo
#  else
      do k=1,N
        do j=JR_RANGE
          do i=IR_RANGE
            t(i,j,k,itemp)=???
            t(i,j,k,isalt)=???
          enddo
        enddo
      enddo
#  endif

#  if defined EW_PERIODIC || defined NS_PERIODIC || defined  MPI
      call exchange_u3d_tile (Istr,Iend,Jstr,Jend,
     &                         u(START_2D_ARRAY,1))
      call exchange_v3d_tile (Istr,Iend,Jstr,Jend,
     &                         v(START_2D_ARRAY,1))
      do itrc=1,NT
        call exchange_r3d_tile (Istr,Iend,Jstr,Jend,
     &                   t(START_2D_ARRAY,1,itrc))
      enddo
#  endif
# endif /* SOLVE3D */
# undef IR_RANGE
# undef IU_RANGE
# undef JR_RANGE
# undef JV_RANGE
      return
      end

#else
      subroutine ana_initial_empty
      return
      end
#endif /* ANA_INITIAL */
