      subroutine read_inp (ierr)
      implicit none
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=384,  MMm=390,  N=66
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp
     &          , isalt
       parameter (itemp=1,
     &            isalt=2,
     &            NT=2
     &           )
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
      integer indxU, indxV, indxT
      parameter (indxU=5, indxV=6, indxT=7)
      integer indxS
      parameter (indxS=indxT+1)
      integer indxO, indxW, indxR, indxAkv, indxAkt
      parameter (indxO=indxT+NT, indxW=indxO+1, indxR=indxO+2,
     &                     indxAkv=indxR+1, indxAkt=indxAkv+1)
      integer indxAks
      parameter (indxAks=indxAkt+1)
      integer indxHbl
      parameter (indxHbl=indxAks+1)
      integer indxSSH
      parameter (indxSSH=indxAkt+3)
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
      integer indxSHFl, indxSWRad
      parameter (indxSHFl=indxSSH+3)
      integer indxSSFl
      parameter (indxSSFl=indxSHFl+1, indxSWRad=indxSHFl+2)
      integer indxSST, indxdQdSST
      parameter (indxSST=indxSWRad+1, indxdQdSST=indxSWRad+2)
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     &                u3dvar, v3dvar, p3dvar, w3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8)
      integer xi_rho,xi_u, eta_rho,eta_v
      parameter (xi_rho=LLm+2,  xi_u=xi_rho-1,
     &           eta_rho=MMm+2, eta_v=eta_rho-1)
      integer ncidfrc, ncidclm,  ntsms,   ntsrf,  ntssh,  ntsst
     &                        ,  nttclm(NT+1),    ntstf(NT+1)
     &                        ,  ntuclm
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV,   rstT(NT+1)
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT(NT+1),      hisR
     &      , hisO,    hisW,     hisAkv,  hisAkt, hisAks
     &                                          , hisHbl
     &      , ncidavg, nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT(NT+1),      avgR
     &      , avgO,    avgW,     avgAkv,  avgAkt, avgAks
     &                                          , avgHbl
      logical wrthis(16+NT-2)
     &      , wrtavg(16+NT-2)
      common/incscrum/
     &        ncidfrc, ncidclm, ntsms,   ntsrf,   ntssh,   ntsst
     &                        ,  nttclm,          ntstf
     &                        ,  ntuclm
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV,   rstT
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT,            hisR
     &      , hisO,    hisW,     hisAkv,  hisAkt, hisAks
     &                                          , hisHbl
     &      , ncidavg,  nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT,            avgR
     &      , avgO,    avgW,     avgAkv,  avgAkt, avgAks
     &                                          , avgHbl
     &      , wrthis
     &      , wrtavg
      character date_str*44, title*80
      character*80 ininame,    grdname,   hisname
     &         ,   rstname,    frcname,   usrname
     &                                ,   avgname
      character*42  vname(4,
     &                          39)
      common /cncscrum/       date_str,   title
     &         ,   ininame,    grdname,   hisname
     &         ,   rstname,    frcname,   usrname
     &                                ,   avgname
     &                      ,  vname
      integer kwsize, testunit, input
      parameter (kwsize=32, testunit=40, input=15)
      character end_signal*3, keyword*32, fname*120
      parameter (end_signal='end')
      integer ierr, iargc, is,ie, kwlen, lstr, lenstr
     &                                       , k, itrc
      fname='scrum.in.US_West'
      if (iargc().eq.1) call getarg(1,fname)
      wrthis(indxTime)=.false.
      wrtavg(indxTime)=.false.
      ierr=0
      call setup_kwds (ierr)
      open (input,file=fname,status='old',form='formatted',err=97)
   1   keyword='                                '
       read(input,'(A)',err=1,end=99) keyword
       if (ichar(keyword(1:1)).eq.33) goto 1
       is=1
   2   if (is.eq.kwsize) then
         goto 1
       elseif (keyword(is:is).eq.' ') then
         is=is+1
         goto 2
       endif
       ie=is
   3   if (keyword(ie:ie).eq.':') then
         keyword(ie:ie)=' '
         goto 4
       elseif (keyword(ie:ie).ne.' ' .and. ie.lt.kwsize) then
         ie=ie+1
         goto 3
       endif
       goto 1
   4   kwlen=ie-is
       if (is.gt.1) keyword(1:kwlen)=keyword(is:is+kwlen-1)
        if (keyword(1:kwlen).eq.'title') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,'(A)',err=95) title
          lstr=lenstr(title)
           write(stdout,'(/1x,A)') title(1:lstr)
        elseif (keyword(1:kwlen).eq.'time_stepping') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) ntimes,dt,ndtfast, ninfo
           write(stdout,
     &   '(I10,2x,A,1x,A /F10.2,2x,A,2(/I10,2x,A,1x,A)/F10.4,2x,A)'
     &    ) ntimes,  'ntimes   Total number of timesteps for',
     &                                              '3D equations.',
     &      dt,      'dt       Timestep [sec] for 3D equations',
     &      ndtfast, 'ndtfast  Number of 2D timesteps within each',
     &                                                   '3D step.',
     &      ninfo,   'ninfo    Number of timesteps between',
     &                                       'runtime diagnostics.'
          dtfast=dt/float(ndtfast)
        elseif (keyword(1:kwlen).eq.'S-coord') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) theta_s, theta_b, Tcline
           write(stdout,
     &                          '(3(1pe10.3,2x,A,1x,A/),32x,A)')
     &      theta_s, 'theta_s  S-coordinate surface control',
     &                                                 'parameter.',
     &      theta_b, 'theta_b  S-coordinate bottom control',
     &                                                 'parameter.',
     &      Tcline,  'Tcline   S-coordinate surface/bottom layer',
     &    'width used in', 'vertical coordinate stretching, meters.'
        elseif (keyword(1:kwlen).eq.'initial') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) nrrec
            read(input,'(A)',err=95) fname
            lstr=lenstr(fname)
            open (testunit, file=fname(1:lstr), status='old', err=97)
            close(testunit)
            ininame=fname(1:lstr)
             write(stdout,'(1x,A,2x,A,4x,A,I3)')
     &       'Initial State File:', ininame(1:lstr), 'Record:',nrrec
        elseif (keyword(1:kwlen).eq.'grid') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,'(A)',err=95) fname
          lstr=lenstr(fname)
          open(testunit,file=fname(1:lstr), status='old', err=97)
          close(testunit)
          grdname=fname(1:lstr)
           write(stdout,'(10x,A,2x,A)')
     &                     'Grid File:', grdname(1:lstr)
        elseif (keyword(1:kwlen).eq.'forcing') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,'(A)',err=95) fname
          lstr=lenstr(fname)
          open (testunit, file=fname(1:lstr), status='old', err=97)
          close(testunit)
          frcname=fname(1:lstr)
           write(stdout,'(2x,A,2x,A)')
     &               'Forcing Data File:', frcname(1:lstr)
        elseif (keyword(1:kwlen).eq.'restart') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) nrst, nrpfrst
          read(input,'(A)',err=95)  fname
          lstr=lenstr(fname)
          rstname=fname(1:lstr)
           write(stdout,
     &               '(7x,A,2x,A,4x,A,I6,4x,A,I4)')
     &               'Restart File:', rstname(1:lstr),
     &               'nrst =', nrst, 'rec/file: ', nrpfrst
        elseif (keyword(1:kwlen).eq.'history') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) ldefhis, nwrt, nrpfhis
          read(input,'(A)',err=95) fname
          lstr=lenstr(fname)
          hisname=fname(1:lstr)
           write(stdout,
     &               '(7x,A,2x,A,2x,A,1x,L1,2x,A,I4,2x,A,I3)')
     &         'History File:', hisname(1:lstr),  'Create new:',
     &         ldefhis, 'nwrt =', nwrt, 'rec/file =', nrpfhis
        elseif (keyword(1:kwlen).eq.'averages') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) ntsavg, navg, nrpfavg
          read(input,'(A)',err=95) fname
          lstr=lenstr(fname)
          avgname=fname(1:lstr)
           write(stdout,
     &           '(2(I10,2x,A,1x,A/32x,A/),6x,A,2x,A,1x,A,I3)')
     &      ntsavg, 'ntsavg      Starting timestep for the',
     &           'accumulation of output', 'time-averaged data.',
     &      navg,   'navg        Number of timesteps between',
     &     'writing of time-averaged','data into averages file.',
     &     'Averages File:', avgname(1:lstr),
     &     'rec/file =', nrpfavg
        elseif (keyword(1:kwlen).eq.'primary_history_fields') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) wrthis(indxZ),  wrthis(indxUb)
     &                                         ,  wrthis(indxVb)
     &                      ,  wrthis(indxU),  wrthis(indxV)
     &                      , (wrthis(itrc), itrc=indxT,indxT+NT-1)
          if ( wrthis(indxZ) .or. wrthis(indxUb) .or. wrthis(indxVb)
     &                          .or. wrthis(indxU) .or. wrthis(indxV)
     &       ) wrthis(indxTime)=.true.
           write(stdout,'(/1x,A,5(/6x,l1,2x,A,1x,A))')
     &    'Fields to be saved in history file: (T/F)'
     &    , wrthis(indxZ),  'write zeta ', 'free-surface.'
     &    , wrthis(indxUb), 'write UBAR ', '2D U-momentum component.'
     &    , wrthis(indxVb), 'write VBAR ', '2D V-momentum component.'
     &    , wrthis(indxU),  'write U    ', '3D U-momentum component.'
     &    , wrthis(indxU),  'write U    ', '3D U-momentum component.'
          do itrc=1,NT
            if (wrthis(indxT+itrc-1)) wrthis(indxTime)=.true.
             write(stdout, '(6x,L1,2x,A,I1,A,I2,A)')
     &                       wrthis(indxT+itrc-1), 'write T(', itrc,
     &                                ')  Tracer of index', itrc,'.'
          enddo
        elseif (keyword(1:kwlen).eq.'auxiliary_history_fields') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) wrthis(indxR), wrthis(indxO)
     &          ,  wrthis(indxW),  wrthis(indxAkv),  wrthis(indxAkt)
     &                                            ,  wrthis(indxAks)
     &                                            ,  wrthis(indxHbl)
          if ( wrthis(indxR) .or. wrthis(indxO) .or. wrthis(indxW)
     &                     .or. wrthis(indxAkv) .or. wrthis(indxAkt)
     &                                          .or. wrthis(indxAks)
     &                                          .or. wrthis(indxHbl)
     &       ) wrthis(indxTime)=.true.
           write(stdout,'(7(/6x,l1,2x,A,1x,A))')
     &      wrthis(indxR),  'write RHO  ', 'Density anomaly'
     &    , wrthis(indxO),  'write Omega', 'Omega vertical velocity.'
     &    , wrthis(indxW),  'write W    ', 'True vertical velocity.'
     &    , wrthis(indxAkv),'write Akv  ', 'Vertical viscosity'
     &    , wrthis(indxAkt),'write Akt  ',
     &                        'Vertical diffusivity for temperature.'
     &    , wrthis(indxAks),'write Aks  ',
     &                           'Vertical diffusivity for salinity.'
     &    , wrthis(indxHbl),'write Hbl  ',
     &                            'Depth of KPP-model boundary layer'
        elseif (keyword(1:kwlen).eq.'primary_averages') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) wrtavg(indxZ),  wrtavg(indxUb)
     &                                         ,  wrtavg(indxVb)
     &                      ,  wrtavg(indxU),  wrtavg(indxV)
     &                      , (wrtavg(itrc), itrc=indxT,indxT+NT-1)
          if ( wrtavg(indxZ) .or. wrtavg(indxUb) .or. wrtavg(indxVb)
     &                          .or. wrtavg(indxU) .or. wrtavg(indxV)
     &       ) wrtavg(indxTime)=.true.
           write(stdout,'(/1x,A,5(/6x,l1,2x,A,1x,A))')
     &    'Fields to be saved in averages file: (T/F)'
     &    , wrtavg(indxZ),  'write zeta ', 'free-surface.'
     &    , wrtavg(indxUb), 'write UBAR ', '2D U-momentum component.'
     &    , wrtavg(indxVb), 'write VBAR ', '2D V-momentum component.'
     &    , wrtavg(indxU),  'write U    ', '3D U-momentum component.'
     &    , wrtavg(indxU),  'write U    ', '3D U-momentum component.'
          do itrc=1,NT
            if (wrtavg(indxT+itrc-1)) wrtavg(indxTime)=.true.
             write(stdout,
     &                       '(6x,L1,2x,A,I1,A,2x,A,I2,A)')
     &                        wrtavg(indxT+itrc-1), 'write T(',
     &                        itrc,')', 'Tracer of index', itrc,'.'
          enddo
        elseif (keyword(1:kwlen).eq.'auxiliary_averages') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) wrtavg(indxR), wrtavg(indxO)
     &          ,  wrtavg(indxW),  wrtavg(indxAkv),  wrtavg(indxAkt)
     &                                            ,  wrtavg(indxAks)
     &                                            ,  wrtavg(indxHbl)
          if ( wrtavg(indxR) .or. wrtavg(indxO) .or. wrtavg(indxW)
     &                     .or. wrtavg(indxAkv) .or. wrtavg(indxAkt)
     &                                          .or. wrtavg(indxAks)
     &                                          .or. wrtavg(indxHbl)
     &       ) wrtavg(indxTime)=.true.
           write(stdout,'(7(/6x,l1,2x,A,1x,A))')
     &      wrtavg(indxR),  'write RHO  ', 'Density anomaly'
     &    , wrtavg(indxO),  'write Omega', 'Omega vertical velocity.'
     &    , wrtavg(indxW),  'write W    ', 'True vertical velocity.'
     &    , wrtavg(indxAkv),'write Akv  ', 'Vertical viscosity'
     &    , wrtavg(indxAkt),'write Akt  ',
     &                        'Vertical diffusivity for temperature.'
     &    , wrtavg(indxAks),'write Aks  ',
     &                           'Vertical diffusivity for salinity.'
     &    , wrtavg(indxHbl),'write Hbl  ',
     &                            'Depth of KPP-model boundary layer'
        elseif (keyword(1:kwlen).eq.'rho0') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) rho0
           write(stdout,'(F10.4,2x,A,1x,A)')
     &          rho0, 'rho0     Boussinesque approximation',
     &                                'mean density, kg/m3.'
        elseif (keyword(1:kwlen).eq.'lateral_visc') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) visc2, visc4
           write(stdout,9) visc2
   9      format(1pe10.3,2x,'visc2       Horizontal Laplacian ',
     &         'mixing coefficient [m2/s]',/,32x,'for momentum.')
        elseif (keyword(1:kwlen).eq.'bottom_drag') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) rdrg, rdrg2
           write(stdout,'((1pe10.3,2x,A))')
     &      rdrg, 'rdrg     Linear bottom drag coefficient, m/s.',
     &      rdrg2,'rdrg2    Quadratic bottom drag coefficient.'
        elseif (keyword(1:kwlen).eq.'gamma2') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) gamma2
           write(stdout,'(f10.2,2x,A,1x,A)')
     &       gamma2, 'gamma2   Slipperiness parameter:',
     &                       'free-slip +1, or no-slip -1.'
        elseif (keyword(1:kwlen).eq.'tracer_diff2') then
          call cancel_kwd (keyword(1:kwlen), ierr)
          read(input,*,err=95) (tnu2(itrc),itrc=1,NT)
          do itrc=1,NT
             write(stdout,7) tnu2(itrc), itrc, itrc
   7        format(1pe10.3,'  tnu2(',i1,')     Horizontal Laplacian '
     &       ,'mixing coefficient (m2/s)',/,32x,'for tracer ',i1,'.')
          enddo
        else
           write(stdout,'(/3(1x,A)/)')
     &                    'WARNING: Urecognized keyword:',
     &                     keyword(1:kwlen),' --> DISREGARDED.'
        endif
       if (keyword(1:kwlen) .eq. end_signal) goto 99
      goto 1
  95  write(stdout,'(/1x,4A/)') 'READ_INP ERROR while reading block',
     &                    ' with keyword ''', keyword(1:kwlen), '''.'
      ierr=ierr+1
      goto 99
  97  write(stdout,'(/1x,4A/)') 'READ_INP ERROR: Cannot find input ',
     &                                'file ''', fname(1:lstr), '''.'
      ierr=ierr+1
  99  close (input)
      if (ierr.eq.0) then
        call check_kwds (ierr)
        call check_srcs
        call check_switches1 (ierr)
        call check_switches2 (ierr)
      endif
      if (ierr.ne.0) then
        write(stdout,'(/1x,2A,I3,1x,A/)') 'READ_INP ERROR: ',
     & 'A total of', ierr, 'configuration errors discovered.'
       return
      endif
      return
      end
