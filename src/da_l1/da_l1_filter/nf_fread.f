      integer function nf_fread  (A, ncid, varid, record, type)
      implicit none
      include 'netcdf.inc'
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=606,  MMm=438,  N=66
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp
     &          , isalt
       parameter (itemp=1,
     &            isalt=2,
     &            NT=2
     &           )
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real A(0:Lm+1+padd_X,0:Mm+1+padd_E,N+1)
      integer ncid,  type,   vert_type,  imin, imax,  start(4)
     &      , varid, record, horiz_type, jmin, jmax,  count(4)
     &      , i,j,k, shift,  ierr,       trd,  omp_get_thread_num
      real buff((Lm+5)*(Mm+5)*(N+1),0:0)
      common /buffer/ buff
      vert_type=type/4
      horiz_type=type-4*vert_type
      jmin=horiz_type/2
      imin=horiz_type-2*jmin
      start(1)=1
      start(2)=1
      imax=Lm+1
      jmax=Mm+1
      count(1)=imax-imin+1
      count(2)=jmax-jmin+1
      if (vert_type.eq.0) then
        count(3)=1
        start(3)=record
      elseif (vert_type.eq.1) then
        count(3)=N
        count(4)=1
        start(3)=1
        start(4)=record
      elseif (vert_type.eq.2) then
        count(3)=N+1
        count(4)=1
        start(3)=1
        start(4)=record
      else
        write(*,'(/1x,2A,I3/)') 'NF_FREAD ERROR: ',
     &                    'illegal grid type', type
        nf_fread=nf_noerr+1
        return
      endif
      trd=omp_get_thread_num()
      ierr=nf_get_vara_double (ncid, varid, start, count, buff(1,trd))
      nf_fread=ierr
      if (ierr .ne. nf_noerr) then
        write(*,'(/1x,2A,I5/1x,3A,I4/)') 'NF_FREAD ERROR: ',
     &               'nf_get_vara netCDF error code =', ierr,
     &               'Cause of error: ', nf_strerror(ierr)
        return
      endif
      do k=1,count(3)
        do j=jmin,jmax
          shift=1-imin+count(1)*(j-jmin+(k-1)*count(2))
          do i=imin,imax
            A(i,j,k)=buff(i+shift,trd)
          enddo
        enddo
      enddo
      return
      end
      integer function nf_fwrite (A, ncid, varid, record, type)
      implicit none
      include 'netcdf.inc'
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=606,  MMm=438,  N=66
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp
     &          , isalt
       parameter (itemp=1,
     &            isalt=2,
     &            NT=2
     &           )
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real A(0:Lm+1+padd_X,0:Mm+1+padd_E,N+1)
      integer ncid,  type,   vert_type,  imin, imax,  start(4)
     &      , varid, record, horiz_type, jmin, jmax,  count(4)
     &      , i,j,k, shift,  ierr,       trd,  omp_get_thread_num
      real buff((Lm+5)*(Mm+5)*(N+1),0:0)
      common /buffer/ buff
      vert_type=type/4
      horiz_type=type-4*vert_type
      jmin=horiz_type/2
      imin=horiz_type-2*jmin
      start(1)=1
      start(2)=1
      imax=Lm+1
      jmax=Mm+1
      count(1)=imax-imin+1
      count(2)=jmax-jmin+1
      if (vert_type.eq.0) then
        count(3)=1
        start(3)=record
      elseif (vert_type.eq.1) then
        count(3)=N
        count(4)=1
        start(3)=1
        start(4)=record
      elseif (vert_type.eq.2) then
        count(3)=N+1
        count(4)=1
        start(3)=1
        start(4)=record
      else
        write(*,'(/1x,2A,I4/)') 'NF_FWRITE ERROR: ',
     &                    'illegal grid type', type
        nf_fwrite=nf_noerr+1
        return
      endif
      trd=omp_get_thread_num()
      do k=1,count(3)
        do j=jmin,jmax
          shift=1-imin+count(1)*(j-jmin+(k-1)*count(2))
          do i=imin,imax
            buff(i+shift,trd)=A(i,j,k)
          enddo
        enddo
      enddo
      ierr=nf_put_vara_double (ncid, varid, start, count, buff(1,trd))
      nf_fwrite=ierr
      if (ierr.ne.nf_noerr) then
        write(*,'(/1x,2A,I5/1x,3A,I4/)') 'NF_FWRITE ERROR: ',
     &               'nf_put_vara netCDF error code =', ierr,
     &               'Cause of error: ', nf_strerror(ierr)
      endif
      return
      end
