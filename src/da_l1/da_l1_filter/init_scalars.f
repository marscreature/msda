       subroutine init_scalars (ierr)
      implicit none
      integer ierr, i,j, lstr,lenstr
      integer omp_get_num_threads
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=606,  MMm=438,  N=66
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp
     &          , isalt
       parameter (itemp=1,
     &            isalt=2,
     &            NT=2
     &           )
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      real visc2_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real visc2_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /mixing_visc2_r/visc2_r /mixing_visc2_p/visc2_p
      real diff2(0:Lm+1+padd_X,0:Mm+1+padd_E,NT)
      common /mixing_diff2/diff2
      real Akv(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      real Akt(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N,NT)
      common /mixing_Akv/Akv /mixing_Akt/Akt
      real bvf(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      common /mixing_bvf/ bvf
      integer kbl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hbl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real ustar(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /lmd_kpp_kbl/kbl     /lmd_kpp_hbl/hbl
     &       /lmd_kpp_ustar/ustar
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
      integer indxU, indxV, indxT
      parameter (indxU=5, indxV=6, indxT=7)
      integer indxS
      parameter (indxS=indxT+1)
      integer indxO, indxW, indxR, indxAkv, indxAkt
      parameter (indxO=indxT+NT, indxW=indxO+1, indxR=indxO+2,
     &                     indxAkv=indxR+1, indxAkt=indxAkv+1)
      integer indxAks
      parameter (indxAks=indxAkt+1)
      integer indxHbl
      parameter (indxHbl=indxAks+1)
      integer indxSSH
      parameter (indxSSH=indxAkt+3)
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
      integer indxSHFl, indxSWRad
      parameter (indxSHFl=indxSSH+3)
      integer indxSSFl
      parameter (indxSSFl=indxSHFl+1, indxSWRad=indxSHFl+2)
      integer indxSST, indxdQdSST
      parameter (indxSST=indxSWRad+1, indxdQdSST=indxSWRad+2)
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     &                u3dvar, v3dvar, p3dvar, w3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8)
      integer xi_rho,xi_u, eta_rho,eta_v
      parameter (xi_rho=LLm+2,  xi_u=xi_rho-1,
     &           eta_rho=MMm+2, eta_v=eta_rho-1)
      integer ncidfrc, ncidclm,  ntsms,   ntsrf,  ntssh,  ntsst
     &                        ,  nttclm(NT+1),    ntstf(NT+1)
     &                        ,  ntuclm
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV,   rstT(NT+1)
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT(NT+1),      hisR
     &      , hisO,    hisW,     hisAkv,  hisAkt, hisAks
     &                                          , hisHbl
     &      , ncidavg, nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT(NT+1),      avgR
     &      , avgO,    avgW,     avgAkv,  avgAkt, avgAks
     &                                          , avgHbl
      logical wrthis(16+NT-2)
     &      , wrtavg(16+NT-2)
      common/incscrum/
     &        ncidfrc, ncidclm, ntsms,   ntsrf,   ntssh,   ntsst
     &                        ,  nttclm,          ntstf
     &                        ,  ntuclm
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV,   rstT
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT,            hisR
     &      , hisO,    hisW,     hisAkv,  hisAkt, hisAks
     &                                          , hisHbl
     &      , ncidavg,  nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT,            avgR
     &      , avgO,    avgW,     avgAkv,  avgAkt, avgAks
     &                                          , avgHbl
     &      , wrthis
     &      , wrtavg
      character date_str*44, title*80
      character*80 ininame,    grdname,   hisname
     &         ,   rstname,    frcname,   usrname
     &                                ,   avgname
      character*42  vname(4,
     &                          39)
      common /cncscrum/       date_str,   title
     &         ,   ininame,    grdname,   hisname
     &         ,   rstname,    frcname,   usrname
     &                                ,   avgname
     &                      ,  vname
C$OMP PARALLEL
C$OMP CRITICAL (isca_cr_rgn)
      numthreads=omp_get_num_threads()
C$OMP END CRITICAL (isca_cr_rgn)
C$OMP END PARALLEL
       write(stdout,'(1x,A,3(1x,A,I3),A)') 'NUMBER',
     &    'OF THREADS:',numthreads,'BLOCKING:',NSUB_X,'x',NSUB_E,'.'
      if (numthreads.gt.NPP) then
         write(stdout,'(/1x,A,I3/)')
     &  'ERROR: Requested number of threads not equal to NPP =', NPP
        ierr=ierr+1
      elseif (mod(NSUB_X*NSUB_E,numthreads).ne.0) then
         write(stdout,
     &                '(/1x,A,1x,A,I3,4x,A,I3,4x,A,I4,A)') 'ERROR:',
     &                'wrong choice of numthreads =', numthreads,
     &                'NSUB_X =', NSUB_X, 'NSUB_E =', NSUB_E, '.'
        ierr=ierr+1
      endif
      time=0.
      tdays=0.
      PREDICTOR_2D_STEP=.FALSE.
      iic=0
      kstp=1
      krhs=1
      knew=1
      ntstart=1
      nstp=1
      nrhs=1
      nnew=1
      nfast=1
      synchro_flag=.true.
      first_time=0
      may_day_flag=0
      do j=0,NPP
        do i=0,31
          proc(i,j)=0
          CPU_time(i,j)=0.E0
        enddo
      enddo
      trd_count=0
      nrecrst=0
      nrechis=0
      nrecavg=0
      tile_count=0
      bc_count=0
      avgke=0.
      avgpe=0.
      avgkp=0.
      volume=0.
      hmin=+1.E+20
      hmax=-1.E+20
      grdmin=+1.E+20
      grdmax=-1.E+20
      Cu_min=+1.E+20
      Cu_max=-1.E+20
      bc_crss=0.q0
      bc_flux=0.q0
      ubar_xs=0.q0
      rx0=-1.E+20
      rx1=-1.E+20
      ncidrst=-1
      ncidhis=-1
      ncidavg=-1
      ncidfrc=-1
      ncidclm=-1
      call get_date (date_str)
      vname(1,indxTime)='scrum_time                                '
      vname(2,indxTime)='time since initialization                 '
      vname(3,indxTime)='second                                    '
      vname(4,indxTime)='time, scalar, series                      '
      vname(1,indxZ)='zeta                                      '
      vname(2,indxZ)='free-surface                              '
      vname(3,indxZ)='meter                                     '
      vname(4,indxZ)='free-surface, scalar, series              '
      vname(1,indxUb)='ubar                                      '
      vname(2,indxUb)='vertically integrated u-momentum component'
      vname(3,indxUb)='meter second-1                            '
      vname(4,indxUb)='ubar-velocity, scalar, series             '
      vname(1,indxVb)='vbar                                      '
      vname(2,indxVb)='vertically integrated v-momentum component'
      vname(3,indxVb)='meter second-1                            '
      vname(4,indxVb)='vbar-velocity, scalar, series             '
      vname(1,indxU)='u                                         '
      vname(2,indxU)='u-momentum component                      '
      vname(3,indxU)='meter second-1                            '
      vname(4,indxU)='u-velocity, scalar, series                '
      vname(1,indxV)='v                                         '
      vname(2,indxV)='v-momentum component                      '
      vname(3,indxV)='meter second-1                            '
      vname(4,indxV)='v-velocity, scalar, series                '
      vname(1,indxT)='temp                                      '
      vname(2,indxT)='potential temperature                     '
      vname(3,indxT)='Celsius                                   '
      vname(4,indxT)='temperature, scalar, series               '
      vname(1,indxS)='salt                                      '
      vname(2,indxS)='salinity                                  '
      vname(3,indxS)='PSU                                       '
      vname(4,indxS)='salinity, scalar, series                  '
      vname(1,indxO)='omega                                    '
      vname(2,indxO)='S-coordinate vertical momentum component '
      vname(3,indxO)='meter3 second-1                          '
      vname(4,indxO)='omega, scalar, series                    '
      vname(1,indxW)='w                                        '
      vname(2,indxW)='vertical momentum component              '
      vname(3,indxW)='meter second-1                           '
      vname(4,indxW)='w-velocity, scalar, series               '
      vname(1,indxR)='rho                                      '
      vname(2,indxR)='density anomaly                          '
      vname(3,indxR)='kilogram meter-3                         '
      vname(4,indxR)='density, scalar, series                  '
      vname(1,indxAkv)='AKv                                    '
      vname(2,indxAkv)='vertical viscosity coefficient         '
      vname(3,indxAkv)='meter2 second-1                        '
      vname(4,indxAkv)='AKv, scalar, series                    '
      vname(1,indxAkt)='AKt                                    '
      vname(2,indxAkt)='temperature vertical diffusion coef    '
      vname(3,indxAkt)='meter2 second-1                        '
      vname(4,indxAkt)='AKt, scalar, series                    '
      vname(1,indxAks)='AKs                                    '
      vname(2,indxAks)='salinity vertical diffusion coefficient'
      vname(3,indxAks)='meter2 second-1                        '
      vname(4,indxAks)='AKs, scalar, series                    '
      vname(1,indxHbl)='hbl                                    '
      vname(2,indxHbl)='depth of planetary boundary layer      '
      vname(3,indxHbl)='meter                                  '
      vname(4,indxHbl)='hbl, scalar, series                    '
      vname(1,indxSSH)='SSH                                    '
      vname(2,indxSSH)='sea surface height                     '
      vname(3,indxSSH)='meter                                  '
      vname(4,indxSSH)='SSH, scalar, series                    '
      vname(1,indxSUSTR)='sustr                                  '
      vname(2,indxSUSTR)='surface u-momentum stress              '
      vname(3,indxSUSTR)='Newton meter-2                         '
      vname(4,indxSUSTR)='surface u-mometum stress, scalar, series'
      vname(1,indxSVSTR)='svstr                                     '
      vname(2,indxSVSTR)='surface v-momentum stress                 '
      vname(3,indxSVSTR)='Newton meter-2                            '
      vname(4,indxSVSTR)='surface v-momentum stress, scalar, series '
      vname(1,indxSHFl)='shflux                                    '
      vname(2,indxSHFl)='surface net heat flux                     '
      vname(3,indxSHFl)='Watts meter-2                             '
      vname(4,indxSHFl)='surface heat flux, scalar, series         '
      vname(1,indxSSFl)='swflux                                    '
      vname(2,indxSSFl)='surface freshwater flux (E-P)             '
      vname(3,indxSSFl)='centimeter day-1                          '
      vname(4,indxSSFl)='surface freshwater flux, scalar, series   '
      vname(1,indxSWRad)='swrad                                     '
      vname(2,indxSWRad)='solar shortwave radiation                 '
      vname(3,indxSWRad)='Watts meter-2                             '
      vname(4,indxSWRad)='shortwave radiation, scalar, series       '
      vname(1,indxSST)='SST                                       '
      vname(2,indxSST)='sea surface temperature                   '
      vname(3,indxSST)='Celsius                                   '
      vname(4,indxSST)='SST, scalar, series                       '
      vname(1,indxdQdSST)='dQdSST                                    '
      vname(2,indxdQdSST)='surface net heat flux sensitivity to SST  '
      vname(3,indxdQdSST)='Watts meter-2 Celsius-1                   '
      vname(4,indxdQdSST)='dQdSST, scalar, series                    '
      return
      end
