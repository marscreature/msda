      subroutine das_s2z_3d(tile)
      implicit none
      integer tile
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=606,  MMm=438,  N=66
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp
     &          , isalt
       parameter (itemp=1,
     &            isalt=2,
     &            NT=2
     &           )
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer chunk_size_X,margin_X,chunk_size_E,margin_E
      parameter (chunk_size_X=(Lm+NSUB_X-1)/NSUB_X,
     &                     margin_X=(NSUB_X*chunk_size_X-Lm)/2,
     &           chunk_size_E=(Mm+NSUB_E-1)/NSUB_E,
     &                     margin_E=(NSUB_E*chunk_size_E-Mm)/2)
      integer Istr,Iend,Jstr,Jend, i_X,j_E
        j_E=tile/NSUB_X
        i_X=tile-j_E*NSUB_X
        Istr=1+i_X*chunk_size_X-margin_X
        Iend=Istr+chunk_size_X-1
        Istr=max(Istr,1)
        Iend=min(Iend,Lm)
        Jstr=1+j_E*chunk_size_E-margin_E
        Jend=Jstr+chunk_size_E-1
        Jstr=max(Jstr,1)
        Jend=min(Jend,Mm)
      call das_s2z_3d_tile (Istr,Iend,Jstr,Jend)
      return
      end
      subroutine das_s2z_3d_tile (Istr,Iend,Jstr,Jend)
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k,itrc
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=606,  MMm=438,  N=66
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp
     &          , isalt
       parameter (itemp=1,
     &            isalt=2,
     &            NT=2
     &           )
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_sio, max_whoi, max_flight, max_prf
      PARAMETER(max_sio=64,max_whoi=192,max_flight=1500,
     &         max_prf=NDAS)
      integer max_moor
      PARAMETER(max_moor=68)
      real sio_ot,sio_os,whoi_ot,whoi_os,moor_ot,moor_os
      PARAMETER( sio_ot=1.0/(1.5*1.5),sio_os=1.0/(0.3*0.3),
     &           whoi_ot=1.0/(1.5*1.5),whoi_os=1.0/(0.3*0.3),
     &           moor_ot=1.0/(1.8*1.8),moor_os=1.0/(0.25*0.25)
     &         )
      real regp
      PARAMETER( regp=1./(10.*10.) )
      integer nratio
      PARAMETER( nratio=3 )
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      real zeta(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real ubar(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vbar(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real rzeta_bak(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real rubar_bak(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real rvbar_bak(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_zeta/zeta  /rhs_rzeta/rzeta_bak
     &       /ocean_ubar/ubar  /rhs_rubar/rubar_bak
     &       /ocean_vbar/vbar  /rhs_rvbar/rvbar_bak
      real u(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real v(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real t(0:Lm+1+padd_X,0:Mm+1+padd_E,N,NT)
      common /ocean_u/u /ocean_v/v /ocean_t/t
      real Hz(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real Hz_bak(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real z_r(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real z_w(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      real Huon(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real Hvom(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real W(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      common /grid_Hz/Hz    /grid_zr/z_r  /grid_W/W
     &  /grid_Hz_bak/Hz_bak /grid_zw/z_w  /grid_Huon/Huon
     &                                    /grid_Hvom/Hvom
      real rho(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real rhop0(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      common /ocean_rho/rho /ocean_rhop0/rhop0
      real zeta_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_das/u_das /ocean_v_das/v_das
     &     /ocean_t_das/t_das /ocean_zeta_das/zeta_das
      real zeta_s(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s/u_s /ocean_v_s/v_s
     &      /ocean_rho_s/rho_s /ocean_p_s/p_s
     &     /ocean_t_s/t_s /ocean_zeta_s/zeta_s
      real rmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real umask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real vmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /mask_r_das/rmask_das
     &       /mask_u_das/umask_das /mask_v_das/vmask_das
      integer nzr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nzr_das/nzr_das
     &       /ocean_nzu_das/nzu_das /ocean_nzv_das/nzv_das
      integer nsr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nsr_das/nsr_das
     &       /ocean_nsu_das/nsu_das /ocean_nsv_das/nsv_das
      real z_das(ndas),tc1d_das(ndas),sc1d_das(ndas)
      common /grid_z_das/z_das,tc1d_das,sc1d_das
       real s1d(N),  z1d(NDAS)
       real xs1d(N), xz1d(NDAS)
       integer KZ
       external das_spln1d
      integer IstrR,IendR,JstrR,JendR
      if (Istr.eq.1) then
        IstrR=Istr-1
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lm) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        JstrR=Jstr-1
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mm) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      do j=JstrR,JendR
        do i=IstrR,IendR
          zeta_das(i,j)=zeta(i,j)
        enddo
      enddo
      do itrc=1,NT
        do j=JstrR,JendR
          do i=IstrR,IendR
             do k=1,N
               s1d(k)=z_r(i,j,k)
               xs1d(k)=t(i,j,k,itrc)
             enddo
             KZ=NDAS-nzr_das(i,j)+1
             do k=nzr_das(i,j),NDAS
               z1d(k-nzr_das(i,j)+1)=z_das(k)
             enddo
             call das_spln1d(N,s1d,xs1d,KZ,z1d,xz1d)
             do k=nzr_das(i,j),NDAS
               t_das(i,j,k,itrc)=xz1d(k-nzr_das(i,j)+1)
             enddo
          enddo
        enddo
      enddo
      do itrc=1,NT
        do j=JstrR,JendR
          do i=IstrR,IendR
            t_das(i,j,NDAS,itrc)=t(i,j,N,itrc)
          enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=Istr,IendR
           do k=1,N
             s1d(k)=0.5*(z_r(i-1,j,k)+z_r(i,j,k))
             xs1d(k)=u(i,j,k)
           enddo
           KZ=NDAS-nzu_das(i,j)+1
           do k=nzu_das(i,j),NDAS
             z1d(k-nzu_das(i,j)+1)=z_das(k)
           enddo
           call das_spln1d(N,s1d,xs1d,KZ,z1d,xz1d)
           do k=nzu_das(i,j),NDAS
             u_das(i,j,k)=xz1d(k-nzu_das(i,j)+1)
           enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=Istr,IendR
          u_das(i,j,NDAS)=u(i,j,N)
        enddo
      enddo
      do j=Jstr,JendR
        do i=IstrR,IendR
           do k=1,N
             s1d(k)=0.5*(z_r(i,j-1,k)+z_r(i,j,k))
             xs1d(k)=v(i,j,k)
           enddo
           KZ=NDAS-nzv_das(i,j)+1
           do k=nzv_das(i,j),NDAS
             z1d(k-nzv_das(i,j)+1)=z_das(k)
           enddo
           call das_spln1d(N,s1d,xs1d,KZ,z1d,xz1d)
           do k=nzv_das(i,j),NDAS
             v_das(i,j,k)=xz1d(k-nzv_das(i,j)+1)
           enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=Istr,IendR
          v_das(i,j,NDAS)=v(i,j,N)
        enddo
      enddo
      return
      end
