! This is include file "ncscrum.h".
! ==== == ======= ==== ============
! indices in character array "vname", which holds variable names
!                                                and attributes.
! indxTime        time
! indxZ           free-surface
! indxUb,indxVb   vertically integrated 2D U,V-momentum components
!
! indxU,indxV     3D U- and V-momenta.
! indxT,indxS, ..., indxZoo  tracers (temerature, salinity,
!                 biological tracers.
! indxO,indeW     omega vertical mass flux and true vertical velocity
! indxR           density anomaly
!
! indxAkv,indxAkt,indxAks  vertical viscosity/diffusivity coeffcients
! indxHbl         depth of planetary boundary layer in KPP model
!
! indxSSH         observed sea surface height (from climatology)
! indxSUSTR,indxSVSTR  surface U-, V-momentum stress (wind forcing)
! indxSHFl        net surface heat flux.
! indxSWRad       shortwave radiation flux
! indxSST         sea surface temperature
! indxdQdSST      Q-correction coefficient dQdSST
! indxSSFl        surface fresh water flux
! indxSSFl        surface fresh water flux
!
! indxAi          fraction of cell covered by ice
! indxUi,indxVi   U,V-components of sea ice velocity
! indxHi,indxHS   depth of ice cover and depth of snow cover
! indxTIsrf       temperature of ice surface
!
! indxBSD,indxBSS bottom sediment grain Density and Size.
! indxWWA,indxWWD,indxWWP   wind induced wave Amplitude,
!                 Direction and Period
!
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
#ifdef SOLVE3D
      integer indxU, indxV, indxT
      parameter (indxU=5, indxV=6, indxT=7)
# ifdef SALINITY
      integer indxS
      parameter (indxS=indxT+1)
# endif
# ifdef BIOLOGY
      integer indxNO3, indxNH4, indxChla,
     &        indxPhyt, indxZoo, indxSDet, indxLDet
#  ifdef SALINITY
      parameter (indxNO3=indxS+1)
#  else
      parameter (indxNO3=indxT+1)
#  endif
      parameter (indxNH4=indxNO3+1,  indxChla=indxNO3+2,
     &           indxPhyt=indxNO3+3, indxZoo=indxNO3+4,
     &           indxSDet=indxNO3+5, indxLDet=indxNO3+6)
# endif
      integer indxO, indxW, indxR, indxAkv, indxAkt
      parameter (indxO=indxT+NT, indxW=indxO+1, indxR=indxO+2,
     &                     indxAkv=indxR+1, indxAkt=indxAkv+1)
# ifdef SALINITY
      integer indxAks
      parameter (indxAks=indxAkt+1)
# endif
# ifdef LMD_KPP
      integer indxHbl
#  ifdef SALINITY
      parameter (indxHbl=indxAks+1)
#  else
      parameter (indxHbl=indxAkt+1)
#  endif
# endif
#endif
      integer indxSSH
#ifdef SOLVE3D
      parameter (indxSSH=indxAkt+3)
#else
      parameter (indxSSH=indxVb +1)
#endif
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
#ifdef SOLVE3D
      integer indxSHFl, indxSWRad
      parameter (indxSHFl=indxSSH+3)
# ifdef SALINITY
      integer indxSSFl
      parameter (indxSSFl=indxSHFl+1, indxSWRad=indxSHFl+2)
#else
      parameter (indxSWRad=indxSHFl+1)
# endif
      integer indxSST, indxdQdSST
      parameter (indxSST=indxSWRad+1, indxdQdSST=indxSWRad+2)
# ifdef SG_BBL96
#  ifndef ANA_WWAVE
      integer indxWWA,indxWWD,indxWWP
      parameter indxWWA=???, indxWWD=???, indxWWP=???)
#  endif
#  ifndef ANA_BSEDIM
#  endif
# endif
#endif
#ifdef ICE
      integer indxAi
      parameter (indxAi=????)
      integer indxUi, indxVi, indxHi, indxHS, indxTIsrf
      parameter (indxUi=indxAi+1, indxVi=indxAi+2, indxHi=indxAi+3,
     &                         indxHS=indxAi+4, indxTIsrf=indxAi+5)
#endif
!
! Grid Type Codes:  r2dvar....w3hvar are codes for array types.
! ==== ==== ======  The codes are set according to the rule:
!                     horiz_grid_type+4*vert_grid_type
!    where horiz_grid_type=0,1,2,3 for RHO-,U-,V-,PSI-points
!    respectively and vert_grid_type=0 for 2D fields; 1,2 for
!    3D-RHO- and W-vertical points.
!
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     &                u3dvar, v3dvar, p3dvar, w3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8) 

!            Horizontal array dimensions in netCDF files.
! xi_rho     WARNING!!! In MPI code in the case of PARALLEL_FILES 
! xi_u       _and_ NON-Periodicity in either XI- or ETA-direction,
! eta_rho    these depend on corresonding MPI-node indices ii,jj
! eta_v      and therefore become live variables, which are placed
!            into common block below rather than defined here as
!            parameters. 

      integer xi_rho,xi_u, eta_rho,eta_v
#if defined MPI && defined PARALLEL_FILES
# ifdef EW_PERIODIC
      parameter (xi_rho=Lm,     xi_u=Lm)
# endif
# ifdef NS_PERIODIC
      parameter (eta_rho=Mm,    eta_v=Mm)
# endif
#else
      parameter (xi_rho=LLm+2,  xi_u=xi_rho-1,
     &           eta_rho=MMm+2, eta_v=eta_rho-1)
#endif
!
! Naming conventions for indices, variable IDs, etc...
!
! prefix ncid_  means netCDF ID for netCDF file
!        nrec_  record number in netCDF file since initialization
!        nrpf_  maximum number of records per file  (output netCDF
!                                                       files only)
! prefix/ending rst_/_rst refers to restart  netCDF file
!               his_/_his           history
!               avg_/_avg           averages
!               sta_/_sta           stations
!                    _frc           forcing
!                    _clm           climatology
!
! endings refer to:  ___Time  time [in seconds]
!                    ___Tstep time step numbers and record numbers
!   all objects      ___Z     free-surface
!   with these       ___Ub    vertically integrated 2D U-momentum
!   endings are      ___Vb    vertically integrated 2D V-momentum
!   either
!     netCDF IDs,    ___U     3D U-momentum
!     if occur with  ___V     3D V-momentum
!     prefices rst/  ___T(NT) tracers
!     /his/avg/sta   ___R     density anomaly
!   or               ___O     omega vertical velocity
!     parameter      ___W     true vertical velocity
!     indices, if
!     occur with     ___Akv   vertical viscosity coefficient
!     prefix indx    ___Akt   vertical T-diffusion coefficient
!     (see above).   ___Aks   vertical S-diffusion coefficient
!                    ___Hbl   depth of mixed layer LMD_KPP.
!
! Sizes of unlimited time dimensions in netCDF files:
!
!   ntsms   surface momentum stress in current forcing file.
!   ntsrf   shortwave radiation flux in current forcing file.
!   ntssh   sea surface height in current climatology file.
!   ntsst   sea surface temperature in current forcing file.
!   ntstf   surface flux of tracers in current forcing file.
!   nttclm  tracer variables in current climatology file.
!   ntuclm  momentum variables in current climatology file.
!   ntww    wind induced wave data in current forcing file.
!
! vname    character array for variable names and attributes;
!
      integer ncidfrc, ncidclm,  ntsms,   ntsrf,  ntssh,  ntsst
#ifdef SOLVE3D
     &                        ,  nttclm(NT+1),    ntstf(NT+1)
#endif
     &                        ,  ntuclm
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
#ifdef SOLVE3D
     &                         , rstU,    rstV,   rstT(NT+1)
# endif
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
#ifdef SOLVE3D
     &      , hisU,    hisV,     hisT(NT+1),      hisR
     &      , hisO,    hisW,     hisAkv,  hisAkt, hisAks
# ifdef LMD_KPP
     &                                          , hisHbl
# endif
#endif
#ifdef AVERAGES
     &      , ncidavg, nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
# ifdef SOLVE3D
     &      , avgU,    avgV,     avgT(NT+1),      avgR
     &      , avgO,    avgW,     avgAkv,  avgAkt, avgAks
#  ifdef LMD_KPP
     &                                          , avgHbl
#  endif
# endif
#endif
#ifdef STATIONS
     &      , ncidsta, nrecsta,  stadid,   stazid
     &      , staubid, stavbid,  nstation,ispos(NS),jspos(NS)
# ifdef SOLVE3D
     &      , stauid,  stavid,   statid(NT+1),     starid
     &      , stawid,  staakvid, staaktid,         staaksid
#  ifdef LMD_KPP
     &      , stahblid   
#  endif
# endif
#endif
      logical wrthis(16+NT-2)
#ifdef AVERAGES
     &      , wrtavg(16+NT-2)
#endif
#ifdef STATIONS
     &      , wsta(16+NT-2)
#endif

      common/incscrum/
     &        ncidfrc, ncidclm, ntsms,   ntsrf,   ntssh,   ntsst
#if defined MPI && defined PARALLEL_FILES
# ifndef EW_PERIODIC
     &      , xi_rho,  xi_u
# endif
# ifndef EW_PERIODIC
     &      , eta_rho, eta_v
# endif
#endif
#ifdef SOLVE3D
     &                        ,  nttclm,          ntstf
#endif
     &                        ,  ntuclm
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
#ifdef SOLVE3D
     &                         , rstU,    rstV,   rstT
# endif
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
#ifdef SOLVE3D
     &      , hisU,    hisV,     hisT,            hisR
     &      , hisO,    hisW,     hisAkv,  hisAkt, hisAks
# ifdef LMD_KPP
     &                                          , hisHbl
# endif
#endif
#ifdef AVERAGES
     &      , ncidavg,  nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
# ifdef SOLVE3D
     &      , avgU,    avgV,     avgT,            avgR
     &      , avgO,    avgW,     avgAkv,  avgAkt, avgAks
#  ifdef LMD_KPP
     &                                          , avgHbl
#  endif
# endif
#endif
#ifdef STATIONS
     &      , ncidsta, nrecsta,  stadid, stazid
     &      , staubid, stavbid,  nstation, ispos,  jspos
# ifdef SOLVE3D
     &      , stauid,  stavid,   statid,           starid
     &      , stawid,  staakvid, staaktid,         staaksid
#  ifdef LMD_KPP
     &      , stahblid  
#  endif
# endif
#endif
     &      , wrthis
#ifdef AVERAGES
     &      , wrtavg
#endif
#ifdef STATIONS
     &      , wsta
#endif

      character date_str*44, title*80
      character*80 ininame,    grdname,   hisname
     &         ,   rstname,    frcname,   usrname
#ifdef AVERAGES
     &                                ,   avgname
#endif
#if (defined TCLIMATOLOGY && !defined ANA_TCLIMA)\
 || (defined ZNUDGING && !defined ANA_SSH)
     &                                ,   clmname
#endif
#ifdef STATIONS
     &                      ,  staname,   sposnam
#endif
#ifdef FLOATS
     &                      ,  fltname,   fposnam
#endif
#ifdef ASSIMILATION
     &                      ,  aparnam,   assname
#endif
      character*42  vname(4,
#ifdef BIOLOGY
     &                          39+NT-2)
#else
     &                          39)
#endif
      common /cncscrum/       date_str,   title
     &         ,   ininame,    grdname,   hisname
     &         ,   rstname,    frcname,   usrname
#ifdef AVERAGES
     &                                ,   avgname
#endif
#if (defined TCLIMATOLOGY && !defined ANA_TCLIMA)\
 || (defined ZNUDGING && !defined ANA_SSH)
     &                                ,   clmname
#endif
#ifdef STATIONS
     &                      ,  staname,   sposnam
#endif
#ifdef FLOATS
     &                      ,  fltname,   fposnam
#endif
#ifdef ASSIMILATION
     &                      ,  aparnam,   assname
#endif
     &                      ,  vname 
