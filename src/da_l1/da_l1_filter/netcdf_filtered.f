      subroutine netcdf_filtered(file_nc)
      implicit none
      include 'netcdf.inc'
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=606,  MMm=438,  N=66
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp
     &          , isalt
       parameter (itemp=1,
     &            isalt=2,
     &            NT=2
     &           )
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_sio, max_whoi, max_flight, max_prf
      PARAMETER(max_sio=64,max_whoi=192,max_flight=1500,
     &         max_prf=NDAS)
      integer max_moor
      PARAMETER(max_moor=68)
      real sio_ot,sio_os,whoi_ot,whoi_os,moor_ot,moor_os
      PARAMETER( sio_ot=1.0/(1.5*1.5),sio_os=1.0/(0.3*0.3),
     &           whoi_ot=1.0/(1.5*1.5),whoi_os=1.0/(0.3*0.3),
     &           moor_ot=1.0/(1.8*1.8),moor_os=1.0/(0.25*0.25)
     &         )
      real regp
      PARAMETER( regp=1./(10.*10.) )
      integer nratio
      PARAMETER( nratio=3 )
      real zeta_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_das/u_das /ocean_v_das/v_das
     &     /ocean_t_das/t_das /ocean_zeta_das/zeta_das
      real zeta_s(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s/u_s /ocean_v_s/v_s
     &      /ocean_rho_s/rho_s /ocean_p_s/p_s
     &     /ocean_t_s/t_s /ocean_zeta_s/zeta_s
      real rmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real umask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real vmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /mask_r_das/rmask_das
     &       /mask_u_das/umask_das /mask_v_das/vmask_das
      integer nzr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nzr_das/nzr_das
     &       /ocean_nzu_das/nzu_das /ocean_nzv_das/nzv_das
      integer nsr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nsr_das/nsr_das
     &       /ocean_nsu_das/nsu_das /ocean_nsv_das/nsv_das
      real z_das(ndas),tc1d_das(ndas),sc1d_das(ndas)
      common /grid_z_das/z_das,tc1d_das,sc1d_das
      real h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hinv(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real f(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fomn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_h/h /grid_hinv/hinv /grid_f/f /grid_fomn/fomn
      real angler(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_angler/angler
      real latr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real lonr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_latr/latr /grid_lonr/lonr
      real pm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_pm/pm    /metrics_pn/pn
     &       /metrics_omu/om_u /metrics_on_u/on_u
     &       /metrics_omv/om_v /metrics_on_v/on_v
      real dmde(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real dndx(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_dmde/dmde    /metrics_dndx/dndx
      real on_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmon_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pnom_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real grdscl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_on_p/on_p /metrics_om_p/om_p
     &       /metrics_on_r/on_r /metrics_om_r/om_r
     &       /metrics_pmon_u/pmon_u /metrics_pnom_v/pnom_v
     &                              /metrics_grdscl/grdscl
      real rmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /mask_r/rmask /mask_p/pmask
     &       /mask_u/umask /mask_v/vmask
      integer xi_rho,eta_rho,xi_u,eta_v,ntime,ns
      parameter(ns=nratio/2+1)
      parameter(xi_rho=(Lm+2-ns)/nratio+1,
     &          eta_rho=(Mm-ns+2)/nratio+1,
     &  xi_u=xi_rho-1, eta_v=eta_rho-1, ntime=1)
      character file_nc*96
      character start_day*20
      integer i,j,k,i0,j0, lstr, lenstr
* error status return
      integer  iret
* netCDF id
      integer  ncid
* dimension ids
      integer  xi_rho_dim
      integer  xi_u_dim
      integer  eta_rho_dim
      integer  eta_v_dim
      integer  s_rho_dim
      integer  time_dim
* dimension lengths
      integer  xi_rho_len
      integer  xi_u_len
      integer  eta_rho_len
      integer  eta_v_len
      integer  s_rho_len
      integer  time_len
      parameter (time_len = NF_UNLIMITED)
* variable ids
      integer  depth_id
      integer  u_da_id
      integer  v_da_id
      integer  t_da_id
      integer  s_da_id
      integer  zeta_da_id
      integer  mask_da_id
      integer  lat_da_id
      integer  lon_da_id
* rank (number of dimensions) for each variable
      integer  depth_rank
      integer  u_da_rank
      integer  v_da_rank
      integer  t_da_rank
      integer  s_da_rank
      integer  mask_da_rank
      integer  lat_da_rank
      integer  lon_da_rank
      integer  zeta_da_rank
      parameter (depth_rank = 1)
      parameter (u_da_rank = 3)
      parameter (v_da_rank = 3)
      parameter (t_da_rank = 3)
      parameter (s_da_rank = 3)
      parameter (mask_da_rank = 3)
      parameter (lat_da_rank = 2)
      parameter (lon_da_rank = 2)
      parameter (zeta_da_rank = 2)
* variable shapes
      integer  depth_dims(depth_rank)
      integer  u_da_dims(u_da_rank)
      integer  v_da_dims(v_da_rank)
      integer  t_da_dims(t_da_rank)
      integer  s_da_dims(s_da_rank)
      integer  mask_da_dims(mask_da_rank)
      integer  lat_da_dims(lat_da_rank)
      integer  lon_da_dims(lon_da_rank)
      integer  zeta_da_dims(zeta_da_rank)
* attribute vectors
      real tt(xi_rho,eta_rho,NDAS)
      real uu(xi_u,eta_rho,NDAS)
      real vv(xi_rho,eta_v,NDAS)
      real zz(xi_rho,eta_rho)
      common /TTT/tt,uu,vv,zz
      real doubleval(1)
      integer nc_start(3)
      integer nc_count(3)
      integer nc_start2(2)
      integer nc_count2(2)
      lstr=lenstr(file_nc)
* enter define mode
      iret = nf_create(file_nc(1:lstr), NF_CLOBBER, ncid)
      call check_err(iret)
* define dimensions
      iret = nf_def_dim(ncid, 'xi_rho', xi_rho, xi_rho_dim)
      call check_err(iret)
      iret = nf_def_dim(ncid, 'xi_u', xi_u, xi_u_dim)
      call check_err(iret)
      iret = nf_def_dim(ncid, 'eta_rho', eta_rho, eta_rho_dim)
      call check_err(iret)
      iret = nf_def_dim(ncid, 'eta_v', eta_v, eta_v_dim)
      call check_err(iret)
      iret = nf_def_dim(ncid, 'ndas', ndas, s_rho_dim)
      call check_err(iret)
* define variables
      u_da_dims(3) =  s_rho_dim
      u_da_dims(2) =  eta_rho_dim
      u_da_dims(1) = xi_u_dim
      iret = nf_def_var(ncid, 'u_da', NF_DOUBLE, u_da_rank,
     &       u_da_dims, u_da_id)
      call check_err(iret)
      v_da_dims(3) =  s_rho_dim
      v_da_dims(2) =  eta_v_dim
      v_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 'v_da', NF_DOUBLE, v_da_rank,
     &       v_da_dims, v_da_id)
      call check_err(iret)
      t_da_dims(3) =  s_rho_dim
      t_da_dims(2) =  eta_rho_dim
      t_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 't_da', NF_DOUBLE, t_da_rank,
     &       t_da_dims, t_da_id)
      call check_err(iret)
      s_da_dims(3) =  s_rho_dim
      s_da_dims(2) =  eta_rho_dim
      s_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 's_da', NF_DOUBLE, s_da_rank,
     &       s_da_dims, s_da_id)
      call check_err(iret)
      mask_da_dims(3) =  s_rho_dim
      mask_da_dims(2) =  eta_rho_dim
      mask_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 'mask_da', NF_DOUBLE, mask_da_rank,
     &       mask_da_dims, mask_da_id)
      call check_err(iret)
      zeta_da_dims(2) =  eta_rho_dim
      zeta_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 'zeta_da', NF_DOUBLE, zeta_da_rank,
     &       zeta_da_dims, zeta_da_id)
      call check_err(iret)
      lat_da_dims(2) =  eta_rho_dim
      lat_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 'lat_da', NF_DOUBLE, lat_da_rank,
     &       lat_da_dims, lat_da_id)
      call check_err(iret)
      lon_da_dims(2) =  eta_rho_dim
      lon_da_dims(1) = xi_rho_dim
      iret = nf_def_var(ncid, 'lon_da', NF_DOUBLE, lon_da_rank,
     &       lon_da_dims, lon_da_id)
      call check_err(iret)
* assign attributes
      iret = nf_put_att_text(ncid, u_da_id, 'long_name', 1, 'u')
      call check_err(iret)
      doubleval(1) = 0.0
      call check_err(iret)
      iret = nf_put_att_text(ncid, v_da_id, 'long_name', 1, 'v')
      call check_err(iret)
      iret = nf_put_att_text(ncid, t_da_id, 'long_name', 4, 'temp')
      call check_err(iret)
      iret = nf_put_att_text(ncid, s_da_id, 'long_name', 4, 'salt')
      call check_err(iret)
      iret = nf_put_att_text(ncid, mask_da_id, 'long_name', 4, 'salt')
      call check_err(iret)
      iret = nf_put_att_text(ncid, zeta_da_id, 'long_name', 4, 'zeta')
      call check_err(iret)
      iret = nf_put_att_text(ncid, lat_da_id, 'long_name', 8, 
     &                                                       'latitude')
      call check_err(iret)
      iret = nf_put_att_text(ncid, lon_da_id, 'long_name', 9, 
     &                                                      'longitude')
      call check_err(iret)
      iret = nf_put_att_text(ncid, NF_GLOBAL, 'type', 11, 'ROMS SCB LR')
      call check_err(iret)
      iret = nf_put_att_text(ncid, NF_GLOBAL, 'data', 16, 'ROMS vert 
     &                                                          interp')
      call check_err(iret)
* leave define mode
      iret = nf_enddef(ncid)
      call check_err(iret)
      iret = nf_close(ncid)
      call check_err(iret)
      iret = NF_OPEN(file_nc(1:lstr),NF_WRITE,ncid)
      nc_start(1)=1
      nc_start(2)=1
      nc_start(3)=1
      nc_count(3)=ndas
      nc_count(2)=eta_rho
      nc_count(1)=xi_u
      i0=0
      j0=0
      do k=1,ndas
        do i=1,xi_u
          do j=1,eta_rho
            i0=(i-1)*nratio+2
            j0=(j-1)*nratio+1
            uu(i,j,k)=u_s(i0,j0,k)*umask_das(i0,j0,k)
          enddo
        enddo
      enddo
      i0=0
      j0=0
      do k=1,ndas
        do i=1,xi_rho
          do j=1,eta_v
            i0=(i-1)*nratio+1
            j0=(j-1)*nratio+2
            vv(i,j,k)=v_s(i0,j0,k)*vmask_das(i0,j0,k)
          enddo
        enddo
      enddo
      iret = NF_INQ_VARID(ncid, 'u_da',u_da_id)
      if (iret.ne.NF_NOERR) then
        print*, '   error inquiring u_da'
        stop
      endif
      iret = NF_PUT_VARA_DOUBLE(ncid, u_da_id,
     &                            nc_start,nc_count,uu)
      if (iret.ne.NF_NOERR) then
        print*, '   error write u_da'
        stop
      endif
      nc_count(3)=ndas
      nc_count(2)=eta_v
      nc_count(1)=xi_rho
      iret = NF_INQ_VARID(ncid, 'v_da',v_da_id)
      if (iret.ne.NF_NOERR) then
        print*, '   error inquiring v_da'
        stop
      endif
      iret = NF_PUT_VARA_DOUBLE(ncid, v_da_id,
     &                            nc_start,nc_count,vv)
      if (iret.ne.NF_NOERR) then
        print*, '   error write v_da'
        stop
      endif
      i0=0
      j0=0
      do k=1,ndas
        do i=1,xi_rho
          do j=1,eta_rho
            i0=(i-1)*nratio+1
            j0=(j-1)*nratio+1
            tt(i,j,k)=t_s(i0,j0,k,1)*rmask_das(i0,j0,k)
          enddo
        enddo
      enddo
      nc_count(3)=ndas
      nc_count(2)=eta_rho
      nc_count(1)=xi_rho
      iret = NF_INQ_VARID(ncid, 't_da',t_da_id)
      if (iret.ne.NF_NOERR) then
        print*, '   error inquiring t_da'
        stop
      endif
      iret = NF_PUT_VARA_DOUBLE(ncid, t_da_id,
     &                            nc_start,nc_count,tt)
      if (iret.ne.NF_NOERR) then
        print*, '   error write t_da'
        stop
      endif
      i0=0
      j0=0
      do k=1,ndas
        do i=1,xi_rho
          do j=1,eta_rho
            i0=(i-1)*nratio+1
            j0=(j-1)*nratio+1
            tt(i,j,k)=t_s(i0,j0,k,2)*rmask_das(i0,j0,k)
          enddo
        enddo
      enddo
      nc_count(3)=ndas
      nc_count(2)=eta_rho
      nc_count(1)=xi_rho
      iret = NF_INQ_VARID(ncid, 's_da',s_da_id)
      if (iret.ne.NF_NOERR) then
        print*, '   error inquiring s_da'
        stop
      endif
      iret = NF_PUT_VARA_DOUBLE(ncid, s_da_id,
     &                            nc_start,nc_count,tt)
      if (iret.ne.NF_NOERR) then
        print*, '   error write s_da'
        stop
      endif
      i0=0
      j0=0
      do k=1,ndas
        do i=1,xi_rho
          do j=1,eta_rho
            i0=(i-1)*nratio+1
            j0=(j-1)*nratio+1
            tt(i,j,k)=rmask_das(i0,j0,k)
          enddo
        enddo
      enddo
      nc_count(3)=ndas
      nc_count(2)=eta_rho
      nc_count(1)=xi_rho
      iret = NF_INQ_VARID(ncid, 'mask_da',mask_da_id)
      if (iret.ne.NF_NOERR) then
        print*, '   error inquiring mask_da'
        stop
      endif
      iret = NF_PUT_VARA_DOUBLE(ncid, mask_da_id,
     &                            nc_start,nc_count,tt)
      if (iret.ne.NF_NOERR) then
        print*, '   error write mask_da'
        stop
      endif
      i0=0
      j0=0
        do i=1,xi_rho
          do j=1,eta_rho
            i0=(i-1)*nratio+1
            j0=(j-1)*nratio+1
            zz(i,j)=zeta_s(i0,j0)*rmask_das(i0,j0,ndas)
          enddo
        enddo
      nc_start2(1)=1
      nc_start2(2)=1
      nc_count2(2)=eta_rho
      nc_count2(1)=xi_rho
      iret = NF_INQ_VARID(ncid, 'zeta_da',zeta_da_id)
      if (iret.ne.NF_NOERR) then
        print*, '   error inquiring zeta_da'
        stop
      endif
      iret = NF_PUT_VARA_DOUBLE(ncid, zeta_da_id,
     &                            nc_start2,nc_count2,zz)
      i0=0
      j0=0
        do i=1,xi_rho
          do j=1,eta_rho
            i0=(i-1)*nratio+1
            j0=(j-1)*nratio+1
            zz(i,j)=latr(i0,j0)
          enddo
        enddo
      nc_start2(1)=1
      nc_start2(2)=1
      nc_count2(2)=eta_rho
      nc_count2(1)=xi_rho
      iret = NF_INQ_VARID(ncid, 'lat_da',lat_da_id)
      if (iret.ne.NF_NOERR) then
        print*, '   error inquiring lat_da'
        stop
      endif
      iret = NF_PUT_VARA_DOUBLE(ncid, lat_da_id,
     &                            nc_start2,nc_count2,zz)
      i0=0
      j0=0
        do i=1,xi_rho
          do j=1,eta_rho
            i0=(i-1)*nratio+1
            j0=(j-1)*nratio+1
            zz(i,j)=lonr(i0,j0)
          enddo
        enddo
      nc_start2(1)=1
      nc_start2(2)=1
      nc_count2(2)=eta_rho
      nc_count2(1)=xi_rho
      iret = NF_INQ_VARID(ncid, 'lon_da',lon_da_id)
      if (iret.ne.NF_NOERR) then
        print*, '   error inquiring lon_da'
        stop
      endif
      iret = NF_PUT_VARA_DOUBLE(ncid, lon_da_id,
     &                            nc_start2,nc_count2,zz)
       iret = NF_CLOSE(ncid)
      end
      subroutine check_err(iret)
      integer iret
      include 'netcdf.inc'
      if (iret .ne. NF_NOERR) then
      print *, nf_strerror(iret)
      stop
      endif
      end
