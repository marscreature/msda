# Universal machine independent makefile for SCRUM/ROMS model 
#====================================================================
# Set machine dependent definitions and rules.

include Makedefs

# Configuration for SCRUM/ROMS model:
#====================================================================
# All source codes files are sorted into eight groups, separated by
# blanc lines:
#  1) main driving part;          5) vertical mixing schemes; 
#  2) 2D time stepping engine;    6) on-fly model diagnostic routines;
#  3) 3D time stepping engine;    7) netCDF I/O routines;
#  4) sea-water EOS routines;     8) model forcing routines;
#--------------------------------------------------------------------

 SRCS = main.F	check_switches1.F	check_switches2.F	 check_srcs.F\
	read_inp.F	setup_kwds.F	check_kwds.F\
	timers.F	init_scalars.F	init_arrays.F\
	set_scoord.F	setup_grid1.F	setup_grid2.F\
	ana_initial.F	analytical.F\
\
	das_init_arrays.F  das_init_scalars.F\
	das_setup1.F  das_setup2.F das_spline1d.F das_exchange.F\
	das_MessPass2D.F das_s2z.F das_z2s.F das_analysis.F\
	das_ana_bvar.F das_costb.F das_vec.F das_adj_costb.F\
	das_get_tmi.F  das_get_mc.F das_get_goes.F das_lbfgs.F das_innov_sst.F\
	das_cost_sst.F das_adj_csst.F das_ca2zero.F das_adj_zero.F\
	das_adj_zero_s.F das_read_inp.F das_matvec.F das_adj_matvec.F\
	das_get_corr.F das_varvec.F das_adj_varvec.F\
	das_rho_eos9.F das_fill9.F das_extrapolate9.F das_geos_uv_p.F das_get_bvar.F\
	das_get_ssh_ref.F rho_eos.F\
	das_zeta_p_smooth.F das_zeta_p_smooth2.F das_diag_pressure.F\
	das_adj_diag_p.F das_get_hfradar.F das_geo_ratio.F das_adj_geo_ratio.F\
	das_cost_reg_uv.F das_adj_cost_reg_uv.F\
\
	das_get_js1.F das_get_swot.F das_innov_js1.F das_innov_swot.F das_rho_eos_lin.F\
	das_diag_ssh.F das_cost_js1.F das_cost_swot.F das_cost_reg.F das_adj_cost_reg.F\
	das_adj_rho_eos.F das_adj_diag_ssh.F das_adj_cjs1.F das_adj_cswot.F\
	das_adj_diag_ssh_steric.F\
	das_adj_geos_uv_p.F das_psichi_uv.F das_adj_psichi_uv.F\
	das_cross_ts.F das_adj_cross_ts.F das_set_geo_ratio.F\
	das_zetah_smooth.F das_adj_zetah_smooth.F das_copy_zetah_smooth.F\
	das_adj_copy_zeteh_smooth.F das_adj_glider_sio.F das_cost_glider_sio.F\
	das_adj_glider_whoi.F das_cost_glider_whoi.F das_adj_moor.F das_cost_moor.F\
	das_pressue_smooth.F das_adj_pressure_smooth.F\
	das_copy_pressure_smooth.F das_adj_copy_pressure_smooth.F\
\
	das_get_insitu.F das_innov_sio.F das_cost_glider.F das_adj_cglider.F\
	das_innov_whoi.F das_innov_flight.F das_cost_flight.F das_adj_cflight.F\
	das_innov_ptsur.F das_innov_martn.F das_innov_moor.F\
	das_get_auv.F das_innov_calauv.F das_cost_calauv.F das_adj_cauv.F\
	das_innov_dorauv.F das_cost_dorauv.F das_adj_dauv.F\
	das_innov_hfradar.F das_cost_hfradar.F das_adj_chfradar.F\
	das_get_initial_lr.F das_interp_lr.F das_nf_fread_lr.F\
	das_add_lr.F das_add_lr_s.F das_ca2zero_s.F\
\
	das_copy_smooth.F das_innov_smooth.F das_diag_ssh_steric.F\
	das_copy_zeta_smooth.F das_zeta_smooth.F das_copy_uv_smooth.F das_innov_uv_smooth.F\
\
	set_depth.F	omega.F\
	wvlcty.F	checkdims.F	grid_stiffness.F\
\
	get_date.F	lenstr.F	closecdf.F\
	insert_node.F	nf_fread.F	get_grid.F	get_initial.F\
	put_global_atts.F def_grid.F wrt_grid.F\
	def_his.F def_rst.F wrt_rst.F das_def_rst.F das_wrt_rst.F\
\
	set_cycle.F\
\
	MPI_Setup.F	MessPass2D.F	MessPass3D.F	exchange.F
#
#	das_cost_ssh_lin.F das_adj_cssh_lin.F das_cost_sst_lin.F das_adj_csst_lin.F\
#	das_costb_lin.F das_adj_costb_lin.F\
 
#
#--------------------------------------------------------------------

 RCS = $(SRCS:.F=.f)

 OBJS = $(RCS:.f=.o)

 SBIN = ../../../bin/msda_l1_ss.exe

#
# Eecutable file.
# ========= =====
#
$(SBIN): $(OBJS)
	$(LDR) $(FFLAGS) $(LDFLAGS) -o a.out $(OBJS) $(LCDF) $(LMPI) 
	mv a.out $(SBIN)
#
# Everything
# ==========
all: tools depend $(SBIN)
#
# Auxiliary utility programs and List of Dependecies:
# ========= ======= ======== === ==== == ============
#
  TOOLS = mpc cross_matrix cppcheck srcscheck checkkwds partit ncjoin ncrename

tools: $(TOOLS)

mpc: mpc.F
	$(CPP) -P mpc.F > mpc.f
	$(LDR) $(FFLAGS) -o mpc mpc.f
cross_matrix: cross_matrix.o
	$(LDR) $(FFLAGS) $(LDFLAGS) -o cross_matrix cross_matrix.o
cppcheck: cppcheck.o
	$(LDR) $(FFLAGS) $(LDFLAGS) -o cppcheck cppcheck.o
srcscheck: srcscheck.o
	$(LDR) $(FFLAGS) $(LDFLAGS) -o srcscheck srcscheck.o
checkkwds: checkkwds.o
	$(LDR) $(FFLAGS) $(LDFLAGS) -o checkkwds checkkwds.o
checkdefs: check_switches1.F setup_kwds.F

check_switches1.F: cppcheck cppdefs.h
	./cppcheck
check_srcs.F: srcscheck Makefile
	./srcscheck
setup_kwds.F: checkkwds read_inp.F
	./checkkwds
partit: partit.o insert_node.o lenstr.o
	$(LDR) $(FFLAGS) -o partit partit.F insert_node.o lenstr.o $(LCDF)
ncjoin: ncjoin.o lenstr.o
	$(LDR) $(FFLAGS) -o ncjoin ncjoin.o lenstr.o $(LCDF)
ncrename: ncrename.o lenstr.o
	$(LDR) $(FFLAGS) -o ncrename ncrename.o lenstr.o $(LCDF)

depend: checkdefs cross_matrix
	./cross_matrix *.F
#
# Target to create tar file.
# ====== == ====== === =====
#
tarfile:
	tar cvf roms.tar Make* *.h *.F *.README README.*  *.in *.in.*
#
# Cleaning:
# =========
#
rmtools:
	/bin/rm -f $(TOOLS)
clean:
	/bin/rm -f core core.* *.o *.i *.s *.f *.trace

clobber: clean
	/bin/rm -rf $(SBIN) $(TOOLS) ./rii_files

#
# Special treatment for barrier function:
# THERE SHALL BE NO OPTIMIZATION HERE!!!!
#
my_barrier.o: my_barrier.f
	$(CFT) -c -O0 my_barrier.f
#
# Include automatically generated dependency list:
#
include Make.depend

