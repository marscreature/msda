#include "cppdefs.h"

      subroutine das_innov_hfradar (tile)
      implicit none
      integer tile,trd, omp_get_thread_num
# include "param.h"
# include "das_param.h"
# include "das_private_scratch.h"
# include "compute_tile_bounds.h"
      trd=omp_get_thread_num()
      call das_innov_hfradar_tile (Istr,Iend,Jstr,Jend,
     &        A2d(1,1,trd),A2d(1,2,trd),A2d(1,3,trd),A2d(1,4,trd))
      return
      end

      subroutine das_innov_hfradar_tile 
     &               (Istr,Iend,Jstr,Jend,ur,vr,eerr,rmaskr)
!
!--------------------------------------------------------------------
!  Set initial conditions for momentum and tracer variables using
!  analytical expressions.
!--------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k, itrc
      real cfu,cfv,crt,hfumask, hfvmask
      real srad,srad0,rr,rc, lonc,latc,ro
      real uu,vv,err,dis
      integer count,num,n0
# include "param.h"
# include "das_param.h"
# include "scalars.h"
# include "grid.h"
# include "das_ocean.h"
# include "das_covar.h"
# include "das_innov.h"
!
      real ur(PRIVATE_2D_SCRATCH_ARRAY),                                                                                             
     &     vr(PRIVATE_2D_SCRATCH_ARRAY),
     &     eerr(PRIVATE_2D_SCRATCH_ARRAY),
     &     rmaskr(PRIVATE_2D_SCRATCH_ARRAY)
!
!
# include "compute_auxiliary_bounds.h"
!
      ro =      (lonr(Lm/2,Mm/2)-lonr(Lm/2+1,Mm/2+1))
     &          *(lonr(Lm/2,Mm/2)-lonr(Lm/2+1,Mm/2+1))
     &          +(latr(Lm/2,Mm/2)-latr(Lm/2+1,Mm/2+1))
     &          *(latr(Lm/2,Mm/2)-latr(Lm/2+1,Mm/2+1))

      srad=4.0*ro
      srad0=0.25*ro     ! larger, smoother
      crt=0.35
!
!     ... form superobs
!
! u
!
      do j=Jstr,Jend   
        do i=Istr,IendR
          u_hf(i,j)=0.0
          hf_umask(i,j)=0.0
        enddo
      enddo
!  
! v                  
      do j=Jstr,JendR 
        do i=Istr,Iend 
          v_hf(i,j)=0.0 
          hf_vmask(i,j) = 0.0
        enddo
      enddo   
      if (num_hfradar .le. 1 ) goto 100
!
      do j=Jstr-1,JendR
        do i=Istr-1,IendR
          if (rmask_das(i,j,NDAS) .gt. 0.5) then
            count=0
            uu=0.
            vv=0.
            rc=0.0
            do num=1,num_hfradar
              rr= (lonr(i,j)-lon_hf(num))
     &           *(lonr(i,j)-lon_hf(num))
     &           +(latr(i,j)-lat_hf(num))
     &           *(latr(i,j)-lat_hf(num))
              if (rr .lt. srad ) then
                count=count+1
                dis=max(rr,srad0)
                uu=uu+u_hf_raw(num)/dis
                vv=vv+v_hf_raw(num)/dis
                rc=rc+1.0/dis
              endif
            enddo
            if (count .ge. 1 ) then
!
! rotate to ROMS curvelinear
! u_r = u.*cosa + v.*sina;
! v_r = v.*cosa - u.*sina;
!
              ur(i,j)=(uu*cos(angler(i,j))+vv*sin(angler(i,j)))/rc
              vr(i,j)=(vv*cos(angler(i,j))-uu*sin(angler(i,j)))/rc
              rmaskr(i,j)=1.0
            else
              ur(i,j)=0.0
              vr(i,j)=0.0
              rmaskr(i,j)=0.0
            endif
          else
            ur(i,j)=0.0
            vr(i,j)=0.0
            rmaskr(i,j)=0.0
          endif
        enddo
      enddo
!
!     ... QC and INNOVATION
!
!u
      do j=Jstr,Jend
        do i=Istr,IendR
          cfu=0.5*(ur(i-1,j)+ur(i,j))-u_das(i,j,NDAS)
          hf_umask(i,j) = rmaskr(i-1,j)*rmaskr(i,j)
     &                  * pmask_das(i,j,NDAS)*pmask_das(i,j+1,NDAS)  ! exclude u adjacent to coast
          if (abs(cfu) .gt. crt ) hf_umask(i,j)=0.0
          u_hf(i,j)=cfu * hf_umask(i,j)
        enddo
      enddo
!
!
! v
      do j=Jstr,JendR
        do i=Istr,Iend
          cfv=0.5*(vr(i,j-1)+vr(i,j))-v_das(i,j,NDAS) 
          hf_vmask(i,j) = rmaskr(i,j-1)*rmaskr(i,j)
     &                  * pmask_das(i,j,NDAS)*pmask_das(i+1,j,NDAS)  ! exclude v adjacent to coast
          if ( abs(cfv) .gt. crt) hf_vmask(i,j)=0.0
          v_hf(i,j)=cfv * hf_vmask(i,j)
        enddo
      enddo
  100 continue
!
!6KM  6KM --------------------
!
      ro =      (lonr(Lm/2,Mm/2)-lonr(Lm/2+1,Mm/2+1))
     &          *(lonr(Lm/2,Mm/2)-lonr(Lm/2+1,Mm/2+1))
     &          +(latr(Lm/2,Mm/2)-latr(Lm/2+1,Mm/2+1))
     &          *(latr(Lm/2,Mm/2)-latr(Lm/2+1,Mm/2+1))

      srad=6.0*ro
      srad0=0.1*srad     ! larger, smoother
      crt=0.25
!
!     ... form superobs
!
! u
      do j=Jstr,Jend
        do i=Istr,IendR
          u_hf6(i,j)=0.0
          hf_uierr6(i,j)=0.0
          hf_umask6(i,j)=0.0
        enddo
      enddo
!
! v
      do j=Jstr,JendR
        do i=Istr,Iend
          v_hf6(i,j)=0.0
          hf_vierr6(i,j) = 0.0
          hf_vmask6(i,j) = 0.0
        enddo
      enddo
      if (num_hfradar6 .le. 1 ) goto 200
!
      do j=Jstr-1,JendR
        do i=Istr-1,IendR
          if (rmask_das(i,j,NDAS) .gt. 0.5) then
            count=0
            uu=0.
            vv=0.
            err=0.
            rc=0.0
            do num=1,num_hfradar6
              rr= (lonr(i,j)-lon_hf6(num))
     &           *(lonr(i,j)-lon_hf6(num))
     &           +(latr(i,j)-lat_hf6(num))
     &           *(latr(i,j)-lat_hf6(num))
              if (rr .lt. srad ) then
                count=count+1
                dis=max(rr,srad0)
                uu=uu+u_hf_raw6(num)/dis
                vv=vv+v_hf_raw6(num)/dis
                err=err+amperr_hf6(num)/dis
                rc=rc+1.0/dis
              endif
            enddo
            if (count .ge. 1 ) then
!
! rotate to ROMS curvelinear
! u_r = u.*cosa + v.*sina;
! v_r = v.*cosa - u.*sina;
!
              ur(i,j)=(uu*cos(angler(i,j))+vv*sin(angler(i,j)))/rc
              vr(i,j)=(vv*cos(angler(i,j))-uu*sin(angler(i,j)))/rc
              eerr(i,j)=err/rc
              rmaskr(i,j)=1.0
            else
              ur(i,j)=0.0
              vr(i,j)=0.0
              eerr(i,j)=0.0
              rmaskr(i,j)=0.0
            endif
          else
            ur(i,j)=0.0
            vr(i,j)=0.0
            eerr(i,j)=0.0
            rmaskr(i,j)=0.0
          endif
        enddo
      enddo
!
!     ... QC and INNOVATION
!     
!u      
      do j=Jstr,Jend
        do i=Istr,IendR
          cfu=0.5*(ur(i-1,j)+ur(i,j))-u_das(i,j,NDAS)
          hfumask = rmaskr(i-1,j)*rmaskr(i,j)
     &                  * pmask_das(i,j,NDAS)*pmask_das(i,j+1,NDAS)  ! exclude u adjacent to coast
          if (abs(cfu) .gt. crt ) hfumask=0.0
          u_hf6(i,j)=cfu * hfumask
          err=0.5*(eerr(i-1,j)+eerr(i,j))   !!!!!NOTE: depend on amplification error
          err=max(1.0,err/0.35)
          hf_uierr6(i,j)=hfumask * hf_ouv6/(err*err)
          hf_umask6(i,j)= hfumask
        enddo
      enddo 
!           
!           
! v         
      do j=Jstr,JendR
        do i=Istr,Iend
          cfv=0.5*(vr(i,j-1)+vr(i,j))-v_das(i,j,NDAS)
          hfvmask = rmaskr(i,j-1)*rmaskr(i,j)
     &                  * pmask_das(i,j,NDAS)*pmask_das(i+1,j,NDAS)  ! exclude v adjacent to coast
          if ( abs(cfv) .gt. crt) hfvmask=0.0
          v_hf6(i,j)= cfv * hfvmask
          err=0.5*(eerr(i,j-1)+eerr(i,j))
          err=max(1.0,err/0.35)
          hf_vierr6(i,j)=hfvmask * hf_ouv6/(err*err)
          hf_vmask6(i,j) = hfvmask
        enddo   
      enddo     
                
  200 continue
!
      return
      end
