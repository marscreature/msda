      subroutine diag (tile)
      implicit none
      integer tile, trd, omp_get_thread_num
      integer  LLmH,  MMmH, NnH
      parameter (LLmH=606,  MMmH=438, NnH=66)
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLmH+2-nhalf)/nratio-1,
     &           MMm_lr=(MMmH+2-nhalf)/nratio-1)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLm_lr,  MMm=MMm_lr,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NSA, N2d,N3d, size_XI,size_ETA
      parameter (NSA=16)
      parameter (size_XI=7+(Lm+NSUB_X-1)/NSUB_X,
     &           size_ETA=7+(Mm+NSUB_E-1)/NSUB_E)
      integer se,sse, sz,ssz
      parameter (sse=size_ETA/Np, ssz=Np/size_ETA)
      parameter (se=sse/(sse+ssz),   sz=1-se)
      parameter (N2d=size_XI*(se*size_ETA+sz*Np),
     &                  N3d=size_XI*size_ETA*Np)
      real A2d(N2d,NSA,0:0), A3d(N3d,6,0:0)
      common /private_scratch/ A2d,A3d
      integer chunk_size_X,margin_X,chunk_size_E,margin_E
      parameter (chunk_size_X=(Lm+NSUB_X-1)/NSUB_X,
     &                     margin_X=(NSUB_X*chunk_size_X-Lm)/2,
     &           chunk_size_E=(Mm+NSUB_E-1)/NSUB_E,
     &                     margin_E=(NSUB_E*chunk_size_E-Mm)/2)
      integer Istr,Iend,Jstr,Jend, i_X,j_E
        j_E=tile/NSUB_X
        i_X=tile-j_E*NSUB_X
        Istr=1+i_X*chunk_size_X-margin_X
        Iend=Istr+chunk_size_X-1
        Istr=max(Istr,1)
        Iend=min(Iend,Lm)
        Jstr=1+j_E*chunk_size_E-margin_E
        Jend=Jstr+chunk_size_E-1
        Jstr=max(Jstr,1)
        Jend=min(Jend,Mm)
      trd=omp_get_thread_num()
      call diag_tile (Istr,Iend,Jstr,Jend, A2d(1,1,trd),A2d(1,2,trd))
      return
      end
      subroutine diag_tile (Istr,Iend,Jstr,Jend, ke2d,pe2d)
      implicit none
      integer Istr,Iend,Jstr,Jend,    i,j,k, NSUB, iocheck,
     &                                trd, omp_get_thread_num
      integer  LLmH,  MMmH, NnH
      parameter (LLmH=606,  MMmH=438, NnH=66)
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLmH+2-nhalf)/nratio-1,
     &           MMm_lr=(MMmH+2-nhalf)/nratio-1)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLm_lr,  MMm=MMm_lr,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real ke2d(Istr-2:Iend+2,Jstr-2:Jend+2),
     &     pe2d(Istr-2:Iend+2,Jstr-2:Jend+2)
      real*16 cff, my_avgke, my_avgpe, my_volume
      character echar*8
      real h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hinv(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real f(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fomn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_h/h /grid_hinv/hinv /grid_f/f /grid_fomn/fomn
      real angler(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_angler/angler
      real latr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real lonr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_latr/latr /grid_lonr/lonr
      real pm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_pm/pm    /metrics_pn/pn
     &       /metrics_omu/om_u /metrics_on_u/on_u
     &       /metrics_omv/om_v /metrics_on_v/on_v
      real dmde(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real dndx(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_dmde/dmde    /metrics_dndx/dndx
      real on_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmon_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pnom_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real grdscl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_on_p/on_p /metrics_om_p/om_p
     &       /metrics_on_r/on_r /metrics_om_r/om_r
     &       /metrics_pmon_u/pmon_u /metrics_pnom_v/pnom_v
     &                              /metrics_grdscl/grdscl
      real rmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /mask_r/rmask /mask_p/pmask
     &       /mask_u/umask /mask_v/vmask
      real u(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real v(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real t(0:Lm+1+padd_X,0:Mm+1+padd_E,N,NT)
      common /ocean_u/u /ocean_v/v /ocean_t/t
      real Hz(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real Hz_bak(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real z_r(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real z_w(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      real Huon(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real Hvom(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real W(0:Lm+1+padd_X,0:Mm+1+padd_E,0:N)
      common /grid_Hz/Hz    /grid_zr/z_r  /grid_W/W
     &  /grid_Hz_bak/Hz_bak /grid_zw/z_w  /grid_Huon/Huon
     &                                    /grid_Hvom/Hvom
      real rho(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real rhop0(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      common /ocean_rho/rho /ocean_rhop0/rhop0
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      real  x_sponge,   v_sponge
       real  tauT_in, tauT_out, tauM_in, tauM_out
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &                      , x_sponge,   v_sponge
     &                      , tauT_in, tauT_out, tauM_in, tauM_out
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      integer ncheck, nparam
      parameter (ncheck=16, nparam=4)
      integer icheck, check_point(ncheck)
      character*60 check_line, etalon_line(ncheck)
      real    A0(nparam), A1(nparam)
      integer P0(nparam), P1(nparam)
      do icheck=1,ncheck
        check_point(icheck)=-1
        etalon_line(icheck)=
     &    '0.000000000E+00 0.0000000E+00 0.0000000E+00 0.0000000E+00'
      enddo
      check_point(1)=2
      etalon_line(1)=
     &    '2.240336721E-07 6.4145828E+02 6.4145828E+02 6.9020968E+15'
      check_point(2)=4
      etalon_line(2)=
     &    '7.840386262E-07 6.4145829E+02 6.4145829E+02 6.9020968E+15'
      check_point(3)=8
      etalon_line(3)=
     &    '2.191832264E-06 6.4145856E+02 6.4145856E+02 6.9020968E+15'
      check_point(4)=16
      etalon_line(4)=
     &    '3.281426815E-06 6.4145867E+02 6.4145867E+02 6.9020968E+15'
      check_point(5)=32
      etalon_line(5)=
     &    '1.452544793E-06 6.4145851E+02 6.4145851E+02 6.9020968E+15'
      check_point(6)=64
      etalon_line(6)=
     &    '2.507970799E-06 6.4145863E+02 6.4145864E+02 6.9020968E+15'
      check_point(7)=128
      etalon_line(7)=
     &    '3.276514285E-06 6.4145879E+02 6.4145880E+02 6.9020968E+15'
      check_point(8)=196
      etalon_line(8)=
     &    '3.907978835E-06 6.4145891E+02 6.4145892E+02 6.9020968E+15'
      check_point(9)=256
      etalon_line(9)=
     &    '4.767773386E-06 6.4145903E+02 6.4145903E+02 6.9020968E+15'
      if (mod(iic-1,ninfo).eq.0) then
        do j=Jstr,Jend
          do i=Istr,Iend
            ke2d(i,j)=0.
            pe2d(i,j)=0.5*g*z_w(i,j,N)*z_w(i,j,N)
          enddo
          cff=g/rho0
          do k=N,1,-1
            do i=Istr,Iend
             ke2d(i,j)=ke2d(i,j)+Hz(i,j,k)*0.25*(
     &                               u(i  ,j,k)*u(i,j,k)+
     &                               u(i+1,j,k)*u(i+1,j,k)+
     &                               v(i,j  ,k)*v(i,j  ,k)+
     &                               v(i,j+1,k)*v(i,j+1,k))
             pe2d(i,j)=pe2d(i,j)+cff*Hz(i,j,k)*rho(i,j,k)
     &                                       *(z_r(i,j,k)-z_w(i,j,0))
            enddo
          enddo
        enddo
        do i=Istr,Iend
          pe2d(i,Jend+1)=0.D0
          pe2d(i,Jstr-1)=0.D0
          ke2d(i,Jstr-1)=0.D0
        enddo
        do j=Jstr,Jend
          do i=Istr,Iend
            cff=1./(pm(i,j)*pn(i,j))
            pe2d(i,Jend+1)=pe2d(i,Jend+1)+cff*(z_w(i,j,N)-z_w(i,j,0))
            pe2d(i,Jstr-1)=pe2d(i,Jstr-1)+cff*pe2d(i,j)
            ke2d(i,Jstr-1)=ke2d(i,Jstr-1)+cff*ke2d(i,j)
          enddo
        enddo
        my_volume=0.
        my_avgpe=0.
        my_avgke=0.
        do i=Istr,Iend
          my_volume=my_volume+pe2d(i,Jend+1)
          my_avgpe =my_avgpe +pe2d(i,Jstr-1)
          my_avgke =my_avgke +ke2d(i,Jstr-1)
        enddo
        if (Iend-Istr+Jend-Jstr.eq.Lm+Mm-2) then
          NSUB=1
        else
          NSUB=NSUB_X*NSUB_E
        endif
C$OMP CRITICAL (diag_cr_rgn)
          if (tile_count.eq.0) then
            volume=0.q0
            avgke= 0.q0
            avgpe= 0.q0
          endif
          volume=volume+my_volume
          avgke =avgke +my_avgke
          avgpe =avgpe +my_avgpe
          tile_count=tile_count+1
          if (tile_count.eq.NSUB) then
            tile_count=0
              avgke=avgke/volume
              avgpe=avgpe/volume
              avgkp=avgke+avgpe
              if (first_time.eq.0) then
                first_time=1
                write(stdout,2) 'STEP','time[DAYS]','KINETIC_ENRG',
     &                  'POTEN_ENRG','TOTAL_ENRG','NET_VOLUME','trd'
   2            format(1x,A4,3x,A10,1x,A12,4x,A10,4x,A10,4x,A10,3x,A3)
              endif
              trd=omp_get_thread_num()
              write(stdout,3)iic-1,tdays,avgke,avgpe,avgkp,volume,trd
   3          format(I6, F12.5, 1PE16.9, 3(1PE14.7), I3)
              write(echar,'(1PE8.1)') avgkp
              do i=1,8
               if (echar(i:i).eq.'N' .or. echar(i:i).eq.'n'
     &                      .or. echar(i:i).eq.'*') may_day_flag=1
              enddo
              do icheck=1,ncheck
                if (iic-1.eq.check_point(icheck)) then
                  write(check_line,'(1PE15.9,3(1PE14.7))',
     &                iostat=iocheck) avgke,avgpe, avgkp,volume
                  if (check_line .eq. etalon_line(icheck)) then
                    write(stdout,*) 'PASSED_ETALON_CHECK'
                  else
                    read(check_line         ,4,iostat=iocheck)
     &                                   (A1(i), P1(i), i=1,nparam)
                    read(etalon_line(icheck),4,iostat=iocheck)
     &                                   (A0(i), P0(i), i=1,nparam)
   4                format (F11.9,1x,I3,3(F10.7,1x,I3))
                    do i=1,nparam
                      A1(i)=A1(i)-A0(i)*10.**float(P0(i)-P1(i))
                    enddo
                    write(check_line, '(F18.9,3F14.7)',
     &                          iostat=iocheck) (A1(i), i=1,nparam)
                    j=0
                    do i=2,60
                      if (check_line(i-1:i).eq.'0.') then
                        check_line(i:i)=':'
                        j=1
                      elseif (j.eq.1.and.check_line(i:i).eq.'0') then
                        check_line(i:i)='.'
                      else
                        j=0
                      endif
                    enddo
                    write(stdout,'(1x,2A/1x,A)') 'difference:',
     &               check_line, 'ETALON_CHECK: SOMETHING_IS_WRONG'
                  endif
                endif
              enddo
            if (ninfo.eq.1 .and. iic.gt.   8) ninfo=2
            if (ninfo.eq.2  .and. iic.gt. 16) ninfo=4
          endif
C$OMP END CRITICAL (diag_cr_rgn)
      endif
      return
      end
