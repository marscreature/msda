      subroutine das_adj_geos_uv_p (tile)
      implicit none
      integer tile,trd, omp_get_thread_num
      integer  LLmH,  MMmH, NnH
      parameter (LLmH=606,  MMmH=438, NnH=66)
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLmH+2-nhalf)/nratio-1,
     &           MMm_lr=(MMmH+2-nhalf)/nratio-1)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLm_lr,  MMm=MMm_lr,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+1)*(Mm+1)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_prf
      PARAMETER( max_prf=NDAS)
      integer max_sio, max_whoi, max_flight,
     &        max_hfradar,max_hfradar6,max_js1
      PARAMETER(max_sio=100,max_whoi=100,max_flight=30000,
     &         max_hfradar=39000,max_hfradar6=40000,max_js1=8000)
      integer max_ptsur
      PARAMETER(max_ptsur=100)
      integer max_martn
      PARAMETER(max_martn=100)
      integer max_moor
      PARAMETER(max_moor=1500)
      real sio_ot_raw,sio_os_raw,whoi_ot_raw,whoi_os_raw,ptsur_ot,
     &                                                         ptsur_os,
     &                   martn_ot,martn_os,moor_ot_raw,moor_os_raw,
     &                   hf_ouv,hf_ouv6,js1_ossh,swot_ossh,hcmin,
     &                                                         sats_osss
      PARAMETER( sio_ot_raw=1.0/(1.5*1.5),sio_os_raw=1.0/(0.21*0.21),
     &        whoi_ot_raw=1.0/(1.2*1.2),whoi_os_raw=1.0/(0.21*0.21),
     &        moor_ot_raw=1.0/(0.6*0.6),moor_os_raw=1.0/(0.06*0.06),
     &        ptsur_ot=1.0/(1.00*1.00),ptsur_os=1.0/(0.15*0.15),
     &        martn_ot=1.0/(1.00*1.00),martn_os=1.0/(0.15*0.15),
     &        js1_ossh=1.0/(0.03*0.03), swot_ossh=1.0/(0.06*0.06),
     &        hcmin=200.0, sats_osss=1.0/(0.10*0.10),
     &        hf_ouv=1.0/(0.09*0.09),hf_ouv6=1.0/(0.10*0.10)
     &         )
      integer max_prf_cal,max_cal
      PARAMETER(max_prf_cal=NDAS,max_cal=100)
      integer max_prf_dor,max_dor
      PARAMETER(max_prf_dor=NDAS,max_dor=180)
      real cal_ot,cal_os,dor_ot,dor_os
      PARAMETER( cal_ot=1.0/(1.5*1.5),
     &           cal_os=1.0/(0.25*0.25),
     &           dor_ot=1.0/(1.5*1.5),
     &           dor_os=1.0/(0.25*0.25)
     &         )
      real geo_ratio, cross_tsp
      PARAMETER( geo_ratio = 1.0, cross_tsp=0.)
      integer sm_rad
      PARAMETER( sm_rad=4)
      integer Local_len
      PARAMETER( Local_len=32)
      real sz_rad
      PARAMETER( sz_rad=4)
      integer sz_rad_len
      PARAMETER( sz_rad_len=15)
      real regp, reguv
      PARAMETER( regp=0.4/(10.*10.) )
      PARAMETER( reguv=0.5/(0.6*0.6)  )
      integer NSA, N2d,N3d, size_XI,size_ETA
      parameter (NSA=8)
      parameter (size_XI=7+(Lm+NSUB_X-1)/NSUB_X,
     &           size_ETA=7+(Mm+NSUB_E-1)/NSUB_E)
      integer se,sse, sz,ssz
      parameter (sse=size_ETA/NDASp, ssz=NDASp/size_ETA)
      parameter (se=sse/(sse+ssz),   sz=1-se)
      parameter (N2d=size_XI*(se*size_ETA+sz*NDASp),
     &                   N3d=size_XI*size_ETA*NDASp)
      real A2d(N2d,NSA,0:NPP-1), A3d(N3d,2,0:NPP-1)
      common /das_private_scratch/A2d,A3d
      integer chunk_size_X,margin_X,chunk_size_E,margin_E
      parameter (chunk_size_X=(Lm+NSUB_X-1)/NSUB_X,
     &                     margin_X=(NSUB_X*chunk_size_X-Lm)/2,
     &           chunk_size_E=(Mm+NSUB_E-1)/NSUB_E,
     &                     margin_E=(NSUB_E*chunk_size_E-Mm)/2)
      integer Istr,Iend,Jstr,Jend, i_X,j_E
        j_E=tile/NSUB_X
        i_X=tile-j_E*NSUB_X
        Istr=1+i_X*chunk_size_X-margin_X
        Iend=Istr+chunk_size_X-1
        Istr=max(Istr,1)
        Iend=min(Iend,Lm)
        Jstr=1+j_E*chunk_size_E-margin_E
        Jend=Jstr+chunk_size_E-1
        Jstr=max(Jstr,1)
        Jend=min(Jend,Mm)
      trd=omp_get_thread_num()
      call das_adj_geos_uv_p_tile (Istr,Iend,Jstr,Jend,
     &                     A2d(1,1,trd),A2d(1,2,trd),
     &                     A3d(1,1,trd))
      return
      end
      subroutine das_adj_geos_uv_p_tile (Istr,Iend,Jstr,Jend,
     &               ug_adj,vg_adj,ps_adj)
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k
      real cff, ff0, crit_lat, crit, rho02, fcr
      integer  LLmH,  MMmH, NnH
      parameter (LLmH=606,  MMmH=438, NnH=66)
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLmH+2-nhalf)/nratio-1,
     &           MMm_lr=(MMmH+2-nhalf)/nratio-1)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLm_lr,  MMm=MMm_lr,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer NDAS,NDASp
      parameter (NDAS=32)
      parameter (NDASp=NDAS+1)
      integer NDIM
      PARAMETER(NDIM=
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +NDAS*(Lm+1)*(Mm+1)
     &        +NDAS*(Lm+2)*(Mm+2)
     &        +(Lm+2)*(Mm+2)
     &                                  )
      integer MSAVE,NXM,NWORK
      PARAMETER(MSAVE=5,NXM=NDIM*MSAVE,NWORK=NDIM+2*MSAVE)
      integer max_prf
      PARAMETER( max_prf=NDAS)
      integer max_sio, max_whoi, max_flight,
     &        max_hfradar,max_hfradar6,max_js1
      PARAMETER(max_sio=100,max_whoi=100,max_flight=30000,
     &         max_hfradar=39000,max_hfradar6=40000,max_js1=8000)
      integer max_ptsur
      PARAMETER(max_ptsur=100)
      integer max_martn
      PARAMETER(max_martn=100)
      integer max_moor
      PARAMETER(max_moor=1500)
      real sio_ot_raw,sio_os_raw,whoi_ot_raw,whoi_os_raw,ptsur_ot,
     &                                                         ptsur_os,
     &                   martn_ot,martn_os,moor_ot_raw,moor_os_raw,
     &                   hf_ouv,hf_ouv6,js1_ossh,swot_ossh,hcmin,
     &                                                         sats_osss
      PARAMETER( sio_ot_raw=1.0/(1.5*1.5),sio_os_raw=1.0/(0.21*0.21),
     &        whoi_ot_raw=1.0/(1.2*1.2),whoi_os_raw=1.0/(0.21*0.21),
     &        moor_ot_raw=1.0/(0.6*0.6),moor_os_raw=1.0/(0.06*0.06),
     &        ptsur_ot=1.0/(1.00*1.00),ptsur_os=1.0/(0.15*0.15),
     &        martn_ot=1.0/(1.00*1.00),martn_os=1.0/(0.15*0.15),
     &        js1_ossh=1.0/(0.03*0.03), swot_ossh=1.0/(0.06*0.06),
     &        hcmin=200.0, sats_osss=1.0/(0.10*0.10),
     &        hf_ouv=1.0/(0.09*0.09),hf_ouv6=1.0/(0.10*0.10)
     &         )
      integer max_prf_cal,max_cal
      PARAMETER(max_prf_cal=NDAS,max_cal=100)
      integer max_prf_dor,max_dor
      PARAMETER(max_prf_dor=NDAS,max_dor=180)
      real cal_ot,cal_os,dor_ot,dor_os
      PARAMETER( cal_ot=1.0/(1.5*1.5),
     &           cal_os=1.0/(0.25*0.25),
     &           dor_ot=1.0/(1.5*1.5),
     &           dor_os=1.0/(0.25*0.25)
     &         )
      real geo_ratio, cross_tsp
      PARAMETER( geo_ratio = 1.0, cross_tsp=0.)
      integer sm_rad
      PARAMETER( sm_rad=4)
      integer Local_len
      PARAMETER( Local_len=32)
      real sz_rad
      PARAMETER( sz_rad=4)
      integer sz_rad_len
      PARAMETER( sz_rad_len=15)
      real regp, reguv
      PARAMETER( regp=0.4/(10.*10.) )
      PARAMETER( reguv=0.5/(0.6*0.6)  )
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      real  x_sponge,   v_sponge
       real  tauT_in, tauT_out, tauM_in, tauM_out
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &                      , x_sponge,   v_sponge
     &                      , tauT_in, tauT_out, tauM_in, tauM_out
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      real h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real hinv(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real f(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real fomn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_h/h /grid_hinv/hinv /grid_f/f /grid_fomn/fomn
      real angler(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_angler/angler
      real latr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real lonr(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /grid_latr/latr /grid_lonr/lonr
      real pm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pn(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_pm/pm    /metrics_pn/pn
     &       /metrics_omu/om_u /metrics_on_u/on_u
     &       /metrics_omv/om_v /metrics_on_v/on_v
      real dmde(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real dndx(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_dmde/dmde    /metrics_dndx/dndx
      real on_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real on_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmon_u(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_p(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real om_r(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pnom_v(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real grdscl(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /metrics_on_p/on_p /metrics_om_p/om_p
     &       /metrics_on_r/on_r /metrics_om_r/om_r
     &       /metrics_pmon_u/pmon_u /metrics_pnom_v/pnom_v
     &                              /metrics_grdscl/grdscl
      real rmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real pmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real umask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vmask(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /mask_r/rmask /mask_p/pmask
     &       /mask_u/umask /mask_v/vmask
      real zeta_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_das/u_das /ocean_v_das/v_das
     &    /ocean_psi_das/psi_das /ocean_chi_das/chi_das
     &    /ocean_t_das/t_das /ocean_zeta_das/zeta_das
      real zeta_s(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real zeta_h(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      real t_w(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s/u_s /ocean_v_s/v_s
     &      /ocean_psi_s/psi_s /ocean_chi_s/chi_s
     &      /ocean_rho_s/rho_s /ocean_p_s/p_s
     &     /ocean_t_s/t_s /ocean_zeta_s/zeta_s
     &     /ocean_zeta_h/zeta_h /ocean_t_w/t_w
      real zeta_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real psi_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common
     &     /ocean_psi_adj/psi_adj /ocean_chi_adj/chi_adj
     &     /ocean_t_adj/t_adj /ocean_zeta_adj/zeta_adj
      real zeta_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real zeta_h_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real u_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real v_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real rho_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real t_s_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      real t_w_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_u_s_adj/u_s_adj /ocean_v_s_adj/v_s_adj
     &   /ocean_psi_s_adj/psi_s_adj /ocean_chi_s_adj/chi_s_adj
     &     /ocean_rho_s_adj/rho_s_adj /ocean_p_s_adj/p_s_adj
     &     /ocean_t_s_adj/t_s_adj /ocean_zeta_s_adj/zeta_s_adj
     &     /ocean_zeta_h_adj/zeta_h_adj /ocean_t_w_adj/t_w_adj
      real rmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real pmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real umask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real vmask_das(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /mask_r_das/rmask_das /mask_p_das/pmask_das
     &       /mask_u_das/umask_das /mask_v_das/vmask_das
      integer nzr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nzv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nzr_das/nzr_das
     &       /ocean_nzu_das/nzu_das /ocean_nzv_das/nzv_das
      integer nsr_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsu_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      integer nsv_das(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_nsr_das/nsr_das
     &       /ocean_nsu_das/nsu_das /ocean_nsv_das/nsv_das
      real georatio(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /cgeoratio/georatio
      real z_das(ndas),tc1d_das(ndas),sc1d_das(ndas)
      common /grid_z_das/z_das,tc1d_das,sc1d_das
      real zeta_das9(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real t_das9(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      real rho_das9(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /ocean_t_das9/t_das9 /ocean_zeta_das9/zeta_das9
     &       /ocean_rho_das9/rho_das9
      real zeta_sm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /ocean_z_smooth/zeta_sm
      real zeta_sm_adj(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /adj_ocean_z_smooth/zeta_sm_adj
      real p_sm(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real p_sm_adj(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real psi_sm(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      real chi_sm(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /ocean_p_smooth/p_sm /ocean_padj_smooth/p_sm_adj
     &       /ocean_psi_smooth/psi_sm /ocean_chi_smooth/chi_sm
      real t_sm(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas,NT)
      common /ocean_t_smooth/t_sm
      real u_sm(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /ocean_u_smooth/u_sm
      real v_sm(0:Lm+1+padd_X,0:Mm+1+padd_E,ndas)
      common /ocean_v_smooth/v_sm
      real ug_adj(Istr-2:Iend+2,Jstr-2:Jend+2),
     &     vg_adj(Istr-2:Iend+2,Jstr-2:Jend+2),
     &     ps_adj(Istr-2:Iend+2,Jstr-2:Jend+2,NDAS)
      integer IstrR,IendR,JstrR,JendR
      integer IstrU
      integer JstrV
      if (Istr.eq.1) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (Iend.eq.Lm) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (Jend.eq.Mm) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      cff=2.0*g
      rho02=2.0*rho0
      crit_lat=2.5
      crit=2.0*1.e-5*sin(crit_lat*3.1415926/180.0)
      ff0=1.2e-7
      do j=Jstr-1,Jend+1
        do i=Istr-1,Iend+1
          ug_adj(i,j)=0.0
          vg_adj(i,j)=0.0
        enddo
      enddo
      do k=1,NDAS
        do j=Jstr-1,Jend+1
          do i=Istr-1,Iend+1
            ps_adj(i,j,k)=0.0
          enddo
        enddo
      enddo
      do k=1,NDAS
        if (Jend.eq.Mm) then
          do i=Istr,IendR
            ug_adj(i-1,Mm+1)=ug_adj(i-1,Mm+1)
     &           +0.5*u_s_adj(i,Mm+1,k)
            ug_adj(i,Mm+1)=ug_adj(i,Mm+1)
     &           +0.5*u_s_adj(i,Mm+1,k)
            u_s_adj(i,Mm+1,k) = 0.
          enddo
        endif
        if (Jstr.eq.1) then
          do i=Istr,IendR
            ug_adj(i-1,1)=ug_adj(i-1,1)
     &              +0.5*u_s_adj(i,0,k)
            ug_adj(i,1)=ug_adj(i,1)
     &              +0.5*u_s_adj(i,0,k)
            u_s_adj(i,0,k) = 0.
          enddo
        endif
        do j=Jstr,Jend
          do i=Istr,IendR
            ug_adj(i-1,j)=ug_adj(i-1,j)
     &              +0.25*u_s_adj(i,j,k)
            ug_adj(i-1,j+1)=ug_adj(i-1,j+1)
     &              +0.25*u_s_adj(i,j,k)
            ug_adj(i,j)=ug_adj(i,j)
     &              +0.25*u_s_adj(i,j,k)
            ug_adj(i,j+1)=ug_adj(i,j+1)
     &              +0.25*u_s_adj(i,j,k)
            u_s_adj(i,j,k)=0.
          enddo
        enddo
        do j=Jstr,Jend+1
          do i=Istr-1,IendR
            fcr=f(i,j-1)+f(i,j)
            if (abs(f(i,j-1)+f(i,j)) .lt. crit) then
              if (abs(fcr) .lt. ff0) then
                ug_adj(i,j)=0.0
              else
                ug_adj(i,j)=ug_adj(i,j)*abs(fcr)/crit
                ps_adj(i,j,k)=ps_adj(i,j,k)
     &            -cff*ug_adj(i,j)*(pn(i,j-1)+pn(i,j))/(f(i,j-1)+f(i,j))
     &            /(rho02+rho_das9(i,j,k)+rho_das9(i,j-1,k))
                ps_adj(i,j-1,k)=ps_adj(i,j-1,k)
     &             +cff*ug_adj(i,j)*(pn(i,j-1)+pn(i,j))
     &                                                /(f(i,j-1)+f(i,j))
     &            /(rho02+rho_das9(i,j,k)+rho_das9(i,j-1,k))
                ug_adj(i,j)=0.
              endif
            else
              ps_adj(i,j,k)=ps_adj(i,j,k)
     &              -cff*ug_adj(i,j)*(pn(i,j-1)+pn(i,j))
     &                                                /(f(i,j-1)+f(i,j))
     &            /(rho02+rho_das9(i,j,k)+rho_das9(i,j-1,k))
              ps_adj(i,j-1,k)=ps_adj(i,j-1,k)
     &              +cff*ug_adj(i,j)*(pn(i,j-1)+pn(i,j))
     &                                                /(f(i,j-1)+f(i,j))
     &            /(rho02+rho_das9(i,j,k)+rho_das9(i,j-1,k))
              ug_adj(i,j)=0.
            endif
          enddo
        enddo
        if (Iend.eq.Lm) then
          do j=Jstr,JendR
            vg_adj(Lm+1,j-1)=vg_adj(Lm+1,j-1)
     &             +0.5*v_s_adj(Lm+1,j,k)
            vg_adj(Lm+1,j)=vg_adj(Lm+1,j)
     &             +0.5*v_s_adj(Lm+1,j,k)
            v_s_adj(Lm+1,j,k) = 0.
          enddo
        endif
        if (Istr.eq.1) then
          do j=Jstr,JendR
            vg_adj(1,j-1)=vg_adj(1,j-1)
     &             +0.5*v_s_adj(0,j,k)
            vg_adj(1,j)=vg_adj(1,j)
     &             +0.5*v_s_adj(0,j,k)
            v_s_adj(0,j,k) = 0.
          enddo
        endif
        do j=Jstr,JendR
          do i=Istr,Iend
            vg_adj(i,j-1)=vg_adj(i,j-1)
     &             +0.25* v_s_adj(i,j,k)
            vg_adj(i,j)=vg_adj(i,j)
     &             +0.25* v_s_adj(i,j,k)
            vg_adj(i+1,j-1)=vg_adj(i+1,j-1)
     &             +0.25* v_s_adj(i,j,k)
            vg_adj(i+1,j)=vg_adj(i+1,j)
     &             +0.25* v_s_adj(i,j,k)
            v_s_adj(i,j,k)=0.
          enddo
        enddo
        do j=Jstr-1,JendR
          do i=Istr,Iend+1
            if (abs(f(i-1,j)+f(i,j)) .lt. crit ) then
              fcr=f(i-1,j)+f(i,j)
              if (abs(fcr) .lt. ff0) then
                vg_adj(i,j)=0.0
              else
                vg_adj(i,j)=vg_adj(i,j)*abs(fcr)/crit
                ps_adj(i,j,k)=ps_adj(i,j,k)
     &            +cff * vg_adj(i,j)*(pm(i-1,j)+pm(i,j))
     &                                                /(f(i-1,j)+f(i,j))
     &            /(rho02+rho_das9(i-1,j,k)+rho_das9(i,j,k))
                ps_adj(i-1,j,k)=ps_adj(i-1,j,k)
     &            -cff * vg_adj(i,j)*(pm(i-1,j)+pm(i,j))
     &                                                /(f(i-1,j)+f(i,j))
     &            /(rho02+rho_das9(i-1,j,k)+rho_das9(i,j,k))
                vg_adj(i,j)= 0.
              endif
            else
              ps_adj(i,j,k)=ps_adj(i,j,k)
     &          +cff * vg_adj(i,j)*(pm(i-1,j)+pm(i,j))/(f(i-1,j)+f(i,j))
     &            /(rho02+rho_das9(i-1,j,k)+rho_das9(i,j,k))
              ps_adj(i-1,j,k)=ps_adj(i-1,j,k)
     &          -cff * vg_adj(i,j)*(pm(i-1,j)+pm(i,j))/(f(i-1,j)+f(i,j))
     &            /(rho02+rho_das9(i-1,j,k)+rho_das9(i,j,k))
              vg_adj(i,j)= 0.
            endif
          enddo
        enddo
      enddo
C$OMP CRITICAL (adj_geosp_cr)
      do k=1,NDAS
        do j=Jstr-1,Jend+1
          do i=Istr-1,Iend+1
            p_s_adj(i,j,k)=p_s_adj(i,j,k)+ps_adj(i,j,k)
            ps_adj(i,j,k)=0.0
          enddo
        enddo
      enddo
C$OMP END CRITICAL (adj_geosp_cr)
      return
      end
