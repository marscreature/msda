/* This is include file "ocean3.h". 
  --------------------------------------------
*/
#ifdef SOLVE3D
      real u(GLOBAL_2D_ARRAY,N)
CSDISTRIBUTE_RESHAPE u(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real v(GLOBAL_2D_ARRAY,N)
CSDISTRIBUTE_RESHAPE v(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real t(GLOBAL_2D_ARRAY,N,NT)
CSDISTRIBUTE_RESHAPE t(BLOCK_PATTERN,*,*) BLOCK_CLAUSE
      common /ocean_u/u /ocean_v/v /ocean_t/t

      real Hz(GLOBAL_2D_ARRAY,N)
CSDISTRIBUTE_RESHAPE Hz(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real Hz_bak(GLOBAL_2D_ARRAY,N)
CSDISTRIBUTE_RESHAPE Hz_bak(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real z_r(GLOBAL_2D_ARRAY,N)
CSDISTRIBUTE_RESHAPE z_r(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real z_w(GLOBAL_2D_ARRAY,0:N)
CSDISTRIBUTE_RESHAPE z_w(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real Huon(GLOBAL_2D_ARRAY,N)
CSDISTRIBUTE_RESHAPE  Huon(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real Hvom(GLOBAL_2D_ARRAY,N)
CSDISTRIBUTE_RESHAPE  Hvom(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real W(GLOBAL_2D_ARRAY,0:N)
CSDISTRIBUTE_RESHAPE W(BLOCK_PATTERN,*) BLOCK_CLAUSE
      common /grid_Hz/Hz    /grid_zr/z_r  /grid_W/W
     &  /grid_Hz_bak/Hz_bak /grid_zw/z_w  /grid_Huon/Huon
     &                                    /grid_Hvom/Hvom
      real rho(GLOBAL_2D_ARRAY,N)
CSDISTRIBUTE_RESHAPE rho(BLOCK_PATTERN,*) BLOCK_CLAUSE
      real rhop0(GLOBAL_2D_ARRAY,N)
CSDISTRIBUTE_RESHAPE rhop0(BLOCK_PATTERN,*) BLOCK_CLAUSE
      common /ocean_rho/rho /ocean_rhop0/rhop0

# ifdef ANA_MEANRHO
      real rhobar(GLOBAL_2D_ARRAY,N)
CSDISTRIBUTE_RESHAPE rhobar(BLOCK_PATTERN,*) BLOCK_CLAUSE
      common /ocean_rhobar/rhobar
# endif
#endif  /* SOLVE3D */

