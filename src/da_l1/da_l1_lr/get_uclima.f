      subroutine get_uclima
      implicit none
      integer  LLmH,  MMmH, NnH
      parameter (LLmH=606,  MMmH=438, NnH=66)
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLmH+2-nhalf)/nratio-1,
     &           MMm_lr=(MMmH+2-nhalf)/nratio-1)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLm_lr,  MMm=MMm_lr,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      real  x_sponge,   v_sponge
       real  tauT_in, tauT_out, tauM_in, tauM_out
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &                      , x_sponge,   v_sponge
     &                      , tauT_in, tauT_out, tauM_in, tauM_out
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
      integer indxU, indxV, indxT
      parameter (indxU=5, indxV=6, indxT=7)
      integer indxS
      parameter (indxS=indxT+1)
      integer indxO, indxW, indxR, indxVisc, indxAkv, indxAkt
      parameter (indxO=indxT+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed
     &                                  +ntrc_diats+ntrc_diauv+1,
     &           indxW=indxO+1, indxR=indxO+2,
     &           indxVisc=indxR+1,
     &           indxAkv=indxR+2, indxAkt=indxAkv+1)
      integer indxAks
      parameter (indxAks=indxAkt+1)
      integer indxSSH
      parameter (indxSSH=indxAkt+4)
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
      integer indxSHFl, indxSWRad
      parameter (indxSHFl=indxSSH+3)
      integer indxSSFl
      parameter (indxSSFl=indxSHFl+1, indxSWRad=indxSHFl+2)
      integer indxSST, indxdQdSST
      parameter (indxSST=indxSWRad+1, indxdQdSST=indxSWRad+2)
      integer indxSSS
      parameter (indxSSS=indxSST+2)
      integer indxBostr
      parameter (indxBostr=indxSUSTR+16)
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     &                u3dvar, v3dvar, p3dvar, w3dvar, b3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8,b3dvar=12)
      integer xi_rho,xi_u, eta_rho,eta_v
      parameter (xi_rho=LLm+2,  xi_u=xi_rho-1,
     &           eta_rho=MMm+2, eta_v=eta_rho-1)
      integer ncidfrc, ncidbulk, ncidclm,  ntsms
     &      , ntsrf,  ntssh,  ntsst, ntsss, ntuclm, ntww,
     &        ntbulk
      integer nttclm(NT),    ntstf(NT)
      integer ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV
      integer rstT(NT)
      integer  ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,  hisV,  hisR,    hisHbl, hisHbbl
     &      , hisO,  hisW,  hisVisc,  hisAkv,  hisAkt, hisAks
     &      , hisBostr
      integer hisT(NT)
      integer   ncidavg, nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,  avgV,  avgR,    avgHbl, avgHbbl
     &      , avgO,  avgW,  avgVisc, avgAkv,  avgAkt, avgAks
      integer avgT(NT)
      integer avgBostr
      logical wrthis(60+NT)
     &      , wrtavg(60+NT)
      common/incscrum/
     &        ncidfrc, ncidbulk,ncidclm, ntsms, ntsrf, ntssh, ntsst
     &      , ntuclm, ntsss, ntww, ntbulk
     &                        ,  nttclm,          ntstf
     &      , ncidrst, nrecrst,  nrpfrst
     &      , rstTime, rstTstep, rstZ,    rstUb,  rstVb
     &                         , rstU,    rstV,   rstT
     &      , ncidhis, nrechis,  nrpfhis
     &      , hisTime, hisTstep, hisZ,    hisUb,  hisVb
     &      , hisU,    hisV,     hisT,    hisR
     &      , hisO,    hisW,     hisVisc, hisAkv,  hisAkt, hisAks
     &      , hisHbl,  hisHbbl,  hisBostr
     &      , ncidavg,  nrecavg,  nrpfavg
     &      , avgTime, avgTstep, avgZ,    avgUb,  avgVb
     &      , avgU,    avgV,     avgT,    avgR
     &      , avgO,    avgW,     avgVisc,  avgAkv,  avgAkt, avgAks
     &      , avgHbl,  avgHbbl
     &      , avgBostr
     &      , wrthis
     &      , wrtavg
      character*80 date_str, title
      character*80 ininame,  grdname,  hisname
     &         ,   rstname,  frcname,  bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
      character*52  vname(4, 150)
      common /cncscrum/       date_str,   title
     &         ,   ininame,  grdname, hisname
     &         ,   rstname,  frcname, bulkname,  usrname
     &                                ,   avgname
     &                                ,   clmname
     &                      ,  vname
      real ssh(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real Znudgcof(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /climat_ssh/ssh /climat_Znudgcof/Znudgcof
      real tclm(0:Lm+1+padd_X,0:Mm+1+padd_E,N,NT)
      common /climat_tclm/tclm
      real Tnudgcof(0:Lm+1+padd_X,0:Mm+1+padd_E,NT)
      common /climat_Tnudgcof/Tnudgcof
      real ubclm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vbclm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /climat_ubclm/ubclm /climat_vbclm/vbclm
      real ubclima(0:Lm+1+padd_X,0:Mm+1+padd_E,2)
      real vbclima(0:Lm+1+padd_X,0:Mm+1+padd_E,2)
      common /climat_ubclima/ubclima /climat_vbclima/vbclima
      real uclm(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real vclm(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      common /climat_uclm/uclm /climat_vclm/vclm
      real uclima(0:Lm+1+padd_X,0:Mm+1+padd_E,N,2)
      real vclima(0:Lm+1+padd_X,0:Mm+1+padd_E,N,2)
      common /climat_uclima/uclima /climat_vclima/vclima
      real    uclm_time(2), uclm_cycle
      integer ituclm, uclm_ncycle, uclm_rec, uclm_tid,
     &        ubclm_id, vbclm_id, uclm_id, vclm_id
      common /climat_udat/  uclm_time,       uclm_cycle,
     &        ituclm,       uclm_ncycle,     uclm_rec,
     &        uclm_tid,     ubclm_id,        vbclm_id,
     &        uclm_id,      vclm_id
      include 'netcdf.inc'
      real cff
      integer i, lstr,lvar,lenstr, ierr, nf_fread, advance_cycle
      if (may_day_flag.ne.0) return
      if (iic.eq.0 ) then
        lstr=lenstr(clmname)
        if (ncidclm.eq.-1) then
          ierr=nf_open (clmname(1:lstr), nf_nowrite, ncidclm)
          if (ierr .ne. nf_noerr) goto 4
        endif
        ierr=nf_inq_varid (ncidclm, 'uclm_time', uclm_tid)
        if (ierr .ne. nf_noerr) then
          write(stdout,3) 'uclm_time', clmname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxUb))
        ierr=nf_inq_varid (ncidclm, vname(1,indxUb)(1:lvar), ubclm_id)
        if (ierr .ne. nf_noerr) then
          write(stdout,3) vname(1,indxUb)(1:lvar), clmname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxVb))
        ierr=nf_inq_varid (ncidclm, vname(1,indxVb)(1:lvar), vbclm_id)
        if (ierr .ne. nf_noerr) then
          write(stdout,3) vname(1,indxVb)(1:lvar), clmname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxU))
        ierr=nf_inq_varid (ncidclm, vname(1,indxU)(1:lvar), uclm_id)
        if (ierr .ne. nf_noerr) then
          write(stdout,3) vname(1,indxU)(1:lvar), clmname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxV))
        ierr=nf_inq_varid (ncidclm, vname(1,indxV)(1:lvar), vclm_id)
        if (ierr .ne. nf_noerr) then
          write(stdout,3) vname(1,indxV)(1:lvar), clmname(1:lstr)
          goto 99
        endif
          call set_cycle (ncidclm, uclm_tid, ntuclm,
     &                    uclm_cycle, uclm_ncycle, uclm_rec)
          if (may_day_flag.ne.0) return
          ituclm=2
          uclm_time(1)=-1.E+20
          uclm_time(2)=-1.E+20
      endif
 10   i=3-ituclm
      cff=time+0.5*dt
      if (uclm_time(i).le.cff .and.
     &  cff.lt.uclm_time(ituclm)) goto 1
      ierr=advance_cycle (uclm_cycle,  ntuclm,
     &                    uclm_ncycle, uclm_rec)
      if (ierr.ne.0) then
        write(stdout,7) uclm_rec, ntuclm,
     &                  clmname(1:lstr), tdays,
     &                  uclm_time(ituclm)*sec2day
        goto 99
      endif
      ierr=nf_get_var1_double(ncidclm, uclm_tid,
     &                             uclm_rec, cff)
      if (ierr.ne.NF_NOERR) then
        write(stdout,6) 'Xclm_time', uclm_rec
        goto 99
      endif
      uclm_time(i)=cff*day2sec+uclm_cycle
     &                             *uclm_ncycle
      if (uclm_time(ituclm).eq.-1.E+20)
     &    uclm_time(ituclm)=uclm_time(i)
      ierr=nf_fread (ubclima(0,0,i),
     &                           ncidclm, ubclm_id,
     &                           uclm_rec, u2dvar)
      if (ierr.ne.NF_NOERR) then
        lvar=lenstr(vname(1,indxUb))
        write(stdout,6) vname(1,indxUb)(1:lvar), uclm_rec
        goto 99
      endif
      ierr=nf_fread (vbclima(0,0,i),
     &                           ncidclm, vbclm_id,
     &                           uclm_rec, v2dvar)
      if (ierr.ne.NF_NOERR) then
        lvar=lenstr(vname(1,indxVb))
        write(stdout,6) vname(1,indxVb)(1:lvar), uclm_rec
        goto 99
      endif
      ierr=nf_fread (uclima(0,0,1,i),
     &                           ncidclm, uclm_id,
     &                           uclm_rec, u3dvar)
      if (ierr.ne.NF_NOERR) then
        lvar=lenstr(vname(1,indxU))
        write(stdout,6) vname(1,indxU)(1:lvar), uclm_rec
        goto 99
      endif
      ierr=nf_fread (vclima(0,0,1,i),
     &                           ncidclm, vclm_id,
     &                           uclm_rec, v3dvar)
      if (ierr.ne.NF_NOERR) then
        lvar=lenstr(vname(1,indxV))
        write(stdout,6) vname(1,indxV)(1:lvar), uclm_rec
        goto 99
      endif
      ituclm=i
      write(stdout,'(6x,A,1x,g12.4,1x,I4)')
     &'GET_UCLIMA -- Read momentum climatology      for time =', cff
        if (ntuclm.gt.1) goto 10
  1    continue
      return
  3   format(/,' GET_UCLIMA - unable to find climatology variable: ',
     &       a,/,15x,'in climatology NetCDF file: ',a)
  4   write(stdout,5) clmname(1:lstr)
  5   format(/,' GET_UCLIMA - unable to open climatology',
     &         1x,'NetCDF file: ',a)
      goto 99
  6   format(/,' GET_UCLIMA - ERROR while reading variable: ',a,2x,
     &       ' at TIME index = ',i4)
  7   format(/,' GET_UCLIMA - ERROR: requested time record ',I4,
     &       1x,'exeeds the last available', /,14x,'record ',I4,
     &       1x,'in climatology file: ',a, /,14x,'TDAYS = ',
     &       g12.4,2x,'last available UCLM_TIME = ',g12.4)
  99  may_day_flag=2
      return
      end
      subroutine set_uclima (tile)
      implicit none
      integer tile
      integer  LLmH,  MMmH, NnH
      parameter (LLmH=606,  MMmH=438, NnH=66)
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLmH+2-nhalf)/nratio-1,
     &           MMm_lr=(MMmH+2-nhalf)/nratio-1)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLm_lr,  MMm=MMm_lr,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      integer chunk_size_X,margin_X,chunk_size_E,margin_E
      parameter (chunk_size_X=(Lm+NSUB_X-1)/NSUB_X,
     &                     margin_X=(NSUB_X*chunk_size_X-Lm)/2,
     &           chunk_size_E=(Mm+NSUB_E-1)/NSUB_E,
     &                     margin_E=(NSUB_E*chunk_size_E-Mm)/2)
      integer Istr,Iend,Jstr,Jend, i_X,j_E
        j_E=tile/NSUB_X
        i_X=tile-j_E*NSUB_X
        Istr=1+i_X*chunk_size_X-margin_X
        Iend=Istr+chunk_size_X-1
        Istr=max(Istr,1)
        Iend=min(Iend,Lm)
        Jstr=1+j_E*chunk_size_E-margin_E
        Jend=Jstr+chunk_size_E-1
        Jstr=max(Jstr,1)
        Jend=min(Jend,Mm)
      call set_uclima_tile (Istr,Iend,Jstr,Jend)
      return
      end
      subroutine set_uclima_tile (Istr,Iend,Jstr,Jend)
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k, it1,it2
      real cff, cff1, cff2
      integer  LLmH,  MMmH, NnH
      parameter (LLmH=606,  MMmH=438, NnH=66)
      integer  nratio,nhalf ,  LLm_lr,Lm_lr,  MMm_lr,Mm_lr
      parameter (nratio=3, nhalf=nratio/2+1)
      parameter (LLm_lr=(LLmH+2-nhalf)/nratio-1,
     &           MMm_lr=(MMmH+2-nhalf)/nratio-1)
      integer  LLm,Lm,  MMm,Mm, N
      parameter (
     &               LLm=LLm_lr,  MMm=MMm_lr,  N=NnH
     &                                      )
      integer NSUB_X, NSUB_E, NPP
      parameter (NSUB_X=2, NSUB_E=28, NPP=56,       Lm=LLm, Mm=MMm)
      integer NT, itemp,
     &        ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
     &        , ntrc_diats, ntrc_diauv
     &          , isalt
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (isalt=itemp+1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
      parameter (ntrc_diats=0)
      parameter (ntrc_diauv=0)
      integer stdout, Np, padd_X,padd_E
      parameter (stdout=6, Np=N+1, padd_X=(Lm+2)/2-(Lm+1)/2,
     &                             padd_E=(Mm+2)/2-(Mm+1)/2)
      integer NWEIGHT
      parameter (NWEIGHT=137)
      real ssh(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real Znudgcof(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /climat_ssh/ssh /climat_Znudgcof/Znudgcof
      real tclm(0:Lm+1+padd_X,0:Mm+1+padd_E,N,NT)
      common /climat_tclm/tclm
      real Tnudgcof(0:Lm+1+padd_X,0:Mm+1+padd_E,NT)
      common /climat_Tnudgcof/Tnudgcof
      real ubclm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      real vbclm(0:Lm+1+padd_X,0:Mm+1+padd_E)
      common /climat_ubclm/ubclm /climat_vbclm/vbclm
      real ubclima(0:Lm+1+padd_X,0:Mm+1+padd_E,2)
      real vbclima(0:Lm+1+padd_X,0:Mm+1+padd_E,2)
      common /climat_ubclima/ubclima /climat_vbclima/vbclima
      real uclm(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      real vclm(0:Lm+1+padd_X,0:Mm+1+padd_E,N)
      common /climat_uclm/uclm /climat_vclm/vclm
      real uclima(0:Lm+1+padd_X,0:Mm+1+padd_E,N,2)
      real vclima(0:Lm+1+padd_X,0:Mm+1+padd_E,N,2)
      common /climat_uclima/uclima /climat_vclima/vclima
      real    uclm_time(2), uclm_cycle
      integer ituclm, uclm_ncycle, uclm_rec, uclm_tid,
     &        ubclm_id, vbclm_id, uclm_id, vclm_id
      common /climat_udat/  uclm_time,       uclm_cycle,
     &        ituclm,       uclm_ncycle,     uclm_rec,
     &        uclm_tid,     ubclm_id,        vbclm_id,
     &        uclm_id,      vclm_id
      real dt, dtfast, time, time_start, tdays
      integer iic, kstp, krhs, knew, next_kstp
     &      , iif, nstp, nrhs, nnew
      logical PREDICTOR_2D_STEP
      common /time_indices/  dt,dtfast, time,time_start, tdays,
     &                       iic, kstp, krhs, knew, next_kstp,
     &                       iif, nstp, nrhs, nnew,
     &                       PREDICTOR_2D_STEP
      real time_avg, rho0
     &               , rdrg, rdrg2, Cdb_min, Cdb_max, Zob
     &               , xl, el, visc2, visc4, gamma2
      real  theta_s,   theta_b,   Tcline,  hc
      real  sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real  rx0, rx1
      real  tnu2(NT),tnu4(NT)
      real weight(2,0:NWEIGHT)
      real  x_sponge,   v_sponge
       real  tauT_in, tauT_out, tauM_in, tauM_out
      integer numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/
     &             time_avg,  rho0,      rdrg,    rdrg2
     &           , Zob,       Cdb_min,   Cdb_max
     &           , xl, el,    visc2,     visc4,   gamma2
     &           , theta_s,   theta_b,   Tcline,  hc
     &           , sc_w,      Cs_w,      sc_r,    Cs_r
     &           , rx0,       rx1,       tnu2,    tnu4
     &                      , weight
     &                      , x_sponge,   v_sponge
     &                      , tauT_in, tauT_out, tauM_in, tauM_out
     &      , numthreads,     ntstart,   ntimes,  ninfo
     &      , ndtfast,nfast,  nrrec,     nrst,    nwrt
     &                                 , ntsavg,  navg
     &                      , ldefhis
     &                      , got_tini
      logical synchro_flag
      integer may_day_flag
      integer tile_count, first_time, bc_count
      real hmin, hmax, grdmin, grdmax, Cu_min, Cu_max
      real volume, avgke, avgpe, avgkp
     &               , bc_crss, bc_flux, ubar_xs
      common /communicators/
     &        volume,     avgke, avgpe, avgkp,
     &        tile_count, first_time, hmin,hmax,    grdmin,grdmax,
     &        Cu_min, Cu_max, may_day_flag, bc_count,
     &        bc_crss, bc_flux,  ubar_xs,
     &        synchro_flag
      real*4 CPU_time(0:31,0:NPP)
      integer proc(0:31,0:NPP),trd_count
      common /timers/CPU_time,proc,trd_count
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180.,
     &                                      rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off,
     &     year2day,day2year
      parameter (Eradius=6371315.0,  day2sec=86400.,
     &           sec2day=1./86400., jul_off=2440000.,
     &           year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
      real vonKar
      parameter (vonKar=0.41)
      integer IstrR,IendR,JstrR,JendR
      if (Istr.eq.1) then
        IstrR=Istr-1
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lm) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        JstrR=Jstr-1
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mm) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      it1=3-ituclm
      it2=ituclm
      cff=time+0.5*dt
      cff1=uclm_time(it2)-cff
      cff2=cff-uclm_time(it1)
      if (Istr+Jstr.eq.2 .and. cff1.lt.dt) synchro_flag=.TRUE.
      if (uclm_cycle.lt.0.) then
        if (iic.eq.0) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              ubclm(i,j)=ubclima(i,j,ituclm)
              vbclm(i,j)=vbclima(i,j,ituclm)
            enddo
          enddo
          do k=1,N
            do j=JstrR,JendR
              do i=IstrR,IendR
                uclm(i,j,k)=uclima(i,j,k,ituclm)
                vclm(i,j,k)=vclima(i,j,k,ituclm)
              enddo
            enddo
          enddo
        endif
      elseif (cff1.ge.0. .and. cff2.ge.0.) then
        cff=1./(cff1+cff2)
        cff1=cff1*cff
        cff2=cff2*cff
        do j=JstrR,JendR
          do i=IstrR,IendR
            ubclm(i,j)=cff1*ubclima(i,j,it1)
     &                +cff2*ubclima(i,j,it2)
            vbclm(i,j)=cff1*vbclima(i,j,it1)
     &                +cff2*vbclima(i,j,it2)
          enddo
        enddo
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              uclm(i,j,k)=cff1*uclima(i,j,k,it1)
     &                   +cff2*uclima(i,j,k,it2)
              vclm(i,j,k)=cff1*vclima(i,j,k,it1)
     &                   +cff2*vclima(i,j,k,it2)
            enddo
          enddo
        enddo
      elseif (Istr+Jstr.eq.2) then
          write(stdout,'(/1x,2A/3(1x,A,F16.10)/)')
     &            'SET_UCLIMA_TILE - current model time is outside ',
     &            'bounds of ''uclm_time''.', 'UCLM_TSTART=',
     &             uclm_time(it1)*sec2day,     'TDAYS=',  tdays,
     &            'UCLM_TEND=',   uclm_time(it2)*sec2day
          may_day_flag=2
      endif
      return
      end
