#include "cppdefs.h"

      subroutine das_innov_hfradar (tile)
      implicit none
      integer tile,trd, omp_get_thread_num
# include "param.h"
# include "das_param.h"
# include "das_private_scratch.h"
# include "compute_tile_bounds.h"
      trd=omp_get_thread_num()
      call das_innov_hfradar_tile (Istr,Iend,Jstr,Jend,
     &               A2d(1,1,trd),A2d(1,2,trd),A2d(1,3,trd))
      return
      end

      subroutine das_innov_hfradar_tile (Istr,Iend,Jstr,Jend,ur,vr,rmaskr)
!
!--------------------------------------------------------------------
!  Set initial conditions for momentum and tracer variables using
!  analytical expressions.
!--------------------------------------------------------------------
!
      implicit none
      integer Istr,Iend,Jstr,Jend, i,j,k, itrc
      real cfu,cfv,crt
      real srad,srad0,rr,rc, lonc,latc
      real uu,vv,dis
      integer count,num,n0
# include "param.h"
# include "das_param.h"
# include "scalars.h"
# include "grid.h"
# include "das_ocean.h"
# include "das_covar.h"
# include "das_innov.h"
!
      real ur(PRIVATE_2D_SCRATCH_ARRAY),                                                                                             
     &     vr(PRIVATE_2D_SCRATCH_ARRAY),
     &     rmaskr(PRIVATE_2D_SCRATCH_ARRAY)
!
!
# include "compute_auxiliary_bounds.h"
!
      srad=2.0*0.015*0.015
      srad0=0.005*0.005
      crt=0.35
!
!     ... form superobs
!
! u
!
      do j=Jstr-1,JendR
        do i=Istr-1,IendR
          if (rmask_das(i,j,NDAS) .gt. 0.5) then
            count=0
            uu=0.
            vv=0.
            rc=0.0
            do num=1,num_hfradar
              rr= (lonr(i,j)-lon_fl(num))
     &           *(lonr(i,j)-lon_fl(num))
     &           +(latr(i,j)-lat_fl(num))
     &           *(latr(i,j)-lat_fl(num))
              if (rr .lt. srad ) then
                count=count+1
                dis=max(rr,srad0)
                uu=uu+u_hf_raw(num)/dis
                vv=vv+v_hf_raw(num)/dis
                rc=rc+1.0/dis
              endif
            enddo
            if (count .ge. 1 ) then
!
! rotate to ROMS curvelinear
! u_r = u.*cosa + v.*sina;
! v_r = v.*cosa - u.*sina;
!
              ur(i,j)=(uu*cos(angler(i,j))+vv*sin(angler(i,j)))/rc
              vr(i,j)=(vv*cos(angler(i,j))-uu*sin(angler(i,j)))/rc
              rmaskr(i,j)=1.0

              print*, 'HF=', ur(i,j), vr(i,j)
            else
              ur(i,j)=0.0
              vr(i,j)=0.0
              rmaskr(i,j)=0.0
            endif
          else
            ur(i,j)=0.0
            vr(i,j)=0.0
            rmaskr(i,j)=0.0
          endif
        enddo
      enddo
!
!     ... QC and INNOVATION
!
!u
      do j=Jstr,Jend
        do i=Istr,IendR
          cfu=0.5*( (ur(i-1,j)+ur(i,j))
     &             -(u_das(i-1,j,NDAS)+u_das(i,j,NDAS)) )
          hf_umask(i,j) = rmaskr(i-1,j)*rmaskr(i,j)
     &                  * pmask_das(i,j,NDAS)*pmask_das(i,j+1,NDAS)  ! exclude u adjacent to coast
          if (abs(cfu) .gt. crt ) hf_umask(i,j)=0.0
          u_hf(i,j)=cfu * hf_umask(i,j)
        enddo
      enddo
!
!
! v
      do j=Jstr,JendR
        do i=Istr,Iend
          cfv=0.5*( (vr(i,j-1)+vr(i,j)) 
     &             -(v_das(i,j-1,NDAS)+v_das(i,j,NDAS)) )
          hf_vmask(i,j) = rmaskr(i,j-1)*rmaskr(i,j)
     &                  * pmask_das(i,j,NDAS)*pmask_das(i+1,j,NDAS)  ! exclude v adjacent to coast
          if ( abs(cfv) .gt. crt) hf_vmask(i,j)=0.0
          v_hf(i,j)=cfv * hf_vmask(i,j)
        enddo
      enddo
!
      return
      end
