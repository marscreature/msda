      integer filetype_his, filetype_avg
     & ,filetype_dia, filetype_dia_avg
     & ,filetype_diaM, filetype_diaM_avg
     & ,filetype_diabio, filetype_diabio_avg
      parameter (filetype_his=1, filetype_avg=2,
     & filetype_dia=3, filetype_dia_avg=4,
     & filetype_diaM=5, filetype_diaM_avg=6,
     & filetype_diabio=7,filetype_diabio_avg=8)
      integer iloop, indextemp
      integer indxTime, indxZ, indxUb, indxVb
      parameter (indxTime=1, indxZ=2, indxUb=3, indxVb=4)
      integer indxU, indxV, indxT
      parameter (indxU=6, indxV=7, indxT=8)
      integer indxS
      parameter (indxS=indxT+1)
      integer indxBSD, indxBSS
      parameter (indxBSD=indxT+ntrc_salt+ntrc_pas+ntrc_bio+1,
     & indxBSS=101)
      integer indxO, indxW, indxR, indxVisc, indxDiff, indxAkv, indxAkt
      parameter (indxO=indxT+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed
     & +ntrc_diats+ntrc_diauv+ntrc_diabio+1,
     & indxW=indxO+1, indxR=indxO+2, indxVisc=indxO+3,
     & indxDiff=indxO+4,indxAkv=indxO+5, indxAkt=indxO+6)
      integer indxAks
      parameter (indxAks=indxAkt+4)
      integer indxHbl
      parameter (indxHbl=indxAkt+5)
      integer indxHbbl
      parameter (indxHbbl=indxAkt+6)
      integer indxSSH
      parameter (indxSSH=indxAkt+12)
      integer indxSUSTR, indxSVSTR
      parameter (indxSUSTR=indxSSH+1, indxSVSTR=indxSSH+2)
      integer indxTime2
      parameter (indxTime2=indxSSH+3)
      integer indxShflx, indxShflx_rsw
      parameter (indxShflx=indxSSH+4)
      integer indxSwflx
      parameter (indxSwflx=indxShflx+1, indxShflx_rsw=indxShflx+2)
      integer indxSST, indxdQdSST
      parameter (indxSST=indxShflx_rsw+1, indxdQdSST=indxShflx_rsw+2)
      integer indxWSPD,indxTAIR,indxRHUM,indxRADLW,indxRADSW,
     & indxPRATE,indxUWND,indxVWND
      parameter (indxWSPD=indxSST+3, indxTAIR=indxSST+4,
     & indxRHUM=indxSST+5, indxRADLW=indxSST+6,
     & indxRADSW=indxSST+7, indxPRATE=indxSST+8,
     & indxUWND=indxSST+9, indxVWND=indxSST+10)
      integer indxShflx_rlw,indxShflx_lat,indxShflx_sen
      parameter (indxShflx_rlw=indxSST+12,
     & indxShflx_lat=indxSST+13, indxShflx_sen=indxSST+14)
      integer indxWstr
      parameter (indxWstr=indxSUSTR+21)
      integer indxUWstr
      parameter (indxUWstr=indxSUSTR+22)
      integer indxVWstr
      parameter (indxVWstr=indxSUSTR+23)
      integer indxBostr
      parameter (indxBostr=indxSUSTR+24)
      integer indxWWA,indxWWD,indxWWP,indxWEB,indxWED,indxWER
      parameter (indxWWA=indxSUSTR+32, indxWWD=indxWWA+1,
     & indxWWP=indxWWA+2
     & )
      integer r2dvar, u2dvar, v2dvar, p2dvar, r3dvar,
     & u3dvar, v3dvar, p3dvar, w3dvar, b3dvar
      parameter (r2dvar=0, u2dvar=1, v2dvar=2, p2dvar=3,
     & r3dvar=4, u3dvar=5, v3dvar=6, p3dvar=7, w3dvar=8,b3dvar=12)
      integer xi_rho,xi_u, eta_rho,eta_v
      common/netCDFhorizdim/xi_rho,xi_u, eta_rho,eta_v
      integer ncidfrc, ncidbulk, ncidclm, ntsms ,
     & ntsrf, ntssh, ntsst, ntsss, ntuclm,
     & ntbulk, ncidqbar, ntqbar, ntww
      integer nttclm(NT), ntstf(NT), nttsrc(NT)
      integer ncidrst, nrecrst, nrpfrst
     & , rstTime, rstTime2, rstTstep, rstZ, rstUb, rstVb
     & , rstU, rstV
      integer rstT(NT)
      integer ncidhis, nrechis, nrpfhis
     & , hisTime, hisTime2, hisTstep, hisZ, hisUb, hisVb
     & , hisBostr, hisWstr, hisUWstr, hisVWstr
     & , hisShflx, hisSwflx, hisShflx_rsw
     & , hisU, hisV, hisR, hisHbl, hisHbbl
     & , hisO, hisW, hisVisc, hisDiff
     & , hisAkv, hisAkt, hisAks
     & , hisShflx_rlw
     & , hisShflx_lat, hisShflx_sen
      integer hisT(NT)
      integer ncidavg, nrecavg, nrpfavg
     & , avgTime, avgTime2, avgTstep, avgZ, avgUb, avgVb
     & , avgBostr, avgWstr, avgUwstr, avgVwstr
     & , avgShflx, avgSwflx, avgShflx_rsw
     & , avgU, avgV, avgR, avgHbl, avgHbbl
     & , avgO, avgW, avgVisc, avgDiff
     & , avgAkv, avgAkt, avgAks
      integer avgT(NT)
      integer avgShflx_rlw
     & , avgShflx_lat, avgShflx_sen
      logical wrthis(500+NT)
     & , wrtavg(500+NT)
      common/incscrum/
     & ncidfrc, ncidbulk,ncidclm, ntsms, ntsrf, ntssh, ntsst
     & , ntuclm, ntsss, ntbulk, ncidqbar, ntqbar, ntww
     & , nttclm, ntstf, nttsrc
     & , ncidrst, nrecrst, nrpfrst
     & , rstTime, rstTime2, rstTstep, rstZ, rstUb, rstVb
     & , rstU, rstV, rstT
     & , ncidhis, nrechis, nrpfhis
     & , hisTime, hisTime2, hisTstep, hisZ, hisUb, hisVb
     & , hisBostr, hisWstr, hisUWstr, hisVWstr
     & , hisShflx, hisSwflx, hisShflx_rsw
     & , hisU, hisV, hisT, hisR
     & , hisO, hisW, hisVisc, hisDiff
     & , hisAkv, hisAkt, hisAks
     & , hisHbl, hisHbbl
     & , hisShflx_rlw
     & , hisShflx_lat, hisShflx_sen
     & , ncidavg, nrecavg, nrpfavg
     & , avgTime, avgTime2, avgTstep, avgZ, avgUb, avgVb
     & , avgBostr, avgWstr, avgUWstr, avgVWstr
     & , avgShflx, avgSwflx, avgShflx_rsw
     & , avgU, avgV, avgT, avgR
     & , avgO, avgW, avgVisc, avgDiff
     & , avgAkv, avgAkt, avgAks
     & , avgHbl, avgHbbl
     & , avgShflx_rlw
     & , avgShflx_lat, avgShflx_sen
     & , wrthis
     & , wrtavg
     & , rstname, frcname, bulkname, usrname
     & , qbarname, tsrcname
     & , avgname
     & , clmname
      common /cncscrum/ date_str, title, start_date
     & , ininame, grdname, hisname
     & , rstname, frcname, bulkname, usrname
     & , qbarname, tsrcname
     & , avgname
     & , clmname
     & , vname
