      if (.not.WEST_INTER) then
        do j=J_EXT_RANGE
          hwrk(Istr-1,j)=hwrk(Istr,j)
        enddo
      endif
      if (.not.EAST_INTER) then
        do j=J_EXT_RANGE
          hwrk(Iend+1,j)=hwrk(Iend,j)
        enddo
      endif
      if (.not.SOUTH_INTER) then
        do i=I_EXT_RANGE
          hwrk(i,Jstr-1)=hwrk(i,Jstr)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=I_EXT_RANGE
          hwrk(i,Jend+1)=hwrk(i,Jend)
        enddo
      endif
      if (.not.WEST_INTER.and. .not.SOUTH_INTER) then
        hwrk(Istr-1,Jstr-1)=hwrk(Istr,Jstr)
      endif
      if (.not.WEST_INTER.and. .not.NORTH_INTER) then
        hwrk(Istr-1,Jend+1)=hwrk(Istr,Jend)
      endif
      if (.not.EAST_INTER.and. .not.SOUTH_INTER) then
        hwrk(Iend+1,Jstr-1)=hwrk(Iend,Jstr)
      endif
      if (.not.EAST_INTER.and. .not.NORTH_INTER) then
        hwrk(Iend+1,Jend+1)=hwrk(Iend,Jend)
      endif
      do j=Jstr,Jend+1
        do i=Istr,Iend+1
     & +hwrk(i,j-1)+hwrk(i-1,j-1))
        enddo
      enddo
      do j=Jstr,Jend
        do i=Istr,Iend
     & +pmask2(i,j+1) +pmask2(i+1,j+1))
     & +wrk(i,j+1)+wrk(i+1,j+1))
        enddo
      enddo
