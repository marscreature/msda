      real Ft0(Mfloats), Fx0(Mfloats), Fy0(Mfloats), Fz0(Mfloats),
     & Fdt(Mfloats), Fdx(Mfloats), Fdy(Mfloats), Fdz(Mfloats)
      common /ncrealfloats/ Ft0, Fx0, Fy0, Fz0, Fdt, Fdx, Fdy, Fdz
      integer Fcoor(Mfloats), Ftype(Mfloats), Fcount(Mfloats),
     & Fgrd(Mfloats), i_floats
      common /ncintfloats/ Fcoor, Ftype, Fcount, Fgrd, i_floats
      common /nccharfloats/ Ftitle
