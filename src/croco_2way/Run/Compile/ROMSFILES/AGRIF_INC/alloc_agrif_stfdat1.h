#include "Param_toamr_stfdat1.h" 
      !! ALLOCATION OF VARIABLE : stf_scale
          if (.not. allocated(Agrif_Gr % tabvars(201)% array1)) then
          allocate(Agrif_Gr % tabvars(201)% array1(1 : NT))
          Agrif_Gr % tabvars(201)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : stf_cycle
          if (.not. allocated(Agrif_Gr % tabvars(202)% array1)) then
          allocate(Agrif_Gr % tabvars(202)% array1(1 : NT))
          Agrif_Gr % tabvars(202)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : stf_time
          if (.not. allocated(Agrif_Gr % tabvars(203)% array2)) then
          allocate(Agrif_Gr % tabvars(203)% array2(1 : 2,1 : NT))
          Agrif_Gr % tabvars(203)% array2 = 0
      endif
      !! ALLOCATION OF VARIABLE : stflxp
          if (.not. allocated(Agrif_Gr % tabvars(204)% array2)) then
          allocate(Agrif_Gr % tabvars(204)% array2(1 : 2,1 : NT))
          Agrif_Gr % tabvars(204)% array2 = 0
      endif
