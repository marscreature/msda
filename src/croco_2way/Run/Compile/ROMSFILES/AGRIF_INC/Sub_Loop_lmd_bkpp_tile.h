      subroutine Sub_Loop_lmd_bkpp_tile(istr,iend,jstr,jend,Kv,Kt,Ks,ws,
     &wm,my_hbbl,wrk,Cr,Gm1,dGm1dS,Gt1,dGt1dS,Gs1,dGs1dS,my_kbbl,padd_E,
     &Mm,padd_X,Lm,hbls,kbbl,hbbl,rmask,pmask2,f,bvf,Hz,v,nstp,u,Zob,z_w
     &,z_r,bvstr,bustr,ustar,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INT
     &ER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: lmd_nu0c = 0.1D0
      real, parameter :: Ricr = 0.3D0
      real, parameter :: Ri_inv = 1.D0/Ricr
      real, parameter :: C_Ek = 258.D0
      real, parameter :: eps = 1.D-20
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      real :: Zob
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: hbls
      integer(4), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: kbbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: f
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: z_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ustar
      integer(4) :: istr
      integer(4) :: iend
      integer(4) :: jstr
      integer(4) :: jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: ws
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: wm
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: my_hbbl
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: wrk
      real, dimension(Istr-2:Iend+2,0:N) :: Cr
      real, dimension(Istr-2:Iend+2) :: Gm1
      real, dimension(Istr-2:Iend+2) :: dGm1dS
      real, dimension(Istr-2:Iend+2) :: Gt1
      real, dimension(Istr-2:Iend+2) :: dGt1dS
      real, dimension(Istr-2:Iend+2) :: Gs1
      real, dimension(Istr-2:Iend+2) :: dGs1dS
      integer(4), dimension(Istr-2:Iend+2) :: my_kbbl

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                                       
                                                                 
      
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: imin
      integer(4) :: imax
      integer(4) :: jmin
      integer(4) :: jmax
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                            
      real :: zscale
      real :: z_bl
      real :: zsbl
      real :: Av_bl
      real :: dAv_bl
      real :: At_bl
      real :: a1
      real :: dAt_bl
      real :: a2
      real :: As_bl
      real :: a3
      real :: dAs_bl
                                       
                                                                       
                                                                 
                                              
      real :: Kern
      real :: sigma
      real :: cff
      real :: cff1
      real :: cff_up
      real :: cff_dn
      real :: invrho
      real :: Kv0
      real :: Kt0
      real :: Ks0
                                                                       
                                                                 
                                                               

      if (.not.WEST_INTER) then
        imin=istr
      else
        imin=istr-1
      endif
      if (.not.EAST_INTER) then
        imax=iend
      else
        imax=iend+1
      endif
      if (.not.SOUTH_INTER) then
        jmin=jstr
      else
        jmin=jstr-1
      endif
      if (.not.NORTH_INTER) then
        jmax=jend
      else
        jmax=jend+1
      endif
      do j=jmin,jmax
        do i=imin,imax
          ustar(i,j)=sqrt(sqrt( (0.5D0*(bustr(i,j)+bustr(i+1,j)))**2
     &                         +(0.5D0*(bvstr(i,j)+bvstr(i,j+1)))**2))
        enddo
      enddo
      do j=jmin,jmax
        do i=imin,imax
          wm(i,j)=vonKar*ustar(i,j)
          ws(i,j)=wm(i,j)
          my_kbbl(i)=N
          Cr(i,0)=0.D0
        enddo
        do k=1,N-1,+1
          do i=imin,imax
            zscale=z_r(i,j,k)-z_w(i,j,0)
            Kern=zscale/(zscale+Zob)
            Cr(i,k)=Cr(i,k-1) + Kern*(
     &                 0.5D0*( ( u(i  ,j,k+1,nstp)-u(i  ,j,k,nstp)
     &                        +u(i+1,j,k+1,nstp)-u(i+1,j,k,nstp) )**2
     &                      +( v(i,j  ,k+1,nstp)-v(i,j  ,k,nstp)
     &                        +v(i,j+1,k+1,nstp)-v(i,j+1,k,nstp) )**2
     &                      )/(Hz(i,j,k)+Hz(i,j,k+1))
     &               -0.5D0*(Hz(i,j,k)+Hz(i,j,k+1))*( Ri_inv*bvf(i,j,k)
     &                                            +C_Ek*f(i,j)*f(i,j)
     &                                                             ))
          enddo
        enddo
        do i=imin,imax
          Cr(i,N)=2.D0*Cr(i,N-1) -Cr(i,N-2)
        enddo
        do k=1,N,+1
          do i=imin,imax
            if (my_kbbl(i).eq.N .and. Cr(i,k).lt.0.D0) my_kbbl(i)=k
          enddo
        enddo
        do i=imin,imax
          my_hbbl(i,j)=z_w(i,j,N)-z_w(i,j,0)
          if (my_kbbl(i).lt.N) then
            k=my_kbbl(i)
            if (k.eq.1) then
              my_hbbl(i,j)=z_r(i,j,1)-z_w(i,j,0)
            else
              my_hbbl(i,j)=( z_r(i,j,k)*Cr(i,k-1)-z_r(i,j,k-1)*Cr(i,k)
     &                            )/(Cr(i,k-1)-Cr(i,k)) -z_w(i,j,0)
            endif
          endif
        enddo
      enddo
      if (.not.WEST_INTER) then
        do j=jmin,jmax
          my_hbbl(Istr-1,j)=my_hbbl(Istr,j)
        enddo
      endif
      if (.not.EAST_INTER) then
        do j=jmin,jmax
          my_hbbl(Iend+1,j)=my_hbbl(Iend,j)
        enddo
      endif
      if (.not.SOUTH_INTER) then
        do i=imin,imax
          my_hbbl(i,Jstr-1)=my_hbbl(i,Jstr)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=imin,imax
          my_hbbl(i,Jend+1)=my_hbbl(i,Jend)
        enddo
      endif
      if (.not.WEST_INTER.and..not.SOUTH_INTER) then
        my_hbbl(Istr-1,Jstr-1)=my_hbbl(Istr,Jstr)
      endif
      if (.not.WEST_INTER.and..not.NORTH_INTER) then
        my_hbbl(Istr-1,Jend+1)=my_hbbl(Istr,Jend)
      endif
      if (.not.EAST_INTER.and..not.SOUTH_INTER) then
        my_hbbl(Iend+1,Jstr-1)=my_hbbl(Iend,Jstr)
      endif
      if (.not.EAST_INTER.and..not.NORTH_INTER) then
        my_hbbl(Iend+1,Jend+1)=my_hbbl(Iend,Jend)
      endif
      do j=Jstr,Jend+1
        do i=Istr,Iend+1
          wrk(i,j)=0.25D0*(my_hbbl(i,j)  +my_hbbl(i-1,j)
     &                  +my_hbbl(i,j-1)+my_hbbl(i-1,j-1))
     &                   *pmask2(i,j)
        enddo
      enddo
      do j=Jstr,Jend
        do i=Istr,Iend
          cff=0.25D0*(pmask2(i,j)   +pmask2(i+1,j)
     &             +pmask2(i,j+1) +pmask2(i+1,j+1))
          my_hbbl(i,j)=(1.D0-cff)*my_hbbl(i,j)+
     &              0.25D0*(wrk(i,j)  +wrk(i+1,j)
     &                   +wrk(i,j+1)+wrk(i+1,j+1))
          my_hbbl(i,j)=my_hbbl(i,j)*rmask(i,j)
        enddo
      enddo
      do j=Jstr,Jend
        do i=Istr,Iend
          hbbl(i,j)=min(my_hbbl(i,j),z_w(i,j,N)-z_w(i,j,0))
     &                                          *rmask(i,j)
          kbbl(i,j)=N
        enddo
        do k=N-1,1,-1
          do i=Istr,Iend
            if (z_r(i,j,k)-z_w(i,j,0).gt.hbbl(i,j)) kbbl(i,j)=k
          enddo
        enddo
        do i=Istr,Iend
          k=kbbl(i,j)
          z_bl=z_w(i,j,0)+hbbl(i,j)
          if (z_bl.lt.z_w(i,j,k-1)) k=k-1
          cff=1.D0/(z_w(i,j,k)-z_w(i,j,k-1))
          cff_up=cff*(z_bl -z_w(i,j,k-1))
          cff_dn=cff*(z_w(i,j,k)   -z_bl)
          Av_bl=cff_up*Kv(i,j,k)+cff_dn*Kv(i,j,k-1)
          dAv_bl=cff * (Kv(i,j,k)  -   Kv(i,j,k-1))
          Gm1(i)=Av_bl/(hbbl(i,j)*wm(i,j)+eps)
          dGm1dS(i)=min(0.D0, -dAv_bl/(wm(i,j)+eps))
          At_bl=cff_up*Kt(i,j,k)+cff_dn*Kt(i,j,k-1)
          dAt_bl=cff * (Kt(i,j,k)  -   Kt(i,j,k-1))
          Gt1(i)=At_bl/(hbbl(i,j)*ws(i,j)+eps)
          dGt1dS(i)=min(0.D0, -dAt_bl/(ws(i,j)+eps))
          As_bl=cff_up*Ks(i,j,k)+cff_dn*Ks(i,j,k-1)
          dAs_bl=cff * (Ks(i,j,k)  -   Ks(i,j,k-1))
          Gs1(i)=As_bl/(hbbl(i,j)*ws(i,j)+eps)
          dGs1dS(i)=min(0.D0, -dAs_bl/(ws(i,j)+eps))
        enddo
        do i=Istr,Iend
          do k=1,N-1
            if (k.lt.kbbl(i,j)) then
              sigma=min((z_w(i,j,k)-z_w(i,j,0))/(hbbl(i,j)+eps),1.D0)
              a1=sigma-2.D0
              a2=3.D0-2.D0*sigma
              a3=sigma-1.D0
              Kv0 =wm(i,j)*hbbl(i,j)*( sigma*( 1.D0+sigma*(
     &                            a1+a2*Gm1(i)+a3*dGm1dS(i) )))
              Kt0 =ws(i,j)*hbbl(i,j)*( sigma*( 1.D0+sigma*(
     &                            a1+a2*Gt1(i)+a3*dGt1dS(i) )))
              Ks0 =ws(i,j)*hbbl(i,j)*( sigma*( 1.D0+sigma*(
     &                            a1+a2*Gs1(i)+a3*dGs1dS(i) )))
              zsbl=z_w(i,j,N)-hbls(i,j,3-nstp)
              if (z_w(i,j,k).gt.zsbl) then
                Kv0=max(Kv(i,j,k),Kv0)
                Kt0=max(Kt(i,j,k),Kt0)
                Ks0=max(Ks(i,j,k),Ks0)
              endif
              Kv(i,j,k)=Kv0
              Kt(i,j,k)=Kt0
              Ks(i,j,k)=Ks0
            else
               if (bvf(i,j,k).lt.0.D0) then
                zsbl=z_w(i,j,N)-hbls(i,j,3-nstp)
                if (z_w(i,j,k).lt.zsbl) then
                  Kv(i,j,k)=Kv(i,j,k)+lmd_nu0c
                  Kt(i,j,k)=Kt(i,j,k)+lmd_nu0c
                  Ks(i,j,k)=Ks(i,j,k)+lmd_nu0c
                endif
              endif
            endif
          enddo
        enddo
      enddo
      if (.not.WEST_INTER) then
        do j=jstr,jend
          hbbl(istr-1,j)=hbbl(istr,j)
        enddo
      endif
      if (.not.EAST_INTER) then
        do j=jstr,jend
          hbbl(iend+1,j)=hbbl(iend,j)
        enddo
      endif
      if (.not.SOUTH_INTER) then
        do i=istr,iend
          hbbl(i,jstr-1)=hbbl(i,jstr)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=istr,iend
          hbbl(i,jend+1)=hbbl(i,jend)
        enddo
      endif
      if (.not.WEST_INTER .and. .not.SOUTH_INTER) then
        hbbl(istr-1,jstr-1)=hbbl(istr,jstr)
      endif
      if (.not.WEST_INTER .and. .not.NORTH_INTER) then
        hbbl(istr-1,jend+1)=hbbl(istr,jend)
      endif
      if (.not.EAST_INTER .and. .not.SOUTH_INTER) then
        hbbl(iend+1,jstr-1)=hbbl(iend,jstr)
      endif
      if (.not.EAST_INTER .and. .not.NORTH_INTER) then
        hbbl(iend+1,jend+1)=hbbl(iend,jend)
      endif
      call exchange_r2d_tile (Istr,Iend,Jstr,Jend,hbbl(-1,-1))
      return
       
      

      end subroutine Sub_Loop_lmd_bkpp_tile

