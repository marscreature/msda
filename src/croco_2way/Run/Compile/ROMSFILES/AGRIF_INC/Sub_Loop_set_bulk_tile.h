      subroutine Sub_Loop_set_bulk_tile(Istr,Iend,Jstr,Jend,padd_E,Mm,pa
     &dd_X,Lm,may_day_flag,tdays,synchro_flag,vwndp,uwndp,pratep,radswp,
     &radlwp,rhump,tairp,wspdp,vwndg,vwnd,uwndg,uwnd,prateg,stf_scale,pr
     &ate,srflx,radswg,radsw,radlwg,srf_scale,radlw,rhumg,rhum,tairg,tai
     &r,wspdg,wspd,ltairgrd,ntstart,iic,bulk_cycle,bulk_time,dt,time,itb
     &ulk,NORTH_INTER,Mmmpi,SOUTH_INTER,EAST_INTER,Lmmpi,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: may_day_flag
      real :: tdays
      logical :: synchro_flag
      real :: srf_scale
      integer(4) :: ltairgrd
      integer(4) :: ntstart
      integer(4) :: iic
      real :: bulk_cycle
      real :: dt
      real :: time
      integer(4) :: itbulk
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(1:2) :: vwndp
      real, dimension(1:2) :: uwndp
      real, dimension(1:2) :: pratep
      real, dimension(1:2) :: radswp
      real, dimension(1:2) :: radlwp
      real, dimension(1:2) :: rhump
      real, dimension(1:2) :: tairp
      real, dimension(1:2) :: wspdp
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vwndg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vwnd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: uwndg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: uwnd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: prateg
      real, dimension(1:NT) :: stf_scale
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: prate
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: radswg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: radsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: radlwg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: radlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: rhumg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhum
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: tairg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: tair
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: wspdg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: wspd
      real, dimension(1:2) :: bulk_time
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                  
      integer(4) :: i
      integer(4) :: j
      integer(4) :: it1
      integer(4) :: it2
                                    
      real :: cff
      real :: cff1
      real :: cff2
      real :: cff3
      real :: cff4
                     
      real :: cff5
      real :: cff6
                                                   
      real :: val1
      real :: val2
      real :: val3
      real :: val4
      real :: val5
      real :: val6
      real :: val7
      real :: val8
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
      if (Istr.eq.1) then
        if (WEST_INTER) then
          IstrR=Istr-2
        else
          IstrR=Istr-1
        endif
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lmmpi) then
        if (EAST_INTER) then
          IendR=Iend+2
        else
          IendR=Iend+1
        endif
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        if (SOUTH_INTER) then
          JstrR=Jstr-2
        else
          JstrR=Jstr-1
        endif
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mmmpi) then
        if (NORTH_INTER) then
          JendR=Jend+2
        else
          JendR=Jend+1
        endif
      else
        JendR=Jend
      endif
      it1=3-itbulk
      it2=itbulk
      cff=time+0.5D0*dt
      cff1=bulk_time(it2)-cff
      cff2=cff-bulk_time(it1)
      if (bulk_cycle.lt.0.D0) then
        if (iic.eq.ntstart) then
          if (ltairgrd.eq.1) then
            do j=JstrR,JendR
              do i=IstrR,IendR
                wspd(i,j)=wspdg(i,j,itbulk)
                tair(i,j)=tairg(i,j,itbulk)
                rhum(i,j)=rhumg(i,j,itbulk)
                radlw(i,j)=srf_scale*radlwg(i,j,itbulk)
                radsw(i,j)=srf_scale*radswg(i,j,itbulk)
                srflx(i,j)=radsw(i,j)
                prate(i,j)=stf_scale(isalt)*prateg(i,j,itbulk)
                uwnd(i,j)=uwndg(i,j,itbulk)
                vwnd(i,j)=vwndg(i,j,itbulk)
              enddo
            enddo
          else
            val1=wspdp(itbulk)
            val2=tairp(itbulk)
            val3=rhump(itbulk)
            val4=srf_scale*radlwp(itbulk)
            val5=srf_scale*radswp(itbulk)
            val6=stf_scale(isalt)*pratep(itbulk)
            val7=uwndp(itbulk)
            val8=vwndp(itbulk)
            do j=JstrR,JendR
              do i=IstrR,IendR
                wspd(i,j)=val1
                tair(i,j)=val2
                rhum(i,j)=val3
                radlw(i,j)=val4
                radsw(i,j)=val5
                srflx(i,j)=val5
                prate(i,j)=val6
                uwnd(i,j)=val7
                vwnd(i,j)=val8
              enddo
            enddo
          endif
        endif
      elseif (cff1.ge.0.D0 .and. cff2.ge.0.D0) then
        if (Istr+Jstr.eq.2 .and. cff1.lt.dt) synchro_flag=.TRUE.
        cff=srf_scale/(cff1+cff2)
        cff3=cff1*cff
        cff4=cff2*cff
        cff=stf_scale(isalt)/(cff1+cff2)
        cff5=cff1*cff
        cff6=cff2*cff
        cff=1.D0/(cff1+cff2)
        cff1=cff1*cff
        cff2=cff2*cff
        if (ltairgrd.eq.1) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              wspd(i,j)=cff1*wspdg(i,j,it1)+cff2*wspdg(i,j,it2)
              tair(i,j)=cff1*tairg(i,j,it1)+cff2*tairg(i,j,it2)
              rhum(i,j)=cff1*rhumg(i,j,it1)+cff2*rhumg(i,j,it2)
              radlw(i,j)=cff3*radlwg(i,j,it1)+cff4*radlwg(i,j,it2)
              radsw(i,j)=cff3*radswg(i,j,it1)+cff4*radswg(i,j,it2)
              srflx(i,j)=radsw(i,j)
              prate(i,j)=cff5*prateg(i,j,it1)+cff6*prateg(i,j,it2)
              uwnd(i,j)=cff1*uwndg(i,j,it1)+cff2*uwndg(i,j,it2)
              vwnd(i,j)=cff1*vwndg(i,j,it1)+cff2*vwndg(i,j,it2)
            enddo
          enddo
        else
          val1=cff1*wspdp(it1)+cff2*wspdp(it2)
          val2=cff1*tairp(it1)+cff2*tairp(it2)
          val3=cff1*rhump(it1)+cff2*rhump(it2)
          val4=cff3*radlwp(it1)+cff4*radlwp(it2)
          val5=cff3*radswp(it1)+cff4*radswp(it2)
          val6=cff5*pratep(it1)+cff6*pratep(it2)
          val7=cff1*uwndp(it1)+cff2*uwndp(it2)
          val8=cff1*vwndp(it1)+cff2*vwndp(it2)
          do j=JstrR,JendR
            do i=IstrR,IendR
              wspd(i,j)=val1
              tair(i,j)=val2
              rhum(i,j)=val3
              radlw(i,j)=val4
              radsw(i,j)=val5
              srflx(i,j)=val5
              prate(i,j)=val6
              uwnd(i,j)=val7
              vwnd(i,j)=val8
            enddo
          enddo
        endif
      else
        if (Istr+Jstr.eq.2) then
          write(stdout,1) 'bulk_time',tdays,bulk_time(it2)*sec2day
  1       format(/,' SET_BULK - current model time exceeds ending',
     &           1x,'value for variable: ',a,/,11x,'TDAYS = ',g12.4,
     &           2x,'TEND = ',g12.4)
          may_day_flag=2
        endif
      endif
      return
       
      

      end subroutine Sub_Loop_set_bulk_tile

