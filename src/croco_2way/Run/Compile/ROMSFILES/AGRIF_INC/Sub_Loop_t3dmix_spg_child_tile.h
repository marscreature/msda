      subroutine Sub_Loop_t3dmix_spg_child_tile(istr,iend,jstr,jend,itrc
     &,FX,FE,FC,dTdr,dTdx,dTde,FFC,CF,BC,CD,DC,diff3u,diff3v,Akz,padd_E,
     &Mm,padd_X,Lm,dt,nnew,dRde,om_v,dRdx,on_u,Hz,z_r,idRz,vmask,pn,tspo
     &nge,nstp,t,umask,pm,diff2_sponge,NORTH_INTER,SOUTH_INTER,EAST_INTE
     &R,WEST_INTER,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: dt
      integer(4) :: nnew
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: dRde
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: dRdx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: idRz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tsponge
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: diff2_sponge
      integer(4) :: istr
      integer(4) :: iend
      integer(4) :: jstr
      integer(4) :: jend
      integer(4) :: itrc
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: FX
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: FE
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Akz
      real, dimension(Istr-2:Iend+2,0:N) :: CF
      real, dimension(Istr-2:Iend+2,0:N) :: DC
      real, dimension(Istr-2:Iend+2,0:N) :: CD
      real, dimension(Istr-2:Iend+2,0:N) :: BC
      real, dimension(Istr-2:Iend+2,0:N) :: FFC
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: FC
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dTdr
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dTdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dTde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: diff3u
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: diff3v

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                                       
                                                                 
                                        
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: kmld
      integer(4) :: imin
      integer(4) :: imax
      integer(4) :: jmin
      integer(4) :: jmax
      integer(4) :: indx
      integer(4) :: idx
      integer(4) :: ide
                                                                       
                                                                 
                      
                      
      real :: cff
      real :: cff1
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                
                                                                       
                                                                 
                                                                 
                    
      real :: TRIADS1
      real :: TRIADS2
      real :: TRIADS3
      real :: TRIADS4
      real :: sumX
      real :: sumE
      real :: sig
      real :: SLOPEXQ1
      real :: SLOPEXQ2
      real :: SLOPEXQ3
      real :: SLOPEXQ4
      real :: SLOPEYQ1
      real :: SLOPEYQ2
      real :: SLOPEYQ3
      real :: SLOPEYQ4
                     
      real, dimension(0:4) :: wgt
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      wgt=(/0.D0,1.D0,0.5D0,0.33333333333D0,0.25D0/)
      if (.not.WEST_INTER) then
        imin=istr
      else
        imin=istr-1
      endif
      if (.not.EAST_INTER) then
        imax=iend
      else
        imax=iend+1
      endif
      if (.not.SOUTH_INTER) then
        jmin=jstr
      else
        jmin=jstr-1
      endif
      if (.not.NORTH_INTER) then
        jmax=jend
      else
        jmax=jend+1
      endif
      do j=jmin,jmax
        do i=imin,imax+1
          diff3u(i,j)=0.5D0*(diff2_sponge(i,j)+diff2_sponge(i-1,j))
        enddo
      enddo
      do j=jmin,jmax+1
        do i=imin,imax
          diff3v(i,j)=0.5D0*(diff2_sponge(i,j)+diff2_sponge(i,j-1))
        enddo
      enddo
      k2=1
      do k=0,N,+1
       k1=k2
       k2=3-k1
        if (k.lt.N) then
          do j=jmin,jmax
            do i=imin,imax+1
              cff=0.5D0*(pm(i,j)+pm(i-1,j)) * umask(i,j)
              dTdx(i,j,k2)=cff*( t(i  ,j,k+1,nstp,itrc)
     &                          -t(i-1,j,k+1,nstp,itrc)
     &                       -tsponge(i  ,j,k+1,itrc)
     &                       +tsponge(i-1,j,k+1,itrc)
     &                         )
            enddo
          enddo
          do j=jmin,jmax+1
            do i=imin,imax
              cff=0.5D0*(pn(i,j)+pn(i,j-1)) * vmask(i,j)
              dTde(i,j,k2)=cff*( t(i,j  ,k+1,nstp,itrc)
     &                          -t(i,j-1,k+1,nstp,itrc)
     &                       -tsponge(i,j  ,k+1,itrc)
     &                       +tsponge(i,j-1,k+1,itrc)
     &                         )
            enddo
          enddo
        endif
        if (k.eq.0 .or. k.eq.N) then
          do j=jmin-1,jmax+1
            do i=imin-1,imax+1
               FC  (i,j,k2) = 0.0D0
               Akz (i,j,k )= 0.0D0
             enddo
          enddo
          if (k.eq.0) then
            do j=jmin-1,jmax+1
              do i=imin-1,imax+1
                dTdr(i,j,k2)= idRz(i,j,1)*( t(i,j,2,nstp,itrc)
     &                                    - t(i,j,1,nstp,itrc)
     &                                 - tsponge(i,j,2,itrc)
     &                                 + tsponge(i,j,1,itrc)
     &                                     )
              enddo
            enddo
          endif
        else
          do j=jmin-1,jmax+1
            do i=imin-1,imax+1
              FC(i,j,k2)  = idRz(i,j,k)*( z_r (i,j,k+1)-z_r (i,j,k) )
              dTdr(i,j,k2)= idRz(i,j,k)*( t(i,j,k+1,nstp,itrc)
     &                                  - t(i,j,k  ,nstp,itrc)
     &                                -tsponge(i,j,k+1,itrc)
     &                                +tsponge(i,j,k  ,itrc)
     &                                  )
            enddo
          enddo
        endif
        if (k.gt.0) then
          cff=0.5D0
          do j=jmin,jmax
            do i=imin,imax+1
              FX(i,j)=cff*diff3u(i,j)*(Hz(i,j,k)+Hz(i-1,j,k))
     &               *on_u(i,j)*(   dTdx(i,j,k1) -
     &                0.25D0*dRdx(i,j,k)*( dTdr(i-1,j,k1)+dTdr(i,j,k2)
     &                                 + dTdr(i-1,j,k2)+dTdr(i,j,k1))
     &                                                              )
            enddo
          enddo
          do j=jmin,jmax+1
            do i=imin,imax
              FE(i,j)=cff*diff3v(i,j)*(Hz(i,j,k)+Hz(i,j-1,k))
     &               *om_v(i,j)*(   dTde(i,j,k1) -
     &              0.25D0*dRde(i,j,k)*(   dTdr(i,j-1,k1)+dTdr(i,j,k2)
     &                              +    dTdr(i,j-1,k2)+dTdr(i,j,k1))
     &                                                              )
            enddo
          enddo
          if (k.lt.N) then
            do j=jmin,jmax
              do i=imin,imax
                TRIADS1=dRdx(i  ,j,k  )*dTdr(i,j,k2)-dTdx(i  ,j,k1)
                TRIADS2=dRdx(i  ,j,k+1)*dTdr(i,j,k2)-dTdx(i  ,j,k2)
                TRIADS3=dRdx(i+1,j,k+1)*dTdr(i,j,k2)-dTdx(i+1,j,k2)
                TRIADS4=dRdx(i+1,j,k  )*dTdr(i,j,k2)-dTdx(i+1,j,k1)
                sumX = diff3u(i  ,j)*dRdx(i  ,j,k  )*TRIADS1
     &               + diff3u(i  ,j)*dRdx(i  ,j,k+1)*TRIADS2
     &               + diff3u(i+1,j)*dRdx(i+1,j,k+1)*TRIADS3
     &               + diff3u(i+1,j)*dRdx(i+1,j,k  )*TRIADS4
                idx = 4
                TRIADS1=dRde(i,j  ,k  )*dTdr(i,j,k2)-dTde(i,j  ,k1)
                TRIADS2=dRde(i,j  ,k+1)*dTdr(i,j,k2)-dTde(i,j  ,k2)
                TRIADS3=dRde(i,j+1,k+1)*dTdr(i,j,k2)-dTde(i,j+1,k2)
                TRIADS4=dRde(i,j+1,k  )*dTdr(i,j,k2)-dTde(i,j+1,k1)
                sumE = diff3v(i,j  )*dRde(i,j  ,k  )*TRIADS1
     &               + diff3v(i,j  )*dRde(i,j  ,k+1)*TRIADS2
     &               + diff3v(i,j+1)*dRde(i,j+1,k+1)*TRIADS3
     &               + diff3v(i,j+1)*dRde(i,j+1,k  )*TRIADS4
                ide = 4
                SLOPEXQ1=(FC(i,j,k2)*dRdx(i  ,j,k  ))**2
                SLOPEXQ2=(FC(i,j,k2)*dRdx(i  ,j,k+1))**2
                SLOPEXQ3=(FC(i,j,k2)*dRdx(i+1,j,k+1))**2
                SLOPEXQ4=(FC(i,j,k2)*dRdx(i+1,j,k  ))**2
                SLOPEYQ1=(FC(i,j,k2)*dRde(i,j  ,k  ))**2
                SLOPEYQ2=(FC(i,j,k2)*dRde(i,j  ,k+1))**2
                SLOPEYQ3=(FC(i,j,k2)*dRde(i,j+1,k+1))**2
                SLOPEYQ4=(FC(i,j,k2)*dRde(i,j+1,k  ))**2
                Akz(i,j,k) = max(
     &                       diff3u(i  ,j)*SLOPEXQ1,
     &                       diff3u(i  ,j)*SLOPEXQ2,
     &                       diff3u(i+1,j)*SLOPEXQ3,
     &                       diff3u(i+1,j)*SLOPEXQ4)
     &                      +max(
     &                       diff3v(i,j  )*SLOPEYQ1,
     &                       diff3v(i,j  )*SLOPEYQ2,
     &                       diff3v(i,j+1)*SLOPEYQ3,
     &                       diff3v(i,j+1)*SLOPEYQ4)
                FC(i,j,k2)=(sumX*wgt(idx)+sumE*wgt(ide))*FC(i,j,k2)
              enddo
            enddo
          endif
          do j=jstr,jend
            do i=istr,iend
              t(i,j,k,nnew,itrc)=Hz(i,j,k)*t(i,j,k,nnew,itrc)
     &         + dt*(
     &                   pm(i,j)*pn(i,j)*( FX(i+1,j)-FX(i,j)
     &                                    +FE(i,j+1)-FE(i,j))
     &                  +FC(i,j,k2)-FC(i,j,k1)    )
            enddo
          enddo
        endif
      enddo
      do j=jstr,jend
        indx=min(itrc,isalt)
        do i=istr,iend
          do k=1,N-1
            CD(i,k) = Akz(i,j,k)*
     &         (t(i,j,k+1,nstp,itrc)-t(i,j,k,nstp,itrc))
     &                     / ( z_r(i,j,k+1)-z_r(i,j,k) )
          enddo
          CD(i,0) = 0.D0
          CD(i,N) = 0.D0
        enddo
        do i=istr,iend
          FFC(i,1)=dt*(Akz(i,j,1))/( z_r(i,j,2)-z_r(i,j,1) )
          cff=1.D0/(Hz(i,j,1)+FFC(i,1))
          CF(i,1)= cff*FFC(i,1)
          DC(i,1)= cff*(t(i,j,1,nnew,itrc)-dt*(CD(i,1)-CD(i,0)))
        enddo
        do k=2,N-1,+1
          do i=istr,iend
            FFC(i,k)=dt*(Akz(i,j,k))/( z_r(i,j,k+1)-z_r(i,j,k) )
            cff=1.D0/(Hz(i,j,k)+FFC(i,k)+FFC(i,k-1)*(1.D0-CF(i,k-1)))
            CF(i,k)=cff*FFC(i,k)
            DC(i,k)=cff*(t(i,j,k,nnew,itrc)+FFC(i,k-1)*DC(i,k-1)
     &                                 -dt*(CD(i,k)-CD(i,k-1)))
          enddo
        enddo
        do i=istr,iend
           t(i,j,N,nnew,itrc)=( t(i,j,N,nnew,itrc)
     &                         -dt*(CD(i,N)-CD(i,N-1))
     &                         +FFC(i,N-1)*DC(i,N-1) )
     &                      /(Hz(i,j,N)+FFC(i,N-1)*(1.D0-CF(i,N-1)))
        enddo
        do k=N-1,1,-1
          do i=istr,iend
            t(i,j,k,nnew,itrc)=DC(i,k)+CF(i,k)*t(i,j,k+1,nnew,itrc)
          enddo
        enddo
      enddo
        call exchange_r3d_tile (Istr,Iend,Jstr,Jend,
     &                          t(-1,-1,1,nnew,itrc))
      return
       
      

      end subroutine Sub_Loop_t3dmix_spg_child_tile

