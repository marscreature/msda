      subroutine Sub_Loop_uv3dpremix_tile(Istr,Iend,Jstr,Jend,CF,DC,Mmmp
     &i,Lmmpi,padd_E,Mm,padd_X,Lm,DV_avg1,om_v,nstp,DU_avg1,on_u,Hz,nbst
     &ep3d,V_sponge_east,U_sponge_east,V_sponge_west,U_sponge_west,V_spo
     &nge_north,U_sponge_north,vmask,vsponge,V_sponge_south,umask,uspong
     &e,U_sponge_south,UVspongeTimeindex2,UVTimesponge,vspongeid,usponge
     &id,UVspongeTimeindex,nbcoarse,NORTH_INTER,SOUTH_INTER,EAST_INTER,W
     &EST_INTER)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      integer(4) :: nbstep3d
      integer(4) :: UVspongeTimeindex2
      integer(4) :: UVTimesponge
      integer(4) :: vspongeid
      integer(4) :: uspongeid
      integer(4) :: UVspongeTimeindex
      integer(4) :: nbcoarse
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(Lmmpi-9:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:2) :: V_spong
     &e_east
      real, dimension(Lmmpi-9:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:2) :: U_spong
     &e_east
      real, dimension(0:10,-1:Mm+2+padd_E,1:N,1:2) :: V_sponge_west
      real, dimension(1:11,-1:Mm+2+padd_E,1:N,1:2) :: U_sponge_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi-9:Mmmpi+1,1:N,1:2) :: V_spong
     &e_north
      real, dimension(-1:Lm+2+padd_X,Mmmpi-9:Mmmpi+1,1:N,1:2) :: U_spong
     &e_north
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vsponge
      real, dimension(-1:Lm+2+padd_X,1:11,1:N,1:2) :: V_sponge_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: usponge
      real, dimension(-1:Lm+2+padd_X,0:10,1:N,1:2) :: U_sponge_south
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: CF
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: DC

                   

                                                 
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: indx
                                                                       
                                                                 
                        
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                       
      integer(4) :: decal
                                       
      real :: tinterp
      real :: onemtinterp
      real :: rrhot
                         
      integer(4) :: nold
                          
      integer(4) :: irhot
       external interpspongeu, interpspongev
                                 
      integer(4) :: parentnbstep
                                                
                                                
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      irhot = Agrif_Irhot()
      rrhot = real(irhot)
      decal = 2*max(Agrif_Irhox(),Agrif_Irhoy())
      parentnbstep=Agrif_Parent_Nb_Step()
C$OMP BARRIER
C$OMP MASTER
      If ((nbcoarse == 1).AND.(UVspongeTimeindex .NE. parentnbstep)) 
     &                                                              THEN
      Call Agrif_Set_bc(uspongeid,(/-decal,0/),
     &         InterpolationShouldbemade=.TRUE.)
      Call Agrif_Set_bc(vspongeid,(/-decal,0/),
     &         InterpolationShouldbemade=.TRUE.)
       tinterp = 1.D0
      Call Agrif_Bc_Variable(uspongeid,calledweight=tinterp,
     &                         procname=interpspongeu)
      Call Agrif_Bc_Variable(vspongeid,calledweight=tinterp,
     &                         procname=interpspongev)
      UVTimesponge = 3 - UVTimesponge
        UVspongeTimeindex = parentnbstep
        UVspongeTimeindex2 = agrif_nb_step()
      ENDIF
C$OMP END MASTER
C$OMP BARRIER
       if (agrif_nb_step() .EQ. UVspongeTimeindex2) then
       if (.not.SOUTH_INTER) then
         do k=1,N
         do j=JstrR,JstrR+decal
         do i=Istr,IendR
          U_sponge_south(i,j,k,UVTimesponge)=
     &              usponge(i,j,k)
     &    *umask(i,j)
         enddo
         enddo
         do j=Jstr,Jstr+decal-1
         do i=IstrR,IendR
          V_sponge_south(i,j,k,UVTimesponge)=
     &              vsponge(i,j,k)
     &    *vmask(i,j)
         enddo
         enddo
         enddo
       endif
      if (.not.NORTH_INTER) then
         do k=1,N
         do j=JendR-decal,JendR
         do i=Istr,IendR
          U_sponge_north(i,j,k,UVTimesponge)=
     &              usponge(i,j,k)
     &    *vmask(i,j)
         enddo
         enddo
         do j=JendR-decal+1,JendR
         do i=IstrR,IendR
          V_sponge_north(i,j,k,UVTimesponge)=
     &              vsponge(i,j,k)
     &    *vmask(i,j)
         enddo
         enddo
         enddo
       endif
      if (.not.WEST_INTER) then
         do k=1,N
         do j=JstrR,JendR
         do i=Istr,Istr+decal-1
          U_sponge_west(i,j,k,UVTimesponge)=
     &              usponge(i,j,k)
         enddo
         enddo
         do j=Jstr,JendR
         do i=IstrR,IstrR+decal
          V_sponge_west(i,j,k,UVTimesponge)=
     &              vsponge(i,j,k)
         enddo
         enddo
         enddo
       endif
      if (.not.EAST_INTER) then
         do k=1,N
         do j=JstrR,JendR
         do i=IendR-decal+1,IendR
          U_sponge_east(i,j,k,UVTimesponge)=
     &              usponge(i,j,k)
         enddo
         enddo
         do j=Jstr,JendR
         do i=IendR-decal,IendR
          V_sponge_east(i,j,k,UVTimesponge)=
     &              vsponge(i,j,k)
         enddo
         enddo
         enddo
       endif
      ENDIF
       tinterp = real(nbcoarse-1)/rrhot
       IF (nbstep3d .LT. irhot) tinterp = 0.D0
       onemtinterp = -tinterp
       tinterp = 1.D0+tinterp
       nold = 3 - UVTimesponge
       if (.not.SOUTH_INTER) then
       do k=1,N
       do j=JstrR,JstrR+decal
       do i=Istr,IendR
          usponge(i,j,k) =
     &          onemtinterp*U_sponge_south(i,j,k,nold)
     &       +tinterp*U_sponge_south(i,j,k,UVTimesponge)
       enddo
       enddo
       do j=Jstr,Jstr+decal-1
       do i=IstrR,IendR
          vsponge(i,j,k) =
     &   onemtinterp*V_sponge_south(i,j,k,nold)
     &       +tinterp*V_sponge_south(i,j,k,UVTimesponge)
       enddo
       enddo
       enddo
       endif
       if (.not.NORTH_INTER) then
       do k=1,N
       do j=JendR-decal,JendR
       do i=Istr,IendR
          usponge(i,j,k) =
     &       onemtinterp*U_sponge_north(i,j,k,nold)
     &       +tinterp*U_sponge_north(i,j,k,UVTimesponge)
       enddo
       enddo
       do j=JendR-decal+1,JendR
       do i=IstrR,IendR
          vsponge(i,j,k) =
     &       onemtinterp*V_sponge_north(i,j,k,nold)
     &       +tinterp*V_sponge_north(i,j,k,UVTimesponge)
       enddo
       enddo
       enddo
       endif
      if (.not.WEST_INTER) then
         do k=1,N
         do j=JstrR,JendR
         do i=Istr,Istr+decal-1
          usponge(i,j,k) =
     &       onemtinterp*U_sponge_west(i,j,k,nold)
     &       +tinterp*U_sponge_west(i,j,k,UVTimesponge)
         enddo
         enddo
         do j=Jstr,JendR
         do i=IstrR,IstrR+decal
          vsponge(i,j,k) =
     &       onemtinterp*V_sponge_west(i,j,k,nold)
     &       +tinterp*V_sponge_west(i,j,k,UVTimesponge)
         enddo
         enddo
         enddo
       endif
      if (.not.EAST_INTER) then
         do k=1,N
         do j=JstrR,JendR
         do i=IendR-decal+1,IendR
          usponge(i,j,k) =
     &       onemtinterp*U_sponge_east(i,j,k,nold)
     &       +tinterp*U_sponge_east(i,j,k,UVTimesponge)
         enddo
         enddo
         do j=Jstr,JendR
         do i=IendR-decal,IendR
          vsponge(i,j,k) =
     &       onemtinterp*V_sponge_east(i,j,k,nold)
     &       +tinterp*V_sponge_east(i,j,k,UVTimesponge)
         enddo
         enddo
         enddo
       endif
         do j=JstrR,JendR
         do i=IstrR,IendR
          DC(i,j,0) = 0.D0
          CF(i,j,0) = 0.D0
         enddo
         enddo
         do k=1,N
         do j=JstrR,JendR
         do i=Istr,IendR
          DC(i,j,k) = 0.5D0*(Hz(i,j,k)+Hz(i-1,j,k))*on_u(i,j)
          DC(i,j,0) = DC(i,j,0) + DC(i,j,k)
          CF(i,j,0) = CF(i,j,0) + DC(i,j,k)*usponge(i,j,k)
         enddo
         enddo
         enddo
         do j=JstrR,JendR
         do i=Istr,IendR
         DC(i,j,0)=1.D0/DC(i,j,0)
         CF(i,j,0)=DC(i,j,0)
     &                   *(CF(i,j,0)-DU_avg1(i,j,nstp))
         enddo
         enddo
         do k = N,1,-1
         do j=JstrR,JendR
         do i=Istr,IendR
         usponge(i,j,k) = (usponge(i,j,k)-CF(i,j,0))
     &             * umask(i,j)
         enddo
         enddo
         enddo
         do j=JstrR,JendR
         do i=IstrR,IendR
          DC(i,j,0) = 0.D0
          CF(i,j,0) = 0.D0
         enddo
         enddo
         do k=1,N
         do j=Jstr,JendR
         do i=IstrR,IendR
          DC(i,j,k) = 0.5D0*(Hz(i,j,k)+Hz(i,j-1,k))*om_v(i,j)
          DC(i,j,0) = DC(i,j,0) + DC(i,j,k)
          CF(i,j,0) = CF(i,j,0) + DC(i,j,k)*vsponge(i,j,k)
         enddo
         enddo
         enddo
         do j=Jstr,JendR
         do i=IstrR,IendR
         DC(i,j,0)=1.D0/DC(i,j,0)
         CF(i,j,0)=DC(i,j,0)
     &                   *(CF(i,j,0)-DV_avg1(i,j,nstp))
         enddo
         enddo
         do k = N,1,-1
         do j=Jstr,JendR
         do i=IstrR,IendR
         vsponge(i,j,k) = (vsponge(i,j,k)-CF(i,j,0))
     &       * vmask(i,j)
         enddo
         enddo
         enddo
      call exchange_u3d_tile (Istr,Iend,Jstr,Jend,
     &                                 usponge(-1,-1,1))
      call exchange_v3d_tile (Istr,Iend,Jstr,Jend,
     &                                 vsponge(-1,-1,1))
      return
       
      

      end subroutine Sub_Loop_uv3dpremix_tile

