      subroutine Sub_Loop_diag_tile(Istr,Iend,Jstr,Jend,ke2d,pe2d,padd_E
     &,Mm,padd_X,Lm,may_day_flag,tdays,first_time,avgkp,buff,mynode,avgp
     &e,avgke,volume,tile_count,Mmmpi,Lmmpi,pn,pm,z_r,rho,v,nstp,u,Hz,rh
     &o0,z_w,ninfo,iic)

      use Agrif_Types, only : Agrif_tabvars


      include 'mpif.h'
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: may_day_flag
      real :: tdays
      integer(4) :: first_time
      real(8) :: avgkp
      integer(4) :: mynode
      real(8) :: avgpe
      real(8) :: avgke
      real(8) :: volume
      integer(4) :: tile_count
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: nstp
      real :: rho0
      integer(4) :: ninfo
      integer(4) :: iic
      real(8), dimension(1:6) :: buff
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: z_w
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: ke2d
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: pe2d

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

!$AGRIF_DO_NOT_TREAT
      INTEGER*4 :: ocean_grid_comm
      common /cpl_comm/ ocean_grid_comm
!$AGRIF_END_DO_NOT_TREAT
                                                                       
                                                                 
                                            
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: NSUB
      integer(4) :: trd
      integer(4) :: omp_get_thread_num
                                                                       
                                                                 
                    
                                                
      real(8) :: cff
      real(8) :: my_avgke
      real(8) :: my_avgpe
      real(8) :: my_volume
                        
      character(8) :: echar
                       
                                                          
      integer(4) :: size
      integer(4) :: step
      integer(4), dimension(1:MPI_STATUS_SIZE) :: status
      integer(4) :: ierr
                     
                        
      if (mod(iic-1,ninfo).eq.0) then
        do j=Jstr,Jend
          do i=Istr,Iend
            ke2d(i,j)=0.D0
            pe2d(i,j)=0.5D0*g*z_w(i,j,N)*z_w(i,j,N)
          enddo
          cff=g/rho0
          do k=N,1,-1
            do i=Istr,Iend
             ke2d(i,j)=ke2d(i,j)+Hz(i,j,k)*0.25D0*(
     &                               u(i  ,j,k,nstp)*u(i,j,k,nstp)+
     &                               u(i+1,j,k,nstp)*u(i+1,j,k,nstp)+
     &                               v(i,j  ,k,nstp)*v(i,j  ,k,nstp)+
     &                               v(i,j+1,k,nstp)*v(i,j+1,k,nstp))
             pe2d(i,j)=pe2d(i,j)+cff*Hz(i,j,k)*rho(i,j,k)
     &                                       *(z_r(i,j,k)-z_w(i,j,0))
            enddo
          enddo
        enddo
        do i=Istr,Iend
          pe2d(i,Jend+1)=0.D0
          pe2d(i,Jstr-1)=0.D0
          ke2d(i,Jstr-1)=0.D0
        enddo
        do j=Jstr,Jend
          do i=Istr,Iend
            cff=1.D0/(pm(i,j)*pn(i,j))
            pe2d(i,Jend+1)=pe2d(i,Jend+1)+cff*(z_w(i,j,N)-z_w(i,j,0))
            pe2d(i,Jstr-1)=pe2d(i,Jstr-1)+cff*pe2d(i,j)
            ke2d(i,Jstr-1)=ke2d(i,Jstr-1)+cff*ke2d(i,j)
          enddo
        enddo
        my_volume=0.D0
        my_avgpe=0.D0
        my_avgke=0.D0
        do i=Istr,Iend
          my_volume=my_volume+pe2d(i,Jend+1)
          my_avgpe =my_avgpe +pe2d(i,Jstr-1)
          my_avgke =my_avgke +ke2d(i,Jstr-1)
        enddo
        if (Iend-Istr+Jend-Jstr.eq.Lmmpi+Mmmpi-2) then
          NSUB=1
        else
          NSUB=NSUB_X*NSUB_E
        endif
C$OMP CRITICAL (diag_cr_rgn)
          if (tile_count.eq.0) then
            volume=0.D0
            avgke= 0.D0
            avgpe= 0.D0
          endif
          volume=volume+my_volume
          avgke =avgke +my_avgke
          avgpe =avgpe +my_avgpe
          tile_count=tile_count+1
          if (tile_count.eq.NSUB) then
            tile_count=0
            if (NNODES.gt.1) then
              size=NNODES
   1           step=(size+1)/2
                if (mynode.ge.step .and. mynode.lt.size) then
                  buff(1)=volume
                  buff(2)=avgke
                  buff(3)=avgpe
                  call MPI_Send (buff,  6, MPI_DOUBLE_PRECISION,
     &                 mynode-step, 17, MPI_COMM_WORLD,      ierr)
                elseif (mynode .lt. size-step) then
                  call MPI_Recv (buff,  6, MPI_DOUBLE_PRECISION,
     &                 mynode+step, 17, MPI_COMM_WORLD, status, ierr)
                  volume=volume+buff(1)
                  avgke=avgke+  buff(2)
                  avgpe=avgpe+  buff(3)
                endif
               size=step
              if (size.gt.1) goto 1
            endif
            if (mynode.eq.0) then
              avgke=avgke/volume
              avgpe=avgpe/volume
              avgkp=avgke+avgpe
              if (first_time.eq.0) then
                first_time=1
                write(stdout,2) 'STEP','time[DAYS]','KINETIC_ENRG',
     &                  'POTEN_ENRG','TOTAL_ENRG','NET_VOLUME','trd'
   2            format(1x,A4,3x,A10,1x,A12,4x,A10,4x,A10,4x,A10,3x,A3)
              endif
              trd=omp_get_thread_num()
              write(stdout,3)iic-1,tdays,avgke,avgpe,avgkp,volume,trd
   3          format(I8, F12.5, 1PE16.9, 3(1PE14.7), I3)
              write(echar,'(1PE8.1)') avgkp
              do i=1,8
               if (echar(i:i).eq.'N' .or. echar(i:i).eq.'n'
     &                      .or. echar(i:i).eq.'*') may_day_flag=1
              enddo
            endif
          endif
C$OMP END CRITICAL (diag_cr_rgn)
      endif
      return
       
      

      end subroutine Sub_Loop_diag_tile

