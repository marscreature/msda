      integer(4), parameter :: NWEIGHT = 1000
      interface
        subroutine Sub_Loop_zetabc_interp_tile(Istr,Iend,Jstr,Jend,Zeta_
     &west4,Zeta_east4,Zeta_west6,Zeta_east6,Zeta_south4,Zeta_north4,Zet
     &a_south6,Zeta_north6,padd_E,Mm,padd_X,Lm,SSH,nfast,Zt_avg3,weight2
     &,ZetaTimeindex2,zetaid,dZtinterp,ZetaTimeindex,iif,NORTH_INTER,SOU
     &TH_INTER,EAST_INTER,WEST_INTER,Mmmpi,Lmmpi)
          use AGRIF_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nfast
      integer(4) :: ZetaTimeindex2
      integer(4) :: zetaid
      integer(4) :: ZetaTimeindex
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: SSH
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: Zt_avg
     &3
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dZtinterp
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-1:Istr,-1:Mm+2+padd_E) :: Zeta_west6
      real, dimension(Iend:Iend+1,-1:Mm+2+padd_E) :: Zeta_east6
      real, dimension(-1:Lm+2+padd_X,Jstr-1:Jstr) :: Zeta_south6
      real, dimension(-1:Lm+2+padd_X,Jend:Jend+1) :: Zeta_north6
      real, dimension(Istr-1:Istr,-1:Mm+2+padd_E,0:NWEIGHT) :: Zeta_west
     &4
      real, dimension(Iend:Iend+1,-1:Mm+2+padd_E,0:NWEIGHT) :: Zeta_east
     &4
      real, dimension(-1:Lm+2+padd_X,Jstr-1:Jstr,0:NWEIGHT) :: Zeta_sout
     &h4
      real, dimension(-1:Lm+2+padd_X,Jend:Jend+1,0:NWEIGHT) :: Zeta_nort
     &h4
        end subroutine Sub_Loop_zetabc_interp_tile

      end interface
