      subroutine Sub_Loop_set_srflux_tile(Istr,Iend,Jstr,Jend,padd_E,Mm,
     &padd_X,Lm,may_day_flag,tdays,srflxp,srflxg,srf_scale,srflx,lsrfgrd
     &,ntstart,iic,srf_cycle,synchro_flag,srf_time,dt,time,itsrf,NORTH_I
     &NTER,Mmmpi,SOUTH_INTER,EAST_INTER,Lmmpi,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: may_day_flag
      real :: tdays
      real :: srf_scale
      integer(4) :: lsrfgrd
      integer(4) :: ntstart
      integer(4) :: iic
      real :: srf_cycle
      logical :: synchro_flag
      real :: dt
      real :: time
      integer(4) :: itsrf
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(1:2) :: srflxp
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: srflxg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx
      real, dimension(1:2) :: srf_time
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                  
      integer(4) :: i
      integer(4) :: j
      integer(4) :: it1
      integer(4) :: it2
                                
      real :: cff
      real :: cff1
      real :: cff2
      real :: srf
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
      if (Istr.eq.1) then
        if (WEST_INTER) then
          IstrR=Istr-2
        else
          IstrR=Istr-1
        endif
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lmmpi) then
        if (EAST_INTER) then
          IendR=Iend+2
        else
          IendR=Iend+1
        endif
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        if (SOUTH_INTER) then
          JstrR=Jstr-2
        else
          JstrR=Jstr-1
        endif
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mmmpi) then
        if (NORTH_INTER) then
          JendR=Jend+2
        else
          JendR=Jend+1
        endif
      else
        JendR=Jend
      endif
      it1=3-itsrf
      it2=itsrf
      cff=time+0.5D0*dt
      cff1=srf_time(it2)-cff
      cff2=cff-srf_time(it1)
      if (Istr+Jstr.eq.2.and. cff1.lt.dt) synchro_flag=.TRUE.
      if (srf_cycle.lt.0.D0) then
        if (iic.eq.ntstart) then
          if (lsrfgrd.eq.1) then
            do j=JstrR,JendR
              do i=IstrR,IendR
                srflx(i,j)=srf_scale*srflxg(i,j,itsrf)
              enddo
            enddo
          else
            srf=srf_scale*srflxp(itsrf)
            do j=JstrR,JendR
              do i=IstrR,IendR
                srflx(i,j)=srf
              enddo
            enddo
          endif
        endif
      elseif (cff1.ge.0.D0 .and. cff2.ge.0.D0) then
        cff=srf_scale/(cff1+cff2)
        cff1=cff1*cff
        cff2=cff2*cff
        if (lsrfgrd.eq.1) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              srflx(i,j)=cff1*srflxg(i,j,it1)+cff2*srflxg(i,j,it2)
            enddo
          enddo
        else
          srf=cff1*srflxp(it1)+cff2*srflxp(it2)
            do j=JstrR,JendR
              do i=IstrR,IendR
              srflx(i,j)=srf
            enddo
          enddo
        endif
      else
        if (Istr+Jstr.eq.2) then
          write(stdout,1) 'srf_time', tdays, srf_time(it2)*sec2day
  1       format(/,' SET_SRFLUX_TILE - current model time exceeds',
     &             ' ending value for variable: ',a,/,14x,'TDAYS = ',
     &                                      g12.4,2x,'TEND = ',g12.4)
          may_day_flag=2
        endif
      endif
      return
       
      

      end subroutine Sub_Loop_set_srflux_tile

