      subroutine Sub_Loop_set_tclima_tile(Istr,Iend,Jstr,Jend,padd_E,Mm,
     &padd_X,Lm,may_day_flag,tdays,tclima,tclm,iic,tclm_cycle,synchro_fl
     &ag,tclm_time,dt,time,ittclm,got_tclm,NORTH_INTER,Mmmpi,SOUTH_INTER
     &,EAST_INTER,Lmmpi,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: may_day_flag
      real :: tdays
      integer(4) :: iic
      logical :: synchro_flag
      real :: dt
      real :: time
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2,1:NT) :: tcl
     &ima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tclm
      real, dimension(1:NT) :: tclm_cycle
      real, dimension(1:2,1:NT) :: tclm_time
      integer(4), dimension(1:NT) :: ittclm
      logical, dimension(1:NT) :: got_tclm
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                                         
      integer(4) :: itrc
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: it1
      integer(4) :: it2
                              
      real :: cff
      real :: cff1
      real :: cff2
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
      if (Istr.eq.1) then
        if (WEST_INTER) then
          IstrR=Istr-2
        else
          IstrR=Istr-1
        endif
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lmmpi) then
        if (EAST_INTER) then
          IendR=Iend+2
        else
          IendR=Iend+1
        endif
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        if (SOUTH_INTER) then
          JstrR=Jstr-2
        else
          JstrR=Jstr-1
        endif
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mmmpi) then
        if (NORTH_INTER) then
          JendR=Jend+2
        else
          JendR=Jend+1
        endif
      else
        JendR=Jend
      endif
      do itrc=1,NT
       if (got_tclm(itrc)) then
        it1=3-ittclm(itrc)
        it2=ittclm(itrc)
        cff=time+0.5D0*dt
        cff1=tclm_time(it2,itrc)-cff
        cff2=cff-tclm_time(it1,itrc)
        if (Istr+Jstr.eq.2 .and. cff1.lt.dt) synchro_flag=.TRUE.
        if (tclm_cycle(itrc).lt.0.D0) then
          if (iic.eq.0) then
            do k=1,N
              do j=JstrR,JendR
                do i=IstrR,IendR
                  tclm(i,j,k,itrc)=tclima(i,j,k,ittclm(itrc),itrc)
                enddo
              enddo
            enddo
          endif
        elseif (cff1.ge.0.D0 .and. cff2.ge.0.D0) then
          cff=1.D0/(cff1+cff2)
          cff1=cff1*cff
          cff2=cff2*cff
          do k=1,N
            do j=JstrR,JendR
              do i=IstrR,IendR
                tclm(i,j,k,itrc)=cff1*tclima(i,j,k,it1,itrc)
     &                          +cff2*tclima(i,j,k,it2,itrc)
              enddo
            enddo
          enddo
        elseif (Istr+Jstr.eq.2) then
          write(stdout,'(/1x,2A/3(1x,A,F16.10)/)')
     &            'SET_TCLIMA_TILE - current model time is outside ',
     &            'bounds of ''tclm_time''.', 'TCLM_TSTART=',
     &             tclm_time(it1,itrc)*sec2day,     'TDAYS=',  tdays,
     &            'TCLM_TEND=',   tclm_time(it2,itrc)*sec2day
          may_day_flag=2
        endif
       endif
      enddo
      return
       
      

      end subroutine Sub_Loop_set_tclima_tile

