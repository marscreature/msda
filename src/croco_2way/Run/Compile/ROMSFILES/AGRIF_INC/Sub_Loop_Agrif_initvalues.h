      subroutine Sub_Loop_Agrif_initvalues(Mmmpi,Lmmpi,padd_E,Mm,padd_X,
     &Lm,vbarid,ubarid,hid,rmaskid,DV_avg1,DU_avg1,myfy,myfx,vsponge,usp
     &onge,UVTimesponge,TTimesponge,nbstep3d,nbcoarse,ntstart,iic,iif,UV
     &spongeTimeindex,TspongeTimeindex,T_east,V_east,U_east,EAST_INTER,T
     &_west,V_west,U_west,WEST_INTER,T_north,V_north,U_north,NORTH_INTER
     &,t,T_south,v,V_south,u,U_south,SOUTH_INTER,Vtimeindex,Utimeindex,T
     &timeindex,ZetaTimeindex,V2DTimeindex,U2DTimeindex,time,time_start,
     &kstp,next_kstp,wrthis,ldefhis,may_day_flag,numthreads,N1dETA,N1dXI
     &,eta_v,eta_rho,xi_u,xi_rho,MMm,LLm,N3d,N2d)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_UTIL
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: vbarid
      integer(4) :: ubarid
      integer(4) :: hid
      integer(4) :: rmaskid
      integer(4) :: UVTimesponge
      integer(4) :: TTimesponge
      integer(4) :: nbstep3d
      integer(4) :: nbcoarse
      integer(4) :: ntstart
      integer(4) :: iic
      integer(4) :: iif
      integer(4) :: UVspongeTimeindex
      integer(4) :: TspongeTimeindex
      logical :: EAST_INTER
      logical :: WEST_INTER
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      integer(4) :: Vtimeindex
      integer(4) :: Utimeindex
      integer(4) :: Ttimeindex
      integer(4) :: ZetaTimeindex
      integer(4) :: V2DTimeindex
      integer(4) :: U2DTimeindex
      real :: time
      real :: time_start
      integer(4) :: kstp
      integer(4) :: next_kstp
      logical :: ldefhis
      integer(4) :: may_day_flag
      integer(4) :: numthreads
      integer(4) :: N1dETA
      integer(4) :: N1dXI
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: MMm
      integer(4) :: LLm
      integer(4) :: N3d
      integer(4) :: N2d
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: myfy
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: myfx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vsponge
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: usponge
      real, dimension(Lmmpi:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_ea
     &st
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: V_east
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: U_east
      real, dimension(0:1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_west
      real, dimension(0:0,-1:Mm+2+padd_E,1:N,1:4) :: V_west
      real, dimension(1:1,-1:Mm+2+padd_E,1:N,1:4) :: U_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi:Mmmpi+1,1:N,1:4,1:NT) :: T_no
     &rth
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: V_north
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: U_north
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,0:1,1:N,1:4,1:NT) :: T_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,1:1,1:N,1:4) :: V_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,0:0,1:N,1:4) :: U_south
      logical, dimension(1:500+NT) :: wrthis

                     
                   

                                      
      integer(4) :: tile
      integer(4) :: ierr
      integer(4) :: trd
      integer(4) :: subs
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                
      integer(4) :: size_XI
      integer(4) :: size_ETA
      integer(4) :: se
      integer(4) :: sse
      integer(4) :: sz
      integer(4) :: ssz
                                                   
      real :: res1
      real :: res2
      real :: res3
      real :: res4
      real :: res5
      real :: res6
      real :: cff1
      real :: cff2
                    
      integer(4) :: i
      integer(4) :: j
                             
      integer(4) :: ipr
      integer(4) :: jpr
      integer(4) :: itrc
                                                                     
      real, pointer, dimension(:,:) :: hparent
      real, pointer, dimension(:,:) :: umaskparent
      real, pointer, dimension(:,:) :: rmaskparent
                                                
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: htest
                   
      real, dimension(1:5) :: tind
                                  
      integer(4) :: ipu
      integer(4) :: jpu
      integer(4) :: ipv
      integer(4) :: jpv
      integer(4) :: k
                                                
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: gradh
                                               
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbis
                                               
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hcur
                                                
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hmean
                                                
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbis2
                                                
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbis3
                                                 
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: CF
                                                 
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: DC
                       
      integer(4) :: nbloop
                             
      integer(4) :: nbconstraint
                      
      integer(4) :: n1
      integer(4) :: n2
                                    
      real, dimension(1:100000,1:2) :: valconstraints
                                            
      integer(4) :: iprmin
      integer(4) :: iprmax
      integer(4) :: jprmin
      integer(4) :: jprmax
                                                                       
                                                                 
                 
      real, allocatable, dimension(:,:) :: hconstraint
      real, allocatable, dimension(:,:) :: constraints
      real, allocatable, dimension(:,:) :: gradconstraints
                         
      integer(4) :: irhot
      external hinterp, updateh, updatermask
      external initzeta,initubar,initvbar,inith
      external initu,initv,initt
!$AGRIF_DO_NOT_TREAT
       integer*4 isens
       common/interph/isens
!$AGRIF_END_DO_NOT_TREAT
      Lm=(LLm+NP_XI-1)/NP_XI; Mm=(MMm+NP_ETA-1)/NP_ETA
      padd_X=(Lm+2)/2-(Lm+1)/2; padd_E=(Mm+2)/2-(Mm+1)/2
      xi_rho=LLm+2;  xi_u=xi_rho-1
      eta_rho=MMm+2; eta_v=eta_rho-1
      size_XI=7+(Lm+NSUB_X-1)/NSUB_X
      size_ETA=7+(Mm+NSUB_E-1)/NSUB_E
      sse=size_ETA/Np; ssz=Np/size_ETA
      se=sse/(sse+ssz);   sz=1-se
      N1dXI = size_XI
      N1dETA = size_ETA
      N2d=size_XI*(se*size_ETA+sz*Np)
      N3d=size_XI*size_ETA*Np
      call declare_zoom_variables()
      call MPI_Setup (ierr)
      if (ierr.ne.0) goto 100
      call read_inp (ierr)
      if (ierr.ne.0) goto 100
      call init_scalars (ierr)
      if (ierr.ne.0) goto 100
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call init_arrays (tile)
         enddo
       enddo
      call get_grid
      if (may_day_flag.ne.0) goto 99
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call setup_grid1 (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call setup_grid2 (tile)
         enddo
       enddo
      call set_scoord
      call set_weights
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call set_depth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call grid_stiffness (tile)
         enddo
       enddo
        call get_initial
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call ana_initial (tile)
         enddo
       enddo
      if (may_day_flag.ne.0) goto 99
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call set_depth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call set_HUV (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call omega (tile)
        call rho_eos (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call set_nudgcof_fine (tile)
         enddo
       enddo
      call get_vbc
      if (may_day_flag.ne.0) goto 99
      if (ldefhis .and. wrthis(indxTime)) call wrt_his
      if (may_day_flag.ne.0) goto 99
 99   continue
 100  continue
      next_kstp=kstp
      time_start=time
      U2DTimeindex = -1
      V2DTimeindex = -1
      ZetaTimeindex = -1
      Ttimeindex = -1
      Utimeindex = -1
      Vtimeindex = -1
            if (.not.SOUTH_INTER) then
                U_south(0:Lmmpi+1,0:0,1:N,4)=
     &          u(0:Lmmpi+1,0:0,1:N,1)
                V_south(0:Lmmpi+1,1:1,1:N,4)=
     &          v(0:Lmmpi+1,1:1,1:N,1)
                T_south(0:Lmmpi+1,0:1,1:N,4,1:NT)=
     &          t(0:Lmmpi+1,0:1,1:N,1,1:NT)
            endif
            if (.not.NORTH_INTER) then
                U_north(0:Lmmpi+1,Mmmpi+1:Mmmpi+1,1:N,4)=
     &          u(0:Lmmpi+1,Mmmpi+1:Mmmpi+1,1:N,1)
                V_north(0:Lmmpi+1,Mmmpi+1:Mmmpi+1,1:N,4)=
     &          v(0:Lmmpi+1,Mmmpi+1:Mmmpi+1,1:N,1)
                T_north(0:Lmmpi+1,Mmmpi:Mmmpi+1,1:N,4,1:NT)=
     &          t(0:Lmmpi+1,Mmmpi:Mmmpi+1,1:N,1,1:NT)
            endif
            if (.not.WEST_INTER) then
                U_west(1:1,0:Mmmpi+1,1:N,4)=
     &          u(1:1,0:Mmmpi+1,1:N,1)
                V_west(0:0,0:Mmmpi+1,1:N,4)=
     &            v(0:0,0:Mmmpi+1,1:N,1)
                T_west(0:1,0:Mmmpi+1,1:N,4,1:NT)=
     &          t(0:1,0:Mmmpi+1,1:N,1,1:NT)
            endif
            if (.not.EAST_INTER) then
                U_east(Lmmpi+1:Lmmpi+1,0:Mmmpi+1,1:N,4)=
     &          u(Lmmpi+1:Lmmpi+1,0:Mmmpi+1,1:N,1)
                V_east(Lmmpi+1:Lmmpi+1,0:Mmmpi+1,1:N,4)=
     &          v(Lmmpi+1:Lmmpi+1,0:Mmmpi+1,1:N,1)
                T_east(Lmmpi:Lmmpi+1,0:Mmmpi+1,1:N,4,1:NT)=
     &          t(Lmmpi:Lmmpi+1,0:Mmmpi+1,1:N,1,1:NT)
            endif
      TspongeTimeindex = -1
      UVspongeTimeindex = -1
      iif = -1
      iic = ntstart
      nbcoarse = 1
      nbstep3d = 0
      TTimesponge = 1
      UVTimesponge = 1
      usponge = 0.D0
      vsponge = 0.D0
      myfx = 0.D0
      myfy = 0.D0
      DU_avg1(:,:,4:5) = 0.D0
      DV_avg1(:,:,4:5) = 0.D0
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      call Agrif_Update_Variable(rmaskid,procname=updatermask)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      call Agrif_ChildGrid_To_ParentGrid()
      call ResetMask()
      call Agrif_ParentGrid_To_ChildGrid()
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      call Agrif_Update_Variable(hid,locupdate=(/1,0/),
     &     procname=updateh)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      isens = 1
      call Agrif_Bc_Variable(ubarid,calledweight=1.D0,procname=hinterp)
      isens = 2
      call Agrif_Bc_Variable(vbarid,calledweight=1.D0,procname=hinterp)
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      call Agrif_Update_Variable(hid,locupdate=(/1,0/),
     &     procname=updateh)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      call Agrif_ChildGrid_To_ParentGrid()
      call UpdateGridhis()
      call Agrif_ParentGrid_To_ChildGrid()
      return
       
      

      end subroutine Sub_Loop_Agrif_initvalues

