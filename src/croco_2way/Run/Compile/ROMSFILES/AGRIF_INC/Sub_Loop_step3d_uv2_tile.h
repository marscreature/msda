      subroutine Sub_Loop_step3d_uv2_tile(Istr,Iend,Jstr,Jend,BC,CF,FC,D
     &C,padd_E,Mm,padd_X,Lm,vclm,uclm,M3nudgcof,dRde,pn,DV_avg2,Hvom,vba
     &r,ismooth,dRdx,qp1,rho1,z_w,pm,DU_avg2,nstp,Huon,knew,ubar,vmask,D
     &V_avg1,om_v,bvstr,svstr,v,umask,DU_avg1,on_u,bustr,sustr,Hz,nnew,u
     &,z_r,Akv,dt,csmooth,ntstart,iic,NORTH_INTER,SOUTH_INTER,EAST_INTER
     &,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: ismooth
      integer(4) :: nstp
      integer(4) :: knew
      integer(4) :: nnew
      real :: dt
      real :: csmooth
      integer(4) :: ntstart
      integer(4) :: iic
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: M3nudgcof
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: dRde
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DV_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hvom
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: dRdx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: qp1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: z_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DU_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Huon
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,0:N) :: BC
      real, dimension(Istr-2:Iend+2,0:N) :: CF
      real, dimension(Istr-2:Iend+2,0:N) :: FC
      real, dimension(Istr-2:Iend+2,0:N) :: DC

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                                                       
                                                                 
                                                                 
    
      real :: cff
                                             
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad
                      
      real :: dpth
      real :: aa
      real :: cc
                          
      real :: dRx
      real :: dRe
      real :: cfilt
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      if (iic.eq.ntstart) then
       cfilt=1.D0
      else
       cfilt=csmooth
      endif
      do j=Jstr,Jend
        do i=IstrU,Iend
          FC(i,0)=0.D0
          FC(i,N)=0.D0
        enddo
        do k=1,N-1
          do i=IstrU,Iend
            FC(i,k)=-dt*(Akv(i,j,k)+Akv(i-1,j,k))
     &                 /( z_r(i,j,k+1)+z_r(i-1,j,k+1)
     &                   -z_r(i,j,k  )-z_r(i-1,j,k  ))
          enddo
        enddo
        do k=1,N
          do i=IstrU,Iend
            DC(i,k)=u(i,j,k,nnew)
            BC(i,k)=0.5D0*(Hz(i,j,k)+Hz(i-1,j,k))-FC(i,k)-FC(i,k-1)
          enddo
        enddo
        do i=IstrU,Iend
          DC(i,N)=DC(i,N)+dt*sustr(i,j)
          DC(i,1)=DC(i,1)-dt*bustr(i,j)
        enddo
        do i=IstrU,Iend
          cff=1.D0/BC(i,1)
          CF(i,1)=cff*( FC(i,1)
     &                                 )
          DC(i,1)=cff*DC(i,1)
        enddo
        do k=2,N-1
          do i=IstrU,Iend
              aa = FC(i,k-1)
              cc = FC(i,k  )
            cff=1.D0/(BC(i,k)-aa*CF(i,k-1))
            CF(i,k)=cff*cc
            DC(i,k)=cff*(DC(i,k)-aa*DC(i,k-1))
          enddo
        enddo
        do i=IstrU,Iend
              aa = FC(i,N-1)
          DC(i,N)=(DC(i,N)-aa*DC(i,N-1))
     &           /(BC(i,N)-aa*CF(i,N-1))
          CF(i,0)=0.5D0*(Hz(i,j,N)+Hz(i-1,j,N))
          DC(i,0)=CF(i,0)*DC(i,N)
        enddo
        do k=N-1,1,-1
          do i=IstrU,Iend
              DC(i,k)=DC(i,k)-CF(i,k)*DC(i,k+1)
            cff=0.5D0*(Hz(i,j,k)+Hz(i-1,j,k))
            CF(i,0)=CF(i,0)+cff
            DC(i,0)=DC(i,0)+cff*DC(i,k)
          enddo
        enddo
        do i=IstrU,Iend
          DC(i,0)=(DC(i,0)*on_u(i,j)-DU_avg1(i,j,nnew))
     &                             /(CF(i,0)*on_u(i,j))
        enddo
        do k=1,N
          do i=IstrU,Iend
            u(i,j,k,nnew)=(DC(i,k)-DC(i,0)) * umask(i,j)
          enddo
        enddo
        if (j.ge.JstrV) then
          do i=Istr,Iend
            FC(i,0)=0.D0
            FC(i,N)=0.D0
          enddo
          do k=1,N-1
            do i=Istr,Iend
              FC(i,k)=-dt*(Akv(i,j,k)+Akv(i,j-1,k))
     &                   /( z_r(i,j,k+1)+z_r(i,j-1,k+1)
     &                     -z_r(i,j,k  )-z_r(i,j-1,k  ))
            enddo
          enddo
          do k=1,N
            do i=Istr,Iend
              DC(i,k)=v(i,j,k,nnew)
              BC(i,k)=0.5D0*(Hz(i,j,k)+Hz(i,j-1,k))-FC(i,k)-FC(i,k-1)
            enddo
          enddo
          do i=Istr,Iend
            DC(i,N)=DC(i,N)+dt*svstr(i,j)
            DC(i,1)=DC(i,1)-dt*bvstr(i,j)
          enddo
          do i=Istr,Iend
            cff=1.D0/BC(i,1)
            CF(i,1)=cff*( FC(i,1)
     &                                 )
            DC(i,1)=cff*DC(i,1)
          enddo
          do k=2,N-1
            do i=Istr,Iend
              aa = FC(i,k-1)
              cc = FC(i,k  )
              cff=1.D0/(BC(i,k)-aa*CF(i,k-1))
              CF(i,k)=cff*cc
              DC(i,k)=cff*(DC(i,k)-aa*DC(i,k-1))
            enddo
          enddo
          do i=Istr,Iend
              aa = FC(i,N-1)
            DC(i,N)=(DC(i,N)-aa*DC(i,N-1))
     &             /(BC(i,N)-aa*CF(i,N-1))
            CF(i,0)=0.5D0*(Hz(i,j,N)+Hz(i,j-1,N))
            DC(i,0)=CF(i,0)*DC(i,N)
          enddo
          do k=N-1,1,-1
            do i=Istr,Iend
              DC(i,k)=DC(i,k)-CF(i,k)*DC(i,k+1)
              cff=0.5D0*(Hz(i,j,k)+Hz(i,j-1,k))
              CF(i,0)=CF(i,0)+cff
              DC(i,0)=DC(i,0)+cff*DC(i,k)
            enddo
          enddo
          do i=Istr,Iend
            DC(i,0)=(DC(i,0)*om_v(i,j)-DV_avg1(i,j,nnew))
     &                               /(CF(i,0)*om_v(i,j))
          enddo
          do k=1,N
            do i=Istr,Iend
              v(i,j,k,nnew)=(DC(i,k)-DC(i,0)) * vmask(i,j)
            enddo
          enddo
        endif
      enddo
      call v3dbc_tile (Istr,Iend,Jstr,Jend, grad)
      call u3dbc_tile (Istr,Iend,Jstr,Jend, grad)
      do j=JstrR,JendR
        do i=Istr,IendR
          DC(i,0)=0.D0
          CF(i,0)=0.D0
          FC(i,0)=0.D0
        enddo
        do k=1,N,+1
          do i=Istr,IendR
            DC(i,k)=0.5D0*(Hz(i,j,k)+Hz(i-1,j,k))*on_u(i,j)
            DC(i,0)=DC(i,0)+DC(i,k)
            CF(i,0)=CF(i,0)+DC(i,k)*u(i,j,k,nnew)
          enddo
        enddo
        do i=Istr,IendR
          DC(i,0)=1.D0/DC(i,0)
          CF(i,0)=DC(i,0)*(CF(i,0)-DU_avg1(i,j,nnew))
          ubar(i,j,knew)=DC(i,0)*DU_avg1(i,j,nnew)
        enddo
        do k=N,1,-1
          do i=Istr,IendR
            u(i,j,k,nnew)=(u(i,j,k,nnew)-CF(i,0))
     &                                *umask(i,j)
            FC(i,k)=0.75D0*Huon(i,j,k) +
     &              0.125D0*DC(i,k)*(u(i,j,k,nstp)+u(i,j,k,nnew))
            FC(i,0)=FC(i,0)+FC(i,k)
          enddo
        enddo
        do i=Istr,IendR
          FC(i,0)=DC(i,0)*(FC(i,0)-DU_avg2(i,j))
        enddo
        do k=1,N,+1
          do i=Istr,IendR
            Huon(i,j,k)=FC(i,k)-DC(i,k)*FC(i,0)
          enddo
        enddo
        do k=1,N-1,+1
          do i=Istr,IendR
            cff =0.5D0*(pm(i,j)+pm(i-1,j)) * umask(i,j)
            dpth=0.5D0*( z_w(i,j,N)+z_w(i-1,j,N)
     &                -z_r(i,j,k)-z_r(i-1,j,k))
            dRx=cff*( rho1(i,j,k)-rho1(i-1,j,k)
     &             +(  qp1(i,j,k)- qp1(i-1,j,k))
     &                     *dpth*(1.D0-qp2*dpth) )
            dRx  = cfilt*dRx + (1.D0-cfilt)*dRdx(i,j,k)
            dRdx(i,j,k)=dRx
          enddo
        enddo
        do i=Istr,IendR
          dRdx(i,j,N)=0.D0
        enddo
      if (mod(iic-ntstart,ismooth).eq.0) then
        do k=N-1,2,-1
          do i=Istr,IendR
            dRdx(i,j,k)=0.05D0*dRdx(i,j,k-1)+
     &                  0.90D0*dRdx(i,j,k  )+
     &                  0.05D0*dRdx(i,j,k+1)
          enddo
        enddo
      endif
        if (j.ge.Jstr) then
          do i=IstrR,IendR
            DC(i,0)=0.D0
            CF(i,0)=0.D0
            FC(i,0)=0.D0
          enddo
          do k=1,N,+1
            do i=IstrR,IendR
              DC(i,k)=0.5D0*(Hz(i,j,k)+Hz(i,j-1,k))*om_v(i,j)
              DC(i,0)=DC(i,0)+DC(i,k)
              CF(i,0)=CF(i,0)+DC(i,k)*v(i,j,k,nnew)
            enddo
          enddo
          do i=IstrR,IendR
            DC(i,0)=1.D0/DC(i,0)
            CF(i,0)=DC(i,0)*(CF(i,0)-DV_avg1(i,j,nnew))
            vbar(i,j,knew)=DC(i,0)*DV_avg1(i,j,nnew)
          enddo
          do k=N,1,-1
            do i=IstrR,IendR
              v(i,j,k,nnew)=(v(i,j,k,nnew)-CF(i,0))
     &                                  *vmask(i,j)
              FC(i,k)=0.75D0*Hvom(i,j,k)+
     &                0.125D0*DC(i,k)*(v(i,j,k,nstp)+v(i,j,k,nnew))
              FC(i,0)=FC(i,0)+FC(i,k)
            enddo
          enddo
          do i=IstrR,IendR
            FC(i,0)=DC(i,0)*(FC(i,0)-DV_avg2(i,j))
          enddo
          do k=1,N,+1
            do i=IstrR,IendR
              Hvom(i,j,k)=FC(i,k)-DC(i,k)*FC(i,0)
            enddo
          enddo
          do k=1,N-1,+1
            do i=IstrR,IendR
              cff=0.5D0*(pn(i,j)+pn(i,j-1)) * vmask(i,j)
              dpth=0.5D0*( z_w(i,j,N)+z_w(i,j-1,N)
     &                -  z_r(i,j,k)-z_r(i,j-1,k))
              dRe=cff*( rho1(i,j,k)-rho1(i,j-1,k)
     &               +(  qp1(i,j,k)- qp1(i,j-1,k))
     &                       *dpth*(1.D0-qp2*dpth) )
              dRe  = cfilt*dRe + (1.D0-cfilt)*dRde(i,j,k)
              dRde(i,j,k)=dRe
            enddo
          enddo
          do i=IstrR,IendR
            dRde(i,j,N)=0.D0
          enddo
      if (mod(iic-ntstart,ismooth).eq.0) then
        do k=N-1,2,-1
          do i=IstrR,IendR
            dRde(i,j,k)=0.05D0*dRde(i,j,k-1)+
     &                  0.90D0*dRde(i,j,k  )+
     &                  0.05D0*dRde(i,j,k+1)
          enddo
        enddo
      endif
        endif
      enddo
      do k=1,N
        do j=JstrR,JendR
          do i=Istr,IendR
            u(i,j,k,nnew)=u(i,j,k,nnew)+dt*M3nudgcof(i,j)*
     &                           (uclm(i,j,k)-u(i,j,k,nnew))
            u(i,j,k,nnew)=u(i,j,k,nnew)*umask(i,j)
          enddo
        enddo
      enddo
      do k=1,N
        do j=Jstr,JendR
          do i=IstrR,IendR
            v(i,j,k,nnew)=v(i,j,k,nnew)+dt*M3nudgcof(i,j)*
     &                            (vclm(i,j,k)-v(i,j,k,nnew))
            v(i,j,k,nnew)=v(i,j,k,nnew)*vmask(i,j)
          enddo
        enddo
      enddo
      call exchange_u3d_tile (Istr,Iend,Jstr,Jend,
     &                        u(-1,-1,1,nnew))
      call exchange_v3d_tile (Istr,Iend,Jstr,Jend,
     &                        v(-1,-1,1,nnew))
      call exchange_u3d_tile (Istr,Iend,Jstr,Jend,
     &                        Huon(-1,-1,1))
      call exchange_v3d_tile (Istr,Iend,Jstr,Jend,
     &                        Hvom(-1,-1,1))
      call exchange_u2d_tile (Istr,Iend,Jstr,Jend,
     &                        ubar(-1,-1,knew))
      call exchange_v2d_tile (Istr,Iend,Jstr,Jend,
     &                        vbar(-1,-1,knew))
      call exchange_u3d_tile (istr,iend,jstr,jend, dRdx  )
      call exchange_v3d_tile (istr,iend,jstr,jend, dRde  )
      return
       
      

      end subroutine Sub_Loop_step3d_uv2_tile

