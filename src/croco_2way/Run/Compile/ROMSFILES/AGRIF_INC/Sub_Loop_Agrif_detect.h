      subroutine Sub_Loop_Agrif_detect(taberr,sizexy,padd_E,Mm,padd_X,Lm
     &,u,nnew,v,Mmmpi,Lmmpi,N3d,N2d)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_types
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      integer(4) :: N2d
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      Integer(4), dimension(1:2) :: sizexy
      Integer(4), dimension(1:sizexy(1),1:sizexy(2)) :: taberr

                      
                   

                                      
      integer(4) :: tile
      integer(4) :: ierr
      integer(4) :: trd
      integer(4) :: subs
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                        
                                                           
                                                
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vort
                    
      integer(4) :: i
      integer(4) :: j
                
      real :: crit
      vort = 0.D0
      do j=1,Mm+1
      do i=1,Lm+1
        vort(i,j) = (v(i,j,N,nnew)-v(i-1,j,N,nnew))
     &     -(u(i,j,N,nnew)-u(i,j-1,N,nnew))
      enddo
      enddo
      crit = maxval(abs(vort))
      taberr=0.D0
      do j=1,Mm+1
      do i=1,Lm+1
          if (abs(vort(i,j)) > 0.8D0*crit) then
            taberr(i,j) = 1
          endif
      enddo
      enddo
      Return
             

      end subroutine Sub_Loop_Agrif_detect

