      subroutine Sub_Loop_lmd_swfrac_ER_tile(Istr,Iend,Jstr,Jend,Zscale,
     &Z,swdk,padd_E,Mm,padd_X,Lm,Jwtype,ntstart,iic,NORTH_INTER,SOUTH_IN
     &TER,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: ntstart
      integer(4) :: iic
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Jwtype
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real :: Zscale
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Z
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: swdk

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                      
                                  
                                              
      integer(4) :: i
      integer(4) :: j
      integer(4) :: indx
                                    
      integer(4) :: imin
      integer(4) :: imax
      integer(4) :: jmin
      integer(4) :: jmax
                                                                       
                                                                 
                                 
                        
      integer(4) :: lmd_Jwt
                                                                     
      real, dimension(1:5) :: lmd_mu1
      real, dimension(1:5) :: lmd_mu2
      real, dimension(1:5) :: lmd_r1
      real :: cff1
      real :: cff2
      real :: cff3
      real :: cff4
      lmd_mu1(1)=0.35D0
      lmd_mu1(2)=0.6D0
      lmd_mu1(3)=1.0D0
      lmd_mu1(4)=1.5D0
      lmd_mu1(5)=1.4D0
      lmd_mu2(1)=23.0D0
      lmd_mu2(2)=20.0D0
      lmd_mu2(3)=17.0D0
      lmd_mu2(4)=14.0D0
      lmd_mu2(5)=7.9D0
      lmd_r1(1)=0.58D0
      lmd_r1(2)=0.62D0
      lmd_r1(3)=0.67D0
      lmd_r1(4)=0.77D0
      lmd_r1(5)=0.78D0
      lmd_Jwt=1
      if (.not.WEST_INTER) then
        imin=Istr
      else
        imin=Istr-1
      endif
      if (.not.EAST_INTER) then
        imax=Iend
      else
        imax=Iend+1
      endif
      if (.not.SOUTH_INTER) then
        jmin=Jstr
      else
        jmin=Jstr-1
      endif
      if (.not.NORTH_INTER) then
        jmax=Jend
      else
        jmax=Jend+1
      endif
      if (iic.eq.ntstart) then
        do j=jmin,jmax
          do i=imin,imax
            Jwtype(i,j)=lmd_Jwt
          enddo
        enddo
      endif
      do j=jmin,jmax
        do i=imin,imax
          indx=Jwtype(i,j)
          cff1=Z(i,j)*Zscale/lmd_mu1(indx)
          cff2=Z(i,j)*Zscale/lmd_mu2(indx)
          if (cff1.ge.-20.D0) then
            cff3=lmd_r1(indx) *exp(cff1)
          else
            cff3=0.D0
          endif
          if (cff2.ge.-20.D0) then
            cff4=(1.D0-lmd_r1(indx)) *exp(cff2)
          else
            cff4=0.D0
          endif
          swdk(i,j)=cff3+cff4
        enddo
      enddo
      return
       
      

      end subroutine Sub_Loop_lmd_swfrac_ER_tile

