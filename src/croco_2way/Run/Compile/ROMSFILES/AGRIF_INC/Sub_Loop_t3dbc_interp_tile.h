      subroutine Sub_Loop_t3dbc_interp_tile(Istr,Iend,Jstr,Jend,indx,itr
     &c,Mmmpi,padd_X,Lm,padd_E,Mm,Lmmpi,T_east,T_west,T_north,rmask,T_so
     &uth,tclm,tid,TTimeindex,nnew,nbcoarse,nbstep3d,NORTH_INTER,SOUTH_I
     &NTER,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      integer(4) :: tid
      integer(4) :: TTimeindex
      integer(4) :: nnew
      integer(4) :: nbcoarse
      integer(4) :: nbstep3d
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(Lmmpi:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_ea
     &st
      real, dimension(0:1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi:Mmmpi+1,1:N,1:4,1:NT) :: T_no
     &rth
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,0:1,1:N,1:4,1:NT) :: T_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tclm
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: itrc
      integer(4) :: indx

                     
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                     
      integer(4) :: itrcind
      integer(4) :: ntind
                   
      real :: tinterp
                                  
      INTEGER(4) :: nbstep3dparent
      external t3dinterp
                      
      real, dimension(1:4) :: ainterp
                         
      integer(4) :: irhot
                    
      real :: rrhot
                                  
      integer(4) :: nsource
      integer(4) :: ndest
                                
      integer(4) :: parentnbstep
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      irhot = Agrif_Irhot()
      rrhot = real(irhot)
       nbstep3dparent=
     &Agrif_Parent(nbstep3d)
C$OMP BARRIER
C$OMP MASTER
      parentnbstep=Agrif_Parent_Nb_Step()
       IF ((((nbcoarse == 1).AND.(nnew == 3)).OR.
     &   ((nbcoarse == irhot).AND.(nnew /=3))).AND.
     &    (TTimeindex.NE.parentnbstep)) THEN
        tinterp=1.D0
        Agrif_UseSpecialvalue=.true.
        Agrif_Specialvalue=0.D0
        Call Agrif_Set_bc(tid,(/-1,0/),
     &     InterpolationShouldbemade=.TRUE.)
        Call Agrif_Bc_variable(tid,calledweight=tinterp,
     & procname = t3dinterp)
        Agrif_UseSpecialValue=.false.
      endif
C$OMP END MASTER
C$OMP BARRIER
       tinterp = real(nbcoarse)/rrhot
       IF (nnew == 3) tinterp = tinterp - 1.D0/(2.D0*rrhot)
       ainterp(3)=tinterp*4*(1+tinterp)
       ainterp(2)=-3.D0*(-1+tinterp+2*tinterp**2)
       ainterp(1)=tinterp*(-1+2*tinterp)
       ainterp = ainterp/3.D0
       ainterp(1) = 0.D0
       ainterp(2) = 1.D0-2*tinterp
       ainterp(3) = 2.D0*tinterp
       ainterp = 0.D0
       IF (nnew == 3) then
         ainterp(1) = 0.5D0-tinterp
         ainterp(2) = 0.5D0+tinterp
       else
         ainterp(3) = -tinterp
         ainterp(4) = 1.D0+tinterp
       endif
       IF (nbstep3dparent .LE. 1) THEN
       ainterp = 0.D0
       ainterp(2) = 1.D0
       ENDIF
       if (nbstep3d .LE. (Agrif_irhot()-1)) then
        ainterp = 0.D0
        ainterp(2) = 1.D0
       endif
       IF ((nnew/=3).AND.(nbcoarse == irhot)) THEN
         ainterp    = 0.D0
         ainterp(4) = 1.D0
       ENDIF
       if (.not.SOUTH_INTER) then
         do k=1,N
           do i=IstrR,IendR
            tclm(i,Jstr-1,k,itrc)=
     &     (ainterp(1)*T_south(i,Jstr-1,k,1,itrc)+
     &      ainterp(2)*T_south(i,Jstr-1,k,2,itrc)+
     &      ainterp(3)*T_south(i,Jstr-1,k,3,itrc)+
     &      ainterp(4)*T_south(i,Jstr-1,k,4,itrc))
     &        *rmask(i,Jstr-1)
          enddo
        enddo
      endif
      if (.not.NORTH_INTER) then
        do k=1,N
          do i=IstrR,IendR
            tclm(i,Jend+1,k,itrc)=
     &     (ainterp(1)*T_north(i,Jend+1,k,1,itrc)+
     &      ainterp(2)*T_north(i,Jend+1,k,2,itrc)+
     &      ainterp(3)*T_north(i,Jend+1,k,3,itrc)+
     &      ainterp(4)*T_north(i,Jend+1,k,4,itrc))
     &          *rmask(i,Jend+1)
          enddo
        enddo
      endif
      if (.not.WEST_INTER) then
        do k=1,N
          do j=JstrR,JendR
            tclm(Istr-1,j,k,itrc)=
     &     (ainterp(1)*T_west(Istr-1,j,k,1,itrc)+
     &      ainterp(2)*T_west(Istr-1,j,k,2,itrc)+
     &      ainterp(3)*T_west(Istr-1,j,k,3,itrc)+
     &      ainterp(4)*T_west(Istr-1,j,k,4,itrc))
     &         *rmask(Istr-1,j)
          enddo
        enddo
      endif
      if (.not.EAST_INTER) then
        do k=1,N
          do j=JstrR,JendR
            tclm(Iend+1,j,k,itrc)=
     &     (ainterp(1)*T_east(Iend+1,j,k,1,itrc)+
     &      ainterp(2)*T_east(Iend+1,j,k,2,itrc)+
     &      ainterp(3)*T_east(Iend+1,j,k,3,itrc)+
     &      ainterp(4)*T_east(Iend+1,j,k,4,itrc))
     &        *rmask(Iend+1,j)
           enddo
        enddo
      endif
      return
       
      

      end subroutine Sub_Loop_t3dbc_interp_tile

