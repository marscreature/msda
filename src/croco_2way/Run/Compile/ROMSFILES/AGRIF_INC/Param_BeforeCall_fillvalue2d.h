      interface
        subroutine Sub_Loop_fillvalue2d(work2d_tmp,ncid,vid,indx,record,
     &typevar2d,typefile,padd_E,Mm,padd_X,Lm,may_day_flag,mynode,vname,v
     &mask,umask,rmask)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4), parameter :: nf_byte = 1
      integer(4), parameter :: nf_int1 = nf_byte
      integer(4), parameter :: nf_char = 2
      integer(4), parameter :: nf_short = 3
      integer(4), parameter :: nf_int2 = nf_short
      integer(4), parameter :: nf_int = 4
      integer(4), parameter :: nf_float = 5
      integer(4), parameter :: nf_real = nf_float
      integer(4), parameter :: nf_double = 6
      integer(4), parameter :: nf_fill_byte = -127
      integer(4), parameter :: nf_fill_int1 = nf_fill_byte
      integer(4), parameter :: nf_fill_char = 0
      integer(4), parameter :: nf_fill_short = -32767
      integer(4), parameter :: nf_fill_int2 = nf_fill_short
      integer(4), parameter :: nf_fill_int = -2147483647
      real, parameter :: nf_fill_float = 9.9692099683868690D+36
      real, parameter :: nf_fill_real = nf_fill_float
      real(8), parameter :: nf_fill_double = 9.9692099683868690D+36
      integer(4), parameter :: nf_nowrite = 0
      integer(4), parameter :: nf_write = 1
      integer(4), parameter :: nf_clobber = 0
      integer(4), parameter :: nf_noclobber = 4
      integer(4), parameter :: nf_fill = 0
      integer(4), parameter :: nf_nofill = 256
      integer(4), parameter :: nf_lock = 1024
      integer(4), parameter :: nf_share = 2048
      integer(4), parameter :: nf_64bit_offset = 512
      integer(4), parameter :: nf_sizehint_default = 0
      integer(4), parameter :: nf_align_chunk = -1
      integer(4), parameter :: nf_format_classic = 1
      integer(4), parameter :: nf_format_64bit = 2
      integer(4), parameter :: nf_diskless = 8
      integer(4), parameter :: nf_mmap = 16
      integer(4), parameter :: nf_unlimited = 0
      integer(4), parameter :: nf_global = 0
      integer(4), parameter :: nf_max_dims = 1024
      integer(4), parameter :: nf_max_attrs = 8192
      integer(4), parameter :: nf_max_vars = 8192
      integer(4), parameter :: nf_max_name = 256
      integer(4), parameter :: nf_max_var_dims = nf_max_dims
      integer(4), parameter :: nf_noerr = 0
      integer(4), parameter :: nf_ebadid = -33
      integer(4), parameter :: nf_eexist = -35
      integer(4), parameter :: nf_einval = -36
      integer(4), parameter :: nf_eperm = -37
      integer(4), parameter :: nf_enotindefine = -38
      integer(4), parameter :: nf_eindefine = -39
      integer(4), parameter :: nf_einvalcoords = -40
      integer(4), parameter :: nf_emaxdims = -41
      integer(4), parameter :: nf_enameinuse = -42
      integer(4), parameter :: nf_enotatt = -43
      integer(4), parameter :: nf_emaxatts = -44
      integer(4), parameter :: nf_ebadtype = -45
      integer(4), parameter :: nf_ebaddim = -46
      integer(4), parameter :: nf_eunlimpos = -47
      integer(4), parameter :: nf_emaxvars = -48
      integer(4), parameter :: nf_enotvar = -49
      integer(4), parameter :: nf_eglobal = -50
      integer(4), parameter :: nf_enotnc = -51
      integer(4), parameter :: nf_ests = -52
      integer(4), parameter :: nf_emaxname = -53
      integer(4), parameter :: nf_eunlimit = -54
      integer(4), parameter :: nf_enorecvars = -55
      integer(4), parameter :: nf_echar = -56
      integer(4), parameter :: nf_eedge = -57
      integer(4), parameter :: nf_estride = -58
      integer(4), parameter :: nf_ebadname = -59
      integer(4), parameter :: nf_erange = -60
      integer(4), parameter :: nf_enomem = -61
      integer(4), parameter :: nf_evarsize = -62
      integer(4), parameter :: nf_edimsize = -63
      integer(4), parameter :: nf_etrunc = -64
      integer(4), parameter :: nf_fatal = 1
      integer(4), parameter :: nf_verbose = 2
      integer(4), parameter :: nf_ubyte = 7
      integer(4), parameter :: nf_ushort = 8
      integer(4), parameter :: nf_uint = 9
      integer(4), parameter :: nf_int64 = 10
      integer(4), parameter :: nf_uint64 = 11
      integer(4), parameter :: nf_string = 12
      integer(4), parameter :: nf_vlen = 13
      integer(4), parameter :: nf_opaque = 14
      integer(4), parameter :: nf_enum = 15
      integer(4), parameter :: nf_compound = 16
      integer(4), parameter :: nf_fill_ubyte = 255
      integer(4), parameter :: nf_fill_ushort = 65535
      integer(4), parameter :: nf_format_netcdf4 = 3
      integer(4), parameter :: nf_format_netcdf4_classic = 4
      integer(4), parameter :: nf_netcdf4 = 4096
      integer(4), parameter :: nf_classic_model = 256
      integer(4), parameter :: nf_chunk_seq = 0
      integer(4), parameter :: nf_chunk_sub = 1
      integer(4), parameter :: nf_chunk_sizes = 2
      integer(4), parameter :: nf_endian_native = 0
      integer(4), parameter :: nf_endian_little = 1
      integer(4), parameter :: nf_endian_big = 2
      integer(4), parameter :: nf_chunked = 0
      integer(4), parameter :: nf_contiguous = 1
      integer(4), parameter :: nf_nochecksum = 0
      integer(4), parameter :: nf_fletcher32 = 1
      integer(4), parameter :: nf_noshuffle = 0
      integer(4), parameter :: nf_shuffle = 1
      integer(4), parameter :: nf_szip_ec_option_mask = 4
      integer(4), parameter :: nf_szip_nn_option_mask = 32
      integer(4), parameter :: nf_mpiio = 8192
      integer(4), parameter :: nf_mpiposix = 16384
      integer(4), parameter :: nf_pnetcdf = 32768
      integer(4), parameter :: nf_independent = 0
      integer(4), parameter :: nf_collective = 1
      integer(4), parameter :: nf_ehdferr = -101
      integer(4), parameter :: nf_ecantread = -102
      integer(4), parameter :: nf_ecantwrite = -103
      integer(4), parameter :: nf_ecantcreate = -104
      integer(4), parameter :: nf_efilemeta = -105
      integer(4), parameter :: nf_edimmeta = -106
      integer(4), parameter :: nf_eattmeta = -107
      integer(4), parameter :: nf_evarmeta = -108
      integer(4), parameter :: nf_enocompound = -109
      integer(4), parameter :: nf_eattexists = -110
      integer(4), parameter :: nf_enotnc4 = -111
      integer(4), parameter :: nf_estrictnc3 = -112
      integer(4), parameter :: nf_enotnc3 = -113
      integer(4), parameter :: nf_enopar = -114
      integer(4), parameter :: nf_eparinit = -115
      integer(4), parameter :: nf_ebadgrpid = -116
      integer(4), parameter :: nf_ebadtypid = -117
      integer(4), parameter :: nf_etypdefined = -118
      integer(4), parameter :: nf_ebadfield = -119
      integer(4), parameter :: nf_ebadclass = -120
      integer(4), parameter :: nf_emaptype = -121
      integer(4), parameter :: nf_elatefill = -122
      integer(4), parameter :: nf_elatedef = -123
      integer(4), parameter :: nf_edimscale = -124
      integer(4), parameter :: nf_enogrp = -125
      integer(4), parameter :: ncbyte = 1
      integer(4), parameter :: ncchar = 2
      integer(4), parameter :: ncshort = 3
      integer(4), parameter :: nclong = 4
      integer(4), parameter :: ncfloat = 5
      integer(4), parameter :: ncdouble = 6
      integer(4), parameter :: ncrdwr = 1
      integer(4), parameter :: nccreat = 2
      integer(4), parameter :: ncexcl = 4
      integer(4), parameter :: ncindef = 8
      integer(4), parameter :: ncnsync = 16
      integer(4), parameter :: nchsync = 32
      integer(4), parameter :: ncndirty = 64
      integer(4), parameter :: nchdirty = 128
      integer(4), parameter :: ncfill = 0
      integer(4), parameter :: ncnofill = 256
      integer(4), parameter :: nclink = 32768
      integer(4), parameter :: ncnowrit = 0
      integer(4), parameter :: ncwrite = ncrdwr
      integer(4), parameter :: ncclob = nf_clobber
      integer(4), parameter :: ncnoclob = nf_noclobber
      integer(4), parameter :: ncunlim = 0
      integer(4), parameter :: ncglobal = 0
      integer(4), parameter :: maxncop = 64
      integer(4), parameter :: maxncdim = 1024
      integer(4), parameter :: maxncatt = 8192
      integer(4), parameter :: maxncvar = 8192
      integer(4), parameter :: maxncnam = 256
      integer(4), parameter :: maxvdims = maxncdim
      integer(4), parameter :: ncnoerr = nf_noerr
      integer(4), parameter :: ncebadid = nf_ebadid
      integer(4), parameter :: ncenfile = -31
      integer(4), parameter :: nceexist = nf_eexist
      integer(4), parameter :: nceinval = nf_einval
      integer(4), parameter :: nceperm = nf_eperm
      integer(4), parameter :: ncenotin = nf_enotindefine
      integer(4), parameter :: nceindef = nf_eindefine
      integer(4), parameter :: ncecoord = nf_einvalcoords
      integer(4), parameter :: ncemaxds = nf_emaxdims
      integer(4), parameter :: ncename = nf_enameinuse
      integer(4), parameter :: ncenoatt = nf_enotatt
      integer(4), parameter :: ncemaxat = nf_emaxatts
      integer(4), parameter :: ncebadty = nf_ebadtype
      integer(4), parameter :: ncebadd = nf_ebaddim
      integer(4), parameter :: nceunlim = nf_eunlimpos
      integer(4), parameter :: ncemaxvs = nf_emaxvars
      integer(4), parameter :: ncenotvr = nf_enotvar
      integer(4), parameter :: nceglob = nf_eglobal
      integer(4), parameter :: ncenotnc = nf_enotnc
      integer(4), parameter :: ncests = nf_ests
      integer(4), parameter :: ncentool = nf_emaxname
      integer(4), parameter :: ncfoobar = 32
      integer(4), parameter :: ncsyserr = -31
      integer(4), parameter :: ncfatal = 1
      integer(4), parameter :: ncverbos = 2
      integer(4), parameter :: filbyte = -127
      integer(4), parameter :: filchar = 0
      integer(4), parameter :: filshort = -32767
      integer(4), parameter :: fillong = -2147483647
      real, parameter :: filfloat = 9.9692099683868690D+36
      real(8), parameter :: fildoub = 9.9692099683868690D+36
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: may_day_flag
      integer(4) :: mynode
      character(75), dimension(1:20,1:500) :: vname
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: work2d_tmp
      integer(4) :: record
      integer(4) :: typevar2d
      integer(4) :: vid
      integer(4) :: ncid
      integer(4) :: indx
      integer(4) :: typefile
        end subroutine Sub_Loop_fillvalue2d

      end interface
