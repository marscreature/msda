      integer(4), parameter :: NWEIGHT = 1000
      interface
        subroutine Sub_Loop_u2dbc_interp_tile(Istr,Iend,Jstr,Jend,DU_wes
     &t4,DU_east4,DU_west6,DU_east6,DU_south4,DU_north4,DU_south6,DU_nor
     &th6,padd_E,Mm,padd_X,Lm,on_u,h,knew,zeta,SSH,om_u,ubclm,dtfast,uma
     &sk,nfast,DU_avg3,weight2,U2DTimeindex2,ubarid,common_index,dUinter
     &p,U2DTimeindex,iif,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,M
     &mmpi,Lmmpi)
          use AGRIF_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      real :: dtfast
      integer(4) :: nfast
      integer(4) :: U2DTimeindex2
      integer(4) :: ubarid
      integer(4) :: common_index
      integer(4) :: U2DTimeindex
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: SSH
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: DU_avg
     &3
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dUinterp
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(-1:Mm+2+padd_E) :: DU_west6
      real, dimension(-1:Mm+2+padd_E) :: DU_east6
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DU_west4
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DU_east4
      real, dimension(-1:Lm+2+padd_X) :: DU_south6
      real, dimension(-1:Lm+2+padd_X) :: DU_north6
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DU_south4
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DU_north4
        end subroutine Sub_Loop_u2dbc_interp_tile

      end interface
