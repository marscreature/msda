      subroutine Sub_Loop_v2dbc_interp_tile(Istr,Iend,Jstr,Jend,DV_west4
     &,DV_east4,DV_west6,DV_east6,DV_south4,DV_north4,DV_south6,DV_north
     &6,padd_E,Mm,padd_X,Lm,om_v,h,knew,zeta,SSH,on_v,vbclm,dtfast,vmask
     &,nfast,DV_avg3,weight2,V2DTimeindex2,vbarid,common_index,dVinterp,
     &V2DTimeindex,iif,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,Mmm
     &pi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util

      include 'mpif.h'
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      real :: dtfast
      integer(4) :: nfast
      integer(4) :: V2DTimeindex2
      integer(4) :: vbarid
      integer(4) :: common_index
      integer(4) :: V2DTimeindex
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: SSH
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: DV_avg
     &3
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dVinterp
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(-1:Mm+2+padd_E) :: DV_west6
      real, dimension(-1:Mm+2+padd_E) :: DV_east6
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DV_west4
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DV_east4
      real, dimension(-1:Lm+2+padd_X) :: DV_south6
      real, dimension(-1:Lm+2+padd_X) :: DV_north6
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DV_south4
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DV_north4

                     
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                         
      integer(4) :: i
      integer(4) :: j
                                                               
      real :: t1
      real :: t2
      real :: t3
      real :: t4
      real :: t5
      real :: t6
      real :: dv1
      real :: dv1np1
      real :: dv2
      real :: tfin
      real :: c1
      real :: c2
      real :: c3
      real :: cff1
                            
      real :: t7
      real :: t8
      real :: t9
      real :: t10
      real :: t11
      external :: v2dinterp
                                       
      integer(4) :: irhox
      integer(4) :: irhoy
      integer(4) :: irhot
                                  
      real :: rrhox
      real :: rrhoy
      real :: rrhot
                                   
      real :: tinterp
      real :: onemtinterp
                        
      integer(4) :: iter
                                
      integer(4) :: parentnbstep
                   
      real :: cffy
                                                        
      real, dimension(Jstr-2:Jend+2) :: DV_west
      real, dimension(Jstr-2:Jend+2) :: DV_east
                                                          
                                                                     
                                                         
      real, dimension(Istr-2:Iend+2) :: DV_south
      real, dimension(Istr-2:Iend+2) :: DV_north
                                                            
                                                                      
                      
      real :: lastcff
                                                   
      real, allocatable, dimension(:,:) :: tabtemp2d
!$AGRIF_DO_NOT_TREAT
      integer*4 :: indinterp,nbgrid
      common/interp2d/indinterp,nbgrid
!$AGRIF_END_DO_NOT_TREAT
                        
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      irhox=Agrif_Irhox()
      irhoy=Agrif_Irhoy()
      irhot=Agrif_Irhot()
      rrhox = real(irhox)
      rrhoy = real(irhoy)
      rrhot = real(irhot)
      parentnbstep=
     &  Agrif_Parent(iif)
      if (V2DTimeindex .NE. parentnbstep) then
C$OMP BARRIER
C$OMP MASTER
        dVinterp = 0.D0
        tinterp=1.D0
        Agrif_UseSpecialValue = .true.
        Agrif_SpecialValue = 0.D0
        nbgrid = Agrif_Fixed()
        common_index = 1
        Call Agrif_Bc_variable(vbarid,calledweight=tinterp,
     &   procname = v2dinterp)
        Agrif_UseSpecialvalue=.false.
C$OMP END MASTER
C$OMP BARRIER
        V2DTimeindex = parentnbstep
        V2DTimeindex2 = agrif_nb_step()
      endif
      if (agrif_nb_step() .EQ. V2DTimeindex2) then
          lastcff=0.D0
          do iter=iif,iif+irhot-1
          lastcff=lastcff+((iter+1-iif)/rrhot)*
     &        weight2(iif+irhot-1,iter)
          enddo
          lastcff=1.D0/lastcff
      if (.not.SOUTH_INTER) then
          j=Jstr
          do i=Istr,Iend
          DV_south4(i,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DV_south6(Istr:Iend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do i=Istr,Iend
             DV_south6(i) = DV_south6(i) +cff1*DV_south4(i,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do i=Istr,Iend
          DV_south6(i)=DV_south6(i)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DV_south4(i,iif-1)
          enddo
          enddo
          do i=Istr,Iend
          DV_south6(i)=lastcff*((dVinterp(i,j)/rrhox)-DV_south6(i))
     &                               * vmask(i,j)
          enddo
          do i=Istr,Iend
          DV_south6(i)=(DV_south6(i)-DV_south4(i,iif-1))/rrhot
          enddo
          endif
      endif
      if (.not.NORTH_INTER) then
          j=Jend+1
          do i=Istr,Iend
          DV_north4(i,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DV_north6(Istr:Iend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do i=Istr,Iend
             DV_north6(i) = DV_north6(i) +cff1*DV_north4(i,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do i=Istr,Iend
          DV_north6(i)=DV_north6(i)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DV_north4(i,iif-1)
          enddo
          enddo
          do i=Istr,Iend
          DV_north6(i)=lastcff*((dVinterp(i,j)/rrhox)-DV_north6(i))
     &                               * vmask(i,j)
          enddo
          do i=Istr,Iend
          DV_north6(i)=(DV_north6(i)-DV_north4(i,iif-1))/rrhot
          enddo
          endif
      endif
      if (.not.WEST_INTER) then
          i = Istr-1
          do j=Jstr,Jend
          DV_west4(j,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DV_west6(Jstr:Jend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=Jstr,Jend
             DV_west6(j) = DV_west6(j) +cff1*DV_west4(j,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=Jstr,Jend
          DV_west6(j)=DV_west6(j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DV_west4(j,iif-1)
          enddo
          enddo
          do j=Jstr,Jend
          DV_west6(j)=lastcff*((dVinterp(i,j)/rrhoy)-DV_west6(j))
     &                               * vmask(i,j)
          enddo
          do j=Jstr,Jend
          DV_west6(j)=(DV_west6(j)-DV_west4(j,iif-1))/rrhot
          enddo
          endif
      endif
      if (.not.EAST_INTER) then
          i=Iend+1
          do j=Jstr,Jend
          DV_east4(j,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DV_east6(Jstr:Jend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=Jstr,Jend
             DV_east6(j) = DV_east6(j) +cff1*DV_east4(j,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=Jstr,Jend
          DV_east6(j)=DV_east6(j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DV_east4(j,iif-1)
          enddo
          enddo
          do j=Jstr,Jend
          DV_east6(j)=lastcff*((dVinterp(i,j)/rrhoy)-DV_east6(j))
     &                               * vmask(i,j)
          enddo
          do j=Jstr,Jend
          DV_east6(j)=(DV_east6(j)-DV_east4(j,iif-1))/rrhot
          enddo
          endif
      endif
      endif
      if (.not.SOUTH_INTER) then
          j=Jstr
          do i=Istr,Iend
           DV_south4(i,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          do i=Istr,Iend
            DV_south(i)=DV_south4(i,iif-1)+DV_south6(i)
          enddo
      endif
      if (.not.NORTH_INTER) then
          j=Jend+1
          do i=Istr,Iend
           DV_north4(i,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          do i=Istr,Iend
            DV_north(i)=DV_north4(i,iif-1)+DV_north6(i)
          enddo
      endif
      if (.not.WEST_INTER) then
          i = Istr-1
          do j=Jstr,Jend
           DV_west4(j,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          do j=Jstr,Jend
            DV_west(j)=DV_west4(j,iif-1)+DV_west6(j)
          enddo
      endif
      if (.not.EAST_INTER) then
          i=Iend+1
          do j=Jstr,Jend
           DV_east4(j,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          do j=Jstr,Jend
            DV_east(j)=DV_east4(j,iif-1)+DV_east6(j)
          enddo
      endif
      cffy = g*dtfast*2.D0/(1.D0+rrhoy)
      if (.not.SOUTH_INTER) then
         do i=Istr,Iend
           vbclm(i,Jstr)=
     &                   (cffy/on_v(i,Jstr))*
     &                   (SSH(i,Jstr)-zeta(i,Jstr,knew))   +
     &              (2.D0*DV_south(i)/((h(i,Jstr-1)+zeta(i,Jstr-1,knew)
     &                                  +h(i,Jstr)+zeta(i,Jstr,knew))
     &                                                 *om_v(i,Jstr)))
     &                  *vmask(i,Jstr)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=Istr,Iend
          vbclm(i,Jend+1)=
     &                    -(cffy/on_v(i,Jend+1))*
     &                     (SSH(i,Jend)-zeta(i,Jend,knew))  +
     &                  (2.D0*DV_north(i)/(( h(i,Jend)+zeta(i,Jend,knew)
     &                               +h(i,Jend+1)+zeta(i,Jend+1,knew))
     &                                               *om_v(i,Jend+1)))
     &                  *vmask(i,Jend+1)
         enddo
      endif
      if (.not.EAST_INTER) then
        do j=Jstr,Jend
          vbclm(Iend+1,j)=
     &             (2.D0*DV_east(j)/(( h(Iend+1,j)+zeta(Iend+1,j,knew)
     &                           +h(Iend+1,j-1)+zeta(Iend+1,j-1,knew))
     &                                             *om_v(Iend+1,j)))
     &                 *vmask(Iend+1,j)
         enddo
      endif
      if (.not.WEST_INTER) then
        do j=Jstr,Jend
          vbclm(Istr-1,j)=
     &                (2.D0*DV_west(j)/((h(Istr-1,j)+zeta(Istr-1,j,knew)
     &                           +h(Istr-1,j-1)+zeta(Istr-1,j-1,knew))
     &                                                *om_v(Istr-1,j)))
     &                  *vmask(Istr-1,j)
         enddo
      endif
      return
       
      

      end subroutine Sub_Loop_v2dbc_interp_tile

