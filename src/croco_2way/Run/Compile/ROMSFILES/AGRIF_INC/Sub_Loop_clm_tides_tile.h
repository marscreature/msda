      subroutine Sub_Loop_clm_tides_tile(Istr,Iend,Jstr,Jend,Cangle,Sang
     &le,Cphase,Sphase,Etide,Utide,Vtide,padd_E,Mm,padd_X,Lm,vbclm,ubclm
     &,vmask,umask,UV_Tminor,UV_Tmajor,UV_Tphase,angler,UV_Tangle,ssh,rm
     &ask,SSH_Tphase,SSH_Tamp,Tperiod,time,NORTH_INTER,SOUTH_INTER,EAST_
     &INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: time
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tmin
     &or
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tmaj
     &or
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tpha
     &se
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: angler
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tang
     &le
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ssh
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: SSH_Tph
     &ase
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: SSH_Tam
     &p
      real, dimension(1:Ntides) :: Tperiod
      integer(4) :: Iend
      integer(4) :: Istr
      integer(4) :: Jend
      integer(4) :: Jstr
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Cangle
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Cphase
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Sangle
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Sphase
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Etide
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Utide
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Vtide

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                           
                                     
                                                          
                                       
                                                            
                                           
                                                           
                                         
                                                           
                                         
                                                           
                                         
                                                           
                                         
                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                                               
      integer(4) :: i
      integer(4) :: itide
      integer(4) :: j
                                                             
      real :: angle
      real :: phase
      real :: omega
      real :: ramp
      real :: cff
      real :: tstime
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      angle=0.D0
      phase=0.D0
      ramp=1.D0
      cff=2.D0*pi*time
      tstime=1.0D0
      do j=Jstr-1,Jend+1
        do i=Istr-1,Iend+1
          Etide(i,j)=0.D0
        enddo
      enddo
      do itide=1,Ntides
        if (Tperiod(itide).gt.0.D0) then
          omega=2.D0*pi*(time-tstime*day2sec)/Tperiod(itide)
          do j=Jstr-1,Jend+1
            do i=Istr-1,Iend+1
              Etide(i,j)=Etide(i,j)+ramp*
     &                   SSH_Tamp(i,j,itide)*
     &                   COS(omega-SSH_Tphase(i,j,itide))
              Etide(i,j)=Etide(i,j)*rmask(i,j)
            enddo
          enddo
        endif
      enddo
      do j=Jstr-1,Jend+1
        do i=Istr-1,Iend+1
          ssh(i,j)=ssh(i,j)+Etide(i,j)
        enddo
      enddo
      do j=JstrR,JendR
        do i=Istr,IendR
          Utide(i,j)=0.D0
        enddo
      enddo
      do j=Jstr,JendR
        do i=IstrR,IendR
          Vtide(i,j)=0.D0
        enddo
      enddo
      do itide=1,Ntides
        if (Tperiod(itide).gt.0.D0) then
          omega=cff/Tperiod(itide)
          do j=Jstr-1,JendR
            do i=Istr-1,IendR
              angle=UV_Tangle(i,j,itide)
     &                      -angler(i,j)
              phase=omega-UV_Tphase(i,j,itide)
              Cangle(i,j)=COS(angle)
              Cphase(i,j)=COS(phase)
              Sangle(i,j)=SIN(angle)
              Sphase(i,j)=SIN(phase)
            enddo
          enddo
          do j=JstrR,JendR
            do i=Istr,IendR
              Utide(i,j)=Utide(i,j)+ramp*
     &                        0.125D0*((UV_Tmajor(i-1,j,itide)+
     &                                UV_Tmajor(i  ,j,itide))*
     &                               (Cangle(i-1,j)+Cangle(i,j))*
     &                               (Cphase(i-1,j)+Cphase(i,j))-
     &                               (UV_Tminor(i-1,j,itide)+
     &                                UV_Tminor(i  ,j,itide))*
     &                               (Sangle(i-1,j)+Sangle(i,j))*
     &                               (Sphase(i-1,j)+Sphase(i,j)))
             Utide(i,j)=Utide(i,j)*umask(i,j)
            enddo
          enddo
          do j=Jstr,JendR
            do i=IstrR,IendR
              Vtide(i,j)=Vtide(i,j)+ramp*
     &                        0.125D0*((UV_Tmajor(i,j-1,itide)+
     &                                UV_Tmajor(i,j  ,itide))*
     &                               (Sangle(i,j-1)+Sangle(i,j))*
     &                               (Cphase(i,j-1)+Cphase(i,j))+
     &                               (UV_Tminor(i,j-1,itide)+
     &                                UV_Tminor(i,j  ,itide))*
     &                               (Cangle(i,j-1)+Cangle(i,j))*
     &                               (Sphase(i,j-1)+Sphase(i,j)))
              Vtide(i,j)=Vtide(i,j)*vmask(i,j)
            enddo
          enddo
        endif
      enddo
      do j=JstrR,JendR
        do i=Istr,IendR
          ubclm(i,j)=ubclm(i,j)+Utide(i,j)
        enddo
      enddo
      do j=Jstr,JendR
        do i=IstrR,IendR
          vbclm(i,j)=vbclm(i,j)+Vtide(i,j)
        enddo
      enddo
      return
       
      

      end subroutine Sub_Loop_clm_tides_tile

