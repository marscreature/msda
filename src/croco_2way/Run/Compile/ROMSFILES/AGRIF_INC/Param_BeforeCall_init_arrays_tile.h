      interface
        subroutine Sub_Loop_init_arrays_tile(Istr,Iend,Jstr,Jend,padd_E,
     &Mm,padd_X,Lm,hbbl_avg,hbbl,hbl_avg,kbl,hbls,ghats,Akt,bvf,Akv,dRde
     &,dRdx,diff3d_v,diff3d_u,diff4_sponge,tnu4,diff4,diff2_sponge,tnu2,
     &diff2,visc2_sponge_p,visc2_sponge_r,visc2_p,visc2,visc2_r,UV_Tmino
     &r,UV_Tmajor,UV_Tphase,UV_Tangle,SSH_Tphase,SSH_Tamp,Tperiod,vclm,u
     &clm,vclima,uclima,vbclima,ubclima,vbclm,ubclm,tclima,tclm,Tnudgcof
     &,sshg,ssh,Znudgcof,srflxg,srflx_avg,srflx,shflx_sen_avg,shflx_lat_
     &avg,shflx_rlw_avg,shflx_rsw_avg,shflx_sen,shflx_lat,shflx_rlw,shfl
     &x_rsw,vwndg,uwndg,wspdg,radswg,radlwg,prateg,rhumg,tairg,vwnd,uwnd
     &,wspd,radsw,radlw,prate,rhum,tair,btflx,stflxg,stflx_avg,stflx,bvs
     &trg,bustrg,svstr_avg,sustr_avg,wstr_avg,bostr_avg,bvstr,bustr,svst
     &rg,sustrg,svstr,sustr,t_avg,t,got_tini,omega_avg,We,w_avg,v_avg,u_
     &avg,rho_avg,rho,v,u,DV_avg2,DU_avg2,DV_avg1,DU_avg1,Zt_avg1,rufrc,
     &vbar_avg,ubar_avg,zeta_avg,vbar,ubar,zeta,NORTH_INTER,Mmmpi,SOUTH_
     &INTER,EAST_INTER,Lmmpi,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: init = 0.D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: visc2
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbl_avg
      integer(4), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: kbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: hbls
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: ghats
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N,1:2) :: Akt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: dRde
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: dRdx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: diff4_sponge
      real, dimension(1:NT) :: tnu4
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: diff4
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: diff2_sponge
      real, dimension(1:NT) :: tnu2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: diff2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tmin
     &or
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tmaj
     &or
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tpha
     &se
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tang
     &le
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: SSH_Tph
     &ase
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: SSH_Tam
     &p
      real, dimension(1:Ntides) :: Tperiod
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: vclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: uclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vbclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: ubclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2,1:NT) :: tcl
     &ima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: Tnudgco
     &f
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: sshg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ssh
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Znudgcof
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: srflxg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vwndg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: uwndg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: wspdg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: radswg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: radlwg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: prateg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: rhumg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: tairg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vwnd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: uwnd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: wspd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: radsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: radlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: prate
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhum
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: tair
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: btflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2,1:NT) :: stflxg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: bvstrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: bustrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: wstr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bostr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: svstrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: sustrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: t_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      logical, dimension(1:NT) :: got_tini
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: omega_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: We
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: w_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: v_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: u_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DV_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DU_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rufrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbar_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubar_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: zeta_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
        end subroutine Sub_Loop_init_arrays_tile

      end interface
