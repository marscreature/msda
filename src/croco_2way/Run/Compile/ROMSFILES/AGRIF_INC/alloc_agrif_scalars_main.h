#include "Param_toamr_scalars_main.h" 
      !! ALLOCATION OF VARIABLE : got_tini
          if (.not. allocated(Agrif_Gr % tabvars_l(10)% larray1)) then
          allocate(Agrif_Gr % tabvars_l(10)% larray1(1 : NT))
      endif
      !! ALLOCATION OF VARIABLE : weight
          if (.not. allocated(Agrif_Gr % tabvars(87)% array2)) then
          allocate(Agrif_Gr % tabvars(87)% array2(1 : 6,0 : NWEIGHT))
          Agrif_Gr % tabvars(87)% array2 = 0
      endif
      !! ALLOCATION OF VARIABLE : tnu4
          if (.not. allocated(Agrif_Gr % tabvars(88)% array1)) then
          allocate(Agrif_Gr % tabvars(88)% array1(1 : NT))
          Agrif_Gr % tabvars(88)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : tnu2
          if (.not. allocated(Agrif_Gr % tabvars(89)% array1)) then
          allocate(Agrif_Gr % tabvars(89)% array1(1 : NT))
          Agrif_Gr % tabvars(89)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : Cs_r
          if (.not. allocated(Agrif_Gr % tabvars(90)% array1)) then
          allocate(Agrif_Gr % tabvars(90)% array1(1 : N))
          Agrif_Gr % tabvars(90)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : sc_r
          if (.not. allocated(Agrif_Gr % tabvars(91)% array1)) then
          allocate(Agrif_Gr % tabvars(91)% array1(1 : N))
          Agrif_Gr % tabvars(91)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : Cs_w
          if (.not. allocated(Agrif_Gr % tabvars(92)% array1)) then
          allocate(Agrif_Gr % tabvars(92)% array1(0 : N))
          Agrif_Gr % tabvars(92)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : sc_w
          if (.not. allocated(Agrif_Gr % tabvars(93)% array1)) then
          allocate(Agrif_Gr % tabvars(93)% array1(0 : N))
          Agrif_Gr % tabvars(93)% array1 = 0
      endif
