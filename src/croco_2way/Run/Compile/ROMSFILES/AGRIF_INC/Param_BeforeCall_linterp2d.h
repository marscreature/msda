      interface
        subroutine Sub_Loop_linterp2d(LBx,UBx,LBy,UBy,Xinp,Yinp,Finp,Ist
     &r,Iend,Jstr,Jend,Xout,Yout,Fout,padd_E,Mm,padd_X,Lm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4), intent(in) :: LBx
      integer(4), intent(in) :: UBx
      integer(4), intent(in) :: LBy
      integer(4), intent(in) :: UBy
      integer(4), intent(in) :: Istr
      integer(4), intent(in) :: Iend
      integer(4), intent(in) :: Jstr
      integer(4), intent(in) :: Jend
      real(8), intent(in), dimension(LBx:UBx) :: Xinp
      real(8), intent(in), dimension(LBy:UBy) :: Yinp
      real(8), intent(in), dimension(LBx:UBx,LBy:UBy) :: Finp
      real(8), intent(in), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: X
     &out
      real(8), intent(in), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Y
     &out
      real(8), intent(out), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: 
     &Fout
        end subroutine Sub_Loop_linterp2d

      end interface
