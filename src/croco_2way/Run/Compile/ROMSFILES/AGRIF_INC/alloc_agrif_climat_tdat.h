#include "Param_toamr_climat_tdat.h" 
      !! ALLOCATION OF VARIABLE : got_tclm
          if (.not. allocated(Agrif_Gr % tabvars_l(12)% larray1)) then
          allocate(Agrif_Gr % tabvars_l(12)% larray1(1 : NT))
      endif
      !! ALLOCATION OF VARIABLE : tclm_id
      do i = 210,214
          if (.not. allocated(Agrif_Gr % tabvars_i(i)% iarray1)) then
          allocate(Agrif_Gr % tabvars_i(i)% iarray1(1 : NT))
          Agrif_Gr % tabvars_i(i)% iarray1 = 0
      endif
      enddo
      !! ALLOCATION OF VARIABLE : tclm_cycle
          if (.not. allocated(Agrif_Gr % tabvars(134)% array1)) then
          allocate(Agrif_Gr % tabvars(134)% array1(1 : NT))
          Agrif_Gr % tabvars(134)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : tclm_time
          if (.not. allocated(Agrif_Gr % tabvars(135)% array2)) then
          allocate(Agrif_Gr % tabvars(135)% array2(1 : 2,1 : NT))
          Agrif_Gr % tabvars(135)% array2 = 0
      endif
