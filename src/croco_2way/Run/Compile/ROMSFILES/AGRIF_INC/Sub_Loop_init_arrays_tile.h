      subroutine Sub_Loop_init_arrays_tile(Istr,Iend,Jstr,Jend,padd_E,Mm
     &,padd_X,Lm,hbbl_avg,hbbl,hbl_avg,kbl,hbls,ghats,Akt,bvf,Akv,dRde,d
     &Rdx,diff3d_v,diff3d_u,diff4_sponge,tnu4,diff4,diff2_sponge,tnu2,di
     &ff2,visc2_sponge_p,visc2_sponge_r,visc2_p,visc2,visc2_r,UV_Tminor,
     &UV_Tmajor,UV_Tphase,UV_Tangle,SSH_Tphase,SSH_Tamp,Tperiod,vclm,ucl
     &m,vclima,uclima,vbclima,ubclima,vbclm,ubclm,tclima,tclm,Tnudgcof,s
     &shg,ssh,Znudgcof,srflxg,srflx_avg,srflx,shflx_sen_avg,shflx_lat_av
     &g,shflx_rlw_avg,shflx_rsw_avg,shflx_sen,shflx_lat,shflx_rlw,shflx_
     &rsw,vwndg,uwndg,wspdg,radswg,radlwg,prateg,rhumg,tairg,vwnd,uwnd,w
     &spd,radsw,radlw,prate,rhum,tair,btflx,stflxg,stflx_avg,stflx,bvstr
     &g,bustrg,svstr_avg,sustr_avg,wstr_avg,bostr_avg,bvstr,bustr,svstrg
     &,sustrg,svstr,sustr,t_avg,t,got_tini,omega_avg,We,w_avg,v_avg,u_av
     &g,rho_avg,rho,v,u,DV_avg2,DU_avg2,DV_avg1,DU_avg1,Zt_avg1,rufrc,vb
     &ar_avg,ubar_avg,zeta_avg,vbar,ubar,zeta,NORTH_INTER,Mmmpi,SOUTH_IN
     &TER,EAST_INTER,Lmmpi,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars


      include 'mpif.h'
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: init = 0.D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: visc2
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbl_avg
      integer(4), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: kbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: hbls
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: ghats
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N,1:2) :: Akt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: dRde
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: dRdx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: diff4_sponge
      real, dimension(1:NT) :: tnu4
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: diff4
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: diff2_sponge
      real, dimension(1:NT) :: tnu2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: diff2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tmin
     &or
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tmaj
     &or
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tpha
     &se
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tang
     &le
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: SSH_Tph
     &ase
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: SSH_Tam
     &p
      real, dimension(1:Ntides) :: Tperiod
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: vclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: uclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vbclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: ubclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2,1:NT) :: tcl
     &ima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: Tnudgco
     &f
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: sshg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ssh
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Znudgcof
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: srflxg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vwndg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: uwndg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: wspdg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: radswg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: radlwg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: prateg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: rhumg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: tairg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vwnd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: uwnd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: wspd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: radsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: radlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: prate
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhum
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: tair
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: btflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2,1:NT) :: stflxg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: bvstrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: bustrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: wstr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bostr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: svstrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: sustrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: t_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      logical, dimension(1:NT) :: got_tini
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: omega_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: We
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: w_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: v_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: u_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DV_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DU_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rufrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbar_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubar_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: zeta_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                   
                                                   
                                                   
                                                                       
                                                                 
                         
                                                    
                                  
                                                   
                                
                                                    
                                  
                                                    
                                  
                                                    
                                  
                                                  
                                                  
                                                     
                                                    
                                                        
                                                  
                                                                       
                                                                 
                                                          
                                                       
                                  
                                                  
                              
                                                   
                                
                                                        
                                                        
                                                        
                                                        
                                          
                                          
                                          
                                          
                                                       
                                    
                                                      
                                                        
                                               
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                           
                                     
                                                          
                                       
                                                            
                                           
                                                           
                                         
                                                           
                                         
                                                           
                                         
                                                           
                                         
                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                                                       
                                                                 
           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: itrc
      integer(4) :: itide
                
                           

                       
                     
      integer(4) :: ierr
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
      if (Istr.eq.1) then
        if (WEST_INTER) then
          IstrR=Istr-2
        else
          IstrR=Istr-1
        endif
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lmmpi) then
        if (EAST_INTER) then
          IendR=Iend+2
        else
          IendR=Iend+1
        endif
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        if (SOUTH_INTER) then
          JstrR=Jstr-2
        else
          JstrR=Jstr-1
        endif
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mmmpi) then
        if (NORTH_INTER) then
          JendR=Jend+2
        else
          JendR=Jend+1
        endif
      else
        JendR=Jend
      endif
      do j=JstrR,JendR
        do i=IstrR,IendR
          zeta(i,j,1)=0.D0
          zeta(i,j,2)=init
          zeta(i,j,3)=init
          zeta(i,j,4)=init
          ubar(i,j,1)=init
          ubar(i,j,2)=init
          ubar(i,j,3)=init
          ubar(i,j,4)=init
          vbar(i,j,1)=init
          vbar(i,j,2)=init
          vbar(i,j,3)=init
          vbar(i,j,4)=init
          zeta_avg(i,j)=init
          ubar_avg(i,j)=init
          vbar_avg(i,j)=init
          rufrc(i,j)=init
          rufrc(i,j)=init
          Zt_avg1(i,j)=0.D0
          DU_avg1(i,j,1)=0.D0
          DV_avg1(i,j,1)=0.D0
          DU_avg1(i,j,2)=0.D0
          DV_avg1(i,j,2)=0.D0
          DU_avg2(i,j)=0.D0
          DV_avg2(i,j)=0.D0
        enddo
      enddo
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            u(i,j,k,1)=init
            u(i,j,k,2)=init
            v(i,j,k,1)=init
            v(i,j,k,2)=init
            rho(i,j,k) =init
            rho_avg(i,j,k)=init
            u_avg(i,j,k)=init
            v_avg(i,j,k)=init
            w_avg(i,j,k)=init
          enddo
        enddo
      enddo
      do k=0,N
        do j=JstrR,JendR
          do i=IstrR,IendR
             We(i,j,k)=init
            omega_avg(i,j,k)=init
          enddo
        enddo
      enddo
      do itrc=1,NT
        got_tini(itrc)=.false.
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              t(i,j,k,1,itrc)=init
              t(i,j,k,2,itrc)=init
              t_avg(i,j,k,itrc)=init
            enddo
          enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          sustr(i,j)=init
          svstr(i,j)=init
          sustrg(i,j,1)=init
          svstrg(i,j,1)=init
          sustrg(i,j,2)=init
          svstrg(i,j,2)=init
          bustr(i,j)=init
          bvstr(i,j)=init
          bostr_avg(i,j)=init
          wstr_avg(i,j)=init
          sustr_avg(i,j)=init
          svstr_avg(i,j)=init
          bustrg(i,j,1)=init
          bvstrg(i,j,1)=init
          bustrg(i,j,2)=init
          bvstrg(i,j,2)=init
        enddo
      enddo
      do itrc=1,NT
        do j=JstrR,JendR
          do i=IstrR,IendR
            stflx(i,j,itrc)=init
            stflx_avg(i,j,itrc)=init
            stflxg(i,j,1,itrc)=init
            stflxg(i,j,2,itrc)=init
            btflx(i,j,itrc)=init
          enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          tair (i,j)=init
          rhum (i,j)=init
          prate (i,j)=init
          radlw (i,j)=init
          radsw (i,j)=init
          wspd(i,j)=init
          uwnd(i,j)=init
          vwnd(i,j)=init
          tairg(i,j,1)=init
          rhumg (i,j,1)=init
          prateg (i,j,1)=init
          radlwg (i,j,1)=init
          radswg (i,j,1)=init
          tairg(i,j,2)=init
          rhumg (i,j,2)=init
          prateg (i,j,2)=init
          radlwg (i,j,2)=init
          radswg (i,j,2)=init
          wspdg(i,j,1)=init
          wspdg(i,j,2)=init
          uwndg(i,j,1)=init
          vwndg(i,j,1)=init
          uwndg(i,j,2)=init
          vwndg(i,j,2)=init
          shflx_rsw(i,j)=init
          shflx_rlw(i,j)=init
          shflx_lat(i,j)=init
          shflx_sen(i,j)=init
          shflx_rsw_avg(i,j)=init
          shflx_rlw_avg(i,j)=init
          shflx_lat_avg(i,j)=init
          shflx_sen_avg(i,j)=init
          srflx(i,j)=init
          srflx_avg(i,j)=init
          srflxg(i,j,1)=init
          srflxg(i,j,2)=init
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          Znudgcof(i,j)=init
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          ssh(i,j)=init
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          sshg(i,j,1)=init
          sshg(i,j,2)=init
        enddo
      enddo
      do itrc=1,NT
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              Tnudgcof(i,j,k,itrc)=init
            enddo
          enddo
        enddo
      enddo
      do itrc=1,NT
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              tclm(i,j,k,itrc)=init
            enddo
          enddo
        enddo
      enddo
      do itrc=1,NT
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              tclima(i,j,k,1,itrc)=init
              tclima(i,j,k,2,itrc)=init
            enddo
          enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          ubclm(i,j)=init
          vbclm(i,j)=init
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          ubclima(i,j,1)=init
          ubclima(i,j,2)=init
          vbclima(i,j,1)=init
          vbclima(i,j,2)=init
        enddo
      enddo
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            uclima(i,j,k,1)=init
            uclima(i,j,k,2)=init
            vclima(i,j,k,1)=init
            vclima(i,j,k,2)=init
          enddo
        enddo
      enddo
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            uclm(i,j,k)=init
            vclm(i,j,k)=init
          enddo
        enddo
      enddo
      do itide=1,Ntides
        Tperiod(itide)=init
        do j=JstrR,JendR
          do i=IstrR,IendR
            SSH_Tamp(i,j,itide)=init
            SSH_Tphase(i,j,itide)=init
            UV_Tangle(i,j,itide)=init
            UV_Tphase(i,j,itide)=init
            UV_Tmajor(i,j,itide)=init
            UV_Tminor(i,j,itide)=init
          enddo
        enddo
      enddo
        do j=JstrR,JendR
          do i=IstrR,IendR
            visc2_r(i,j)=visc2
            visc2_p(i,j)=visc2
            visc2_sponge_r(i,j)=init
            visc2_sponge_p(i,j)=init
          enddo
        enddo
        do itrc=1,NT
          do j=JstrR,JendR
            do i=IstrR,IendR
              diff2(i,j,itrc)=tnu2(itrc)
            enddo
          enddo
        enddo
          do j=JstrR,JendR
            do i=IstrR,IendR
              diff2_sponge(i,j)=init
            enddo
          enddo
        do itrc=1,NT
          do j=JstrR,JendR
            do i=IstrR,IendR
              diff4(i,j,itrc)=tnu4(itrc)
            enddo
          enddo
        enddo
          do j=JstrR,JendR
            do i=IstrR,IendR
              diff4_sponge(i,j)=init
            enddo
          enddo
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              diff3d_u(i,j,k)=init
              diff3d_v(i,j,k)=init
            enddo
          enddo
        enddo
        do k=1,N-1
          do j=JstrR,JendR
            do i=IstrR,IendR
              dRdx(i,j,k)=0.D0
              dRde(i,j,k)=0.D0
            enddo
          enddo
        enddo
      do k=0,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            Akv(i,j,k)=init
            bvf(i,j,k)=init
          enddo
        enddo
        do j=JstrR,JendR
          do i=IstrR,IendR
              Akt(i,j,k,itemp)=init
              Akt(i,j,k,isalt)=init
          enddo
        enddo
      enddo
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            ghats(i,j,k)=init
          enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          hbls(i,j,1)=init
          hbls(i,j,2)=init
          kbl(i,j)=init
          hbl_avg(i,j)=init
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          hbbl(i,j)=init
          hbbl_avg(i,j)=init
        enddo
      enddo
      return
       
      

      end subroutine Sub_Loop_init_arrays_tile

