      subroutine Sub_Loop_v3dbc_interp_tile(Istr,Iend,Jstr,Jend,Mmmpi,pa
     &dd_X,Lm,padd_E,Mm,Lmmpi,V_east,V_west,V_north,vmask,nstp,v,V_south
     &,vclm,nbstep3d,vid,VTimeindex,nnew,nbcoarse,NORTH_INTER,SOUTH_INTE
     &R,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      integer(4) :: nstp
      integer(4) :: nbstep3d
      integer(4) :: vid
      integer(4) :: VTimeindex
      integer(4) :: nnew
      integer(4) :: nbcoarse
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: V_east
      real, dimension(0:0,-1:Mm+2+padd_E,1:N,1:4) :: V_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: V_north
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,1:1,1:N,1:4) :: V_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                     
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                        
      integer(4) :: itrcind
                   
      real :: tinterp
                                  
      INTEGER(4) :: nbstep3dparent
      external :: v3dinterp
                      
      real, dimension(1:6) :: ainterp
                         
      integer(4) :: irhot
                    
      real :: rrhot
                                  
      integer(4) :: nsource
      integer(4) :: ndest
                    
      real :: gamma
                                
      integer(4) :: parentnbstep
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      irhot=Agrif_Irhot()
      rrhot=real(irhot)
C$OMP BARRIER
C$OMP MASTER
      parentnbstep=Agrif_Parent_Nb_Step()
       IF ((((nbcoarse == 1).AND.(nnew == 3)).OR.
     &   ((nbcoarse == irhot).AND.(nnew /=3))).AND.
     &    (VTimeindex.NE.parentnbstep)) THEN
        tinterp=1.D0
        AGRIF_UseSpecialvalue=.true.
        AGRIF_Specialvalue=0.D0
        Call Agrif_Set_bc(vid,(/0,0/),InterpolationShouldbemade=.TRUE.)
        Call Agrif_Bc_variable(vid,calledweight=tinterp,
     &procname=v3dinterp)
        AGRIF_UseSpecialValue=.false.
        VTimeindex=parentnbstep
      endif
C$OMP END MASTER
C$OMP BARRIER
       nbstep3dparent=
     &Agrif_Parent(nbstep3d)
       gamma = 1.D0/12.D0
       tinterp = real(nbcoarse)/rrhot
       IF (nnew == 3) tinterp = tinterp - 1.D0/(2.D0*rrhot)
       ainterp(3)=tinterp*4*(1+tinterp)
       ainterp(2)=-3.D0*(-1+tinterp+2*tinterp**2)
       ainterp(1)=tinterp*(-1+2*tinterp)
       ainterp = ainterp/3.D0
       ainterp(1) = 0.D0
       ainterp(2) = 1.D0-2*tinterp
       ainterp(3) = 2.D0*tinterp
       ainterp = 0.D0
       IF (nnew == 3) then
         ainterp(1) = 0.5D0-tinterp
         ainterp(2) = 0.5D0+tinterp
       else
         ainterp(3) = -tinterp
         ainterp(4) = 1.D0+tinterp
       endif
       IF (nbstep3dparent .LE. 1) THEN
       ainterp = 0.D0
       ainterp(2) = 1.D0
       ENDIF
       if (nbstep3d .LE. (Agrif_irhot()-1)) then
        ainterp = 0.D0
        ainterp(2) = 1.D0
       endif
      IF ((nbstep3dparent .NE. 0) .AND. (nnew ==3)) THEN
      ENDIF
      IF ((nbstep3dparent .NE. 0) .AND. (nnew /=3)) THEN
      ENDIF
       IF ((nnew/=3).AND.(nbcoarse == irhot)) THEN
         ainterp    = 0.D0
         ainterp(4) = 1.D0
       ENDIF
      if (.not.SOUTH_INTER) then
        do k=1,N
          do i=IstrR,IendR
            vclm(i,Jstr,k)=
     &    (ainterp(1)*V_south(i,Jstr,k,1)+
     &     ainterp(2)*V_south(i,Jstr,k,2)+
     &     ainterp(3)*V_south(i,Jstr,k,3)+
     &     ainterp(4)*V_south(i,Jstr,k,4)+
     &     ainterp(5)*v(i,Jstr,k,3-nstp)+
     &     ainterp(6)*v(i,Jstr,k,nstp))
     &    *vmask(i,Jstr)
          enddo
        enddo
      endif
      if (.not.NORTH_INTER) then
        do k=1,N
          do i=IstrR,IendR
            vclm(i,Jend+1,k)=
     &    (ainterp(1)*V_north(i,Jend+1,k,1)+
     &     ainterp(2)*V_north(i,Jend+1,k,2)+
     &     ainterp(3)*V_north(i,Jend+1,k,3)+
     &     ainterp(4)*V_north(i,Jend+1,k,4)+
     &     ainterp(5)*v(i,Jend+1,k,3-nstp)+
     &     ainterp(6)*v(i,Jend+1,k,nstp))
     &    *vmask(i,Jend+1)
          enddo
        enddo
      endif
      if (.not.WEST_INTER) then
        do k=1,N
          do j=Jstr,JendR
            vclm(Istr-1,j,k)=
     &    (ainterp(1)*V_west(Istr-1,j,k,1)+
     &     ainterp(2)*V_west(Istr-1,j,k,2)+
     &     ainterp(3)*V_west(Istr-1,j,k,3)+
     &     ainterp(4)*V_west(Istr-1,j,k,4)+
     &     ainterp(5)*v(Istr-1,j,k,3-nstp)+
     &     ainterp(6)*v(Istr-1,j,k,nstp))
     &    *vmask(Istr-1,j)
          enddo
        enddo
      endif
      if (.not.EAST_INTER) then
        do k=1,N
          do j=Jstr,JendR
            vclm(Iend+1,j,k)=
     &    (ainterp(1)*V_east(Iend+1,j,k,1)+
     &     ainterp(2)*V_east(Iend+1,j,k,2)+
     &     ainterp(3)*V_east(Iend+1,j,k,3)+
     &     ainterp(4)*V_east(Iend+1,j,k,4)+
     &     ainterp(5)*v(Iend+1,j,k,3-nstp)+
     &     ainterp(6)*v(Iend+1,j,k,nstp))
     &    *vmask(Iend+1,j)
          enddo
        enddo
      endif
      return
       
      

      end subroutine Sub_Loop_v3dbc_interp_tile

