      subroutine Sub_Loop_def_rst(ncid,total_rec,ierr,nrecrst,rstT,rstV,
     &rstU,rstVb,rstUb,rstZ,rstTime2,rstTime,vname,rstTstep,eta_v,eta_rh
     &o,xi_u,xi_rho,mynode,ldefhis,nrpfrst,rstname)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4), parameter :: nf_byte = 1
      integer(4), parameter :: nf_int1 = nf_byte
      integer(4), parameter :: nf_char = 2
      integer(4), parameter :: nf_short = 3
      integer(4), parameter :: nf_int2 = nf_short
      integer(4), parameter :: nf_int = 4
      integer(4), parameter :: nf_float = 5
      integer(4), parameter :: nf_real = nf_float
      integer(4), parameter :: nf_double = 6
      integer(4), parameter :: nf_fill_byte = -127
      integer(4), parameter :: nf_fill_int1 = nf_fill_byte
      integer(4), parameter :: nf_fill_char = 0
      integer(4), parameter :: nf_fill_short = -32767
      integer(4), parameter :: nf_fill_int2 = nf_fill_short
      integer(4), parameter :: nf_fill_int = -2147483647
      real, parameter :: nf_fill_float = 9.9692099683868690D+36
      real, parameter :: nf_fill_real = nf_fill_float
      real(8), parameter :: nf_fill_double = 9.9692099683868690D+36
      integer(4), parameter :: nf_nowrite = 0
      integer(4), parameter :: nf_write = 1
      integer(4), parameter :: nf_clobber = 0
      integer(4), parameter :: nf_noclobber = 4
      integer(4), parameter :: nf_fill = 0
      integer(4), parameter :: nf_nofill = 256
      integer(4), parameter :: nf_lock = 1024
      integer(4), parameter :: nf_share = 2048
      integer(4), parameter :: nf_64bit_offset = 512
      integer(4), parameter :: nf_sizehint_default = 0
      integer(4), parameter :: nf_align_chunk = -1
      integer(4), parameter :: nf_format_classic = 1
      integer(4), parameter :: nf_format_64bit = 2
      integer(4), parameter :: nf_diskless = 8
      integer(4), parameter :: nf_mmap = 16
      integer(4), parameter :: nf_unlimited = 0
      integer(4), parameter :: nf_global = 0
      integer(4), parameter :: nf_max_dims = 1024
      integer(4), parameter :: nf_max_attrs = 8192
      integer(4), parameter :: nf_max_vars = 8192
      integer(4), parameter :: nf_max_name = 256
      integer(4), parameter :: nf_max_var_dims = nf_max_dims
      integer(4), parameter :: nf_noerr = 0
      integer(4), parameter :: nf_ebadid = -33
      integer(4), parameter :: nf_eexist = -35
      integer(4), parameter :: nf_einval = -36
      integer(4), parameter :: nf_eperm = -37
      integer(4), parameter :: nf_enotindefine = -38
      integer(4), parameter :: nf_eindefine = -39
      integer(4), parameter :: nf_einvalcoords = -40
      integer(4), parameter :: nf_emaxdims = -41
      integer(4), parameter :: nf_enameinuse = -42
      integer(4), parameter :: nf_enotatt = -43
      integer(4), parameter :: nf_emaxatts = -44
      integer(4), parameter :: nf_ebadtype = -45
      integer(4), parameter :: nf_ebaddim = -46
      integer(4), parameter :: nf_eunlimpos = -47
      integer(4), parameter :: nf_emaxvars = -48
      integer(4), parameter :: nf_enotvar = -49
      integer(4), parameter :: nf_eglobal = -50
      integer(4), parameter :: nf_enotnc = -51
      integer(4), parameter :: nf_ests = -52
      integer(4), parameter :: nf_emaxname = -53
      integer(4), parameter :: nf_eunlimit = -54
      integer(4), parameter :: nf_enorecvars = -55
      integer(4), parameter :: nf_echar = -56
      integer(4), parameter :: nf_eedge = -57
      integer(4), parameter :: nf_estride = -58
      integer(4), parameter :: nf_ebadname = -59
      integer(4), parameter :: nf_erange = -60
      integer(4), parameter :: nf_enomem = -61
      integer(4), parameter :: nf_evarsize = -62
      integer(4), parameter :: nf_edimsize = -63
      integer(4), parameter :: nf_etrunc = -64
      integer(4), parameter :: nf_fatal = 1
      integer(4), parameter :: nf_verbose = 2
      integer(4), parameter :: nf_ubyte = 7
      integer(4), parameter :: nf_ushort = 8
      integer(4), parameter :: nf_uint = 9
      integer(4), parameter :: nf_int64 = 10
      integer(4), parameter :: nf_uint64 = 11
      integer(4), parameter :: nf_string = 12
      integer(4), parameter :: nf_vlen = 13
      integer(4), parameter :: nf_opaque = 14
      integer(4), parameter :: nf_enum = 15
      integer(4), parameter :: nf_compound = 16
      integer(4), parameter :: nf_fill_ubyte = 255
      integer(4), parameter :: nf_fill_ushort = 65535
      integer(4), parameter :: nf_format_netcdf4 = 3
      integer(4), parameter :: nf_format_netcdf4_classic = 4
      integer(4), parameter :: nf_netcdf4 = 4096
      integer(4), parameter :: nf_classic_model = 256
      integer(4), parameter :: nf_chunk_seq = 0
      integer(4), parameter :: nf_chunk_sub = 1
      integer(4), parameter :: nf_chunk_sizes = 2
      integer(4), parameter :: nf_endian_native = 0
      integer(4), parameter :: nf_endian_little = 1
      integer(4), parameter :: nf_endian_big = 2
      integer(4), parameter :: nf_chunked = 0
      integer(4), parameter :: nf_contiguous = 1
      integer(4), parameter :: nf_nochecksum = 0
      integer(4), parameter :: nf_fletcher32 = 1
      integer(4), parameter :: nf_noshuffle = 0
      integer(4), parameter :: nf_shuffle = 1
      integer(4), parameter :: nf_szip_ec_option_mask = 4
      integer(4), parameter :: nf_szip_nn_option_mask = 32
      integer(4), parameter :: nf_mpiio = 8192
      integer(4), parameter :: nf_mpiposix = 16384
      integer(4), parameter :: nf_pnetcdf = 32768
      integer(4), parameter :: nf_independent = 0
      integer(4), parameter :: nf_collective = 1
      integer(4), parameter :: nf_ehdferr = -101
      integer(4), parameter :: nf_ecantread = -102
      integer(4), parameter :: nf_ecantwrite = -103
      integer(4), parameter :: nf_ecantcreate = -104
      integer(4), parameter :: nf_efilemeta = -105
      integer(4), parameter :: nf_edimmeta = -106
      integer(4), parameter :: nf_eattmeta = -107
      integer(4), parameter :: nf_evarmeta = -108
      integer(4), parameter :: nf_enocompound = -109
      integer(4), parameter :: nf_eattexists = -110
      integer(4), parameter :: nf_enotnc4 = -111
      integer(4), parameter :: nf_estrictnc3 = -112
      integer(4), parameter :: nf_enotnc3 = -113
      integer(4), parameter :: nf_enopar = -114
      integer(4), parameter :: nf_eparinit = -115
      integer(4), parameter :: nf_ebadgrpid = -116
      integer(4), parameter :: nf_ebadtypid = -117
      integer(4), parameter :: nf_etypdefined = -118
      integer(4), parameter :: nf_ebadfield = -119
      integer(4), parameter :: nf_ebadclass = -120
      integer(4), parameter :: nf_emaptype = -121
      integer(4), parameter :: nf_elatefill = -122
      integer(4), parameter :: nf_elatedef = -123
      integer(4), parameter :: nf_edimscale = -124
      integer(4), parameter :: nf_enogrp = -125
      integer(4), parameter :: ncbyte = 1
      integer(4), parameter :: ncchar = 2
      integer(4), parameter :: ncshort = 3
      integer(4), parameter :: nclong = 4
      integer(4), parameter :: ncfloat = 5
      integer(4), parameter :: ncdouble = 6
      integer(4), parameter :: ncrdwr = 1
      integer(4), parameter :: nccreat = 2
      integer(4), parameter :: ncexcl = 4
      integer(4), parameter :: ncindef = 8
      integer(4), parameter :: ncnsync = 16
      integer(4), parameter :: nchsync = 32
      integer(4), parameter :: ncndirty = 64
      integer(4), parameter :: nchdirty = 128
      integer(4), parameter :: ncfill = 0
      integer(4), parameter :: ncnofill = 256
      integer(4), parameter :: nclink = 32768
      integer(4), parameter :: ncnowrit = 0
      integer(4), parameter :: ncwrite = ncrdwr
      integer(4), parameter :: ncclob = nf_clobber
      integer(4), parameter :: ncnoclob = nf_noclobber
      integer(4), parameter :: ncunlim = 0
      integer(4), parameter :: ncglobal = 0
      integer(4), parameter :: maxncop = 64
      integer(4), parameter :: maxncdim = 1024
      integer(4), parameter :: maxncatt = 8192
      integer(4), parameter :: maxncvar = 8192
      integer(4), parameter :: maxncnam = 256
      integer(4), parameter :: maxvdims = maxncdim
      integer(4), parameter :: ncnoerr = nf_noerr
      integer(4), parameter :: ncebadid = nf_ebadid
      integer(4), parameter :: ncenfile = -31
      integer(4), parameter :: nceexist = nf_eexist
      integer(4), parameter :: nceinval = nf_einval
      integer(4), parameter :: nceperm = nf_eperm
      integer(4), parameter :: ncenotin = nf_enotindefine
      integer(4), parameter :: nceindef = nf_eindefine
      integer(4), parameter :: ncecoord = nf_einvalcoords
      integer(4), parameter :: ncemaxds = nf_emaxdims
      integer(4), parameter :: ncename = nf_enameinuse
      integer(4), parameter :: ncenoatt = nf_enotatt
      integer(4), parameter :: ncemaxat = nf_emaxatts
      integer(4), parameter :: ncebadty = nf_ebadtype
      integer(4), parameter :: ncebadd = nf_ebaddim
      integer(4), parameter :: nceunlim = nf_eunlimpos
      integer(4), parameter :: ncemaxvs = nf_emaxvars
      integer(4), parameter :: ncenotvr = nf_enotvar
      integer(4), parameter :: nceglob = nf_eglobal
      integer(4), parameter :: ncenotnc = nf_enotnc
      integer(4), parameter :: ncests = nf_ests
      integer(4), parameter :: ncentool = nf_emaxname
      integer(4), parameter :: ncfoobar = 32
      integer(4), parameter :: ncsyserr = -31
      integer(4), parameter :: ncfatal = 1
      integer(4), parameter :: ncverbos = 2
      integer(4), parameter :: filbyte = -127
      integer(4), parameter :: filchar = 0
      integer(4), parameter :: filshort = -32767
      integer(4), parameter :: fillong = -2147483647
      real, parameter :: filfloat = 9.9692099683868690D+36
      real(8), parameter :: fildoub = 9.9692099683868690D+36
      integer(4) :: nrecrst
      integer(4) :: rstV
      integer(4) :: rstU
      integer(4) :: rstVb
      integer(4) :: rstUb
      integer(4) :: rstZ
      integer(4) :: rstTime2
      integer(4) :: rstTime
      integer(4) :: rstTstep
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: mynode
      logical :: ldefhis
      integer(4) :: nrpfrst
      character(128) :: rstname
      integer(4), dimension(1:NT) :: rstT
      character(75), dimension(1:20,1:500) :: vname
      integer(4) :: ncid
      integer(4) :: total_rec
      integer(4) :: ierr

                   

                              
      logical :: create_new_file
                                                                       
                                                                 
                                                                 
                                                                 
                            
      integer(4) :: rec
      integer(4) :: lstr
      integer(4) :: lvar
      integer(4) :: lenstr
      integer(4) :: timedim
      integer(4), dimension(1:3) :: r2dgrd
      integer(4), dimension(1:3) :: u2dgrd
      integer(4), dimension(1:3) :: v2dgrd
      integer(4), dimension(1:2) :: auxil
      integer(4) :: checkdims
      integer(4), dimension(1:4) :: b3dgrd
      integer(4), dimension(1:4) :: r3dgrd
      integer(4), dimension(1:4) :: u3dgrd
      integer(4), dimension(1:4) :: v3dgrd
      integer(4), dimension(1:4) :: w3dgrd
      integer(4) :: itrc
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                        
                        
                        
                         
                        
                       
                         
                        
                          
                             

                                   

                             

                              

                                    

                            

                              

                                    

                               

                                       
                                       
                                       
                                        
                                       
                                      
                                      
                                     
                                       
                                     

                                             

                                  

                                        

                                              

                                           

                                                        

                                              

                                                         

                           
                         
                           
                             
                        
                          
                        
                         
                                
                                    
                               
                                  
                                
                            
                        
                                

                              

                                

                                  

                             

                                 

                                

                                 

                                       

                                         

                                     

                                       

                                     

                                 

                              

                             
                                  

                          
                               

                            
                             
                            
                            
                                
                                    

                                     

                                    

                                   

                                               

                         
                          
                          
                          
                         
                                
                             
                                
                            
                              
                           
                            
                            
                           
                             
                            
                           
                           
                          
                        
                            
                            
                              
                         
                         
                           
                            
                          
                          
                            
                            
                          
                              

                                 

                                 

                                 

                                

                                       

                                    

                                       

                                   

                                     

                                  

                                   

                                   

                                  

                                    

                                   

                                  

                                  

                                 

                               

                                   

                                   

                                     

                                

                                

                                  

                                   

                                 

                                 

                                   

                                   

                                 

                          
                           
                              

                                

                                    
      character(80) :: nf_inq_libvers
      external       nf_inq_libvers
                                 
      character(80) :: nf_strerror
      external       nf_strerror
                                 
      logical :: nf_issyserr
      external       nf_issyserr
                                       
      integer(4) :: nf_inq_base_pe
      external        nf_inq_base_pe
                                       
      integer(4) :: nf_set_base_pe
      external        nf_set_base_pe
                                  
      integer(4) :: nf_create
      external        nf_create
                                   
      integer(4) :: nf__create
      external        nf__create
                                      
      integer(4) :: nf__create_mp
      external        nf__create_mp
                                
      integer(4) :: nf_open
      external        nf_open
                                 
      integer(4) :: nf__open
      external        nf__open
                                    
      integer(4) :: nf__open_mp
      external        nf__open_mp
                                    
      integer(4) :: nf_set_fill
      external        nf_set_fill
                                              
      integer(4) :: nf_set_default_format
      external        nf_set_default_format
                                 
      integer(4) :: nf_redef
      external        nf_redef
                                  
      integer(4) :: nf_enddef
      external        nf_enddef
                                   
      integer(4) :: nf__enddef
      external        nf__enddef
                                
      integer(4) :: nf_sync
      external        nf_sync
                                 
      integer(4) :: nf_abort
      external        nf_abort
                                 
      integer(4) :: nf_close
      external        nf_close
                                  
      integer(4) :: nf_delete
      external        nf_delete
                               
      integer(4) :: nf_inq
      external        nf_inq
                            
      integer(4) :: nf_inq_path
      external nf_inq_path
                                     
      integer(4) :: nf_inq_ndims
      external        nf_inq_ndims
                                     
      integer(4) :: nf_inq_nvars
      external        nf_inq_nvars
                                     
      integer(4) :: nf_inq_natts
      external        nf_inq_natts
                                        
      integer(4) :: nf_inq_unlimdim
      external        nf_inq_unlimdim
                                      
      integer(4) :: nf_inq_format
      external        nf_inq_format
                                   
      integer(4) :: nf_def_dim
      external        nf_def_dim
                                     
      integer(4) :: nf_inq_dimid
      external        nf_inq_dimid
                                   
      integer(4) :: nf_inq_dim
      external        nf_inq_dim
                                       
      integer(4) :: nf_inq_dimname
      external        nf_inq_dimname
                                      
      integer(4) :: nf_inq_dimlen
      external        nf_inq_dimlen
                                      
      integer(4) :: nf_rename_dim
      external        nf_rename_dim
                                   
      integer(4) :: nf_inq_att
      external        nf_inq_att
                                     
      integer(4) :: nf_inq_attid
      external        nf_inq_attid
                                       
      integer(4) :: nf_inq_atttype
      external        nf_inq_atttype
                                      
      integer(4) :: nf_inq_attlen
      external        nf_inq_attlen
                                       
      integer(4) :: nf_inq_attname
      external        nf_inq_attname
                                    
      integer(4) :: nf_copy_att
      external        nf_copy_att
                                      
      integer(4) :: nf_rename_att
      external        nf_rename_att
                                   
      integer(4) :: nf_del_att
      external        nf_del_att
                                        
      integer(4) :: nf_put_att_text
      external        nf_put_att_text
                                        
      integer(4) :: nf_get_att_text
      external        nf_get_att_text
                                        
      integer(4) :: nf_put_att_int1
      external        nf_put_att_int1
                                        
      integer(4) :: nf_get_att_int1
      external        nf_get_att_int1
                                        
      integer(4) :: nf_put_att_int2
      external        nf_put_att_int2
                                        
      integer(4) :: nf_get_att_int2
      external        nf_get_att_int2
                                       
      integer(4) :: nf_put_att_int
      external        nf_put_att_int
                                       
      integer(4) :: nf_get_att_int
      external        nf_get_att_int
                                        
      integer(4) :: nf_put_att_real
      external        nf_put_att_real
                                        
      integer(4) :: nf_get_att_real
      external        nf_get_att_real
                                          
      integer(4) :: nf_put_att_double
      external        nf_put_att_double
                                          
      integer(4) :: nf_get_att_double
      external        nf_get_att_double
                                   
      integer(4) :: nf_def_var
      external        nf_def_var
                                   
      integer(4) :: nf_inq_var
      external        nf_inq_var
                                     
      integer(4) :: nf_inq_varid
      external        nf_inq_varid
                                       
      integer(4) :: nf_inq_varname
      external        nf_inq_varname
                                       
      integer(4) :: nf_inq_vartype
      external        nf_inq_vartype
                                        
      integer(4) :: nf_inq_varndims
      external        nf_inq_varndims
                                        
      integer(4) :: nf_inq_vardimid
      external        nf_inq_vardimid
                                        
      integer(4) :: nf_inq_varnatts
      external        nf_inq_varnatts
                                      
      integer(4) :: nf_rename_var
      external        nf_rename_var
                                    
      integer(4) :: nf_copy_var
      external        nf_copy_var
                                        
      integer(4) :: nf_put_var_text
      external        nf_put_var_text
                                        
      integer(4) :: nf_get_var_text
      external        nf_get_var_text
                                        
      integer(4) :: nf_put_var_int1
      external        nf_put_var_int1
                                        
      integer(4) :: nf_get_var_int1
      external        nf_get_var_int1
                                        
      integer(4) :: nf_put_var_int2
      external        nf_put_var_int2
                                        
      integer(4) :: nf_get_var_int2
      external        nf_get_var_int2
                                       
      integer(4) :: nf_put_var_int
      external        nf_put_var_int
                                       
      integer(4) :: nf_get_var_int
      external        nf_get_var_int
                                        
      integer(4) :: nf_put_var_real
      external        nf_put_var_real
                                        
      integer(4) :: nf_get_var_real
      external        nf_get_var_real
                                          
      integer(4) :: nf_put_var_double
      external        nf_put_var_double
                                          
      integer(4) :: nf_get_var_double
      external        nf_get_var_double
                                         
      integer(4) :: nf_put_var1_text
      external        nf_put_var1_text
                                         
      integer(4) :: nf_get_var1_text
      external        nf_get_var1_text
                                         
      integer(4) :: nf_put_var1_int1
      external        nf_put_var1_int1
                                         
      integer(4) :: nf_get_var1_int1
      external        nf_get_var1_int1
                                         
      integer(4) :: nf_put_var1_int2
      external        nf_put_var1_int2
                                         
      integer(4) :: nf_get_var1_int2
      external        nf_get_var1_int2
                                        
      integer(4) :: nf_put_var1_int
      external        nf_put_var1_int
                                        
      integer(4) :: nf_get_var1_int
      external        nf_get_var1_int
                                         
      integer(4) :: nf_put_var1_real
      external        nf_put_var1_real
                                         
      integer(4) :: nf_get_var1_real
      external        nf_get_var1_real
                                           
      integer(4) :: nf_put_var1_double
      external        nf_put_var1_double
                                           
      integer(4) :: nf_get_var1_double
      external        nf_get_var1_double
                                         
      integer(4) :: nf_put_vara_text
      external        nf_put_vara_text
                                         
      integer(4) :: nf_get_vara_text
      external        nf_get_vara_text
                                         
      integer(4) :: nf_put_vara_int1
      external        nf_put_vara_int1
                                         
      integer(4) :: nf_get_vara_int1
      external        nf_get_vara_int1
                                         
      integer(4) :: nf_put_vara_int2
      external        nf_put_vara_int2
                                         
      integer(4) :: nf_get_vara_int2
      external        nf_get_vara_int2
                                        
      integer(4) :: nf_put_vara_int
      external        nf_put_vara_int
                                        
      integer(4) :: nf_get_vara_int
      external        nf_get_vara_int
                                         
      integer(4) :: nf_put_vara_real
      external        nf_put_vara_real
                                         
      integer(4) :: nf_get_vara_real
      external        nf_get_vara_real
                                           
      integer(4) :: nf_put_vara_double
      external        nf_put_vara_double
                                           
      integer(4) :: nf_get_vara_double
      external        nf_get_vara_double
                                         
      integer(4) :: nf_put_vars_text
      external        nf_put_vars_text
                                         
      integer(4) :: nf_get_vars_text
      external        nf_get_vars_text
                                         
      integer(4) :: nf_put_vars_int1
      external        nf_put_vars_int1
                                         
      integer(4) :: nf_get_vars_int1
      external        nf_get_vars_int1
                                         
      integer(4) :: nf_put_vars_int2
      external        nf_put_vars_int2
                                         
      integer(4) :: nf_get_vars_int2
      external        nf_get_vars_int2
                                        
      integer(4) :: nf_put_vars_int
      external        nf_put_vars_int
                                        
      integer(4) :: nf_get_vars_int
      external        nf_get_vars_int
                                         
      integer(4) :: nf_put_vars_real
      external        nf_put_vars_real
                                         
      integer(4) :: nf_get_vars_real
      external        nf_get_vars_real
                                           
      integer(4) :: nf_put_vars_double
      external        nf_put_vars_double
                                           
      integer(4) :: nf_get_vars_double
      external        nf_get_vars_double
                                         
      integer(4) :: nf_put_varm_text
      external        nf_put_varm_text
                                         
      integer(4) :: nf_get_varm_text
      external        nf_get_varm_text
                                         
      integer(4) :: nf_put_varm_int1
      external        nf_put_varm_int1
                                         
      integer(4) :: nf_get_varm_int1
      external        nf_get_varm_int1
                                         
      integer(4) :: nf_put_varm_int2
      external        nf_put_varm_int2
                                         
      integer(4) :: nf_get_varm_int2
      external        nf_get_varm_int2
                                        
      integer(4) :: nf_put_varm_int
      external        nf_put_varm_int
                                        
      integer(4) :: nf_get_varm_int
      external        nf_get_varm_int
                                         
      integer(4) :: nf_put_varm_real
      external        nf_put_varm_real
                                         
      integer(4) :: nf_get_varm_real
      external        nf_get_varm_real
                                           
      integer(4) :: nf_put_varm_double
      external        nf_put_varm_double
                                           
      integer(4) :: nf_get_varm_double
      external        nf_get_varm_double
                         
                          
                        
                         
                          
                          
                        
                          
                        
                            
                              

                               

                             

                               

                                

                                

                              

                                

                              

                                  

                                        
                                         
                                     

                                        

                                  
                                       

                                          
                                               

                           
                                   

                                 
                                        

                             
                                  

                             
                                  

                               
                                    

                                 
                                      

                                 
                                      

                              
                                   

                           
                                

                              
                                   

                              
                                   

                              
                                   

                             
                                  

                           
                                

                                       
                                            

                                       
                                             

                         
                                 

                            
                                     

                           
                                    

                               
                                    

                              
                                   

                           
                                   

                             
                                     

                              
                                      

                               
                                       

                             
                                     

                            
                                    

                            
                                    

                            
                                    

                               
                                       

                              
                                      

                           
                                   

                              
                                      

                           
                                   

                          
                                  

                            
                                    

                             
                                     

                             
                                     

                               
                                       

                             
                                     

                             
                                     

                            
                                    

                             
                                     

                            
                                    

                             
                                     

                          
                                  

                              
      integer(4) :: nf_create_par
      external nf_create_par
                            
      integer(4) :: nf_open_par
      external nf_open_par
                                  
      integer(4) :: nf_var_par_access
      external nf_var_par_access
                            
      integer(4) :: nf_inq_ncid
      external nf_inq_ncid
                            
      integer(4) :: nf_inq_grps
      external nf_inq_grps
                               
      integer(4) :: nf_inq_grpname
      external nf_inq_grpname
                                    
      integer(4) :: nf_inq_grpname_full
      external nf_inq_grpname_full
                                   
      integer(4) :: nf_inq_grpname_len
      external nf_inq_grpname_len
                                  
      integer(4) :: nf_inq_grp_parent
      external nf_inq_grp_parent
                                
      integer(4) :: nf_inq_grp_ncid
      external nf_inq_grp_ncid
                                     
      integer(4) :: nf_inq_grp_full_ncid
      external nf_inq_grp_full_ncid
                              
      integer(4) :: nf_inq_varids
      external nf_inq_varids
                              
      integer(4) :: nf_inq_dimids
      external nf_inq_dimids
                           
      integer(4) :: nf_def_grp
      external nf_def_grp
                              
      integer(4) :: nf_rename_grp
      external nf_rename_grp
                                   
      integer(4) :: nf_def_var_deflate
      external nf_def_var_deflate
                                   
      integer(4) :: nf_inq_var_deflate
      external nf_inq_var_deflate
                                      
      integer(4) :: nf_def_var_fletcher32
      external nf_def_var_fletcher32
                                      
      integer(4) :: nf_inq_var_fletcher32
      external nf_inq_var_fletcher32
                                    
      integer(4) :: nf_def_var_chunking
      external nf_def_var_chunking
                                    
      integer(4) :: nf_inq_var_chunking
      external nf_inq_var_chunking
                                
      integer(4) :: nf_def_var_fill
      external nf_def_var_fill
                                
      integer(4) :: nf_inq_var_fill
      external nf_inq_var_fill
                                  
      integer(4) :: nf_def_var_endian
      external nf_def_var_endian
                                  
      integer(4) :: nf_inq_var_endian
      external nf_inq_var_endian
                               
      integer(4) :: nf_inq_typeids
      external nf_inq_typeids
                              
      integer(4) :: nf_inq_typeid
      external nf_inq_typeid
                            
      integer(4) :: nf_inq_type
      external nf_inq_type
                                 
      integer(4) :: nf_inq_user_type
      external nf_inq_user_type
                                
      integer(4) :: nf_def_compound
      external nf_def_compound
                                   
      integer(4) :: nf_insert_compound
      external nf_insert_compound
                                         
      integer(4) :: nf_insert_array_compound
      external nf_insert_array_compound
                                
      integer(4) :: nf_inq_compound
      external nf_inq_compound
                                     
      integer(4) :: nf_inq_compound_name
      external nf_inq_compound_name
                                     
      integer(4) :: nf_inq_compound_size
      external nf_inq_compound_size
                                        
      integer(4) :: nf_inq_compound_nfields
      external nf_inq_compound_nfields
                                      
      integer(4) :: nf_inq_compound_field
      external nf_inq_compound_field
                                          
      integer(4) :: nf_inq_compound_fieldname
      external nf_inq_compound_fieldname
                                           
      integer(4) :: nf_inq_compound_fieldindex
      external nf_inq_compound_fieldindex
                                            
      integer(4) :: nf_inq_compound_fieldoffset
      external nf_inq_compound_fieldoffset
                                          
      integer(4) :: nf_inq_compound_fieldtype
      external nf_inq_compound_fieldtype
                                           
      integer(4) :: nf_inq_compound_fieldndims
      external nf_inq_compound_fieldndims
                                               
      integer(4) :: nf_inq_compound_fielddim_sizes
      external nf_inq_compound_fielddim_sizes
                            
      integer(4) :: nf_def_vlen
      external nf_def_vlen
                            
      integer(4) :: nf_inq_vlen
      external nf_inq_vlen
                             
      integer(4) :: nf_free_vlen
      external nf_free_vlen
                            
      integer(4) :: nf_def_enum
      external nf_def_enum
                               
      integer(4) :: nf_insert_enum
      external nf_insert_enum
                            
      integer(4) :: nf_inq_enum
      external nf_inq_enum
                                   
      integer(4) :: nf_inq_enum_member
      external nf_inq_enum_member
                                  
      integer(4) :: nf_inq_enum_ident
      external nf_inq_enum_ident
                              
      integer(4) :: nf_def_opaque
      external nf_def_opaque
                              
      integer(4) :: nf_inq_opaque
      external nf_inq_opaque
                           
      integer(4) :: nf_put_att
      external nf_put_att
                           
      integer(4) :: nf_get_att
      external nf_get_att
                           
      integer(4) :: nf_put_var
      external nf_put_var
                            
      integer(4) :: nf_put_var1
      external nf_put_var1
                            
      integer(4) :: nf_put_vara
      external nf_put_vara
                            
      integer(4) :: nf_put_vars
      external nf_put_vars
                           
      integer(4) :: nf_get_var
      external nf_get_var
                            
      integer(4) :: nf_get_var1
      external nf_get_var1
                            
      integer(4) :: nf_get_vara
      external nf_get_vara
                            
      integer(4) :: nf_get_vars
      external nf_get_vars
                                  
      integer(4) :: nf_put_var1_int64
      external nf_put_var1_int64
                                  
      integer(4) :: nf_put_vara_int64
      external nf_put_vara_int64
                                  
      integer(4) :: nf_put_vars_int64
      external nf_put_vars_int64
                                  
      integer(4) :: nf_put_varm_int64
      external nf_put_varm_int64
                                 
      integer(4) :: nf_put_var_int64
      external nf_put_var_int64
                                  
      integer(4) :: nf_get_var1_int64
      external nf_get_var1_int64
                                  
      integer(4) :: nf_get_vara_int64
      external nf_get_vara_int64
                                  
      integer(4) :: nf_get_vars_int64
      external nf_get_vars_int64
                                  
      integer(4) :: nf_get_varm_int64
      external nf_get_varm_int64
                                 
      integer(4) :: nf_get_var_int64
      external nf_get_var_int64
                                    
      integer(4) :: nf_get_vlen_element
      external nf_get_vlen_element
                                    
      integer(4) :: nf_put_vlen_element
      external nf_put_vlen_element
                                   
      integer(4) :: nf_set_chunk_cache
      external nf_set_chunk_cache
                                   
      integer(4) :: nf_get_chunk_cache
      external nf_get_chunk_cache
                                       
      integer(4) :: nf_set_var_chunk_cache
      external nf_set_var_chunk_cache
                                       
      integer(4) :: nf_get_var_chunk_cache
      external nf_get_var_chunk_cache
                      
      integer(4) :: nccre
                      
      integer(4) :: ncopn
                       
      integer(4) :: ncddef
                      
      integer(4) :: ncdid
                       
      integer(4) :: ncvdef
                      
      integer(4) :: ncvid
                       
      integer(4) :: nctlen
                       
      integer(4) :: ncsfil
      external nccre
      external ncopn
      external ncddef
      external ncdid
      external ncvdef
      external ncvid
      external nctlen
      external ncsfil
                       
                        
                       
                        
                        
                        
                         
                         
                       
                         
                        
                       
                         
                         
                       
                         
                        
                         
                         
                         
                         
                         
                        
                         
                         
                         
                         
                        
                         
                         
                         
                         
                        
                         
                         
                         
                        
                       
                         
                         
                         
                        
                         
                         
                         
                        
                         
                         
                       
                       
                        
                       
                        
                         
                           

                           

                            

                           

                            

                             

                           

                            

                           

                            

                             

                             

                              

                               

                           

                               

                               

                             

                                 

                                    

                                        

                        
                            

                              

                             

                                

                                

                                

                               

                                    

                                   

                                     

                               

                                     

                                     

                                   

                                            

                                        

                                           

                                       

                                        

                                      

                                       

                                       

                                     

                                        

                                       

                                      

                                     

                                     

                                 

                                        

                              

                               

                            

                             

                        
                        
                         
                        
                    
                              
                                

                             

                                   

                                       

                                                   

                                                  

      ierr=0
      lstr=lenstr(rstname)
      if (nrpfrst.gt.0) then
        lvar=total_rec - (1+mod(total_rec-1, nrpfrst))
        call insert_time_index (rstname, lstr, lvar, ierr)
        if (ierr .ne. 0) goto 99
      endif
      create_new_file=ldefhis
      if (ncid.ne.-1) create_new_file=.false.
      if (mynode.gt.0) create_new_file=.false.
  10  if (create_new_file) then
        ierr=nf_create(rstname(1:lstr),nf_clobber, ncid)
        if (ierr.ne.nf_noerr) then
          write(stdout,'(/3(1x,A)/)') 'ERROR in DEF_RST: Cannot',
     &             'create restart NetCDF file:', rstname(1:lstr)
          goto 99
        endif
        if (nrpfrst.eq.0) total_rec=0
        call put_global_atts (ncid, ierr)
        ierr=nf_def_dim (ncid, 'xi_rho',   xi_rho,  r2dgrd(1))
        ierr=nf_def_dim (ncid, 'xi_u',     xi_u,     u2dgrd(1))
        ierr=nf_def_dim (ncid, 'eta_rho',  eta_rho,  r2dgrd(2))
        ierr=nf_def_dim (ncid, 'eta_v',    eta_v,    v2dgrd(2))
        ierr=nf_def_dim (ncid, 's_rho',    N,        r3dgrd(3))
        ierr=nf_def_dim (ncid, 's_w',      N+1,      w3dgrd(3))
        ierr=nf_def_dim (ncid, 'time', nf_unlimited, timedim)
        ierr=nf_def_dim (ncid, 'auxil',    4,        auxil(1))
        auxil(2)=timedim
        r2dgrd(3)=timedim
        u2dgrd(2)=r2dgrd(2)
        u2dgrd(3)=timedim
        v2dgrd(1)=r2dgrd(1)
        v2dgrd(3)=timedim
        b3dgrd(1)=r2dgrd(1)
        b3dgrd(2)=r2dgrd(2)
        b3dgrd(4)=timedim
        r3dgrd(1)=r2dgrd(1)
        r3dgrd(2)=r2dgrd(2)
        r3dgrd(4)=timedim
        u3dgrd(1)=u2dgrd(1)
        u3dgrd(2)=r2dgrd(2)
        u3dgrd(3)=r3dgrd(3)
        u3dgrd(4)=timedim
        v3dgrd(1)=r2dgrd(1)
        v3dgrd(2)=v2dgrd(2)
        v3dgrd(3)=r3dgrd(3)
        v3dgrd(4)=timedim
        w3dgrd(1)=r2dgrd(1)
        w3dgrd(2)=r2dgrd(2)
        w3dgrd(4)=timedim
        if (total_rec.eq.1) then
         call def_grid_3d(ncid, r2dgrd, u2dgrd, v2dgrd
     &                   ,r3dgrd, w3dgrd)
      endif
        ierr=nf_def_var (ncid, 'time_step', nf_int, 2, auxil,
     &       rstTstep)
        ierr=nf_put_att_text (ncid, rstTstep, 'long_name', 48,
     &       'time step and record numbers from initialization')
        lvar=lenstr(vname(1,indxTime))
        ierr=nf_def_var (ncid, vname(1,indxTime)(1:lvar),
     &                              NF_DOUBLE, 1, timedim, rstTime)
        lvar=lenstr(vname(2,indxTime))
        ierr=nf_put_att_text (ncid, rstTime, 'long_name', lvar,
     &                                  vname(2,indxTime)(1:lvar))
        lvar=lenstr(vname(3,indxTime))
        ierr=nf_put_att_text (ncid, rstTime, 'units',     lvar,
     &                                  vname(3,indxTime)(1:lvar))
        lvar=lenstr (vname(4,indxTime))
        ierr=nf_put_att_text(ncid, rstTime, 'field',     lvar,
     &                                  vname(4,indxTime)(1:lvar))
      call nf_add_attribute(ncid, rstTime, indxTime, 5, NF_DOUBLE, ierr)
        lvar=lenstr(vname(1,indxTime2))
        ierr=nf_def_var (ncid, vname(1,indxTime2)(1:lvar),
     &                              NF_DOUBLE, 1, timedim, rstTime2)
        lvar=lenstr(vname(2,indxTime2))
        ierr=nf_put_att_text (ncid, rstTime2, 'long_name', lvar,
     &                                  vname(2,indxTime2)(1:lvar))
        lvar=lenstr(vname(3,indxTime2))
        ierr=nf_put_att_text (ncid, rstTime2, 'units',     lvar,
     &                                  vname(3,indxTime2)(1:lvar))
        lvar=lenstr (vname(4,indxTime2))
        ierr=nf_put_att_text(ncid, rstTime2, 'field',     lvar,
     &                                  vname(4,indxTime2)(1:lvar))
        call nf_add_attribute(ncid, rstTime2, indxTime2, 5,
     &       NF_DOUBLE, ierr)
        lvar=lenstr(vname(1,indxZ))
        ierr=nf_def_var (ncid, vname(1,indxZ)(1:lvar),
     &                                  NF_DOUBLE, 3, r2dgrd,rstZ)
        lvar=lenstr(vname(2,indxZ))
        ierr=nf_put_att_text (ncid, rstZ, 'long_name', lvar,
     &                                     vname(2,indxZ)(1:lvar))
        lvar=lenstr(vname(3,indxZ))
        ierr=nf_put_att_text (ncid, rstZ, 'units', lvar,
     &                                     vname(3,indxZ)(1:lvar))
        lvar=lenstr(vname(4,indxZ))
        ierr=nf_put_att_text (ncid, rstZ, 'field', lvar,
     &                                     vname(4,indxZ)(1:lvar))
      call nf_add_attribute(ncid, rstZ, indxZ, 5, NF_DOUBLE, ierr)
        lvar=lenstr(vname(1,indxUb))
        ierr=nf_def_var (ncid, vname(1,indxUb)(1:lvar),
     &                               NF_DOUBLE, 3, u2dgrd, rstUb)
        lvar=lenstr(vname(2,indxUb))
        ierr=nf_put_att_text (ncid, rstUb, 'long_name', lvar,
     &                                   vname(2,indxUb)(1:lvar))
        lvar=lenstr(vname(3,indxUb))
        ierr=nf_put_att_text (ncid, rstUb, 'units', lvar,
     &                                   vname(3,indxUb)(1:lvar))
        lvar=lenstr(vname(4,indxUb))
        ierr=nf_put_att_text (ncid, rstUb, 'field', lvar,
     &                                   vname(4,indxUb)(1:lvar))
      call nf_add_attribute(ncid, rstUb, indxUb, 5, NF_DOUBLE, ierr)
        lvar=lenstr(vname(1,indxVb))
        ierr=nf_def_var (ncid, vname(1,indxVb)(1:lvar),
     &                               NF_DOUBLE, 3, v2dgrd, rstVb)
        lvar=lenstr(vname(2,indxVb))
        ierr=nf_put_att_text (ncid, rstVb, 'long_name', lvar,
     &                                   vname(2,indxVb)(1:lvar))
        lvar=lenstr(vname(3,indxVb))
        ierr=nf_put_att_text (ncid, rstVb, 'units',     lvar,
     &                                   vname(3,indxVb)(1:lvar))
        lvar=lenstr(vname(4,indxVb))
        ierr=nf_put_att_text (ncid, rstVb, 'field',     lvar,
     &                                  vname(4,indxVb)(1:lvar))
       call nf_add_attribute(ncid, rstVb, indxVb, 5, NF_DOUBLE, ierr)
        lvar=lenstr(vname(1,indxU))
        ierr=nf_def_var (ncid, vname(1,indxU)(1:lvar),
     &                              NF_DOUBLE, 4, u3dgrd, rstU)
        lvar=lenstr(vname(2,indxU))
        ierr=nf_put_att_text (ncid, rstU, 'long_name', lvar,
     &                                   vname(2,indxU)(1:lvar))
        lvar=lenstr(vname(3,indxU))
        ierr=nf_put_att_text (ncid, rstU, 'units',     lvar,
     &                                   vname(3,indxU)(1:lvar))
        lvar=lenstr(vname(4,indxU))
        ierr=nf_put_att_text (ncid, rstU, 'field',     lvar,
     &                                  vname(4,indxU)(1:lvar))
        call nf_add_attribute(ncid, rstU, indxU, 5, NF_DOUBLE, ierr)
        lvar=lenstr(vname(1,indxV))
        ierr=nf_def_var (ncid, vname(1,indxV)(1:lvar),
     &                              NF_DOUBLE, 4, v3dgrd, rstV)
        lvar=lenstr(vname(2,indxV))
        ierr=nf_put_att_text (ncid, rstV, 'long_name', lvar,
     &                                   vname(2,indxV)(1:lvar))
        lvar=lenstr(vname(3,indxV))
        ierr=nf_put_att_text (ncid, rstV, 'units',     lvar,
     &                                   vname(3,indxV)(1:lvar))
        lvar=lenstr(vname(4,indxV))
        ierr=nf_put_att_text (ncid, rstV, 'field',     lvar,
     &                                  vname(4,indxV)(1:lvar))
        call nf_add_attribute(ncid, rstV, indxVb, 5, NF_DOUBLE, ierr)
        do itrc=1,NT
          lvar=lenstr(vname(1,indxT+itrc-1))
          ierr=nf_def_var (ncid, vname(1,indxT+itrc-1)(1:lvar),
     &         NF_DOUBLE, 4, r3dgrd, rstT(itrc))
          lvar=lenstr(vname(2,indxT+itrc-1))
          ierr=nf_put_att_text (ncid, rstT(itrc), 'long_name',
     &                     lvar,   vname(2,indxT+itrc-1)(1:lvar))
          lvar=lenstr(vname(3,indxT+itrc-1))
          ierr=nf_put_att_text (ncid, rstT(itrc), 'units', lvar,
     &         vname(3,indxT+itrc-1)(1:lvar))
          lvar=lenstr(vname(4,indxT+itrc-1))
          ierr=nf_put_att_text (ncid, rstT(itrc), 'field', lvar,
     &         vname(4,indxT+itrc-1)(1:lvar))
          call nf_add_attribute(ncid, rstT(itrc), indxT+itrc-1, 5,
     &         NF_DOUBLE,ierr)
        enddo
        ierr=nf_enddef(ncid)
        write(*,'(6x,4A,1x,A,i4)') 'DEF_RST - Created new ',
     &              'netCDF file ''', rstname(1:lstr), '''.'
     &               ,' mynode =', mynode
      elseif (ncid.eq.-1) then
        ierr=nf_open (rstname(1:lstr), nf_write, ncid)
        if (ierr. eq. nf_noerr) then
          ierr=checkdims (ncid, rstname, lstr, rec)
          if (ierr .eq. nf_noerr) then
            if (nrpfrst.eq.0) then
              ierr=rec+1 - nrecrst
            else
              ierr=rec+1 - (1+mod(nrecrst-1, abs(nrpfrst)))
            endif
            if (ierr.gt.0) then
              if (mynode.eq.0) write( stdout,
     &                 '(/1x,A,I5,1x,A/8x,3A,I5/8x,A,I5,1x,A/)'
     &           ) 'DEF_RST WARNING: Actual number of records', rec,
     &             'in netCDF file',  '''',  rstname(1:lstr),
     &             ''' exceeds the record number from restart data',
     &             rec+1-ierr,'/', total_rec,', restart is assumed.'
              rec=rec-ierr
            elseif (nrpfrst.eq.0) then
              total_rec=rec+1
              if (mynode.gt.0) total_rec=total_rec-1
            endif
            ierr=nf_noerr
          endif
        endif
        if (ierr. ne. nf_noerr) then
          if (mynode.eq.0) then
            create_new_file=.true.
            goto 10
          else
            write(stdout,'(/1x,4A, 1x,A,I4/)')     'DEF_RST ERROR: ',
     &     'Cannot open restart netCDF file ''',rstname(1:lstr),'''.'
     &               ,' mynode =', mynode
            goto 99
          endif
        endif
        ierr=nf_inq_varid (ncid, 'time_step', rstTstep)
        if (ierr .ne. nf_noerr) then
          write(stdout,1) 'time_step', rstname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxTime))
        ierr=nf_inq_varid (ncid, vname(1,indxTime)(1:lvar), rstTime)
        if (ierr. ne. nf_noerr) then
          write(stdout,1) vname(1,indxTime)(1:lvar), rstname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxTime2))
        ierr=nf_inq_varid (ncid, vname(1,indxTime2)(1:lvar), rstTime2)
        if (ierr. ne. nf_noerr) then
          write(stdout,1) vname(1,indxTime2)(1:lvar), rstname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxZ))
        ierr=nf_inq_varid (ncid, vname(1,indxZ)(1:lvar), rstZ)
        if (ierr. ne. nf_noerr) then
          write(stdout,1) vname(1,indxZ)(1:lvar),  rstname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxUb))
        ierr=nf_inq_varid (ncid, vname(1,indxUb)(1:lvar), rstUb)
        if (ierr. ne. nf_noerr) then
          write(stdout,1) vname(1,indxUb)(1:lvar), rstname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxVb))
        ierr=nf_inq_varid (ncid, vname(1,indxVb)(1:lvar), rstVb)
        if (ierr. ne. nf_noerr) then
          write(stdout,1) vname(1,indxVb)(1:lvar), rstname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxU))
        ierr=nf_inq_varid (ncid, vname(1,indxU)(1:lvar), rstU)
        if (ierr. ne. nf_noerr) then
          write(stdout,1) vname(1,indxU)(1:lvar), rstname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxV))
        ierr=nf_inq_varid (ncid, vname(1,indxV)(1:lvar), rstV)
        if (ierr. ne. nf_noerr) then
          write(stdout,1) vname(1,indxV)(1:lvar), rstname(1:lstr)
          goto 99
        endif
        do itrc=1,NT
          lvar=lenstr(vname(1,indxT+itrc-1))
          ierr=nf_inq_varid (ncid, vname(1,indxT+itrc-1)(1:lvar),
     &                                                rstT(itrc))
          if (ierr. ne. nf_noerr) then
            write(stdout,1) vname(1,indxT+itrc-1)(1:lvar),
     &                                            rstname(1:lstr)
            goto 99
          endif
        enddo
      if (mynode.eq.0) write(*,'(6x,2A,i4,1x,A,i4)')
     &             'DEF_RST -- Opened ',
     &             'existing restart file,  record =', rec
     &              ,' mynode =', mynode
      else
        ierr=nf_open (rstname(1:lstr), nf_write, ncid)
        if (ierr .ne. nf_noerr) then
         if (mynode.eq.0) write(stdout,'(/1x,4A, 1x,A,I4/)')
     &          'DEF_RST ERROR: Cannot',
     &          'open restart netCDF file ''', rstname(1:lstr), '''.'
     &           ,' mynode =', mynode
          goto 99
        endif
      endif
   1  format(/1x,'DEF_RST ERROR: Cannot find variable ''',
     &               A, ''' in netCDF file ''', A, '''.'/)
      if (total_rec.eq.1) call wrt_grid (ncid, rstname, lstr)
  99  return
       
      

      end subroutine Sub_Loop_def_rst

