      integer(4), parameter :: N = 66
      interface
        subroutine Sub_Loop_rhs3d_tile(Istr,Iend,Jstr,Jend,ru,rv,CF,FC,D
     &C,wrk1,wrk2,UFx,UFe,VFx,VFe,padd_E,Mm,padd_X,Lm,on_v,om_v,bvstr,sv
     &str,rvfrc,on_u,om_u,bustr,sustr,rufrc,We,pmask,Hvom,vmask,Mmmpi,Hu
     &on,umask,Lmmpi,dmde,u,dndx,nrhs,v,fomn,Hz,NORTH_INTER,SOUTH_INTER,
     &EAST_INTER,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: gamma = -0.25D0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: nrhs
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rvfrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rufrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: We
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hvom
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Huon
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dmde
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dndx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: fomn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:N) :: ru
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:N) :: rv
      real, dimension(Istr-2:Iend+2,0:N) :: CF
      real, dimension(Istr-2:Iend+2,0:N) :: FC
      real, dimension(Istr-2:Iend+2,0:N) :: DC
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: wrk1
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: wrk2
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFe
        end subroutine Sub_Loop_rhs3d_tile

      end interface
