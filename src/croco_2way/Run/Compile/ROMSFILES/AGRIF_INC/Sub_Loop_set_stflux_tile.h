      subroutine Sub_Loop_set_stflux_tile(Istr,Iend,Jstr,Jend,itrc,padd_
     &E,Mm,padd_X,Lm,may_day_flag,tdays,synchro_flag,stflxp,stflxg,stflx
     &,stf_scale,lstfgrd,ntstart,iic,stf_cycle,stf_time,dt,time,itstf,NO
     &RTH_INTER,Mmmpi,SOUTH_INTER,EAST_INTER,Lmmpi,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: may_day_flag
      real :: tdays
      logical :: synchro_flag
      integer(4) :: ntstart
      integer(4) :: iic
      real :: dt
      real :: time
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(1:2,1:NT) :: stflxp
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2,1:NT) :: stflxg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx
      real, dimension(1:NT) :: stf_scale
      integer(4), dimension(1:NT) :: lstfgrd
      real, dimension(1:NT) :: stf_cycle
      real, dimension(1:2,1:NT) :: stf_time
      integer(4), dimension(1:NT) :: itstf
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: itrc

                   

                                                        
      integer(4) :: i
      integer(4) :: j
      integer(4) :: it1
      integer(4) :: it2
                           
      real :: cff
      real :: cff1
      real :: cff2
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
      if (Istr.eq.1) then
        if (WEST_INTER) then
          IstrR=Istr-2
        else
          IstrR=Istr-1
        endif
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lmmpi) then
        if (EAST_INTER) then
          IendR=Iend+2
        else
          IendR=Iend+1
        endif
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        if (SOUTH_INTER) then
          JstrR=Jstr-2
        else
          JstrR=Jstr-1
        endif
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mmmpi) then
        if (NORTH_INTER) then
          JendR=Jend+2
        else
          JendR=Jend+1
        endif
      else
        JendR=Jend
      endif
      it1=3-itstf(itrc)
      it2=itstf(itrc)
      cff=time+0.5D0*dt
      cff1=stf_time(it2,itrc)-cff
      cff2=cff-stf_time(it1,itrc)
      if (stf_cycle(itrc).lt.0.D0) then
        if (iic.eq.ntstart) then
          if (lstfgrd(itrc).eq.1) then
            cff=stf_scale(itrc)
            do j=JstrR,JendR
              do i=IstrR,IendR
                stflx(i,j,itrc)=cff*stflxg(i,j,itstf(itrc),itrc)
              enddo
            enddo
          else
            cff=stf_scale(itrc)*stflxp(itstf(itrc),itrc)
            do j=JstrR,JendR
              do i=IstrR,IendR
                stflx(i,j,itrc)=cff
              enddo
            enddo
          endif
        endif
      elseif (cff1.ge.0.D0 .and. cff2.ge.0.D0) then
        if (Istr+Jstr.eq.2 .and. cff1.lt.dt) synchro_flag=.TRUE.
        cff=stf_scale(itrc)/(cff1+cff2)
        cff1=cff1*cff
        cff2=cff2*cff
        if (lstfgrd(itrc).eq.1) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              stflx(i,j,itrc)=cff1*stflxg(i,j,it1,itrc)
     &                       +cff2*stflxg(i,j,it2,itrc)
            enddo
          enddo
        else
          cff=cff1*stflxp(it1,itrc)+cff2*stflxp(it2,itrc)
          do j=JstrR,JendR
            do i=IstrR,IendR
              stflx(i,j,itrc)=cff
            enddo
          enddo
        endif
      else
        if (Istr+Jstr.eq.2) then
          write(stdout,1) 'stf_time',tdays,stf_time(it2,itrc)*sec2day
  1       format(/,' SET_STFLUX_TILE - current model time exceeds',
     &          1x,'ending value for variable: ',A8,/,14x,'TDAYS = ',
     &                                    g12.4, 2x, 'TSTF = ',g12.4)
          may_day_flag=2
        endif
      endif
      return
       
      

      end subroutine Sub_Loop_set_stflux_tile

