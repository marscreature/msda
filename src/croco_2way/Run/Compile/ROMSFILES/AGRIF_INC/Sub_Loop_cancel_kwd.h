      subroutine Sub_Loop_cancel_kwd(keyword,ierr,Coptions)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: max_opt_size = 3400
      character(3400) :: Coptions
      character*(*) :: keyword
      integer(4) :: ierr

                   

                             
                                   

                                   
                                     
                            
                                            
      integer(4) :: is
      integer(4) :: i
      integer(4) :: ie
      integer(4) :: lenkw
      integer(4) :: lenstr
      lenkw=lenstr(keyword)
      is=0
   1   is=is+1
       if (Coptions(is:is).eq.' ' .and. is.lt.max_opt_size) goto 1
      ie=is
   2   ie=ie+1
       if (Coptions(ie:ie).ne.' ' .and. ie.lt.max_opt_size) goto 2
      if (lenkw.eq.ie-is .and. Coptions(is:ie-1).eq.keyword) then
        do i=is,ie-1
          Coptions(i:i)=' '
        enddo
        return
      elseif (is.lt.max_opt_size) then
        is=ie
        goto 1
      endif
      write(*,'(2(/1x,A,1x,A,1x,A)/)') 'CANCEL_KW ERROR:',
     &        'Can not cancel keyword:',  keyword(1:lenkw),
     &        'Check SCRUM/ROMS input script for possible',
     &        'duplicated keyword.'
      ierr=ierr+1
      return
       
      

      end subroutine Sub_Loop_cancel_kwd

