      subroutine Sub_Loop_wrt_his(padd_E,Mm,padd_X,Lm,may_day_flag,hisSh
     &flx_sen,shflx_sen,hisShflx_lat,shflx_lat,hisShflx_rlw,shflx_rlw,hi
     &sShflx_rsw,shflx_rsw,hisSwflx,hisShflx,stflx,hisHbbl,hbbl,hisHbl,h
     &bls,hisAks,hisAkt,Akt,hisAkv,Akv,hisDiff,diff3d_v,diff3d_u,diff4,h
     &isW,hisO,pn,pm,We,work,hisR,rho,hisT,rmask,t,hisV,v,hisU,nstp,u,wo
     &rkr,hisVWstr,hisUWstr,hisWstr,svstr,sustr,work2d2,hisBostr,rho0,bv
     &str,bustr,hisVb,vbar,hisUb,ubar,hisZ,knew,zeta,work2d,wrthis,hisTi
     &me2,vname,time,hisTime,hisTstep,nrecavg,nrecrst,iic,nrpfhis,hisnam
     &e,nrechis,ncidhis,mynode)

      use Agrif_Types, only : Agrif_tabvars


      include 'mpif.h'
      real, parameter :: eps = 1.D-20
      real, parameter :: stf_cff = 86400/0.01D0
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4), parameter :: nf_byte = 1
      integer(4), parameter :: nf_int1 = nf_byte
      integer(4), parameter :: nf_char = 2
      integer(4), parameter :: nf_short = 3
      integer(4), parameter :: nf_int2 = nf_short
      integer(4), parameter :: nf_int = 4
      integer(4), parameter :: nf_float = 5
      integer(4), parameter :: nf_real = nf_float
      integer(4), parameter :: nf_double = 6
      integer(4), parameter :: nf_fill_byte = -127
      integer(4), parameter :: nf_fill_int1 = nf_fill_byte
      integer(4), parameter :: nf_fill_char = 0
      integer(4), parameter :: nf_fill_short = -32767
      integer(4), parameter :: nf_fill_int2 = nf_fill_short
      integer(4), parameter :: nf_fill_int = -2147483647
      real, parameter :: nf_fill_float = 9.9692099683868690D+36
      real, parameter :: nf_fill_real = nf_fill_float
      real(8), parameter :: nf_fill_double = 9.9692099683868690D+36
      integer(4), parameter :: nf_nowrite = 0
      integer(4), parameter :: nf_write = 1
      integer(4), parameter :: nf_clobber = 0
      integer(4), parameter :: nf_noclobber = 4
      integer(4), parameter :: nf_fill = 0
      integer(4), parameter :: nf_nofill = 256
      integer(4), parameter :: nf_lock = 1024
      integer(4), parameter :: nf_share = 2048
      integer(4), parameter :: nf_64bit_offset = 512
      integer(4), parameter :: nf_sizehint_default = 0
      integer(4), parameter :: nf_align_chunk = -1
      integer(4), parameter :: nf_format_classic = 1
      integer(4), parameter :: nf_format_64bit = 2
      integer(4), parameter :: nf_diskless = 8
      integer(4), parameter :: nf_mmap = 16
      integer(4), parameter :: nf_unlimited = 0
      integer(4), parameter :: nf_global = 0
      integer(4), parameter :: nf_max_dims = 1024
      integer(4), parameter :: nf_max_attrs = 8192
      integer(4), parameter :: nf_max_vars = 8192
      integer(4), parameter :: nf_max_name = 256
      integer(4), parameter :: nf_max_var_dims = nf_max_dims
      integer(4), parameter :: nf_noerr = 0
      integer(4), parameter :: nf_ebadid = -33
      integer(4), parameter :: nf_eexist = -35
      integer(4), parameter :: nf_einval = -36
      integer(4), parameter :: nf_eperm = -37
      integer(4), parameter :: nf_enotindefine = -38
      integer(4), parameter :: nf_eindefine = -39
      integer(4), parameter :: nf_einvalcoords = -40
      integer(4), parameter :: nf_emaxdims = -41
      integer(4), parameter :: nf_enameinuse = -42
      integer(4), parameter :: nf_enotatt = -43
      integer(4), parameter :: nf_emaxatts = -44
      integer(4), parameter :: nf_ebadtype = -45
      integer(4), parameter :: nf_ebaddim = -46
      integer(4), parameter :: nf_eunlimpos = -47
      integer(4), parameter :: nf_emaxvars = -48
      integer(4), parameter :: nf_enotvar = -49
      integer(4), parameter :: nf_eglobal = -50
      integer(4), parameter :: nf_enotnc = -51
      integer(4), parameter :: nf_ests = -52
      integer(4), parameter :: nf_emaxname = -53
      integer(4), parameter :: nf_eunlimit = -54
      integer(4), parameter :: nf_enorecvars = -55
      integer(4), parameter :: nf_echar = -56
      integer(4), parameter :: nf_eedge = -57
      integer(4), parameter :: nf_estride = -58
      integer(4), parameter :: nf_ebadname = -59
      integer(4), parameter :: nf_erange = -60
      integer(4), parameter :: nf_enomem = -61
      integer(4), parameter :: nf_evarsize = -62
      integer(4), parameter :: nf_edimsize = -63
      integer(4), parameter :: nf_etrunc = -64
      integer(4), parameter :: nf_fatal = 1
      integer(4), parameter :: nf_verbose = 2
      integer(4), parameter :: nf_ubyte = 7
      integer(4), parameter :: nf_ushort = 8
      integer(4), parameter :: nf_uint = 9
      integer(4), parameter :: nf_int64 = 10
      integer(4), parameter :: nf_uint64 = 11
      integer(4), parameter :: nf_string = 12
      integer(4), parameter :: nf_vlen = 13
      integer(4), parameter :: nf_opaque = 14
      integer(4), parameter :: nf_enum = 15
      integer(4), parameter :: nf_compound = 16
      integer(4), parameter :: nf_fill_ubyte = 255
      integer(4), parameter :: nf_fill_ushort = 65535
      integer(4), parameter :: nf_format_netcdf4 = 3
      integer(4), parameter :: nf_format_netcdf4_classic = 4
      integer(4), parameter :: nf_netcdf4 = 4096
      integer(4), parameter :: nf_classic_model = 256
      integer(4), parameter :: nf_chunk_seq = 0
      integer(4), parameter :: nf_chunk_sub = 1
      integer(4), parameter :: nf_chunk_sizes = 2
      integer(4), parameter :: nf_endian_native = 0
      integer(4), parameter :: nf_endian_little = 1
      integer(4), parameter :: nf_endian_big = 2
      integer(4), parameter :: nf_chunked = 0
      integer(4), parameter :: nf_contiguous = 1
      integer(4), parameter :: nf_nochecksum = 0
      integer(4), parameter :: nf_fletcher32 = 1
      integer(4), parameter :: nf_noshuffle = 0
      integer(4), parameter :: nf_shuffle = 1
      integer(4), parameter :: nf_szip_ec_option_mask = 4
      integer(4), parameter :: nf_szip_nn_option_mask = 32
      integer(4), parameter :: nf_mpiio = 8192
      integer(4), parameter :: nf_mpiposix = 16384
      integer(4), parameter :: nf_pnetcdf = 32768
      integer(4), parameter :: nf_independent = 0
      integer(4), parameter :: nf_collective = 1
      integer(4), parameter :: nf_ehdferr = -101
      integer(4), parameter :: nf_ecantread = -102
      integer(4), parameter :: nf_ecantwrite = -103
      integer(4), parameter :: nf_ecantcreate = -104
      integer(4), parameter :: nf_efilemeta = -105
      integer(4), parameter :: nf_edimmeta = -106
      integer(4), parameter :: nf_eattmeta = -107
      integer(4), parameter :: nf_evarmeta = -108
      integer(4), parameter :: nf_enocompound = -109
      integer(4), parameter :: nf_eattexists = -110
      integer(4), parameter :: nf_enotnc4 = -111
      integer(4), parameter :: nf_estrictnc3 = -112
      integer(4), parameter :: nf_enotnc3 = -113
      integer(4), parameter :: nf_enopar = -114
      integer(4), parameter :: nf_eparinit = -115
      integer(4), parameter :: nf_ebadgrpid = -116
      integer(4), parameter :: nf_ebadtypid = -117
      integer(4), parameter :: nf_etypdefined = -118
      integer(4), parameter :: nf_ebadfield = -119
      integer(4), parameter :: nf_ebadclass = -120
      integer(4), parameter :: nf_emaptype = -121
      integer(4), parameter :: nf_elatefill = -122
      integer(4), parameter :: nf_elatedef = -123
      integer(4), parameter :: nf_edimscale = -124
      integer(4), parameter :: nf_enogrp = -125
      integer(4), parameter :: ncbyte = 1
      integer(4), parameter :: ncchar = 2
      integer(4), parameter :: ncshort = 3
      integer(4), parameter :: nclong = 4
      integer(4), parameter :: ncfloat = 5
      integer(4), parameter :: ncdouble = 6
      integer(4), parameter :: ncrdwr = 1
      integer(4), parameter :: nccreat = 2
      integer(4), parameter :: ncexcl = 4
      integer(4), parameter :: ncindef = 8
      integer(4), parameter :: ncnsync = 16
      integer(4), parameter :: nchsync = 32
      integer(4), parameter :: ncndirty = 64
      integer(4), parameter :: nchdirty = 128
      integer(4), parameter :: ncfill = 0
      integer(4), parameter :: ncnofill = 256
      integer(4), parameter :: nclink = 32768
      integer(4), parameter :: ncnowrit = 0
      integer(4), parameter :: ncwrite = ncrdwr
      integer(4), parameter :: ncclob = nf_clobber
      integer(4), parameter :: ncnoclob = nf_noclobber
      integer(4), parameter :: ncunlim = 0
      integer(4), parameter :: ncglobal = 0
      integer(4), parameter :: maxncop = 64
      integer(4), parameter :: maxncdim = 1024
      integer(4), parameter :: maxncatt = 8192
      integer(4), parameter :: maxncvar = 8192
      integer(4), parameter :: maxncnam = 256
      integer(4), parameter :: maxvdims = maxncdim
      integer(4), parameter :: ncnoerr = nf_noerr
      integer(4), parameter :: ncebadid = nf_ebadid
      integer(4), parameter :: ncenfile = -31
      integer(4), parameter :: nceexist = nf_eexist
      integer(4), parameter :: nceinval = nf_einval
      integer(4), parameter :: nceperm = nf_eperm
      integer(4), parameter :: ncenotin = nf_enotindefine
      integer(4), parameter :: nceindef = nf_eindefine
      integer(4), parameter :: ncecoord = nf_einvalcoords
      integer(4), parameter :: ncemaxds = nf_emaxdims
      integer(4), parameter :: ncename = nf_enameinuse
      integer(4), parameter :: ncenoatt = nf_enotatt
      integer(4), parameter :: ncemaxat = nf_emaxatts
      integer(4), parameter :: ncebadty = nf_ebadtype
      integer(4), parameter :: ncebadd = nf_ebaddim
      integer(4), parameter :: nceunlim = nf_eunlimpos
      integer(4), parameter :: ncemaxvs = nf_emaxvars
      integer(4), parameter :: ncenotvr = nf_enotvar
      integer(4), parameter :: nceglob = nf_eglobal
      integer(4), parameter :: ncenotnc = nf_enotnc
      integer(4), parameter :: ncests = nf_ests
      integer(4), parameter :: ncentool = nf_emaxname
      integer(4), parameter :: ncfoobar = 32
      integer(4), parameter :: ncsyserr = -31
      integer(4), parameter :: ncfatal = 1
      integer(4), parameter :: ncverbos = 2
      integer(4), parameter :: filbyte = -127
      integer(4), parameter :: filchar = 0
      integer(4), parameter :: filshort = -32767
      integer(4), parameter :: fillong = -2147483647
      real, parameter :: filfloat = 9.9692099683868690D+36
      real(8), parameter :: fildoub = 9.9692099683868690D+36
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: may_day_flag
      integer(4) :: hisShflx_sen
      integer(4) :: hisShflx_lat
      integer(4) :: hisShflx_rlw
      integer(4) :: hisShflx_rsw
      integer(4) :: hisSwflx
      integer(4) :: hisShflx
      integer(4) :: hisHbbl
      integer(4) :: hisHbl
      integer(4) :: hisAks
      integer(4) :: hisAkt
      integer(4) :: hisAkv
      integer(4) :: hisDiff
      integer(4) :: hisW
      integer(4) :: hisO
      integer(4) :: hisR
      integer(4) :: hisV
      integer(4) :: hisU
      integer(4) :: nstp
      integer(4) :: hisVWstr
      integer(4) :: hisUWstr
      integer(4) :: hisWstr
      integer(4) :: hisBostr
      real :: rho0
      integer(4) :: hisVb
      integer(4) :: hisUb
      integer(4) :: hisZ
      integer(4) :: knew
      integer(4) :: hisTime2
      real :: time
      integer(4) :: hisTime
      integer(4) :: hisTstep
      integer(4) :: nrecavg
      integer(4) :: nrecrst
      integer(4) :: iic
      integer(4) :: nrpfhis
      character(128) :: hisname
      integer(4) :: nrechis
      integer(4) :: ncidhis
      integer(4) :: mynode
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: hbls
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N,1:2) :: Akt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: diff4
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: We
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: work
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho
      integer(4), dimension(1:NT) :: hisT
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: workr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: work2d2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: work2d
      logical, dimension(1:500+NT) :: wrthis
      character(75), dimension(1:20,1:500) :: vname
                                                                       
                                                                 
                                         
      integer(4) :: ierr
      integer(4) :: record
      integer(4) :: lstr
      integer(4) :: lvar
      integer(4) :: lenstr
      integer(4) :: type
      integer(4), dimension(1:2) :: start
      integer(4), dimension(1:2) :: count
      integer(4), dimension(1:4) :: ibuff
      integer(4) :: nf_fwrite
      integer(4) :: cff
               
                            

                   
                                     

                                 
      integer(4) :: tile
      integer(4) :: itrc
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                       
                                               
      integer(4), dimension(1:MPI_STATUS_SIZE) :: status
      integer(4) :: blank
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
!$AGRIF_DO_NOT_TREAT
      INTEGER*4 :: ocean_grid_comm
      common /cpl_comm/ ocean_grid_comm
!$AGRIF_END_DO_NOT_TREAT
                                                   
                           
                                                    
                              
                                                 
                             
                                                  
                               
                        
                        
                        
                         
                        
                       
                         
                        
                          
                             

                                   

                             

                              

                                    

                            

                              

                                    

                               

                                       
                                       
                                       
                                        
                                       
                                      
                                      
                                     
                                       
                                     

                                             

                                  

                                        

                                              

                                           

                                                        

                                              

                                                         

                           
                         
                           
                             
                        
                          
                        
                         
                                
                                    
                               
                                  
                                
                            
                        
                                

                              

                                

                                  

                             

                                 

                                

                                 

                                       

                                         

                                     

                                       

                                     

                                 

                              

                             
                                  

                          
                               

                            
                             
                            
                            
                                
                                    

                                     

                                    

                                   

                                               

                         
                          
                          
                          
                         
                                
                             
                                
                            
                              
                           
                            
                            
                           
                             
                            
                           
                           
                          
                        
                            
                            
                              
                         
                         
                           
                            
                          
                          
                            
                            
                          
                              

                                 

                                 

                                 

                                

                                       

                                    

                                       

                                   

                                     

                                  

                                   

                                   

                                  

                                    

                                   

                                  

                                  

                                 

                               

                                   

                                   

                                     

                                

                                

                                  

                                   

                                 

                                 

                                   

                                   

                                 

                          
                           
                              

                                

                                    
      character(80) :: nf_inq_libvers
      external       nf_inq_libvers
                                 
      character(80) :: nf_strerror
      external       nf_strerror
                                 
      logical :: nf_issyserr
      external       nf_issyserr
                                       
      integer(4) :: nf_inq_base_pe
      external        nf_inq_base_pe
                                       
      integer(4) :: nf_set_base_pe
      external        nf_set_base_pe
                                  
      integer(4) :: nf_create
      external        nf_create
                                   
      integer(4) :: nf__create
      external        nf__create
                                      
      integer(4) :: nf__create_mp
      external        nf__create_mp
                                
      integer(4) :: nf_open
      external        nf_open
                                 
      integer(4) :: nf__open
      external        nf__open
                                    
      integer(4) :: nf__open_mp
      external        nf__open_mp
                                    
      integer(4) :: nf_set_fill
      external        nf_set_fill
                                              
      integer(4) :: nf_set_default_format
      external        nf_set_default_format
                                 
      integer(4) :: nf_redef
      external        nf_redef
                                  
      integer(4) :: nf_enddef
      external        nf_enddef
                                   
      integer(4) :: nf__enddef
      external        nf__enddef
                                
      integer(4) :: nf_sync
      external        nf_sync
                                 
      integer(4) :: nf_abort
      external        nf_abort
                                 
      integer(4) :: nf_close
      external        nf_close
                                  
      integer(4) :: nf_delete
      external        nf_delete
                               
      integer(4) :: nf_inq
      external        nf_inq
                            
      integer(4) :: nf_inq_path
      external nf_inq_path
                                     
      integer(4) :: nf_inq_ndims
      external        nf_inq_ndims
                                     
      integer(4) :: nf_inq_nvars
      external        nf_inq_nvars
                                     
      integer(4) :: nf_inq_natts
      external        nf_inq_natts
                                        
      integer(4) :: nf_inq_unlimdim
      external        nf_inq_unlimdim
                                      
      integer(4) :: nf_inq_format
      external        nf_inq_format
                                   
      integer(4) :: nf_def_dim
      external        nf_def_dim
                                     
      integer(4) :: nf_inq_dimid
      external        nf_inq_dimid
                                   
      integer(4) :: nf_inq_dim
      external        nf_inq_dim
                                       
      integer(4) :: nf_inq_dimname
      external        nf_inq_dimname
                                      
      integer(4) :: nf_inq_dimlen
      external        nf_inq_dimlen
                                      
      integer(4) :: nf_rename_dim
      external        nf_rename_dim
                                   
      integer(4) :: nf_inq_att
      external        nf_inq_att
                                     
      integer(4) :: nf_inq_attid
      external        nf_inq_attid
                                       
      integer(4) :: nf_inq_atttype
      external        nf_inq_atttype
                                      
      integer(4) :: nf_inq_attlen
      external        nf_inq_attlen
                                       
      integer(4) :: nf_inq_attname
      external        nf_inq_attname
                                    
      integer(4) :: nf_copy_att
      external        nf_copy_att
                                      
      integer(4) :: nf_rename_att
      external        nf_rename_att
                                   
      integer(4) :: nf_del_att
      external        nf_del_att
                                        
      integer(4) :: nf_put_att_text
      external        nf_put_att_text
                                        
      integer(4) :: nf_get_att_text
      external        nf_get_att_text
                                        
      integer(4) :: nf_put_att_int1
      external        nf_put_att_int1
                                        
      integer(4) :: nf_get_att_int1
      external        nf_get_att_int1
                                        
      integer(4) :: nf_put_att_int2
      external        nf_put_att_int2
                                        
      integer(4) :: nf_get_att_int2
      external        nf_get_att_int2
                                       
      integer(4) :: nf_put_att_int
      external        nf_put_att_int
                                       
      integer(4) :: nf_get_att_int
      external        nf_get_att_int
                                        
      integer(4) :: nf_put_att_real
      external        nf_put_att_real
                                        
      integer(4) :: nf_get_att_real
      external        nf_get_att_real
                                          
      integer(4) :: nf_put_att_double
      external        nf_put_att_double
                                          
      integer(4) :: nf_get_att_double
      external        nf_get_att_double
                                   
      integer(4) :: nf_def_var
      external        nf_def_var
                                   
      integer(4) :: nf_inq_var
      external        nf_inq_var
                                     
      integer(4) :: nf_inq_varid
      external        nf_inq_varid
                                       
      integer(4) :: nf_inq_varname
      external        nf_inq_varname
                                       
      integer(4) :: nf_inq_vartype
      external        nf_inq_vartype
                                        
      integer(4) :: nf_inq_varndims
      external        nf_inq_varndims
                                        
      integer(4) :: nf_inq_vardimid
      external        nf_inq_vardimid
                                        
      integer(4) :: nf_inq_varnatts
      external        nf_inq_varnatts
                                      
      integer(4) :: nf_rename_var
      external        nf_rename_var
                                    
      integer(4) :: nf_copy_var
      external        nf_copy_var
                                        
      integer(4) :: nf_put_var_text
      external        nf_put_var_text
                                        
      integer(4) :: nf_get_var_text
      external        nf_get_var_text
                                        
      integer(4) :: nf_put_var_int1
      external        nf_put_var_int1
                                        
      integer(4) :: nf_get_var_int1
      external        nf_get_var_int1
                                        
      integer(4) :: nf_put_var_int2
      external        nf_put_var_int2
                                        
      integer(4) :: nf_get_var_int2
      external        nf_get_var_int2
                                       
      integer(4) :: nf_put_var_int
      external        nf_put_var_int
                                       
      integer(4) :: nf_get_var_int
      external        nf_get_var_int
                                        
      integer(4) :: nf_put_var_real
      external        nf_put_var_real
                                        
      integer(4) :: nf_get_var_real
      external        nf_get_var_real
                                          
      integer(4) :: nf_put_var_double
      external        nf_put_var_double
                                          
      integer(4) :: nf_get_var_double
      external        nf_get_var_double
                                         
      integer(4) :: nf_put_var1_text
      external        nf_put_var1_text
                                         
      integer(4) :: nf_get_var1_text
      external        nf_get_var1_text
                                         
      integer(4) :: nf_put_var1_int1
      external        nf_put_var1_int1
                                         
      integer(4) :: nf_get_var1_int1
      external        nf_get_var1_int1
                                         
      integer(4) :: nf_put_var1_int2
      external        nf_put_var1_int2
                                         
      integer(4) :: nf_get_var1_int2
      external        nf_get_var1_int2
                                        
      integer(4) :: nf_put_var1_int
      external        nf_put_var1_int
                                        
      integer(4) :: nf_get_var1_int
      external        nf_get_var1_int
                                         
      integer(4) :: nf_put_var1_real
      external        nf_put_var1_real
                                         
      integer(4) :: nf_get_var1_real
      external        nf_get_var1_real
                                           
      integer(4) :: nf_put_var1_double
      external        nf_put_var1_double
                                           
      integer(4) :: nf_get_var1_double
      external        nf_get_var1_double
                                         
      integer(4) :: nf_put_vara_text
      external        nf_put_vara_text
                                         
      integer(4) :: nf_get_vara_text
      external        nf_get_vara_text
                                         
      integer(4) :: nf_put_vara_int1
      external        nf_put_vara_int1
                                         
      integer(4) :: nf_get_vara_int1
      external        nf_get_vara_int1
                                         
      integer(4) :: nf_put_vara_int2
      external        nf_put_vara_int2
                                         
      integer(4) :: nf_get_vara_int2
      external        nf_get_vara_int2
                                        
      integer(4) :: nf_put_vara_int
      external        nf_put_vara_int
                                        
      integer(4) :: nf_get_vara_int
      external        nf_get_vara_int
                                         
      integer(4) :: nf_put_vara_real
      external        nf_put_vara_real
                                         
      integer(4) :: nf_get_vara_real
      external        nf_get_vara_real
                                           
      integer(4) :: nf_put_vara_double
      external        nf_put_vara_double
                                           
      integer(4) :: nf_get_vara_double
      external        nf_get_vara_double
                                         
      integer(4) :: nf_put_vars_text
      external        nf_put_vars_text
                                         
      integer(4) :: nf_get_vars_text
      external        nf_get_vars_text
                                         
      integer(4) :: nf_put_vars_int1
      external        nf_put_vars_int1
                                         
      integer(4) :: nf_get_vars_int1
      external        nf_get_vars_int1
                                         
      integer(4) :: nf_put_vars_int2
      external        nf_put_vars_int2
                                         
      integer(4) :: nf_get_vars_int2
      external        nf_get_vars_int2
                                        
      integer(4) :: nf_put_vars_int
      external        nf_put_vars_int
                                        
      integer(4) :: nf_get_vars_int
      external        nf_get_vars_int
                                         
      integer(4) :: nf_put_vars_real
      external        nf_put_vars_real
                                         
      integer(4) :: nf_get_vars_real
      external        nf_get_vars_real
                                           
      integer(4) :: nf_put_vars_double
      external        nf_put_vars_double
                                           
      integer(4) :: nf_get_vars_double
      external        nf_get_vars_double
                                         
      integer(4) :: nf_put_varm_text
      external        nf_put_varm_text
                                         
      integer(4) :: nf_get_varm_text
      external        nf_get_varm_text
                                         
      integer(4) :: nf_put_varm_int1
      external        nf_put_varm_int1
                                         
      integer(4) :: nf_get_varm_int1
      external        nf_get_varm_int1
                                         
      integer(4) :: nf_put_varm_int2
      external        nf_put_varm_int2
                                         
      integer(4) :: nf_get_varm_int2
      external        nf_get_varm_int2
                                        
      integer(4) :: nf_put_varm_int
      external        nf_put_varm_int
                                        
      integer(4) :: nf_get_varm_int
      external        nf_get_varm_int
                                         
      integer(4) :: nf_put_varm_real
      external        nf_put_varm_real
                                         
      integer(4) :: nf_get_varm_real
      external        nf_get_varm_real
                                           
      integer(4) :: nf_put_varm_double
      external        nf_put_varm_double
                                           
      integer(4) :: nf_get_varm_double
      external        nf_get_varm_double
                         
                          
                        
                         
                          
                          
                        
                          
                        
                            
                              

                               

                             

                               

                                

                                

                              

                                

                              

                                  

                                        
                                         
                                     

                                        

                                  
                                       

                                          
                                               

                           
                                   

                                 
                                        

                             
                                  

                             
                                  

                               
                                    

                                 
                                      

                                 
                                      

                              
                                   

                           
                                

                              
                                   

                              
                                   

                              
                                   

                             
                                  

                           
                                

                                       
                                            

                                       
                                             

                         
                                 

                            
                                     

                           
                                    

                               
                                    

                              
                                   

                           
                                   

                             
                                     

                              
                                      

                               
                                       

                             
                                     

                            
                                    

                            
                                    

                            
                                    

                               
                                       

                              
                                      

                           
                                   

                              
                                      

                           
                                   

                          
                                  

                            
                                    

                             
                                     

                             
                                     

                               
                                       

                             
                                     

                             
                                     

                            
                                    

                             
                                     

                            
                                    

                             
                                     

                          
                                  

                              
      integer(4) :: nf_create_par
      external nf_create_par
                            
      integer(4) :: nf_open_par
      external nf_open_par
                                  
      integer(4) :: nf_var_par_access
      external nf_var_par_access
                            
      integer(4) :: nf_inq_ncid
      external nf_inq_ncid
                            
      integer(4) :: nf_inq_grps
      external nf_inq_grps
                               
      integer(4) :: nf_inq_grpname
      external nf_inq_grpname
                                    
      integer(4) :: nf_inq_grpname_full
      external nf_inq_grpname_full
                                   
      integer(4) :: nf_inq_grpname_len
      external nf_inq_grpname_len
                                  
      integer(4) :: nf_inq_grp_parent
      external nf_inq_grp_parent
                                
      integer(4) :: nf_inq_grp_ncid
      external nf_inq_grp_ncid
                                     
      integer(4) :: nf_inq_grp_full_ncid
      external nf_inq_grp_full_ncid
                              
      integer(4) :: nf_inq_varids
      external nf_inq_varids
                              
      integer(4) :: nf_inq_dimids
      external nf_inq_dimids
                           
      integer(4) :: nf_def_grp
      external nf_def_grp
                              
      integer(4) :: nf_rename_grp
      external nf_rename_grp
                                   
      integer(4) :: nf_def_var_deflate
      external nf_def_var_deflate
                                   
      integer(4) :: nf_inq_var_deflate
      external nf_inq_var_deflate
                                      
      integer(4) :: nf_def_var_fletcher32
      external nf_def_var_fletcher32
                                      
      integer(4) :: nf_inq_var_fletcher32
      external nf_inq_var_fletcher32
                                    
      integer(4) :: nf_def_var_chunking
      external nf_def_var_chunking
                                    
      integer(4) :: nf_inq_var_chunking
      external nf_inq_var_chunking
                                
      integer(4) :: nf_def_var_fill
      external nf_def_var_fill
                                
      integer(4) :: nf_inq_var_fill
      external nf_inq_var_fill
                                  
      integer(4) :: nf_def_var_endian
      external nf_def_var_endian
                                  
      integer(4) :: nf_inq_var_endian
      external nf_inq_var_endian
                               
      integer(4) :: nf_inq_typeids
      external nf_inq_typeids
                              
      integer(4) :: nf_inq_typeid
      external nf_inq_typeid
                            
      integer(4) :: nf_inq_type
      external nf_inq_type
                                 
      integer(4) :: nf_inq_user_type
      external nf_inq_user_type
                                
      integer(4) :: nf_def_compound
      external nf_def_compound
                                   
      integer(4) :: nf_insert_compound
      external nf_insert_compound
                                         
      integer(4) :: nf_insert_array_compound
      external nf_insert_array_compound
                                
      integer(4) :: nf_inq_compound
      external nf_inq_compound
                                     
      integer(4) :: nf_inq_compound_name
      external nf_inq_compound_name
                                     
      integer(4) :: nf_inq_compound_size
      external nf_inq_compound_size
                                        
      integer(4) :: nf_inq_compound_nfields
      external nf_inq_compound_nfields
                                      
      integer(4) :: nf_inq_compound_field
      external nf_inq_compound_field
                                          
      integer(4) :: nf_inq_compound_fieldname
      external nf_inq_compound_fieldname
                                           
      integer(4) :: nf_inq_compound_fieldindex
      external nf_inq_compound_fieldindex
                                            
      integer(4) :: nf_inq_compound_fieldoffset
      external nf_inq_compound_fieldoffset
                                          
      integer(4) :: nf_inq_compound_fieldtype
      external nf_inq_compound_fieldtype
                                           
      integer(4) :: nf_inq_compound_fieldndims
      external nf_inq_compound_fieldndims
                                               
      integer(4) :: nf_inq_compound_fielddim_sizes
      external nf_inq_compound_fielddim_sizes
                            
      integer(4) :: nf_def_vlen
      external nf_def_vlen
                            
      integer(4) :: nf_inq_vlen
      external nf_inq_vlen
                             
      integer(4) :: nf_free_vlen
      external nf_free_vlen
                            
      integer(4) :: nf_def_enum
      external nf_def_enum
                               
      integer(4) :: nf_insert_enum
      external nf_insert_enum
                            
      integer(4) :: nf_inq_enum
      external nf_inq_enum
                                   
      integer(4) :: nf_inq_enum_member
      external nf_inq_enum_member
                                  
      integer(4) :: nf_inq_enum_ident
      external nf_inq_enum_ident
                              
      integer(4) :: nf_def_opaque
      external nf_def_opaque
                              
      integer(4) :: nf_inq_opaque
      external nf_inq_opaque
                           
      integer(4) :: nf_put_att
      external nf_put_att
                           
      integer(4) :: nf_get_att
      external nf_get_att
                           
      integer(4) :: nf_put_var
      external nf_put_var
                            
      integer(4) :: nf_put_var1
      external nf_put_var1
                            
      integer(4) :: nf_put_vara
      external nf_put_vara
                            
      integer(4) :: nf_put_vars
      external nf_put_vars
                           
      integer(4) :: nf_get_var
      external nf_get_var
                            
      integer(4) :: nf_get_var1
      external nf_get_var1
                            
      integer(4) :: nf_get_vara
      external nf_get_vara
                            
      integer(4) :: nf_get_vars
      external nf_get_vars
                                  
      integer(4) :: nf_put_var1_int64
      external nf_put_var1_int64
                                  
      integer(4) :: nf_put_vara_int64
      external nf_put_vara_int64
                                  
      integer(4) :: nf_put_vars_int64
      external nf_put_vars_int64
                                  
      integer(4) :: nf_put_varm_int64
      external nf_put_varm_int64
                                 
      integer(4) :: nf_put_var_int64
      external nf_put_var_int64
                                  
      integer(4) :: nf_get_var1_int64
      external nf_get_var1_int64
                                  
      integer(4) :: nf_get_vara_int64
      external nf_get_vara_int64
                                  
      integer(4) :: nf_get_vars_int64
      external nf_get_vars_int64
                                  
      integer(4) :: nf_get_varm_int64
      external nf_get_varm_int64
                                 
      integer(4) :: nf_get_var_int64
      external nf_get_var_int64
                                    
      integer(4) :: nf_get_vlen_element
      external nf_get_vlen_element
                                    
      integer(4) :: nf_put_vlen_element
      external nf_put_vlen_element
                                   
      integer(4) :: nf_set_chunk_cache
      external nf_set_chunk_cache
                                   
      integer(4) :: nf_get_chunk_cache
      external nf_get_chunk_cache
                                       
      integer(4) :: nf_set_var_chunk_cache
      external nf_set_var_chunk_cache
                                       
      integer(4) :: nf_get_var_chunk_cache
      external nf_get_var_chunk_cache
                      
      integer(4) :: nccre
                      
      integer(4) :: ncopn
                       
      integer(4) :: ncddef
                      
      integer(4) :: ncdid
                       
      integer(4) :: ncvdef
                      
      integer(4) :: ncvid
                       
      integer(4) :: nctlen
                       
      integer(4) :: ncsfil
      external nccre
      external ncopn
      external ncddef
      external ncdid
      external ncvdef
      external ncvid
      external nctlen
      external ncsfil
                       
                        
                       
                        
                        
                        
                         
                         
                       
                         
                        
                       
                         
                         
                       
                         
                        
                         
                         
                         
                         
                         
                        
                         
                         
                         
                         
                        
                         
                         
                         
                         
                        
                         
                         
                         
                        
                       
                         
                         
                         
                        
                         
                         
                         
                        
                         
                         
                       
                       
                        
                       
                        
                         
                           

                           

                            

                           

                            

                             

                           

                            

                           

                            

                             

                             

                              

                               

                           

                               

                               

                             

                                 

                                    

                                        

                        
                            

                              

                             

                                

                                

                                

                               

                                    

                                   

                                     

                               

                                     

                                     

                                   

                                            

                                        

                                           

                                       

                                        

                                      

                                       

                                       

                                     

                                        

                                       

                                      

                                     

                                     

                                 

                                        

                              

                               

                            

                             

                        
                        
                         
                        
                    
                              
                                

                             

                                   

                                       

                                                   

                                                  

      if (mynode.gt.0) then
        call MPI_Recv (blank, 1, MPI_INTEGER, mynode-1,
     &                 1, MPI_COMM_WORLD, status, ierr)
      endif
      call def_his (ncidhis, nrechis, ierr)
      if (ierr .ne. nf_noerr) goto 99
      lstr=lenstr(hisname)
      nrechis=max(1,nrechis)
      if (nrpfhis.eq.0) then
        record=nrechis
      else
        record=1+mod(nrechis-1, nrpfhis)
      endif
      type=filetype_his
      ibuff(1)=iic
      ibuff(2)=nrecrst
      ibuff(3)=nrechis
      ibuff(4)=nrecavg
      start(1)=1
      start(2)=record
      count(1)=4
      count(2)=1
      ierr=nf_put_vara_int (ncidhis, hisTstep, start, count, ibuff)
      if (ierr .ne. nf_noerr) then
         write(stdout,1) 'time_step', record, ierr
     &        ,' mynode =', mynode
         goto 99
      endif
      ierr=nf_put_var1_double (ncidhis, hisTime, record, time)
      if (ierr .ne. nf_noerr) then
         lvar=lenstr(vname(1,indxTime))
         write(stdout,1) vname(1,indxTime)(1:lvar), record, ierr
     &        ,' mynode =', mynode
         goto 99
      endif
      ierr=nf_put_var1_double (ncidhis, hisTime2, record, time)
      if (ierr .ne. nf_noerr) then
         lvar=lenstr(vname(1,indxTime2))
         write(stdout,1) vname(1,indxTime2)(1:lvar), record, ierr
     &        ,' mynode =', mynode
         goto 99
      endif
      if (wrthis(indxZ)) then
         work2d=zeta(:,:,knew)
         call fillvalue2d(work2d,ncidhis,hisZ,indxZ,
     &        record,r2dvar,type)
      endif
      if (wrthis(indxUb)) then
         work2d(:,:)=ubar(:,:,knew)
         call fillvalue2d(work2d,ncidhis,hisUb,indxUb,
     &        record,u2dvar,type)
      endif
      if (wrthis(indxVb)) then
         work2d(:,:)=vbar(:,:,knew)
         call fillvalue2d(work2d,ncidhis,hisVb,indxVb,
     &        record,v2dvar,type)
      endif
      if (wrthis(indxBostr)) then
         do j=0,Mm
            do i=0,Lm
               work2d(i,j)=0.5D0*sqrt((bustr(i,j)+bustr(i+1,j))**2
     &                             +(bvstr(i,j)+bvstr(i,j+1))**2)
     &                                                      *rho0
            enddo
         enddo
         call fillvalue2d(work2d,ncidhis,hisBostr,indxBostr,
     &        record,r2dvar,type)
      endif
      if (wrthis(indxWstr)) then
         do j=1,Mm
            do i=1,Lm
              work2d2(i,j)=0.5D0*sqrt((sustr(i,j)+sustr(i+1,j))**2
     &                              +(svstr(i,j)+svstr(i,j+1))**2)
     &                                                       *rho0
            enddo
         enddo
         ierr=nf_fwrite(work2d2, ncidhis, hisWstr, record, r2dvar)
         if (ierr .ne. nf_noerr) then
            lvar=lenstr(vname(1,indxWstr))
            write(stdout,1) vname(1,indxWstr)(1:lvar), record, ierr
     &           ,' mynode =', mynode
            goto 99
         endif
      endif
      if (wrthis(indxUWstr)) then
         work2d=sustr*rho0
         ierr=nf_fwrite(work2d, ncidhis, hisUWstr, record, u2dvar)
         if (ierr .ne. nf_noerr) then
            lvar=lenstr(vname(1,indxUWstr))
            write(stdout,1) vname(1,indxUWstr)(1:lvar), record, ierr
     &           ,' mynode =', mynode
            goto 99
         endif
      endif
      if (wrthis(indxVWstr)) then
         work2d=svstr*rho0
         ierr=nf_fwrite(work2d,ncidhis,hisVWstr,record,v2dvar)
         if (ierr .ne. nf_noerr) then
            lvar=lenstr(vname(1,indxVWstr))
            write(stdout,1) vname(1,indxVWstr)(1:lvar), record, ierr
     &           ,' mynode =', mynode
            goto 99
         endif
      endif
      if (wrthis(indxU)) then
         workr=u(:,:,:,nstp)
         call fillvalue3d(workr,ncidhis,hisU,indxU,
     &        record,u3dvar,type)
      endif
      if (wrthis(indxV)) then
         workr=v(:,:,:,nstp)
         call fillvalue3d(workr,ncidhis,hisV,indxV,
     &        record,v3dvar,type)
      endif
      do itrc=1,NT
         if (wrthis(indxT+itrc-1)) then
            workr=t(:,:,:,nstp,itrc)
            if (iic.eq.0) then
               do k=1,N
                  workr(:,:,k)=workr(:,:,k)
     &                         * rmask(:,:)
               enddo
            endif
            call fillvalue3d(workr,ncidhis,hisT(itrc),
     &           indxT+itrc-1,record,r3dvar,type)
         endif
      enddo
      if (wrthis(indxR)) then
         workr=rho+rho0-1000.D0
         call fillvalue3d(workr,ncidhis,hisR,indxR,
     &        record,r3dvar,type)
      endif
      if (wrthis(indxO)) then
         do k=0,N
            do j=0,Mm+1
               do i=0,Lm+1
                  work(i,j,k)= ( We(i,j,k)
     &                              ) *pm(i,j)*pn(i,j)
               enddo
            enddo
         enddo
         call fillvalue3d_w(work,ncidhis,hisO,indxO,
     &        record,w3dvar,type)
      endif
      if (wrthis(indxW)) then
         do tile=0,NSUB_X*NSUB_E-1
            call Wvlcty (tile, workr)
         enddo
         call fillvalue3d(workr,ncidhis,hisW,indxW,record,r3dvar,type)
      endif
      if (wrthis(indxDiff)) then
         do k=1,N
            do j=1,Mm
               do i=1,Lm
                  workr(i,j,k)=
     &                 diff4(i,j,itemp)
     &                 +0.25D0*(diff3d_u(i,j,k)+diff3d_u(i+1,j,k)
     &                 +diff3d_v(i,j,k)+diff3d_v(i,j+1,k))
               enddo
            enddo
         enddo
         call fillvalue3d(workr,ncidhis,hisDiff,indxDiff,
     &        record,r3dvar,type)
      endif
      if (wrthis(indxAkv)) then
         work=Akv
         call fillvalue3d_w(work,ncidhis,hisAkv,indxAkv,
     &        record,w3dvar,type)
      endif
      if (wrthis(indxAkt)) then
         work=Akt(:,:,:,itemp)
         call fillvalue3d_w(work,ncidhis,hisAkt,indxAkt,
     &        record,w3dvar,type)
      endif
      if (wrthis(indxAks)) then
         work=Akt(:,:,:,isalt)
         call fillvalue3d_w(work,ncidhis,hisAks,indxAks,
     &        record,w3dvar,type)
      endif
      if (wrthis(indxHbl)) then
         work2d=hbls(:,:,nstp)
         call fillvalue2d(work2d,ncidhis,hisHbl,indxHbl,
     &        record,r2dvar,type)
      endif
      if (wrthis(indxHbbl)) then
         work2d=hbbl
         call fillvalue2d(work2d,ncidhis,hisHbbl,indxHbbl,
     &        record,r2dvar,type)
      endif
      if (wrthis(indxShflx)) then
         work2d=stflx(:,:,itemp)*rho0*Cp
     &          * rmask
         ierr=nf_fwrite(work2d, ncidhis, hisShflx, record, r2dvar)
         if (ierr .ne. nf_noerr) then
            lvar=lenstr(vname(1,indxShflx))
            write(stdout,1) vname(1,indxShflx)(1:lvar), record, ierr
     &           ,' mynode =', mynode
            goto 99
         endif
      endif
      if (wrthis(indxSwflx)) then
         do j=0,Mm+1
            do i=0,Lm+1
               work2d(i,j)=stf_cff*stflx(i,j,isalt)/
     &              ( max(eps,t(i,j,N,nstp,isalt)))
     &              * rmask(i,j)
            enddo
        enddo
         ierr=nf_fwrite(work2d, ncidhis, hisSwflx, record, r2dvar)
         if (ierr .ne. nf_noerr) then
            lvar=lenstr(vname(1,indxSwflx))
            write(stdout,1) vname(1,indxSwflx)(1:lvar), record, ierr
     &           ,' mynode =', mynode
            goto 99
         endif
      endif
      if (wrthis(indxShflx_rsw)) then
         work2d=shflx_rsw*rho0*Cp
         ierr=nf_fwrite(work2d, ncidhis, hisShflx_rsw, record,
     &        r2dvar)
         if (ierr .ne. nf_noerr) then
            lvar=lenstr(vname(1,indxShflx_rsw))
            write(stdout,1) vname(1,indxShflx_rsw)(1:lvar),
     &           record, ierr
     &           ,' mynode =', mynode
            goto 99
         endif
      endif
      if (wrthis(indxShflx_rlw)) then
         work2d=shflx_rlw*rho0*Cp
         ierr=nf_fwrite(work2d, ncidhis, hisShflx_rlw, record,
     &        r2dvar)
         if (ierr .ne. nf_noerr) then
            lvar=lenstr(vname(1,indxShflx_rlw))
            write(stdout,1) vname(1,indxShflx_rlw)(1:lvar),
     &           record, ierr
     &           ,' mynode =', mynode
            goto 99
         endif
      endif
      if (wrthis(indxShflx_lat)) then
         work2d=shflx_lat*rho0*Cp
         ierr=nf_fwrite(work2d, ncidhis, hisShflx_lat, record,
     &        r2dvar)
         if (ierr .ne. nf_noerr) then
            lvar=lenstr(vname(1,indxShflx_lat))
            write(stdout,1) vname(1,indxShflx_lat)(1:lvar),
     &           record, ierr
     &           ,' mynode =', mynode
            goto 99
         endif
      endif
      if (wrthis(indxShflx_sen)) then
        work2d=shflx_sen*rho0*Cp
        ierr=nf_fwrite(work2d, ncidhis, hisShflx_sen, record,
     &        r2dvar)
         if (ierr .ne. nf_noerr) then
            lvar=lenstr(vname(1,indxShflx_sen))
            write(stdout,1) vname(1,indxShflx_sen)(1:lvar),
     &           record, ierr
     &           ,' mynode =', mynode
            goto 99
         endif
      endif
 1    format(/1x,'WRT_HIS ERROR while writing variable ''', A,
     &     ''' into history file.', /11x, 'Time record:',
     &     I6,3x,'netCDF error code',i4,3x,a,i4)
      goto 100
 99   may_day_flag=3
 100  continue
      ierr=nf_close (ncidhis)
      if (nrpfhis.gt.0 .and. record.ge.nrpfhis) ncidhis=-1
      if (ierr .eq. nf_noerr) then
       if (mynode.eq.0) write(stdout,'(6x,A,2(A,I4,1x),A,I3)')
     &       'WRT_HIS -- wrote ',
     &       'history fields into time record =', record, '/',
     &        nrechis  ,' mynode =', mynode
      else
        if (mynode.eq.0) write(stdout,'(/1x,2A/)')
     &        'WRT_HIS ERROR: Cannot ',
     &        'synchronize/close history netCDF file.'
         may_day_flag=3
      endif
      if (mynode .lt. NNODES-1) then
         call MPI_Send (blank, 1, MPI_INTEGER, mynode+1,
     &        1, MPI_COMM_WORLD,  ierr)
      endif
      return
       
      

      end subroutine Sub_Loop_wrt_his

