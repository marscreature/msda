      subroutine Sub_Loop_lmd_finalize_tile(istr,iend,jstr,jend,Kv,Kt,Ks
     &,padd_E,Mm,padd_X,Lm,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER
     &,Akt,rmask,Akv)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N,1:2) :: Akt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv
      integer(4) :: istr
      integer(4) :: iend
      integer(4) :: jstr
      integer(4) :: jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                                                       
                                                                 
                                                                 
          
      do j=jstr,jend
        do i=istr,iend
          do k=1,N-1
            Akv(i,j,k)       =Kv(i,j,k) * rmask(i,j)
            Akt(i,j,k,itemp) =Kt(i,j,k) * rmask(i,j)
            Akt(i,j,k,isalt) =Ks(i,j,k) * rmask(i,j)
          enddo
          Akv(i,j,N)       =max(1.5D0*Kv(i,j,N-1)-0.5D0*Kv(i,j,N-2),
     &                                                             0.D0)
          Akt(i,j,N,itemp) =max(1.5D0*Kt(i,j,N-1)-0.5D0*Kt(i,j,N-2),
     &                                                             0.D0)
          Akt(i,j,N,isalt) =max(1.5D0*Ks(i,j,N-1)-0.5D0*Ks(i,j,N-2),
     &                                                             0.D0)
          Akv(i,j,0)       =0.D0
          Akt(i,j,0,itemp) =0.D0
          Akt(i,j,0,isalt) =0.D0
        enddo
      enddo
      if (.not.WEST_INTER) then
        do j=jstr,jend
          do k=0,N
            Akv(istr-1,j,k)=Akv(istr,j,k)
            Akt(istr-1,j,k,itemp)=Akt(istr,j,k,itemp)
            Akt(istr-1,j,k,isalt)=Akt(istr,j,k,isalt)
          enddo
        enddo
      endif
      if (.not.EAST_INTER) then
        do j=jstr,jend
          do k=0,N
            Akv(iend+1,j,k)=Akv(iend,j,k)
            Akt(iend+1,j,k,itemp)=Akt(iend,j,k,itemp)
            Akt(iend+1,j,k,isalt)=Akt(iend,j,k,isalt)
          enddo
        enddo
      endif
      if (.not.SOUTH_INTER) then
        do i=istr,iend
          do k=0,N
            Akv(i,jstr-1,k)=Akv(i,jstr,k)
            Akt(i,jstr-1,k,itemp)=Akt(i,jstr,k,itemp)
            Akt(i,jstr-1,k,isalt)=Akt(i,jstr,k,isalt)
          enddo
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=istr,iend
          do k=0,N
            Akv(i,jend+1,k)=Akv(i,jend,k)
            Akt(i,jend+1,k,itemp)=Akt(i,jend,k,itemp)
            Akt(i,jend+1,k,isalt)=Akt(i,jend,k,isalt)
          enddo
        enddo
      endif
      if (.not.WEST_INTER .and. .not.SOUTH_INTER) then
        do k=0,N
          Akv(istr-1,jstr-1,k)=Akv(istr,jstr,k)
          Akt(istr-1,jstr-1,k,itemp)=Akt(istr,jstr,k,itemp)
          Akt(istr-1,jstr-1,k,isalt)=Akt(istr,jstr,k,isalt)
        enddo
      endif
      if (.not.WEST_INTER .and. .not.NORTH_INTER) then
        do k=0,N
          Akv(istr-1,jend+1,k)=Akv(istr,jend,k)
          Akt(istr-1,jend+1,k,itemp)=Akt(istr,jend,k,itemp)
          Akt(istr-1,jend+1,k,isalt)=Akt(istr,jend,k,isalt)
        enddo
      endif
      if (.not.EAST_INTER .and. .not.SOUTH_INTER) then
        do k=0,N
          Akv(iend+1,jstr-1,k)=Akv(iend,jstr,k)
          Akt(iend+1,jstr-1,k,itemp)=Akt(iend,jstr,k,itemp)
          Akt(iend+1,jstr-1,k,isalt)=Akt(iend,jstr,k,isalt)
        enddo
      endif
      if (.not.EAST_INTER .and. .not.NORTH_INTER) then
        do k=0,N
          Akv(iend+1,jend+1,k)=Akv(iend,jend,k)
          Akt(iend+1,jend+1,k,itemp)=Akt(iend,jend,k,itemp)
          Akt(iend+1,jend+1,k,isalt)=Akt(iend,jend,k,isalt)
        enddo
      endif
      call exchange_w3d_tile (istr,iend,jstr,jend, Akv)
      call exchange_w3d_tile (istr,iend,jstr,jend,
     &                        Akt(-1,-1,0,itemp))
      call exchange_w3d_tile (istr,iend,jstr,jend,
     &                        Akt(-1,-1,0,isalt))
      return
       
      

      end subroutine Sub_Loop_lmd_finalize_tile

