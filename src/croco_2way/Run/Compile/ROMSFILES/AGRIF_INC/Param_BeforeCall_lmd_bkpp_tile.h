      integer(4), parameter :: N = 66
      interface
        subroutine Sub_Loop_lmd_bkpp_tile(istr,iend,jstr,jend,Kv,Kt,Ks,w
     &s,wm,my_hbbl,wrk,Cr,Gm1,dGm1dS,Gt1,dGt1dS,Gs1,dGs1dS,my_kbbl,padd_
     &E,Mm,padd_X,Lm,hbls,kbbl,hbbl,rmask,pmask2,f,bvf,Hz,v,nstp,u,Zob,z
     &_w,z_r,bvstr,bustr,ustar,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_I
     &NTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: lmd_nu0c = 0.1D0
      real, parameter :: Ricr = 0.3D0
      real, parameter :: Ri_inv = 1.D0/Ricr
      real, parameter :: C_Ek = 258.D0
      real, parameter :: eps = 1.D-20
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      real :: Zob
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: hbls
      integer(4), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: kbbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: f
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: z_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ustar
      integer(4) :: istr
      integer(4) :: iend
      integer(4) :: jstr
      integer(4) :: jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: ws
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: wm
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: my_hbbl
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: wrk
      real, dimension(Istr-2:Iend+2,0:N) :: Cr
      real, dimension(Istr-2:Iend+2) :: Gm1
      real, dimension(Istr-2:Iend+2) :: dGm1dS
      real, dimension(Istr-2:Iend+2) :: Gt1
      real, dimension(Istr-2:Iend+2) :: dGt1dS
      real, dimension(Istr-2:Iend+2) :: Gs1
      real, dimension(Istr-2:Iend+2) :: dGs1dS
      integer(4), dimension(Istr-2:Iend+2) :: my_kbbl
        end subroutine Sub_Loop_lmd_bkpp_tile

      end interface
