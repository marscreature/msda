      interface
        subroutine Sub_Loop_alfabeta_tile(Istr,Iend,Jstr,Jend,alpha,beta
     &,padd_E,Mm,padd_X,Lm,rho0,nstp,t,NORTH_INTER,SOUTH_INTER,EAST_INTE
     &R,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: Q00 = +999.842594D0
      real, parameter :: Q01 = +6.793952D-2
      real, parameter :: Q02 = -9.095290D-3
      real, parameter :: Q03 = +1.001685D-4
      real, parameter :: Q04 = -1.120083D-6
      real, parameter :: Q05 = +6.536332D-9
      real, parameter :: U00 = +0.824493D0
      real, parameter :: U01 = -4.08990D-3
      real, parameter :: U02 = +7.64380D-5
      real, parameter :: U03 = -8.24670D-7
      real, parameter :: U04 = +5.38750D-9
      real, parameter :: V00 = -5.72466D-3
      real, parameter :: V01 = +1.02270D-4
      real, parameter :: V02 = -1.65460D-6
      real, parameter :: W00 = +4.8314D-4
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: rho0
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: alpha
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: beta
        end subroutine Sub_Loop_alfabeta_tile

      end interface
