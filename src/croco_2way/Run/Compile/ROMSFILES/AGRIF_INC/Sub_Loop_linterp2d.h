      subroutine Sub_Loop_linterp2d(LBx,UBx,LBy,UBy,Xinp,Yinp,Finp,Istr,
     &Iend,Jstr,Jend,Xout,Yout,Fout,padd_E,Mm,padd_X,Lm)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4), intent(in) :: LBx
      integer(4), intent(in) :: UBx
      integer(4), intent(in) :: LBy
      integer(4), intent(in) :: UBy
      integer(4), intent(in) :: Istr
      integer(4), intent(in) :: Iend
      integer(4), intent(in) :: Jstr
      integer(4), intent(in) :: Jend
      real(8), intent(in), dimension(LBx:UBx) :: Xinp
      real(8), intent(in), dimension(LBy:UBy) :: Yinp
      real(8), intent(in), dimension(LBx:UBx,LBy:UBy) :: Finp
      real(8), intent(in), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: X
     &out
      real(8), intent(in), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Y
     &out
      real(8), intent(out), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: 
     &Fout

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                  
                                                      
                                                
                                                
                                                        
                                                                      
                                                                      
                                                                       
                                             
      integer(4) :: i
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: ii
      integer(4) :: jj
                                             
      real(8) :: cff
      real(8) :: x
      real(8) :: x1
      real(8) :: x2
      real(8) :: y
      real(8) :: y1
      real(8) :: y2
      DO j=Jstr,Jend
        DO i=Istr,Iend
           DO ii=LBx,(UBx-1)
             if ((Xinp(ii).le.Xout(i,j)).and.
     &           (Xinp(ii+1).gt.Xout(i,j))) then
               i1=ii
               i2=ii+1
               goto 10
             endif
           enddo
           print*, 'Did not find i1 and i2'
           goto 100
 10        continue
           DO jj=LBy,(UBy-1)
             if ((Yinp(jj).le.Yout(i,j)).and.
     &           (Yinp(jj+1).gt.Yout(i,j))) then
               j1=jj
               j2=jj+1
               goto 20
             endif
           enddo
           print*, 'Did not find j1 and j2'
           goto 100
 20        continue
          IF (((LBx.le.i1).and.(i1.le.UBx)).and.
     &        ((LBy.le.j1).and.(j1.le.UBy))) THEN
            x1=Xinp(i1)
            x2=Xinp(i2)
            y1=Yinp(j1)
            y2=Yinp(j2)
            x=Xout(i,j)
            y=Yout(i,j)
            cff= Finp(i1,j1)*(x2-x )*(y2-y )
     &          +Finp(i2,j1)*(x -x1)*(y2-y )
     &          +Finp(i1,j2)*(x2-x )*(y -y1)
     &          +Finp(i2,j2)*(x -x1)*(y -y1)
            Fout(i,j)=cff/((x2-x1)*(y2-y1))
          END IF
        END DO
      END DO
      RETURN
 100  continue
      print*, 'error in linterp2d'
             

      end subroutine Sub_Loop_linterp2d

