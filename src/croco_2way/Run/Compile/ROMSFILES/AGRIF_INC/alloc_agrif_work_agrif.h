#include "Param_toamr_work_agrif.h" 
      !! ALLOCATION OF VARIABLE : A1dETA
          if (.not. allocated(Agrif_Gr % tabvars(4)% array2)) then
          allocate(Agrif_Gr % tabvars(4)% array2(-1 :  Agrif_tabvars_i(1
     &97)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 10*NWEIGHT))
          Agrif_Gr % tabvars(4)% array2 = 0
      endif
      !! ALLOCATION OF VARIABLE : A1dXI
          if (.not. allocated(Agrif_Gr % tabvars(5)% array2)) then
          allocate(Agrif_Gr % tabvars(5)% array2(-1 :  Agrif_tabvars_i(2
     &00)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,1 : 10*NWEIGHT))
          Agrif_Gr % tabvars(5)% array2 = 0
      endif
