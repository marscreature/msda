      subroutine Sub_Loop_hinterp(tabres,i1,i2,j1,j2,before,nb,ndir,padd
     &_E,Mm,padd_X,Lm,Mmmpi,Lmmpi,hinv,h)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hinv
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      logical :: before
      real, dimension(i1:i2,j1:j2) :: tabres
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

!$AGRIF_DO_NOT_TREAT
       integer*4 isens
       common/interph/isens
!$AGRIF_END_DO_NOT_TREAT
                             
                      
                                
                     
      integer(4) :: i
      integer(4) :: j
                         
                                             
      logical :: western_side
      logical :: eastern_side
                                              
      logical :: northern_side
      logical :: southern_side
       if (before) then
         if (isens == 1) THEN
           do j=j1,j2
             do i=max(i1,lbound(h,1)+1),i2
               tabres(i,j) = 0.5D0*(h(i-1,j)+h(i,j))
             enddo
           enddo
         else
           do j=max(j1,lbound(h,2)+1),j2
             do i=i1,i2
               tabres(i,j) = 0.5D0*(h(i,j-1)+h(i,j))
             enddo
           enddo
         endif
       else
         western_side  = (nb == 1).AND.(ndir == 1)
         eastern_side  = (nb == 1).AND.(ndir == 2)
         southern_side = (nb == 2).AND.(ndir == 1)
         northern_side = (nb == 2).AND.(ndir == 2)
         if (isens == 1) then
           if ( western_side ) then
             do j=j1,j2
               h(0,j) = 2.D0*tabres(1,j)-h(1,j)
               hinv(0,j) = 1.D0/h(0,j)
             enddo
           endif
           if ( eastern_side ) then
             do j=j1,j2
               h(Lmmpi+1,j) = 2.D0*tabres(Lmmpi+1,j)-h(Lmmpi,j)
               hinv(Lmmpi+1,j) = 1.D0/h(Lmmpi+1,j)
             enddo
           endif
         elseif (isens == 2) then
           if ( southern_side ) then
             do i=i1,i2
               h(i,0) = 2.D0*tabres(i,1)-h(i,1)
               hinv(i,0) = 1.D0/h(i,0)
             enddo
           endif
           if ( northern_side ) then
             do i=i1,i2
               h(i,Mmmpi+1) = 2.D0*tabres(i,Mmmpi+1)-h(i,Mmmpi)
               hinv(i,Mmmpi+1) = 1.D0/h(i,Mmmpi+1)
             enddo
           endif
         endif
        endif
      return
       
      

      end subroutine Sub_Loop_hinterp

