      subroutine Sub_Loop_t3dbc_parent_tile(Istr,Iend,Jstr,Jend,indx,itr
     &c,grad,padd_E,Mm,padd_X,Lm,umask,rmask,tclm,vmask,nstp,t,tauT_out,
     &tauT_in,dt,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      real :: tauT_out
      real :: tauT_in
      real :: dt
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: indx
      integer(4) :: itrc
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                                    
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                                
                                                                       
                                                                 
      
      real :: cff
      real :: cx
      real :: cy
      real :: dft
      real :: dfx
      real :: dfy
      real :: tau
      real :: tau_in
      real :: tau_out
                            

                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      tau_in=dt*tauT_in
      tau_out=dt*tauT_out
      if (.not.WEST_INTER) then
        do k=1,N
          do j=Jstr,Jend+1
            grad(Istr-1,j)=( t(Istr-1,j  ,k,nstp,itrc)
     &                      -t(Istr-1,j-1,k,nstp,itrc))
     &                                 *vmask(Istr-1,j)
            grad(Istr  ,j)=( t(Istr  ,j  ,k,nstp,itrc)
     &                      -t(Istr  ,j-1,k,nstp,itrc))
     &                                   *vmask(Istr,j)
          enddo
          do j=Jstr,Jend
            dft=t(Istr,j,k,nstp,itrc)-t(Istr  ,j,k,indx,itrc)
            dfx=t(Istr,j,k,indx,itrc)-t(Istr+1,j,k,indx,itrc)
            if (dfx*dft .lt. 0.D0) then
              dft=0.D0
              tau=tau_in
            else
              tau=tau_out
            endif
            if (dft*(grad(Istr,j)+grad(Istr,j+1)) .gt. 0.D0) then
              dfy=grad(Istr,j)
            else
              dfy=grad(Istr,j+1)
            endif
            cff=max(dfx*dfx+dfy*dfy, eps)
            cx=dft*dfx
            cy=min(cff,max(dft*dfy,-cff))
            t(Istr-1,j,k,indx,itrc)=( cff*t(Istr-1,j,k,nstp,itrc)
     &                                   +cx*t(Istr,j,k,indx,itrc)
     &                                -max(cy,0.D0)*grad(Istr-1,j  )
     &                                -min(cy,0.D0)*grad(Istr-1,j+1)
     &                                                 )/(cff+cx)
            t(Istr-1,j,k,indx,itrc)=(1.D0-tau)*t(Istr-1,j,k,indx,itrc)
     &                                    +tau*tclm(Istr-1,j,k,itrc)
            t(Istr-1,j,k,indx,itrc)=t(Istr-1,j,k,indx,itrc)
     &                                      *rmask(Istr-1,j)
          enddo
        enddo
      endif
      if (.not.EAST_INTER) then
        do k=1,N
          do j=Jstr,Jend+1
           grad(Iend  ,j)=( t(Iend  ,j  ,k,nstp,itrc)
     &                     -t(Iend  ,j-1,k,nstp,itrc))
     &                                  *vmask(Iend,j)
           grad(Iend+1,j)=( t(Iend+1,j  ,k,nstp,itrc)
     &                     -t(Iend+1,j-1,k,nstp,itrc))
     &                                *vmask(Iend+1,j)
          enddo
          do j=Jstr,Jend
            dft=t(Iend,j,k,nstp,itrc)-t(Iend  ,j,k,indx,itrc)
            dfx=t(Iend,j,k,indx,itrc)-t(Iend-1,j,k,indx,itrc)
            if (dfx*dft .lt. 0.D0) then
              dft=0.D0
              tau=tau_in
            else
              tau=tau_out
            endif
            if (dft*(grad(Iend,j)+grad(Iend,j+1)) .gt. 0.D0) then
              dfy=grad(Iend,j)
            else
              dfy=grad(Iend,j+1)
            endif
            cff=max(dfx*dfx+dfy*dfy, eps)
            cx=dft*dfx
            cy=min(cff,max(dft*dfy,-cff))
            t(Iend+1,j,k,indx,itrc)=( cff*t(Iend+1,j,k,nstp,itrc)
     &                                   +cx*t(Iend,j,k,indx,itrc)
     &                                -max(cy,0.D0)*grad(Iend+1,j  )
     &                                -min(cy,0.D0)*grad(Iend+1,j+1)
     &                                                 )/(cff+cx)
            t(Iend+1,j,k,indx,itrc)=(1.D0-tau)*t(Iend+1,j,k,indx,itrc)
     &                                    +tau*tclm(Iend+1,j,k,itrc)
            t(Iend+1,j,k,indx,itrc)=t(Iend+1,j,k,indx,itrc)
     &                                     *rmask(Iend+1,j)
          enddo
        enddo
      endif
      if (.not.SOUTH_INTER) then
        do k=1,N
          do i=Istr,Iend+1
            grad(i,Jstr  )=( t(i  ,Jstr  ,k,nstp,itrc)
     &                      -t(i-1,Jstr  ,k,nstp,itrc))
     &                                   *umask(i,Jstr)
            grad(i,Jstr-1)=( t(i  ,Jstr-1,k,nstp,itrc)
     &                      -t(i-1,Jstr-1,k,nstp,itrc))
     &                                *umask(i,Jstr-1)
          enddo
          do i=Istr,Iend
            dft=t(i,Jstr,k,nstp,itrc)-t(i,Jstr  ,k,indx,itrc)
            dfx=t(i,Jstr,k,indx,itrc)-t(i,Jstr+1,k,indx,itrc)
            if (dfx*dft .lt. 0.D0) then
              dft=0.D0
              tau=tau_in
            else
              tau=tau_out
            endif
            if (dft*(grad(i,Jstr)+grad(i+1,Jstr)) .gt. 0.D0) then
              dfy=grad(i,Jstr)
            else
              dfy=grad(i+1,Jstr)
            endif
            cff=max(dfx*dfx+dfy*dfy, eps)
            cx=dft*dfx
            cy=min(cff,max(dft*dfy,-cff))
            t(i,Jstr-1,k,indx,itrc)=( cff*t(i,Jstr-1,k,nstp,itrc)
     &                                   +cx*t(i,Jstr,k,indx,itrc)
     &                                -max(cy,0.D0)*grad(i  ,Jstr-1)
     &                                -min(cy,0.D0)*grad(i+1,Jstr-1)
     &                                                 )/(cff+cx)
            t(i,Jstr-1,k,indx,itrc)=(1.D0-tau)*t(i,Jstr-1,k,indx,itrc)
     &                                    +tau*tclm(i,Jstr-1,k,itrc)
            t(i,Jstr-1,k,indx,itrc)=t(i,Jstr-1,k,indx,itrc)
     &                                     *rmask(i,Jstr-1)
          enddo
        enddo
      endif
      if (.not.NORTH_INTER) then
        do k=1,N
          do i=Istr,Iend+1
            grad(i,Jend  )=( t(i  ,Jend  ,k,nstp,itrc)
     &                      -t(i-1,Jend  ,k,nstp,itrc))
     &                                   *umask(i,Jend)
            grad(i,Jend+1)=( t(i  ,Jend+1,k,nstp,itrc)
     &                      -t(i-1,Jend+1,k,nstp,itrc))
     &                                 *umask(i,Jend+1)
          enddo
          do i=Istr,Iend
            dft=t(i,Jend,k,nstp,itrc)-t(i,Jend  ,k,indx,itrc)
            dfx=t(i,Jend,k,indx,itrc)-t(i,Jend-1,k,indx,itrc)
            if (dfx*dft .lt. 0.D0) then
              dft=0.D0
              tau=tau_in
            else
              tau=tau_out
            endif
            if (dft*(grad(i,Jend)+grad(i+1,Jend)) .gt. 0.D0) then
              dfy=grad(i,Jend)
            else
              dfy=grad(i+1,Jend)
            endif
            cff=max(dfx*dfx+dfy*dfy, eps)
            cx=dft*dfx
            cy=min(cff,max(dft*dfy,-cff))
            t(i,Jend+1,k,indx,itrc)=( cff*t(i,Jend+1,k,nstp,itrc)
     &                                 +cx*t(i,Jend  ,k,indx,itrc)
     &                                -max(cy,0.D0)*grad(i  ,Jend+1)
     &                                -min(cy,0.D0)*grad(i+1,Jend+1)
     &                                                 )/(cff+cx)
            t(i,Jend+1,k,indx,itrc)=(1.D0-tau)*t(i,Jend+1,k,indx,itrc)
     &                                    +tau*tclm(i,Jend+1,k,itrc)
            t(i,Jend+1,k,indx,itrc)=t(i,Jend+1,k,indx,itrc)
     &                                     *rmask(i,Jend+1)
          enddo
        enddo
      endif
      if (.not.SOUTH_INTER .and. .not.WEST_INTER) then
        do k=1,N
          t(Istr-1,Jstr-1,k,indx,itrc)=0.5D0*
     &                          ( t(Istr,Jstr-1,k,indx,itrc)
     &                           +t(Istr-1,Jstr,k,indx,itrc))
     &                                 *rmask(Istr-1,Jstr-1)
        enddo
      endif
      if (.not.SOUTH_INTER .and. .not.EAST_INTER) then
        do k=1,N
          t(Iend+1,Jstr-1,k,indx,itrc)=0.5D0*
     &                          (t(Iend,Jstr-1,k,indx,itrc)
     &                          +t(Iend+1,Jstr,k,indx,itrc))
     &                                 *rmask(Iend+1,Jstr-1)
        enddo
      endif
      if (.not.NORTH_INTER .and. .not.WEST_INTER) then
        do k=1,N
          t(Istr-1,Jend+1,k,indx,itrc)=0.5D0*
     &                          ( t(Istr,Jend+1,k,indx,itrc)
     &                           +t(Istr-1,Jend,k,indx,itrc))
     &                                 *rmask(Istr-1,Jend+1)
        enddo
      endif
      if (.not.NORTH_INTER .and. .not.EAST_INTER) then
        do k=1,N
          t(Iend+1,Jend+1,k,indx,itrc)=0.5D0*
     &                          ( t(Iend,Jend+1,k,indx,itrc)
     &                           +t(Iend+1,Jend,k,indx,itrc))
     &                                 *rmask(Iend+1,Jend+1)
        enddo
      endif
      return
       
      

      end subroutine Sub_Loop_t3dbc_parent_tile

