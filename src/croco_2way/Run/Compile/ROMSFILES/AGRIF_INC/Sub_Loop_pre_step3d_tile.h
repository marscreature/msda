      subroutine Sub_Loop_pre_step3d_tile(Istr,Iend,Jstr,Jend,ru,rv,rw,F
     &C,CF,DC,FX,FE,WORK,Hz_half,padd_E,Mm,padd_X,Lm,Zt_avg1,knew,zeta,D
     &V_avg1,om_v,DU_avg1,on_u,v,pn_v,pm_v,u,pn_u,pm_u,nnew,vmask,umask,
     &t,Mmmpi,Lmmpi,We,Hvom,Huon,pn,pm,Hz_bak,Hz,dt,ntstart,iic,nstp,NOR
     &TH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: gamma = 1.D0/6.D0
      real, parameter :: epsil = 1.D-16
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real :: dt
      integer(4) :: ntstart
      integer(4) :: iic
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: We
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hvom
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Huon
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz_bak
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:N) :: ru
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:N) :: rv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: rw
      real, dimension(Istr-2:Iend+2,0:N) :: FC
      real, dimension(Istr-2:Iend+2,0:N) :: CF
      real, dimension(Istr-2:Iend+2,0:N) :: DC
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: FX
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: FE
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: WORK
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:N) :: Hz_half

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                                       
                                                                 
                            
      integer(4) :: itrc
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: indx
      integer(4) :: imin
      integer(4) :: imax
      integer(4) :: jmin
      integer(4) :: jmax
      integer(4) :: nadv
      integer(4) :: iAkt
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                   
      real :: cff
      real :: cff1
      real :: cff2
      real :: cdt
                                                  
                                               

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                 
                                                 
                                                 
                              
                              
                              
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      indx=3-nstp
      nadv=  nstp
      if (iic.eq.ntstart) then
        cff=0.5D0*dt
        cff1=1.D0
        cff2=0.D0
      else
        cff=(1.D0-gamma)*dt
        cff1=0.5D0+gamma
        cff2=0.5D0-gamma
      endif
      do k=1,N
        do j=JstrV-1,Jend
          do i=IstrU-1,Iend
            Hz_half(i,j,k)=cff1*Hz(i,j,k)+cff2*Hz_bak(i,j,k)
     &        -cff*pm(i,j)*pn(i,j)*( Huon(i+1,j,k)-Huon(i,j,k)
     &                              +Hvom(i,j+1,k)-Hvom(i,j,k)
     &                                  +We(i,j,k)-We(i,j,k-1)
     &                                                       )
          enddo
        enddo
      enddo
      do itrc=1,NT
        do k=1,N
          if (WEST_INTER) then
            imin=Istr-1
          else
            imin=max(Istr-1,1)
          endif
          if (EAST_INTER) then
            imax=Iend+2
          else
            imax=min(Iend+2,Lmmpi+1)
          endif
          if (SOUTH_INTER) then
            jmin=Jstr-1
          else
            jmin=max(Jstr-1,1)
          endif
          if (NORTH_INTER) then
            jmax=Jend+2
          else
            jmax=min(Jend+2,Mmmpi+1)
          endif
          do j=Jstr,Jend
            do i=imin,imax
              FX(i,j)=(t(i,j,k,nadv,itrc)-t(i-1,j,k,nadv,itrc))
     &                                               *umask(i,j)
            enddo
          enddo
          if (.not.WEST_INTER) then
            do j=Jstr,Jend
              FX(0,j)=FX(1,j)
            enddo
          endif
          if (.not.EAST_INTER) then
            do j=Jstr,Jend
              FX(Lmmpi+2,j)=FX(Lmmpi+1,j)
            enddo
          endif
          do j=Jstr,Jend
            do i=Istr-1,Iend+1
              WORK(i,j)=0.5D0*(FX(i+1,j)+FX(i,j))
            enddo
          enddo
          do j=Jstr,Jend
            do i=Istr,Iend+1
              FX(i,j)=0.5D0*( t(i,j,k,nadv,itrc)+t(i-1,j,k,nadv,itrc)
     &                     -0.333333333333D0*(WORK(i,j)-WORK(i-1,j))
     &                                                )*Huon(i,j,k)
            enddo
          enddo
          do j=jmin,jmax
            do i=Istr,Iend
              FE(i,j)=(t(i,j,k,nadv,itrc)-t(i,j-1,k,nadv,itrc))
     &                                               *vmask(i,j)
            enddo
          enddo
          if (.not.SOUTH_INTER) then
            do i=Istr,Iend
              FE(i,0)=FE(i,1)
            enddo
          endif
          if (.not.NORTH_INTER) then
            do i=Istr,Iend
              FE(i,Mmmpi+2)=FE(i,Mmmpi+1)
            enddo
          endif
          do j=Jstr-1,Jend+1
            do i=Istr,Iend
              WORK(i,j)=0.5D0*(FE(i,j+1)+FE(i,j))
            enddo
          enddo
          do j=Jstr,Jend+1
            do i=Istr,Iend
              FE(i,j)=0.5D0*( t(i,j,k,nadv,itrc)+t(i,j-1,k,nadv,itrc)
     &                     -0.333333333333D0*(WORK(i,j)-WORK(i,j-1))
     &                                               )*Hvom(i,j,k)
            enddo
          enddo
          if (iic.eq.ntstart) then
            cff=0.5D0*dt
            do j=Jstr,Jend
              do i=Istr,Iend
                t(i,j,k,nnew,itrc)=Hz(i,j,k)*t(i,j,k,nstp,itrc)
     &                      -cff*pm(i,j)*pn(i,j)*( FX(i+1,j)-FX(i,j)
     &                                            +FE(i,j+1)-FE(i,j))
              enddo
            enddo
          else
            cff=(1.D0-gamma)*dt
            cff1=0.5D0+gamma
            cff2=0.5D0-gamma
            do j=Jstr,Jend
              do i=Istr,Iend
                t(i,j,k,nnew,itrc)=cff1*Hz(i,j,k)*t(i,j,k,nstp,itrc)
     &                            +cff2*Hz_bak(i,j,k)*t(i,j,k,indx,itrc)
     &                         -cff*pm(i,j)*pn(i,j)*( FX(i+1,j)-FX(i,j)
     &                                               +FE(i,j+1)-FE(i,j))
              enddo
            enddo
          endif
        enddo
      enddo
      if (iic.eq.ntstart) then
            cdt=0.5D0*dt
      else
            cdt=(1.D0-gamma)*dt
      endif
      do j=Jstr,Jend
        do i=Istr,Iend
           do k=1,N
             DC(i,k)=1.D0/Hz_half(i,j,k)
           enddo
             DC(i,0)=cdt*pn(i,j)*pm(i,j)
        enddo
        do itrc=1,NT
          do k=1,N-1
            do i=istr,iend
              FC(i,k)=t(i,j,k+1,nadv,itrc)-t(i,j,k,nadv,itrc)
            enddo
          enddo
          do i=istr,iend
            FC(i,0)=FC(i,1)
            FC(i,N)=FC(i,N-1)
          enddo
          do k=1,N
            do i=istr,iend
              cff=2.D0*FC(i,k)*FC(i,k-1)
              if (cff.gt.epsil) then
                CF(i,k)=cff/(FC(i,k)+FC(i,k-1))
              else
                CF(i,k)=0.D0
              endif
            enddo
          enddo
          do k=1,N-1
            do i=istr,iend
              FC(i,k)=0.5D0*(   t(i,j,k  ,nadv,itrc)
     &                +       t(i,j,k+1,nadv,itrc)
     &                -0.333333333333D0*(CF(i,k+1)-CF(i,k))
     &                                                  )*We(i,j,k)
            enddo
          enddo
          do i=istr,iend
            FC(i,0)=0.D0
            FC(i,N)=0.D0
            CF(i,0)=dt*pm(i,j)*pn(i,j)
          enddo
          do k=1,N
            do i=Istr,Iend
              t(i,j,k,nnew,itrc)=DC(i,k)*( t(i,j,k,nnew,itrc)
     &               -DC(i,0)*(FC(i,k)-FC(i,k-1)))
            enddo
          enddo
        enddo
        do i=IstrU,Iend
          DC(i,0)=pm_u(i,j)*pn_u(i,j)
        enddo
        if (iic.eq.ntstart) then
          do k=1,N
            do i=IstrU,Iend
              cff = 2.D0/(Hz_half(i,j,k)+Hz_half(i-1,j,k))
              u(i,j,k,nnew)=(u(i,j,k,nstp)*0.5D0*(Hz(i,j,k)+Hz(i-1,j,k))
     &                                           +cdt*DC(i,0)*ru(i,j,k)
     &                      )*cff
              u(i,j,k,indx)=u(i,j,k,nstp)*0.5D0*(Hz(i,j,k)+
     &                                         Hz(i-1,j,k))
            enddo
          enddo
        else
          cff1=0.5D0+gamma
          cff2=0.5D0-gamma
          do k=1,N
            do i=IstrU,Iend
              cff = 2.D0/(Hz_half(i,j,k)+Hz_half(i-1,j,k))
              u(i,j,k,nnew)=( cff1*u(i,j,k,nstp)*0.5D0*(Hz(i,j,k)+
     &                                              Hz(i-1,j,k))
     &                       +cff2*u(i,j,k,indx)*0.5D0*(Hz_bak(i,j,k)+
     &                                              Hz_bak(i-1,j,k))
     &                       +cdt*DC(i,0)*ru(i,j,k)
     &                                                     )*cff
              u(i,j,k,indx)=u(i,j,k,nstp)*0.5D0*(Hz(i,j,k)+
     &                                         Hz(i-1,j,k))
            enddo
          enddo
         endif
        if (j.ge.JstrV) then
          do i=Istr,Iend
            DC(i,0)=pm_v(i,j)*pn_v(i,j)
          enddo
          if (iic.eq.ntstart) then
            do k=1,N
              do i=Istr,Iend
                cff = 2.D0/(Hz_half(i,j,k)+Hz_half(i,j-1,k))
                v(i,j,k,nnew)=(v(i,j,k,nstp)*0.5D0*(Hz(i,j,k)+Hz(i,j-1,
     &                                                               k))
     &                                           +cdt*DC(i,0)*rv(i,j,k)
     &                           )*cff
                v(i,j,k,indx)=v(i,j,k,nstp)*0.5D0*(Hz(i,j  ,k)+
     &                                           Hz(i,j-1,k))
              enddo
            enddo
          else
            cff1=0.5D0+gamma
            cff2=0.5D0-gamma
            do k=1,N
              do i=Istr,Iend
                cff = 2.D0/(Hz_half(i,j,k)+Hz_half(i,j-1,k))
                v(i,j,k,nnew)=( cff1*v(i,j,k,nstp)*0.5D0*(Hz(i,j,k)+
     &                                                Hz(i,j-1,k))
     &                         +cff2*v(i,j,k,indx)*0.5D0*(Hz_bak(i,j,k)+
     &                                                Hz_bak(i,j-1,k))
     &                         +cdt*DC(i,0)*rv(i,j,k)
     &                                                           )*cff
                v(i,j,k,indx)=v(i,j,k,nstp)*0.5D0*(Hz(i,j,k)+
     &                                           Hz(i,j-1,k))
              enddo
            enddo
          endif
        endif
      enddo
      do itrc=1,NT
        call t3dbc_tile (Istr,Iend,Jstr,Jend, nnew,itrc, WORK)
        call exchange_r3d_tile (Istr,Iend,Jstr,Jend,
     &                          t(-1,-1,1,nnew,itrc))
      enddo
      call u3dbc_tile (Istr,Iend,Jstr,Jend, WORK)
      call v3dbc_tile (Istr,Iend,Jstr,Jend, WORK)
      do j=JstrR,JendR
        do i=Istr,IendR
          DC(i,0)=0.D0
          CF(i,0)=0.D0
        enddo
        do k=1,N,+1
          do i=Istr,IendR
            DC(i,k)=0.5D0*(Hz(i,j,k)+Hz(i-1,j,k))*on_u(i,j)
            DC(i,0)=DC(i,0)+DC(i,k)
            CF(i,0)=CF(i,0)+DC(i,k)*u(i,j,k,nnew)
          enddo
        enddo
        if (iic.eq.ntstart) then
          cff1=1.D0
          cff2=0.D0
        else
          cff1=1.5D0
          cff2=0.5D0
        endif
        do i=Istr,IendR
          CF(i,0)=( CF(i,0)-cff1*DU_avg1(i,j,nstp)
     &                     +cff2*DU_avg1(i,j,indx) )/DC(i,0)
        enddo
        do k=N,1,-1
          do i=Istr,IendR
            u(i,j,k,nnew)=(u(i,j,k,nnew)-CF(i,0))
     &                                 *umask(i,j)
          enddo
        enddo
      enddo
      do j=Jstr,JendR
        do i=IstrR,IendR
          DC(i,0)=0.D0
          CF(i,0)=0.D0
        enddo
        do k=1,N,+1
          do i=IstrR,IendR
            DC(i,k)=0.5D0*(Hz(i,j,k)+Hz(i,j-1,k))*om_v(i,j)
            DC(i,0)=DC(i,0)+DC(i,k)
            CF(i,0)=CF(i,0)+DC(i,k)*v(i,j,k,nnew)
          enddo
        enddo
        if (iic.eq.ntstart) then
          cff1=1.D0
          cff2=0.D0
        else
          cff1=1.5D0
          cff2=0.5D0
        endif
        do i=IstrR,IendR
          CF(i,0)=( CF(i,0)-cff1*DV_avg1(i,j,nstp)
     &                     +cff2*DV_avg1(i,j,indx) )/DC(i,0)
        enddo
        do k=N,1,-1
          do i=IstrR,IendR
            v(i,j,k,nnew)=(v(i,j,k,nnew)-CF(i,0))
     &                                  *vmask(i,j)
          enddo
        enddo
      enddo
      call exchange_u3d_tile (Istr,Iend,Jstr,Jend,
     &                        u(-1,-1,1,nnew))
      call exchange_v3d_tile (Istr,Iend,Jstr,Jend,
     &                        v(-1,-1,1,nnew))
      do j=JstrR,JendR
        do i=IstrR,IendR
          zeta(i,j,knew)=Zt_avg1(i,j)
        enddo
      enddo
      call exchange_r2d_tile (Istr,Iend,Jstr,Jend,
     &                          zeta(-1,-1,knew))
      return
       
      

      end subroutine Sub_Loop_pre_step3d_tile

