      integer(4), parameter :: N = 66
      interface
        subroutine Sub_Loop_uv3dpremix_tile(Istr,Iend,Jstr,Jend,CF,DC,Mm
     &mpi,Lmmpi,padd_E,Mm,padd_X,Lm,DV_avg1,om_v,nstp,DU_avg1,on_u,Hz,nb
     &step3d,V_sponge_east,U_sponge_east,V_sponge_west,U_sponge_west,V_s
     &ponge_north,U_sponge_north,vmask,vsponge,V_sponge_south,umask,uspo
     &nge,U_sponge_south,UVspongeTimeindex2,UVTimesponge,vspongeid,uspon
     &geid,UVspongeTimeindex,nbcoarse,NORTH_INTER,SOUTH_INTER,EAST_INTER
     &,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      integer(4) :: nbstep3d
      integer(4) :: UVspongeTimeindex2
      integer(4) :: UVTimesponge
      integer(4) :: vspongeid
      integer(4) :: uspongeid
      integer(4) :: UVspongeTimeindex
      integer(4) :: nbcoarse
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(Lmmpi-9:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:2) :: V_spong
     &e_east
      real, dimension(Lmmpi-9:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:2) :: U_spong
     &e_east
      real, dimension(0:10,-1:Mm+2+padd_E,1:N,1:2) :: V_sponge_west
      real, dimension(1:11,-1:Mm+2+padd_E,1:N,1:2) :: U_sponge_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi-9:Mmmpi+1,1:N,1:2) :: V_spong
     &e_north
      real, dimension(-1:Lm+2+padd_X,Mmmpi-9:Mmmpi+1,1:N,1:2) :: U_spong
     &e_north
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vsponge
      real, dimension(-1:Lm+2+padd_X,1:11,1:N,1:2) :: V_sponge_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: usponge
      real, dimension(-1:Lm+2+padd_X,0:10,1:N,1:2) :: U_sponge_south
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: CF
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: DC
        end subroutine Sub_Loop_uv3dpremix_tile

      end interface
