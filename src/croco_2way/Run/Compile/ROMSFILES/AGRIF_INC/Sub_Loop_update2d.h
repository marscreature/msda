      subroutine Sub_Loop_update2d(padd_E,Mm,padd_X,Lm,DV_avg1,DU_avg1,u
     &pdatedvavg2id,updateduavg2id,updatevbarid,updateubarid,updatezetai
     &d,indupdate,nfast,nbcoarse,iif,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      use Agrif_Util
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: updatedvavg2id
      integer(4) :: updateduavg2id
      integer(4) :: updatevbarid
      integer(4) :: updateubarid
      integer(4) :: updatezetaid
      integer(4) :: indupdate
      integer(4) :: nfast
      integer(4) :: nbcoarse
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
                     
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
      external updateubar,updatevbar,updatezeta
      external updateduavg2, updatedvavg2
                                                                
      real, pointer, dimension(:,:) :: ztavg1_parent
      real, pointer, dimension(:,:) :: duavg2parent
                                                                   
      real, pointer, dimension(:,:) :: parenth
      real, pointer, dimension(:,:) :: parenton_u
      real, pointer, dimension(:,:) :: parentom_v
                                                 
      real, pointer, dimension(:,:) :: dvavg2parent
                                            
      integer(4) :: i
      integer(4) :: j
      integer(4) :: ipu
      integer(4) :: jpu
      integer(4) :: ipv
      integer(4) :: jpv
      integer(4) :: ipr
      integer(4) :: jpr
                       
      real :: dtparent
                                                                   
      real, pointer, dimension(:,:) :: pmparent
      real, pointer, dimension(:,:) :: pnparent
      real, pointer, dimension(:,:) :: rmaskparent
                                                             
      real, pointer, dimension(:,:,:) :: zetaparent
      real, pointer, dimension(:,:,:) :: parentzeta
                                                                
      real, pointer, dimension(:,:,:) :: parentubar
      real, pointer, dimension(:,:,:) :: parentdu_avg1
                                                                
      real, pointer, dimension(:,:,:) :: parentdv_avg1
      real, pointer, dimension(:,:,:) :: parentvbar
                                        
      real :: t1
      real :: t2
      real :: t3
      real :: t4
      real :: t5
      real :: t6
      real, dimension(1:5) :: tind
                   
      integer(4) :: j1
                                                              
      integer(4) :: parentknew
      integer(4) :: parentiif
      integer(4) :: parentnnew
      integer(4) :: parentkstp
                                             
      integer(4) :: iter
      integer(4) :: irhox
      integer(4) :: irhoy
      integer(4) :: irhot
                  
      real :: cff
                       
      real :: invrrhot
                      
      integer(4) :: pp
                          
      integer(4) :: idecal
                                                          
      real, allocatable, dimension(:,:) :: duavg2parentold
                                                          
      real, allocatable, dimension(:,:) :: dvavg2parentold
                          
      integer(4) :: pngrid
      external :: updatermask_child
!$AGRIF_DO_NOT_TREAT
      logical global_update_2d
      common/globalupdate/global_update_2d
      integer*4 :: iiffine, irhotfine, nbgrid
      common/updateubar2val/iiffine,irhotfine,
     &   nbgrid
!$AGRIF_END_DO_NOT_TREAT
      irhot = Agrif_Irhot()
      IF ((mod(iif+(nbcoarse-1)*mod(nfast,irhot),irhot).NE.0)) RETURN
         iiffine = iif
         irhotfine = Agrif_Irhot()
         nbgrid = Agrif_Fixed()
C$OMP BARRIER
C$OMP MASTER
        irhox = Agrif_Irhox()
        irhoy = Agrif_Irhoy()
        pngrid = Agrif_Parent_Fixed()
        if (nbgrid == grids_at_level(pngrid,0)) then
           Call Agrif_Set_Parent(indupdate,0)
        endif
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      indupdate = 0
      global_update_2d = .FALSE.
       Call Agrif_Update_Variable(updatezetaid,
     &         locupdate=(/1,4/),procname = updatezeta)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      Call Agrif_Update_Variable(updateubarid,
     &     locupdate1=(/0,3/),locupdate2=(/1,4/),
     &     procname = updateubar)
      Call Agrif_Update_Variable(updatevbarid,
     &     locupdate1=(/1,4/),locupdate2=(/0,3/),
     &     procname = updatevbar)
      IF (iif == nfast) THEN
      IF (nbcoarse == irhot) THEN
      global_update_2d = .TRUE.
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      Call Agrif_Update_Variable(updatezetaid,
     &         locupdate=(/1,0/),procname = updatezeta)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      Call Agrif_Update_Variable(updateubarid,
     &     locupdate1=(/0,-1/),locupdate2=(/1,-2/),
     &     procname = updateubar)
      Call Agrif_Update_Variable(updatevbarid,
     &     locupdate1=(/1,-2/),locupdate2=(/0,-1/),
     &     procname = updatevbar)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      Call Agrif_Update_Variable(updateduavg2id,
     &     locupdate1=(/0,0/),locupdate2=(/1,1/),
     &         procname = updateduavg2)
      Call Agrif_Update_Variable(updatedvavg2id,
     &     locupdate1=(/1,1/),locupdate2=(/0,0/),
     &         procname = updatedvavg2)
      Call Agrif_ChildGrid_To_ParentGrid()
      Call ExchangeParentValues()
      Call Agrif_ParentGrid_To_ChildGrid()
      DU_avg1(:,:,4) = 0.D0
      DV_avg1(:,:,4) = 0.D0
      ENDIF
      ENDIF
C$OMP END MASTER
      return
       
      

      end subroutine Sub_Loop_update2d

