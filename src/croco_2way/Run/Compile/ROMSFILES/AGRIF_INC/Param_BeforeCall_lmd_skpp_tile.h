      integer(4), parameter :: N = 66
      interface
        subroutine Sub_Loop_lmd_skpp_tile(Istr,Iend,Jstr,Jend,Kv,Kt,Ks,m
     &y_hbl,Bo,Bosol,Gm1,dGm1dS,Gt1,dGt1dS,Gs1,dGs1dS,Bfsfc_bl,Cr,FC,wrk
     &1,wrk2,wrk3,swr_frac,my_kbl,padd_E,Mm,padd_X,Lm,ghats,bvstr,bustr,
     &kbl,pmask2,f,bvf,rmask,v,u,Hz,nstp,hbls,z_w,svstr,sustr,ustar,srfl
     &x,stflx,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      real, parameter :: Cv = 1.8D0
      real, parameter :: Ric = 0.3D0
      real, parameter :: Ri_inv = 1.D0/Ric
      real, parameter :: betaT = -0.2D0
      real, parameter :: epssfc = 0.1D0
      real, parameter :: Cstar = 10.D0
      real, parameter :: nu0c = 0.1D0
      real, parameter :: C_Ek = 258.D0
      real, parameter :: zeta_m = -0.2D0
      real, parameter :: a_m = 1.257D0
      real, parameter :: c_m = 8.360D0
      real, parameter :: zeta_s = -1.0D0
      real, parameter :: a_s = -28.86D0
      real, parameter :: c_s = 98.96D0
      real, parameter :: r2 = 0.5D0
      real, parameter :: r3 = 1.D0/3.D0
      real, parameter :: r4 = 0.25D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: ghats
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      integer(4), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: kbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: f
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: hbls
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: z_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ustar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4), dimension(Istr-2:Iend+2) :: my_kbl
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: swr_frac
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: my_hbl
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Bo
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Bosol
      real, dimension(Istr-2:Iend+2) :: Gm1
      real, dimension(Istr-2:Iend+2) :: dGm1dS
      real, dimension(Istr-2:Iend+2) :: Gt1
      real, dimension(Istr-2:Iend+2) :: dGt1dS
      real, dimension(Istr-2:Iend+2) :: Gs1
      real, dimension(Istr-2:Iend+2) :: dGs1dS
      real, dimension(Istr-2:Iend+2) :: Bfsfc_bl
      real, dimension(Istr-2:Iend+2,0:N) :: Cr
      real, dimension(Istr-2:Iend+2,0:N) :: FC
      real, dimension(Istr-2:Iend+2,0:N) :: wrk1
      real, dimension(Istr-2:Iend+2,0:N) :: wrk2
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: wrk3
        end subroutine Sub_Loop_lmd_skpp_tile

      end interface
