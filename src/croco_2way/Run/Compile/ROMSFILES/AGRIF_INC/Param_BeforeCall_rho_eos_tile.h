      integer(4), parameter :: N = 66
      interface
        subroutine Sub_Loop_rho_eos_tile(Istr,Iend,Jstr,Jend,K_up,K_dw,p
     &add_E,Mm,padd_X,Lm,rhoA,rhoS,Hz,bvf,rho,z_r,z_w,qp1,rmask,rho1,nrh
     &s,t,rho0,NORTH_INTER,Mmmpi,SOUTH_INTER,EAST_INTER,Lmmpi,WEST_INTER
     &)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: r00 = 999.842594D0
      real, parameter :: r01 = 6.793952D-2
      real, parameter :: r02 = -9.095290D-3
      real, parameter :: r03 = 1.001685D-4
      real, parameter :: r04 = -1.120083D-6
      real, parameter :: r05 = 6.536332D-9
      real, parameter :: r10 = 0.824493D0
      real, parameter :: r11 = -4.08990D-3
      real, parameter :: r12 = 7.64380D-5
      real, parameter :: r13 = -8.24670D-7
      real, parameter :: r14 = 5.38750D-9
      real, parameter :: rS0 = -5.72466D-3
      real, parameter :: rS1 = 1.02270D-4
      real, parameter :: rS2 = -1.65460D-6
      real, parameter :: r20 = 4.8314D-4
      real, parameter :: K00 = 19092.56D0
      real, parameter :: K01 = 209.8925D0
      real, parameter :: K02 = -3.041638D0
      real, parameter :: K03 = -1.852732D-3
      real, parameter :: K04 = -1.361629D-5
      real, parameter :: K10 = 104.4077D0
      real, parameter :: K11 = -6.500517D0
      real, parameter :: K12 = 0.1553190D0
      real, parameter :: K13 = 2.326469D-4
      real, parameter :: KS0 = -5.587545D0
      real, parameter :: KS1 = +0.7390729D0
      real, parameter :: KS2 = -1.909078D-2
      real, parameter :: B00 = 0.4721788D0
      real, parameter :: B01 = 0.01028859D0
      real, parameter :: B02 = -2.512549D-4
      real, parameter :: B03 = -5.939910D-7
      real, parameter :: B10 = -0.01571896D0
      real, parameter :: B11 = -2.598241D-4
      real, parameter :: B12 = 7.267926D-6
      real, parameter :: BS1 = 2.042967D-3
      real, parameter :: E00 = +1.045941D-5
      real, parameter :: E01 = -5.782165D-10
      real, parameter :: E02 = +1.296821D-7
      real, parameter :: E10 = -2.595994D-7
      real, parameter :: E11 = -1.248266D-9
      real, parameter :: E12 = -3.508914D-9
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nrhs
      real :: rho0
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhoA
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhoS
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: z_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: qp1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,0:N) :: K_up
      real, dimension(Istr-2:Iend+2,0:N) :: K_dw
        end subroutine Sub_Loop_rho_eos_tile

      end interface
