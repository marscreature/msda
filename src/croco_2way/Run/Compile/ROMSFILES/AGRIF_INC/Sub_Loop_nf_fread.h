      subroutine Sub_Loop_nf_fread(A,ncid,varid,record,type,nf_fread,Mm,
     &Lm,mynode,buff,Mmmpi,jminmpi,jj,Lmmpi,iminmpi,ii,padd_E,padd_X)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: nf_byte = 1
      integer(4), parameter :: nf_int1 = nf_byte
      integer(4), parameter :: nf_char = 2
      integer(4), parameter :: nf_short = 3
      integer(4), parameter :: nf_int2 = nf_short
      integer(4), parameter :: nf_int = 4
      integer(4), parameter :: nf_float = 5
      integer(4), parameter :: nf_real = nf_float
      integer(4), parameter :: nf_double = 6
      integer(4), parameter :: nf_fill_byte = -127
      integer(4), parameter :: nf_fill_int1 = nf_fill_byte
      integer(4), parameter :: nf_fill_char = 0
      integer(4), parameter :: nf_fill_short = -32767
      integer(4), parameter :: nf_fill_int2 = nf_fill_short
      integer(4), parameter :: nf_fill_int = -2147483647
      real, parameter :: nf_fill_float = 9.9692099683868690D+36
      real, parameter :: nf_fill_real = nf_fill_float
      real(8), parameter :: nf_fill_double = 9.9692099683868690D+36
      integer(4), parameter :: nf_nowrite = 0
      integer(4), parameter :: nf_write = 1
      integer(4), parameter :: nf_clobber = 0
      integer(4), parameter :: nf_noclobber = 4
      integer(4), parameter :: nf_fill = 0
      integer(4), parameter :: nf_nofill = 256
      integer(4), parameter :: nf_lock = 1024
      integer(4), parameter :: nf_share = 2048
      integer(4), parameter :: nf_64bit_offset = 512
      integer(4), parameter :: nf_sizehint_default = 0
      integer(4), parameter :: nf_align_chunk = -1
      integer(4), parameter :: nf_format_classic = 1
      integer(4), parameter :: nf_format_64bit = 2
      integer(4), parameter :: nf_diskless = 8
      integer(4), parameter :: nf_mmap = 16
      integer(4), parameter :: nf_unlimited = 0
      integer(4), parameter :: nf_global = 0
      integer(4), parameter :: nf_max_dims = 1024
      integer(4), parameter :: nf_max_attrs = 8192
      integer(4), parameter :: nf_max_vars = 8192
      integer(4), parameter :: nf_max_name = 256
      integer(4), parameter :: nf_max_var_dims = nf_max_dims
      integer(4), parameter :: nf_noerr = 0
      integer(4), parameter :: nf_ebadid = -33
      integer(4), parameter :: nf_eexist = -35
      integer(4), parameter :: nf_einval = -36
      integer(4), parameter :: nf_eperm = -37
      integer(4), parameter :: nf_enotindefine = -38
      integer(4), parameter :: nf_eindefine = -39
      integer(4), parameter :: nf_einvalcoords = -40
      integer(4), parameter :: nf_emaxdims = -41
      integer(4), parameter :: nf_enameinuse = -42
      integer(4), parameter :: nf_enotatt = -43
      integer(4), parameter :: nf_emaxatts = -44
      integer(4), parameter :: nf_ebadtype = -45
      integer(4), parameter :: nf_ebaddim = -46
      integer(4), parameter :: nf_eunlimpos = -47
      integer(4), parameter :: nf_emaxvars = -48
      integer(4), parameter :: nf_enotvar = -49
      integer(4), parameter :: nf_eglobal = -50
      integer(4), parameter :: nf_enotnc = -51
      integer(4), parameter :: nf_ests = -52
      integer(4), parameter :: nf_emaxname = -53
      integer(4), parameter :: nf_eunlimit = -54
      integer(4), parameter :: nf_enorecvars = -55
      integer(4), parameter :: nf_echar = -56
      integer(4), parameter :: nf_eedge = -57
      integer(4), parameter :: nf_estride = -58
      integer(4), parameter :: nf_ebadname = -59
      integer(4), parameter :: nf_erange = -60
      integer(4), parameter :: nf_enomem = -61
      integer(4), parameter :: nf_evarsize = -62
      integer(4), parameter :: nf_edimsize = -63
      integer(4), parameter :: nf_etrunc = -64
      integer(4), parameter :: nf_fatal = 1
      integer(4), parameter :: nf_verbose = 2
      integer(4), parameter :: nf_ubyte = 7
      integer(4), parameter :: nf_ushort = 8
      integer(4), parameter :: nf_uint = 9
      integer(4), parameter :: nf_int64 = 10
      integer(4), parameter :: nf_uint64 = 11
      integer(4), parameter :: nf_string = 12
      integer(4), parameter :: nf_vlen = 13
      integer(4), parameter :: nf_opaque = 14
      integer(4), parameter :: nf_enum = 15
      integer(4), parameter :: nf_compound = 16
      integer(4), parameter :: nf_fill_ubyte = 255
      integer(4), parameter :: nf_fill_ushort = 65535
      integer(4), parameter :: nf_format_netcdf4 = 3
      integer(4), parameter :: nf_format_netcdf4_classic = 4
      integer(4), parameter :: nf_netcdf4 = 4096
      integer(4), parameter :: nf_classic_model = 256
      integer(4), parameter :: nf_chunk_seq = 0
      integer(4), parameter :: nf_chunk_sub = 1
      integer(4), parameter :: nf_chunk_sizes = 2
      integer(4), parameter :: nf_endian_native = 0
      integer(4), parameter :: nf_endian_little = 1
      integer(4), parameter :: nf_endian_big = 2
      integer(4), parameter :: nf_chunked = 0
      integer(4), parameter :: nf_contiguous = 1
      integer(4), parameter :: nf_nochecksum = 0
      integer(4), parameter :: nf_fletcher32 = 1
      integer(4), parameter :: nf_noshuffle = 0
      integer(4), parameter :: nf_shuffle = 1
      integer(4), parameter :: nf_szip_ec_option_mask = 4
      integer(4), parameter :: nf_szip_nn_option_mask = 32
      integer(4), parameter :: nf_mpiio = 8192
      integer(4), parameter :: nf_mpiposix = 16384
      integer(4), parameter :: nf_pnetcdf = 32768
      integer(4), parameter :: nf_independent = 0
      integer(4), parameter :: nf_collective = 1
      integer(4), parameter :: nf_ehdferr = -101
      integer(4), parameter :: nf_ecantread = -102
      integer(4), parameter :: nf_ecantwrite = -103
      integer(4), parameter :: nf_ecantcreate = -104
      integer(4), parameter :: nf_efilemeta = -105
      integer(4), parameter :: nf_edimmeta = -106
      integer(4), parameter :: nf_eattmeta = -107
      integer(4), parameter :: nf_evarmeta = -108
      integer(4), parameter :: nf_enocompound = -109
      integer(4), parameter :: nf_eattexists = -110
      integer(4), parameter :: nf_enotnc4 = -111
      integer(4), parameter :: nf_estrictnc3 = -112
      integer(4), parameter :: nf_enotnc3 = -113
      integer(4), parameter :: nf_enopar = -114
      integer(4), parameter :: nf_eparinit = -115
      integer(4), parameter :: nf_ebadgrpid = -116
      integer(4), parameter :: nf_ebadtypid = -117
      integer(4), parameter :: nf_etypdefined = -118
      integer(4), parameter :: nf_ebadfield = -119
      integer(4), parameter :: nf_ebadclass = -120
      integer(4), parameter :: nf_emaptype = -121
      integer(4), parameter :: nf_elatefill = -122
      integer(4), parameter :: nf_elatedef = -123
      integer(4), parameter :: nf_edimscale = -124
      integer(4), parameter :: nf_enogrp = -125
      integer(4), parameter :: ncbyte = 1
      integer(4), parameter :: ncchar = 2
      integer(4), parameter :: ncshort = 3
      integer(4), parameter :: nclong = 4
      integer(4), parameter :: ncfloat = 5
      integer(4), parameter :: ncdouble = 6
      integer(4), parameter :: ncrdwr = 1
      integer(4), parameter :: nccreat = 2
      integer(4), parameter :: ncexcl = 4
      integer(4), parameter :: ncindef = 8
      integer(4), parameter :: ncnsync = 16
      integer(4), parameter :: nchsync = 32
      integer(4), parameter :: ncndirty = 64
      integer(4), parameter :: nchdirty = 128
      integer(4), parameter :: ncfill = 0
      integer(4), parameter :: ncnofill = 256
      integer(4), parameter :: nclink = 32768
      integer(4), parameter :: ncnowrit = 0
      integer(4), parameter :: ncwrite = ncrdwr
      integer(4), parameter :: ncclob = nf_clobber
      integer(4), parameter :: ncnoclob = nf_noclobber
      integer(4), parameter :: ncunlim = 0
      integer(4), parameter :: ncglobal = 0
      integer(4), parameter :: maxncop = 64
      integer(4), parameter :: maxncdim = 1024
      integer(4), parameter :: maxncatt = 8192
      integer(4), parameter :: maxncvar = 8192
      integer(4), parameter :: maxncnam = 256
      integer(4), parameter :: maxvdims = maxncdim
      integer(4), parameter :: ncnoerr = nf_noerr
      integer(4), parameter :: ncebadid = nf_ebadid
      integer(4), parameter :: ncenfile = -31
      integer(4), parameter :: nceexist = nf_eexist
      integer(4), parameter :: nceinval = nf_einval
      integer(4), parameter :: nceperm = nf_eperm
      integer(4), parameter :: ncenotin = nf_enotindefine
      integer(4), parameter :: nceindef = nf_eindefine
      integer(4), parameter :: ncecoord = nf_einvalcoords
      integer(4), parameter :: ncemaxds = nf_emaxdims
      integer(4), parameter :: ncename = nf_enameinuse
      integer(4), parameter :: ncenoatt = nf_enotatt
      integer(4), parameter :: ncemaxat = nf_emaxatts
      integer(4), parameter :: ncebadty = nf_ebadtype
      integer(4), parameter :: ncebadd = nf_ebaddim
      integer(4), parameter :: nceunlim = nf_eunlimpos
      integer(4), parameter :: ncemaxvs = nf_emaxvars
      integer(4), parameter :: ncenotvr = nf_enotvar
      integer(4), parameter :: nceglob = nf_eglobal
      integer(4), parameter :: ncenotnc = nf_enotnc
      integer(4), parameter :: ncests = nf_ests
      integer(4), parameter :: ncentool = nf_emaxname
      integer(4), parameter :: ncfoobar = 32
      integer(4), parameter :: ncsyserr = -31
      integer(4), parameter :: ncfatal = 1
      integer(4), parameter :: ncverbos = 2
      integer(4), parameter :: filbyte = -127
      integer(4), parameter :: filchar = 0
      integer(4), parameter :: filshort = -32767
      integer(4), parameter :: fillong = -2147483647
      real, parameter :: filfloat = 9.9692099683868690D+36
      real(8), parameter :: fildoub = 9.9692099683868690D+36
      integer(4) :: Mm
      integer(4) :: Lm
      integer(4) :: mynode
      integer(4) :: Mmmpi
      integer(4) :: jminmpi
      integer(4) :: jj
      integer(4) :: Lmmpi
      integer(4) :: iminmpi
      integer(4) :: ii
      integer(4) :: padd_E
      integer(4) :: padd_X
      real, dimension(1:(Lm+5)*(Mm+5)*(N+1)) :: buff
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N+1) :: A
      integer(4) :: ncid
      integer(4) :: type
      integer(4) :: varid
      integer(4) :: record
      integer :: nf_fread
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                                       
                                                                 
                                                                 
                          
      integer(4) :: vert_type
      integer(4) :: imin
      integer(4) :: imax
      integer(4), dimension(1:4) :: start
      integer(4) :: horiz_type
      integer(4) :: jmin
      integer(4) :: jmax
      integer(4), dimension(1:4) :: count
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: shift
      integer(4) :: ierr
                                     
                        
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                        
                        
                        
                         
                        
                       
                         
                        
                          
                             

                                   

                             

                              

                                    

                            

                              

                                    

                               

                                       
                                       
                                       
                                        
                                       
                                      
                                      
                                     
                                       
                                     

                                             

                                  

                                        

                                              

                                           

                                                        

                                              

                                                         

                           
                         
                           
                             
                        
                          
                        
                         
                                
                                    
                               
                                  
                                
                            
                        
                                

                              

                                

                                  

                             

                                 

                                

                                 

                                       

                                         

                                     

                                       

                                     

                                 

                              

                             
                                  

                          
                               

                            
                             
                            
                            
                                
                                    

                                     

                                    

                                   

                                               

                         
                          
                          
                          
                         
                                
                             
                                
                            
                              
                           
                            
                            
                           
                             
                            
                           
                           
                          
                        
                            
                            
                              
                         
                         
                           
                            
                          
                          
                            
                            
                          
                              

                                 

                                 

                                 

                                

                                       

                                    

                                       

                                   

                                     

                                  

                                   

                                   

                                  

                                    

                                   

                                  

                                  

                                 

                               

                                   

                                   

                                     

                                

                                

                                  

                                   

                                 

                                 

                                   

                                   

                                 

                          
                           
                              

                                

                                    
      character(80) :: nf_inq_libvers
      external       nf_inq_libvers
                                 
      character(80) :: nf_strerror
      external       nf_strerror
                                 
      logical :: nf_issyserr
      external       nf_issyserr
                                       
      integer(4) :: nf_inq_base_pe
      external        nf_inq_base_pe
                                       
      integer(4) :: nf_set_base_pe
      external        nf_set_base_pe
                                  
      integer(4) :: nf_create
      external        nf_create
                                   
      integer(4) :: nf__create
      external        nf__create
                                      
      integer(4) :: nf__create_mp
      external        nf__create_mp
                                
      integer(4) :: nf_open
      external        nf_open
                                 
      integer(4) :: nf__open
      external        nf__open
                                    
      integer(4) :: nf__open_mp
      external        nf__open_mp
                                    
      integer(4) :: nf_set_fill
      external        nf_set_fill
                                              
      integer(4) :: nf_set_default_format
      external        nf_set_default_format
                                 
      integer(4) :: nf_redef
      external        nf_redef
                                  
      integer(4) :: nf_enddef
      external        nf_enddef
                                   
      integer(4) :: nf__enddef
      external        nf__enddef
                                
      integer(4) :: nf_sync
      external        nf_sync
                                 
      integer(4) :: nf_abort
      external        nf_abort
                                 
      integer(4) :: nf_close
      external        nf_close
                                  
      integer(4) :: nf_delete
      external        nf_delete
                               
      integer(4) :: nf_inq
      external        nf_inq
                            
      integer(4) :: nf_inq_path
      external nf_inq_path
                                     
      integer(4) :: nf_inq_ndims
      external        nf_inq_ndims
                                     
      integer(4) :: nf_inq_nvars
      external        nf_inq_nvars
                                     
      integer(4) :: nf_inq_natts
      external        nf_inq_natts
                                        
      integer(4) :: nf_inq_unlimdim
      external        nf_inq_unlimdim
                                      
      integer(4) :: nf_inq_format
      external        nf_inq_format
                                   
      integer(4) :: nf_def_dim
      external        nf_def_dim
                                     
      integer(4) :: nf_inq_dimid
      external        nf_inq_dimid
                                   
      integer(4) :: nf_inq_dim
      external        nf_inq_dim
                                       
      integer(4) :: nf_inq_dimname
      external        nf_inq_dimname
                                      
      integer(4) :: nf_inq_dimlen
      external        nf_inq_dimlen
                                      
      integer(4) :: nf_rename_dim
      external        nf_rename_dim
                                   
      integer(4) :: nf_inq_att
      external        nf_inq_att
                                     
      integer(4) :: nf_inq_attid
      external        nf_inq_attid
                                       
      integer(4) :: nf_inq_atttype
      external        nf_inq_atttype
                                      
      integer(4) :: nf_inq_attlen
      external        nf_inq_attlen
                                       
      integer(4) :: nf_inq_attname
      external        nf_inq_attname
                                    
      integer(4) :: nf_copy_att
      external        nf_copy_att
                                      
      integer(4) :: nf_rename_att
      external        nf_rename_att
                                   
      integer(4) :: nf_del_att
      external        nf_del_att
                                        
      integer(4) :: nf_put_att_text
      external        nf_put_att_text
                                        
      integer(4) :: nf_get_att_text
      external        nf_get_att_text
                                        
      integer(4) :: nf_put_att_int1
      external        nf_put_att_int1
                                        
      integer(4) :: nf_get_att_int1
      external        nf_get_att_int1
                                        
      integer(4) :: nf_put_att_int2
      external        nf_put_att_int2
                                        
      integer(4) :: nf_get_att_int2
      external        nf_get_att_int2
                                       
      integer(4) :: nf_put_att_int
      external        nf_put_att_int
                                       
      integer(4) :: nf_get_att_int
      external        nf_get_att_int
                                        
      integer(4) :: nf_put_att_real
      external        nf_put_att_real
                                        
      integer(4) :: nf_get_att_real
      external        nf_get_att_real
                                          
      integer(4) :: nf_put_att_double
      external        nf_put_att_double
                                          
      integer(4) :: nf_get_att_double
      external        nf_get_att_double
                                   
      integer(4) :: nf_def_var
      external        nf_def_var
                                   
      integer(4) :: nf_inq_var
      external        nf_inq_var
                                     
      integer(4) :: nf_inq_varid
      external        nf_inq_varid
                                       
      integer(4) :: nf_inq_varname
      external        nf_inq_varname
                                       
      integer(4) :: nf_inq_vartype
      external        nf_inq_vartype
                                        
      integer(4) :: nf_inq_varndims
      external        nf_inq_varndims
                                        
      integer(4) :: nf_inq_vardimid
      external        nf_inq_vardimid
                                        
      integer(4) :: nf_inq_varnatts
      external        nf_inq_varnatts
                                      
      integer(4) :: nf_rename_var
      external        nf_rename_var
                                    
      integer(4) :: nf_copy_var
      external        nf_copy_var
                                        
      integer(4) :: nf_put_var_text
      external        nf_put_var_text
                                        
      integer(4) :: nf_get_var_text
      external        nf_get_var_text
                                        
      integer(4) :: nf_put_var_int1
      external        nf_put_var_int1
                                        
      integer(4) :: nf_get_var_int1
      external        nf_get_var_int1
                                        
      integer(4) :: nf_put_var_int2
      external        nf_put_var_int2
                                        
      integer(4) :: nf_get_var_int2
      external        nf_get_var_int2
                                       
      integer(4) :: nf_put_var_int
      external        nf_put_var_int
                                       
      integer(4) :: nf_get_var_int
      external        nf_get_var_int
                                        
      integer(4) :: nf_put_var_real
      external        nf_put_var_real
                                        
      integer(4) :: nf_get_var_real
      external        nf_get_var_real
                                          
      integer(4) :: nf_put_var_double
      external        nf_put_var_double
                                          
      integer(4) :: nf_get_var_double
      external        nf_get_var_double
                                         
      integer(4) :: nf_put_var1_text
      external        nf_put_var1_text
                                         
      integer(4) :: nf_get_var1_text
      external        nf_get_var1_text
                                         
      integer(4) :: nf_put_var1_int1
      external        nf_put_var1_int1
                                         
      integer(4) :: nf_get_var1_int1
      external        nf_get_var1_int1
                                         
      integer(4) :: nf_put_var1_int2
      external        nf_put_var1_int2
                                         
      integer(4) :: nf_get_var1_int2
      external        nf_get_var1_int2
                                        
      integer(4) :: nf_put_var1_int
      external        nf_put_var1_int
                                        
      integer(4) :: nf_get_var1_int
      external        nf_get_var1_int
                                         
      integer(4) :: nf_put_var1_real
      external        nf_put_var1_real
                                         
      integer(4) :: nf_get_var1_real
      external        nf_get_var1_real
                                           
      integer(4) :: nf_put_var1_double
      external        nf_put_var1_double
                                           
      integer(4) :: nf_get_var1_double
      external        nf_get_var1_double
                                         
      integer(4) :: nf_put_vara_text
      external        nf_put_vara_text
                                         
      integer(4) :: nf_get_vara_text
      external        nf_get_vara_text
                                         
      integer(4) :: nf_put_vara_int1
      external        nf_put_vara_int1
                                         
      integer(4) :: nf_get_vara_int1
      external        nf_get_vara_int1
                                         
      integer(4) :: nf_put_vara_int2
      external        nf_put_vara_int2
                                         
      integer(4) :: nf_get_vara_int2
      external        nf_get_vara_int2
                                        
      integer(4) :: nf_put_vara_int
      external        nf_put_vara_int
                                        
      integer(4) :: nf_get_vara_int
      external        nf_get_vara_int
                                         
      integer(4) :: nf_put_vara_real
      external        nf_put_vara_real
                                         
      integer(4) :: nf_get_vara_real
      external        nf_get_vara_real
                                           
      integer(4) :: nf_put_vara_double
      external        nf_put_vara_double
                                           
      integer(4) :: nf_get_vara_double
      external        nf_get_vara_double
                                         
      integer(4) :: nf_put_vars_text
      external        nf_put_vars_text
                                         
      integer(4) :: nf_get_vars_text
      external        nf_get_vars_text
                                         
      integer(4) :: nf_put_vars_int1
      external        nf_put_vars_int1
                                         
      integer(4) :: nf_get_vars_int1
      external        nf_get_vars_int1
                                         
      integer(4) :: nf_put_vars_int2
      external        nf_put_vars_int2
                                         
      integer(4) :: nf_get_vars_int2
      external        nf_get_vars_int2
                                        
      integer(4) :: nf_put_vars_int
      external        nf_put_vars_int
                                        
      integer(4) :: nf_get_vars_int
      external        nf_get_vars_int
                                         
      integer(4) :: nf_put_vars_real
      external        nf_put_vars_real
                                         
      integer(4) :: nf_get_vars_real
      external        nf_get_vars_real
                                           
      integer(4) :: nf_put_vars_double
      external        nf_put_vars_double
                                           
      integer(4) :: nf_get_vars_double
      external        nf_get_vars_double
                                         
      integer(4) :: nf_put_varm_text
      external        nf_put_varm_text
                                         
      integer(4) :: nf_get_varm_text
      external        nf_get_varm_text
                                         
      integer(4) :: nf_put_varm_int1
      external        nf_put_varm_int1
                                         
      integer(4) :: nf_get_varm_int1
      external        nf_get_varm_int1
                                         
      integer(4) :: nf_put_varm_int2
      external        nf_put_varm_int2
                                         
      integer(4) :: nf_get_varm_int2
      external        nf_get_varm_int2
                                        
      integer(4) :: nf_put_varm_int
      external        nf_put_varm_int
                                        
      integer(4) :: nf_get_varm_int
      external        nf_get_varm_int
                                         
      integer(4) :: nf_put_varm_real
      external        nf_put_varm_real
                                         
      integer(4) :: nf_get_varm_real
      external        nf_get_varm_real
                                           
      integer(4) :: nf_put_varm_double
      external        nf_put_varm_double
                                           
      integer(4) :: nf_get_varm_double
      external        nf_get_varm_double
                         
                          
                        
                         
                          
                          
                        
                          
                        
                            
                              

                               

                             

                               

                                

                                

                              

                                

                              

                                  

                                        
                                         
                                     

                                        

                                  
                                       

                                          
                                               

                           
                                   

                                 
                                        

                             
                                  

                             
                                  

                               
                                    

                                 
                                      

                                 
                                      

                              
                                   

                           
                                

                              
                                   

                              
                                   

                              
                                   

                             
                                  

                           
                                

                                       
                                            

                                       
                                             

                         
                                 

                            
                                     

                           
                                    

                               
                                    

                              
                                   

                           
                                   

                             
                                     

                              
                                      

                               
                                       

                             
                                     

                            
                                    

                            
                                    

                            
                                    

                               
                                       

                              
                                      

                           
                                   

                              
                                      

                           
                                   

                          
                                  

                            
                                    

                             
                                     

                             
                                     

                               
                                       

                             
                                     

                             
                                     

                            
                                    

                             
                                     

                            
                                    

                             
                                     

                          
                                  

                              
      integer(4) :: nf_create_par
      external nf_create_par
                            
      integer(4) :: nf_open_par
      external nf_open_par
                                  
      integer(4) :: nf_var_par_access
      external nf_var_par_access
                            
      integer(4) :: nf_inq_ncid
      external nf_inq_ncid
                            
      integer(4) :: nf_inq_grps
      external nf_inq_grps
                               
      integer(4) :: nf_inq_grpname
      external nf_inq_grpname
                                    
      integer(4) :: nf_inq_grpname_full
      external nf_inq_grpname_full
                                   
      integer(4) :: nf_inq_grpname_len
      external nf_inq_grpname_len
                                  
      integer(4) :: nf_inq_grp_parent
      external nf_inq_grp_parent
                                
      integer(4) :: nf_inq_grp_ncid
      external nf_inq_grp_ncid
                                     
      integer(4) :: nf_inq_grp_full_ncid
      external nf_inq_grp_full_ncid
                              
      integer(4) :: nf_inq_varids
      external nf_inq_varids
                              
      integer(4) :: nf_inq_dimids
      external nf_inq_dimids
                           
      integer(4) :: nf_def_grp
      external nf_def_grp
                              
      integer(4) :: nf_rename_grp
      external nf_rename_grp
                                   
      integer(4) :: nf_def_var_deflate
      external nf_def_var_deflate
                                   
      integer(4) :: nf_inq_var_deflate
      external nf_inq_var_deflate
                                      
      integer(4) :: nf_def_var_fletcher32
      external nf_def_var_fletcher32
                                      
      integer(4) :: nf_inq_var_fletcher32
      external nf_inq_var_fletcher32
                                    
      integer(4) :: nf_def_var_chunking
      external nf_def_var_chunking
                                    
      integer(4) :: nf_inq_var_chunking
      external nf_inq_var_chunking
                                
      integer(4) :: nf_def_var_fill
      external nf_def_var_fill
                                
      integer(4) :: nf_inq_var_fill
      external nf_inq_var_fill
                                  
      integer(4) :: nf_def_var_endian
      external nf_def_var_endian
                                  
      integer(4) :: nf_inq_var_endian
      external nf_inq_var_endian
                               
      integer(4) :: nf_inq_typeids
      external nf_inq_typeids
                              
      integer(4) :: nf_inq_typeid
      external nf_inq_typeid
                            
      integer(4) :: nf_inq_type
      external nf_inq_type
                                 
      integer(4) :: nf_inq_user_type
      external nf_inq_user_type
                                
      integer(4) :: nf_def_compound
      external nf_def_compound
                                   
      integer(4) :: nf_insert_compound
      external nf_insert_compound
                                         
      integer(4) :: nf_insert_array_compound
      external nf_insert_array_compound
                                
      integer(4) :: nf_inq_compound
      external nf_inq_compound
                                     
      integer(4) :: nf_inq_compound_name
      external nf_inq_compound_name
                                     
      integer(4) :: nf_inq_compound_size
      external nf_inq_compound_size
                                        
      integer(4) :: nf_inq_compound_nfields
      external nf_inq_compound_nfields
                                      
      integer(4) :: nf_inq_compound_field
      external nf_inq_compound_field
                                          
      integer(4) :: nf_inq_compound_fieldname
      external nf_inq_compound_fieldname
                                           
      integer(4) :: nf_inq_compound_fieldindex
      external nf_inq_compound_fieldindex
                                            
      integer(4) :: nf_inq_compound_fieldoffset
      external nf_inq_compound_fieldoffset
                                          
      integer(4) :: nf_inq_compound_fieldtype
      external nf_inq_compound_fieldtype
                                           
      integer(4) :: nf_inq_compound_fieldndims
      external nf_inq_compound_fieldndims
                                               
      integer(4) :: nf_inq_compound_fielddim_sizes
      external nf_inq_compound_fielddim_sizes
                            
      integer(4) :: nf_def_vlen
      external nf_def_vlen
                            
      integer(4) :: nf_inq_vlen
      external nf_inq_vlen
                             
      integer(4) :: nf_free_vlen
      external nf_free_vlen
                            
      integer(4) :: nf_def_enum
      external nf_def_enum
                               
      integer(4) :: nf_insert_enum
      external nf_insert_enum
                            
      integer(4) :: nf_inq_enum
      external nf_inq_enum
                                   
      integer(4) :: nf_inq_enum_member
      external nf_inq_enum_member
                                  
      integer(4) :: nf_inq_enum_ident
      external nf_inq_enum_ident
                              
      integer(4) :: nf_def_opaque
      external nf_def_opaque
                              
      integer(4) :: nf_inq_opaque
      external nf_inq_opaque
                           
      integer(4) :: nf_put_att
      external nf_put_att
                           
      integer(4) :: nf_get_att
      external nf_get_att
                           
      integer(4) :: nf_put_var
      external nf_put_var
                            
      integer(4) :: nf_put_var1
      external nf_put_var1
                            
      integer(4) :: nf_put_vara
      external nf_put_vara
                            
      integer(4) :: nf_put_vars
      external nf_put_vars
                           
      integer(4) :: nf_get_var
      external nf_get_var
                            
      integer(4) :: nf_get_var1
      external nf_get_var1
                            
      integer(4) :: nf_get_vara
      external nf_get_vara
                            
      integer(4) :: nf_get_vars
      external nf_get_vars
                                  
      integer(4) :: nf_put_var1_int64
      external nf_put_var1_int64
                                  
      integer(4) :: nf_put_vara_int64
      external nf_put_vara_int64
                                  
      integer(4) :: nf_put_vars_int64
      external nf_put_vars_int64
                                  
      integer(4) :: nf_put_varm_int64
      external nf_put_varm_int64
                                 
      integer(4) :: nf_put_var_int64
      external nf_put_var_int64
                                  
      integer(4) :: nf_get_var1_int64
      external nf_get_var1_int64
                                  
      integer(4) :: nf_get_vara_int64
      external nf_get_vara_int64
                                  
      integer(4) :: nf_get_vars_int64
      external nf_get_vars_int64
                                  
      integer(4) :: nf_get_varm_int64
      external nf_get_varm_int64
                                 
      integer(4) :: nf_get_var_int64
      external nf_get_var_int64
                                    
      integer(4) :: nf_get_vlen_element
      external nf_get_vlen_element
                                    
      integer(4) :: nf_put_vlen_element
      external nf_put_vlen_element
                                   
      integer(4) :: nf_set_chunk_cache
      external nf_set_chunk_cache
                                   
      integer(4) :: nf_get_chunk_cache
      external nf_get_chunk_cache
                                       
      integer(4) :: nf_set_var_chunk_cache
      external nf_set_var_chunk_cache
                                       
      integer(4) :: nf_get_var_chunk_cache
      external nf_get_var_chunk_cache
                      
      integer(4) :: nccre
                      
      integer(4) :: ncopn
                       
      integer(4) :: ncddef
                      
      integer(4) :: ncdid
                       
      integer(4) :: ncvdef
                      
      integer(4) :: ncvid
                       
      integer(4) :: nctlen
                       
      integer(4) :: ncsfil
      external nccre
      external ncopn
      external ncddef
      external ncdid
      external ncvdef
      external ncvid
      external nctlen
      external ncsfil
                       
                        
                       
                        
                        
                        
                         
                         
                       
                         
                        
                       
                         
                         
                       
                         
                        
                         
                         
                         
                         
                         
                        
                         
                         
                         
                         
                        
                         
                         
                         
                         
                        
                         
                         
                         
                        
                       
                         
                         
                         
                        
                         
                         
                         
                        
                         
                         
                       
                       
                        
                       
                        
                         
                           

                           

                            

                           

                            

                             

                           

                            

                           

                            

                             

                             

                              

                               

                           

                               

                               

                             

                                 

                                    

                                        

                        
                            

                              

                             

                                

                                

                                

                               

                                    

                                   

                                     

                               

                                     

                                     

                                   

                                            

                                        

                                           

                                       

                                        

                                      

                                       

                                       

                                     

                                        

                                       

                                      

                                     

                                     

                                 

                                        

                              

                               

                            

                             

                        
                        
                         
                        
                    
                              
                                

                             

                                   

                                       

                                                   

                                                  

      vert_type=type/4
      horiz_type=type-4*vert_type
      jmin=horiz_type/2
      imin=horiz_type-2*jmin
      start(1)=1
      start(2)=1
      if (ii.gt.0) then
        start(1)=1-imin+iminmpi
        imin=1
      endif
      if (ii.eq.NP_XI-1) then
        imax=Lmmpi+1
      else
        imax=Lmmpi
      endif
      if (jj.gt.0) then
        start(2)=1-jmin+jminmpi
        jmin=1
      endif
      if (jj.eq.NP_ETA-1) then
        jmax=Mmmpi+1
      else
        jmax=Mmmpi
      endif
      count(1)=imax-imin+1
      count(2)=jmax-jmin+1
      if (vert_type.eq.0) then
        count(3)=1
        start(3)=record
      elseif (vert_type.eq.1) then
        count(3)=N
        count(4)=1
        start(3)=1
        start(4)=record
      elseif (vert_type.eq.2) then
        count(3)=N+1
        count(4)=1
        start(3)=1
        start(4)=record
      else
        write(*,'(/1x,2A,I3/)') 'NF_FREAD ERROR: ',
     &                    'illegal grid type', type
        nf_fread=nf_noerr+1
        return
      endif
      ierr=nf_get_vara_double (ncid, varid, start, count, buff(1))
      nf_fread=ierr
      if (ierr .ne. nf_noerr) then
        write(*,'(/1x,2A,I5,1x,A,I4/)') 'NF_FREAD ERROR: ',
     &             'nf_get_vara netCDF error code =', ierr
     &              ,' mynode =', mynode
        return
      endif
      do k=1,count(3)
        do j=jmin,jmax
          shift=1-imin+count(1)*(j-jmin+(k-1)*count(2))
          do i=imin,imax
            A(i,j,k)=buff(i+shift)
          enddo
        enddo
      enddo
      if (horiz_type.eq.0 .and. vert_type.eq.0) then
        call exchange_r2d_tile (1,Lmmpi,1,Mmmpi, A)
      elseif (horiz_type.eq.1 .and. vert_type.eq.0) then
        call exchange_u2d_tile (1,Lmmpi,1,Mmmpi, A)
      elseif (horiz_type.eq.2 .and. vert_type.eq.0) then
        call exchange_v2d_tile (1,Lmmpi,1,Mmmpi, A)
      elseif (horiz_type.eq.3 .and. vert_type.eq.0) then
        call exchange_p2d_tile (1,Lmmpi,1,Mmmpi, A)
      elseif (horiz_type.eq.0 .and. vert_type.eq.1) then
        call exchange_r3d_tile (1,Lmmpi,1,Mmmpi, A)
      elseif (horiz_type.eq.1 .and. vert_type.eq.1) then
        call exchange_u3d_tile (1,Lmmpi,1,Mmmpi, A)
      elseif (horiz_type.eq.2 .and. vert_type.eq.1) then
        call exchange_v3d_tile (1,Lmmpi,1,Mmmpi, A)
      elseif (horiz_type.eq.3 .and. vert_type.eq.1) then
        call exchange_p3d_tile (1,Lmmpi,1,Mmmpi, A)
      elseif (horiz_type.eq.0 .and. vert_type.eq.2) then
        call exchange_w3d_tile (1,Lmmpi,1,Mmmpi, A)
      endif
      return
       
      

      end subroutine Sub_Loop_nf_fread

