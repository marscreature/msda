      subroutine Sub_Loop_lmd_skpp_tile(Istr,Iend,Jstr,Jend,Kv,Kt,Ks,my_
     &hbl,Bo,Bosol,Gm1,dGm1dS,Gt1,dGt1dS,Gs1,dGs1dS,Bfsfc_bl,Cr,FC,wrk1,
     &wrk2,wrk3,swr_frac,my_kbl,padd_E,Mm,padd_X,Lm,ghats,bvstr,bustr,kb
     &l,pmask2,f,bvf,rmask,v,u,Hz,nstp,hbls,z_w,svstr,sustr,ustar,srflx,
     &stflx,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      real, parameter :: Cv = 1.8D0
      real, parameter :: Ric = 0.3D0
      real, parameter :: Ri_inv = 1.D0/Ric
      real, parameter :: betaT = -0.2D0
      real, parameter :: epssfc = 0.1D0
      real, parameter :: Cstar = 10.D0
      real, parameter :: nu0c = 0.1D0
      real, parameter :: C_Ek = 258.D0
      real, parameter :: zeta_m = -0.2D0
      real, parameter :: a_m = 1.257D0
      real, parameter :: c_m = 8.360D0
      real, parameter :: zeta_s = -1.0D0
      real, parameter :: a_s = -28.86D0
      real, parameter :: c_s = 98.96D0
      real, parameter :: r2 = 0.5D0
      real, parameter :: r3 = 1.D0/3.D0
      real, parameter :: r4 = 0.25D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: ghats
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      integer(4), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: kbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: f
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: hbls
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: z_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ustar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4), dimension(Istr-2:Iend+2) :: my_kbl
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: swr_frac
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: my_hbl
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Bo
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Bosol
      real, dimension(Istr-2:Iend+2) :: Gm1
      real, dimension(Istr-2:Iend+2) :: dGm1dS
      real, dimension(Istr-2:Iend+2) :: Gt1
      real, dimension(Istr-2:Iend+2) :: dGt1dS
      real, dimension(Istr-2:Iend+2) :: Gs1
      real, dimension(Istr-2:Iend+2) :: dGs1dS
      real, dimension(Istr-2:Iend+2) :: Bfsfc_bl
      real, dimension(Istr-2:Iend+2,0:N) :: Cr
      real, dimension(Istr-2:Iend+2,0:N) :: FC
      real, dimension(Istr-2:Iend+2,0:N) :: wrk1
      real, dimension(Istr-2:Iend+2,0:N) :: wrk2
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: wrk3

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                 
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: khbl
                                    
      integer(4) :: imin
      integer(4) :: imax
      integer(4) :: jmin
      integer(4) :: jmax
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                          
               
                            

                                                   
      real :: ustar3
      real :: Bfsfc
      real :: zscale
      real :: zetahat
      real :: ws
      real :: wm
      real :: z_bl
                                                      
      real :: Av_bl
      real :: dAv_bl
      real :: f1
      real :: At_bl
      real :: dAt_bl
      real :: As_bl
      real :: dAs_bl
                                                            
      real :: Kern
      real :: Vtc
      real :: Vtsq
      real :: sigma
      real :: cff
      real :: cff1
      real :: cff_up
      real :: cff_dn
                                                      
                    
      real :: a1
      real :: a2
      real :: a3
                                                                       
                                                                 
 
      real :: Cg
                  
      real :: ustarb
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                            

      if (.not.WEST_INTER) then
        imin=Istr
      else
        imin=Istr-1
      endif
      if (.not.EAST_INTER) then
        imax=Iend
      else
        imax=Iend+1
      endif
      if (.not.SOUTH_INTER) then
        jmin=Jstr
      else
        jmin=Jstr-1
      endif
      if (.not.NORTH_INTER) then
        jmax=Jend
      else
        jmax=Jend+1
      endif
      Cg=Cstar*vonKar*(c_s*vonKar*epssfc)**r3
      Vtc= Cv * sqrt(-betaT/(c_s*epssfc)) / (Ric*vonKar**2)
      call alfabeta_tile (Istr,Iend,Jstr,Jend, Bosol,Bo)
      do j=jmin,jmax
        do i=imin,imax
          Bo(i,j)=g*( Bosol(i,j)*(stflx(i,j,itemp)-srflx(i,j))
     &                              -Bo(i,j)*stflx(i,j,isalt)
     &                                                        )
          Bosol(i,j)=g*Bosol(i,j)*srflx(i,j)
          ustar(i,j)=sqrt(sqrt( (0.5D0*(sustr(i,j)+sustr(i+1,j)))**2
     &                         +(0.5D0*(svstr(i,j)+svstr(i,j+1)))**2))
        enddo
      enddo
      do k=0,N-1
        do j=jmin,jmax
          do i=imin,imax
            wrk3(i,j)=z_w(i,j,k)-z_w(i,j,N)
          enddo
        enddo
        call lmd_swfrac_ER_tile (Istr,Iend,Jstr,Jend,1.D0,wrk3,my_hbl)
        do j=jmin,jmax
          do i=imin,imax
            swr_frac(i,j,k)=my_hbl(i,j)
          enddo
        enddo
      enddo
      do j=jmin,jmax
        do i=imin,imax
          swr_frac(i,j,N)=1.D0
        enddo
      enddo
      do j=jmin,jmax
        do i=imin,imax
          my_hbl(i,j)  = hbls(i,j,nstp)
          my_kbl(i)    = 0
          FC(i,N)      = 0.D0
          Cr(i,N)      = 0.D0
          Cr(i,0)      = 0.D0
        enddo
        do k=1,N-1
         do i=imin,imax
          cff=1.D0/(Hz(i,j,k)+Hz(i,j,k+1))
          wrk1(i,k)=cff*( u(i,j,k+1,nstp)+u(i+1,j,k+1,nstp)
     &                 -u(i,j,k  ,nstp)-u(i+1,j,k  ,nstp))
          wrk2(i,k)=cff*( v(i,j,k+1,nstp)+v(i,j+1,k+1,nstp)
     &                 -v(i,j,k  ,nstp)-v(i,j+1,k  ,nstp))
         enddo
        enddo
        do i=imin,imax
          wrk1(i,N)=wrk1(i,N-1)
          wrk2(i,N)=wrk2(i,N-1)
          wrk1(i,0)=wrk1(i,  1)
          wrk2(i,0)=wrk2(i,  1)
        enddo
        do k=N,1,-1
          do i=imin,imax
            zscale = z_w(i,j,N)-z_w(i,j,k-1)
            Kern   = zscale/(zscale+epssfc*my_hbl(i,j))
            Bfsfc  = Bo(i,j) +Bosol(i,j)*(1.D0-swr_frac(i,j,k-1))
          if (Bfsfc .lt. 0.D0) zscale=min(zscale,
     &                                  my_hbl(i,j)*epssfc)
          zscale=zscale*rmask(i,j)
          zetahat=vonKar*zscale*Bfsfc
          ustar3=ustar(i,j)**3
          if (zetahat .ge. 0.D0) then
            ws=vonKar*ustar(i,j)*ustar3/max(ustar3+5.D0*zetahat,
     &                                                 1.D-20)
          elseif (zetahat .gt. zeta_s*ustar3) then
            ws=vonKar*( (ustar3-16.D0*zetahat)/ustar(i,j) )**r2
          else
            ws=vonKar*(a_s*ustar3-c_s*zetahat)**r3
          endif
            cff=bvf(i,j,k)*bvf(i,j,k-1)
            if (cff.gt.0.D0) then
              cff=cff/(bvf(i,j,k)+bvf(i,j,k-1))
            else
              cff=0.D0
            endif
            FC(i,k-1)=FC(i,k) + Kern*Hz(i,j,k)*
     &       ( 0.375D0*( wrk1(i,k)**2 + wrk1(i,k-1)**2
     &               + wrk2(i,k)**2 + wrk2(i,k-1)**2 )
     &       + 0.25D0 *( wrk1(i,k-1)*wrk1(i,k)
     &               + wrk2(i,k-1)*wrk2(i,k) )
     &       - Ri_inv*( cff +
     &              0.25D0*(bvf(i,j,k)+bvf(i,j,k-1)) )
     &       - C_Ek*f(i,j)*f(i,j) )
            Vtsq=Vtc*ws*sqrt(max(0.D0, bvf(i,j,k-1)))
            Cr(i,k-1)=FC(i,k-1) +Vtsq
            if (my_kbl(i).eq.0 .and. Cr(i,k-1).lt.0.D0)
     &                          my_kbl(i)=k
          enddo
        enddo
        do i=imin,imax
          if (my_kbl(i).gt.0) then
            k=my_kbl(i)
            my_hbl(i,j)=z_w(i,j,N)-( z_w(i,j,k-1)*Cr(i,k)
     &                              -z_w(i,j,k)*Cr(i,k-1)
     &                              )/(Cr(i,k)-Cr(i,k-1))
          else
            my_hbl(i,j)=z_w(i,j,N)-z_w(i,j,0)
          endif
        enddo
      enddo
      if (.not.WEST_INTER) then
        do j=jmin,jmax
          my_hbl(Istr-1,j)=my_hbl(Istr,j)
        enddo
      endif
      if (.not.EAST_INTER) then
        do j=jmin,jmax
          my_hbl(Iend+1,j)=my_hbl(Iend,j)
        enddo
      endif
      if (.not.SOUTH_INTER) then
        do i=imin,imax
          my_hbl(i,Jstr-1)=my_hbl(i,Jstr)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=imin,imax
          my_hbl(i,Jend+1)=my_hbl(i,Jend)
        enddo
      endif
      if (.not.WEST_INTER.and..not.SOUTH_INTER) then
        my_hbl(Istr-1,Jstr-1)=my_hbl(Istr,Jstr)
      endif
      if (.not.WEST_INTER.and..not.NORTH_INTER) then
        my_hbl(Istr-1,Jend+1)=my_hbl(Istr,Jend)
      endif
      if (.not.EAST_INTER.and..not.SOUTH_INTER) then
        my_hbl(Iend+1,Jstr-1)=my_hbl(Iend,Jstr)
      endif
      if (.not.EAST_INTER.and..not.NORTH_INTER) then
        my_hbl(Iend+1,Jend+1)=my_hbl(Iend,Jend)
      endif
      do j=Jstr,Jend+1
        do i=Istr,Iend+1
          wrk3(i,j)=0.25D0*(my_hbl(i,j)  +my_hbl(i-1,j)
     &                  +my_hbl(i,j-1)+my_hbl(i-1,j-1))
     &                   *pmask2(i,j)
        enddo
      enddo
      do j=Jstr,Jend
        do i=Istr,Iend
          cff=0.25D0*(pmask2(i,j)   +pmask2(i+1,j)
     &             +pmask2(i,j+1) +pmask2(i+1,j+1))
          my_hbl(i,j)=(1.D0-cff)*my_hbl(i,j)+
     &              0.25D0*(wrk3(i,j)  +wrk3(i+1,j)
     &                   +wrk3(i,j+1)+wrk3(i+1,j+1))
          my_hbl(i,j)=my_hbl(i,j)*rmask(i,j)
        enddo
      enddo
      do j=Jstr,Jend
        do i=istr,iend
          kbl(i,j)=N
        enddo
        do k=N-1,1,-1
          do i=istr,iend
            my_hbl(i,j)=min(my_hbl(i,j),z_w(i,j,N)-z_w(i,j,0))
     &                                             *rmask(i,j)
            if (z_w(i,j,k) .gt. z_w(i,j,N)-my_hbl(i,j)) kbl(i,j)=k
          enddo
        enddo
        do i=istr,iend
          k=kbl(i,j)
          z_bl=z_w(i,j,N)-my_hbl(i,j)
          zscale=my_hbl(i,j)
          if (swr_frac(i,j,k-1).gt. 0.D0) then
            Bfsfc=Bo(i,j) +Bosol(i,j)*( 1.D0 -swr_frac(i,j,k-1)
     &              *swr_frac(i,j,k)*(z_w(i,j,k)-z_w(i,j,k-1))
     &               /( swr_frac(i,j,k  )*(z_w(i,j,k)   -z_bl)
     &                 +swr_frac(i,j,k-1)*(z_bl -z_w(i,j,k-1))
     &                                                      ))
          else
            Bfsfc=Bo(i,j)+Bosol(i,j)
          endif
            if (Bfsfc.lt.0.D0) zscale=min(zscale,
     &                       my_hbl(i,j)*epssfc)
            zscale=zscale*rmask(i,j)
            zetahat=vonKar*zscale*Bfsfc
            ustar3=ustar(i,j)**3
            if (zetahat.ge.0.D0) then
              wm=vonKar*ustar(i,j)*ustar3/max( ustar3+5.D0*zetahat,
     &                                                    1.D-20)
              ws=wm
            else
              if (zetahat .gt. zeta_m*ustar3) then
                wm=vonKar*( ustar(i,j)*(ustar3-16.D0*zetahat) )**r4
              else
                wm=vonKar*(a_m*ustar3-c_m*zetahat)**r3
              endif
              if (zetahat .gt. zeta_s*ustar3) then
                ws=vonKar*( (ustar3-16.D0*zetahat)/ustar(i,j) )**r2
              else
                ws=vonKar*(a_s*ustar3-c_s*zetahat)**r3
              endif
            endif
          f1=5.0D0 * max(0.D0, Bfsfc) * vonKar/(ustar(i,j)**4+eps)
          cff=1.D0/(z_w(i,j,k)-z_w(i,j,k-1))
          cff_up=cff*(z_bl -z_w(i,j,k-1))
          cff_dn=cff*(z_w(i,j,k)   -z_bl)
         if(k.eq.1) then
            ustarb=SQRT(SQRT((0.5D0*(bustr(i,j)+bustr(i+1,j)))**2+
     &                       (0.5D0*(bvstr(i,j)+bvstr(i,j+1)))**2))
            dAv_bl=vonKar*ustarb
            Av_bl=dAv_bl*(z_bl-z_w(i,j,0))
            dAt_bl=vonKar*ustarb
            At_bl=dAt_bl*(z_bl-z_w(i,j,0))
            dAs_bl=vonKar*ustarb
            As_bl=dAs_bl*(z_bl-z_w(i,j,0))
          else
            Av_bl=cff_up*Kv(i,j,k)+cff_dn*Kv(i,j,k-1)
            dAv_bl=cff * (Kv(i,j,k)  -   Kv(i,j,k-1))
            At_bl=cff_up*Kt(i,j,k)+cff_dn*Kt(i,j,k-1)
            dAt_bl=cff * (Kt(i,j,k)  -   Kt(i,j,k-1))
            As_bl=cff_up*Ks(i,j,k)+cff_dn*Ks(i,j,k-1)
            dAs_bl=cff * (Ks(i,j,k)  -   Ks(i,j,k-1))
          endif
          Gm1(i)=Av_bl/(my_hbl(i,j)*wm+eps)
          dGm1dS(i)=min(0.D0, Av_bl*f1-dAv_bl/(wm+eps))
          Gt1(i)=At_bl/(my_hbl(i,j)*ws+eps)
          dGt1dS(i)=min(0.D0, At_bl*f1-dAt_bl/(ws+eps))
          Gs1(i)=As_bl/(my_hbl(i,j)*ws+eps)
          dGs1dS(i)=min(0.D0, As_bl*f1-dAs_bl/(ws+eps))
          Bfsfc_bl(i)=Bfsfc
        enddo
        do i=istr,iend
          khbl=kbl(i,j)
          do k=N-1,khbl,-1
            Bfsfc=Bfsfc_bl(i)
            zscale=z_w(i,j,N)-z_w(i,j,k)
            if (Bfsfc.lt.0.D0) zscale=min(zscale,
     &                       my_hbl(i,j)*epssfc)
            zscale=zscale*rmask(i,j)
            zetahat=vonKar*zscale*Bfsfc
            ustar3=ustar(i,j)**3
            if (zetahat.ge.0.D0) then
              wm=vonKar*ustar(i,j)*ustar3/max( ustar3+5.D0*zetahat,
     &                                                    1.D-20)
              ws=wm
            else
              if (zetahat .gt. zeta_m*ustar3) then
                wm=vonKar*( ustar(i,j)*(ustar3-16.D0*zetahat) )**r4
              else
                wm=vonKar*(a_m*ustar3-c_m*zetahat)**r3
              endif
              if (zetahat .gt. zeta_s*ustar3) then
                ws=vonKar*( (ustar3-16.D0*zetahat)/ustar(i,j) )**r2
              else
                ws=vonKar*(a_s*ustar3-c_s*zetahat)**r3
              endif
            endif
            sigma=(z_w(i,j,N)-z_w(i,j,k))/max(my_hbl(i,j),eps)
            a1=sigma-2.D0
            a2=3.D0-2.D0*sigma
            a3=sigma-1.D0
            if (sigma.lt.0.07D0) then
              cff=0.5D0*(sigma-0.07D0)**2/0.07D0
            else
              cff=0.D0
            endif
            Kv(i,j,k)=wm*my_hbl(i,j)*( cff + sigma*( 1.D0+sigma*(
     &               a1+a2*Gm1(i)+a3*dGm1dS(i) )))
            Kt(i,j,k)=ws*my_hbl(i,j)*( cff + sigma*( 1.D0+sigma*(
     &               a1+a2*Gt1(i)+a3*dGt1dS(i) )))
            Ks(i,j,k)=ws*my_hbl(i,j)*( cff + sigma*( 1.D0+sigma*(
     &               a1+a2*Gs1(i)+a3*dGs1dS(i) )))
            if (Bfsfc .lt. 0.D0) then
              ghats(i,j,k)=Cg * sigma*(1.D0-sigma)**2
            else
              ghats(i,j,k)=0.D0
            endif
          enddo
          do k=khbl-1,1,-1
            ghats(i,j,k)=0.D0
          enddo
        enddo
      enddo
      do j=jstr,jend
        do i=istr,iend
          hbls(i,j,3-nstp)=my_hbl(i,j)
        enddo
      enddo
      if (.not.WEST_INTER) then
        do j=jstr,jend
          hbls(istr-1,j,3-nstp)=hbls(istr,j,3-nstp)
          kbl (istr-1,j)       =kbl(istr,j)
        enddo
      endif
      if (.not.EAST_INTER) then
        do j=jstr,jend
          hbls(iend+1,j,3-nstp)=hbls(iend,j,3-nstp)
          kbl(iend+1,j)=kbl(iend,j)
        enddo
      endif
      if (.not.SOUTH_INTER) then
        do i=istr,iend
          hbls(i,jstr-1,3-nstp)=hbls(i,jstr,3-nstp)
          kbl(i,jstr-1)=kbl(i,jstr)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=istr,iend
          hbls(i,jend+1,3-nstp)=hbls(i,jend,3-nstp)
          kbl(i,jend+1)=kbl(i,jend)
        enddo
      endif
      if (.not.WEST_INTER .and. .not.SOUTH_INTER) then
        hbls(istr-1,jstr-1,3-nstp)=hbls(istr,jstr,3-nstp)
        kbl(istr-1,jstr-1)=kbl(istr,jstr)
      endif
      if (.not.WEST_INTER .and. .not.NORTH_INTER) then
        hbls(istr-1,jend+1,3-nstp)=hbls(istr,jend,3-nstp)
        kbl(istr-1,jend+1)=kbl(istr,jend)
      endif
      if (.not.EAST_INTER .and. .not.SOUTH_INTER) then
        hbls(iend+1,jstr-1,3-nstp)=hbls(iend,jstr,3-nstp)
        kbl(iend+1,jstr-1)=kbl(iend,jstr)
      endif
      if (.not.EAST_INTER .and. .not.NORTH_INTER) then
        hbls(iend+1,jend+1,3-nstp)=hbls(iend,jend,3-nstp)
        kbl(iend+1,jend+1)=kbl(iend,jend)
      endif
      call exchange_r2d_tile (istr,iend,jstr,jend,
     &                     hbls(-1,-1,3-nstp))
      return
       
      

      end subroutine Sub_Loop_lmd_skpp_tile

