      subroutine Sub_Loop_Agrif_update_uv_np1(updatevid,updateuid,nbcoar
     &se,Mmmpi,Lmmpi,padd_E,Mm,padd_X,Lm)

      use Agrif_Types, only : Agrif_tabvars

      use Agrif_Util
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      integer(4) :: updatevid
      integer(4) :: updateuid
      integer(4) :: nbcoarse
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
                     
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                            
      integer(4) :: NNO
      integer(4) :: NNOX
      integer(4) :: NNOY
      integer(4) :: ich
      integer(4) :: jch
      integer(4) :: k
      integer(4) :: itrc
      integer(4) :: ipr
      integer(4) :: jpr
      integer(4) :: nnewpr
                                                                
      integer(4) :: irmin
      integer(4) :: irmax
      integer(4) :: jrmin
      integer(4) :: jrmax
      integer(4) :: iumin
      integer(4) :: iumax
      integer(4) :: jvmin
      integer(4) :: jvmax
                                
      integer(4) :: iint
      integer(4) :: jint
                             
      real :: cff
      real :: fxc
      real :: surfc
                            

                                                       
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tabtemp
      External UpdateTranp1, Updateunp1, Updatevnp1
                                                                       
                                                                 
 
      real, pointer, dimension(:,:,:,:) :: myfxparent
      real, pointer, dimension(:,:,:,:) :: myfyparent
      real, pointer, dimension(:,:,:,:) :: uparent
                                                                       
                                                                 
                                                 
      real, allocatable, dimension(:,:,:,:) :: myfxoldparent
      real, allocatable, dimension(:,:,:,:) :: myfyoldparent
                                                  
      real, pointer, dimension(:,:,:,:) :: vparent
                                                                   
      real, pointer, dimension(:,:) :: pmparent
      real, pointer, dimension(:,:) :: pnparent
      real, pointer, dimension(:,:) :: rmaskparent
                                                
      real, pointer, dimension(:,:,:) :: Hzparent
                                                  
      real, pointer, dimension(:,:,:,:,:) :: tparent
                                
      integer(4) :: ipu
      integer(4) :: jpu
      integer(4) :: ipv
      integer(4) :: jpv
                                                  
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: HTEMP
                    
      integer(4) :: j
      integer(4) :: i
                                  
      real :: t1
      real :: t2
      real :: t3
      real :: t4
      real :: t5
      real :: t6
      real :: t7
                    
      real :: dtparent
                                      
      integer(4) :: nnewparent
      integer(4) :: parentnnew
                   
      real, dimension(1:5) :: tind
                   
      integer(4) :: j1
                      
      integer(4) :: irhot
                       
      integer(4) :: idecal
      external :: updatemyfx, updatemyfy
      external :: updateunp1pre
      external :: updatevnp1pre
!$AGRIF_DO_NOT_TREAT
      integer*4 :: nbcoarsechild
      common/updateprestepint/nbcoarsechild
!$AGRIF_END_DO_NOT_TREAT
      nbcoarsechild = nbcoarse
      Call Agrif_ChildGrid_To_ParentGrid()
      Call ResetAlready()
      Call Agrif_ParentGrid_To_ChildGrid()
      Agrif_UseSpecialValueInUpdate = .FALSE.
      Agrif_SpecialValueFineGrid = 0.D0
      Agrif_UseSpecialValueInUpdate = .FALSE.
      IF (nbcoarse .NE. Agrif_Irhot()) return
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      irhot=Agrif_Irhot()
      IF (.TRUE.) THEN
      Agrif_UseSpecialValueInUpdate = .FALSE.
      Call Agrif_Update_Variable(updateuid,
     &     locupdate1=(/0,-1/),locupdate2=(/1,-2/),
     &     procname = updateunp1)
      Call Agrif_Update_Variable(updatevid,
     &     locupdate1=(/1,-2/),locupdate2=(/0,-1/),
     &     procname = updatevnp1)
      ELSE
      Agrif_UseSpecialValueInUpdate = .FALSE.
      Call Agrif_Update_Variable(updateuid,
     &     locupdate1=(/0,2/),locupdate2=(/1,3/),
     &     procname = updateunp1)
      Call Agrif_Update_Variable(updatevid,
     &     locupdate1=(/1,3/),locupdate2=(/0,2/),
     &     procname = updatevnp1)
      ENDIF
      return
       
      

      end subroutine Sub_Loop_Agrif_update_uv_np1

