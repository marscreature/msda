      interface
        subroutine Sub_Loop_step2D_FB_tile(Istr,Iend,Jstr,Jend,zeta_new,
     &Dnew,rubar,rvbar,Drhs,UFx,UFe,VFx,VFe,urhs,vrhs,DUon,DVom,padd_E,M
     &m,padd_X,Lm,may_day_flag,jminmpi,iminmpi,dt,nfast,vbclm,ubclm,M2nu
     &dgcof,vmask,umask,rvfrc_bak,rvfrc,nstp,rufrc_bak,rufrc,ntstart,iic
     &,on_v,om_u,rdrg,rdrg2,dmde,dndx,fomn,pmask,DV_avg2,DU_avg2,DV_avg1
     &,nnew,DU_avg1,Zt_avg1,weight,dv_avg3,du_avg3,Zt_avg3,knew,rhoA,rho
     &S,rmask,ssh,Znudgcof,pn,pm,dtfast,om_v,vbar,on_u,ubar,h,zeta,kstp,
     &iif,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: may_day_flag
      integer(4) :: jminmpi
      integer(4) :: iminmpi
      real :: dt
      integer(4) :: nfast
      integer(4) :: nstp
      integer(4) :: ntstart
      integer(4) :: iic
      real :: rdrg
      real :: rdrg2
      integer(4) :: nnew
      integer(4) :: knew
      real :: dtfast
      integer(4) :: kstp
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: M2nudgcof
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: rvfrc_bak
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rvfrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: rufrc_bak
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rufrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dmde
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dndx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: fomn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DV_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DU_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      real, dimension(1:6,0:NWEIGHT) :: weight
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: dv_avg
     &3
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: du_avg
     &3
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: Zt_avg
     &3
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhoA
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhoS
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ssh
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Znudgcof
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: zeta_new
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Dnew
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: rubar
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: rvbar
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: Drhs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: urhs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: vrhs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: DUon
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: DVom
        end subroutine Sub_Loop_step2D_FB_tile

      end interface
