      subroutine Sub_Loop_lmd_vmix_tile(Istr,Iend,Jstr,Jend,Kv,Kt,Ks,Rig
     &,padd_E,Mm,padd_X,Lm,rmask,pmask2,bvf,v,nstp,u,z_r,NORTH_INTER,SOU
     &TH_INTER,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-14
      real, parameter :: lmd_Ri0 = 0.7D0
      real, parameter :: lmd_nuwm = 1.0D-4
      real, parameter :: lmd_nuws = 0.1D-4
      real, parameter :: lmd_nu0m = 50.D-4
      real, parameter :: lmd_nu0s = 50.D-4
      real, parameter :: lmd_nu0c = 0.1D0
      real, parameter :: lmd_nu = 1.5D-6
      real, parameter :: lmd_Rrho0 = 1.9D0
      real, parameter :: lmd_nuf = 10.0D-4
      real, parameter :: lmd_fdd = 0.7D0
      real, parameter :: lmd_tdd1 = 0.909D0
      real, parameter :: lmd_tdd2 = 4.6D0
      real, parameter :: lmd_tdd3 = 0.54D0
      real, parameter :: lmd_sdd1 = 0.15D0
      real, parameter :: lmd_sdd2 = 1.85D0
      real, parameter :: lmd_sdd3 = 0.85D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Rig
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                                                       
                                                                 
                                                                 
                                                           
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                   
      real :: lmd_iwm
      real :: lmd_iws
      real :: nu_sx
      real :: nu_sxc
      real :: cff
                            

                                                                       
                                                                 
                                                                 
                                                                 
      
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                              

                                       
      real :: Ri
      real :: ratio
      real :: dudz
      real :: dvdz
      real :: shear2
                          
      integer(4) :: imin
      integer(4) :: imax
                          
      integer(4) :: jmin
      integer(4) :: jmax
      if (.not.WEST_INTER) then
        imin=Istr
      else
        imin=Istr-1
      endif
      if (.not.EAST_INTER) then
        imax=Iend
      else
        imax=Iend+1
      endif
      if (.not.SOUTH_INTER) then
        jmin=Jstr
      else
        jmin=Jstr-1
      endif
      if (.not.NORTH_INTER) then
        jmax=Jend
      else
        jmax=Jend+1
      endif
      do k=1,N-1
        do j=jmin,jmax
          do i=imin,imax
            cff=0.5D0/(z_r(i,j,k+1)-z_r(i,j,k))
            dudz=cff*(u(i  ,j,k+1,nstp)-u(i  ,j,k,nstp)+
     &                u(i+1,j,k+1,nstp)-u(i+1,j,k,nstp))
            dvdz=cff*(v(i,j  ,k+1,nstp)-v(i,j  ,k,nstp)+
     &                v(i,j+1,k+1,nstp)-v(i,j+1,k,nstp))
            shear2=dudz*dudz+dvdz*dvdz
            Rig(i,j,k)=bvf(i,j,k)/max(shear2, 1.D-10)
          enddo
        enddo
        if (.not.WEST_INTER) then
          do j=jmin,jmax
            Rig(Istr-1,j,k)=Rig(Istr,j,k)
          enddo
        endif
        if (.not.EAST_INTER) then
          do j=jmin,jmax
            Rig(Iend+1,j,k)=Rig(Iend,j,k)
          enddo
        endif
        if (.not.SOUTH_INTER) then
          do i=imin,imax
            Rig(i,Jstr-1,k)=Rig(i,Jstr,k)
          enddo
        endif
        if (.not.NORTH_INTER) then
          do i=imin,imax
            Rig(i,Jend+1,k)=Rig(i,Jend,k)
          enddo
        endif
        if (.not.WEST_INTER.and..not.SOUTH_INTER) then
          Rig(Istr-1,Jstr-1,k)=Rig(Istr,Jstr,k)
        endif
        if (.not.WEST_INTER.and..not.NORTH_INTER) then
          Rig(Istr-1,Jend+1,k)=Rig(Istr,Jend,k)
        endif
        if (.not.EAST_INTER.and..not.SOUTH_INTER) then
          Rig(Iend+1,Jstr-1,k)=Rig(Iend,Jstr,k)
        endif
        if (.not.EAST_INTER.and..not.NORTH_INTER) then
          Rig(Iend+1,Jend+1,k)=Rig(Iend,Jend,k)
        endif
      do j=Jstr,Jend+1
        do i=Istr,Iend+1
          Rig(i,j,0)=0.25D0*(Rig(i,j  ,k)+Rig(i-1,j  ,k)
     &                    +Rig(i,j-1,k)+Rig(i-1,j-1,k))
     &                                      *pmask2(i,j)
        enddo
      enddo
      do j=Jstr,Jend
        do i=Istr,Iend
          cff=0.25D0*(pmask2(i,j)   +pmask2(i+1,j)
     &             +pmask2(i,j+1) +pmask2(i+1,j+1))
          Rig(i,j,k)=(1-cff)*Rig(i,j,k)+
     &                0.25D0*(Rig(i,j  ,0)+Rig(i+1,j  ,0)
     &                     +Rig(i,j+1,0)+Rig(i+1,j+1,0))
          Rig(i,j,k)=Rig(i,j,k)*rmask(i,j)
        enddo
      enddo
      enddo
      do k=N-2,2,-1
        do j=Jstr,Jend
          do i=Istr,Iend
            Rig(i,j,k)=0.25D0*Rig(i,j,k-1)+
     &                 0.50D0*Rig(i,j,k  )+
     &                 0.25D0*Rig(i,j,k+1)
          enddo
        enddo
      enddo
      do k=1,N-1
        do j=Jstr,Jend
          do i=Istr,Iend
            Ri=max(0.D0,Rig(i,j,k))
            ratio=min(1.D0,Ri/lmd_Ri0)
            nu_sx=1.D0-ratio*ratio
            nu_sx=nu_sx*nu_sx*nu_sx
           lmd_iwm=lmd_nuwm
           lmd_iws=lmd_nuws
            nu_sxc=0.D0
            Kv(i,j,k)=lmd_iwm+lmd_nu0m*nu_sx+lmd_nu0c*nu_sxc
            Kt(i,j,k)=lmd_iws+lmd_nu0s*nu_sx+lmd_nu0c*nu_sxc
            Ks(i,j,k)=Kt(i,j,k)
          enddo
        enddo
      enddo
      do j=Jstr,Jend
        do i=Istr,Iend
          Kv(i,j,N)=Kv(i,j,N-1)
          Ks(i,j,N)=Ks(i,j,N-1)
          Kt(i,j,N)=Kt(i,j,N-1)
          Kv(i,j,0)=Kv(i,j,  1)
          Ks(i,j,0)=Ks(i,j,  1)
          Kt(i,j,0)=Kt(i,j,  1)
        enddo
      enddo
      return
       
      

      end subroutine Sub_Loop_lmd_vmix_tile

