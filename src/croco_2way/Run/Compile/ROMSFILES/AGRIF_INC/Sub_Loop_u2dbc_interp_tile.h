      subroutine Sub_Loop_u2dbc_interp_tile(Istr,Iend,Jstr,Jend,DU_west4
     &,DU_east4,DU_west6,DU_east6,DU_south4,DU_north4,DU_south6,DU_north
     &6,padd_E,Mm,padd_X,Lm,on_u,h,knew,zeta,SSH,om_u,ubclm,dtfast,umask
     &,nfast,DU_avg3,weight2,U2DTimeindex2,ubarid,common_index,dUinterp,
     &U2DTimeindex,iif,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,Mmm
     &pi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util

      include 'mpif.h'
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      real :: dtfast
      integer(4) :: nfast
      integer(4) :: U2DTimeindex2
      integer(4) :: ubarid
      integer(4) :: common_index
      integer(4) :: U2DTimeindex
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: SSH
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: DU_avg
     &3
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dUinterp
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(-1:Mm+2+padd_E) :: DU_west6
      real, dimension(-1:Mm+2+padd_E) :: DU_east6
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DU_west4
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DU_east4
      real, dimension(-1:Lm+2+padd_X) :: DU_south6
      real, dimension(-1:Lm+2+padd_X) :: DU_north6
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DU_south4
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DU_north4

                     
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                         
      integer(4) :: i
      integer(4) :: j
                                                                     
      real :: t1
      real :: t2
      real :: t3
      real :: t7
      real :: t8
      real :: t9
      real :: t10
      real :: t11
      real :: t12
      real :: t13
      real :: t14
      real :: t15
      real :: t16
      real :: t17
      real :: t18
      real :: t19
                       
      logical :: ptinterp
                                                 
      real :: t4
      real :: t5
      real :: t6
      real :: tfin
      real :: c1
      real :: c2
      real :: c3
      real :: cff1
      real :: cff2
      real :: cff3
      external :: u2dinterp
                                       
      integer(4) :: irhox
      integer(4) :: irhoy
      integer(4) :: irhot
                                  
      real :: rrhox
      real :: rrhoy
      real :: rrhot
                                   
      real :: tinterp
      real :: onemtinterp
                        
      integer(4) :: iter
                                        
      integer(4) :: ipu
      integer(4) :: jpu
      integer(4) :: parentnnew
                                
      integer(4) :: parentnbstep
                         
      real :: cffx
      real :: cffy
                                                        
      real, dimension(Jstr-2:Jend+2) :: DU_west
      real, dimension(Jstr-2:Jend+2) :: DU_east
                                                          
                                                                     
                                                         
      real, dimension(Istr-2:Iend+2) :: DU_south
      real, dimension(Istr-2:Iend+2) :: DU_north
                                                            
                                                                      
                      
      real :: lastcff
!$AGRIF_DO_NOT_TREAT
      integer*4 :: nbgrid,indinterp
      common/interp2d/indinterp,nbgrid
!$AGRIF_END_DO_NOT_TREAT
                        
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      irhox=Agrif_Irhox()
      irhoy=Agrif_Irhoy()
      irhot=Agrif_Irhot()
      rrhox = real(irhox)
      rrhoy = real(irhoy)
      rrhot = real(irhot)
      parentnbstep=
     & Agrif_Parent(iif)
      if (U2DTimeindex .NE. parentnbstep) then
C$OMP BARRIER
C$OMP MASTER
        dUinterp = 0.D0
        tinterp=1.D0
        Agrif_UseSpecialValue = .true.
        Agrif_SpecialValue = 0.D0
        nbgrid = Agrif_Fixed()
        common_index = 1
        Call Agrif_Bc_variable(ubarid,
     &    calledweight=tinterp,procname = u2dinterp)
        Agrif_UseSpecialvalue=.false.
C$OMP END MASTER
C$OMP BARRIER
        U2DTimeindex = parentnbstep
        U2DTimeindex2 = agrif_nb_step()
      endif
       if (agrif_nb_step() .EQ. U2DTimeindex2) then
          lastcff=0.D0
          do iter=iif,iif+irhot-1
          lastcff=lastcff+((iter+1-iif)/rrhot)*
     &        weight2(iif+irhot-1,iter)
          enddo
          lastcff=1.D0/lastcff
      if (.not.WEST_INTER) then
          i = Istr
          do j=Jstr,Jend
          DU_west4(j,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DU_west6(Jstr:Jend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=Jstr,Jend
             DU_west6(j) = DU_west6(j) +cff1*DU_west4(j,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=Jstr,Jend
          DU_west6(j)=DU_west6(j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DU_west4(j,iif-1)
          enddo
          enddo
          do j=Jstr,Jend
          DU_west6(j)=lastcff*((dUinterp(i,j)/rrhoy)-DU_west6(j))
     &                               * umask(i,j)
          enddo
          do j=Jstr,Jend
          DU_west6(j)=(DU_west6(j)-DU_west4(j,iif-1))/rrhot
          enddo
          endif
      endif
      if (.not.EAST_INTER) then
          i=Iend+1
          do j=Jstr,Jend
          DU_east4(j,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DU_east6(Jstr:Jend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=Jstr,Jend
             DU_east6(j) = DU_east6(j) +cff1*DU_east4(j,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=Jstr,Jend
          DU_east6(j)=DU_east6(j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DU_east4(j,iif-1)
          enddo
          enddo
          do j=Jstr,Jend
          DU_east6(j)=lastcff*((dUinterp(i,j)/rrhoy)-DU_east6(j))
     &                               * umask(i,j)
          enddo
          do j=Jstr,Jend
          DU_east6(j)=(DU_east6(j)-DU_east4(j,iif-1))/rrhot
          enddo
          endif
      endif
      if (.not.SOUTH_INTER) then
          j=Jstr-1
          do i=Istr,Iend
          DU_south4(i,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DU_south6(Istr:Iend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do i=Istr,Iend
             DU_south6(i) = DU_south6(i) +cff1*DU_south4(i,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do i=Istr,Iend
          DU_south6(i)=DU_south6(i)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DU_south4(i,iif-1)
          enddo
          enddo
          do i=Istr,Iend
          DU_south6(i)=lastcff*((dUinterp(i,j)/rrhox)-DU_south6(i))
     &                               * umask(i,j)
          enddo
          do i=Istr,Iend
          DU_south6(i)=(DU_south6(i)-DU_south4(i,iif-1))/rrhot
          enddo
          endif
      endif
      if (.not.NORTH_INTER) then
          j=Jend+1
          do i=Istr,Iend
          DU_north4(i,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DU_north6(Istr:Iend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do i=Istr,Iend
             DU_north6(i) = DU_north6(i) +cff1*DU_north4(i,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do i=Istr,Iend
          DU_north6(i)=DU_north6(i)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DU_north4(i,iif-1)
          enddo
          enddo
          do i=Istr,Iend
          DU_north6(i)=lastcff*((dUinterp(i,j)/rrhox)-DU_north6(i))
     &                               * umask(i,j)
          enddo
          do i=Istr,Iend
          DU_north6(i)=(DU_north6(i)-DU_north4(i,iif-1))/rrhot
          enddo
          endif
      endif
      endif
      if (.not.WEST_INTER) then
          i = Istr
          do j=Jstr,Jend
           DU_west4(j,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          do j=Jstr,Jend
            DU_west(j)=DU_west4(j,iif-1)+DU_west6(j)
          enddo
       endif
      if (.not.EAST_INTER) then
          i=Iend+1
          do j=Jstr,Jend
           DU_east4(j,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          do j=Jstr,Jend
            DU_east(j)=DU_east4(j,iif-1)+DU_east6(j)
          enddo
        endif
      if (.not.SOUTH_INTER) then
          j=Jstr-1
          do i=Istr,Iend
           DU_south4(i,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          do i=Istr,Iend
            DU_south(i)=DU_south4(i,iif-1)+DU_south6(i)
          enddo
       endif
      if (.not.NORTH_INTER) then
          j=Jend+1
          do i=Istr,Iend
           DU_north4(i,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          do i=Istr,Iend
            DU_north(i)=DU_north4(i,iif-1)+DU_north6(i)
          enddo
       endif
      cffx = g*dtfast*2.D0/(1.D0+rrhox)
      if (.not.WEST_INTER) then
        do j=Jstr,Jend
          ubclm(Istr,j)=
     &                   (cffx/om_u(Istr,j))*
     &                   (SSH(Istr,j)-zeta(Istr,j,knew))  +
     &              (2.D0*DU_west(j)/((h(Istr-1,j)+zeta(Istr-1,j,knew)
     &                                 +h(Istr,j)+zeta(Istr,j,knew))
     &                                                *on_u(Istr,j)))
     &                  *umask(Istr,j)
         enddo
      endif
      if (.not.EAST_INTER) then
        do j=Jstr,Jend
          ubclm(Iend+1,j)=
     &                    -(cffx/om_u(Iend+1,j))*
     &                     (SSH(Iend,j)-zeta(Iend,j,knew)) +
     &                  (2.D0*DU_east(j)/(( h(Iend,j)+zeta(Iend,j,knew)
     &                              +h(Iend+1,j)+zeta(Iend+1,j,knew))
     &                                              *on_u(Iend+1,j)))
     &                      *umask(Iend+1,j)
          enddo
      endif
      if (.not.SOUTH_INTER) then
        do i=Istr,Iend
          ubclm(i,Jstr-1)=
     &              (2.D0*DU_south(i)/(( h(i,Jstr-1)+zeta(i,Jstr-1,knew)
     &                           +h(i-1,Jstr-1)+zeta(i-1,Jstr-1,knew))
     &                                                *on_u(i,Jstr-1)))
     &                  *umask(i,Jstr-1)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=Istr,Iend
          ubclm(i,Jend+1)=
     &            (2.D0*DU_north(i)/(( h(i,Jend+1)+zeta(i,Jend+1,knew)
     &                         +h(i-1,Jend+1)+zeta(i-1,Jend+1,knew))
     &                                             *on_u(i,Jend+1)))
     &                *umask(i,Jend+1)
         enddo
      endif
      return
       
      

      end subroutine Sub_Loop_u2dbc_interp_tile

