      interface
        subroutine Sub_Loop_uv3dmix_spg_tile(Istr,Iend,Jstr,Jend,UFx,UFe
     &,VFx,VFe,UFs,VFs,dnUdx,dmUde,dUdz,dnVdx,dmVde,dVdz,dZdx_r,dZdx_p,d
     &Zde_r,dZde_p,padd_E,Mm,padd_X,Lm,rvfrc,dt,rufrc,on_p,om_p,pmask,pm
     &_u,pnom_p,pn_v,pmon_p,visc2_sponge_p,om_r,on_r,vclm,v,pm_v,pnom_r,
     &uclm,u,pn_u,pmon_r,visc2_sponge_r,Hz,nstp,NORTH_INTER,SOUTH_INTER,
     &EAST_INTER,WEST_INTER,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: dt
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rvfrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rufrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pnom_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmon_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pnom_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmon_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: UFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: VFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmUde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmVde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnUdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnVdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dUdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dVdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_r
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_r
        end subroutine Sub_Loop_uv3dmix_spg_tile

      end interface
