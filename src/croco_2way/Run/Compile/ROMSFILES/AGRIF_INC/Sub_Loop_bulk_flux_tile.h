      subroutine Sub_Loop_bulk_flux_tile(Istr,Iend,Jstr,Jend,aer,cer,pad
     &d_E,Mm,padd_X,Lm,vmask,vwnd,svstr,umask,uwnd,sustr,shflx_rlw,shflx
     &_sen,shflx_lat,shflx_rsw,rmask,prate,srflx,stflx,radlw,rhum,nrhs,t
     &,tair,wspd,rho0,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: blk_Rgas = 287.1D0
      real, parameter :: blk_ZW = 10.0D0
      real, parameter :: blk_ZT = 10.0D0
      real, parameter :: blk_ZQ = 10.0D0
      real, parameter :: blk_Zabl = 600.0D0
      real, parameter :: blk_beta = 1.2D0
      real, parameter :: blk_Cpa = 1004.67D0
      real, parameter :: rhow = 1000.0D0
      real, parameter :: patm = 1010.0D0
      real, parameter :: eps = 1.D-20
      real, parameter :: r3 = 1.0D0/3.0D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nrhs
      real :: rho0
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vwnd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: uwnd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: prate
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: radlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhum
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: tair
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: wspd
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: aer
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: cer

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                                       
                                                                 
     
      integer(4) :: i
      integer(4) :: j
      integer(4) :: imin
      integer(4) :: imax
      integer(4) :: jmin
      integer(4) :: jmax
                             
      integer(4) :: IterMax
      integer(4) :: Iter
                 
      real :: a
      real :: cff
                     
      real :: rho0i
      real :: cpi
                            
      real :: TseaC
      real :: TseaK
      real :: Qsea
                                   
      real :: TairC
      real :: TairK
      real :: rhoAir
      real :: Qair
                           
      real :: Q
      real :: RH
      real :: VisAir
      real :: Hlv
                          
      real :: delW
      real :: delT
      real :: delQ
                                      
      real :: u10
      real :: Zo10
      real :: Cd10
      real :: Ch10
      real :: Ct10
      real :: Cd
                                   
      real :: Ct
      real :: CC
      real :: Ri
      real :: Ribcu
      real :: Zetu
      real :: L10
                             
      real :: Wstar
      real :: Tstar
      real :: Qstar
                                         
      real :: ZoW
      real :: ZoT
      real :: ZoT10
      real :: ZoQ
      real :: ZoL
      real :: L
      real :: Rr
      real :: Bf
                          
      real :: Wpsi
      real :: Tpsi
      real :: Qpsi
                            
      real :: wspd0
      real :: Wgus
      real :: charn
                               
      real :: bulk_psiu
      real :: bulk_psit
                                       
      real :: hfsen
      real :: hflat
      real :: hflw
      real :: upvel
      real :: evap
                                                                   
                                  

                               

                               

                               

                                  

                                

                                   

                     
                               

                               

                  
                            

                                

                                                                       
                                                                 
                  
      if (.not.WEST_INTER) then
        imin=Istr-1
      else
        imin=Istr-2
      endif
      if (.not.EAST_INTER) then
        imax=Iend+1
      else
        imax=Iend+2
      endif
      if (.not.SOUTH_INTER) then
        jmin=Jstr-1
      else
        jmin=Jstr-2
      endif
      if (.not.NORTH_INTER) then
        jmax=Jend+1
      else
        jmax=Jend+2
      endif
      rho0i=1.0D0/rho0
      cpi=1.0D0/cp
      do j=jmin,jmax
        do i=imin,imax
          wspd0=wspd(i,j)
          TairC=tair(i,j)
          TairK=TairC+273.16D0
          TseaC=t(i,j,N,nrhs,itemp)
          TseaK=TseaC+273.16D0
          RH=rhum(i,j)
          hflw=-radlw(i,j)
          cff=(1.0007D0+3.46D-6*patm)*6.1121D0*
     &        exp(17.502D0*TairC/(240.97D0+TairC))
          Qair=0.62197D0*(cff/(patm-0.378D0*cff))
          if (RH.lt.2.0D0) then
            cff=cff*RH
            Q=0.62197D0*(cff/(patm-0.378D0*cff))
          else
            Q=RH/1000.0D0
          endif
          cff=(1.0007D0+3.46D-6*patm)*6.1121D0*
     &            exp(17.502D0*TseaC/(240.97D0+TseaC))
          cff=cff*0.98D0
          Qsea=0.62197D0*(cff/(patm-0.378D0*cff))
          rhoAir=patm*100.0D0/(blk_Rgas*TairK*
     &                              (1.0D0+0.61D0*Q))
          VisAir=1.326D-5*(1.0D0+TairC*(6.542D-3+TairC*
     &               (8.301D-6-4.84D-9*TairC)))
          Hlv=(2.501D0-0.00237D0*TseaC)*1.0D+6
          Wgus=0.5D0
          delW=SQRT(wspd0*wspd0+Wgus*Wgus)
          delQ=Qsea-Q
          delT=TseaC-TairC
          ZoW=0.0001D0
          u10=delW*LOG(10.0D0/ZoW)/LOG(blk_ZW/ZoW)
          Wstar=0.035D0*u10
          Zo10=0.011D0*Wstar*Wstar/g+0.11D0*VisAir/Wstar
          Cd10=(vonKar/LOG(10.0D0/Zo10))**2
          Ch10=0.00115D0
          Ct10=Ch10/sqrt(Cd10)
          ZoT10=10.0D0/exp(vonKar/Ct10)
          Cd=(vonKar/LOG(blk_ZW/Zo10))**2
          Ct=vonKar/LOG(blk_ZT/ZoT10)
          CC=vonKar*Ct/Cd
          Ribcu=-blk_ZW/(blk_Zabl*0.004D0*blk_beta**3)
          Ri=-g*blk_ZW*(delT+0.61D0*TairK*delQ)/
     &          (TairK*delW*delW)
          if (Ri.lt.0.0D0) then
            Zetu=CC*Ri/(1.0D0+Ri/Ribcu)
          else
            Zetu=CC*Ri/(1.0D0+3.0D0*Ri/CC)
          endif
          L10=blk_ZW/Zetu
          if (Zetu.gt.50.0D0) then
            IterMax=1
          else
            IterMax=3
          endif
          Wstar=delW*vonKar/(LOG(blk_ZW/Zo10)-
     &                             bulk_psiu(blk_ZW/L10,pi))
          Tstar=-delT*vonKar/(LOG(blk_ZT/ZoT10)-
     &              bulk_psit(blk_ZT/L10,pi))
          Qstar=-delQ*vonKar/(LOG(blk_ZQ/ZoT10)-
     &              bulk_psit(blk_ZQ/L10,pi))
          if (delW.gt.18.0D0) then
            charn=0.018D0
          elseif ((10.0D0.lt.delW).and.(delW.le.18.0D0)) then
            charn=0.011D0+0.125D0*(0.018D0-0.011D0)*(delW-10.D0)
          else
            charn=0.011D0
          endif
        do Iter=1,IterMax
          ZoW=charn*Wstar*Wstar/g+0.11D0*VisAir/(Wstar+eps)
          Rr=ZoW*Wstar/VisAir
          ZoQ=MIN(1.15D-4,5.5D-5/Rr**0.6D0)
          ZoT=ZoQ
          ZoL=vonKar*g*blk_ZW*
     &             (Tstar*(1.0D0+0.61D0*Q)+0.61D0*TairK*Qstar)/
     &             (TairK*Wstar*Wstar*(1.0D0+0.61D0*Q)+eps)
          L=blk_ZW/(ZoL+eps)
          Wpsi=bulk_psiu(ZoL,pi)
          Tpsi=bulk_psit(blk_ZT/L,pi)
          Qpsi=bulk_psit(blk_ZQ/L,pi)
          Wstar=MAX(eps,delW*vonKar/(LOG(blk_ZW/ZoW)-Wpsi))
          Tstar=-delT*vonKar/(LOG(blk_ZT/ZoT)-Tpsi)
          Qstar=-delQ*vonKar/(LOG(blk_ZQ/ZoQ)-Qpsi)
          Bf=-g/TairK*Wstar*(Tstar+0.61D0*TairK*Qstar)
          if (Bf.gt.0.0D0) then
            Wgus=blk_beta*(Bf*blk_Zabl)**r3
          else
            Wgus=0.2D0
          endif
          delW=SQRT(wspd0*wspd0+Wgus*Wgus)
        enddo
          wspd0=SQRT(wspd0*wspd0+Wgus*Wgus)
          Cd=Wstar*Wstar/(wspd0*wspd0+eps)
          hfsen=-blk_Cpa*rhoAir*Wstar*Tstar
          hflat=-Hlv*rhoAir*Wstar*Qstar
          upvel=-1.61D0*Wstar*Qstar-(1.0D0+1.61D0*Q)*Wstar*Tstar/TairK
          hflat=hflat+rhoAir*Hlv*upvel*Q
          hflat=-hflat*rho0i*cpi
          hfsen=-hfsen*rho0i*cpi
          stflx(i,j,itemp)=srflx(i,j)+hflw+hflat+hfsen
          evap=-cp*hflat/Hlv
          stflx(i,j,isalt)=(evap-prate(i,j))*t(i,j,N,nrhs,isalt)
          stflx(i,j,itemp)=stflx(i,j,itemp)*rmask(i,j)
          stflx(i,j,isalt)=stflx(i,j,isalt)*rmask(i,j)
          aer(i,j)=rhoAir*wspd0*rho0i
          cer(i,j)=Cd
          shflx_rsw(i,j)=srflx(i,j)
          shflx_lat(i,j)=hflat
          shflx_sen(i,j)=hfsen
          shflx_rlw(i,j)=hflw
        enddo
      enddo
      do j=jmin,jmax
        do i=imin+1,imax
          a=0.5D0*(aer(i-1,j)+aer(i,j))
          cff=0.5D0*(cer(i-1,j)+cer(i,j))
          sustr(i,j)=a*cff*uwnd(i,j)
          sustr(i,j)=sustr(i,j)*umask(i,j)
        enddo
      enddo
      do j=jmin+1,jmax
        do i=imin,imax
          a=0.5D0*(aer(i,j-1)+aer(i,j))
          cff=0.5D0*(cer(i,j-1)+cer(i,j))
          svstr(i,j)=a*cff*vwnd(i,j)
          svstr(i,j)=svstr(i,j)*vmask(i,j)
        enddo
      enddo
      return
       
      

      end subroutine Sub_Loop_bulk_flux_tile

