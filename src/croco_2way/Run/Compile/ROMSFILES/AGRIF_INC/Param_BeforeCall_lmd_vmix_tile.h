      integer(4), parameter :: N = 66
      interface
        subroutine Sub_Loop_lmd_vmix_tile(Istr,Iend,Jstr,Jend,Kv,Kt,Ks,R
     &ig,padd_E,Mm,padd_X,Lm,rmask,pmask2,bvf,v,nstp,u,z_r,NORTH_INTER,S
     &OUTH_INTER,EAST_INTER,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-14
      real, parameter :: lmd_Ri0 = 0.7D0
      real, parameter :: lmd_nuwm = 1.0D-4
      real, parameter :: lmd_nuws = 0.1D-4
      real, parameter :: lmd_nu0m = 50.D-4
      real, parameter :: lmd_nu0s = 50.D-4
      real, parameter :: lmd_nu0c = 0.1D0
      real, parameter :: lmd_nu = 1.5D-6
      real, parameter :: lmd_Rrho0 = 1.9D0
      real, parameter :: lmd_nuf = 10.0D-4
      real, parameter :: lmd_fdd = 0.7D0
      real, parameter :: lmd_tdd1 = 0.909D0
      real, parameter :: lmd_tdd2 = 4.6D0
      real, parameter :: lmd_tdd3 = 0.54D0
      real, parameter :: lmd_sdd1 = 0.15D0
      real, parameter :: lmd_sdd2 = 1.85D0
      real, parameter :: lmd_sdd3 = 0.85D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Rig
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks
        end subroutine Sub_Loop_lmd_vmix_tile

      end interface
