      subroutine Sub_Loop_set_smflux_tile(Istr,Iend,Jstr,Jend,padd_E,Mm,
     &padd_X,Lm,may_day_flag,tdays,synchro_flag,svstrp,sustrp,svstrg,svs
     &tr,sustrg,sms_scale,sustr,lsvsgrd,lsusgrd,ntstart,iic,sms_cycle,sm
     &s_time,dt,time,itsms,NORTH_INTER,Mmmpi,SOUTH_INTER,EAST_INTER,Lmmp
     &i,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: may_day_flag
      real :: tdays
      logical :: synchro_flag
      real :: sms_scale
      integer(4) :: lsvsgrd
      integer(4) :: lsusgrd
      integer(4) :: ntstart
      integer(4) :: iic
      real :: sms_cycle
      real :: dt
      real :: time
      integer(4) :: itsms
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(1:2) :: svstrp
      real, dimension(1:2) :: sustrp
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: svstrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: sustrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(1:2) :: sms_time
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                   

                                                  
      integer(4) :: i
      integer(4) :: j
      integer(4) :: it1
      integer(4) :: it2
                                             
      real :: cff
      real :: cff1
      real :: cff2
      real :: ustress
      real :: vstress
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
      if (Istr.eq.1) then
        if (WEST_INTER) then
          IstrR=Istr-2
        else
          IstrR=Istr-1
        endif
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lmmpi) then
        if (EAST_INTER) then
          IendR=Iend+2
        else
          IendR=Iend+1
        endif
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        if (SOUTH_INTER) then
          JstrR=Jstr-2
        else
          JstrR=Jstr-1
        endif
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mmmpi) then
        if (NORTH_INTER) then
          JendR=Jend+2
        else
          JendR=Jend+1
        endif
      else
        JendR=Jend
      endif
      it1=3-itsms
      it2=itsms
      cff=time+0.5D0*dt
      cff1=sms_time(it2)-cff
      cff2=cff-sms_time(it1)
      if (sms_cycle.lt.0.D0) then
        if (iic.eq.ntstart) then
          if (lsusgrd.eq.1 .and. lsvsgrd.eq.1) then
            do j=JstrR,JendR
              do i=Istr,IendR
                sustr(i,j)=sms_scale*sustrg(i,j,itsms)
              enddo
            enddo
            do j=Jstr,JendR
              do i=IstrR,IendR
                svstr(i,j)=sms_scale*svstrg(i,j,itsms)
              enddo
            enddo
          else
            ustress=sms_scale*sustrp(itsms)
            vstress=sms_scale*svstrp(itsms)
            do j=JstrR,JendR
              do i=Istr,IendR
                sustr(i,j)=ustress
              enddo
            enddo
            do j=Jstr,JendR
              do i=IstrR,IendR
                svstr(i,j)=vstress
              enddo
            enddo
          endif
        endif
      elseif (cff1.ge.0.D0 .and. cff2.ge.0.D0) then
        if (Istr+Jstr.eq.2 .and. cff1.lt.dt) synchro_flag=.TRUE.
        cff=sms_scale/(cff1+cff2)
        cff1=cff1*cff
        cff2=cff2*cff
        if (lsusgrd.eq.1 .and. lsvsgrd.eq.1) then
          do j=JstrR,JendR
            do i=Istr,IendR
              sustr(i,j)=cff1*sustrg(i,j,it1)+cff2*sustrg(i,j,it2)
            enddo
          enddo
          do j=Jstr,JendR
            do i=IstrR,IendR
              svstr(i,j)=cff1*svstrg(i,j,it1)+cff2*svstrg(i,j,it2)
            enddo
          enddo
        else
          ustress=cff1*sustrp(it1)+cff2*sustrp(it2)
          vstress=cff1*svstrp(it1)+cff2*svstrp(it2)
          do j=JstrR,JendR
            do i=Istr,IendR
              sustr(i,j)=ustress
            enddo
          enddo
          do j=Jstr,JendR
            do i=IstrR,IendR
              svstr(i,j)=vstress
            enddo
          enddo
        endif
      else
        if (Istr+Jstr.eq.2) then
          write(stdout,1) 'sms_time', tdays, sms_time(it1)*sec2day,
     &                                       sms_time(it2)*sec2day
  1       format(/,' SET_SMFLUX_TILE - current model time',
     &           1x,'exceeds ending value for variable: ',a,/,
     &           14x,'TDAYS = ',g12.4,2x,'SMS_TSTART = ',g12.4,2x,
     &                                        'SMS_TEND = ',g12.4)
          may_day_flag=2
        endif
      endif
      return
       
      

      end subroutine Sub_Loop_set_smflux_tile

