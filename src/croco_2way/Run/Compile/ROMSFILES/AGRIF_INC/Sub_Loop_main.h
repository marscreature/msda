      subroutine Sub_Loop_main(ntimes,ntstart,iic,nbstep3d,iif,time,time
     &_start,kstp,next_kstp,mynode,wrthis,ldefhis,may_day_flag,numthread
     &s,N1dETA,N1dXI,eta_v,eta_rho,xi_u,xi_rho,MMm,LLm,Mmmpi,Lmmpi,padd_
     &E,Mm,padd_X,Lm,N3d,N2d)

      use Agrif_Util


      include 'mpif.h'
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4) :: ntimes
      integer(4) :: ntstart
      integer(4) :: iic
      integer(4) :: nbstep3d
      integer(4) :: iif
      real :: time
      real :: time_start
      integer(4) :: kstp
      integer(4) :: next_kstp
      integer(4) :: mynode
      logical :: ldefhis
      integer(4) :: may_day_flag
      integer(4) :: numthreads
      integer(4) :: N1dETA
      integer(4) :: N1dXI
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: MMm
      integer(4) :: LLm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: N3d
      integer(4) :: N2d
      logical, dimension(1:500+NT) :: wrthis

                                      
      integer(4) :: tile
      integer(4) :: subs
      integer(4) :: trd
      integer(4) :: ierr
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
!$AGRIF_DO_NOT_TREAT
      INTEGER*4 :: ocean_grid_comm
      common /cpl_comm/ ocean_grid_comm
!$AGRIF_END_DO_NOT_TREAT
                                              
      type(Agrif_pgrid), pointer :: parcours
                       
                                    
      integer(4) :: iifroot
      integer(4) :: iicroot
                                                
      integer(4) :: size_XI
      integer(4) :: size_ETA
      integer(4) :: se
      integer(4) :: sse
      integer(4) :: sz
      integer(4) :: ssz
      external :: step
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
      LLm=LLm0;  MMm=MMm0
      Lm=(LLm+NP_XI-1)/NP_XI; Mm=(MMm+NP_ETA-1)/NP_ETA
      padd_X=(Lm+2)/2-(Lm+1)/2; padd_E=(Mm+2)/2-(Mm+1)/2
      xi_rho=LLm+2;  xi_u=xi_rho-1
      eta_rho=MMm+2; eta_v=eta_rho-1
      size_XI=7+(Lm+NSUB_X-1)/NSUB_X
      size_ETA=7+(Mm+NSUB_E-1)/NSUB_E
      sse=size_ETA/Np; ssz=Np/size_ETA
      se=sse/(sse+ssz);   sz=1-se
      N1dXI = size_XI
      N1dETA = size_ETA
      N2d=size_XI*(se*size_ETA+sz*Np)
      N3d=size_XI*size_ETA*Np
      call Agrif_MPI_Init(MPI_COMM_WORLD)
                             

      call declare_zoom_variables()
      call MPI_Setup (ierr)
      if (ierr.ne.0) goto 100
      call read_inp (ierr)
      if (ierr.ne.0) goto 100
      call init_scalars (ierr)
      if (ierr.ne.0) goto 100
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call start_timers()
        call init_arrays (tile)
         enddo
       enddo
      call get_grid
      if (may_day_flag.ne.0) goto 99
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call setup_grid1 (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call setup_grid2 (tile)
         enddo
       enddo
      call set_scoord
      call set_weights
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call set_depth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call grid_stiffness (tile)
         enddo
       enddo
      call get_initial
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call ana_initial (tile)
         enddo
       enddo
      if (may_day_flag.ne.0) goto 99
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call set_depth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call set_HUV (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call omega (tile)
        call rho_eos (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call set_nudgcof (tile)
         enddo
       enddo
      call get_tclima
      call get_uclima
      call get_ssh
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call ana_tclima (tile)
         enddo
       enddo
      call get_vbc
      call get_tides
      if (may_day_flag.ne.0) goto 99
      if (ldefhis .and. wrthis(indxTime)) call wrt_his
      if (may_day_flag.ne.0) goto 99
      if (mynode.eq.0) write(stdout,'(/1x,A27/)')
     &                'MAIN: started time-steping.'
      next_kstp=kstp
      time_start=time
      call MPI_Barrier(MPI_COMM_WORLD, ierr)
      iif = -1
      nbstep3d = 0
      iic = ntstart
      iind = -1
      grids_at_level = -1
      sortedint = -1
      call computenbmaxtimes
      do iicroot=ntstart,ntimes+1
        nbtimes = 0
        do while (nbtimes.LE.nbmaxtimes)
          call Agrif_Step(step)
        enddo
        if (may_day_flag.ne.0) goto 99
      enddo
  99  continue
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call stop_timers()
         enddo
       enddo
      call closecdf
      parcours=>Agrif_Curgrid%child_list % first
      do while (associated(parcours))
        Call Agrif_Instance(parcours % gr)
        call closecdf
        parcours => parcours % next
      enddo
 100  continue
      call MPI_Barrier(MPI_COMM_WORLD, ierr)
      call MPI_Finalize (ierr)
      stop
       
      

      end subroutine Sub_Loop_main

