      subroutine Sub_Loop_u3dbc_parent_tile(Istr,Iend,Jstr,Jend,grad,pad
     &d_E,Mm,padd_X,Lm,umask,uclm,nnew,pmask,nstp,u,tauM_out,tauM_in,dt,
     &NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: nstp
      real :: tauM_out
      real :: tauM_in
      real :: dt
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                                
                                                                       
                                                                 
     
      real :: cff
      real :: cx
      real :: cy
      real :: dft
      real :: dfx
      real :: dfy
      real :: tau
      real :: tau_in
      real :: tau_out
                            

                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      tau_in=dt*tauM_in
      tau_out=dt*tauM_out
      if (.not.WEST_INTER) then
        do k=1,N
          do j=Jstr,Jend+1
            grad(Istr  ,j)=(u(Istr  ,j,k,nstp)-u(Istr  ,j-1,k,nstp))
     &                                                *pmask(Istr,j)
            grad(Istr+1,j)=(u(Istr+1,j,k,nstp)-u(Istr+1,j-1,k,nstp))
     &                                              *pmask(Istr+1,j)
          enddo
          do j=Jstr,Jend
            dft=u(Istr+1,j,k,nstp)-u(Istr+1,j,k,nnew)
            dfx=u(Istr+1,j,k,nnew)-u(Istr+2,j,k,nnew)
            if (dfx*dft .lt. 0.D0) then
              dft=0.D0
              tau=tau_in
            else
              tau=tau_out
            endif
            if (dft*(grad(Istr+1,j)+grad(Istr+1,j+1)) .gt. 0.D0) then
              dfy=grad(Istr+1,j)
            else
              dfy=grad(Istr+1,j+1)
            endif
            cff=max(dfx*dfx+dfy*dfy, eps)
            cx=dft*dfx
            cy=min(cff,max(dft*dfy,-cff))
            u(Istr,j,k,nnew)=( cff*u(Istr,j,k,nstp)
     &                        +cx*u(Istr+1,j,k,nnew)
     &                    -max(cy,0.D0)*grad(Istr,j  )
     &                    -min(cy,0.D0)*grad(Istr,j+1)
     &                                   )/(cff+cx)
            u(Istr,j,k,nnew)=(1.D0-tau)*u(Istr,j,k,nnew)+tau*
     &                                      uclm(Istr,j,k)
            u(Istr,j,k,nnew)=u(Istr,j,k,nnew)*umask(Istr,j)
          enddo
        enddo
      endif
      if (.not.EAST_INTER) then
        do k=1,N
          do j=Jstr,Jend+1
            grad(Iend  ,j)=(u(Iend  ,j,k,nstp)-u(Iend  ,j-1,k,nstp))
     &                                                *pmask(Iend,j)
            grad(Iend+1,j)=(u(Iend+1,j,k,nstp)-u(Iend+1,j-1,k,nstp))
     &                                              *pmask(Iend+1,j)
          enddo
          do j=Jstr,Jend
            dft=u(Iend,j,k,nstp)-u(Iend  ,j,k,nnew)
            dfx=u(Iend,j,k,nnew)-u(Iend-1,j,k,nnew)
            if (dfx*dft .lt. 0.D0) then
              dft=0.D0
              tau=tau_in
            else
              tau=tau_out
            endif
            if (dft*(grad(Iend,j)+grad(Iend,j+1)) .gt. 0.D0) then
              dfy=grad(Iend,j)
            else
              dfy=grad(Iend,j+1)
            endif
            cff=max(dfx*dfx+dfy*dfy, eps)
            cx=dft*dfx
            cy=min(cff,max(dft*dfy,-cff))
            u(Iend+1,j,k,nnew)=( cff*u(Iend+1,j,k,nstp)
     &                              +cx*u(Iend,j,k,nnew)
     &                      -max(cy,0.D0)*grad(Iend+1,j  )
     &                      -min(cy,0.D0)*grad(Iend+1,j+1)
     &                                       )/(cff+cx)
            u(Iend+1,j,k,nnew)=(1.D0-tau)*u(Iend+1,j,k,nnew)+tau*
     &                                        uclm(Iend+1,j,k)
            u(Iend+1,j,k,nnew)=u(Iend+1,j,k,nnew)*umask(Iend+1,j)
          enddo
        enddo
      endif
      if (.not.SOUTH_INTER) then
        do k=1,N
          do i=IstrU-1,Iend
            grad(i,Jstr-1)=u(i+1,Jstr-1,k,nstp)-u(i,Jstr-1,k,nstp)
            grad(i,Jstr  )=u(i+1,Jstr  ,k,nstp)-u(i,Jstr  ,k,nstp)
          enddo
          do i=IstrU,Iend
            dft=u(i,Jstr,k,nstp)-u(i,Jstr  ,k,nnew)
            dfx=u(i,Jstr,k,nnew)-u(i,Jstr+1,k,nnew)
            if (dfx*dft .lt. 0.D0) then
              dft=0.D0
              tau=tau_in
            else
              tau=tau_out
            endif
            if (dft*(grad(i-1,Jstr)+grad(i,Jstr)) .gt. 0.D0) then
              dfy=grad(i-1,Jstr)
            else
              dfy=grad(i  ,Jstr)
            endif
            cff=max(dfx*dfx+dfy*dfy, eps)
            cx=dft*dfx
            cy=min(cff,max(dft*dfy,-cff))
            u(i,Jstr-1,k,nnew)=( cff*u(i,Jstr-1,k,nstp)
     &                              +cx*u(i,Jstr,k,nnew)
     &                      -max(cy,0.D0)*grad(i-1,Jstr-1)
     &                      -min(cy,0.D0)*grad(i  ,Jstr-1)
     &                                       )/(cff+cx)
           u(i,Jstr-1,k,nnew)=(1.D0-tau)*u(i,Jstr-1,k,nnew)
     &                              +tau*uclm(i,Jstr-1,k)
            u(i,Jstr-1,k,nnew)=u(i,Jstr-1,k,nnew)*umask(i,Jstr-1)
          enddo
        enddo
      endif
      if (.not.NORTH_INTER) then
        do k=1,N
          do i=IstrU-1,Iend
            grad(i,Jend  )=u(i+1,Jend  ,k,nstp)-u(i,Jend  ,k,nstp)
            grad(i,Jend+1)=u(i+1,Jend+1,k,nstp)-u(i,Jend+1,k,nstp)
          enddo
          do i=IstrU,Iend
            dft=u(i,Jend,k,nstp)-u(i,Jend  ,k,nnew)
            dfx=u(i,Jend,k,nnew)-u(i,Jend-1,k,nnew)
            if (dfx*dft .lt. 0.D0) then
              dft=0.D0
              tau=tau_in
            else
              tau=tau_out
            endif
            if (dft*(grad(i-1,Jend)+grad(i,Jend)) .gt. 0.D0) then
              dfy=grad(i-1,Jend)
            else
              dfy=grad(i  ,Jend)
            endif
            cff=max(dfx*dfx+dfy*dfy, eps)
            cx=dft*dfx
            cy=min(cff,max(dft*dfy,-cff))
            u(i,Jend+1,k,nnew)=( cff*u(i,Jend+1,k,nstp)
     &                              +cx*u(i,Jend,k,nnew)
     &                      -max(cy,0.D0)*grad(i-1,Jend+1)
     &                      -min(cy,0.D0)*grad(i  ,Jend+1)
     &                                       )/(cff+cx)
            u(i,Jend+1,k,nnew)=(1.D0-tau)*u(i,Jend+1,k,nnew)
     &                               +tau*uclm(i,Jend+1,k)
            u(i,Jend+1,k,nnew)=u(i,Jend+1,k,nnew)*umask(i,Jend+1)
          enddo
        enddo
      endif
      if (.not.WEST_INTER .and. .not.SOUTH_INTER) then
        do k=1,N
          u(Istr,Jstr-1,k,nnew)=0.5D0*( u(Istr+1,Jstr-1,k,nnew)
     &                               +u(Istr  ,Jstr  ,k,nnew))
     &                          *umask(Istr,Jstr-1)
        enddo
      endif
      if (.not.EAST_INTER .and. .not.SOUTH_INTER) then
        do k=1,N
          u(Iend+1,Jstr-1,k,nnew)=0.5D0*( u(Iend,Jstr-1,k,nnew)
     &                                 +u(Iend+1,Jstr,k,nnew))
     &                            *umask(Iend+1,Jstr-1)
        enddo
      endif
      if (.not.WEST_INTER .and. .not.NORTH_INTER) then
        do k=1,N
          u(Istr,Jend+1,k,nnew)=0.5D0*( u(Istr+1,Jend+1,k,nnew)
     &                               +u(Istr  ,Jend  ,k,nnew))
     &                          *umask(Istr,Jend+1)
        enddo
      endif
      if (.not.EAST_INTER .and. .not.NORTH_INTER) then
        do k=1,N
          u(Iend+1,Jend+1,k,nnew)=0.5D0*( u(Iend,Jend+1,k,nnew)
     &                                 +u(Iend+1,Jend,k,nnew))
     &                            *umask(Iend+1,Jend+1)
        enddo
      endif
      return
       
      

      end subroutine Sub_Loop_u3dbc_parent_tile

