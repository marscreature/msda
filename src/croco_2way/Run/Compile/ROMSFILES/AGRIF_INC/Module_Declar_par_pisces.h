      LOGICAL, parameter, public :: lk_pisces = .FALSE.
      LOGICAL, parameter, public :: lk_kriest = .FALSE.
      INTEGER, parameter, public :: jp_pisces = 0
      INTEGER, parameter, public :: jp_pisces_2d = 0
      INTEGER, parameter, public :: jp_pisces_3d = 0
      INTEGER, parameter, public :: jptra = jp_pisces
      INTEGER, parameter, public :: jp_pcs0 = 1
      INTEGER, parameter, public :: jp_pcs1 = jp_pisces
