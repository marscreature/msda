      integer(4), parameter :: N = 66
      interface
        subroutine Sub_Loop_ab_ratio_slice(ratio,Istr,Iend,j,padd_E,Mm,p
     &add_X,Lm,z_w,nstp,t)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: A0 = +0.665157D-01
      real, parameter :: A1 = +0.170907D-01
      real, parameter :: A2 = -0.203814D-03
      real, parameter :: A3 = +0.298357D-05
      real, parameter :: A4 = -0.255019D-07
      real, parameter :: B0 = +0.378110D-02
      real, parameter :: B1 = -0.846960D-04
      real, parameter :: CO = -0.678662D-05
      real, parameter :: D0 = +0.380374D-04
      real, parameter :: D1 = -0.933746D-06
      real, parameter :: D2 = +0.791325D-08
      real, parameter :: E0 = -0.164759D-06
      real, parameter :: F0 = -0.251520D-11
      real, parameter :: G0 = +0.512857D-12
      real, parameter :: H0 = -0.302285D-13
      real, parameter :: Smean = 35.0D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: z_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: j
      real, dimension(Istr-2:Iend+2,0:N) :: ratio
        end subroutine Sub_Loop_ab_ratio_slice

      end interface
