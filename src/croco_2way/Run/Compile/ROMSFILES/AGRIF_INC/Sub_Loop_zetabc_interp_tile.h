      subroutine Sub_Loop_zetabc_interp_tile(Istr,Iend,Jstr,Jend,Zeta_we
     &st4,Zeta_east4,Zeta_west6,Zeta_east6,Zeta_south4,Zeta_north4,Zeta_
     &south6,Zeta_north6,padd_E,Mm,padd_X,Lm,SSH,nfast,Zt_avg3,weight2,Z
     &etaTimeindex2,zetaid,dZtinterp,ZetaTimeindex,iif,NORTH_INTER,SOUTH
     &_INTER,EAST_INTER,WEST_INTER,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nfast
      integer(4) :: ZetaTimeindex2
      integer(4) :: zetaid
      integer(4) :: ZetaTimeindex
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: SSH
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: Zt_avg
     &3
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dZtinterp
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-1:Istr,-1:Mm+2+padd_E) :: Zeta_west6
      real, dimension(Iend:Iend+1,-1:Mm+2+padd_E) :: Zeta_east6
      real, dimension(-1:Lm+2+padd_X,Jstr-1:Jstr) :: Zeta_south6
      real, dimension(-1:Lm+2+padd_X,Jend:Jend+1) :: Zeta_north6
      real, dimension(Istr-1:Istr,-1:Mm+2+padd_E,0:NWEIGHT) :: Zeta_west
     &4
      real, dimension(Iend:Iend+1,-1:Mm+2+padd_E,0:NWEIGHT) :: Zeta_east
     &4
      real, dimension(-1:Lm+2+padd_X,Jstr-1:Jstr,0:NWEIGHT) :: Zeta_sout
     &h4
      real, dimension(-1:Lm+2+padd_X,Jend:Jend+1,0:NWEIGHT) :: Zeta_nort
     &h4

                     
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                 
      integer(4) :: i
      integer(4) :: j
      integer(4) :: i1
      integer(4) :: j1
                   
      real :: tinterp
                        
      Integer(4) :: itrcind
                                                         
      INTEGER(4) :: parentknew
      INTEGER(4) :: parentkstp
      INTEGER(4) :: nbstep3dparent
                                           
      real :: t1
      real :: t2
      real :: t3
      real :: t4
      real :: t5
      real :: t6
      real :: t9
      real :: t10
      real :: t11
      real :: t7
                           
      real, dimension(1:2) :: tin
      real, dimension(1:2) :: tout
                
      real :: cff1
                       
      real :: zeta1
      real :: zeta2
      external zetainterp
                                       
      integer(4) :: irhot
      integer(4) :: irhox
      integer(4) :: irhoy
                    
      real :: rrhot
                        
      integer(4) :: iter
                          
      real :: onemtinterp
                                
      integer(4) :: parentnbstep
                          
      integer(4) :: pngrid
                                                               
                                                               
                                                                
                                                                
                                                              
      real, dimension(Istr-1:Istr,Jstr-1:Jend+1) :: Zeta_west7
                                                              
      real, dimension(Iend:Iend+1,Jstr-1:Jend+1) :: Zeta_east7
                                                               
      real, dimension(Istr-1:Iend+1,Jstr-1:Jstr) :: Zeta_south7
                                                               
      real, dimension(Istr-1:Iend+1,Jend:Jend+1) :: Zeta_north7
                                                                       
                                                                 
  
                                                                       
                                                                 
  
                                                                       
                                                                 
                                                                 
 
                                                                       
                                                                 
                                                                 
 
                   
      real :: lastcff
!$AGRIF_DO_NOT_TREAT
      integer*4 :: nbgrid, indinterp
      common/interp2d/indinterp,nbgrid
!$AGRIF_END_DO_NOT_TREAT
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
       irhot = Agrif_Irhot()
       irhox = Agrif_Irhox()
       irhoy = Agrif_Irhoy()
       rrhot = real(irhot)
      parentnbstep=
     &  Agrif_Parent(iif)
      if (ZetaTimeindex .NE. parentnbstep) then
C$OMP BARRIER
C$OMP MASTER
        dZtinterp = 0.D0
            tinterp=1.D0
            Agrif_UseSpecialvalue=.true.
            Agrif_Specialvalue=0.D0
        nbgrid = Agrif_Fixed()
        pngrid = Agrif_Parent_Fixed()
        if (nbgrid == grids_at_level(pngrid,0)) indinterp = 0
            Call Agrif_Bc_variable(zetaid,
     &                                        calledweight=tinterp,
     &                                        procname=zetainterp)
            Agrif_UseSpecialvalue=.false.
C$OMP END MASTER
C$OMP BARRIER
            ZetaTimeindex = parentnbstep
            ZetaTimeindex2 = agrif_nb_step()
          endif
          if (agrif_nb_step() .EQ. ZetaTimeindex2) then
          lastcff=0.D0
          do iter=iif,iif+irhot-1
          lastcff=lastcff+((iter+1-iif)/rrhot)*
     &        weight2(iif+irhot-1,iter)
          enddo
          lastcff=1.D0/lastcff
      if (.not.SOUTH_INTER) then
          do j=Jstr-1,Jstr
          do i=IstrU-1,Iend
          Zeta_south4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          if (iif+irhot-1<=nfast) then
           Zeta_south7(IstrU-1:Iend,Jstr-1:Jstr)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=Jstr-1,Jstr
            do i=IstrU-1,Iend
             Zeta_south7(i,j) = Zeta_south7(i,j)
     &                         +cff1*Zeta_south4(i,j,iter)
            enddo
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=Jstr-1,Jstr
          do i=IstrU-1,Iend
          Zeta_south7(i,j)=Zeta_south7(i,j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*Zeta_south4(i,j,iif-1)
          enddo
          enddo
          enddo
          do j=Jstr-1,Jstr
          do i=IstrU-1,Iend
          Zeta_south7(i,j)=lastcff*(dZtinterp(i,j)-Zeta_south7(i,j))
          enddo
          enddo
          do j=Jstr-1,Jstr
          do i=IstrU-1,Iend
          Zeta_south6(i,j)=(Zeta_south7(i,j)-Zeta_south4(i,j,iif-1))
     &                           /rrhot
          enddo
          enddo
          endif
      endif
      if (.not.NORTH_INTER) then
          do j=Jend,Jend+1
          do i=IstrU-1,Iend
          Zeta_north4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          if (iif+irhot-1<=nfast) then
           Zeta_north7(IstrU-1:Iend,Jend:Jend+1)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=Jend,Jend+1
            do i=IstrU-1,Iend
             Zeta_north7(i,j) = Zeta_north7(i,j)
     &                         +cff1*Zeta_north4(i,j,iter)
            enddo
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=Jend,Jend+1
          do i=IstrU-1,Iend
          Zeta_north7(i,j)=Zeta_north7(i,j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*Zeta_north4(i,j,iif-1)
          enddo
          enddo
          enddo
          do j=Jend,Jend+1
          do i=IstrU-1,Iend
          Zeta_north7(i,j)=lastcff*(dZtinterp(i,j)-Zeta_north7(i,j))
          enddo
          enddo
          do j=Jend,Jend+1
          do i=IstrU-1,Iend
          Zeta_north6(i,j)=(Zeta_north7(i,j)-Zeta_north4(i,j,iif-1))
     &                           /rrhot
          enddo
          enddo
          endif
      endif
      if (.not.WEST_INTER) then
          do j=JstrV-1,Jend
          do i=Istr-1,Istr
          Zeta_west4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          if (iif+irhot-1<=nfast) then
           Zeta_west7(Istr-1:Istr,JstrV-1:Jend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=JstrV-1,Jend
            do i=Istr-1,Istr
             Zeta_west7(i,j) = Zeta_west7(i,j)
     &                         +cff1*Zeta_west4(i,j,iter)
            enddo
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=JstrV-1,Jend
          do i=Istr-1,Istr
          Zeta_west7(i,j)=Zeta_west7(i,j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*Zeta_west4(i,j,iif-1)
          enddo
          enddo
          enddo
          do j=JstrV-1,Jend
          do i=Istr-1,Istr
          Zeta_west7(i,j)=lastcff*(dZtinterp(i,j)-Zeta_west7(i,j))
          enddo
          enddo
          do j=JstrV-1,Jend
          do i=Istr-1,Istr
          Zeta_west6(i,j)=(Zeta_west7(i,j)-Zeta_west4(i,j,iif-1))
     &                           /rrhot
          enddo
          enddo
          endif
      endif
      if (.not.EAST_INTER) then
          do j=JstrV-1,Jend
          do i=Iend,Iend+1
          Zeta_east4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          if (iif+irhot-1<=nfast) then
           Zeta_east7(Iend:Iend+1,JstrV-1:Jend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=JstrV-1,Jend
            do i=Iend,Iend+1
             Zeta_east7(i,j) = Zeta_east7(i,j)
     &                         +cff1*Zeta_east4(i,j,iter)
            enddo
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=JstrV-1,Jend
          do i=Iend,Iend+1
          Zeta_east7(i,j)=Zeta_east7(i,j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*Zeta_east4(i,j,iif-1)
          enddo
          enddo
          enddo
          do j=JstrV-1,Jend
          do i=Iend,Iend+1
          Zeta_east7(i,j)=lastcff*(dZtinterp(i,j)-Zeta_east7(i,j))
          enddo
          enddo
          do j=JstrV-1,Jend
          do i=Iend,Iend+1
          Zeta_east6(i,j)=(Zeta_east7(i,j)-Zeta_east4(i,j,iif-1))
     &                           /rrhot
          enddo
          enddo
          endif
      endif
          endif
          if (.not.SOUTH_INTER) then
          do j=Jstr-1,Jstr
          do i=IstrU-1,Iend
          Zeta_south4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          do j=Jstr-1,Jstr
          do i=IstrU-1,Iend
          SSH(i,j)=Zeta_south4(i,j,iif-1)+Zeta_south6(i,j)
           enddo
          enddo
          endif
          if (.not.NORTH_INTER) then
          do j=Jend,Jend+1
          do i=IstrU-1,Iend
          Zeta_north4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          do j=Jend,Jend+1
          do i=IstrU-1,Iend
          SSH(i,j)=Zeta_north4(i,j,iif-1)+Zeta_north6(i,j)
          enddo
          enddo
          endif
          if (.not.WEST_INTER) then
          do j=JstrV-1,Jend
          do i=Istr-1,Istr
          Zeta_west4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          do j=JstrV-1,Jend
          do i=Istr-1,Istr
          SSH(i,j)=Zeta_west4(i,j,iif-1)+Zeta_west6(i,j)
          enddo
          enddo
          endif
          if (.not.EAST_INTER) then
          do j=JstrV-1,Jend
          do i=Iend,Iend+1
          Zeta_east4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          do j=JstrV-1,Jend
          do i=Iend,Iend+1
          SSH(i,j)=Zeta_east4(i,j,iif-1)+Zeta_east6(i,j)
          enddo
          enddo
          endif
      return
       
      

      end subroutine Sub_Loop_zetabc_interp_tile

