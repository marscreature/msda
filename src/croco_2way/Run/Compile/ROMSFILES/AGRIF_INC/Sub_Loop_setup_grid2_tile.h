      subroutine Sub_Loop_setup_grid2_tile(Istr,Iend,Jstr,Jend,padd_E,Mm
     &,padd_X,Lm,mynode,tile_count,bc_crss,volume,Cu_max,Cu_min,grdmax,g
     &rdmin,latmax,latmin,lonmax,lonmin,hmax,hmin,Mmmpi,Lmmpi,vmask,om_v
     &,umask,on_u,latr,lonr,dtfast,pn,pm,rmask,h,NORTH_INTER,SOUTH_INTER
     &,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars


      include 'mpif.h'
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: mynode
      integer(4) :: tile_count
      real(8) :: bc_crss
      real(8) :: volume
      real :: Cu_max
      real :: Cu_min
      real :: grdmax
      real :: grdmin
      real :: latmax
      real :: latmin
      real :: lonmax
      real :: lonmin
      real :: hmax
      real :: hmin
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real :: dtfast
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: latr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: lonr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                                               
      integer(4) :: i
      integer(4) :: j
      integer(4) :: NSUB
                                                                       
                                                                 
                                                                 
                              
      real :: cff
      real :: my_hmax
      real :: my_grdmax
      real :: my_Cu_max
      real :: my_hmin
      real :: my_grdmin
      real :: my_Cu_min
      real :: my_lonmin
      real :: my_lonmax
      real :: my_latmin
      real :: my_latmax
                                
      real(8) :: my_volume
      real(8) :: my_crss
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
!$AGRIF_DO_NOT_TREAT
      INTEGER*4 :: ocean_grid_comm
      common /cpl_comm/ ocean_grid_comm
!$AGRIF_END_DO_NOT_TREAT
                       
                                                          
      integer(4) :: size
      integer(4) :: step
      integer(4), dimension(1:MPI_STATUS_SIZE) :: status
      integer(4) :: ierr
                      
      real(8), dimension(1:14) :: buff
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      my_lonmin=+1.D+20
      my_lonmax=-1.D+20
      my_latmin=+1.D+20
      my_latmax=-1.D+20
      my_hmin=+1.D+20
      my_hmax=-1.D+20
      my_grdmin=+1.D+20
      my_grdmax=-1.D+20
      my_Cu_min=+1.D+20
      my_Cu_max=-1.D+20
      do j=JstrR,JendR
        do i=IstrR,IendR
            my_hmin=min(my_hmin, h(i,j))
            my_hmax=max(my_hmax, h(i,j))
          if (rmask(i,j).gt.0.D0) then
            cff=1.D0/sqrt(pm(i,j)*pn(i,j))
            my_grdmin=min(my_grdmin, cff)
            my_grdmax=max(my_grdmax, cff)
            cff=dtfast*sqrt( g*max(h(i,j),0.D0)*( pm(i,j)*pm(i,j)
     &                                 +pn(i,j)*pn(i,j) ))
            my_Cu_min=min(my_Cu_min, cff)
            my_Cu_max=max(my_Cu_max, cff)
          endif
            my_lonmin=min(my_lonmin, lonr(i,j))
            my_lonmax=max(my_lonmax, lonr(i,j))
            my_latmin=min(my_latmin, latr(i,j))
            my_latmax=max(my_latmax, latr(i,j))
        enddo
      enddo
      my_volume=0.D0
      do j=Jstr,Jend
        do i=Istr,Iend
          my_volume=my_volume+h(i,j)/(pm(i,j)*pn(i,j))
        enddo
      enddo
      my_crss=0.D0
      if (.not.WEST_INTER) then
        do j=Jstr,Jend
          my_crss=my_crss+0.5D0*(h(Istr-1,j)+h(Istr,j))*on_u(Istr,j)
     &                                                *umask(Istr,j)
        enddo
      endif
      if (.not.EAST_INTER) then
        do j=Jstr,Jend
          my_crss=my_crss+0.5D0*(h(Iend,j)+h(Iend+1,j))*on_u(Iend+1,j)
     &                                              *umask(Iend+1,j)
        enddo
      endif
      if (.not.SOUTH_INTER) then
        do i=Istr,Iend
          my_crss=my_crss+0.5D0*(h(i,Jstr)+h(i,Jstr-1))*om_v(i,Jstr)
     &                                               *vmask(i,Jstr)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=Istr,Iend
          my_crss=my_crss+0.5D0*(h(i,Jend)+h(i,Jend+1))*om_v(i,Jend+1)
     &                                              *vmask(i,Jend+1)
        enddo
      endif
      if (Iend-Istr+Jend-Jstr.eq.Lmmpi+Mmmpi-2) then
        NSUB=1
      else
        NSUB=NSUB_X*NSUB_E
      endif
C$OMP CRITICAL (grd2_cr_rgn)
        hmin=min(hmin, my_hmin)
        hmax=max(hmax, my_hmax)
        lonmin=min(lonmin, my_lonmin)
        lonmax=max(lonmax, my_lonmax)
        latmin=min(latmin, my_latmin)
        latmax=max(latmax, my_latmax)
        grdmin=min(grdmin, my_grdmin)
        grdmax=max(grdmax, my_grdmax)
        Cu_min=min(Cu_min, my_Cu_min)
        Cu_max=max(Cu_max, my_Cu_max)
        volume=volume+my_volume
        bc_crss=bc_crss+my_crss
        tile_count=tile_count+1
        if (tile_count.eq.NSUB) then
          tile_count=0
          size=NNODES
  1        step=(size+1)/2
            if (mynode.ge.step .and. mynode.lt.size) then
              buff(1)=hmin
              buff(2)=hmax
              buff(3)=grdmin
              buff(4)=grdmax
              buff(5)=Cu_min
              buff(6)=Cu_max
              buff(7)=lonmin
              buff(8)=lonmax
              buff(9)=latmin
              buff(10)=latmax
              buff(11)=volume
              buff(12)=bc_crss
              call MPI_Send (buff,  14, MPI_DOUBLE_PRECISION,
     &             mynode-step, 17, MPI_COMM_WORLD,      ierr)
            elseif (mynode .lt. size-step) then
              call MPI_Recv (buff,  14, MPI_DOUBLE_PRECISION,
     &             mynode+step, 17, MPI_COMM_WORLD, status, ierr)
              hmin=  min(hmin,   buff(1))
              hmax=  max(hmax,   buff(2))
              grdmin=min(grdmin, buff(3))
              grdmax=max(grdmax, buff(4))
              Cu_min=min(Cu_min, buff(5))
              Cu_max=max(Cu_max, buff(6))
              lonmin=min(lonmin,buff(7))
              lonmax=max(lonmax,buff(8))
              latmin=min(latmin,buff(9))
              latmax=max(latmax,buff(10))
              volume=volume + buff(11)
               bc_crss=bc_crss + buff(12)
            endif
           size=step
          if (size.gt.1) goto 1
          buff(1)=hmin
          buff(2)=hmax
          buff(3)=grdmin
          buff(4)=grdmax
          buff(5)=Cu_min
          buff(6)=Cu_max
          buff(7)=lonmin
          buff(8)=lonmax
          buff(9)=latmin
          buff(10)=latmax
          buff(11)=volume
          buff(12)=bc_crss
          call MPI_Bcast(buff, 14, MPI_DOUBLE_PRECISION,
     &                          0, MPI_COMM_WORLD, ierr)
          hmin=  buff(1)
          hmax=  buff(2)
          grdmin=buff(3)
          grdmax=buff(4)
          Cu_min=buff(5)
          Cu_max=buff(6)
          lonmin=buff(7)
          lonmax=buff(8)
          latmin=buff(9)
          latmax=buff(10)
          volume=buff(11)
          bc_crss=buff(12)
          if (mynode.eq.0) write( stdout,
     &        '(/1x,A,8x,A,9x,A,9x,A,9x,A,6x,A)')
     &        'hmin','hmax','grdmin','grdmax','Cu_min','Cu_max'
          if (mynode.eq.0) write( stdout,
     &        '(F12.6,F13.6,2(1x,E14.9),2F12.8)')
     &         hmin,  hmax,  grdmin,  grdmax,  Cu_min,  Cu_max
          if (mynode.eq.0) write( stdout,'(2(3x,A,1PE27.21)/)')
     &        'volume=', volume, 'open_cross=', bc_crss
          if (mynode.eq.0) write(stdout,
     &        '(5x,A,F6.2,A,F6.2,A,F6.2,A,F6.2/)')
     &        '  lonmin = ', lonmin, '  lonmax = ', lonmax,
     &        '  latmin = ', latmin, '  latmax = ', latmax
        endif
C$OMP END CRITICAL (grd2_cr_rgn)
      return
       
      

      end subroutine Sub_Loop_setup_grid2_tile

