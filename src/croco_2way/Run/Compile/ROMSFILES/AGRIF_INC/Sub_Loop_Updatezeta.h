      subroutine Sub_Loop_Updatezeta(tabres,i1,i2,j1,j2,k1,k2,before,nb,
     &ndir,padd_E,Mm,padd_X,Lm,knew,weight2,kstp,zeta,indupdate,coarseva
     &lues,nfast,finevalues,nbstep3d,rmask,Zt_avg3,iif,Zt_avg1,Mmmpi,Lmm
     &pi)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: kstp
      integer(4) :: indupdate
      integer(4) :: nfast
      integer(4) :: nbstep3d
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, allocatable, dimension(:,:) :: coarsevalues
      real, allocatable, dimension(:,:) :: finevalues
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: Zt_avg
     &3
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                  
                                     
                     
                         
               
      real :: hzu
                    
      real :: t1
      real :: t2
      real :: t3
                    
      integer(4) :: i
      integer(4) :: j
                  
      real :: t4
      real :: t8
                                   
      integer(4) :: iter
      integer(4) :: iifparent
                                
      integer(4) :: oldindupdate
                              
      real :: cff
      real :: cff1
      real :: cff2
                                                                       
                                                                 
 
      logical :: western_side
      logical :: eastern_side
      logical :: northern_side
      logical :: southern_side
!$AGRIF_DO_NOT_TREAT
      logical global_update_2d
      common/globalupdate/global_update_2d
      integer*4 :: iiffine, irhotfine, nbgrid
      common/updateubar2val/iiffine,irhotfine,
     &   nbgrid
!$AGRIF_END_DO_NOT_TREAT
                                     
      integer(4) :: irhot
      integer(4) :: ibegin
      integer(4) :: isize
                                 
      real, dimension(i1:i2,j1:j2) :: tabtemp
                             
      integer(4) :: n1
      integer(4) :: n2
      integer(4) :: n3
      IF (before) THEN
         IF (global_update_2d) THEN
           tabres(i1:i2,j1:j2,1) = Zt_avg1(i1:i2,j1:j2)
           RETURN
         ENDIF
         irhot=Agrif_Irhot()
         western_side  = (nb == 1).AND.(ndir == 1)
         eastern_side  = (nb == 1).AND.(ndir == 2)
         southern_side = (nb == 2).AND.(ndir == 1)
         northern_side = (nb == 2).AND.(ndir == 2)
         ibegin = min(irhot,iif+1)
         IF (iif .LE. irhot) THEN
            ibegin = iif + 1
         ENDIF
          do iter=1,ibegin
          do j=j1,j2
          do i=i1,i2
          tabres(i,j,iter) = Zt_avg3(i,j,iif-iter+1)
          enddo
          enddo
          enddo
       ELSE
       IF (global_update_2d) THEN
         Zt_avg1(i1:i2,j1:j2) = tabres(i1:i2,j1:j2,1)
     &             *rmask(i1:i2,j1:j2)
         RETURN
       ENDIF
       ibegin = min(irhotfine,iiffine+1)
       IF (iiffine .LE. irhotfine) THEN
        ibegin = iiffine + 1
       ENDIF
       isize = (j2-j1+1)*(i2-i1+1)
       IF ((nbstep3d == 0).AND.(iif == 1)) THEN
       IF (.NOT.allocated(finevalues)) THEN
       Allocate(finevalues(isize,0:nfast))
       Allocate(coarsevalues(isize,0:nfast))
       ELSE
       CALL checksize(indupdate+isize)
       ENDIF
       ENDIF
          do iter=1,ibegin
          oldindupdate = indupdate
          do j=j1,j2
          do i=i1,i2
          oldindupdate = oldindupdate + 1
          finevalues(oldindupdate,iiffine-iter+1)
     &         = tabres(i,j,iter)
          enddo
          enddo
          enddo
          IF (iif == 1) THEN
          oldindupdate = indupdate
          do j=j1,j2
          do i=i1,i2
          oldindupdate = oldindupdate + 1
          coarsevalues(oldindupdate,0) = zeta(i,j,kstp)
          enddo
          enddo
          ENDIF
         tabtemp = 0.D0
         do iter=0,iif-1
         cff = -weight2(iif,iter)
         call copy1d(tabtemp,coarsevalues(indupdate+1,iter),
     &               cff,isize)
         enddo
         do iter=0,iiffine
         cff=weight2(iiffine,iter)
         call copy1d(tabtemp,finevalues(indupdate+1,iter),
     &               cff,isize)
         enddo
         tabtemp = tabtemp/weight2(iif,iif)
         DO j=j1,j2
           DO i=i1,i2
           t2 = tabtemp(i,j)
     &           * rmask(i,j)
          zeta(i,j,knew) = t2
          indupdate = indupdate + 1
          coarsevalues(indupdate,iif) = tabtemp(i,j)
          ENDDO
         ENDDO
       ENDIF
      return
       
      

      end subroutine Sub_Loop_Updatezeta

