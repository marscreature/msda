      integer(4), parameter :: NWEIGHT = 1000
      interface
        subroutine Sub_Loop_v2dbc_interp_tile(Istr,Iend,Jstr,Jend,DV_wes
     &t4,DV_east4,DV_west6,DV_east6,DV_south4,DV_north4,DV_south6,DV_nor
     &th6,padd_E,Mm,padd_X,Lm,om_v,h,knew,zeta,SSH,on_v,vbclm,dtfast,vma
     &sk,nfast,DV_avg3,weight2,V2DTimeindex2,vbarid,common_index,dVinter
     &p,V2DTimeindex,iif,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,M
     &mmpi,Lmmpi)
          use AGRIF_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      real :: dtfast
      integer(4) :: nfast
      integer(4) :: V2DTimeindex2
      integer(4) :: vbarid
      integer(4) :: common_index
      integer(4) :: V2DTimeindex
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: SSH
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: DV_avg
     &3
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dVinterp
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(-1:Mm+2+padd_E) :: DV_west6
      real, dimension(-1:Mm+2+padd_E) :: DV_east6
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DV_west4
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DV_east4
      real, dimension(-1:Lm+2+padd_X) :: DV_south6
      real, dimension(-1:Lm+2+padd_X) :: DV_north6
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DV_south4
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DV_north4
        end subroutine Sub_Loop_v2dbc_interp_tile

      end interface
