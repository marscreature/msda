      real Tperiod(Ntides)
      common /tides_Tperiod/ Tperiod
      real SSH_Tamp(-1:Lm+2+padd_X,-1:Mm+2+padd_E,Ntides)
      common /tides_SSH_Tamp/ SSH_Tamp
      real SSH_Tphase(-1:Lm+2+padd_X,-1:Mm+2+padd_E,Ntides)
      common /tides_SSH_Tphase/ SSH_Tphase
      real UV_Tangle(-1:Lm+2+padd_X,-1:Mm+2+padd_E,Ntides)
      common /tides_UV_Tangle/ UV_Tangle
      real UV_Tmajor(-1:Lm+2+padd_X,-1:Mm+2+padd_E,Ntides)
      common /tides_UV_Tmajor/ UV_Tmajor
      real UV_Tminor(-1:Lm+2+padd_X,-1:Mm+2+padd_E,Ntides)
      common /tides_UV_Tminor/ UV_Tminor
      real UV_Tphase(-1:Lm+2+padd_X,-1:Mm+2+padd_E,Ntides)
      common /tides_UV_Tphase/ UV_Tphase
