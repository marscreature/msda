      real ssh(-1:Lm+2+padd_X,-1:Mm+2+padd_E)
      common /climat_ssh/ssh
      real Znudgcof(-1:Lm+2+padd_X,-1:Mm+2+padd_E)
      common /climat_Znudgcof/Znudgcof
      real sshg(-1:Lm+2+padd_X,-1:Mm+2+padd_E,2)
      common /climat_sshg/sshg
      real ssh_time(2)
      real ssh_cycle
      integer itssh, ssh_ncycle, ssh_rec, ssh_tid, ssh_id
      common /climat_zdat1/ ssh_time
      common /climat_zdat2/ ssh_cycle
      common /climat_zdat3/
     & itssh, ssh_ncycle, ssh_rec, ssh_tid, ssh_id
      real tclm(-1:Lm+2+padd_X,-1:Mm+2+padd_E,N,NT)
      common /climat_tclm/tclm
      real Tnudgcof(-1:Lm+2+padd_X,-1:Mm+2+padd_E,N,NT)
      common /climat_Tnudgcof/Tnudgcof
      real tclima(-1:Lm+2+padd_X,-1:Mm+2+padd_E,N,2,NT)
      common /climat_tclima/tclima
      real tclm_time(2,NT)
      real tclm_cycle(NT)
      integer ittclm(NT), tclm_ncycle(NT), tclm_rec(NT),
     & tclm_tid(NT), tclm_id(NT)
      logical got_tclm(NT)
      common /climat_tdat/ tclm_time, tclm_cycle,
     & ittclm, tclm_ncycle, tclm_rec,
     & tclm_tid, tclm_id,
     & got_tclm
      real ubclm(-1:Lm+2+padd_X,-1:Mm+2+padd_E)
      real vbclm(-1:Lm+2+padd_X,-1:Mm+2+padd_E)
      common /climat_ubclm/ubclm /climat_vbclm/vbclm
      real uclm(-1:Lm+2+padd_X,-1:Mm+2+padd_E,N)
      real vclm(-1:Lm+2+padd_X,-1:Mm+2+padd_E,N)
      common /climat_uclm/uclm /climat_vclm/vclm
      real M2nudgcof(-1:Lm+2+padd_X,-1:Mm+2+padd_E)
      common /climat_M2nudgcof/M2nudgcof
      real ubclima(-1:Lm+2+padd_X,-1:Mm+2+padd_E,2)
      real vbclima(-1:Lm+2+padd_X,-1:Mm+2+padd_E,2)
      common /climat_ubclima/ubclima /climat_vbclima/vbclima
      real M3nudgcof(-1:Lm+2+padd_X,-1:Mm+2+padd_E)
      common /climat_M3nudgcof/M3nudgcof
      real uclima(-1:Lm+2+padd_X,-1:Mm+2+padd_E,N,2)
      real vclima(-1:Lm+2+padd_X,-1:Mm+2+padd_E,N,2)
      common /climat_uclima/uclima /climat_vclima/vclima
      real uclm_time(2)
      real uclm_cycle
      integer ituclm, uclm_ncycle, uclm_rec, uclm_tid,
     & ubclm_id, vbclm_id, uclm_id, vclm_id
      common /climat_udat1/ uclm_time
      common /climat_udat2/ uclm_cycle
      common /climat_udat3/
     & ituclm, uclm_ncycle, uclm_rec,
     & uclm_tid, ubclm_id, vbclm_id,
     & uclm_id, vclm_id
