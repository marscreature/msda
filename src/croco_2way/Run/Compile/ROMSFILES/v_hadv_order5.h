          if (SOUTH_INTER) then
            jmin=0
          else
            jmin=3
          endif
          if (NORTH_INTER) then
            jmax=Mmmpi+1
          else
            jmax=Mmmpi-1
          endif
          if (WEST_INTER) then
            imin=1
          else
            imin=3
          endif
          if (EAST_INTER) then
            imax=Lmmpi+1
          else
            imax=Lmmpi-1
          endif
              DO i = Istr,Iend
                vel = flux6(Hvom(i,j-2,k),Hvom(i,j-1,k),Hvom(i,j ,k),
     & Hvom(i,j+1,k),Hvom(i,j+2,k),Hvom(i,j+3,k),1.)
     & v(i,j-2,k,nrhs), v(i,j-1,k,nrhs),
     & v(i,j ,k,nrhs), v(i,j+1,k,nrhs),
     & v(i,j+2,k,nrhs), v(i,j+3,k,nrhs), vel )
                vel = flux4(Hvom(i,j-1,k),Hvom(i,j ,k),
     & Hvom(i,j+1,k),Hvom(i,j+2,k),1.)
     & v(i,j-1,k,nrhs), v(i,j ,k,nrhs),
     & v(i,j+1,k,nrhs), v(i,j+2,k,nrhs), vel )
     & v(i,j ,k,nrhs), v(i,j+1,k,nrhs), vel, cdif)
              ENDDO
              DO i = Istr,Iend
     & v(i,j,k,nrhs), v(i,j+1,k,nrhs), vel, cdif)
              ENDDO
              DO i = Istr,Iend
                vel = flux4(Hvom(i,j-1,k),Hvom(i,j,k),
     & Hvom(i,j+1,k),Hvom(i,j+2,k),1.)
     & v(i,j-1,k,nrhs), v(i,j ,k,nrhs),
     & v(i,j+1,k,nrhs), v(i,j+2,k,nrhs), vel )
     & v(i,j ,k,nrhs), v(i,j+1,k,nrhs), vel, cdif)
              ENDDO
              DO i = Istr,Iend
     & v(i,j,k,nrhs), v(i,j+1,k,nrhs), vel, cdif)
              ENDDO
              DO i = Istr,Iend
                vel = flux4(Hvom(i,j-1,k),Hvom(i,j ,k),
     & Hvom(i,j+1,k),Hvom(i,j+2,k),1.)
     & v(i,j-1,k,nrhs), v(i,j ,k,nrhs),
     & v(i,j+1,k,nrhs), v(i,j+2,k,nrhs), vel )
     & v(i,j ,k,nrhs), v(i,j+1,k,nrhs), vel, cdif)
              ENDDO
            ENDIF
              DO j = JstrV,Jend
                if ( j.ge.jmin .and. j.le.jmax ) then
                  vel = flux6(Huon(i,j-3,k),Huon(i,j-2,k),Huon(i,j-1,k),
     & Huon(i,j ,k),Huon(i,j+1,k),Huon(i,j+2,k),1.)
                else
                endif
     & v(i-3,j,k,nrhs), v(i-2,j,k,nrhs),
     & v(i-1,j,k,nrhs), v(i ,j,k,nrhs),
     & v(i+1,j,k,nrhs), v(i+2,j,k,nrhs), vel )
     & v(i-2,j,k,nrhs), v(i-1,j,k,nrhs),
     & v(i ,j,k,nrhs), v(i+1,j,k,nrhs), vel )
     & v(i-1,j,k,nrhs), v(i ,j,k,nrhs), vel, cdif)
              ENDDO
              DO j = JstrV,Jend
     & v(i-1,j,k,nrhs), v(i,j,k,nrhs), vel, cdif)
              ENDDO
              DO j = JstrV,Jend
     & v(i-2,j,k,nrhs), v(i-1,j,k,nrhs),
     & v(i ,j,k,nrhs), v(i+1,j,k,nrhs), vel )
     & v(i-1,j,k,nrhs), v(i ,j,k,nrhs), vel, cdif)
              ENDDO
              DO j = JstrV,Jend
     & v(i-1,j,k,nrhs), v(i,j,k,nrhs), vel, cdif)
              ENDDO
              DO j = JstrV,Jend
     & v(i-2,j,k,nrhs), v(i-1,j,k,nrhs),
     & v(i ,j,k,nrhs), v(i+1,j,k,nrhs), vel )
     & v(i-1,j,k,nrhs), v(i,j,k,nrhs), vel, cdif)
              ENDDO
            ENDIF
