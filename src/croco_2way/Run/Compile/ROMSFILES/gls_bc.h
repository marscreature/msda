      if (.not.WEST_INTER) then
        do k=1,N
          do j=jstr,jend+1
            grad(istr-1,j)=( tmp(istr-1,j ,k,nstp)
     & -tmp(istr-1,j-1,k,nstp))
            grad(istr ,j)=( tmp(istr ,j ,k,nstp)
     & -tmp(istr ,j-1,k,nstp))
          enddo
          do j=jstr,jend
            if (cx.gt.0.) then
              tau=0.
            else
              tau=-cx
              cx=0.
            endif
     & )
     & )
            tmp(istr-1,j,k,nnew)=tmp(istr-1,j,k,nnew)
          enddo
        enddo
      if (.not.EAST_INTER) then
          do j=jstr,jend+1
           grad(iend ,j)=( tmp(iend ,j ,k,nstp)
     & -tmp(iend ,j-1,k,nstp))
           grad(iend+1,j)=( tmp(iend+1,j ,k,nstp)
     & -tmp(iend+1,j-1,k,nstp))
          enddo
          do j=jstr,jend
            if (cx.gt.0.) then
              tau=0.
            else
              tau=-cx
              cx=0.
            endif
     & )
     & )
            tmp(iend+1,j,k,nnew)=tmp(iend+1,j,k,nnew)
          enddo
        enddo
      if (.not.SOUTH_INTER) then
        do k=1,N
          do i=istr,iend+1
            grad(i,jstr )=( tmp(i ,jstr ,k,nstp)
     & -tmp(i-1,jstr ,k,nstp))
            grad(i,jstr-1)=( tmp(i ,jstr-1,k,nstp)
     & -tmp(i-1,jstr-1,k,nstp))
          enddo
          do i=istr,iend
            if (cx.gt.0.) then
              tau=0.
            else
              tau=-cx
              cx=0.
            endif
     & )
     & )
            tmp(i,jstr-1,k,nnew)=tmp(i,jstr-1,k,nnew)
          enddo
        enddo
      if (.not.NORTH_INTER) then
        do k=1,N
          do i=istr,iend+1
            grad(i,jend )=( tmp(i ,jend ,k,nstp)
     & -tmp(i-1,jend ,k,nstp))
            grad(i,jend+1)=( tmp(i ,jend+1,k,nstp)
     & -tmp(i-1,jend+1,k,nstp))
          enddo
          do i=istr,iend
            if (cx.gt.0.) then
              tau=0.
            else
              tau=-cx
              cx=0.
            endif
     & )
     & )
            tmp(i,jend+1,k,nnew)=tmp(i,jend+1,k,nnew)
          enddo
        enddo
      if (.not.SOUTH_INTER .and. .not.WEST_INTER) then
        cff=rmask(istr,jstr-1)+rmask(istr-1,jstr)
        if (cff.gt.0.) then
          cff=1./cff
          do k=1,N
          enddo
        else
          do k=1,N
            tmp(istr-1,jstr-1,k,nnew)=0.
          enddo
        endif
      endif
      if (.not.SOUTH_INTER .and. .not.EAST_INTER) then
        cff=rmask(iend,jstr-1)+rmask(iend+1,jstr)
        if (cff.gt.0.) then
          cff=1./cff
          do k=1,N
          enddo
        else
          do k=1,N
            tmp(iend+1,jstr-1,k,nnew)=0.
          enddo
        endif
      endif
      if (.not.NORTH_INTER .and. .not.WEST_INTER) then
        cff=rmask(istr,jend+1)+rmask(istr-1,jend)
        if (cff.gt.0.) then
          cff=1./cff
          do k=1,N
          enddo
        else
          do k=1,N
            tmp(istr-1,jend+1,k,nnew)=0.
          enddo
        endif
      endif
      if (.not.NORTH_INTER .and. .not.EAST_INTER) then
        cff=rmask(iend,jend+1)+rmask(iend+1,jend)
        if (cff.gt.0.) then
          cff=1./cff
          do k=1,N
          enddo
        else
          do k=1,N
            tmp(iend+1,jend+1,k,nnew)=0.
          enddo
        endif
      endif
