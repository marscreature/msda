      real dt, dtfast, time, time2, time_start, tdays
      integer ndtfast, iic, kstp, krhs, knew, next_kstp
      integer numthreads, ntstart, ntimes, ninfo, &
              nfast, nrrec, nrst, nwrt
      integer iif, nstp, nrhs, nnew, nbstep3d
      logical PREDICTOR_2D_STEP
      common /time_indices/ dt,dtfast, time, time2,time_start, tdays, &
                             ndtfast, iic, kstp, krhs, knew, next_kstp, &
                             iif, nstp, nrhs, nnew, nbstep3d, &
                             PREDICTOR_2D_STEP
      real time_avg, time2_avg, rho0, &
     & rdrg, rdrg2, Cdb_min, Cdb_max, Zob, &
     & xl, el, visc2, visc4, gamma2
      real theta_s, theta_b, Tcline, hc
      real sc_w(0:N), Cs_w(0:N), sc_r(N), Cs_r(N)
      real rx0, rx1
      real tnu2(NT),tnu4(NT)
      real weight(6,0:NWEIGHT)
      real x_sponge, v_sponge
       real tauT_in, tauT_out, tauM_in, tauM_out
      integer ntsavg, navg
      logical ldefhis
      logical got_tini(NT)
      common /scalars_main/ &
     & time_avg, time2_avg, rho0, rdrg, rdrg2 &
     & , Zob, Cdb_min, Cdb_max &
     & , xl, el, visc2, visc4, gamma2 &
     & , theta_s, theta_b, Tcline, hc &
     & , sc_w, Cs_w, sc_r, Cs_r &
     & , rx0, rx1, tnu2, tnu4 &
     & , weight &
     & , x_sponge, v_sponge &
     & , tauT_in, tauT_out, tauM_in, tauM_out &
     & , numthreads, ntstart, ntimes, ninfo &
     & , nfast, nrrec, nrst, nwrt &
     & , ntsavg, navg &
     & , got_tini &
     & , ldefhis
      logical EAST_INTER, WEST_INTER, NORTH_INTER, SOUTH_INTER
      integer mynode, ii,jj, p_W,p_E,p_S,p_N, p_SW,p_SE, p_NW,p_NE
      common /comm_setup/ mynode, ii,jj, p_W,p_E,p_S,p_N, p_SW,p_SE, &
        p_NW,p_NE, EAST_INTER, WEST_INTER, NORTH_INTER, SOUTH_INTER
      real pi, deg2rad, rad2deg
      parameter (pi=3.14159265358979323846, deg2rad=pi/180., &
                                            rad2deg=180./pi)
      real Eradius, g, day2sec,sec2day, jul_off, &
           year2day,day2year
      parameter (Eradius=6371315.0, day2sec=86400., &
                 sec2day=1./86400., jul_off=2440000., &
                 year2day=365.25, day2year=1./365.25)
      parameter (g=9.81)
      real Cp
      parameter (Cp=3985.0)
