        do k=3,N-3
          do i=Istr,Iend
     & FLUX5(
     & v(i,j,k-2,nrhs), v(i,j,k-1,nrhs),
     & v(i,j,k ,nrhs), v(i,j,k+1,nrhs),
     & v(i,j,k+2,nrhs), v(i,j,k+3,nrhs), vel)
          enddo
        enddo
        do i=Istr,Iend
     & FLUX3(
     & v(i,j,1,nrhs), v(i,j,2,nrhs),
     & v(i,j,3,nrhs), v(i,j,4,nrhs), vel)
     & FLUX3(
     & v(i,j,N-3,nrhs), v(i,j,N-2,nrhs),
     & v(i,j,N-1,nrhs), v(i,j,N ,nrhs), vel)
          FC(i,1)=FLUX2(
     & v(i,j,1,nrhs), v(i,j,2,nrhs), vel, cdif)
          FC(i,N-1)=FLUX2(
     & v(i,j,N-1,nrhs), v(i,j,N,nrhs), vel, cdif)
          FC(i,0)=0.
          FC(i,N)=0.
        enddo
