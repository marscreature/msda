          if (SOUTH_INTER) then
            jmin=1
          else
            jmin=3
          endif
          if (NORTH_INTER) then
            jmax=Mmmpi+1
          else
            jmax=Mmmpi-1
          endif
          if (WEST_INTER) then
            imin=1
          else
            imin=3
          endif
          if (EAST_INTER) then
            imax=Lmmpi+1
          else
            imax=Lmmpi-1
          endif
              DO i = Istr,Iend
                vel = Hvom(i,j,k)
     & t(i,j-3,k,nrhs,itrc), t(i,j-2,k,nrhs,itrc),
     & t(i,j-1,k,nrhs,itrc), t(i,j ,k,nrhs,itrc),
     & t(i,j+1,k,nrhs,itrc), t(i,j+2,k,nrhs,itrc), vel )
     & t(i,j-2,k,nrhs,itrc), t(i,j-1,k,nrhs,itrc),
     & t(i,j ,k,nrhs,itrc), t(i,j+1,k,nrhs,itrc), vel )
     & t(i,j-1,k,nrhs,itrc), t(i,j,k,nrhs,itrc), vel, cdif)
              ENDDO
              DO i = Istr,Iend
                vel = Hvom(i,j,k)
     & t(i,j-1,k,nrhs,itrc), t(i,j,k,nrhs,itrc), vel, cdif)
              ENDDO
              DO i = Istr,Iend
                vel = Hvom(i,j,k)
     & t(i,j-2,k,nrhs,itrc), t(i,j-1,k,nrhs,itrc),
     & t(i,j ,k,nrhs,itrc), t(i,j+1,k,nrhs,itrc), vel )
     & t(i,j-1,k,nrhs,itrc), t(i,j,k,nrhs,itrc), vel, cdif)
              ENDDO
              DO i = Istr,Iend
                vel = Hvom(i,j,k)
     & t(i,j-1,k,nrhs,itrc), t(i,j,k,nrhs,itrc), vel, cdif)
              ENDDO
              DO i = Istr,Iend
                vel = Hvom(i,j,k)
     & t(i,j-2,k,nrhs,itrc), t(i,j-1,k,nrhs,itrc),
     & t(i,j ,k,nrhs,itrc), t(i,j+1,k,nrhs,itrc), vel )
     & t(i,j-1,k,nrhs,itrc), t(i,j,k,nrhs,itrc), vel, cdif)
              ENDDO
            ENDIF
              DO j = Jstr,Jend
                vel = Huon(i,j,k)
     & t(i-3,j,k,nrhs,itrc), t(i-2,j,k,nrhs,itrc),
     & t(i-1,j,k,nrhs,itrc), t(i ,j,k,nrhs,itrc),
     & t(i+1,j,k,nrhs,itrc), t(i+2,j,k,nrhs,itrc), vel )
     & t(i-2,j,k,nrhs,itrc), t(i-1,j,k,nrhs,itrc),
     & t(i ,j,k,nrhs,itrc), t(i+1,j,k,nrhs,itrc), vel )
     & t(i-1,j,k,nrhs,itrc), t(i,j,k,nrhs,itrc), vel, cdif)
              ENDDO
              DO j = Jstr,Jend
                vel = Huon(i,j,k)
     & t(i-1,j,k,nrhs,itrc), t(i,j,k,nrhs,itrc), vel, cdif)
              ENDDO
              DO j = Jstr,Jend
                vel = Huon(i,j,k)
     & t(i-2,j,k,nrhs,itrc), t(i-1,j,k,nrhs,itrc),
     & t(i ,j,k,nrhs,itrc), t(i+1,j,k,nrhs,itrc), vel )
     & t(i-1,j,k,nrhs,itrc), t(i,j,k,nrhs,itrc), vel, cdif)
              ENDDO
              DO j = Jstr,Jend
                vel = Huon(i,j,k)
     & t(i-1,j,k,nrhs,itrc), t(i,j,k,nrhs,itrc), vel, cdif)
              ENDDO
              DO j = Jstr,Jend
                vel = Huon(i,j,k)
     & t(i-2,j,k,nrhs,itrc), t(i-1,j,k,nrhs,itrc),
     & t(i ,j,k,nrhs,itrc), t(i+1,j,k,nrhs,itrc), vel )
     & t(i-1,j,k,nrhs,itrc), t(i,j,k,nrhs,itrc), vel, cdif)
              ENDDO
            ENDIF
