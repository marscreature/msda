      integer LLm,Lm,MMm,Mm,N, LLm0,MMm0
      integer LLmm2, MMmm2
      common /scrum_physical_grid/ LLm,Lm,LLmm2,MMm,Mm,MMmm2
      integer Lmmpi,Mmmpi,iminmpi,imaxmpi,jminmpi,jmaxmpi
      common /comm_setup_mpi1/ Lmmpi,Mmmpi
      common /comm_setup_mpi2/ iminmpi,imaxmpi,jminmpi,jmaxmpi
      integer NSUB_X, NSUB_E, NPP
      integer NP_XI, NP_ETA, NNODES
      parameter (NPP=1)
      parameter (NSUB_X=1, NSUB_E=1)
      integer NWEIGHT
      parameter (NWEIGHT=1000)
      parameter (Ntides=10)
      integer stdout, Np, padd_X,padd_E
      common/scrum_deriv_param/padd_X,padd_E
      parameter (stdout=6, Np=N+1)
      integer NSA, N2d,N3d,N1dXI,N1dETA
      parameter (NSA=28)
      common /scrum_private_param/ N2d,N3d,N1dXI,N1dETA
      real Vtransform
      parameter (Vtransform=1)
      integer NT, itemp
      integer ntrc_salt, ntrc_pas, ntrc_bio, ntrc_sed
      parameter (itemp=1)
      parameter (ntrc_salt=1)
      parameter (ntrc_pas=0)
      parameter (ntrc_bio=0)
      parameter (ntrc_sed=0)
      parameter (NT=itemp+ntrc_salt+ntrc_pas+ntrc_bio+ntrc_sed)
