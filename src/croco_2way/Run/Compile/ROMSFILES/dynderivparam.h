      Lm=(LLm+NP_XI-1)/NP_XI; Mm=(MMm+NP_ETA-1)/NP_ETA
      padd_X=(Lm+2)/2-(Lm+1)/2; padd_E=(Mm+2)/2-(Mm+1)/2
      xi_rho=LLm+2; xi_u=xi_rho-1
      eta_rho=MMm+2; eta_v=eta_rho-1
      size_XI=7+(Lm+NSUB_X-1)/NSUB_X
      size_ETA=7+(Mm+NSUB_E-1)/NSUB_E
      sse=size_ETA/Np; ssz=Np/size_ETA
      se=sse/(sse+ssz); sz=1-se
      N1dXI = size_XI
      N1dETA = size_ETA
