        do k=3,N-3
          do i=IstrU,Iend
     & FLUX5(
     & u(i,j,k-2,nrhs), u(i,j,k-1,nrhs),
     & u(i,j,k ,nrhs), u(i,j,k+1,nrhs),
     & u(i,j,k+2,nrhs), u(i,j,k+3,nrhs), vel)
          enddo
        enddo
        do i=IstrU,Iend
     & FLUX3(
     & u(i,j,1,nrhs), u(i,j,2,nrhs),
     & u(i,j,3,nrhs), u(i,j,4,nrhs), vel)
     & FLUX3(
     & u(i,j,N-3,nrhs), u(i,j,N-2,nrhs),
     & u(i,j,N-1,nrhs), u(i,j,N ,nrhs), vel)
          FC(i,1)=FLUX2(
     & u(i,j,1,nrhs), u(i,j,2,nrhs), vel, cdif)
          FC(i,N-1)=FLUX2(
     & u(i,j,N-1,nrhs), u(i,j,N,nrhs), vel, cdif)
          FC(i,0)=0.
          FC(i,N)=0.
        enddo
