      real kwater, kChla, palpha1, palpha2,
     & abio1, abio2, bbio, cbio,
     & K_psi, K1_NO32, K2_NO32, K1_NH4, K2_NH4,
     & epsilon1, epsilon2, K_NH4_NO2, K_NO2_NO3,
     & O2nf, mu_P1_Sd, mu_P2_Sd, gmax1, gmax2,
     & K_Zoo1, K_Zoo2, beta1, beta2,
     & e_11, e_12, e_21, e_22, e_ZZ,
     & gamma_Z1_ADON, gamma_Z2_ADON, mu_Z1_Sd, mu_Z2_Ld,
     & f2_Z1_NH4, f2_Z2_NH4, O2ox, Kox, Ktox,
     & K_Sd_NH4, K_Ld_NH4, K_DON_NH4,
     & O2denitr, O2anam, aNO3mi, aNO2mi, K_NO3_NO2, K_NO2_N2O,
     & mu_Sd_DON, mu_Ld_DON, K_anam, K_convert,
     & CN_Phyt, wLPhy, wSDet, wLDet,
     & ro2n
     & , theta_m
      integer ITERMAX
      parameter (
     & )
