      subroutine MPI_Setup (ierr)      


      use Agrif_Util
#include "Param_BeforeCall_MPI_Setup.h"
      integer(4) :: ierr
      

        call Sub_Loop_MPI_Setup(ierr, Agrif_tabvars_i(190)%iarray0, Agri
     &f_tabvars_i(191)%iarray0, Agrif_tabvars_i(192)%iarray0, Agrif_tabv
     &ars_i(193)%iarray0, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(
     &195)%iarray0, Agrif_tabvars_i(198)%iarray0, Agrif_tabvars_i(201)%i
     &array0, Agrif_tabvars_i(148)%iarray0, Agrif_tabvars_i(146)%iarray0
     &, Agrif_tabvars_i(149)%iarray0, Agrif_tabvars_i(147)%iarray0, Agri
     &f_tabvars_i(150)%iarray0, Agrif_tabvars_i(151)%iarray0, Agrif_tabv
     &ars_i(152)%iarray0, Agrif_tabvars_i(153)%iarray0, Agrif_tabvars_l(
     &5)%larray0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0
     &, Agrif_tabvars_l(6)%larray0, Agrif_tabvars_i(154)%iarray0, Agrif_
     &tabvars_i(155)%iarray0, Agrif_tabvars_i(156)%iarray0)

      end


#include "Sub_Loop_MPI_Setup.h"
