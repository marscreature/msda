      subroutine u3dbc_tile (Istr,Iend,Jstr,Jend,grad)
      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util

#include "Param_BeforeCall_u3dbc_tile.h"
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad

                     
                                    
                                             
      if (AGRIF_Root()) then
        call u3dbc_parent_tile (Istr,Iend,Jstr,Jend,grad)
      else
        call u3dbc_child_tile (Istr,Iend,Jstr,Jend,grad)
      endif
      return
      end
      subroutine u3dbc_parent_tile (Istr,Iend,Jstr,Jend,grad)      


      use Agrif_Util
#include "Param_BeforeCall_u3dbc_parent_tile.h"
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad
      

        call Sub_Loop_u3dbc_parent_tile(Istr,Iend,Jstr,Jend,grad, Agrif_
     &tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvar
     &s_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(49)%
     &array2, Agrif_tabvars(131)%array3, Agrif_tabvars_i(174)%iarray0, A
     &grif_tabvars(50)%array2, Agrif_tabvars_i(176)%iarray0, Agrif_tabva
     &rs(111)%array4, Agrif_tabvars_r(16)%array0, Agrif_tabvars_r(17)%ar
     &ray0, Agrif_tabvars_r(46)%array0, Agrif_tabvars_l(5)%larray0, Agri
     &f_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Agrif_tabvars_
     &l(6)%larray0)

      end


#include "Sub_Loop_u3dbc_parent_tile.h"
      subroutine u3dbc_child_tile (Istr,Iend,Jstr,Jend,grad)      


      use Agrif_Util
#include "Param_BeforeCall_u3dbc_child_tile.h"
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad
      

        call Sub_Loop_u3dbc_child_tile(Istr,Iend,Jstr,Jend,grad, Agrif_t
     &abvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars
     &_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(49)%a
     &rray2, Agrif_tabvars(131)%array3, Agrif_tabvars_i(174)%iarray0, Ag
     &rif_tabvars(111)%array4, Agrif_tabvars_r(16)%array0, Agrif_tabvars
     &_r(17)%array0, Agrif_tabvars_r(46)%array0, Agrif_tabvars_l(5)%larr
     &ay0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Agrif
     &_tabvars_l(6)%larray0)

      end


#include "Sub_Loop_u3dbc_child_tile.h"
