      subroutine rho_eos (tile)      


      use Agrif_Util
#include "Param_BeforeCall_rho_eos.h"
      integer(4) :: tile
      

        call Sub_Loop_rho_eos(tile, Agrif_tabvars_i(187)%iarray0, Agrif_
     &tabvars(95)%array3, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(
     &195)%iarray0, Agrif_tabvars_i(186)%iarray0)

      end


#include "Sub_Loop_rho_eos.h"
      subroutine rho_eos_tile (Istr,Iend,Jstr,Jend, K_up,K_dw)      


      use Agrif_Util
#include "Param_BeforeCall_rho_eos_tile.h"
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,0:N) :: K_up
      real, dimension(Istr-2:Iend+2,0:N) :: K_dw
      

        call Sub_Loop_rho_eos_tile(Istr,Iend,Jstr,Jend,K_up,K_dw, Agrif_
     &tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvar
     &s_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(122)
     &%array2, Agrif_tabvars(121)%array2, Agrif_tabvars(104)%array3, Agr
     &if_tabvars(148)%array3, Agrif_tabvars(100)%array3, Agrif_tabvars(1
     &03)%array3, Agrif_tabvars(107)%array3, Agrif_tabvars(99)%array3, A
     &grif_tabvars(51)%array2, Agrif_tabvars(101)%array3, Agrif_tabvars_
     &i(175)%iarray0, Agrif_tabvars(109)%array5, Agrif_tabvars_r(38)%arr
     &ay0, Agrif_tabvars_l(5)%larray0, Agrif_tabvars_i(194)%iarray0, Agr
     &if_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Agrif_tabvars
     &_i(195)%iarray0, Agrif_tabvars_l(6)%larray0)

      end


#include "Sub_Loop_rho_eos_tile.h"
