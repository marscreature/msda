      subroutine uv3dmix_spg (tile)      


      use Agrif_Util
#include "Param_BeforeCall_uv3dmix_spg.h"
      integer(4) :: tile
      

        call Sub_Loop_uv3dmix_spg(tile, Agrif_tabvars_i(187)%iarray0, Ag
     &rif_tabvars(95)%array3, Agrif_tabvars_i(194)%iarray0, Agrif_tabvar
     &s_i(195)%iarray0, Agrif_tabvars_i(186)%iarray0)

      end


#include "Sub_Loop_uv3dmix_spg.h"
      subroutine uv3dmix_spg_tile (Istr,Iend,Jstr,Jend,
     &                             UFx,UFe,VFx,VFe,
     &                             UFs,VFs, dnUdx,  dmUde,
     &                             dUdz,    dnVdx,   dmVde,  dVdz,
     &                             dZdx_r,  dZdx_p,  dZde_r, dZde_p)      


      use Agrif_Util
#include "Param_BeforeCall_uv3dmix_spg_tile.h"
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: UFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: VFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmUde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmVde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnUdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnVdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dUdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dVdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_r
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_r
      

        call Sub_Loop_uv3dmix_spg_tile(Istr,Iend,Jstr,Jend,UFx,UFe,VFx,V
     &Fe,UFs,VFs,dnUdx,dmUde,dUdz,dnVdx,dmVde,dVdz,dZdx_r,dZdx_p,dZde_r,
     &dZde_p, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0
     &, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agri
     &f_tabvars(119)%array2, Agrif_tabvars_r(46)%array0, Agrif_tabvars(1
     &20)%array2, Agrif_tabvars(65)%array2, Agrif_tabvars(66)%array2, Ag
     &rif_tabvars(50)%array2, Agrif_tabvars(62)%array2, Agrif_tabvars(57
     &)%array2, Agrif_tabvars(61)%array2, Agrif_tabvars(58)%array2, Agri
     &f_tabvars(160)%array2, Agrif_tabvars(72)%array2, Agrif_tabvars(71)
     &%array2, Agrif_tabvars(130)%array3, Agrif_tabvars(110)%array4, Agr
     &if_tabvars(63)%array2, Agrif_tabvars(55)%array2, Agrif_tabvars(131
     &)%array3, Agrif_tabvars(111)%array4, Agrif_tabvars(64)%array2, Agr
     &if_tabvars(56)%array2, Agrif_tabvars(161)%array2, Agrif_tabvars(10
     &4)%array3, Agrif_tabvars_i(176)%iarray0, Agrif_tabvars_l(5)%larray
     &0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Agrif_t
     &abvars_l(6)%larray0, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i
     &(195)%iarray0)

      end


#include "Sub_Loop_uv3dmix_spg_tile.h"
      subroutine uv3dmix_spg_child_tile(Istr,Iend,Jstr,Jend,
     &                             UFx,UFe,VFx,VFe,
     &                             UFs,VFs, dnUdx,  dmUde,
     &                             dUdz,    dnVdx,   dmVde,  dVdz,
     &                             dZdx_r,  dZdx_p,  dZde_r, dZde_p)      


      use Agrif_Util
#include "Param_BeforeCall_uv3dmix_spg_child_tile.h"
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: UFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: VFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmUde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmVde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnUdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnVdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dUdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dVdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_r
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_r
      

        call Sub_Loop_uv3dmix_spg_child_tile(Istr,Iend,Jstr,Jend,UFx,UFe
     &,VFx,VFe,UFs,VFs,dnUdx,dmUde,dUdz,dnVdx,dmVde,dVdz,dZdx_r,dZdx_p,d
     &Zde_r,dZde_p, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%i
     &array0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0
     &, Agrif_tabvars(119)%array2, Agrif_tabvars_r(46)%array0, Agrif_tab
     &vars(120)%array2, Agrif_tabvars(65)%array2, Agrif_tabvars(66)%arra
     &y2, Agrif_tabvars(50)%array2, Agrif_tabvars(62)%array2, Agrif_tabv
     &ars(57)%array2, Agrif_tabvars(61)%array2, Agrif_tabvars(58)%array2
     &, Agrif_tabvars(160)%array2, Agrif_tabvars(72)%array2, Agrif_tabva
     &rs(71)%array2, Agrif_tabvars(24)%array3, Agrif_tabvars(110)%array4
     &, Agrif_tabvars(63)%array2, Agrif_tabvars(55)%array2, Agrif_tabvar
     &s(25)%array3, Agrif_tabvars(111)%array4, Agrif_tabvars(64)%array2,
     & Agrif_tabvars(56)%array2, Agrif_tabvars(161)%array2, Agrif_tabvar
     &s(104)%array3, Agrif_tabvars_i(176)%iarray0, Agrif_tabvars_l(5)%la
     &rray0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Agr
     &if_tabvars_l(6)%larray0, Agrif_tabvars_i(194)%iarray0, Agrif_tabva
     &rs_i(195)%iarray0)

      end


#include "Sub_Loop_uv3dmix_spg_child_tile.h"
