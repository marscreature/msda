      integer stafield
      parameter(stafield=5)
      integer indxstaGrd, indxstaTemp, indxstaSalt,
     & indxstaRho, indxstaVel
      parameter ( indxstaGrd=1, indxstaTemp=2,
     & indxstaSalt=3, indxstaRho=4, indxstaVel=5)
      integer ncidsta, nrecsta, staGlevel
     & , staTstep, staTime, staXgrd, staYgrd
     & , staZgrd, staZeta, staU, staV
     & , staLon, staLat
     & , staDepth, staDen, staTemp
     & , staSal
      logical wrtsta(stafield)
      common/incscrum_sta/
     & ncidsta, nrecsta, staGlevel
     & , staTstep, staTime, staXgrd, staYgrd
     & , staZgrd, staZeta, staU, staV
     & , staLon, staLat
     & , staDepth, staDen, staTemp
     & , staSal
     & , wrtsta
      common /cncscrum_sta/ staname, staposname
