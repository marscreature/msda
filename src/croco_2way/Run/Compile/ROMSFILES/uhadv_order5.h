          if (SOUTH_INTER) then
            jmin=1
          else
            jmin=3
          endif
          if (NORTH_INTER) then
            jmax=Mmmpi+1
          else
            jmax=Mmmpi-1
          endif
          if (WEST_INTER) then
            imin=0
          else
            imin=3
          endif
          if (EAST_INTER) then
            imax=Lmmpi+1
          else
            imax=Lmmpi-1
          endif
              DO i = IstrU,Iend
                vel = FLUX6(Hvom(i-3,j,k),Hvom(i-2,j,k),Hvom(i-1,j,k),
     & Hvom(i ,j,k),Hvom(i+1,j,k),Hvom(i+2,j,k),1.)
     & u(i,j-3,k,nrhs), u(i,j-2,k,nrhs),
     & u(i,j-1,k,nrhs), u(i,j ,k,nrhs),
     & u(i,j+1,k,nrhs), u(i,j+2,k,nrhs), vel )
                vel = FLUX4(Hvom(i-2,j,k),Hvom(i-1,j,k),
     & Hvom(i ,j,k),Hvom(i+1,j,k),1.)
     & u(i,j-2,k,nrhs), u(i,j-1,k,nrhs),
     & u(i,j ,k,nrhs), u(i,j+1,k,nrhs), vel )
     & u(i,j-1,k,nrhs), u(i,j ,k,nrhs), vel, cdif)
              ENDDO
              DO i = IstrU,Iend
     & u(i,j-1,k,nrhs), u(i,j ,k,nrhs), vel, cdif)
              ENDDO
              DO i = IstrU,Iend
                vel = FLUX4(Hvom(i-2,j,k),Hvom(i-1,j,k),
     & Hvom(i ,j,k),Hvom(i+1,j,k),1.)
     & u(i,j-2,k,nrhs), u(i,j-1,k,nrhs),
     & u(i,j ,k,nrhs), u(i,j+1,k,nrhs), vel )
     & u(i,j-1,k,nrhs), u(i,j ,k,nrhs), vel, cdif)
              ENDDO
              DO i = IstrU,Iend
     & u(i,j-1,k,nrhs), u(i,j ,k,nrhs), vel, cdif)
              ENDDO
              DO i = IstrU,Iend
                vel = FLUX4(Hvom(i-2,j,k),Hvom(i-1,j,k),
     & Hvom(i ,j,k),Hvom(i+1,j,k),1.)
     & u(i,j-2,k,nrhs), u(i,j-1,k,nrhs),
     & u(i,j ,k,nrhs), u(i,j+1,k,nrhs), vel )
     & u(i,j-1,k,nrhs), u(i,j ,k,nrhs), vel, cdif)
              ENDDO
            ENDIF
              DO j = Jstr,Jend
                vel = FLUX6(Huon(i-2,j,k),Huon(i-1,j,k),Huon(i ,j,k),
     & Huon(i+1,j,k),Huon(i+2,j,k),Huon(i+3,j,k),1.)
     & u(i-2,j,k,nrhs), u(i-1,j,k,nrhs),
     & u(i ,j,k,nrhs), u(i+1,j,k,nrhs),
     & u(i+2,j,k,nrhs), u(i+3,j,k,nrhs), vel )
                vel = FLUX4(Huon(i-1,j,k),Huon(i ,j,k),
     & Huon(i+1,j,k),Huon(i+2,j,k),1.)
     & u(i-1,j,k,nrhs), u(i ,j,k,nrhs),
     & u(i+1,j,k,nrhs), u(i+2,j,k,nrhs), vel )
     & u(i ,j,k,nrhs), u(i+1,j,k,nrhs), vel, cdif)
              ENDDO
              DO j = Jstr,Jend
     & u(i,j,k,nrhs), u(i+1,j,k,nrhs), vel, cdif)
              ENDDO
              DO j = Jstr,Jend
                vel = FLUX4(Huon(i-1,j,k),Huon(i ,j,k),
     & Huon(i+1,j,k),Huon(i+2,j,k),1.)
     & u(i-1,j,k,nrhs), u(i ,j,k,nrhs),
     & u(i+1,j,k,nrhs), u(i+2,j,k,nrhs), vel )
     & u(i ,j,k,nrhs), u(i+1,j,k,nrhs), vel, cdif)
              ENDDO
              DO j = Jstr,Jend
     & u(i,j,k,nrhs), u(i+1,j,k,nrhs), vel, cdif)
              ENDDO
              DO j = Jstr,Jend
                vel = FLUX4(Huon(i-1,j,k),Huon(i ,j,k),
     & Huon(i+1,j,k),Huon(i+2,j,k),1.)
     & u(i-1,j,k,nrhs), u(i ,j,k,nrhs),
     & u(i+1,j,k,nrhs), u(i+2,j,k,nrhs), vel )
     & u(i,j,k,nrhs), u(i+1,j,k,nrhs), vel, cdif)
              ENDDO
            ENDIF
