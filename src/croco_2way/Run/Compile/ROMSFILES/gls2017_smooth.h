      if (.not.WEST_INTER) then
        do j=J_EXT_RANGE
          trb(Istr-1,j,k,nnew,ig)=trb(Istr,j,k,nnew,ig)
        enddo
      endif
      if (.not.EAST_INTER) then
        do j=J_EXT_RANGE
          trb(Iend+1,j,k,nnew,ig)=trb(Iend,j,k,nnew,ig)
        enddo
      endif
      if (.not.SOUTH_INTER) then
        do i=I_EXT_RANGE
          trb(i,Jstr-1,k,nnew,ig)=trb(i,Jstr,k,nnew,ig)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=I_EXT_RANGE
          trb(i,Jend+1,k,nnew,ig)=trb(i,Jend,k,nnew,ig)
        enddo
      endif
      if (.not.WEST_INTER.and. .not.SOUTH_INTER) then
        trb(Istr-1,Jstr-1,k,nnew,ig)=trb(Istr,Jstr,k,nnew,ig)
      endif
      if (.not.WEST_INTER.and. .not.NORTH_INTER) then
        trb(Istr-1,Jend+1,k,nnew,ig)=trb(Istr,Jend,k,nnew,ig)
      endif
      if (.not.EAST_INTER.and. .not.SOUTH_INTER) then
        trb(Iend+1,Jstr-1,k,nnew,ig)=trb(Iend,Jstr,k,nnew,ig)
      endif
      if (.not.EAST_INTER.and. .not.NORTH_INTER) then
        trb(Iend+1,Jend+1,k,nnew,ig)=trb(Iend,Jend,k,nnew,ig)
      endif
         DO j=jstr-1,jend+1
            DO i=istr,iend+1
               FX (i,j )=( trb(i ,j,k,nnew,ig)
     & - trb(i-1,j,k,nnew,ig) )
            ENDDO
         ENDDO
         DO j=jstr,jend+1
            DO i=istr-1,iend+1
               FE1(i,j,0)=( trb(i,j ,k,nnew,ig)
     & - trb(i,j-1,k,nnew,ig) )
            ENDDO
            DO i=istr,iend
              FE(i,j)=FE1(i,j,0)
     & -FX(i ,j)-FX(i+1,j-1))
            ENDDO
         ENDDO
         DO j=jstr,jend
            DO i=istr,iend+1
              FX(i,j)=FX(i,j )
     & -FE1(i,j ,0)-FE1(i-1,j+1,0))
            ENDDO
            DO i=istr,iend
               trb(i,j,k,nnew,ig)=trb(i,j,k,nnew,ig)
     & +FE(i,j+1)-FE(i,j) )
            ENDDO
