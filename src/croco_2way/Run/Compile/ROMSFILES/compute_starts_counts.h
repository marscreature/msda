      integer imin,imax,jmin,jmax, start(4), count(4),
     & vert_type, horiz_type
      if (ii.gt.0) then
        start(1)=1-imin+iminmpi
        imin=1
      endif
      if (ii.eq.NP_XI-1) then
        imax=Lmmpi+1
      else
        imax=Lmmpi
      endif
      if (jj.gt.0) then
        start(2)=1-jmin+jminmpi
        jmin=1
      endif
      if (jj.eq.NP_ETA-1) then
        jmax=Mmmpi+1
      else
        jmax=Mmmpi
      endif
      count(1)=imax-imin+1
      count(2)=jmax-jmin+1
      elseif (vert_type.eq.1) then
        start(4)=record
      elseif (vert_type.eq.2) then
        start(4)=record
      else
        ierr=ierr+1
      endif
