      integer indxfltGrd, indxfltTemp, indxfltSalt,
     & indxfltRho, indxfltVel
      parameter ( indxfltGrd=1, indxfltTemp=2,
     & indxfltSalt=3, indxfltRho=4, indxfltVel=5)
      integer fltfield
      parameter(fltfield=5)
      integer ncidflt, nrecflt, fltGlevel
     & , fltTstep, fltTime, fltXgrd, fltYgrd
     & , fltZgrd, fltVel
     & , rstnfloats, rstTinfo, rstfltgrd
     & , rsttrack
     & , fltLon, fltLat
     & , fltDepth, fltDen, fltTemp
     & , fltSal
      logical wrtflt(fltfield)
      common/incscrum_floats/
     & ncidflt, nrecflt, fltGlevel
     & , fltTstep, fltTime, fltXgrd, fltYgrd
     & , fltZgrd, fltVel
     & , rstnfloats, rstTinfo, rstfltgrd
     & , rsttrack
     & , fltLon, fltLat
     & , fltDepth, fltDen, fltTemp
     & , fltSal
     & , wrtflt
      common /cncscrum_floats/ fltname, fposnam
