          if (WEST_INTER) then
            imin=Istr-1
          else
            imin=max(Istr-1,1)
          endif
          if (EAST_INTER) then
            imax=Iend+2
          else
            imax=min(Iend+2,Lmmpi+1)
          endif
          if (SOUTH_INTER) then
            jmin=Jstr-1
          else
            jmin=max(Jstr-1,1)
          endif
          if (NORTH_INTER) then
            jmax=Jend+2
          else
            jmax=min(Jend+2,Mmmpi+1)
          endif
          do j=Jstr,Jend
            do i=imin,imax
              FX(i,j)=(tmp(i,j,k,3)-tmp(i-1,j,k,3))
            enddo
          enddo
          if (.not.WEST_INTER) then
            do j=Jstr,Jend
              FX(0,j)=FX(1,j)
            enddo
          endif
          if (.not.EAST_INTER) then
            do j=Jstr,Jend
              FX(Lmmpi+2,j)=FX(Lmmpi+1,j)
            enddo
          endif
          do j=Jstr,Jend
            do i=Istr-1,Iend+1
              WORK(i,j)=FX(i+1,j)-FX(i,j)
            enddo
          do j=Jstr,Jend
            do i=Istr,Iend+1
              if (Huon(i,j,k) .gt. 0.) then
                cff=WORK(i-1,j)
              else
                cff=WORK(i,j)
              endif
          enddo
          do j=jmin,jmax
            do i=Istr,Iend
              FE(i,j)=(tmp(i,j,k,3)-tmp(i,j-1,k,3))
            enddo
          enddo
          if (.not.SOUTH_INTER) then
            do i=Istr,Iend
              FE(i,0)=FE(i,1)
            enddo
          endif
          if (.not.NORTH_INTER) then
            do i=Istr,Iend
              FE(i,Mmmpi+2)=FE(i,Mmmpi+1)
            enddo
          endif
          do j=Jstr-1,Jend+1
            do i=Istr,Iend
              WORK(i,j)=FE(i,j+1)-FE(i,j)
            enddo
          do j=Jstr,Jend+1
            do i=Istr,Iend
              if (Hvom(i,j,k) .gt. 0.) then
                cff=WORK(i,j-1)
              else
                cff=WORK(i,j)
              endif
            enddo
