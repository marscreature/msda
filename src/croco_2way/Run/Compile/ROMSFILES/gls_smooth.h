      if (.not.WEST_INTER) then
        do j=J_EXT_RANGE
          hwrk(Istr-1,j,k,nnew,ig)=hwrk(Istr,j,k,nnew,ig)
        enddo
      endif
      if (.not.EAST_INTER) then
        do j=J_EXT_RANGE
          hwrk(Iend+1,j,k,nnew,ig)=hwrk(Iend,j,k,nnew,ig)
        enddo
      endif
      if (.not.SOUTH_INTER) then
        do i=I_EXT_RANGE
          hwrk(i,Jstr-1,k,nnew,ig)=hwrk(i,Jstr,k,nnew,ig)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=I_EXT_RANGE
          hwrk(i,Jend+1,k,nnew,ig)=hwrk(i,Jend,k,nnew,ig)
        enddo
      endif
      if (.not.WEST_INTER.and. .not.SOUTH_INTER) then
        hwrk(Istr-1,Jstr-1,k,nnew,ig)=hwrk(Istr,Jstr,k,nnew,ig)
      endif
      if (.not.WEST_INTER.and. .not.NORTH_INTER) then
        hwrk(Istr-1,Jend+1,k,nnew,ig)=hwrk(Istr,Jend,k,nnew,ig)
      endif
      if (.not.EAST_INTER.and. .not.SOUTH_INTER) then
        hwrk(Iend+1,Jstr-1,k,nnew,ig)=hwrk(Iend,Jstr,k,nnew,ig)
      endif
      if (.not.EAST_INTER.and. .not.NORTH_INTER) then
        hwrk(Iend+1,Jend+1,k,nnew,ig)=hwrk(Iend,Jend,k,nnew,ig)
      endif
      do j=Jstr,Jend+1
        do i=Istr,Iend+1
     & + hwrk(i-1,j ,k,nnew,ig)
     & + hwrk(i ,j-1,k,nnew,ig)
     & + hwrk(i-1,j-1,k,nnew,ig) )
        enddo
      enddo
      do j=Jstr,Jend
        do i=Istr,Iend
     & +pmask2(i,j+1) +pmask2(i+1,j+1))
     & +wrk(i,j+1)+wrk(i+1,j+1))
        enddo
      enddo
