












      subroutine Agrif_initworkspace()      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Agrif_initworkspace(N1dETA,N1dXI,eta_v,eta_r
     &ho,xi_u,xi_rho,padd_E,padd_X,Mm,Lm,MMm,LLm,N3d,N2d)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: N1dETA
      integer(4) :: N1dXI
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: padd_E
      integer(4) :: padd_X
      integer(4) :: Mm
      integer(4) :: Lm
      integer(4) :: MMm
      integer(4) :: LLm
      integer(4) :: N3d
      integer(4) :: N2d
        end subroutine Sub_Loop_Agrif_initworkspace

      end interface
      

        call Sub_Loop_Agrif_initworkspace( Agrif_tabvars_i(184)%iarray0,
     & Agrif_tabvars_i(185)%iarray0, Agrif_tabvars_i(142)%iarray0, Agrif
     &_tabvars_i(143)%iarray0, Agrif_tabvars_i(144)%iarray0, Agrif_tabva
     &rs_i(145)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(1
     &89)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(200)%ia
     &rray0, Agrif_tabvars_i(198)%iarray0, Agrif_tabvars_i(201)%iarray0,
     & Agrif_tabvars_i(186)%iarray0, Agrif_tabvars_i(187)%iarray0)

      end


      subroutine Sub_Loop_Agrif_initworkspace(N1dETA,N1dXI,eta_v,eta_rho
     &,xi_u,xi_rho,padd_E,padd_X,Mm,Lm,MMm,LLm,N3d,N2d)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: N1dETA
      integer(4) :: N1dXI
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: padd_E
      integer(4) :: padd_X
      integer(4) :: Mm
      integer(4) :: Lm
      integer(4) :: MMm
      integer(4) :: LLm
      integer(4) :: N3d
      integer(4) :: N2d

                   

                     
      integer(4) :: ierr
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

!$AGRIF_DO_NOT_TREAT
      INTEGER*4 :: ocean_grid_comm
      common /cpl_comm/ ocean_grid_comm
!$AGRIF_END_DO_NOT_TREAT
                                                
      integer(4) :: size_XI
      integer(4) :: size_ETA
      integer(4) :: se
      integer(4) :: sse
      integer(4) :: sz
      integer(4) :: ssz
!$AGRIF_DO_NOT_TREAT
      logical, SAVE :: MPIisinitialize = .FALSE.
!$AGRIF_END_DO_NOT_TREAT
      IF (Agrif_Root()) THEN
      LLm=LLm0;  MMm=MMm0
        IF (.Not.MPIisinitialize) THEN
           call MPI_Init(ierr)
           MPIisinitialize = .TRUE.
        ENDIF
      ENDIF
      Lm=(LLm+NP_XI-1)/NP_XI; Mm=(MMm+NP_ETA-1)/NP_ETA
      padd_X=(Lm+2)/2-(Lm+1)/2; padd_E=(Mm+2)/2-(Mm+1)/2
      xi_rho=LLm+2;  xi_u=xi_rho-1
      eta_rho=MMm+2; eta_v=eta_rho-1
      size_XI=7+(Lm+NSUB_X-1)/NSUB_X
      size_ETA=7+(Mm+NSUB_E-1)/NSUB_E
      sse=size_ETA/Np; ssz=Np/size_ETA
      se=sse/(sse+ssz);   sz=1-se
      N1dXI = size_XI
      N1dETA = size_ETA
      N2d=size_XI*(se*size_ETA+sz*Np)
      N3d=size_XI*size_ETA*Np
      call MPI_Setup(ierr)
      return
       
      

      end subroutine Sub_Loop_Agrif_initworkspace

      subroutine Agrif_initvalues()      

      use AGRIF_UTIL
      interface
        subroutine Sub_Loop_Agrif_initvalues(Mmmpi,Lmmpi,padd_E,Mm,padd_
     &X,Lm,vbarid,ubarid,hid,rmaskid,DV_avg1,DU_avg1,myfy,myfx,vsponge,u
     &sponge,UVTimesponge,TTimesponge,nbstep3d,nbcoarse,ntstart,iic,iif,
     &UVspongeTimeindex,TspongeTimeindex,T_east,V_east,U_east,EAST_INTER
     &,T_west,V_west,U_west,WEST_INTER,T_north,V_north,U_north,NORTH_INT
     &ER,t,T_south,v,V_south,u,U_south,SOUTH_INTER,Vtimeindex,Utimeindex
     &,Ttimeindex,ZetaTimeindex,V2DTimeindex,U2DTimeindex,time,time_star
     &t,kstp,next_kstp,wrthis,ldefhis,may_day_flag,numthreads,N1dETA,N1d
     &XI,eta_v,eta_rho,xi_u,xi_rho,MMm,LLm,N3d,N2d)
          use AGRIF_UTIL
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: vbarid
      integer(4) :: ubarid
      integer(4) :: hid
      integer(4) :: rmaskid
      integer(4) :: UVTimesponge
      integer(4) :: TTimesponge
      integer(4) :: nbstep3d
      integer(4) :: nbcoarse
      integer(4) :: ntstart
      integer(4) :: iic
      integer(4) :: iif
      integer(4) :: UVspongeTimeindex
      integer(4) :: TspongeTimeindex
      logical :: EAST_INTER
      logical :: WEST_INTER
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      integer(4) :: Vtimeindex
      integer(4) :: Utimeindex
      integer(4) :: Ttimeindex
      integer(4) :: ZetaTimeindex
      integer(4) :: V2DTimeindex
      integer(4) :: U2DTimeindex
      real :: time
      real :: time_start
      integer(4) :: kstp
      integer(4) :: next_kstp
      logical :: ldefhis
      integer(4) :: may_day_flag
      integer(4) :: numthreads
      integer(4) :: N1dETA
      integer(4) :: N1dXI
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: MMm
      integer(4) :: LLm
      integer(4) :: N3d
      integer(4) :: N2d
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: myfy
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: myfx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vsponge
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: usponge
      real, dimension(Lmmpi:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_ea
     &st
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: V_east
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: U_east
      real, dimension(0:1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_west
      real, dimension(0:0,-1:Mm+2+padd_E,1:N,1:4) :: V_west
      real, dimension(1:1,-1:Mm+2+padd_E,1:N,1:4) :: U_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi:Mmmpi+1,1:N,1:4,1:NT) :: T_no
     &rth
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: V_north
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: U_north
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,0:1,1:N,1:4,1:NT) :: T_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,1:1,1:N,1:4) :: V_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,0:0,1:N,1:4) :: U_south
      logical, dimension(1:500+NT) :: wrthis
        end subroutine Sub_Loop_Agrif_initvalues

      end interface
      

        call Sub_Loop_Agrif_initvalues( Agrif_tabvars_i(194)%iarray0, Ag
     &rif_tabvars_i(195)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_ta
     &bvars_i(197)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_
     &i(200)%iarray0, Agrif_tabvars_i(20)%iarray0, Agrif_tabvars_i(21)%i
     &array0, Agrif_tabvars_i(23)%iarray0, Agrif_tabvars_i(13)%iarray0, 
     &Agrif_tabvars(114)%array3, Agrif_tabvars(115)%array3, Agrif_tabvar
     &s(30)%array4, Agrif_tabvars(31)%array4, Agrif_tabvars(24)%array3, 
     &Agrif_tabvars(25)%array3, Agrif_tabvars_i(37)%iarray0, Agrif_tabva
     &rs_i(38)%iarray0, Agrif_tabvars_i(173)%iarray0, Agrif_tabvars_i(39
     &)%iarray0, Agrif_tabvars_i(171)%iarray0, Agrif_tabvars_i(182)%iarr
     &ay0, Agrif_tabvars_i(177)%iarray0, Agrif_tabvars_i(35)%iarray0, Ag
     &rif_tabvars_i(36)%iarray0, Agrif_tabvars(43)%array5, Agrif_tabvars
     &(41)%array4, Agrif_tabvars(42)%array4, Agrif_tabvars_l(7)%larray0,
     & Agrif_tabvars(46)%array5, Agrif_tabvars(44)%array4, Agrif_tabvars
     &(45)%array4, Agrif_tabvars_l(6)%larray0, Agrif_tabvars(37)%array5,
     & Agrif_tabvars(35)%array4, Agrif_tabvars(36)%array4, Agrif_tabvars
     &_l(5)%larray0, Agrif_tabvars(109)%array5, Agrif_tabvars(40)%array5
     &, Agrif_tabvars(110)%array4, Agrif_tabvars(38)%array4, Agrif_tabva
     &rs(111)%array4, Agrif_tabvars(39)%array4, Agrif_tabvars_l(4)%larra
     &y0, Agrif_tabvars_i(41)%iarray0, Agrif_tabvars_i(42)%iarray0, Agri
     &f_tabvars_i(43)%iarray0, Agrif_tabvars_i(49)%iarray0, Agrif_tabvar
     &s_i(45)%iarray0, Agrif_tabvars_i(47)%iarray0, Agrif_tabvars_r(44)%
     &array0, Agrif_tabvars_r(42)%array0, Agrif_tabvars_i(181)%iarray0, 
     &Agrif_tabvars_i(178)%iarray0, Agrif_tabvars_l(3)%larray1, Agrif_ta
     &bvars_l(9)%larray0, Agrif_tabvars_i(162)%iarray0, Agrif_tabvars_i(
     &172)%iarray0, Agrif_tabvars_i(184)%iarray0, Agrif_tabvars_i(185)%i
     &array0, Agrif_tabvars_i(142)%iarray0, Agrif_tabvars_i(143)%iarray0
     &, Agrif_tabvars_i(144)%iarray0, Agrif_tabvars_i(145)%iarray0, Agri
     &f_tabvars_i(198)%iarray0, Agrif_tabvars_i(201)%iarray0, Agrif_tabv
     &ars_i(186)%iarray0, Agrif_tabvars_i(187)%iarray0)

      end


      subroutine Sub_Loop_Agrif_initvalues(Mmmpi,Lmmpi,padd_E,Mm,padd_X,
     &Lm,vbarid,ubarid,hid,rmaskid,DV_avg1,DU_avg1,myfy,myfx,vsponge,usp
     &onge,UVTimesponge,TTimesponge,nbstep3d,nbcoarse,ntstart,iic,iif,UV
     &spongeTimeindex,TspongeTimeindex,T_east,V_east,U_east,EAST_INTER,T
     &_west,V_west,U_west,WEST_INTER,T_north,V_north,U_north,NORTH_INTER
     &,t,T_south,v,V_south,u,U_south,SOUTH_INTER,Vtimeindex,Utimeindex,T
     &timeindex,ZetaTimeindex,V2DTimeindex,U2DTimeindex,time,time_start,
     &kstp,next_kstp,wrthis,ldefhis,may_day_flag,numthreads,N1dETA,N1dXI
     &,eta_v,eta_rho,xi_u,xi_rho,MMm,LLm,N3d,N2d)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_UTIL
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: vbarid
      integer(4) :: ubarid
      integer(4) :: hid
      integer(4) :: rmaskid
      integer(4) :: UVTimesponge
      integer(4) :: TTimesponge
      integer(4) :: nbstep3d
      integer(4) :: nbcoarse
      integer(4) :: ntstart
      integer(4) :: iic
      integer(4) :: iif
      integer(4) :: UVspongeTimeindex
      integer(4) :: TspongeTimeindex
      logical :: EAST_INTER
      logical :: WEST_INTER
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      integer(4) :: Vtimeindex
      integer(4) :: Utimeindex
      integer(4) :: Ttimeindex
      integer(4) :: ZetaTimeindex
      integer(4) :: V2DTimeindex
      integer(4) :: U2DTimeindex
      real :: time
      real :: time_start
      integer(4) :: kstp
      integer(4) :: next_kstp
      logical :: ldefhis
      integer(4) :: may_day_flag
      integer(4) :: numthreads
      integer(4) :: N1dETA
      integer(4) :: N1dXI
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: MMm
      integer(4) :: LLm
      integer(4) :: N3d
      integer(4) :: N2d
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: myfy
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: myfx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vsponge
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: usponge
      real, dimension(Lmmpi:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_ea
     &st
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: V_east
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: U_east
      real, dimension(0:1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_west
      real, dimension(0:0,-1:Mm+2+padd_E,1:N,1:4) :: V_west
      real, dimension(1:1,-1:Mm+2+padd_E,1:N,1:4) :: U_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi:Mmmpi+1,1:N,1:4,1:NT) :: T_no
     &rth
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: V_north
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: U_north
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,0:1,1:N,1:4,1:NT) :: T_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,1:1,1:N,1:4) :: V_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,0:0,1:N,1:4) :: U_south
      logical, dimension(1:500+NT) :: wrthis

                     
                   

                                      
      integer(4) :: tile
      integer(4) :: ierr
      integer(4) :: trd
      integer(4) :: subs
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                
      integer(4) :: size_XI
      integer(4) :: size_ETA
      integer(4) :: se
      integer(4) :: sse
      integer(4) :: sz
      integer(4) :: ssz
                                                   
      real :: res1
      real :: res2
      real :: res3
      real :: res4
      real :: res5
      real :: res6
      real :: cff1
      real :: cff2
                    
      integer(4) :: i
      integer(4) :: j
                             
      integer(4) :: ipr
      integer(4) :: jpr
      integer(4) :: itrc
                                                                     
      real, pointer, dimension(:,:) :: hparent
      real, pointer, dimension(:,:) :: umaskparent
      real, pointer, dimension(:,:) :: rmaskparent
                                                
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: htest
                   
      real, dimension(1:5) :: tind
                                  
      integer(4) :: ipu
      integer(4) :: jpu
      integer(4) :: ipv
      integer(4) :: jpv
      integer(4) :: k
                                                
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: gradh
                                               
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbis
                                               
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hcur
                                                
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hmean
                                                
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbis2
                                                
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbis3
                                                 
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: CF
                                                 
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: DC
                       
      integer(4) :: nbloop
                             
      integer(4) :: nbconstraint
                      
      integer(4) :: n1
      integer(4) :: n2
                                    
      real, dimension(1:100000,1:2) :: valconstraints
                                            
      integer(4) :: iprmin
      integer(4) :: iprmax
      integer(4) :: jprmin
      integer(4) :: jprmax
                                                                       
                                                                 
                 
      real, allocatable, dimension(:,:) :: hconstraint
      real, allocatable, dimension(:,:) :: constraints
      real, allocatable, dimension(:,:) :: gradconstraints
                         
      integer(4) :: irhot
      external hinterp, updateh, updatermask
      external initzeta,initubar,initvbar,inith
      external initu,initv,initt
!$AGRIF_DO_NOT_TREAT
       integer*4 isens
       common/interph/isens
!$AGRIF_END_DO_NOT_TREAT
      Lm=(LLm+NP_XI-1)/NP_XI; Mm=(MMm+NP_ETA-1)/NP_ETA
      padd_X=(Lm+2)/2-(Lm+1)/2; padd_E=(Mm+2)/2-(Mm+1)/2
      xi_rho=LLm+2;  xi_u=xi_rho-1
      eta_rho=MMm+2; eta_v=eta_rho-1
      size_XI=7+(Lm+NSUB_X-1)/NSUB_X
      size_ETA=7+(Mm+NSUB_E-1)/NSUB_E
      sse=size_ETA/Np; ssz=Np/size_ETA
      se=sse/(sse+ssz);   sz=1-se
      N1dXI = size_XI
      N1dETA = size_ETA
      N2d=size_XI*(se*size_ETA+sz*Np)
      N3d=size_XI*size_ETA*Np
      call declare_zoom_variables()
      call MPI_Setup (ierr)
      if (ierr.ne.0) goto 100
      call read_inp (ierr)
      if (ierr.ne.0) goto 100
      call init_scalars (ierr)
      if (ierr.ne.0) goto 100
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call init_arrays (tile)
         enddo
       enddo
      call get_grid
      if (may_day_flag.ne.0) goto 99
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call setup_grid1 (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call setup_grid2 (tile)
         enddo
       enddo
      call set_scoord
      call set_weights
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call set_depth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call grid_stiffness (tile)
         enddo
       enddo
        call get_initial
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call ana_initial (tile)
         enddo
       enddo
      if (may_day_flag.ne.0) goto 99
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call set_depth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call set_HUV (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call omega (tile)
        call rho_eos (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call set_nudgcof_fine (tile)
         enddo
       enddo
      call get_vbc
      if (may_day_flag.ne.0) goto 99
      if (ldefhis .and. wrthis(indxTime)) call wrt_his
      if (may_day_flag.ne.0) goto 99
 99   continue
 100  continue
      next_kstp=kstp
      time_start=time
      U2DTimeindex = -1
      V2DTimeindex = -1
      ZetaTimeindex = -1
      Ttimeindex = -1
      Utimeindex = -1
      Vtimeindex = -1
            if (.not.SOUTH_INTER) then
                U_south(0:Lmmpi+1,0:0,1:N,4)=
     &          u(0:Lmmpi+1,0:0,1:N,1)
                V_south(0:Lmmpi+1,1:1,1:N,4)=
     &          v(0:Lmmpi+1,1:1,1:N,1)
                T_south(0:Lmmpi+1,0:1,1:N,4,1:NT)=
     &          t(0:Lmmpi+1,0:1,1:N,1,1:NT)
            endif
            if (.not.NORTH_INTER) then
                U_north(0:Lmmpi+1,Mmmpi+1:Mmmpi+1,1:N,4)=
     &          u(0:Lmmpi+1,Mmmpi+1:Mmmpi+1,1:N,1)
                V_north(0:Lmmpi+1,Mmmpi+1:Mmmpi+1,1:N,4)=
     &          v(0:Lmmpi+1,Mmmpi+1:Mmmpi+1,1:N,1)
                T_north(0:Lmmpi+1,Mmmpi:Mmmpi+1,1:N,4,1:NT)=
     &          t(0:Lmmpi+1,Mmmpi:Mmmpi+1,1:N,1,1:NT)
            endif
            if (.not.WEST_INTER) then
                U_west(1:1,0:Mmmpi+1,1:N,4)=
     &          u(1:1,0:Mmmpi+1,1:N,1)
                V_west(0:0,0:Mmmpi+1,1:N,4)=
     &            v(0:0,0:Mmmpi+1,1:N,1)
                T_west(0:1,0:Mmmpi+1,1:N,4,1:NT)=
     &          t(0:1,0:Mmmpi+1,1:N,1,1:NT)
            endif
            if (.not.EAST_INTER) then
                U_east(Lmmpi+1:Lmmpi+1,0:Mmmpi+1,1:N,4)=
     &          u(Lmmpi+1:Lmmpi+1,0:Mmmpi+1,1:N,1)
                V_east(Lmmpi+1:Lmmpi+1,0:Mmmpi+1,1:N,4)=
     &          v(Lmmpi+1:Lmmpi+1,0:Mmmpi+1,1:N,1)
                T_east(Lmmpi:Lmmpi+1,0:Mmmpi+1,1:N,4,1:NT)=
     &          t(Lmmpi:Lmmpi+1,0:Mmmpi+1,1:N,1,1:NT)
            endif
      TspongeTimeindex = -1
      UVspongeTimeindex = -1
      iif = -1
      iic = ntstart
      nbcoarse = 1
      nbstep3d = 0
      TTimesponge = 1
      UVTimesponge = 1
      usponge = 0.D0
      vsponge = 0.D0
      myfx = 0.D0
      myfy = 0.D0
      DU_avg1(:,:,4:5) = 0.D0
      DV_avg1(:,:,4:5) = 0.D0
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      call Agrif_Update_Variable(rmaskid,procname=updatermask)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      call Agrif_ChildGrid_To_ParentGrid()
      call ResetMask()
      call Agrif_ParentGrid_To_ChildGrid()
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      call Agrif_Update_Variable(hid,locupdate=(/1,0/),
     &     procname=updateh)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      isens = 1
      call Agrif_Bc_Variable(ubarid,calledweight=1.D0,procname=hinterp)
      isens = 2
      call Agrif_Bc_Variable(vbarid,calledweight=1.D0,procname=hinterp)
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      call Agrif_Update_Variable(hid,locupdate=(/1,0/),
     &     procname=updateh)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      call Agrif_ChildGrid_To_ParentGrid()
      call UpdateGridhis()
      call Agrif_ParentGrid_To_ChildGrid()
      return
       
      

      end subroutine Sub_Loop_Agrif_initvalues

      subroutine declare_zoom_variables()      

      use AGRIF_UTIL
      interface
        subroutine Sub_Loop_declare_zoom_variables(vspongeid,vid,usponge
     &id,uid,tspongeid,tid,vbarid,ubarid,zetaid,rmaskid,hid,updatemyfyid
     &,updatehvomid,updatevid,updatemyfxid,updatehuonid,updateuid,update
     &tid,updatedvavg2id,updatevbarid,updateduavg2id,updateubarid,update
     &zetaid,j2v,j1v,i2u,i1u,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INT
     &ER,j2t,j1t,i2t,i1t,Mmmpi,padd_X,Lm,Lmmpi,padd_E,Mm)
          use AGRIF_UTIL
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: vspongeid
      integer(4) :: vid
      integer(4) :: uspongeid
      integer(4) :: uid
      integer(4) :: tspongeid
      integer(4) :: tid
      integer(4) :: vbarid
      integer(4) :: ubarid
      integer(4) :: zetaid
      integer(4) :: rmaskid
      integer(4) :: hid
      integer(4) :: updatemyfyid
      integer(4) :: updatehvomid
      integer(4) :: updatevid
      integer(4) :: updatemyfxid
      integer(4) :: updatehuonid
      integer(4) :: updateuid
      integer(4) :: updatetid
      integer(4) :: updatedvavg2id
      integer(4) :: updatevbarid
      integer(4) :: updateduavg2id
      integer(4) :: updateubarid
      integer(4) :: updatezetaid
      integer(4) :: j2v
      integer(4) :: j1v
      integer(4) :: i2u
      integer(4) :: i1u
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: j2t
      integer(4) :: j1t
      integer(4) :: i2t
      integer(4) :: i1t
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
        end subroutine Sub_Loop_declare_zoom_variables

      end interface
      

        call Sub_Loop_declare_zoom_variables( Agrif_tabvars_i(14)%iarray
     &0, Agrif_tabvars_i(18)%iarray0, Agrif_tabvars_i(15)%iarray0, Agrif
     &_tabvars_i(19)%iarray0, Agrif_tabvars_i(16)%iarray0, Agrif_tabvars
     &_i(17)%iarray0, Agrif_tabvars_i(20)%iarray0, Agrif_tabvars_i(21)%i
     &array0, Agrif_tabvars_i(22)%iarray0, Agrif_tabvars_i(13)%iarray0, 
     &Agrif_tabvars_i(23)%iarray0, Agrif_tabvars_i(3)%iarray0, Agrif_tab
     &vars_i(1)%iarray0, Agrif_tabvars_i(5)%iarray0, Agrif_tabvars_i(4)%
     &iarray0, Agrif_tabvars_i(2)%iarray0, Agrif_tabvars_i(6)%iarray0, A
     &grif_tabvars_i(7)%iarray0, Agrif_tabvars_i(8)%iarray0, Agrif_tabva
     &rs_i(10)%iarray0, Agrif_tabvars_i(9)%iarray0, Agrif_tabvars_i(11)%
     &iarray0, Agrif_tabvars_i(12)%iarray0, Agrif_tabvars_i(25)%iarray0,
     & Agrif_tabvars_i(26)%iarray0, Agrif_tabvars_i(27)%iarray0, Agrif_t
     &abvars_i(28)%iarray0, Agrif_tabvars_l(5)%larray0, Agrif_tabvars_l(
     &4)%larray0, Agrif_tabvars_l(7)%larray0, Agrif_tabvars_l(6)%larray0
     &, Agrif_tabvars_i(29)%iarray0, Agrif_tabvars_i(31)%iarray0, Agrif_
     &tabvars_i(30)%iarray0, Agrif_tabvars_i(32)%iarray0, Agrif_tabvars_
     &i(194)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)
     &%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars_i(188)%iarra
     &y0, Agrif_tabvars_i(197)%iarray0)

      end


      subroutine Sub_Loop_declare_zoom_variables(vspongeid,vid,uspongeid
     &,uid,tspongeid,tid,vbarid,ubarid,zetaid,rmaskid,hid,updatemyfyid,u
     &pdatehvomid,updatevid,updatemyfxid,updatehuonid,updateuid,updateti
     &d,updatedvavg2id,updatevbarid,updateduavg2id,updateubarid,updateze
     &taid,j2v,j1v,i2u,i1u,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER
     &,j2t,j1t,i2t,i1t,Mmmpi,padd_X,Lm,Lmmpi,padd_E,Mm)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_UTIL
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: vspongeid
      integer(4) :: vid
      integer(4) :: uspongeid
      integer(4) :: uid
      integer(4) :: tspongeid
      integer(4) :: tid
      integer(4) :: vbarid
      integer(4) :: ubarid
      integer(4) :: zetaid
      integer(4) :: rmaskid
      integer(4) :: hid
      integer(4) :: updatemyfyid
      integer(4) :: updatehvomid
      integer(4) :: updatevid
      integer(4) :: updatemyfxid
      integer(4) :: updatehuonid
      integer(4) :: updateuid
      integer(4) :: updatetid
      integer(4) :: updatedvavg2id
      integer(4) :: updatevbarid
      integer(4) :: updateduavg2id
      integer(4) :: updateubarid
      integer(4) :: updatezetaid
      integer(4) :: j2v
      integer(4) :: j1v
      integer(4) :: i2u
      integer(4) :: i1u
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: j2t
      integer(4) :: j1t
      integer(4) :: i2t
      integer(4) :: i1t
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm

                     
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                         
      integer(4) :: irhot
      i1t = 0
      i2t = Lm+1
      j1t = 0
      j2t = Mm+1
      if (WEST_INTER) then
        i1t = -1
      endif
      if (EAST_INTER) then
        i2t = Lmmpi+2
      else
        i2t = Lmmpi+1
      endif
      if (SOUTH_INTER) then
        j1t = -1
      endif
      if (NORTH_INTER) then
        j2t = Mmmpi+2
      else
        j2t = Mmmpi+1
      endif
      i1u = i1t
      i2u = i2t
      j1v = j1t
      j2v = j2t
      irhot = Agrif_Irhot()
      call Agrif_Declare_Variable((/2,2,0/),(/1,1,0/),
     &    (/'x','y','N'/),(/i1t,j1t,1/),
     &    (/i2t,j2t,irhot+1/),updatezetaid)
      call Agrif_Set_UpdateType(updatezetaid,update=
     &Agrif_Update_full_weighting)
      call Agrif_Declare_Variable((/1,2,0/),(/1,1,0/),
     &    (/'x','y','N'/),(/0,j1t,1/),
     &    (/i2u,j2t,irhot+1/),updateubarid)
      call Agrif_Declare_Variable((/1,2/),(/1,1/),
     &    (/'x','y'/),(/0,j1t/),
     &    (/i2u,j2t/),updateduavg2id)
      call Agrif_Set_UpdateType(updateubarid,
     &update1=Agrif_Update_Average,
     &update2=Agrif_Update_Full_Weighting)
      call Agrif_Set_UpdateType(updateduavg2id,
     &update1=Agrif_Update_Average,
     &update2=Agrif_Update_Full_Weighting)
      call Agrif_Declare_Variable((/2,1,0/),(/1,1,0/),
     &    (/'x','y','N'/),(/i1t,0,1/),
     &    (/i2t,j2v,irhot+1/),updatevbarid)
      call Agrif_Declare_Variable((/2,1/),(/1,1/),
     &    (/'x','y'/),(/i1t,0/),
     &    (/i2t,j2v/),updatedvavg2id)
      call Agrif_Set_UpdateType(updatevbarid,
     &update1=Agrif_Update_Full_Weighting,
     &update2=Agrif_Update_Average)
      call Agrif_Set_UpdateType(updatedvavg2id,
     &update1=Agrif_Update_Full_Weighting,
     &update2=Agrif_Update_Average)
      call Agrif_Declare_Variable((/2,2,0,0/),(/1,1,0,0/),
     &    (/'x','y','N','N'/),(/i1t,j1t,1,1/),
     &    (/i2t,j2t,N,NT/),updatetid)
      call Agrif_Set_UpdateType(updatetid,update=
     &Agrif_Update_full_weighting)
      call Agrif_Declare_Variable((/1,2,0/),(/1,1,0/),
     &    (/'x','y','N'/),(/i1u,j1t,1/),
     &    (/i2u,j2t,N/),updateuid)
      call Agrif_Set_UpdateType(updateuid,
     &update1=Agrif_Update_Average,
     &update2=Agrif_Update_Full_Weighting)
      call Agrif_Declare_Variable((/1,2,0/),(/1,1,0/),
     &    (/'x','y','N'/),(/i1u,j1t,1/),
     &    (/i2u,j2t,N/),updatehuonid)
      call Agrif_Set_UpdateType(updatehuonid,
     &update1=Agrif_Update_Average,
     &update2=Agrif_Update_Full_Weighting)
      call Agrif_Declare_Variable((/1,2,0,0/),(/1,1,0,0/),
     &    (/'x','y','N','N'/),(/i1u,j1t,1,1/),
     &    (/i2u,j2t,N,NT/),updatemyfxid)
      call Agrif_Set_UpdateType(updatemyfxid,
     &update1=Agrif_Update_Average,
     &update2=Agrif_Update_Full_Weighting)
      call Agrif_Declare_Variable((/2,1,0/),(/1,1,0/),
     &    (/'x','y','N'/),(/i1t,j1v,1/),
     &    (/i2t,j2v,N/),updatevid)
      call Agrif_Set_UpdateType(updatevid,
     &update1=Agrif_Update_Full_Weighting,
     &update2=Agrif_Update_Average)
      call Agrif_Declare_Variable((/2,1,0/),(/1,1,0/),
     &    (/'x','y','N'/),(/i1t,j1v,1/),
     &    (/i2t,j2v,N/),updatehvomid)
      call Agrif_Set_UpdateType(updatehvomid,
     &update1=Agrif_Update_Full_Weighting,
     &update2=Agrif_Update_Average)
      call Agrif_Declare_Variable((/2,1,0,0/),(/1,1,0,0/),
     &    (/'x','y','N','N'/),(/i1t,j1v,1,1/),
     &    (/i2t,j2v,N,NT/),updatemyfyid)
      call Agrif_Set_UpdateType(updatemyfyid,
     &update1=Agrif_Update_Full_Weighting,
     &update2=Agrif_Update_Average)
        call Agrif_Declare_Variable((/2,2/),(/1,1/),(/'x','y'/),
     &   (/i1t,j1t/),(/i2t,j2t/),hid)
        call Agrif_Declare_Variable((/2,2/),(/1,1/),(/'x','y'/),
     &   (/i1t,j1t/),(/i2t,j2t/),rmaskid)
        call Agrif_Declare_Variable((/2,2/),(/1,1/),(/'x','y'/),
     &   (/i1t,j1t/),(/i2t,j2t/),zetaid)
        call Agrif_Declare_Variable((/1,2/),(/1,1/),(/'x','y'/),
     &   (/0,j1t/),(/i2u,j2t/),ubarid)
        call Agrif_Declare_Variable((/2,1/),(/1,1/),(/'x','y'/),
     &   (/i1t,0/),(/i2t,j2v/),vbarid)
        call Agrif_Declare_Variable((/2,2,0,0/),(/1,1,0,0/),
     &    (/'x','y','N','N'/),(/i1t,j1t,1,1/),
     &    (/i2t,j2t,N,NT/),tid)
        call Agrif_Declare_Variable((/2,2,0,0/),(/1,1,0,0/),
     &    (/'x','y','N','N'/),(/i1t,j1t,1,1/),
     &    (/i2t,j2t,N,NT/),tspongeid)
        call Agrif_Declare_Variable((/1,2,0/),(/1,1,0/),
     &   (/'x','y','N'/),(/i1u,j1t,1/),
     &   (/i2u,j2t,N/),uid)
        call Agrif_Declare_Variable((/1,2,0/),(/1,1,0/),
     &   (/'x','y','N'/),(/i1u,j1t,1/),
     &   (/i2u,j2t,N/),uspongeid)
        call Agrif_Declare_Variable((/2,1,0/),(/1,1,0/),
     &   (/'x','y','N'/),(/i1t,j1v,1/),
     &   (/i2t,j2v,N/),vid)
        call Agrif_Declare_Variable((/2,1,0/),(/1,1,0/),
     &   (/'x','y','N'/),(/i1t,j1v,1/),
     &   (/i2t,j2v,N/),vspongeid)
        call Agrif_Set_bc(hid,(/0,0/))
        call Agrif_Set_bc(zetaid,(/-1,0/))
        call Agrif_Set_bc(ubarid,(/0,0/))
        call Agrif_Set_bc(vbarid,(/0,0/))
        call Agrif_Set_bc(tid,(/-1,0/))
        call Agrif_Set_bc(uid,(/0,0/))
        call Agrif_Set_bc(vid,(/0,0/))
        call Agrif_Set_bcinterp(hid,   interp=Agrif_linear)
        call Agrif_Set_interp(hid,   interp=Agrif_linear)
        call Agrif_Set_bcinterp(zetaid,interp=Agrif_lagrange)
        call Agrif_Set_bcinterp(ubarid,interp1=Agrif_lagrange,
     &                                 interp2=Agrif_ppm)
        call Agrif_Set_bcinterp(vbarid,interp1=Agrif_ppm,
     &                                 interp2=Agrif_lagrange)
        call Agrif_Set_bcinterp(tid,interp=Agrif_lagrange)
        call Agrif_Set_bcinterp(tspongeid,interp=Agrif_lagrange)
        call Agrif_Set_bcinterp(uid,interp1=Agrif_lagrange,
     &                                 interp2=Agrif_ppm)
        call Agrif_Set_bcinterp(uspongeid,interp1=Agrif_lagrange,
     &                                 interp2=Agrif_ppm)
        call Agrif_Set_bcinterp(vid,interp1=Agrif_ppm,
     &                                 interp2=Agrif_lagrange)
        call Agrif_Set_bcinterp(vspongeid,interp1=Agrif_ppm,
     &                                 interp2=Agrif_lagrange)
      call Agrif_Set_UpdateType(rmaskid,update=
     &Agrif_Update_Average)
      call Agrif_Set_UpdateType(hid,update=
     &Agrif_Update_full_weighting)
             

      end subroutine Sub_Loop_declare_zoom_variables


       recursive subroutine ResetMask()      

      use Agrif_Util
      interface
        recursive subroutine Sub_Loop_ResetMask(padd_E,Mm,padd_X,Lm,rmas
     &kid,gamma2,pmask2,pmask,vmask,umask,rmask,Mmmpi,Lmmpi)
          use Agrif_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: rmaskid
      real :: gamma2
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
        end subroutine Sub_Loop_ResetMask

      end interface
      

        call Sub_Loop_ResetMask( Agrif_tabvars_i(188)%iarray0, Agrif_tab
     &vars_i(197)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i
     &(200)%iarray0, Agrif_tabvars_i(13)%iarray0, Agrif_tabvars_r(28)%ar
     &ray0, Agrif_tabvars(47)%array2, Agrif_tabvars(50)%array2, Agrif_ta
     &bvars(48)%array2, Agrif_tabvars(49)%array2, Agrif_tabvars(51)%arra
     &y2, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      recursive subroutine Sub_Loop_ResetMask(padd_E,Mm,padd_X,Lm,rmaski
     &d,gamma2,pmask2,pmask,vmask,umask,rmask,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      use Agrif_Util

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: rmaskid
      real :: gamma2
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask

                      
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                    
      integer(4) :: i
      integer(4) :: j
      external updatermask
      call exchange_r2d_tile (1,Lm,1,Mm,  rmask)
      do j=0,Mm+1
        do i=1,Lm+1
          umask(i,j) = rmask(i,j)*rmask(i-1,j)
         enddo
      enddo
       do j=1,Mm+1
       do i=0,Lm+1
        vmask(i,j) = rmask(i,j)*rmask(i,j-1)
       enddo
       enddo
       do j=1,Mm+1
       do i=1,Lm+1
          pmask(i,j)=rmask(i,j)*rmask(i-1,j)*rmask(i,j-1)
     &                                      *rmask(i-1,j-1)
          pmask2(i,j)=pmask(i,j)
          if (gamma2.lt.0.D0) pmask(i,j)=2.D0-pmask(i,j)
        enddo
      enddo
      call exchange_u2d_tile (1,Lm,1,Mm,  umask)
      call exchange_v2d_tile (1,Lm,1,Mm,  vmask)
      call exchange_p2d_tile (1,Lm,1,Mm,  pmask)
      call exchange_p2d_tile (1,Lm,1,Mm,  pmask2)
      if (.Not.Agrif_Root()) then
        Agrif_UseSpecialValueInUpdate = .TRUE.
        Agrif_SpecialValueFineGrid = 0.D0
        call Agrif_Update_Variable(rmaskid,procname=updatermask)
        Agrif_UseSpecialValueInUpdate = .FALSE.
        call Agrif_ChildGrid_To_ParentGrid()
        call ResetMask()
        call Agrif_ParentGrid_To_ChildGrid()
      endif
             

      end subroutine Sub_Loop_ResetMask


      Subroutine Updateh(tabres,i1,i2,j1,j2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updateh(tabres,i1,i2,j1,j2,before,padd_E,Mm,
     &padd_X,Lm,Hz,Hz_bak,z_r,Zt_avg1,Cs_r,sc_r,Cs_w,sc_w,z_w,hinv,hc,rm
     &ask,h,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: hc
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz_bak
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      real, dimension(1:N) :: Cs_r
      real, dimension(1:N) :: sc_r
      real, dimension(0:N) :: Cs_w
      real, dimension(0:N) :: sc_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: z_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hinv
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
        end subroutine Sub_Loop_Updateh

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      

        call Sub_Loop_Updateh(tabres,i1,i2,j1,j2,before, Agrif_tabvars_i
     &(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%
     &iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(104)%array3, 
     &Agrif_tabvars(108)%array3, Agrif_tabvars(103)%array3, Agrif_tabvar
     &s(116)%array2, Agrif_tabvars(90)%array1, Agrif_tabvars(91)%array1,
     & Agrif_tabvars(92)%array1, Agrif_tabvars(93)%array1, Agrif_tabvars
     &(107)%array3, Agrif_tabvars(84)%array2, Agrif_tabvars_r(24)%array0
     &, Agrif_tabvars(51)%array2, Agrif_tabvars(85)%array2, Agrif_tabvar
     &s_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_Updateh(tabres,i1,i2,j1,j2,before,padd_E,Mm,pa
     &dd_X,Lm,Hz,Hz_bak,z_r,Zt_avg1,Cs_r,sc_r,Cs_w,sc_w,z_w,hinv,hc,rmas
     &k,h,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: hc
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz_bak
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      real, dimension(1:N) :: Cs_r
      real, dimension(1:N) :: sc_r
      real, dimension(0:N) :: Cs_w
      real, dimension(0:N) :: sc_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: z_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hinv
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                            
                               
                     
                                                
      real :: cff_r
      real :: cff1_r
      real :: cff_w
      real :: cff1_w
      real :: z_r0
      real :: z_w0
                      
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                 
      real, dimension(i1:i2,j1:j2) :: tabtemp
      if (before) then
        tabres(i1:i2,j1:j2) = h(i1:i2,j1:j2)
     &                        *rmask(i1:i2,j1:j2)
       else
         do j=j1,j2
           do i=i1,i2
             h(i,j)=max(tabres(i,j),hc)
             hinv(i,j)=1.D0/h(i,j)
           enddo
         enddo
         do j=j1,j2
           do i=i1,i2
             z_w(i,j,0) = -h(i,j)
           enddo
           do k=1,N,+1
             cff_w =hc*(sc_w(k)-Cs_w(k))
             cff_r =hc*(sc_r(k)-Cs_r(k))
             cff1_w=Cs_w(k)
             cff1_r=Cs_r(k)
             do i=i1,i2
               z_w0=cff_w+cff1_w*h(i,j)
               z_r0=cff_r+cff1_r*h(i,j)
               z_w(i,j,k)=z_w0+Zt_avg1(i,j)*(1.D0+z_w0*hinv(i,j))
               z_r(i,j,k)=z_r0+Zt_avg1(i,j)*(1.D0+z_r0*hinv(i,j))
               Hz_bak(i,j,k)=Hz(i,j,k)
               Hz(i,j,k)=z_w(i,j,k)-z_w(i,j,k-1)
             enddo
           enddo
         enddo
       endif
      return
       
      

      end subroutine Sub_Loop_Updateh

      Subroutine Updatermask(tabres,i1,i2,j1,j2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updatermask(tabres,i1,i2,j1,j2,before,padd_E
     &,Mm,padd_X,Lm,rmask,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
        end subroutine Sub_Loop_Updatermask

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      

        call Sub_Loop_Updatermask(tabres,i1,i2,j1,j2,before, Agrif_tabva
     &rs_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(1
     &89)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(51)%array
     &2, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_Updatermask(tabres,i1,i2,j1,j2,before,padd_E,M
     &m,padd_X,Lm,rmask,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                            
                               
                     
                      
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      if (before) then
        tabres(i1:i2,j1:j2) = rmask(i1:i2,j1:j2)
      else
        rmask(i1:i2,j1:j2)=tabres(i1:i2,j1:j2)
      endif
      return
       
      

      end subroutine Sub_Loop_Updatermask

      recursive Subroutine UpdateGridhis()      


      use Agrif_Util
      interface
        recursive subroutine Sub_Loop_UpdateGridhis(padd_E,Mm,padd_X,Lm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
        end subroutine Sub_Loop_UpdateGridhis

      end interface
      

        call Sub_Loop_UpdateGridhis( Agrif_tabvars_i(188)%iarray0, Agrif
     &_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabva
     &rs_i(200)%iarray0)

      end


      recursive subroutine Sub_Loop_UpdateGridhis(padd_E,Mm,padd_X,Lm)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
      call wrt_his
      if (.Not.Agrif_Root()) Then
        call Agrif_ChildGrid_To_ParentGrid()
        call UpdateGridhis()
        call Agrif_ParentGrid_To_ChildGrid()
      endif
             

      end subroutine Sub_Loop_UpdateGridhis


      subroutine hinterp(tabres,i1,i2,j1,j2,before,nb,ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_hinterp(tabres,i1,i2,j1,j2,before,nb,ndir,pa
     &dd_E,Mm,padd_X,Lm,Mmmpi,Lmmpi,hinv,h)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hinv
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      logical :: before
      real, dimension(i1:i2,j1:j2) :: tabres
      integer(4) :: nb
      integer(4) :: ndir
        end subroutine Sub_Loop_hinterp

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      logical :: before
      real, dimension(i1:i2,j1:j2) :: tabres
      integer(4) :: nb
      integer(4) :: ndir
      

        call Sub_Loop_hinterp(tabres,i1,i2,j1,j2,before,nb,ndir, Agrif_t
     &abvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars
     &_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(194
     &)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars(84)%array2,
     & Agrif_tabvars(85)%array2)

      end


      subroutine Sub_Loop_hinterp(tabres,i1,i2,j1,j2,before,nb,ndir,padd
     &_E,Mm,padd_X,Lm,Mmmpi,Lmmpi,hinv,h)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hinv
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      logical :: before
      real, dimension(i1:i2,j1:j2) :: tabres
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

!$AGRIF_DO_NOT_TREAT
       integer*4 isens
       common/interph/isens
!$AGRIF_END_DO_NOT_TREAT
                             
                      
                                
                     
      integer(4) :: i
      integer(4) :: j
                         
                                             
      logical :: western_side
      logical :: eastern_side
                                              
      logical :: northern_side
      logical :: southern_side
       if (before) then
         if (isens == 1) THEN
           do j=j1,j2
             do i=max(i1,lbound(h,1)+1),i2
               tabres(i,j) = 0.5D0*(h(i-1,j)+h(i,j))
             enddo
           enddo
         else
           do j=max(j1,lbound(h,2)+1),j2
             do i=i1,i2
               tabres(i,j) = 0.5D0*(h(i,j-1)+h(i,j))
             enddo
           enddo
         endif
       else
         western_side  = (nb == 1).AND.(ndir == 1)
         eastern_side  = (nb == 1).AND.(ndir == 2)
         southern_side = (nb == 2).AND.(ndir == 1)
         northern_side = (nb == 2).AND.(ndir == 2)
         if (isens == 1) then
           if ( western_side ) then
             do j=j1,j2
               h(0,j) = 2.D0*tabres(1,j)-h(1,j)
               hinv(0,j) = 1.D0/h(0,j)
             enddo
           endif
           if ( eastern_side ) then
             do j=j1,j2
               h(Lmmpi+1,j) = 2.D0*tabres(Lmmpi+1,j)-h(Lmmpi,j)
               hinv(Lmmpi+1,j) = 1.D0/h(Lmmpi+1,j)
             enddo
           endif
         elseif (isens == 2) then
           if ( southern_side ) then
             do i=i1,i2
               h(i,0) = 2.D0*tabres(i,1)-h(i,1)
               hinv(i,0) = 1.D0/h(i,0)
             enddo
           endif
           if ( northern_side ) then
             do i=i1,i2
               h(i,Mmmpi+1) = 2.D0*tabres(i,Mmmpi+1)-h(i,Mmmpi)
               hinv(i,Mmmpi+1) = 1.D0/h(i,Mmmpi+1)
             enddo
           endif
         endif
        endif
      return
       
      

      end subroutine Sub_Loop_hinterp

      subroutine hinterp_old(tabres,i1,i2,j1,j2)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_hinterp_old(tabres,i1,i2,j1,j2,padd_E,Mm,pad
     &d_X,Lm,h)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
        end subroutine Sub_Loop_hinterp_old

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      

        call Sub_Loop_hinterp_old(tabres,i1,i2,j1,j2, Agrif_tabvars_i(18
     &8)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%iar
     &ray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(85)%array2)

      end


      subroutine Sub_Loop_hinterp_old(tabres,i1,i2,j1,j2,padd_E,Mm,padd_
     &X,Lm,h)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

!$AGRIF_DO_NOT_TREAT
       integer*4 isens
       common/interph/isens
!$AGRIF_END_DO_NOT_TREAT
                             
                                
                     
      integer(4) :: i
      integer(4) :: j
       if (isens == 1) THEN
         do j=j1,j2
           do i=max(i1,lbound(h,1)+1),i2
             tabres(i,j) = 0.5D0*(h(i-1,j)+h(i,j))
           enddo
         enddo
       else
         do j=max(j1,lbound(h,2)+1),j2
           do i=i1,i2
             tabres(i,j) = 0.5D0*(h(i,j-1)+h(i,j))
           enddo
         enddo
       endif
      return
       
      

      end subroutine Sub_Loop_hinterp_old

        Subroutine Agrif_Invloc(indloc,proc,dir,indglob)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Agrif_Invloc(indloc,proc,dir,indglob,jminmpi
     &,iminmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: jminmpi
      integer(4) :: iminmpi
      integer(4) :: indloc
      integer(4) :: proc
      integer(4) :: dir
      integer(4) :: indglob
        end subroutine Sub_Loop_Agrif_Invloc

      end interface
      integer(4) :: indloc
      integer(4) :: proc
      integer(4) :: dir
      integer(4) :: indglob
      

        call Sub_Loop_Agrif_Invloc(indloc,proc,dir,indglob, Agrif_tabvar
     &s_i(191)%iarray0, Agrif_tabvars_i(193)%iarray0)

      end


      subroutine Sub_Loop_Agrif_Invloc(indloc,proc,dir,indglob,jminmpi,i
     &minmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: jminmpi
      integer(4) :: iminmpi
      integer(4) :: indloc
      integer(4) :: proc
      integer(4) :: dir
      integer(4) :: indglob

                     

                                             
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

        If (dir == 1) Then
           indglob = indloc + iminmpi-1
        Else If (dir == 2) Then
           indglob = indloc + jminmpi-1
        Else
           indglob = indloc
        End If
        Return
         
      

      end subroutine Sub_Loop_Agrif_Invloc

        subroutine Agrif_get_proc_info ( oimin, oimax, ojmin, ojmax )      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Agrif_get_proc_info(oimin,oimax,ojmin,ojmax,
     &jmaxmpi,jminmpi,imaxmpi,iminmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: jmaxmpi
      integer(4) :: jminmpi
      integer(4) :: imaxmpi
      integer(4) :: iminmpi
      integer(4), intent(out) :: oimin
      integer(4), intent(out) :: oimax
      integer(4), intent(out) :: ojmin
      integer(4), intent(out) :: ojmax
        end subroutine Sub_Loop_Agrif_get_proc_info

      end interface
      integer(4), intent(out) :: oimin
      integer(4), intent(out) :: oimax
      integer(4), intent(out) :: ojmin
      integer(4), intent(out) :: ojmax
      

        call Sub_Loop_Agrif_get_proc_info(oimin,oimax,ojmin,ojmax, Agrif
     &_tabvars_i(190)%iarray0, Agrif_tabvars_i(191)%iarray0, Agrif_tabva
     &rs_i(192)%iarray0, Agrif_tabvars_i(193)%iarray0)

      end


      subroutine Sub_Loop_Agrif_get_proc_info(oimin,oimax,ojmin,ojmax,jm
     &axmpi,jminmpi,imaxmpi,iminmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: jmaxmpi
      integer(4) :: jminmpi
      integer(4) :: imaxmpi
      integer(4) :: iminmpi
      integer(4), intent(out) :: oimin
      integer(4), intent(out) :: oimax
      integer(4), intent(out) :: ojmin
      integer(4), intent(out) :: ojmax

                     

                                               
                                               
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

        oimin = iminmpi
        oimax = imaxmpi
        ojmin = jminmpi
        ojmax = jmaxmpi
               

      end subroutine Sub_Loop_Agrif_get_proc_info


        subroutine Agrif_estimate_parallel_cost(i1,i2,j1,j2,nbprocs,
     &                                                        grid_cost)
      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), intent(in) :: i1
      integer(4), intent(in) :: i2
      integer(4), intent(in) :: j1
      integer(4), intent(in) :: j2
      integer(4), intent(in) :: nbprocs
      real, intent(out) :: grid_cost

                     

                                                          
                                          
        grid_cost = (i2-i1+1)*(j2-j1+1) / real(nbprocs)
        end subroutine agrif_estimate_parallel_cost
      SUBROUTINE Agrif_detect(taberr,sizexy)      

      use AGRIF_types

      use Agrif_Util
      interface
        subroutine Sub_Loop_Agrif_detect(taberr,sizexy,padd_E,Mm,padd_X,
     &Lm,u,nnew,v,Mmmpi,Lmmpi,N3d,N2d)
          use AGRIF_types
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      integer(4) :: N2d
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      Integer(4), dimension(1:2) :: sizexy
      Integer(4), dimension(1:sizexy(1),1:sizexy(2)) :: taberr
        end subroutine Sub_Loop_Agrif_detect

      end interface
      Integer(4), dimension(1:2) :: sizexy
      Integer(4), dimension(1:sizexy(1),1:sizexy(2)) :: taberr
      

        call Sub_Loop_Agrif_detect(taberr,sizexy, Agrif_tabvars_i(188)%i
     &array0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%iarray0
     &, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(111)%array4, Agrif_t
     &abvars_i(174)%iarray0, Agrif_tabvars(110)%array4, Agrif_tabvars_i(
     &194)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars_i(186)%i
     &array0, Agrif_tabvars_i(187)%iarray0)

      end


      subroutine Sub_Loop_Agrif_detect(taberr,sizexy,padd_E,Mm,padd_X,Lm
     &,u,nnew,v,Mmmpi,Lmmpi,N3d,N2d)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_types
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      integer(4) :: N2d
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      Integer(4), dimension(1:2) :: sizexy
      Integer(4), dimension(1:sizexy(1),1:sizexy(2)) :: taberr

                      
                   

                                      
      integer(4) :: tile
      integer(4) :: ierr
      integer(4) :: trd
      integer(4) :: subs
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                        
                                                           
                                                
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vort
                    
      integer(4) :: i
      integer(4) :: j
                
      real :: crit
      vort = 0.D0
      do j=1,Mm+1
      do i=1,Lm+1
        vort(i,j) = (v(i,j,N,nnew)-v(i-1,j,N,nnew))
     &     -(u(i,j,N,nnew)-u(i,j-1,N,nnew))
      enddo
      enddo
      crit = maxval(abs(vort))
      taberr=0.D0
      do j=1,Mm+1
      do i=1,Lm+1
          if (abs(vort(i,j)) > 0.8D0*crit) then
            taberr(i,j) = 1
          endif
      enddo
      enddo
      Return
             

      end subroutine Sub_Loop_Agrif_detect


      Subroutine Agrif_Before_Regridding()      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Agrif_Before_Regridding(padd_E,Mm,padd_X,Lm,
     &tid,t,vid,v,uid,u,vbarid,DV_avg1,ubarid,nnew,DU_avg1,zetaid,Zt_avg
     &1,Mmmpi,Lmmpi,N3d,N2d)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: tid
      integer(4) :: vid
      integer(4) :: uid
      integer(4) :: vbarid
      integer(4) :: ubarid
      integer(4) :: nnew
      integer(4) :: zetaid
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      integer(4) :: N2d
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
        end subroutine Sub_Loop_Agrif_Before_Regridding

      end interface
      

        call Sub_Loop_Agrif_Before_Regridding( Agrif_tabvars_i(188)%iarr
     &ay0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%iarray0, A
     &grif_tabvars_i(200)%iarray0, Agrif_tabvars_i(17)%iarray0, Agrif_ta
     &bvars(109)%array5, Agrif_tabvars_i(18)%iarray0, Agrif_tabvars(110)
     &%array4, Agrif_tabvars_i(19)%iarray0, Agrif_tabvars(111)%array4, A
     &grif_tabvars_i(20)%iarray0, Agrif_tabvars(114)%array3, Agrif_tabva
     &rs_i(21)%iarray0, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars(115)
     &%array3, Agrif_tabvars_i(22)%iarray0, Agrif_tabvars(116)%array2, A
     &grif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_t
     &abvars_i(186)%iarray0, Agrif_tabvars_i(187)%iarray0)

      end


      subroutine Sub_Loop_Agrif_Before_Regridding(padd_E,Mm,padd_X,Lm,ti
     &d,t,vid,v,uid,u,vbarid,DV_avg1,ubarid,nnew,DU_avg1,zetaid,Zt_avg1,
     &Mmmpi,Lmmpi,N3d,N2d)

      use Agrif_Util


      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: tid
      integer(4) :: vid
      integer(4) :: uid
      integer(4) :: vbarid
      integer(4) :: ubarid
      integer(4) :: nnew
      integer(4) :: zetaid
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      integer(4) :: N2d
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                    
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: tabtemp2d
                                                      
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: tabtemp3d
                                                    
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: text
                     
      integer(4) :: itrc
      tabtemp2d = Zt_avg1
      call Agrif_Save_ForRestore(tabtemp2d,zetaid)
      tabtemp2d = Agrif_irhoy()*DU_avg1(:,:,nnew)
      call Agrif_Save_ForRestore(tabtemp2d,ubarid)
      tabtemp2d = Agrif_irhox()*DV_avg1(:,:,nnew)
      call Agrif_Save_ForRestore(tabtemp2d,vbarid)
      tabtemp3d = u(:,:,:,nnew)
      call Agrif_Save_ForRestore(tabtemp3d,uid)
      tabtemp3d = v(:,:,:,nnew)
      call Agrif_Save_ForRestore(tabtemp3d,vid)
      do itrc=1,NT
            text(:,:,:,itrc) = t(:,:,:,nnew,itrc)
      enddo
      call Agrif_Save_ForRestore(text,tid)
      Return
             

      end subroutine Sub_Loop_Agrif_Before_Regridding


      subroutine inith(tabres,i1,i2,j1,j2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_inith(tabres,i1,i2,j1,j2,before,padd_E,Mm,pa
     &dd_X,Lm,h,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
        end subroutine Sub_Loop_inith

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      

        call Sub_Loop_inith(tabres,i1,i2,j1,j2,before, Agrif_tabvars_i(1
     &88)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%ia
     &rray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(85)%array2, Agr
     &if_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_inith(tabres,i1,i2,j1,j2,before,padd_E,Mm,padd
     &_X,Lm,h,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                             
                                
                      
       if (before) then
         tabres = h(i1:i2,j1:j2)
       else
         h(i1:i2,j1:j2) = tabres
       endif
      return
       
      

      end subroutine Sub_Loop_inith

      subroutine initzeta(tabres,i1,i2,j1,j2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_initzeta(tabres,i1,i2,j1,j2,before,padd_E,Mm
     &,padd_X,Lm,zeta,Zt_avg1,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
        end subroutine Sub_Loop_initzeta

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      

        call Sub_Loop_initzeta(tabres,i1,i2,j1,j2,before, Agrif_tabvars_
     &i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)
     &%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(98)%array3, 
     &Agrif_tabvars(116)%array2, Agrif_tabvars_i(194)%iarray0, Agrif_tab
     &vars_i(195)%iarray0)

      end


      subroutine Sub_Loop_initzeta(tabres,i1,i2,j1,j2,before,padd_E,Mm,p
     &add_X,Lm,zeta,Zt_avg1,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                             
                                
                      
       if (before) then
         tabres = Zt_avg1(i1:i2,j1:j2)
       else
         zeta(i1:i2,j1:j2,1) = tabres
       endif
      return
       
      

      end subroutine Sub_Loop_initzeta

      subroutine initzeta_old(tabres,i1,i2,j1,j2)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_initzeta_old(tabres,i1,i2,j1,j2,padd_E,Mm,pa
     &dd_X,Lm,Zt_avg1,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
        end subroutine Sub_Loop_initzeta_old

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      

        call Sub_Loop_initzeta_old(tabres,i1,i2,j1,j2, Agrif_tabvars_i(1
     &88)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%ia
     &rray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(116)%array2, Ag
     &rif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_initzeta_old(tabres,i1,i2,j1,j2,padd_E,Mm,padd
     &_X,Lm,Zt_avg1,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                             
                                
       tabres = Zt_avg1(i1:i2,j1:j2)
      return
       
      

      end subroutine Sub_Loop_initzeta_old

      subroutine initubar(tabres,i1,i2,j1,j2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_initubar(tabres,i1,i2,j1,j2,before,padd_E,Mm
     &,padd_X,Lm,nnew,DU_avg1,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
        end subroutine Sub_Loop_initubar

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      

        call Sub_Loop_initubar(tabres,i1,i2,j1,j2,before, Agrif_tabvars_
     &i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)
     &%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(174)%iarra
     &y0, Agrif_tabvars(115)%array3, Agrif_tabvars_i(194)%iarray0, Agrif
     &_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_initubar(tabres,i1,i2,j1,j2,before,padd_E,Mm,p
     &add_X,Lm,nnew,DU_avg1,Mmmpi,Lmmpi)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                             
                                
                      
       if (before) then
         tabres = DU_avg1(i1:i2,j1:j2,nnew)
       else
         DU_avg1(i1:i2,j1:j2,1) = tabres / Agrif_irhoy()
       endif
      return
       
      

      end subroutine Sub_Loop_initubar

      subroutine initubar_old(tabres,i1,i2,j1,j2)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_initubar_old(tabres,i1,i2,j1,j2,padd_E,Mm,pa
     &dd_X,Lm,nnew,DU_avg1,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
        end subroutine Sub_Loop_initubar_old

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      

        call Sub_Loop_initubar_old(tabres,i1,i2,j1,j2, Agrif_tabvars_i(1
     &88)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%ia
     &rray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(174)%iarray0,
     & Agrif_tabvars(115)%array3, Agrif_tabvars_i(194)%iarray0, Agrif_ta
     &bvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_initubar_old(tabres,i1,i2,j1,j2,padd_E,Mm,padd
     &_X,Lm,nnew,DU_avg1,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                             
                                
       tabres = DU_avg1(i1:i2,j1:j2,nnew)
      return
       
      

      end subroutine Sub_Loop_initubar_old

      subroutine initvbar(tabres,i1,i2,j1,j2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_initvbar(tabres,i1,i2,j1,j2,before,padd_E,Mm
     &,padd_X,Lm,nnew,DV_avg1,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
        end subroutine Sub_Loop_initvbar

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      

        call Sub_Loop_initvbar(tabres,i1,i2,j1,j2,before, Agrif_tabvars_
     &i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)
     &%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(174)%iarra
     &y0, Agrif_tabvars(114)%array3, Agrif_tabvars_i(194)%iarray0, Agrif
     &_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_initvbar(tabres,i1,i2,j1,j2,before,padd_E,Mm,p
     &add_X,Lm,nnew,DV_avg1,Mmmpi,Lmmpi)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                             
                                
                      
       if (before) then
         tabres = DV_avg1(i1:i2,j1:j2,nnew)
       else
         DV_avg1(i1:i2,j1:j2,1) = tabres / Agrif_irhox()
       endif
      return
       
      

      end subroutine Sub_Loop_initvbar

      subroutine initvbar_old(tabres,i1,i2,j1,j2)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_initvbar_old(tabres,i1,i2,j1,j2,padd_E,Mm,pa
     &dd_X,Lm,nnew,DV_avg1,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
        end subroutine Sub_Loop_initvbar_old

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      

        call Sub_Loop_initvbar_old(tabres,i1,i2,j1,j2, Agrif_tabvars_i(1
     &88)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%ia
     &rray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(174)%iarray0,
     & Agrif_tabvars(114)%array3, Agrif_tabvars_i(194)%iarray0, Agrif_ta
     &bvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_initvbar_old(tabres,i1,i2,j1,j2,padd_E,Mm,padd
     &_X,Lm,nnew,DV_avg1,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                             
                                
       tabres = DV_avg1(i1:i2,j1:j2,nnew)
      return
       
      

      end subroutine Sub_Loop_initvbar_old

      subroutine initu(tabres,i1,i2,j1,j2,k1,k2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_initu(tabres,i1,i2,j1,j2,k1,k2,before,padd_E
     &,Mm,padd_X,Lm,nnew,u,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
        end subroutine Sub_Loop_initu

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      

        call Sub_Loop_initu(tabres,i1,i2,j1,j2,k1,k2,before, Agrif_tabva
     &rs_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(1
     &89)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(174)%ia
     &rray0, Agrif_tabvars(111)%array4, Agrif_tabvars_i(194)%iarray0, Ag
     &rif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_initu(tabres,i1,i2,j1,j2,k1,k2,before,padd_E,M
     &m,padd_X,Lm,nnew,u,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                   
                                      
                      
       if (before) then
         tabres = u(i1:i2,j1:j2,k1:k2,nnew)
       else
         u(i1:i2,j1:j2,k1:k2,1) = tabres
       endif
      return
       
      

      end subroutine Sub_Loop_initu

      subroutine initu_old(tabres,i1,i2,j1,j2,k1,k2)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_initu_old(tabres,i1,i2,j1,j2,k1,k2,padd_E,Mm
     &,padd_X,Lm,nnew,u,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
        end subroutine Sub_Loop_initu_old

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      

        call Sub_Loop_initu_old(tabres,i1,i2,j1,j2,k1,k2, Agrif_tabvars_
     &i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)
     &%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(174)%iarra
     &y0, Agrif_tabvars(111)%array4, Agrif_tabvars_i(194)%iarray0, Agrif
     &_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_initu_old(tabres,i1,i2,j1,j2,k1,k2,padd_E,Mm,p
     &add_X,Lm,nnew,u,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                   
                                      
       tabres = u(i1:i2,j1:j2,k1:k2,nnew)
      return
       
      

      end subroutine Sub_Loop_initu_old

      subroutine initv(tabres,i1,i2,j1,j2,k1,k2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_initv(tabres,i1,i2,j1,j2,k1,k2,before,padd_E
     &,Mm,padd_X,Lm,nnew,v,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
        end subroutine Sub_Loop_initv

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      

        call Sub_Loop_initv(tabres,i1,i2,j1,j2,k1,k2,before, Agrif_tabva
     &rs_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(1
     &89)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(174)%ia
     &rray0, Agrif_tabvars(110)%array4, Agrif_tabvars_i(194)%iarray0, Ag
     &rif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_initv(tabres,i1,i2,j1,j2,k1,k2,before,padd_E,M
     &m,padd_X,Lm,nnew,v,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                   
                                      
                      
       if (before) then
         tabres = v(i1:i2,j1:j2,k1:k2,nnew)
       else
         v(i1:i2,j1:j2,k1:k2,1) = tabres
       endif
      return
       
      

      end subroutine Sub_Loop_initv

      subroutine initv_old(tabres,i1,i2,j1,j2,k1,k2)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_initv_old(tabres,i1,i2,j1,j2,k1,k2,padd_E,Mm
     &,padd_X,Lm,nnew,v,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
        end subroutine Sub_Loop_initv_old

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      

        call Sub_Loop_initv_old(tabres,i1,i2,j1,j2,k1,k2, Agrif_tabvars_
     &i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)
     &%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(174)%iarra
     &y0, Agrif_tabvars(110)%array4, Agrif_tabvars_i(194)%iarray0, Agrif
     &_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_initv_old(tabres,i1,i2,j1,j2,k1,k2,padd_E,Mm,p
     &add_X,Lm,nnew,v,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                   
                                      
       tabres = v(i1:i2,j1:j2,k1:k2,nnew)
      return
       
      

      end subroutine Sub_Loop_initv_old

      subroutine initt(tabres,i1,i2,j1,j2,k1,k2,m1,m2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_initt(tabres,i1,i2,j1,j2,k1,k2,m1,m2,before,
     &padd_E,Mm,padd_X,Lm,got_tini,nnew,t,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      logical, dimension(1:NT) :: got_tini
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres
      logical :: before
        end subroutine Sub_Loop_initt

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres
      logical :: before
      

        call Sub_Loop_initt(tabres,i1,i2,j1,j2,k1,k2,m1,m2,before, Agrif
     &_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabva
     &rs_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_l(1
     &0)%larray1, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars(109)%array
     &5, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_initt(tabres,i1,i2,j1,j2,k1,k2,m1,m2,before,pa
     &dd_E,Mm,padd_X,Lm,got_tini,nnew,t,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      logical, dimension(1:NT) :: got_tini
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                         
                                            
                      
                      
      integer(4) :: itrc
       if (before) then
         tabres = t(i1:i2,j1:j2,k1:k2,nnew,m1:m2)
       else
         do itrc=m1,m2
             got_tini(itrc) = .true.
             t(i1:i2,j1:j2,k1:k2,1,itrc)=tabres(i1:i2,j1:j2,k1:k2,itrc)
         enddo
       endif
      return
       
      

      end subroutine Sub_Loop_initt

      subroutine initt_old(tabres,i1,i2,j1,j2,k1,k2,m1,m2)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_initt_old(tabres,i1,i2,j1,j2,k1,k2,m1,m2,pad
     &d_E,Mm,padd_X,Lm,nnew,t,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres
        end subroutine Sub_Loop_initt_old

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres
      

        call Sub_Loop_initt_old(tabres,i1,i2,j1,j2,k1,k2,m1,m2, Agrif_ta
     &bvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_
     &i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(174)
     &%iarray0, Agrif_tabvars(109)%array5, Agrif_tabvars_i(194)%iarray0,
     & Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_initt_old(tabres,i1,i2,j1,j2,k1,k2,m1,m2,padd_
     &E,Mm,padd_X,Lm,nnew,t,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                         
                                            
       tabres = t(i1:i2,j1:j2,k1:k2,nnew,m1:m2)
      return
       
      

      end subroutine Sub_Loop_initt_old

      subroutine computenbmaxtimes()      


      use Agrif_Util
      interface
        subroutine Sub_Loop_computenbmaxtimes(Mmmpi,padd_X,Lm,Lmmpi,padd
     &_E,Mm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
        end subroutine Sub_Loop_computenbmaxtimes

      end interface
      

        call Sub_Loop_computenbmaxtimes( Agrif_tabvars_i(194)%iarray0, A
     &grif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_t
     &abvars_i(195)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars
     &_i(197)%iarray0)

      end


      subroutine Sub_Loop_computenbmaxtimes(Mmmpi,padd_X,Lm,Lmmpi,padd_E
     &,Mm)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                      
      integer(4) :: nunit
                      
      integer(4) :: j
      integer(4) :: jp
                      
      integer(4) :: nb
                                  
      integer(4), dimension(0:20) :: nbstep2d
      nunit = Agrif_Get_Unit()
      open(nunit,file='AGRIF_FixedGrids.in',form='formatted')
      j=1
      jp = 0
      call agrif_read_fixed(jp,j,nunit)
      close(nunit)
      nbstep2d = 0
      call write_number(0,nbstep2d)
      nb = 0
      do while (sortedint(nb) /= -1)
        nbmaxtimes = nb
        nb = nb + 1
      enddo
      return
       
      

      end subroutine Sub_Loop_computenbmaxtimes

      recursive subroutine write_number(j,nbstep2d)      


      use Agrif_Util
      interface
        recursive subroutine Sub_Loop_write_number(j,nbstep2d,nfast,Mmmp
     &i,padd_X,Lm,Lmmpi,padd_E,Mm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: nfast
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: j
      integer(4), dimension(0:20) :: nbstep2d
        end subroutine Sub_Loop_write_number

      end interface
      integer(4) :: j
      integer(4), dimension(0:20) :: nbstep2d
      

        call Sub_Loop_write_number(j,nbstep2d, Agrif_tabvars_i(168)%iarr
     &ay0, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(189)%iarray0, A
     &grif_tabvars_i(200)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_t
     &abvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0)

      end


      recursive subroutine Sub_Loop_write_number(j,nbstep2d,nfast,Mmmpi,
     &padd_X,Lm,Lmmpi,padd_E,Mm)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: nfast
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: j
      integer(4), dimension(0:20) :: nbstep2d

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                     
                   
      integer(4) :: nb
                      
      integer(4) :: ngrid
                                  
      call write_prestep3d(j)
      do nb=1,nfast
       call write_step2d(j,nbstep2d)
      enddo
             

      end subroutine Sub_Loop_write_number


      recursive subroutine write_prestep3d(j)      


      use Agrif_Util
      interface
        recursive subroutine Sub_Loop_write_prestep3d(j,Mmmpi,padd_X,Lm,
     &Lmmpi,padd_E,Mm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: j
        end subroutine Sub_Loop_write_prestep3d

      end interface
      integer(4) :: j
      

        call Sub_Loop_write_prestep3d(j, Agrif_tabvars_i(194)%iarray0, A
     &grif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_t
     &abvars_i(195)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars
     &_i(197)%iarray0)

      end


      recursive subroutine Sub_Loop_write_prestep3d(j,Mmmpi,padd_X,Lm,Lm
     &mpi,padd_E,Mm)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: j

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                     
                   
      integer(4) :: nb
                      
      integer(4) :: ngrid
                                  
      integer(4), dimension(0:20) :: nbstep2d
      iind = iind + 1
      sortedint(iind) = j
      whichstep(iind) = 0
      ngrid = 0
      do while (grids_at_level(j,ngrid)/=-1)
      call write_prestep3d(grids_at_level(j,ngrid))
      ngrid = ngrid + 1
      enddo
             

      end subroutine Sub_Loop_write_prestep3d


      recursive subroutine write_step2d(j,nbstep2d)      


      use Agrif_Util
      interface
        recursive subroutine Sub_Loop_write_step2d(j,nbstep2d,nfast,Mmmp
     &i,padd_X,Lm,Lmmpi,padd_E,Mm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: nfast
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: j
      integer(4), dimension(0:20) :: nbstep2d
        end subroutine Sub_Loop_write_step2d

      end interface
      integer(4) :: j
      integer(4), dimension(0:20) :: nbstep2d
      

        call Sub_Loop_write_step2d(j,nbstep2d, Agrif_tabvars_i(168)%iarr
     &ay0, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(189)%iarray0, A
     &grif_tabvars_i(200)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_t
     &abvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0)

      end


      recursive subroutine Sub_Loop_write_step2d(j,nbstep2d,nfast,Mmmpi,
     &padd_X,Lm,Lmmpi,padd_E,Mm)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: nfast
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: j
      integer(4), dimension(0:20) :: nbstep2d

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                     
                   
      integer(4) :: nb
                      
      integer(4) :: ngrid
                                  
      iind = iind + 1
      sortedint(iind) = j
      whichstep(iind) = 1
      nbstep2d(j) = nbstep2d(j)+1
      ngrid = 0
      do while (grids_at_level(j,ngrid)/=-1)
      do nb=1,coeff_ref_time(grids_at_level(j,ngrid))
      call write_step2d(grids_at_level(j,ngrid),nbstep2d)
      enddo
      ngrid = ngrid + 1
      enddo
      if (nbstep2d(j) == nfast) then
        if (j /= 0) then
          if (nbstep2d(parent_grid(j)) /= nfast) then
            call write_step3d1(j)
            call write_step3d2(j)
            call write_prestep3d(j)
          endif
        endif
        if (j == 0) then
          call write_step3d1(j)
          call write_step3d2(j)
        endif
        nbstep2d(j) = 0
      endif
             

      end subroutine Sub_Loop_write_step2d


      recursive subroutine write_step3d1(j)      


      use Agrif_Util
      interface
        recursive subroutine Sub_Loop_write_step3d1(j,Mmmpi,padd_X,Lm,Lm
     &mpi,padd_E,Mm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: j
        end subroutine Sub_Loop_write_step3d1

      end interface
      integer(4) :: j
      

        call Sub_Loop_write_step3d1(j, Agrif_tabvars_i(194)%iarray0, Agr
     &if_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tab
     &vars_i(195)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i
     &(197)%iarray0)

      end


      recursive subroutine Sub_Loop_write_step3d1(j,Mmmpi,padd_X,Lm,Lmmp
     &i,padd_E,Mm)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: j

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                     
                   
      integer(4) :: nb
                      
      integer(4) :: ngrid
                                  
      integer(4), dimension(0:20) :: nbstep2d
      iind = iind + 1
      sortedint(iind) = j
      whichstep(iind) = 2
      ngrid = 0
      do while (grids_at_level(j,ngrid)/=-1)
      call write_step3d1(grids_at_level(j,ngrid))
      ngrid = ngrid + 1
      enddo
             

      end subroutine Sub_Loop_write_step3d1


      recursive subroutine write_step3d2(j)      


      use Agrif_Util
      interface
        recursive subroutine Sub_Loop_write_step3d2(j,Mmmpi,padd_X,Lm,Lm
     &mpi,padd_E,Mm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: j
        end subroutine Sub_Loop_write_step3d2

      end interface
      integer(4) :: j
      

        call Sub_Loop_write_step3d2(j, Agrif_tabvars_i(194)%iarray0, Agr
     &if_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tab
     &vars_i(195)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i
     &(197)%iarray0)

      end


      recursive subroutine Sub_Loop_write_step3d2(j,Mmmpi,padd_X,Lm,Lmmp
     &i,padd_E,Mm)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: j

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                     
                   
      integer(4) :: nb
                      
      integer(4) :: ngrid
                                  
      integer(4), dimension(0:20) :: nbstep2d
      iind = iind + 1
      sortedint(iind) = j
      whichstep(iind) = 3
      ngrid = 0
      do while (grids_at_level(j,ngrid)/=-1)
      call write_step3d2(grids_at_level(j,ngrid))
      ngrid = ngrid + 1
      enddo
             

      end subroutine Sub_Loop_write_step3d2


      recursive subroutine agrif_read_fixed(jp,j,nunit)      


      use Agrif_Util
      interface
        recursive subroutine Sub_Loop_agrif_read_fixed(jp,j,nunit,Mmmpi,
     &padd_X,Lm,Lmmpi,padd_E,Mm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: j
      integer(4) :: nunit
      integer(4) :: jp
        end subroutine Sub_Loop_agrif_read_fixed

      end interface
      integer(4) :: j
      integer(4) :: nunit
      integer(4) :: jp
      

        call Sub_Loop_agrif_read_fixed(jp,j,nunit, Agrif_tabvars_i(194)%
     &iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray
     &0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars_i(188)%iarray0, Agr
     &if_tabvars_i(197)%iarray0)

      end


      recursive subroutine Sub_Loop_agrif_read_fixed(jp,j,nunit,Mmmpi,pa
     &dd_X,Lm,Lmmpi,padd_E,Mm)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: j
      integer(4) :: nunit
      integer(4) :: jp

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                              
                     
      integer(4) :: i
                            
      integer(4) :: nb_grids
                               
      integer(4) :: grid_number
                                                                    
      integer(4) :: r_imin
      integer(4) :: r_imax
      integer(4) :: r_jmin
      integer(4) :: r_jmax
      integer(4) :: r_rhox
      integer(4) :: r_rhoy
      integer(4) :: r_time
                                 
      integer(4), dimension(1:1000) :: gridnum
      read(nunit,*)nb_grids
      do i=1,nb_grids
        grid_number = j
        grids_at_level(jp,i-1)=j
        gridnum(i)=j
        parent_grid(j) = jp
        read(nunit,*)r_imin,r_imax,r_jmin,r_jmax,r_rhox,r_rhoy,r_time
        coeff_ref_time(j)=r_time
        j=j+1
      enddo
      do i=1,nb_grids
       call agrif_read_fixed(gridnum(i),j,nunit)
      enddo
      return
       
      

      end subroutine Sub_Loop_agrif_read_fixed

