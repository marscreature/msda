












      subroutine t3dpremix (tile)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_t3dpremix(tile,Mmmpi,Lmmpi,N3d,N2d)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      integer(4) :: N2d
      integer(4) :: tile
        end subroutine Sub_Loop_t3dpremix

      end interface
      integer(4) :: tile
      

        call Sub_Loop_t3dpremix(tile, Agrif_tabvars_i(194)%iarray0, Agri
     &f_tabvars_i(195)%iarray0, Agrif_tabvars_i(186)%iarray0, Agrif_tabv
     &ars_i(187)%iarray0)

      end


      subroutine Sub_Loop_t3dpremix(tile,Mmmpi,Lmmpi,N3d,N2d)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      integer(4) :: N2d
      integer(4) :: tile

                   

                                              
      integer(4) :: trd
      integer(4) :: omp_get_thread_num
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                                                            
      integer(4) :: chunk_size_X
      integer(4) :: margin_X
      integer(4) :: chunk_size_E
      integer(4) :: margin_E
                                             
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: i_X
      integer(4) :: j_E
      chunk_size_X=(Lmmpi+NSUB_X-1)/NSUB_X
      margin_X=(NSUB_X*chunk_size_X-Lmmpi)/2
      chunk_size_E=(Mmmpi+NSUB_E-1)/NSUB_E
      margin_E=(NSUB_E*chunk_size_E-Mmmpi)/2
      j_E=tile/NSUB_X
      i_X=tile-j_E*NSUB_X
      Istr=1+i_X*chunk_size_X-margin_X
      Iend=Istr+chunk_size_X-1
      Istr=max(Istr,1)
      Iend=min(Iend,Lmmpi)
      Jstr=1+j_E*chunk_size_E-margin_E
      Jend=Jstr+chunk_size_E-1
      Jstr=max(Jstr,1)
      Jend=min(Jend,Mmmpi)
      trd=omp_get_thread_num()
        call t3dpremix_tile (Istr,Iend,Jstr,Jend)
      return
       
      

      end subroutine Sub_Loop_t3dpremix

      subroutine t3dpremix_tile (Istr,Iend,Jstr,Jend)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_t3dpremix_tile(Istr,Iend,Jstr,Jend,Mmmpi,pad
     &d_X,Lm,padd_E,Mm,Lmmpi,nbstep3d,T_sponge_east,T_sponge_west,T_spon
     &ge_north,tsponge,T_sponge_south,TspongeTimeindex2,TTimesponge,tspo
     &ngeid,TspongeTimeindex,nbcoarse,NORTH_INTER,SOUTH_INTER,EAST_INTER
     &,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      integer(4) :: nbstep3d
      integer(4) :: TspongeTimeindex2
      integer(4) :: TTimesponge
      integer(4) :: tspongeid
      integer(4) :: TspongeTimeindex
      integer(4) :: nbcoarse
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(Lmmpi-9:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:2,1:NT) :: T_
     &sponge_east
      real, dimension(0:10,-1:Mm+2+padd_E,1:N,1:2,1:NT) :: T_sponge_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi-9:Mmmpi+1,1:N,1:2,1:NT) :: T_
     &sponge_north
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tsponge
      real, dimension(-1:Lm+2+padd_X,0:10,1:N,1:2,1:NT) :: T_sponge_sout
     &h
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
        end subroutine Sub_Loop_t3dpremix_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      

        call Sub_Loop_t3dpremix_tile(Istr,Iend,Jstr,Jend, Agrif_tabvars_
     &i(194)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)
     &%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarra
     &y0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars_i(173)%iarray0, Ag
     &rif_tabvars(14)%array5, Agrif_tabvars(17)%array5, Agrif_tabvars(8)
     &%array5, Agrif_tabvars(23)%array4, Agrif_tabvars(11)%array5, Agrif
     &_tabvars_i(34)%iarray0, Agrif_tabvars_i(38)%iarray0, Agrif_tabvars
     &_i(16)%iarray0, Agrif_tabvars_i(36)%iarray0, Agrif_tabvars_i(39)%i
     &array0, Agrif_tabvars_l(5)%larray0, Agrif_tabvars_l(4)%larray0, Ag
     &rif_tabvars_l(7)%larray0, Agrif_tabvars_l(6)%larray0)

      end


      subroutine Sub_Loop_t3dpremix_tile(Istr,Iend,Jstr,Jend,Mmmpi,padd_
     &X,Lm,padd_E,Mm,Lmmpi,nbstep3d,T_sponge_east,T_sponge_west,T_sponge
     &_north,tsponge,T_sponge_south,TspongeTimeindex2,TTimesponge,tspong
     &eid,TspongeTimeindex,nbcoarse,NORTH_INTER,SOUTH_INTER,EAST_INTER,W
     &EST_INTER)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      integer(4) :: nbstep3d
      integer(4) :: TspongeTimeindex2
      integer(4) :: TTimesponge
      integer(4) :: tspongeid
      integer(4) :: TspongeTimeindex
      integer(4) :: nbcoarse
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(Lmmpi-9:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:2,1:NT) :: T_
     &sponge_east
      real, dimension(0:10,-1:Mm+2+padd_E,1:N,1:2,1:NT) :: T_sponge_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi-9:Mmmpi+1,1:N,1:2,1:NT) :: T_
     &sponge_north
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tsponge
      real, dimension(-1:Lm+2+padd_X,0:10,1:N,1:2,1:NT) :: T_sponge_sout
     &h
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                 
      integer(4) :: itrc
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                       
      integer(4) :: decal
                    
      real :: maxdiff
                                       
      real :: tinterp
      real :: onemtinterp
      real :: rrhot
                         
      integer(4) :: nold
                          
      integer(4) :: irhot
       external interpsponget
                                 
      integer(4) :: parentnbstep
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      irhot = Agrif_Irhot()
      rrhot = real(irhot)
      decal = 2*max(Agrif_Irhox(),Agrif_Irhoy())
      parentnbstep=Agrif_Parent_Nb_Step()
C$OMP BARRIER
C$OMP MASTER
      If ((nbcoarse == 1).AND.(TspongeTimeindex .NE. parentnbstep)) THEN
        Call Agrif_Set_bc(tspongeid,(/-decal,0/),
     &     InterpolationShouldbemade=.TRUE.)
        Agrif_UseSpecialvalue=.true.
        Agrif_Specialvalue=0.D0
        tinterp = 1.D0
        Call Agrif_Bc_Variable(tspongeid,calledweight=tinterp,
     &                              procname=interpsponget)
        Agrif_UseSpecialvalue=.false.
        TTimesponge = 3 - TTimesponge
        TspongeTimeindex = parentnbstep
        TspongeTimeindex2 = agrif_nb_step()
      ENDIF
C$OMP END MASTER
C$OMP BARRIER
      if (agrif_nb_step() .EQ. TspongeTimeindex2) then
        if (.not.SOUTH_INTER) then
          do itrc=1,NT
            do k=1,N
              do j=JstrR,JstrR+decal
                do i=IstrR,IendR
                  T_sponge_south(i,j,k,TTimesponge,itrc)=
     &                                tsponge(i,j,k,itrc)
                enddo
              enddo
            enddo
          enddo
        endif
        if (.not.NORTH_INTER) then
          do itrc=1,NT
            do k=1,N
              do j=JendR-decal,JendR
                do i=IstrR,IendR
                  T_sponge_north(i,j,k,TTimesponge,itrc)=
     &                                tsponge(i,j,k,itrc)
                enddo
              enddo
            enddo
          enddo
        endif
        if (.not.WEST_INTER) then
          do itrc=1,NT
            do k=1,N
              do j=JstrR,JendR
                do i=IstrR,IstrR+decal
                  T_sponge_west(i,j,k,TTimesponge,itrc)=
     &                               tsponge(i,j,k,itrc)
                enddo
              enddo
            enddo
          enddo
        endif
        if (.not.EAST_INTER) then
          do itrc=1,NT
            do k=1,N
              do j=JstrR,JendR
                do i=IendR-decal,IendR
                  T_sponge_east(i,j,k,TTimesponge,itrc)=
     &                               tsponge(i,j,k,itrc)
                enddo
              enddo
            enddo
          enddo
        endif
      ENDIF
      tinterp = 0.5D0+(real(nbcoarse)-0.5D0)/rrhot
      IF (nbstep3d .LT. irhot) tinterp = 1.D0
      onemtinterp = 1.D0-tinterp
      nold = 3 - TTimesponge
      if (.not.SOUTH_INTER) then
        do itrc=1,NT
          do k=1,N
            do j=JstrR,JstrR+decal
              do i=IstrR,IendR
                tsponge(i,j,k,itrc) =
     &                   onemtinterp*T_sponge_south(i,j,k,nold,itrc)
     &               +tinterp*T_sponge_south(i,j,k,TTimesponge,itrc)
              enddo
            enddo
          enddo
        enddo
      endif
      if (.not.NORTH_INTER) then
        do itrc=1,NT
          do k=1,N
            do j=JendR-decal,JendR
              do i=IstrR,IendR
                tsponge(i,j,k,itrc) =
     &                  onemtinterp*T_sponge_north(i,j,k,nold,itrc)
     &              +tinterp*T_sponge_north(i,j,k,TTimesponge,itrc)
              enddo
            enddo
          enddo
        enddo
      endif
      if (.not.WEST_INTER) then
        do itrc=1,NT
          do k=1,N
            do j=JstrR,JendR
              do i=IstrR,IstrR+decal
                tsponge(i,j,k,itrc) =
     &                    onemtinterp*T_sponge_west(i,j,k,nold,itrc)
     &               + tinterp*T_sponge_west(i,j,k,TTimesponge,itrc)
              enddo
            enddo
          enddo
        enddo
      endif
      if (.not.EAST_INTER) then
        do itrc=1,NT
          do k=1,N
            do j=JstrR,JendR
              do i=IendR-decal,IendR
                tsponge(i,j,k,itrc)=
     &                    onemtinterp*T_sponge_east(i,j,k,nold,itrc)
     &               + tinterp*T_sponge_east(i,j,k,TTimesponge,itrc)
              enddo
            enddo
          enddo
        enddo
      endif
      do itrc=1,NT
        call exchange_r3d_tile (Istr,Iend,Jstr,Jend,
     &                          tsponge(-1,-1,1,itrc))
      enddo
      return
       
      

      end subroutine Sub_Loop_t3dpremix_tile

      subroutine interpsponget(tabres,i1,i2,j1,j2,k1,k2,m1,m2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_interpsponget(tabres,i1,i2,j1,j2,k1,k2,m1,m2
     &,before,padd_E,Mm,padd_X,Lm,tsponge,nrhs,t,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nrhs
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tsponge
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      logical :: before
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres
        end subroutine Sub_Loop_interpsponget

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      logical :: before
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres
      

        call Sub_Loop_interpsponget(tabres,i1,i2,j1,j2,k1,k2,m1,m2,befor
     &e, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agr
     &if_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tab
     &vars(23)%array4, Agrif_tabvars_i(175)%iarray0, Agrif_tabvars(109)%
     &array5, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0
     &)

      end


      subroutine Sub_Loop_interpsponget(tabres,i1,i2,j1,j2,k1,k2,m1,m2,b
     &efore,padd_E,Mm,padd_X,Lm,tsponge,nrhs,t,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nrhs
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tsponge
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      logical :: before
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                        
                        
                                           
      if (before) then
          tabres(i1:i2,j1:j2,k1:k2,m1:m2) =
     &               t(i1:i2,j1:j2,k1:k2,nrhs,m1:m2)
      else
          tsponge(i1:i2,j1:j2,k1:k2,m1:m2) = 
     &                                   tabres(i1:i2,j1:j2,k1:k2,m1:m2)
      endif
      return
       
      

      end subroutine Sub_Loop_interpsponget

