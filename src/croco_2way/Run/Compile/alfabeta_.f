












      subroutine alfabeta_tile (Istr,Iend,Jstr,Jend, alpha,beta)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_alfabeta_tile(Istr,Iend,Jstr,Jend,alpha,beta
     &,padd_E,Mm,padd_X,Lm,rho0,nstp,t,NORTH_INTER,SOUTH_INTER,EAST_INTE
     &R,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: Q00 = +999.842594D0
      real, parameter :: Q01 = +6.793952D-2
      real, parameter :: Q02 = -9.095290D-3
      real, parameter :: Q03 = +1.001685D-4
      real, parameter :: Q04 = -1.120083D-6
      real, parameter :: Q05 = +6.536332D-9
      real, parameter :: U00 = +0.824493D0
      real, parameter :: U01 = -4.08990D-3
      real, parameter :: U02 = +7.64380D-5
      real, parameter :: U03 = -8.24670D-7
      real, parameter :: U04 = +5.38750D-9
      real, parameter :: V00 = -5.72466D-3
      real, parameter :: V01 = +1.02270D-4
      real, parameter :: V02 = -1.65460D-6
      real, parameter :: W00 = +4.8314D-4
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: rho0
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: alpha
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: beta
        end subroutine Sub_Loop_alfabeta_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: alpha
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: beta
      

        call Sub_Loop_alfabeta_tile(Istr,Iend,Jstr,Jend,alpha,beta, Agri
     &f_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabv
     &ars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_r(
     &38)%array0, Agrif_tabvars_i(176)%iarray0, Agrif_tabvars(109)%array
     &5, Agrif_tabvars_l(5)%larray0, Agrif_tabvars_l(4)%larray0, Agrif_t
     &abvars_l(7)%larray0, Agrif_tabvars_l(6)%larray0)

      end


      subroutine Sub_Loop_alfabeta_tile(Istr,Iend,Jstr,Jend,alpha,beta,p
     &add_E,Mm,padd_X,Lm,rho0,nstp,t,NORTH_INTER,SOUTH_INTER,EAST_INTER,
     &WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: Q00 = +999.842594D0
      real, parameter :: Q01 = +6.793952D-2
      real, parameter :: Q02 = -9.095290D-3
      real, parameter :: Q03 = +1.001685D-4
      real, parameter :: Q04 = -1.120083D-6
      real, parameter :: Q05 = +6.536332D-9
      real, parameter :: U00 = +0.824493D0
      real, parameter :: U01 = -4.08990D-3
      real, parameter :: U02 = +7.64380D-5
      real, parameter :: U03 = -8.24670D-7
      real, parameter :: U04 = +5.38750D-9
      real, parameter :: V00 = -5.72466D-3
      real, parameter :: V01 = +1.02270D-4
      real, parameter :: V02 = -1.65460D-6
      real, parameter :: W00 = +4.8314D-4
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: rho0
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: alpha
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: beta

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                         
      integer(4) :: i
      integer(4) :: j
                                    
      integer(4) :: imin
      integer(4) :: imax
      integer(4) :: jmin
      integer(4) :: jmax
                                                                       
                                                                 
                           
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      

                                    
      real :: Tt
      real :: Ts
      real :: sqrtTs
      real :: cff
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      if (.not.WEST_INTER) then
        imin=Istr
      else
        imin=Istr-1
      endif
      if (.not.EAST_INTER) then
        imax=Iend
      else
        imax=Iend+1
      endif
      if (.not.SOUTH_INTER) then
        jmin=Jstr
      else
        jmin=Jstr-1
      endif
      if (.not.NORTH_INTER) then
        jmax=Jend
      else
        jmax=Jend+1
      endif
      do j=jmin,jmax
        do i=imin,imax
          Tt=t(i,j,N,nstp,itemp)
          Ts=t(i,j,N,nstp,isalt)
          sqrtTs=sqrt(Ts)
          cff=1.D0/rho0
          alpha(i,j)=-cff*( Q01+Tt*(2.D0*Q02+Tt*(3.D0*Q03+
     &                                      Tt*(4.D0*Q04+Tt*5.D0*Q05)))
     &                   +Ts*(U01+Tt*(2.D0*U02+Tt*(3.D0*U03+Tt*4.D0*U04
     &                                                                ))
     &                                     +sqrtTs*(V01+Tt*2.D0*V02))
     &                                                            )
          beta(i,j)= cff*( U00+Tt*(U01+Tt*(U02+Tt*(U03+Tt*U04)))
     &                 +1.5D0*(V00+Tt*(V01+Tt*V02))*sqrtTs+2.D0*W00*Ts
     &                                                            )
        enddo
      enddo
      return
       
      

      end subroutine Sub_Loop_alfabeta_tile

