












      subroutine lmd_vmix (tile)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_lmd_vmix(tile,N3d,N2d,A2d,A3d,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: N3d
      integer(4) :: N2d
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(1:N2d,1:NSA,0:NPP-1) :: A2d
      real, dimension(1:N3d,1:5,0:NPP-1) :: A3d
      integer(4) :: tile
        end subroutine Sub_Loop_lmd_vmix

      end interface
      integer(4) :: tile
      

        call Sub_Loop_lmd_vmix(tile, Agrif_tabvars_i(186)%iarray0, Agrif
     &_tabvars_i(187)%iarray0, Agrif_tabvars(95)%array3, Agrif_tabvars(9
     &4)%array3, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarr
     &ay0)

      end


      subroutine Sub_Loop_lmd_vmix(tile,N3d,N2d,A2d,A3d,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: N3d
      integer(4) :: N2d
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(1:N2d,1:NSA,0:NPP-1) :: A2d
      real, dimension(1:N3d,1:5,0:NPP-1) :: A3d
      integer(4) :: tile

                   

                                              
      integer(4) :: trd
      integer(4) :: omp_get_thread_num
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                                                            
      integer(4) :: chunk_size_X
      integer(4) :: margin_X
      integer(4) :: chunk_size_E
      integer(4) :: margin_E
                                             
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: i_X
      integer(4) :: j_E
      chunk_size_X=(Lmmpi+NSUB_X-1)/NSUB_X
      margin_X=(NSUB_X*chunk_size_X-Lmmpi)/2
      chunk_size_E=(Mmmpi+NSUB_E-1)/NSUB_E
      margin_E=(NSUB_E*chunk_size_E-Mmmpi)/2
      j_E=tile/NSUB_X
      i_X=tile-j_E*NSUB_X
      Istr=1+i_X*chunk_size_X-margin_X
      Iend=Istr+chunk_size_X-1
      Istr=max(Istr,1)
      Iend=min(Iend,Lmmpi)
      Jstr=1+j_E*chunk_size_E-margin_E
      Jend=Jstr+chunk_size_E-1
      Jstr=max(Jstr,1)
      Jend=min(Jend,Mmmpi)
      trd=omp_get_thread_num()
      call  lmd_vmix_tile (Istr,Iend,Jstr,Jend,
     &                     A3d(1, 1,trd), A3d(1, 2,trd), A3d(1, 3,trd),
     &                                                   A3d(1, 4,trd))
      call lmd_skpp_tile  (Istr,Iend,Jstr,Jend,
     &                     A3d(1, 1,trd), A3d(1, 2,trd), A3d(1, 3,trd),
     &                     A2d(1, 1,trd), A2d(1, 2,trd), A2d(1, 3,trd),
     &                     A2d(1, 4,trd), A2d(1, 5,trd), A2d(1, 6,trd),
     &                     A2d(1, 7,trd), A2d(1, 8,trd), A2d(1, 9,trd),
     &                     A2d(1,10,trd), A2d(1,11,trd), A2d(1,12,trd),
     &                     A2d(1,13,trd), A2d(1,14,trd), A2d(1,15,trd),
     &                                    A3d(1, 4,trd),    B2d(1,trd))
C$OMP BARRIER
      call lmd_bkpp_tile (Istr,Iend,Jstr,Jend,
     &                    A3d(1, 1,trd), A3d(1, 2,trd), A3d(1, 3,trd),
     &                    A2d(1, 5,trd), A2d(1, 6,trd), A2d(1, 7,trd),
     &                    A2d(1, 8,trd), A2d(1, 9,trd), A2d(1,10,trd),
     &                    A2d(1,11,trd), A2d(1,12,trd), A2d(1,13,trd),
     &                    A2d(1,14,trd), A2d(1,15,trd), A2d(1,16,trd))
      call lmd_finalize_tile (Istr,Iend,Jstr,Jend,
     &                    A3d(1, 1,trd), A3d(1, 2,trd), A3d(1, 3,trd))
      return
       
      

      end subroutine Sub_Loop_lmd_vmix

      subroutine lmd_vmix_tile (Istr,Iend,Jstr,Jend, Kv,Kt,Ks,Rig)      


      use Agrif_Util
      integer(4), parameter :: N = 66
      interface
        subroutine Sub_Loop_lmd_vmix_tile(Istr,Iend,Jstr,Jend,Kv,Kt,Ks,R
     &ig,padd_E,Mm,padd_X,Lm,rmask,pmask2,bvf,v,nstp,u,z_r,NORTH_INTER,S
     &OUTH_INTER,EAST_INTER,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-14
      real, parameter :: lmd_Ri0 = 0.7D0
      real, parameter :: lmd_nuwm = 1.0D-4
      real, parameter :: lmd_nuws = 0.1D-4
      real, parameter :: lmd_nu0m = 50.D-4
      real, parameter :: lmd_nu0s = 50.D-4
      real, parameter :: lmd_nu0c = 0.1D0
      real, parameter :: lmd_nu = 1.5D-6
      real, parameter :: lmd_Rrho0 = 1.9D0
      real, parameter :: lmd_nuf = 10.0D-4
      real, parameter :: lmd_fdd = 0.7D0
      real, parameter :: lmd_tdd1 = 0.909D0
      real, parameter :: lmd_tdd2 = 4.6D0
      real, parameter :: lmd_tdd3 = 0.54D0
      real, parameter :: lmd_sdd1 = 0.15D0
      real, parameter :: lmd_sdd2 = 1.85D0
      real, parameter :: lmd_sdd3 = 0.85D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Rig
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks
        end subroutine Sub_Loop_lmd_vmix_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Rig
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks
      

        call Sub_Loop_lmd_vmix_tile(Istr,Iend,Jstr,Jend,Kv,Kt,Ks,Rig, Ag
     &rif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_ta
     &bvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(
     &51)%array2, Agrif_tabvars(47)%array2, Agrif_tabvars(148)%array3, A
     &grif_tabvars(110)%array4, Agrif_tabvars_i(176)%iarray0, Agrif_tabv
     &ars(111)%array4, Agrif_tabvars(103)%array3, Agrif_tabvars_l(5)%lar
     &ray0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Agri
     &f_tabvars_l(6)%larray0)

      end


      subroutine Sub_Loop_lmd_vmix_tile(Istr,Iend,Jstr,Jend,Kv,Kt,Ks,Rig
     &,padd_E,Mm,padd_X,Lm,rmask,pmask2,bvf,v,nstp,u,z_r,NORTH_INTER,SOU
     &TH_INTER,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-14
      real, parameter :: lmd_Ri0 = 0.7D0
      real, parameter :: lmd_nuwm = 1.0D-4
      real, parameter :: lmd_nuws = 0.1D-4
      real, parameter :: lmd_nu0m = 50.D-4
      real, parameter :: lmd_nu0s = 50.D-4
      real, parameter :: lmd_nu0c = 0.1D0
      real, parameter :: lmd_nu = 1.5D-6
      real, parameter :: lmd_Rrho0 = 1.9D0
      real, parameter :: lmd_nuf = 10.0D-4
      real, parameter :: lmd_fdd = 0.7D0
      real, parameter :: lmd_tdd1 = 0.909D0
      real, parameter :: lmd_tdd2 = 4.6D0
      real, parameter :: lmd_tdd3 = 0.54D0
      real, parameter :: lmd_sdd1 = 0.15D0
      real, parameter :: lmd_sdd2 = 1.85D0
      real, parameter :: lmd_sdd3 = 0.85D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Rig
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                                                       
                                                                 
                                                                 
                                                           
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                   
      real :: lmd_iwm
      real :: lmd_iws
      real :: nu_sx
      real :: nu_sxc
      real :: cff
                            

                                                                       
                                                                 
                                                                 
                                                                 
      
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                              

                                       
      real :: Ri
      real :: ratio
      real :: dudz
      real :: dvdz
      real :: shear2
                          
      integer(4) :: imin
      integer(4) :: imax
                          
      integer(4) :: jmin
      integer(4) :: jmax
      if (.not.WEST_INTER) then
        imin=Istr
      else
        imin=Istr-1
      endif
      if (.not.EAST_INTER) then
        imax=Iend
      else
        imax=Iend+1
      endif
      if (.not.SOUTH_INTER) then
        jmin=Jstr
      else
        jmin=Jstr-1
      endif
      if (.not.NORTH_INTER) then
        jmax=Jend
      else
        jmax=Jend+1
      endif
      do k=1,N-1
        do j=jmin,jmax
          do i=imin,imax
            cff=0.5D0/(z_r(i,j,k+1)-z_r(i,j,k))
            dudz=cff*(u(i  ,j,k+1,nstp)-u(i  ,j,k,nstp)+
     &                u(i+1,j,k+1,nstp)-u(i+1,j,k,nstp))
            dvdz=cff*(v(i,j  ,k+1,nstp)-v(i,j  ,k,nstp)+
     &                v(i,j+1,k+1,nstp)-v(i,j+1,k,nstp))
            shear2=dudz*dudz+dvdz*dvdz
            Rig(i,j,k)=bvf(i,j,k)/max(shear2, 1.D-10)
          enddo
        enddo
        if (.not.WEST_INTER) then
          do j=jmin,jmax
            Rig(Istr-1,j,k)=Rig(Istr,j,k)
          enddo
        endif
        if (.not.EAST_INTER) then
          do j=jmin,jmax
            Rig(Iend+1,j,k)=Rig(Iend,j,k)
          enddo
        endif
        if (.not.SOUTH_INTER) then
          do i=imin,imax
            Rig(i,Jstr-1,k)=Rig(i,Jstr,k)
          enddo
        endif
        if (.not.NORTH_INTER) then
          do i=imin,imax
            Rig(i,Jend+1,k)=Rig(i,Jend,k)
          enddo
        endif
        if (.not.WEST_INTER.and..not.SOUTH_INTER) then
          Rig(Istr-1,Jstr-1,k)=Rig(Istr,Jstr,k)
        endif
        if (.not.WEST_INTER.and..not.NORTH_INTER) then
          Rig(Istr-1,Jend+1,k)=Rig(Istr,Jend,k)
        endif
        if (.not.EAST_INTER.and..not.SOUTH_INTER) then
          Rig(Iend+1,Jstr-1,k)=Rig(Iend,Jstr,k)
        endif
        if (.not.EAST_INTER.and..not.NORTH_INTER) then
          Rig(Iend+1,Jend+1,k)=Rig(Iend,Jend,k)
        endif
      do j=Jstr,Jend+1
        do i=Istr,Iend+1
          Rig(i,j,0)=0.25D0*(Rig(i,j  ,k)+Rig(i-1,j  ,k)
     &                    +Rig(i,j-1,k)+Rig(i-1,j-1,k))
     &                                      *pmask2(i,j)
        enddo
      enddo
      do j=Jstr,Jend
        do i=Istr,Iend
          cff=0.25D0*(pmask2(i,j)   +pmask2(i+1,j)
     &             +pmask2(i,j+1) +pmask2(i+1,j+1))
          Rig(i,j,k)=(1-cff)*Rig(i,j,k)+
     &                0.25D0*(Rig(i,j  ,0)+Rig(i+1,j  ,0)
     &                     +Rig(i,j+1,0)+Rig(i+1,j+1,0))
          Rig(i,j,k)=Rig(i,j,k)*rmask(i,j)
        enddo
      enddo
      enddo
      do k=N-2,2,-1
        do j=Jstr,Jend
          do i=Istr,Iend
            Rig(i,j,k)=0.25D0*Rig(i,j,k-1)+
     &                 0.50D0*Rig(i,j,k  )+
     &                 0.25D0*Rig(i,j,k+1)
          enddo
        enddo
      enddo
      do k=1,N-1
        do j=Jstr,Jend
          do i=Istr,Iend
            Ri=max(0.D0,Rig(i,j,k))
            ratio=min(1.D0,Ri/lmd_Ri0)
            nu_sx=1.D0-ratio*ratio
            nu_sx=nu_sx*nu_sx*nu_sx
           lmd_iwm=lmd_nuwm
           lmd_iws=lmd_nuws
            nu_sxc=0.D0
            Kv(i,j,k)=lmd_iwm+lmd_nu0m*nu_sx+lmd_nu0c*nu_sxc
            Kt(i,j,k)=lmd_iws+lmd_nu0s*nu_sx+lmd_nu0c*nu_sxc
            Ks(i,j,k)=Kt(i,j,k)
          enddo
        enddo
      enddo
      do j=Jstr,Jend
        do i=Istr,Iend
          Kv(i,j,N)=Kv(i,j,N-1)
          Ks(i,j,N)=Ks(i,j,N-1)
          Kt(i,j,N)=Kt(i,j,N-1)
          Kv(i,j,0)=Kv(i,j,  1)
          Ks(i,j,0)=Ks(i,j,  1)
          Kt(i,j,0)=Kt(i,j,  1)
        enddo
      enddo
      return
       
      

      end subroutine Sub_Loop_lmd_vmix_tile

      subroutine lmd_finalize_tile (istr,iend,jstr,jend, Kv,Kt,Ks)      


      use Agrif_Util
      integer(4), parameter :: N = 66
      interface
        subroutine Sub_Loop_lmd_finalize_tile(istr,iend,jstr,jend,Kv,Kt,
     &Ks,padd_E,Mm,padd_X,Lm,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INT
     &ER,Akt,rmask,Akv)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N,1:2) :: Akt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv
      integer(4) :: istr
      integer(4) :: iend
      integer(4) :: jstr
      integer(4) :: jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks
        end subroutine Sub_Loop_lmd_finalize_tile

      end interface
      integer(4) :: istr
      integer(4) :: iend
      integer(4) :: jstr
      integer(4) :: jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks
      

        call Sub_Loop_lmd_finalize_tile(istr,iend,jstr,jend,Kv,Kt,Ks, Ag
     &rif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_ta
     &bvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_
     &l(5)%larray0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larra
     &y0, Agrif_tabvars_l(6)%larray0, Agrif_tabvars(149)%array4, Agrif_t
     &abvars(51)%array2, Agrif_tabvars(150)%array3)

      end


      subroutine Sub_Loop_lmd_finalize_tile(istr,iend,jstr,jend,Kv,Kt,Ks
     &,padd_E,Mm,padd_X,Lm,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER
     &,Akt,rmask,Akv)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N,1:2) :: Akt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv
      integer(4) :: istr
      integer(4) :: iend
      integer(4) :: jstr
      integer(4) :: jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kv
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Kt
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,0:N) :: Ks

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                                                       
                                                                 
                                                                 
          
      do j=jstr,jend
        do i=istr,iend
          do k=1,N-1
            Akv(i,j,k)       =Kv(i,j,k) * rmask(i,j)
            Akt(i,j,k,itemp) =Kt(i,j,k) * rmask(i,j)
            Akt(i,j,k,isalt) =Ks(i,j,k) * rmask(i,j)
          enddo
          Akv(i,j,N)       =max(1.5D0*Kv(i,j,N-1)-0.5D0*Kv(i,j,N-2),
     &                                                             0.D0)
          Akt(i,j,N,itemp) =max(1.5D0*Kt(i,j,N-1)-0.5D0*Kt(i,j,N-2),
     &                                                             0.D0)
          Akt(i,j,N,isalt) =max(1.5D0*Ks(i,j,N-1)-0.5D0*Ks(i,j,N-2),
     &                                                             0.D0)
          Akv(i,j,0)       =0.D0
          Akt(i,j,0,itemp) =0.D0
          Akt(i,j,0,isalt) =0.D0
        enddo
      enddo
      if (.not.WEST_INTER) then
        do j=jstr,jend
          do k=0,N
            Akv(istr-1,j,k)=Akv(istr,j,k)
            Akt(istr-1,j,k,itemp)=Akt(istr,j,k,itemp)
            Akt(istr-1,j,k,isalt)=Akt(istr,j,k,isalt)
          enddo
        enddo
      endif
      if (.not.EAST_INTER) then
        do j=jstr,jend
          do k=0,N
            Akv(iend+1,j,k)=Akv(iend,j,k)
            Akt(iend+1,j,k,itemp)=Akt(iend,j,k,itemp)
            Akt(iend+1,j,k,isalt)=Akt(iend,j,k,isalt)
          enddo
        enddo
      endif
      if (.not.SOUTH_INTER) then
        do i=istr,iend
          do k=0,N
            Akv(i,jstr-1,k)=Akv(i,jstr,k)
            Akt(i,jstr-1,k,itemp)=Akt(i,jstr,k,itemp)
            Akt(i,jstr-1,k,isalt)=Akt(i,jstr,k,isalt)
          enddo
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=istr,iend
          do k=0,N
            Akv(i,jend+1,k)=Akv(i,jend,k)
            Akt(i,jend+1,k,itemp)=Akt(i,jend,k,itemp)
            Akt(i,jend+1,k,isalt)=Akt(i,jend,k,isalt)
          enddo
        enddo
      endif
      if (.not.WEST_INTER .and. .not.SOUTH_INTER) then
        do k=0,N
          Akv(istr-1,jstr-1,k)=Akv(istr,jstr,k)
          Akt(istr-1,jstr-1,k,itemp)=Akt(istr,jstr,k,itemp)
          Akt(istr-1,jstr-1,k,isalt)=Akt(istr,jstr,k,isalt)
        enddo
      endif
      if (.not.WEST_INTER .and. .not.NORTH_INTER) then
        do k=0,N
          Akv(istr-1,jend+1,k)=Akv(istr,jend,k)
          Akt(istr-1,jend+1,k,itemp)=Akt(istr,jend,k,itemp)
          Akt(istr-1,jend+1,k,isalt)=Akt(istr,jend,k,isalt)
        enddo
      endif
      if (.not.EAST_INTER .and. .not.SOUTH_INTER) then
        do k=0,N
          Akv(iend+1,jstr-1,k)=Akv(iend,jstr,k)
          Akt(iend+1,jstr-1,k,itemp)=Akt(iend,jstr,k,itemp)
          Akt(iend+1,jstr-1,k,isalt)=Akt(iend,jstr,k,isalt)
        enddo
      endif
      if (.not.EAST_INTER .and. .not.NORTH_INTER) then
        do k=0,N
          Akv(iend+1,jend+1,k)=Akv(iend,jend,k)
          Akt(iend+1,jend+1,k,itemp)=Akt(iend,jend,k,itemp)
          Akt(iend+1,jend+1,k,isalt)=Akt(iend,jend,k,isalt)
        enddo
      endif
      call exchange_w3d_tile (istr,iend,jstr,jend, Akv)
      call exchange_w3d_tile (istr,iend,jstr,jend,
     &                        Akt(-1,-1,0,itemp))
      call exchange_w3d_tile (istr,iend,jstr,jend,
     &                        Akt(-1,-1,0,isalt))
      return
       
      

      end subroutine Sub_Loop_lmd_finalize_tile

