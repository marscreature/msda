












      program main      


      use Agrif_Util
      interface
        subroutine Sub_Loop_main(ntimes,ntstart,iic,nbstep3d,iif,time,ti
     &me_start,kstp,next_kstp,mynode,wrthis,ldefhis,may_day_flag,numthre
     &ads,N1dETA,N1dXI,eta_v,eta_rho,xi_u,xi_rho,MMm,LLm,Mmmpi,Lmmpi,pad
     &d_E,Mm,padd_X,Lm,N3d,N2d)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4) :: ntimes
      integer(4) :: ntstart
      integer(4) :: iic
      integer(4) :: nbstep3d
      integer(4) :: iif
      real :: time
      real :: time_start
      integer(4) :: kstp
      integer(4) :: next_kstp
      integer(4) :: mynode
      logical :: ldefhis
      integer(4) :: may_day_flag
      integer(4) :: numthreads
      integer(4) :: N1dETA
      integer(4) :: N1dXI
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: MMm
      integer(4) :: LLm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: N3d
      integer(4) :: N2d
      logical, dimension(1:500+NT) :: wrthis
        end subroutine Sub_Loop_main

      end interface
      call Agrif_Init_Grids()
      

        call Sub_Loop_main( Agrif_tabvars_i(170)%iarray0, Agrif_tabvars_
     &i(171)%iarray0, Agrif_tabvars_i(182)%iarray0, Agrif_tabvars_i(173)
     &%iarray0, Agrif_tabvars_i(177)%iarray0, Agrif_tabvars_r(44)%array0
     &, Agrif_tabvars_r(42)%array0, Agrif_tabvars_i(181)%iarray0, Agrif_
     &tabvars_i(178)%iarray0, Agrif_tabvars_i(156)%iarray0, Agrif_tabvar
     &s_l(3)%larray1, Agrif_tabvars_l(9)%larray0, Agrif_tabvars_i(162)%i
     &array0, Agrif_tabvars_i(172)%iarray0, Agrif_tabvars_i(184)%iarray0
     &, Agrif_tabvars_i(185)%iarray0, Agrif_tabvars_i(142)%iarray0, Agri
     &f_tabvars_i(143)%iarray0, Agrif_tabvars_i(144)%iarray0, Agrif_tabv
     &ars_i(145)%iarray0, Agrif_tabvars_i(198)%iarray0, Agrif_tabvars_i(
     &201)%iarray0, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%i
     &array0, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0
     &, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agri
     &f_tabvars_i(186)%iarray0, Agrif_tabvars_i(187)%iarray0)

      end program main


      subroutine Sub_Loop_main(ntimes,ntstart,iic,nbstep3d,iif,time,time
     &_start,kstp,next_kstp,mynode,wrthis,ldefhis,may_day_flag,numthread
     &s,N1dETA,N1dXI,eta_v,eta_rho,xi_u,xi_rho,MMm,LLm,Mmmpi,Lmmpi,padd_
     &E,Mm,padd_X,Lm,N3d,N2d)

      use Agrif_Util


      include 'mpif.h'
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4) :: ntimes
      integer(4) :: ntstart
      integer(4) :: iic
      integer(4) :: nbstep3d
      integer(4) :: iif
      real :: time
      real :: time_start
      integer(4) :: kstp
      integer(4) :: next_kstp
      integer(4) :: mynode
      logical :: ldefhis
      integer(4) :: may_day_flag
      integer(4) :: numthreads
      integer(4) :: N1dETA
      integer(4) :: N1dXI
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: MMm
      integer(4) :: LLm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: N3d
      integer(4) :: N2d
      logical, dimension(1:500+NT) :: wrthis

                                      
      integer(4) :: tile
      integer(4) :: subs
      integer(4) :: trd
      integer(4) :: ierr
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
!$AGRIF_DO_NOT_TREAT
      INTEGER*4 :: ocean_grid_comm
      common /cpl_comm/ ocean_grid_comm
!$AGRIF_END_DO_NOT_TREAT
                                              
      type(Agrif_pgrid), pointer :: parcours
                       
                                    
      integer(4) :: iifroot
      integer(4) :: iicroot
                                                
      integer(4) :: size_XI
      integer(4) :: size_ETA
      integer(4) :: se
      integer(4) :: sse
      integer(4) :: sz
      integer(4) :: ssz
      external :: step
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
      LLm=LLm0;  MMm=MMm0
      Lm=(LLm+NP_XI-1)/NP_XI; Mm=(MMm+NP_ETA-1)/NP_ETA
      padd_X=(Lm+2)/2-(Lm+1)/2; padd_E=(Mm+2)/2-(Mm+1)/2
      xi_rho=LLm+2;  xi_u=xi_rho-1
      eta_rho=MMm+2; eta_v=eta_rho-1
      size_XI=7+(Lm+NSUB_X-1)/NSUB_X
      size_ETA=7+(Mm+NSUB_E-1)/NSUB_E
      sse=size_ETA/Np; ssz=Np/size_ETA
      se=sse/(sse+ssz);   sz=1-se
      N1dXI = size_XI
      N1dETA = size_ETA
      N2d=size_XI*(se*size_ETA+sz*Np)
      N3d=size_XI*size_ETA*Np
      call Agrif_MPI_Init(MPI_COMM_WORLD)
                             

      call declare_zoom_variables()
      call MPI_Setup (ierr)
      if (ierr.ne.0) goto 100
      call read_inp (ierr)
      if (ierr.ne.0) goto 100
      call init_scalars (ierr)
      if (ierr.ne.0) goto 100
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call start_timers()
        call init_arrays (tile)
         enddo
       enddo
      call get_grid
      if (may_day_flag.ne.0) goto 99
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call setup_grid1 (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call setup_grid2 (tile)
         enddo
       enddo
      call set_scoord
      call set_weights
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call set_depth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call grid_stiffness (tile)
         enddo
       enddo
      call get_initial
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call ana_initial (tile)
         enddo
       enddo
      if (may_day_flag.ne.0) goto 99
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call set_depth (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call set_HUV (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call omega (tile)
        call rho_eos (tile)
         enddo
       enddo
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call set_nudgcof (tile)
         enddo
       enddo
      call get_tclima
      call get_uclima
      call get_ssh
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*trd,subs*(trd+1)-1,+1
        call ana_tclima (tile)
         enddo
       enddo
      call get_vbc
      call get_tides
      if (may_day_flag.ne.0) goto 99
      if (ldefhis .and. wrthis(indxTime)) call wrt_his
      if (may_day_flag.ne.0) goto 99
      if (mynode.eq.0) write(stdout,'(/1x,A27/)')
     &                'MAIN: started time-steping.'
      next_kstp=kstp
      time_start=time
      call MPI_Barrier(MPI_COMM_WORLD, ierr)
      iif = -1
      nbstep3d = 0
      iic = ntstart
      iind = -1
      grids_at_level = -1
      sortedint = -1
      call computenbmaxtimes
      do iicroot=ntstart,ntimes+1
        nbtimes = 0
        do while (nbtimes.LE.nbmaxtimes)
          call Agrif_Step(step)
        enddo
        if (may_day_flag.ne.0) goto 99
      enddo
  99  continue
C$OMP PARALLEL DO PRIVATE(trd,subs, tile), SHARED(numthreads)
       do trd=0,numthreads-1
        subs=NSUB_X*NSUB_E/numthreads
         do tile=subs*(trd+1)-1,subs*trd,-1
        call stop_timers()
         enddo
       enddo
      call closecdf
      parcours=>Agrif_Curgrid%child_list % first
      do while (associated(parcours))
        Call Agrif_Instance(parcours % gr)
        call closecdf
        parcours => parcours % next
      enddo
 100  continue
      call MPI_Barrier(MPI_COMM_WORLD, ierr)
      call MPI_Finalize (ierr)
      stop
       
      

      end subroutine Sub_Loop_main

      module gridinter2


        implicit none
        public :: Alloc_agrif_gridinter2
      contains
      subroutine Alloc_agrif_gridinter2(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      end subroutine Alloc_agrif_gridinter2
      end module gridinter2
      module gridinter


        implicit none
        public :: Alloc_agrif_gridinter
      contains
      subroutine Alloc_agrif_gridinter(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      end subroutine Alloc_agrif_gridinter
      end module gridinter
      module work_agrif


        implicit none
        public :: Alloc_agrif_work_agrif
      contains
      subroutine Alloc_agrif_work_agrif(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: NWEIGHT = 1000
      !! ALLOCATION OF VARIABLE : A1dETA
          if (.not. allocated(Agrif_Gr % tabvars(4)% array2)) then
          allocate(Agrif_Gr % tabvars(4)% array2(-1 :  Agrif_tabvars_i(1
     &97)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 10*NWEIGHT))
          Agrif_Gr % tabvars(4)% array2 = 0
      endif
      !! ALLOCATION OF VARIABLE : A1dXI
          if (.not. allocated(Agrif_Gr % tabvars(5)% array2)) then
          allocate(Agrif_Gr % tabvars(5)% array2(-1 :  Agrif_tabvars_i(2
     &00)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,1 : 10*NWEIGHT))
          Agrif_Gr % tabvars(5)% array2 = 0
      endif
      end subroutine Alloc_agrif_work_agrif
      end module work_agrif
      module zoom3D_sponge_VN


        implicit none
        public :: Alloc_agrif_zoom3D_sponge_VN
      contains
      subroutine Alloc_agrif_zoom3D_sponge_VN(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : V_sponge_north
          if (.not. allocated(Agrif_Gr % tabvars(6)% array4)) then
          allocate(Agrif_Gr % tabvars(6)% array4(-1 :  Agrif_tabvars_i(2
     &00)%iarray0+2+ Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(194)%
     &iarray0-9 :  Agrif_tabvars_i(194)%iarray0+1,1 : N,1 : 2))
          Agrif_Gr % tabvars(6)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_sponge_VN
      end module zoom3D_sponge_VN
      module zoom3D_sponge_UN


        implicit none
        public :: Alloc_agrif_zoom3D_sponge_UN
      contains
      subroutine Alloc_agrif_zoom3D_sponge_UN(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : U_sponge_north
          if (.not. allocated(Agrif_Gr % tabvars(7)% array4)) then
          allocate(Agrif_Gr % tabvars(7)% array4(-1 :  Agrif_tabvars_i(2
     &00)%iarray0+2+ Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(194)%
     &iarray0-9 :  Agrif_tabvars_i(194)%iarray0+1,1 : N,1 : 2))
          Agrif_Gr % tabvars(7)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_sponge_UN
      end module zoom3D_sponge_UN
      module zoom3D_sponge_TN


        implicit none
        public :: Alloc_agrif_zoom3D_sponge_TN
      contains
      subroutine Alloc_agrif_zoom3D_sponge_TN(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : T_sponge_north
          if (.not. allocated(Agrif_Gr % tabvars(8)% array5)) then
          allocate(Agrif_Gr % tabvars(8)% array5(-1 :  Agrif_tabvars_i(2
     &00)%iarray0+2+ Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(194)%
     &iarray0-9 :  Agrif_tabvars_i(194)%iarray0+1,1 : N,1 : 2,1 : NT))
          Agrif_Gr % tabvars(8)% array5 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_sponge_TN
      end module zoom3D_sponge_TN
      module zoom3D_sponge_VS


        implicit none
        public :: Alloc_agrif_zoom3D_sponge_VS
      contains
      subroutine Alloc_agrif_zoom3D_sponge_VS(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : V_sponge_south
          if (.not. allocated(Agrif_Gr % tabvars(9)% array4)) then
          allocate(Agrif_Gr % tabvars(9)% array4(-1 :  Agrif_tabvars_i(2
     &00)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,1 : 11,1 : N,1 : 2))
          Agrif_Gr % tabvars(9)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_sponge_VS
      end module zoom3D_sponge_VS
      module zoom3D_sponge_US


        implicit none
        public :: Alloc_agrif_zoom3D_sponge_US
      contains
      subroutine Alloc_agrif_zoom3D_sponge_US(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : U_sponge_south
          if (.not. allocated(Agrif_Gr % tabvars(10)% array4)) then
          allocate(Agrif_Gr % tabvars(10)% array4(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,0 : 10,1 : N,1 : 2))
          Agrif_Gr % tabvars(10)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_sponge_US
      end module zoom3D_sponge_US
      module zoom3D_sponge_TS


        implicit none
        public :: Alloc_agrif_zoom3D_sponge_TS
      contains
      subroutine Alloc_agrif_zoom3D_sponge_TS(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : T_sponge_south
          if (.not. allocated(Agrif_Gr % tabvars(11)% array5)) then
          allocate(Agrif_Gr % tabvars(11)% array5(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,0 : 10,1 : N,1 : 2,1 
     &: NT))
          Agrif_Gr % tabvars(11)% array5 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_sponge_TS
      end module zoom3D_sponge_TS
      module zoom3D_sponge_VE


        implicit none
        public :: Alloc_agrif_zoom3D_sponge_VE
      contains
      subroutine Alloc_agrif_zoom3D_sponge_VE(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : V_sponge_east
          if (.not. allocated(Agrif_Gr % tabvars(12)% array4)) then
          allocate(Agrif_Gr % tabvars(12)% array4( Agrif_tabvars_i(195)%
     &iarray0-9 :  Agrif_tabvars_i(195)%iarray0+1,-1 :  Agrif_tabvars_i(
     &197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 2))
          Agrif_Gr % tabvars(12)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_sponge_VE
      end module zoom3D_sponge_VE
      module zoom3D_sponge_UE


        implicit none
        public :: Alloc_agrif_zoom3D_sponge_UE
      contains
      subroutine Alloc_agrif_zoom3D_sponge_UE(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : U_sponge_east
          if (.not. allocated(Agrif_Gr % tabvars(13)% array4)) then
          allocate(Agrif_Gr % tabvars(13)% array4( Agrif_tabvars_i(195)%
     &iarray0-9 :  Agrif_tabvars_i(195)%iarray0+1,-1 :  Agrif_tabvars_i(
     &197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 2))
          Agrif_Gr % tabvars(13)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_sponge_UE
      end module zoom3D_sponge_UE
      module zoom3D_sponge_TE


        implicit none
        public :: Alloc_agrif_zoom3D_sponge_TE
      contains
      subroutine Alloc_agrif_zoom3D_sponge_TE(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : T_sponge_east
          if (.not. allocated(Agrif_Gr % tabvars(14)% array5)) then
          allocate(Agrif_Gr % tabvars(14)% array5( Agrif_tabvars_i(195)%
     &iarray0-9 :  Agrif_tabvars_i(195)%iarray0+1,-1 :  Agrif_tabvars_i(
     &197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 2,1 : NT))
          Agrif_Gr % tabvars(14)% array5 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_sponge_TE
      end module zoom3D_sponge_TE
      module zoom3D_sponge_VW


        implicit none
        public :: Alloc_agrif_zoom3D_sponge_VW
      contains
      subroutine Alloc_agrif_zoom3D_sponge_VW(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : V_sponge_west
          if (.not. allocated(Agrif_Gr % tabvars(15)% array4)) then
          allocate(Agrif_Gr % tabvars(15)% array4(0 : 10,-1 :  Agrif_tab
     &vars_i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 2))
          Agrif_Gr % tabvars(15)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_sponge_VW
      end module zoom3D_sponge_VW
      module zoom3D_sponge_UW


        implicit none
        public :: Alloc_agrif_zoom3D_sponge_UW
      contains
      subroutine Alloc_agrif_zoom3D_sponge_UW(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : U_sponge_west
          if (.not. allocated(Agrif_Gr % tabvars(16)% array4)) then
          allocate(Agrif_Gr % tabvars(16)% array4(1 : 11,-1 :  Agrif_tab
     &vars_i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 2))
          Agrif_Gr % tabvars(16)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_sponge_UW
      end module zoom3D_sponge_UW
      module zoom3D_sponge_TW


        implicit none
        public :: Alloc_agrif_zoom3D_sponge_TW
      contains
      subroutine Alloc_agrif_zoom3D_sponge_TW(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : T_sponge_west
          if (.not. allocated(Agrif_Gr % tabvars(17)% array5)) then
          allocate(Agrif_Gr % tabvars(17)% array5(0 : 10,-1 :  Agrif_tab
     &vars_i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 2,1 
     &: NT))
          Agrif_Gr % tabvars(17)% array5 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_sponge_TW
      end module zoom3D_sponge_TW
      module zoom2D_V2


        implicit none
        public :: Alloc_agrif_zoom2D_V2
      contains
      subroutine Alloc_agrif_zoom2D_V2(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: NWEIGHT = 1000
      !! ALLOCATION OF VARIABLE : DV_avg3
          if (.not. allocated(Agrif_Gr % tabvars(18)% array3)) then
          allocate(Agrif_Gr % tabvars(18)% array3(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,0 : NWEIGHT))
          Agrif_Gr % tabvars(18)% array3 = 0
      endif
      end subroutine Alloc_agrif_zoom2D_V2
      end module zoom2D_V2
      module zoom2D_U2


        implicit none
        public :: Alloc_agrif_zoom2D_U2
      contains
      subroutine Alloc_agrif_zoom2D_U2(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: NWEIGHT = 1000
      !! ALLOCATION OF VARIABLE : DU_avg3
          if (.not. allocated(Agrif_Gr % tabvars(19)% array3)) then
          allocate(Agrif_Gr % tabvars(19)% array3(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,0 : NWEIGHT))
          Agrif_Gr % tabvars(19)% array3 = 0
      endif
      end subroutine Alloc_agrif_zoom2D_U2
      end module zoom2D_U2
      module zoom2D_Zeta2


        implicit none
        public :: Alloc_agrif_zoom2D_Zeta2
      contains
      subroutine Alloc_agrif_zoom2D_Zeta2(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: NWEIGHT = 1000
      !! ALLOCATION OF VARIABLE : Zt_avg3
          if (.not. allocated(Agrif_Gr % tabvars(20)% array3)) then
          allocate(Agrif_Gr % tabvars(20)% array3(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,0 : NWEIGHT))
          Agrif_Gr % tabvars(20)% array3 = 0
      endif
      end subroutine Alloc_agrif_zoom2D_Zeta2
      end module zoom2D_Zeta2
      module huvagrif


        implicit none
        public :: Alloc_agrif_huvagrif
      contains
      subroutine Alloc_agrif_huvagrif(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : Hvomagrif
          if (.not. allocated(Agrif_Gr % tabvars(21)% array3)) then
          allocate(Agrif_Gr % tabvars(21)% array3(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(21)% array3 = 0
      endif
      !! ALLOCATION OF VARIABLE : Huonagrif
          if (.not. allocated(Agrif_Gr % tabvars(22)% array3)) then
          allocate(Agrif_Gr % tabvars(22)% array3(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(22)% array3 = 0
      endif
      end subroutine Alloc_agrif_huvagrif
      end module huvagrif
      module sponge_com


        implicit none
        public :: Alloc_agrif_sponge_com
      contains
      subroutine Alloc_agrif_sponge_com(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : tsponge
          if (.not. allocated(Agrif_Gr % tabvars(23)% array4)) then
          allocate(Agrif_Gr % tabvars(23)% array4(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : NT))
          Agrif_Gr % tabvars(23)% array4 = 0
      endif
      !! ALLOCATION OF VARIABLE : vsponge
          if (.not. allocated(Agrif_Gr % tabvars(24)% array3)) then
          allocate(Agrif_Gr % tabvars(24)% array3(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(24)% array3 = 0
      endif
      !! ALLOCATION OF VARIABLE : usponge
          if (.not. allocated(Agrif_Gr % tabvars(25)% array3)) then
          allocate(Agrif_Gr % tabvars(25)% array3(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(25)% array3 = 0
      endif
      end subroutine Alloc_agrif_sponge_com
      end module sponge_com
      module updateprestep


        implicit none
        public :: Alloc_agrif_updateprestep
      contains
      subroutine Alloc_agrif_updateprestep(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : Alreadyupdated
          if (.not. allocated(Agrif_Gr % tabvars_l(1)% larray3)) then
          allocate(Agrif_Gr % tabvars_l(1)% larray3(-1 :  Agrif_tabvars_
     &i(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars
     &_i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 3))
      endif
      end subroutine Alloc_agrif_updateprestep
      end module updateprestep
      module zoombc2D


        implicit none
        public :: Alloc_agrif_zoombc2D
      contains
      subroutine Alloc_agrif_zoombc2D(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : dVinterp
          if (.not. allocated(Agrif_Gr % tabvars(26)% array2)) then
          allocate(Agrif_Gr % tabvars(26)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(26)% array2 = 0
      endif
      !! ALLOCATION OF VARIABLE : dUinterp
          if (.not. allocated(Agrif_Gr % tabvars(27)% array2)) then
          allocate(Agrif_Gr % tabvars(27)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(27)% array2 = 0
      endif
      !! ALLOCATION OF VARIABLE : dZtinterp
          if (.not. allocated(Agrif_Gr % tabvars(28)% array2)) then
          allocate(Agrif_Gr % tabvars(28)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(28)% array2 = 0
      endif
      end subroutine Alloc_agrif_zoombc2D
      end module zoombc2D
      module averagebaro


        implicit none
        public :: Alloc_agrif_averagebaro
      contains
      subroutine Alloc_agrif_averagebaro(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : Zt_avg2
          if (.not. allocated(Agrif_Gr % tabvars(29)% array3)) then
          allocate(Agrif_Gr % tabvars(29)% array3(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 4))
          Agrif_Gr % tabvars(29)% array3 = 0
      endif
      end subroutine Alloc_agrif_averagebaro
      end module averagebaro
      module myfluxes


        implicit none
        public :: Alloc_agrif_myfluxes
      contains
      subroutine Alloc_agrif_myfluxes(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : myfy
          if (.not. allocated(Agrif_Gr % tabvars(30)% array4)) then
          allocate(Agrif_Gr % tabvars(30)% array4(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : NT))
          Agrif_Gr % tabvars(30)% array4 = 0
      endif
      !! ALLOCATION OF VARIABLE : myfx
          if (.not. allocated(Agrif_Gr % tabvars(31)% array4)) then
          allocate(Agrif_Gr % tabvars(31)% array4(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : NT))
          Agrif_Gr % tabvars(31)% array4 = 0
      endif
      end subroutine Alloc_agrif_myfluxes
      end module myfluxes
      module updatevalues


        implicit none
        public :: Alloc_agrif_updatevalues
      contains
      subroutine Alloc_agrif_updatevalues(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: NWEIGHT = 1000
      !! ALLOCATION OF VARIABLE : myvalues
          if (.not. allocated(Agrif_Gr % tabvars(32)% array2)) then
          allocate(Agrif_Gr % tabvars(32)% array2(1 : 3*7*(2*( Agrif_tab
     &vars_i(200)%iarray0+3)+2*( Agrif_tabvars_i(197)%iarray0+3)),0 : NW
     &EIGHT))
          Agrif_Gr % tabvars(32)% array2 = 0
      endif
      end subroutine Alloc_agrif_updatevalues
      end module updatevalues
      module updateTprofile


        implicit none
        public :: Alloc_agrif_updateTprofile
      contains
      subroutine Alloc_agrif_updateTprofile(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : updateTprof
          if (.not. allocated(Agrif_Gr % tabvars(33)% array4)) then
          allocate(Agrif_Gr % tabvars(33)% array4(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : NT))
          Agrif_Gr % tabvars(33)% array4 = 0
      endif
      end subroutine Alloc_agrif_updateTprofile
      end module updateTprofile
      module weighting


        implicit none
        public :: Alloc_agrif_weighting
      contains
      subroutine Alloc_agrif_weighting(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: NWEIGHT = 1000
      !! ALLOCATION OF VARIABLE : weight2
          if (.not. allocated(Agrif_Gr % tabvars(34)% array2)) then
          allocate(Agrif_Gr % tabvars(34)% array2(0 : NWEIGHT,0 : NWEIGH
     &T))
          Agrif_Gr % tabvars(34)% array2 = 0
      endif
      end subroutine Alloc_agrif_weighting
      end module weighting
      module zoom3D_VN


        implicit none
        public :: Alloc_agrif_zoom3D_VN
      contains
      subroutine Alloc_agrif_zoom3D_VN(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : V_north
          if (.not. allocated(Agrif_Gr % tabvars(35)% array4)) then
          allocate(Agrif_Gr % tabvars(35)% array4(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(194)
     &%iarray0+1 :  Agrif_tabvars_i(194)%iarray0+1,1 : N,1 : 4))
          Agrif_Gr % tabvars(35)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_VN
      end module zoom3D_VN
      module zoom3D_UN


        implicit none
        public :: Alloc_agrif_zoom3D_UN
      contains
      subroutine Alloc_agrif_zoom3D_UN(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : U_north
          if (.not. allocated(Agrif_Gr % tabvars(36)% array4)) then
          allocate(Agrif_Gr % tabvars(36)% array4(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(194)
     &%iarray0+1 :  Agrif_tabvars_i(194)%iarray0+1,1 : N,1 : 4))
          Agrif_Gr % tabvars(36)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_UN
      end module zoom3D_UN
      module zoom3D_TN


        implicit none
        public :: Alloc_agrif_zoom3D_TN
      contains
      subroutine Alloc_agrif_zoom3D_TN(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : T_north
          if (.not. allocated(Agrif_Gr % tabvars(37)% array5)) then
          allocate(Agrif_Gr % tabvars(37)% array5(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(194)
     &%iarray0 :  Agrif_tabvars_i(194)%iarray0+1,1 : N,1 : 4,1 : NT))
          Agrif_Gr % tabvars(37)% array5 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_TN
      end module zoom3D_TN
      module zoom3D_VS


        implicit none
        public :: Alloc_agrif_zoom3D_VS
      contains
      subroutine Alloc_agrif_zoom3D_VS(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : V_south
          if (.not. allocated(Agrif_Gr % tabvars(38)% array4)) then
          allocate(Agrif_Gr % tabvars(38)% array4(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,1 : 1,1 : N,1 : 4))
          Agrif_Gr % tabvars(38)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_VS
      end module zoom3D_VS
      module zoom3D_US


        implicit none
        public :: Alloc_agrif_zoom3D_US
      contains
      subroutine Alloc_agrif_zoom3D_US(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : U_south
          if (.not. allocated(Agrif_Gr % tabvars(39)% array4)) then
          allocate(Agrif_Gr % tabvars(39)% array4(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,0 : 0,1 : N,1 : 4))
          Agrif_Gr % tabvars(39)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_US
      end module zoom3D_US
      module zoom3D_TS


        implicit none
        public :: Alloc_agrif_zoom3D_TS
      contains
      subroutine Alloc_agrif_zoom3D_TS(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : T_south
          if (.not. allocated(Agrif_Gr % tabvars(40)% array5)) then
          allocate(Agrif_Gr % tabvars(40)% array5(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,0 : 1,1 : N,1 : 4,1 :
     & NT))
          Agrif_Gr % tabvars(40)% array5 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_TS
      end module zoom3D_TS
      module zoom3D_VE


        implicit none
        public :: Alloc_agrif_zoom3D_VE
      contains
      subroutine Alloc_agrif_zoom3D_VE(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : V_east
          if (.not. allocated(Agrif_Gr % tabvars(41)% array4)) then
          allocate(Agrif_Gr % tabvars(41)% array4( Agrif_tabvars_i(195)%
     &iarray0+1 :  Agrif_tabvars_i(195)%iarray0+1,-1 :  Agrif_tabvars_i(
     &197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 4))
          Agrif_Gr % tabvars(41)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_VE
      end module zoom3D_VE
      module zoom3D_UE


        implicit none
        public :: Alloc_agrif_zoom3D_UE
      contains
      subroutine Alloc_agrif_zoom3D_UE(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : U_east
          if (.not. allocated(Agrif_Gr % tabvars(42)% array4)) then
          allocate(Agrif_Gr % tabvars(42)% array4( Agrif_tabvars_i(195)%
     &iarray0+1 :  Agrif_tabvars_i(195)%iarray0+1,-1 :  Agrif_tabvars_i(
     &197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 4))
          Agrif_Gr % tabvars(42)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_UE
      end module zoom3D_UE
      module zoom3D_TE


        implicit none
        public :: Alloc_agrif_zoom3D_TE
      contains
      subroutine Alloc_agrif_zoom3D_TE(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : T_east
          if (.not. allocated(Agrif_Gr % tabvars(43)% array5)) then
          allocate(Agrif_Gr % tabvars(43)% array5( Agrif_tabvars_i(195)%
     &iarray0 :  Agrif_tabvars_i(195)%iarray0+1,-1 :  Agrif_tabvars_i(19
     &7)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 4,1 : NT))
          Agrif_Gr % tabvars(43)% array5 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_TE
      end module zoom3D_TE
      module zoom3D_VW


        implicit none
        public :: Alloc_agrif_zoom3D_VW
      contains
      subroutine Alloc_agrif_zoom3D_VW(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : V_west
          if (.not. allocated(Agrif_Gr % tabvars(44)% array4)) then
          allocate(Agrif_Gr % tabvars(44)% array4(0 : 0,-1 :  Agrif_tabv
     &ars_i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 4))
          Agrif_Gr % tabvars(44)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_VW
      end module zoom3D_VW
      module zoom3D_UW


        implicit none
        public :: Alloc_agrif_zoom3D_UW
      contains
      subroutine Alloc_agrif_zoom3D_UW(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : U_west
          if (.not. allocated(Agrif_Gr % tabvars(45)% array4)) then
          allocate(Agrif_Gr % tabvars(45)% array4(1 : 1,-1 :  Agrif_tabv
     &ars_i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 4))
          Agrif_Gr % tabvars(45)% array4 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_UW
      end module zoom3D_UW
      module zoom3D_TW


        implicit none
        public :: Alloc_agrif_zoom3D_TW
      contains
      subroutine Alloc_agrif_zoom3D_TW(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : T_west
          if (.not. allocated(Agrif_Gr % tabvars(46)% array5)) then
          allocate(Agrif_Gr % tabvars(46)% array5(0 : 1,-1 :  Agrif_tabv
     &ars_i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 4,1 :
     & NT))
          Agrif_Gr % tabvars(46)% array5 = 0
      endif
      end subroutine Alloc_agrif_zoom3D_TW
      end module zoom3D_TW
      module mask_p2


        implicit none
        public :: Alloc_agrif_mask_p2
      contains
      subroutine Alloc_agrif_mask_p2(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pmask2
          if (.not. allocated(Agrif_Gr % tabvars(47)% array2)) then
          allocate(Agrif_Gr % tabvars(47)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(47)% array2 = 0
      endif
      end subroutine Alloc_agrif_mask_p2
      end module mask_p2
      module mask_v


        implicit none
        public :: Alloc_agrif_mask_v
      contains
      subroutine Alloc_agrif_mask_v(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : vmask
          if (.not. allocated(Agrif_Gr % tabvars(48)% array2)) then
          allocate(Agrif_Gr % tabvars(48)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(48)% array2 = 0
      endif
      end subroutine Alloc_agrif_mask_v
      end module mask_v
      module mask_u


        implicit none
        public :: Alloc_agrif_mask_u
      contains
      subroutine Alloc_agrif_mask_u(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : umask
          if (.not. allocated(Agrif_Gr % tabvars(49)% array2)) then
          allocate(Agrif_Gr % tabvars(49)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(49)% array2 = 0
      endif
      end subroutine Alloc_agrif_mask_u
      end module mask_u
      module mask_p


        implicit none
        public :: Alloc_agrif_mask_p
      contains
      subroutine Alloc_agrif_mask_p(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pmask
          if (.not. allocated(Agrif_Gr % tabvars(50)% array2)) then
          allocate(Agrif_Gr % tabvars(50)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(50)% array2 = 0
      endif
      end subroutine Alloc_agrif_mask_p
      end module mask_p
      module mask_r


        implicit none
        public :: Alloc_agrif_mask_r
      contains
      subroutine Alloc_agrif_mask_r(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : rmask
          if (.not. allocated(Agrif_Gr % tabvars(51)% array2)) then
          allocate(Agrif_Gr % tabvars(51)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(51)% array2 = 0
      endif
      end subroutine Alloc_agrif_mask_r
      end module mask_r
      module metrics_grdscl


        implicit none
        public :: Alloc_agrif_metrics_grdscl
      contains
      subroutine Alloc_agrif_metrics_grdscl(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : grdscl
          if (.not. allocated(Agrif_Gr % tabvars(52)% array2)) then
          allocate(Agrif_Gr % tabvars(52)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(52)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_grdscl
      end module metrics_grdscl
      module metrics_pnom_v


        implicit none
        public :: Alloc_agrif_metrics_pnom_v
      contains
      subroutine Alloc_agrif_metrics_pnom_v(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pnom_v
          if (.not. allocated(Agrif_Gr % tabvars(53)% array2)) then
          allocate(Agrif_Gr % tabvars(53)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(53)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_pnom_v
      end module metrics_pnom_v
      module metrics_pmon_u


        implicit none
        public :: Alloc_agrif_metrics_pmon_u
      contains
      subroutine Alloc_agrif_metrics_pmon_u(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pmon_u
          if (.not. allocated(Agrif_Gr % tabvars(54)% array2)) then
          allocate(Agrif_Gr % tabvars(54)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(54)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_pmon_u
      end module metrics_pmon_u
      module metrics_pnom_r


        implicit none
        public :: Alloc_agrif_metrics_pnom_r
      contains
      subroutine Alloc_agrif_metrics_pnom_r(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pnom_r
          if (.not. allocated(Agrif_Gr % tabvars(55)% array2)) then
          allocate(Agrif_Gr % tabvars(55)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(55)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_pnom_r
      end module metrics_pnom_r
      module metrics_pmon_r


        implicit none
        public :: Alloc_agrif_metrics_pmon_r
      contains
      subroutine Alloc_agrif_metrics_pmon_r(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pmon_r
          if (.not. allocated(Agrif_Gr % tabvars(56)% array2)) then
          allocate(Agrif_Gr % tabvars(56)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(56)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_pmon_r
      end module metrics_pmon_r
      module metrics_pnom_p


        implicit none
        public :: Alloc_agrif_metrics_pnom_p
      contains
      subroutine Alloc_agrif_metrics_pnom_p(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pnom_p
          if (.not. allocated(Agrif_Gr % tabvars(57)% array2)) then
          allocate(Agrif_Gr % tabvars(57)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(57)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_pnom_p
      end module metrics_pnom_p
      module metrics_pmon_p


        implicit none
        public :: Alloc_agrif_metrics_pmon_p
      contains
      subroutine Alloc_agrif_metrics_pmon_p(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pmon_p
          if (.not. allocated(Agrif_Gr % tabvars(58)% array2)) then
          allocate(Agrif_Gr % tabvars(58)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(58)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_pmon_p
      end module metrics_pmon_p
      module metrics_dndx


        implicit none
        public :: Alloc_agrif_metrics_dndx
      contains
      subroutine Alloc_agrif_metrics_dndx(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : dndx
          if (.not. allocated(Agrif_Gr % tabvars(59)% array2)) then
          allocate(Agrif_Gr % tabvars(59)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(59)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_dndx
      end module metrics_dndx
      module metrics_dmde


        implicit none
        public :: Alloc_agrif_metrics_dmde
      contains
      subroutine Alloc_agrif_metrics_dmde(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : dmde
          if (.not. allocated(Agrif_Gr % tabvars(60)% array2)) then
          allocate(Agrif_Gr % tabvars(60)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(60)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_dmde
      end module metrics_dmde
      module metrics_pnv


        implicit none
        public :: Alloc_agrif_metrics_pnv
      contains
      subroutine Alloc_agrif_metrics_pnv(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pn_v
          if (.not. allocated(Agrif_Gr % tabvars(61)% array2)) then
          allocate(Agrif_Gr % tabvars(61)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(61)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_pnv
      end module metrics_pnv
      module metrics_pmu


        implicit none
        public :: Alloc_agrif_metrics_pmu
      contains
      subroutine Alloc_agrif_metrics_pmu(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pm_u
          if (.not. allocated(Agrif_Gr % tabvars(62)% array2)) then
          allocate(Agrif_Gr % tabvars(62)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(62)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_pmu
      end module metrics_pmu
      module metrics_pmv


        implicit none
        public :: Alloc_agrif_metrics_pmv
      contains
      subroutine Alloc_agrif_metrics_pmv(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pm_v
          if (.not. allocated(Agrif_Gr % tabvars(63)% array2)) then
          allocate(Agrif_Gr % tabvars(63)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(63)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_pmv
      end module metrics_pmv
      module metrics_pnu


        implicit none
        public :: Alloc_agrif_metrics_pnu
      contains
      subroutine Alloc_agrif_metrics_pnu(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pn_u
          if (.not. allocated(Agrif_Gr % tabvars(64)% array2)) then
          allocate(Agrif_Gr % tabvars(64)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(64)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_pnu
      end module metrics_pnu
      module metrics_on_p


        implicit none
        public :: Alloc_agrif_metrics_on_p
      contains
      subroutine Alloc_agrif_metrics_on_p(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : on_p
          if (.not. allocated(Agrif_Gr % tabvars(65)% array2)) then
          allocate(Agrif_Gr % tabvars(65)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(65)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_on_p
      end module metrics_on_p
      module metrics_omp


        implicit none
        public :: Alloc_agrif_metrics_omp
      contains
      subroutine Alloc_agrif_metrics_omp(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : om_p
          if (.not. allocated(Agrif_Gr % tabvars(66)% array2)) then
          allocate(Agrif_Gr % tabvars(66)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(66)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_omp
      end module metrics_omp
      module metrics_on_v


        implicit none
        public :: Alloc_agrif_metrics_on_v
      contains
      subroutine Alloc_agrif_metrics_on_v(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : on_v
          if (.not. allocated(Agrif_Gr % tabvars(67)% array2)) then
          allocate(Agrif_Gr % tabvars(67)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(67)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_on_v
      end module metrics_on_v
      module metrics_omv


        implicit none
        public :: Alloc_agrif_metrics_omv
      contains
      subroutine Alloc_agrif_metrics_omv(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : om_v
          if (.not. allocated(Agrif_Gr % tabvars(68)% array2)) then
          allocate(Agrif_Gr % tabvars(68)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(68)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_omv
      end module metrics_omv
      module metrics_on_u


        implicit none
        public :: Alloc_agrif_metrics_on_u
      contains
      subroutine Alloc_agrif_metrics_on_u(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : on_u
          if (.not. allocated(Agrif_Gr % tabvars(69)% array2)) then
          allocate(Agrif_Gr % tabvars(69)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(69)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_on_u
      end module metrics_on_u
      module metrics_omu


        implicit none
        public :: Alloc_agrif_metrics_omu
      contains
      subroutine Alloc_agrif_metrics_omu(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : om_u
          if (.not. allocated(Agrif_Gr % tabvars(70)% array2)) then
          allocate(Agrif_Gr % tabvars(70)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(70)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_omu
      end module metrics_omu
      module metrics_on_r


        implicit none
        public :: Alloc_agrif_metrics_on_r
      contains
      subroutine Alloc_agrif_metrics_on_r(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : on_r
          if (.not. allocated(Agrif_Gr % tabvars(71)% array2)) then
          allocate(Agrif_Gr % tabvars(71)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(71)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_on_r
      end module metrics_on_r
      module metrics_omr


        implicit none
        public :: Alloc_agrif_metrics_omr
      contains
      subroutine Alloc_agrif_metrics_omr(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : om_r
          if (.not. allocated(Agrif_Gr % tabvars(72)% array2)) then
          allocate(Agrif_Gr % tabvars(72)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(72)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_omr
      end module metrics_omr
      module metrics_pn


        implicit none
        public :: Alloc_agrif_metrics_pn
      contains
      subroutine Alloc_agrif_metrics_pn(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pn
          if (.not. allocated(Agrif_Gr % tabvars(73)% array2)) then
          allocate(Agrif_Gr % tabvars(73)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(73)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_pn
      end module metrics_pn
      module metrics_pm


        implicit none
        public :: Alloc_agrif_metrics_pm
      contains
      subroutine Alloc_agrif_metrics_pm(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : pm
          if (.not. allocated(Agrif_Gr % tabvars(74)% array2)) then
          allocate(Agrif_Gr % tabvars(74)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(74)% array2 = 0
      endif
      end subroutine Alloc_agrif_metrics_pm
      end module metrics_pm
      module grid_lonv


        implicit none
        public :: Alloc_agrif_grid_lonv
      contains
      subroutine Alloc_agrif_grid_lonv(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : lonv
          if (.not. allocated(Agrif_Gr % tabvars(75)% array2)) then
          allocate(Agrif_Gr % tabvars(75)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(75)% array2 = 0
      endif
      end subroutine Alloc_agrif_grid_lonv
      end module grid_lonv
      module grid_latv


        implicit none
        public :: Alloc_agrif_grid_latv
      contains
      subroutine Alloc_agrif_grid_latv(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : latv
          if (.not. allocated(Agrif_Gr % tabvars(76)% array2)) then
          allocate(Agrif_Gr % tabvars(76)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(76)% array2 = 0
      endif
      end subroutine Alloc_agrif_grid_latv
      end module grid_latv
      module grid_lonu


        implicit none
        public :: Alloc_agrif_grid_lonu
      contains
      subroutine Alloc_agrif_grid_lonu(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : lonu
          if (.not. allocated(Agrif_Gr % tabvars(77)% array2)) then
          allocate(Agrif_Gr % tabvars(77)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(77)% array2 = 0
      endif
      end subroutine Alloc_agrif_grid_lonu
      end module grid_lonu
      module grid_latu


        implicit none
        public :: Alloc_agrif_grid_latu
      contains
      subroutine Alloc_agrif_grid_latu(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : latu
          if (.not. allocated(Agrif_Gr % tabvars(78)% array2)) then
          allocate(Agrif_Gr % tabvars(78)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(78)% array2 = 0
      endif
      end subroutine Alloc_agrif_grid_latu
      end module grid_latu
      module grid_lonr


        implicit none
        public :: Alloc_agrif_grid_lonr
      contains
      subroutine Alloc_agrif_grid_lonr(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : lonr
          if (.not. allocated(Agrif_Gr % tabvars(79)% array2)) then
          allocate(Agrif_Gr % tabvars(79)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(79)% array2 = 0
      endif
      end subroutine Alloc_agrif_grid_lonr
      end module grid_lonr
      module grid_latr


        implicit none
        public :: Alloc_agrif_grid_latr
      contains
      subroutine Alloc_agrif_grid_latr(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : latr
          if (.not. allocated(Agrif_Gr % tabvars(80)% array2)) then
          allocate(Agrif_Gr % tabvars(80)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(80)% array2 = 0
      endif
      end subroutine Alloc_agrif_grid_latr
      end module grid_latr
      module grid_angler


        implicit none
        public :: Alloc_agrif_grid_angler
      contains
      subroutine Alloc_agrif_grid_angler(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : angler
          if (.not. allocated(Agrif_Gr % tabvars(81)% array2)) then
          allocate(Agrif_Gr % tabvars(81)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(81)% array2 = 0
      endif
      end subroutine Alloc_agrif_grid_angler
      end module grid_angler
      module grid_fomn


        implicit none
        public :: Alloc_agrif_grid_fomn
      contains
      subroutine Alloc_agrif_grid_fomn(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : fomn
          if (.not. allocated(Agrif_Gr % tabvars(82)% array2)) then
          allocate(Agrif_Gr % tabvars(82)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(82)% array2 = 0
      endif
      end subroutine Alloc_agrif_grid_fomn
      end module grid_fomn
      module grid_f


        implicit none
        public :: Alloc_agrif_grid_f
      contains
      subroutine Alloc_agrif_grid_f(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : f
          if (.not. allocated(Agrif_Gr % tabvars(83)% array2)) then
          allocate(Agrif_Gr % tabvars(83)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(83)% array2 = 0
      endif
      end subroutine Alloc_agrif_grid_f
      end module grid_f
      module grid_hinv


        implicit none
        public :: Alloc_agrif_grid_hinv
      contains
      subroutine Alloc_agrif_grid_hinv(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : hinv
          if (.not. allocated(Agrif_Gr % tabvars(84)% array2)) then
          allocate(Agrif_Gr % tabvars(84)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(84)% array2 = 0
      endif
      end subroutine Alloc_agrif_grid_hinv
      end module grid_hinv
      module grid_h


        implicit none
        public :: Alloc_agrif_grid_h
      contains
      subroutine Alloc_agrif_grid_h(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : h
          if (.not. allocated(Agrif_Gr % tabvars(85)% array2)) then
          allocate(Agrif_Gr % tabvars(85)% array2(-1 :  Agrif_tabvars_i(
     &200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_i
     &(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(85)% array2 = 0
      endif
      end subroutine Alloc_agrif_grid_h
      end module grid_h
      module cncscrum


        implicit none
        public :: Alloc_agrif_cncscrum
      contains
      subroutine Alloc_agrif_cncscrum(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : vname
          if (.not. allocated(Agrif_Gr % tabvars_c(1)% carray2)) then
          allocate(Agrif_Gr % tabvars_c(1)% carray2(1 : 20,1 : 500))
      endif
      end subroutine Alloc_agrif_cncscrum
      end module cncscrum
      module incscrum


        implicit none
        public :: Alloc_agrif_incscrum
      contains
      subroutine Alloc_agrif_incscrum(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      !! ALLOCATION OF VARIABLE : wrtavg
          if (.not. allocated(Agrif_Gr % tabvars_l(2)% larray1)) then
          allocate(Agrif_Gr % tabvars_l(2)% larray1(1 : 500+NT))
      endif
      !! ALLOCATION OF VARIABLE : wrthis
          if (.not. allocated(Agrif_Gr % tabvars_l(3)% larray1)) then
          allocate(Agrif_Gr % tabvars_l(3)% larray1(1 : 500+NT))
      endif
      !! ALLOCATION OF VARIABLE : avgT
          if (.not. allocated(Agrif_Gr % tabvars_i(63)% iarray1)) then
          allocate(Agrif_Gr % tabvars_i(63)% iarray1(1 : NT))
          Agrif_Gr % tabvars_i(63)% iarray1 = 0
      endif
      !! ALLOCATION OF VARIABLE : hisT
          if (.not. allocated(Agrif_Gr % tabvars_i(95)% iarray1)) then
          allocate(Agrif_Gr % tabvars_i(95)% iarray1(1 : NT))
          Agrif_Gr % tabvars_i(95)% iarray1 = 0
      endif
      !! ALLOCATION OF VARIABLE : rstT
          if (.not. allocated(Agrif_Gr % tabvars_i(114)% iarray1)) then
          allocate(Agrif_Gr % tabvars_i(114)% iarray1(1 : NT))
          Agrif_Gr % tabvars_i(114)% iarray1 = 0
      endif
      !! ALLOCATION OF VARIABLE : nttsrc
          if (.not. allocated(Agrif_Gr % tabvars_i(126)% iarray1)) then
          allocate(Agrif_Gr % tabvars_i(126)% iarray1(1 : NT))
          Agrif_Gr % tabvars_i(126)% iarray1 = 0
      endif
      !! ALLOCATION OF VARIABLE : ntstf
          if (.not. allocated(Agrif_Gr % tabvars_i(127)% iarray1)) then
          allocate(Agrif_Gr % tabvars_i(127)% iarray1(1 : NT))
          Agrif_Gr % tabvars_i(127)% iarray1 = 0
      endif
      !! ALLOCATION OF VARIABLE : nttclm
          if (.not. allocated(Agrif_Gr % tabvars_i(128)% iarray1)) then
          allocate(Agrif_Gr % tabvars_i(128)% iarray1(1 : NT))
          Agrif_Gr % tabvars_i(128)% iarray1 = 0
      endif
      end subroutine Alloc_agrif_incscrum
      end module incscrum
      module timers_roms


        implicit none
        public :: Alloc_agrif_timers_roms
      contains
      subroutine Alloc_agrif_timers_roms(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: NPP = 1
      !! ALLOCATION OF VARIABLE : proc
          if (.not. allocated(Agrif_Gr % tabvars_i(158)% iarray2)) then
          allocate(Agrif_Gr % tabvars_i(158)% iarray2(0 : 31,0 : NPP))
          Agrif_Gr % tabvars_i(158)% iarray2 = 0
      endif
      !! ALLOCATION OF VARIABLE : CPU_time
          if (.not. allocated(Agrif_Gr % tabvars(86)% sarray2)) then
          allocate(Agrif_Gr % tabvars(86)% sarray2(0 : 31,0 : NPP))
          Agrif_Gr % tabvars(86)% sarray2 = 0
      endif
      end subroutine Alloc_agrif_timers_roms
      end module timers_roms
      module scalars_main


        implicit none
        public :: Alloc_agrif_scalars_main
      contains
      subroutine Alloc_agrif_scalars_main(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : got_tini
          if (.not. allocated(Agrif_Gr % tabvars_l(10)% larray1)) then
          allocate(Agrif_Gr % tabvars_l(10)% larray1(1 : NT))
      endif
      !! ALLOCATION OF VARIABLE : weight
          if (.not. allocated(Agrif_Gr % tabvars(87)% array2)) then
          allocate(Agrif_Gr % tabvars(87)% array2(1 : 6,0 : NWEIGHT))
          Agrif_Gr % tabvars(87)% array2 = 0
      endif
      !! ALLOCATION OF VARIABLE : tnu4
          if (.not. allocated(Agrif_Gr % tabvars(88)% array1)) then
          allocate(Agrif_Gr % tabvars(88)% array1(1 : NT))
          Agrif_Gr % tabvars(88)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : tnu2
          if (.not. allocated(Agrif_Gr % tabvars(89)% array1)) then
          allocate(Agrif_Gr % tabvars(89)% array1(1 : NT))
          Agrif_Gr % tabvars(89)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : Cs_r
          if (.not. allocated(Agrif_Gr % tabvars(90)% array1)) then
          allocate(Agrif_Gr % tabvars(90)% array1(1 : N))
          Agrif_Gr % tabvars(90)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : sc_r
          if (.not. allocated(Agrif_Gr % tabvars(91)% array1)) then
          allocate(Agrif_Gr % tabvars(91)% array1(1 : N))
          Agrif_Gr % tabvars(91)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : Cs_w
          if (.not. allocated(Agrif_Gr % tabvars(92)% array1)) then
          allocate(Agrif_Gr % tabvars(92)% array1(0 : N))
          Agrif_Gr % tabvars(92)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : sc_w
          if (.not. allocated(Agrif_Gr % tabvars(93)% array1)) then
          allocate(Agrif_Gr % tabvars(93)% array1(0 : N))
          Agrif_Gr % tabvars(93)% array1 = 0
      endif
      end subroutine Alloc_agrif_scalars_main
      end module scalars_main
      module private_scratch


        implicit none
        public :: Alloc_agrif_private_scratch
      contains
      subroutine Alloc_agrif_private_scratch(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSA = 28
      !! ALLOCATION OF VARIABLE : A3d
          if (.not. allocated(Agrif_Gr % tabvars(94)% array3)) then
          allocate(Agrif_Gr % tabvars(94)% array3(1 :  Agrif_tabvars_i(1
     &86)%iarray0,1 : 5,0 : NPP-1))
          Agrif_Gr % tabvars(94)% array3 = 0
      endif
      !! ALLOCATION OF VARIABLE : A2d
          if (.not. allocated(Agrif_Gr % tabvars(95)% array3)) then
          allocate(Agrif_Gr % tabvars(95)% array3(1 :  Agrif_tabvars_i(1
     &87)%iarray0,1 : NSA,0 : NPP-1))
          Agrif_Gr % tabvars(95)% array3 = 0
      endif
      end subroutine Alloc_agrif_private_scratch
      end module private_scratch
