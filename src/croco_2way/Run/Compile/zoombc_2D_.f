












      subroutine u2dbc_interp_tile(Istr,Iend,Jstr,Jend
     &        ,DU_west4,DU_east4,DU_west6,DU_east6
     &        ,DU_south4,DU_north4,DU_south6,DU_north6)      

      use AGRIF_Util
      integer(4), parameter :: NWEIGHT = 1000
      interface
        subroutine Sub_Loop_u2dbc_interp_tile(Istr,Iend,Jstr,Jend,DU_wes
     &t4,DU_east4,DU_west6,DU_east6,DU_south4,DU_north4,DU_south6,DU_nor
     &th6,padd_E,Mm,padd_X,Lm,on_u,h,knew,zeta,SSH,om_u,ubclm,dtfast,uma
     &sk,nfast,DU_avg3,weight2,U2DTimeindex2,ubarid,common_index,dUinter
     &p,U2DTimeindex,iif,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,M
     &mmpi,Lmmpi)
          use AGRIF_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      real :: dtfast
      integer(4) :: nfast
      integer(4) :: U2DTimeindex2
      integer(4) :: ubarid
      integer(4) :: common_index
      integer(4) :: U2DTimeindex
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: SSH
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: DU_avg
     &3
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dUinterp
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(-1:Mm+2+padd_E) :: DU_west6
      real, dimension(-1:Mm+2+padd_E) :: DU_east6
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DU_west4
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DU_east4
      real, dimension(-1:Lm+2+padd_X) :: DU_south6
      real, dimension(-1:Lm+2+padd_X) :: DU_north6
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DU_south4
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DU_north4
        end subroutine Sub_Loop_u2dbc_interp_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(-1: Agrif_tabvars_i(197)%iarray0+2+ Agrif_tabvars_
     &i(188)%iarray0) :: DU_west6
      real, dimension(-1: Agrif_tabvars_i(197)%iarray0+2+ Agrif_tabvars_
     &i(188)%iarray0) :: DU_east6
      real, dimension(-1: Agrif_tabvars_i(197)%iarray0+2+ Agrif_tabvars_
     &i(188)%iarray0,0:NWEIGHT) :: DU_west4
      real, dimension(-1: Agrif_tabvars_i(197)%iarray0+2+ Agrif_tabvars_
     &i(188)%iarray0,0:NWEIGHT) :: DU_east4
      real, dimension(-1: Agrif_tabvars_i(200)%iarray0+2+ Agrif_tabvars_
     &i(189)%iarray0) :: DU_south6
      real, dimension(-1: Agrif_tabvars_i(200)%iarray0+2+ Agrif_tabvars_
     &i(189)%iarray0) :: DU_north6
      real, dimension(-1: Agrif_tabvars_i(200)%iarray0+2+ Agrif_tabvars_
     &i(189)%iarray0,0:NWEIGHT) :: DU_south4
      real, dimension(-1: Agrif_tabvars_i(200)%iarray0+2+ Agrif_tabvars_
     &i(189)%iarray0,0:NWEIGHT) :: DU_north4
      

        call Sub_Loop_u2dbc_interp_tile(Istr,Iend,Jstr,Jend,DU_west4,DU_
     &east4,DU_west6,DU_east6,DU_south4,DU_north4,DU_south6,DU_north6, A
     &grif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_t
     &abvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars
     &(69)%array2, Agrif_tabvars(85)%array2, Agrif_tabvars_i(179)%iarray
     &0, Agrif_tabvars(98)%array3, Agrif_tabvars(142)%array2, Agrif_tabv
     &ars(70)%array2, Agrif_tabvars(133)%array2, Agrif_tabvars_r(45)%arr
     &ay0, Agrif_tabvars(49)%array2, Agrif_tabvars_i(168)%iarray0, Agrif
     &_tabvars(19)%array3, Agrif_tabvars(34)%array2, Agrif_tabvars_i(46)
     &%iarray0, Agrif_tabvars_i(21)%iarray0, Agrif_tabvars_i(24)%iarray0
     &, Agrif_tabvars(27)%array2, Agrif_tabvars_i(47)%iarray0, Agrif_tab
     &vars_i(177)%iarray0, Agrif_tabvars_l(5)%larray0, Agrif_tabvars_l(4
     &)%larray0, Agrif_tabvars_l(7)%larray0, Agrif_tabvars_l(6)%larray0,
     & Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_u2dbc_interp_tile(Istr,Iend,Jstr,Jend,DU_west4
     &,DU_east4,DU_west6,DU_east6,DU_south4,DU_north4,DU_south6,DU_north
     &6,padd_E,Mm,padd_X,Lm,on_u,h,knew,zeta,SSH,om_u,ubclm,dtfast,umask
     &,nfast,DU_avg3,weight2,U2DTimeindex2,ubarid,common_index,dUinterp,
     &U2DTimeindex,iif,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,Mmm
     &pi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util

      include 'mpif.h'
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      real :: dtfast
      integer(4) :: nfast
      integer(4) :: U2DTimeindex2
      integer(4) :: ubarid
      integer(4) :: common_index
      integer(4) :: U2DTimeindex
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: SSH
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: DU_avg
     &3
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dUinterp
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(-1:Mm+2+padd_E) :: DU_west6
      real, dimension(-1:Mm+2+padd_E) :: DU_east6
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DU_west4
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DU_east4
      real, dimension(-1:Lm+2+padd_X) :: DU_south6
      real, dimension(-1:Lm+2+padd_X) :: DU_north6
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DU_south4
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DU_north4

                     
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                         
      integer(4) :: i
      integer(4) :: j
                                                                     
      real :: t1
      real :: t2
      real :: t3
      real :: t7
      real :: t8
      real :: t9
      real :: t10
      real :: t11
      real :: t12
      real :: t13
      real :: t14
      real :: t15
      real :: t16
      real :: t17
      real :: t18
      real :: t19
                       
      logical :: ptinterp
                                                 
      real :: t4
      real :: t5
      real :: t6
      real :: tfin
      real :: c1
      real :: c2
      real :: c3
      real :: cff1
      real :: cff2
      real :: cff3
      external :: u2dinterp
                                       
      integer(4) :: irhox
      integer(4) :: irhoy
      integer(4) :: irhot
                                  
      real :: rrhox
      real :: rrhoy
      real :: rrhot
                                   
      real :: tinterp
      real :: onemtinterp
                        
      integer(4) :: iter
                                        
      integer(4) :: ipu
      integer(4) :: jpu
      integer(4) :: parentnnew
                                
      integer(4) :: parentnbstep
                         
      real :: cffx
      real :: cffy
                                                        
      real, dimension(Jstr-2:Jend+2) :: DU_west
      real, dimension(Jstr-2:Jend+2) :: DU_east
                                                          
                                                                     
                                                         
      real, dimension(Istr-2:Iend+2) :: DU_south
      real, dimension(Istr-2:Iend+2) :: DU_north
                                                            
                                                                      
                      
      real :: lastcff
!$AGRIF_DO_NOT_TREAT
      integer*4 :: nbgrid,indinterp
      common/interp2d/indinterp,nbgrid
!$AGRIF_END_DO_NOT_TREAT
                        
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      irhox=Agrif_Irhox()
      irhoy=Agrif_Irhoy()
      irhot=Agrif_Irhot()
      rrhox = real(irhox)
      rrhoy = real(irhoy)
      rrhot = real(irhot)
      parentnbstep=
     & Agrif_Parent(iif)
      if (U2DTimeindex .NE. parentnbstep) then
C$OMP BARRIER
C$OMP MASTER
        dUinterp = 0.D0
        tinterp=1.D0
        Agrif_UseSpecialValue = .true.
        Agrif_SpecialValue = 0.D0
        nbgrid = Agrif_Fixed()
        common_index = 1
        Call Agrif_Bc_variable(ubarid,
     &    calledweight=tinterp,procname = u2dinterp)
        Agrif_UseSpecialvalue=.false.
C$OMP END MASTER
C$OMP BARRIER
        U2DTimeindex = parentnbstep
        U2DTimeindex2 = agrif_nb_step()
      endif
       if (agrif_nb_step() .EQ. U2DTimeindex2) then
          lastcff=0.D0
          do iter=iif,iif+irhot-1
          lastcff=lastcff+((iter+1-iif)/rrhot)*
     &        weight2(iif+irhot-1,iter)
          enddo
          lastcff=1.D0/lastcff
      if (.not.WEST_INTER) then
          i = Istr
          do j=Jstr,Jend
          DU_west4(j,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DU_west6(Jstr:Jend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=Jstr,Jend
             DU_west6(j) = DU_west6(j) +cff1*DU_west4(j,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=Jstr,Jend
          DU_west6(j)=DU_west6(j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DU_west4(j,iif-1)
          enddo
          enddo
          do j=Jstr,Jend
          DU_west6(j)=lastcff*((dUinterp(i,j)/rrhoy)-DU_west6(j))
     &                               * umask(i,j)
          enddo
          do j=Jstr,Jend
          DU_west6(j)=(DU_west6(j)-DU_west4(j,iif-1))/rrhot
          enddo
          endif
      endif
      if (.not.EAST_INTER) then
          i=Iend+1
          do j=Jstr,Jend
          DU_east4(j,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DU_east6(Jstr:Jend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=Jstr,Jend
             DU_east6(j) = DU_east6(j) +cff1*DU_east4(j,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=Jstr,Jend
          DU_east6(j)=DU_east6(j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DU_east4(j,iif-1)
          enddo
          enddo
          do j=Jstr,Jend
          DU_east6(j)=lastcff*((dUinterp(i,j)/rrhoy)-DU_east6(j))
     &                               * umask(i,j)
          enddo
          do j=Jstr,Jend
          DU_east6(j)=(DU_east6(j)-DU_east4(j,iif-1))/rrhot
          enddo
          endif
      endif
      if (.not.SOUTH_INTER) then
          j=Jstr-1
          do i=Istr,Iend
          DU_south4(i,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DU_south6(Istr:Iend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do i=Istr,Iend
             DU_south6(i) = DU_south6(i) +cff1*DU_south4(i,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do i=Istr,Iend
          DU_south6(i)=DU_south6(i)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DU_south4(i,iif-1)
          enddo
          enddo
          do i=Istr,Iend
          DU_south6(i)=lastcff*((dUinterp(i,j)/rrhox)-DU_south6(i))
     &                               * umask(i,j)
          enddo
          do i=Istr,Iend
          DU_south6(i)=(DU_south6(i)-DU_south4(i,iif-1))/rrhot
          enddo
          endif
      endif
      if (.not.NORTH_INTER) then
          j=Jend+1
          do i=Istr,Iend
          DU_north4(i,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DU_north6(Istr:Iend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do i=Istr,Iend
             DU_north6(i) = DU_north6(i) +cff1*DU_north4(i,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do i=Istr,Iend
          DU_north6(i)=DU_north6(i)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DU_north4(i,iif-1)
          enddo
          enddo
          do i=Istr,Iend
          DU_north6(i)=lastcff*((dUinterp(i,j)/rrhox)-DU_north6(i))
     &                               * umask(i,j)
          enddo
          do i=Istr,Iend
          DU_north6(i)=(DU_north6(i)-DU_north4(i,iif-1))/rrhot
          enddo
          endif
      endif
      endif
      if (.not.WEST_INTER) then
          i = Istr
          do j=Jstr,Jend
           DU_west4(j,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          do j=Jstr,Jend
            DU_west(j)=DU_west4(j,iif-1)+DU_west6(j)
          enddo
       endif
      if (.not.EAST_INTER) then
          i=Iend+1
          do j=Jstr,Jend
           DU_east4(j,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          do j=Jstr,Jend
            DU_east(j)=DU_east4(j,iif-1)+DU_east6(j)
          enddo
        endif
      if (.not.SOUTH_INTER) then
          j=Jstr-1
          do i=Istr,Iend
           DU_south4(i,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          do i=Istr,Iend
            DU_south(i)=DU_south4(i,iif-1)+DU_south6(i)
          enddo
       endif
      if (.not.NORTH_INTER) then
          j=Jend+1
          do i=Istr,Iend
           DU_north4(i,iif-1) = DU_avg3(i,j,iif-1)
          enddo
          do i=Istr,Iend
            DU_north(i)=DU_north4(i,iif-1)+DU_north6(i)
          enddo
       endif
      cffx = g*dtfast*2.D0/(1.D0+rrhox)
      if (.not.WEST_INTER) then
        do j=Jstr,Jend
          ubclm(Istr,j)=
     &                   (cffx/om_u(Istr,j))*
     &                   (SSH(Istr,j)-zeta(Istr,j,knew))  +
     &              (2.D0*DU_west(j)/((h(Istr-1,j)+zeta(Istr-1,j,knew)
     &                                 +h(Istr,j)+zeta(Istr,j,knew))
     &                                                *on_u(Istr,j)))
     &                  *umask(Istr,j)
         enddo
      endif
      if (.not.EAST_INTER) then
        do j=Jstr,Jend
          ubclm(Iend+1,j)=
     &                    -(cffx/om_u(Iend+1,j))*
     &                     (SSH(Iend,j)-zeta(Iend,j,knew)) +
     &                  (2.D0*DU_east(j)/(( h(Iend,j)+zeta(Iend,j,knew)
     &                              +h(Iend+1,j)+zeta(Iend+1,j,knew))
     &                                              *on_u(Iend+1,j)))
     &                      *umask(Iend+1,j)
          enddo
      endif
      if (.not.SOUTH_INTER) then
        do i=Istr,Iend
          ubclm(i,Jstr-1)=
     &              (2.D0*DU_south(i)/(( h(i,Jstr-1)+zeta(i,Jstr-1,knew)
     &                           +h(i-1,Jstr-1)+zeta(i-1,Jstr-1,knew))
     &                                                *on_u(i,Jstr-1)))
     &                  *umask(i,Jstr-1)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=Istr,Iend
          ubclm(i,Jend+1)=
     &            (2.D0*DU_north(i)/(( h(i,Jend+1)+zeta(i,Jend+1,knew)
     &                         +h(i-1,Jend+1)+zeta(i-1,Jend+1,knew))
     &                                             *on_u(i,Jend+1)))
     &                *umask(i,Jend+1)
         enddo
      endif
      return
       
      

      end subroutine Sub_Loop_u2dbc_interp_tile

      subroutine u2Dinterp(tabres,i1,i2,j1,j2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_u2Dinterp(tabres,i1,i2,j1,j2,before,padd_E,M
     &m,padd_X,Lm,dUinterp,common_index,weight2,knew,ubar,on_u,kstp,zeta
     &,h,nfast,coarsevaluesinterp,iif,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: common_index
      integer(4) :: knew
      integer(4) :: kstp
      integer(4) :: nfast
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dUinterp
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, allocatable, dimension(:,:) :: coarsevaluesinterp
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
        end subroutine Sub_Loop_u2Dinterp

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      

        call Sub_Loop_u2Dinterp(tabres,i1,i2,j1,j2,before, Agrif_tabvars
     &_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189
     &)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(27)%array2,
     & Agrif_tabvars_i(24)%iarray0, Agrif_tabvars(34)%array2, Agrif_tabv
     &ars_i(179)%iarray0, Agrif_tabvars(97)%array3, Agrif_tabvars(69)%ar
     &ray2, Agrif_tabvars_i(181)%iarray0, Agrif_tabvars(98)%array3, Agri
     &f_tabvars(85)%array2, Agrif_tabvars_i(168)%iarray0, Agrif_tabvars(
     &1)%array2, Agrif_tabvars_i(177)%iarray0, Agrif_tabvars_i(194)%iarr
     &ay0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_u2Dinterp(tabres,i1,i2,j1,j2,before,padd_E,Mm,
     &padd_X,Lm,dUinterp,common_index,weight2,knew,ubar,on_u,kstp,zeta,h
     &,nfast,coarsevaluesinterp,iif,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: common_index
      integer(4) :: knew
      integer(4) :: kstp
      integer(4) :: nfast
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dUinterp
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, allocatable, dimension(:,:) :: coarsevaluesinterp
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                             
                                
                      
                                
      integer(4) :: i
      integer(4) :: j
      integer(4) :: iter
      integer(4) :: isize
                    
      real :: cff1
!$AGRIF_DO_NOT_TREAT
      integer*4 :: indinterp,nbgrid
      common/interp2d/indinterp,nbgrid
!$AGRIF_END_DO_NOT_TREAT
                                 
      integer(4) :: oldindinterp
       if (before) then
       isize = (j2-j1+1)*(i2-i1+1)
       IF (iif == 1) THEN
         IF (.NOT.allocated(coarsevaluesinterp)) THEN
         Allocate(coarsevaluesinterp(isize,0:nfast))
         ELSE
         CALL checksizeinterp(indinterp+isize)
         ENDIF
         oldindinterp = indinterp
         do j=j1,j2
         do i=i1,i2
         oldindinterp=oldindinterp+1
         coarsevaluesinterp(oldindinterp,0) =
     &     0.5D0*(h(i-1,j)+zeta(i-1,j,kstp)+h(i,j)+
     &    zeta(i,j,kstp))*on_u(i,j)*ubar(i,j,kstp)
         enddo
         enddo
       ENDIF
       oldindinterp = indinterp
       do j=j1,j2
       do i=i1,i2
       oldindinterp=oldindinterp+1
       coarsevaluesinterp(oldindinterp,iif) =
     &   0.5D0*(h(i-1,j)+zeta(i-1,j,knew)+h(i,j)+
     &  zeta(i,j,knew))*on_u(i,j)*ubar(i,j,knew)
       enddo
       enddo
       do iter=0,iif
          cff1 = weight2(iif,iter)
          call copy1d(tabres,coarsevaluesinterp(indinterp+1,iter),
     &                cff1,isize)
       enddo
       indinterp = oldindinterp
       else
       if (common_index == 1) then
         dUinterp(i1:i2,j1:j2) = tabres
        endif
        endif
      return
       
      

      end subroutine Sub_Loop_u2Dinterp

      subroutine u2Dinterp_old(tabres,i1,i2,j1,j2)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_u2Dinterp_old(tabres,i1,i2,j1,j2,padd_E,Mm,p
     &add_X,Lm,weight2,knew,ubar,on_u,kstp,zeta,h,nfast,coarsevaluesinte
     &rp,iif,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: kstp
      integer(4) :: nfast
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, allocatable, dimension(:,:) :: coarsevaluesinterp
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
        end subroutine Sub_Loop_u2Dinterp_old

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      

        call Sub_Loop_u2Dinterp_old(tabres,i1,i2,j1,j2, Agrif_tabvars_i(
     &188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%i
     &array0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(34)%array2, Ag
     &rif_tabvars_i(179)%iarray0, Agrif_tabvars(97)%array3, Agrif_tabvar
     &s(69)%array2, Agrif_tabvars_i(181)%iarray0, Agrif_tabvars(98)%arra
     &y3, Agrif_tabvars(85)%array2, Agrif_tabvars_i(168)%iarray0, Agrif_
     &tabvars(1)%array2, Agrif_tabvars_i(177)%iarray0, Agrif_tabvars_i(1
     &94)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_u2Dinterp_old(tabres,i1,i2,j1,j2,padd_E,Mm,pad
     &d_X,Lm,weight2,knew,ubar,on_u,kstp,zeta,h,nfast,coarsevaluesinterp
     &,iif,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: kstp
      integer(4) :: nfast
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, allocatable, dimension(:,:) :: coarsevaluesinterp
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                             
                                
                                
      integer(4) :: i
      integer(4) :: j
      integer(4) :: iter
      integer(4) :: isize
                    
      real :: cff1
!$AGRIF_DO_NOT_TREAT
      integer*4 :: indinterp,nbgrid
      common/interp2d/indinterp,nbgrid
!$AGRIF_END_DO_NOT_TREAT
                                 
      integer(4) :: oldindinterp
       isize = (j2-j1+1)*(i2-i1+1)
       IF (iif == 1) THEN
       IF (.NOT.allocated(coarsevaluesinterp)) THEN
       Allocate(coarsevaluesinterp(isize,0:nfast))
       ELSE
       CALL checksizeinterp(indinterp+isize)
       ENDIF
       oldindinterp = indinterp
       do j=j1,j2
       do i=i1,i2
       oldindinterp=oldindinterp+1
       coarsevaluesinterp(oldindinterp,0) =
     &  0.5D0*(h(i-1,j)+zeta(i-1,j,kstp)+h(i,j)+
     &  zeta(i,j,kstp))*on_u(i,j)*ubar(i,j,kstp)
       enddo
       enddo
       ENDIF
       oldindinterp = indinterp
       do j=j1,j2
       do i=i1,i2
       oldindinterp=oldindinterp+1
       coarsevaluesinterp(oldindinterp,iif) =
     &   0.5D0*(h(i-1,j)+zeta(i-1,j,knew)+h(i,j)+
     &  zeta(i,j,knew))*on_u(i,j)*ubar(i,j,knew)
       enddo
       enddo
       do iter=0,iif
          cff1 = weight2(iif,iter)
          call copy1d(tabres,coarsevaluesinterp(indinterp+1,iter),
     &                cff1,isize)
       enddo
       indinterp = oldindinterp
      return
       
      

      end subroutine Sub_Loop_u2Dinterp_old

      subroutine v2dbc_interp_tile(Istr,Iend,Jstr,Jend
     &        ,DV_west4,DV_east4,DV_west6,DV_east6
     &        ,DV_south4,DV_north4,DV_south6,DV_north6)      

      use AGRIF_Util
      integer(4), parameter :: NWEIGHT = 1000
      interface
        subroutine Sub_Loop_v2dbc_interp_tile(Istr,Iend,Jstr,Jend,DV_wes
     &t4,DV_east4,DV_west6,DV_east6,DV_south4,DV_north4,DV_south6,DV_nor
     &th6,padd_E,Mm,padd_X,Lm,om_v,h,knew,zeta,SSH,on_v,vbclm,dtfast,vma
     &sk,nfast,DV_avg3,weight2,V2DTimeindex2,vbarid,common_index,dVinter
     &p,V2DTimeindex,iif,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,M
     &mmpi,Lmmpi)
          use AGRIF_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      real :: dtfast
      integer(4) :: nfast
      integer(4) :: V2DTimeindex2
      integer(4) :: vbarid
      integer(4) :: common_index
      integer(4) :: V2DTimeindex
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: SSH
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: DV_avg
     &3
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dVinterp
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(-1:Mm+2+padd_E) :: DV_west6
      real, dimension(-1:Mm+2+padd_E) :: DV_east6
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DV_west4
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DV_east4
      real, dimension(-1:Lm+2+padd_X) :: DV_south6
      real, dimension(-1:Lm+2+padd_X) :: DV_north6
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DV_south4
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DV_north4
        end subroutine Sub_Loop_v2dbc_interp_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(-1: Agrif_tabvars_i(197)%iarray0+2+ Agrif_tabvars_
     &i(188)%iarray0) :: DV_west6
      real, dimension(-1: Agrif_tabvars_i(197)%iarray0+2+ Agrif_tabvars_
     &i(188)%iarray0) :: DV_east6
      real, dimension(-1: Agrif_tabvars_i(197)%iarray0+2+ Agrif_tabvars_
     &i(188)%iarray0,0:NWEIGHT) :: DV_west4
      real, dimension(-1: Agrif_tabvars_i(197)%iarray0+2+ Agrif_tabvars_
     &i(188)%iarray0,0:NWEIGHT) :: DV_east4
      real, dimension(-1: Agrif_tabvars_i(200)%iarray0+2+ Agrif_tabvars_
     &i(189)%iarray0) :: DV_south6
      real, dimension(-1: Agrif_tabvars_i(200)%iarray0+2+ Agrif_tabvars_
     &i(189)%iarray0) :: DV_north6
      real, dimension(-1: Agrif_tabvars_i(200)%iarray0+2+ Agrif_tabvars_
     &i(189)%iarray0,0:NWEIGHT) :: DV_south4
      real, dimension(-1: Agrif_tabvars_i(200)%iarray0+2+ Agrif_tabvars_
     &i(189)%iarray0,0:NWEIGHT) :: DV_north4
      

        call Sub_Loop_v2dbc_interp_tile(Istr,Iend,Jstr,Jend,DV_west4,DV_
     &east4,DV_west6,DV_east6,DV_south4,DV_north4,DV_south6,DV_north6, A
     &grif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_t
     &abvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars
     &(68)%array2, Agrif_tabvars(85)%array2, Agrif_tabvars_i(179)%iarray
     &0, Agrif_tabvars(98)%array3, Agrif_tabvars(142)%array2, Agrif_tabv
     &ars(67)%array2, Agrif_tabvars(132)%array2, Agrif_tabvars_r(45)%arr
     &ay0, Agrif_tabvars(48)%array2, Agrif_tabvars_i(168)%iarray0, Agrif
     &_tabvars(18)%array3, Agrif_tabvars(34)%array2, Agrif_tabvars_i(44)
     &%iarray0, Agrif_tabvars_i(20)%iarray0, Agrif_tabvars_i(24)%iarray0
     &, Agrif_tabvars(26)%array2, Agrif_tabvars_i(45)%iarray0, Agrif_tab
     &vars_i(177)%iarray0, Agrif_tabvars_l(5)%larray0, Agrif_tabvars_l(4
     &)%larray0, Agrif_tabvars_l(7)%larray0, Agrif_tabvars_l(6)%larray0,
     & Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_v2dbc_interp_tile(Istr,Iend,Jstr,Jend,DV_west4
     &,DV_east4,DV_west6,DV_east6,DV_south4,DV_north4,DV_south6,DV_north
     &6,padd_E,Mm,padd_X,Lm,om_v,h,knew,zeta,SSH,on_v,vbclm,dtfast,vmask
     &,nfast,DV_avg3,weight2,V2DTimeindex2,vbarid,common_index,dVinterp,
     &V2DTimeindex,iif,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,Mmm
     &pi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util

      include 'mpif.h'
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      real :: dtfast
      integer(4) :: nfast
      integer(4) :: V2DTimeindex2
      integer(4) :: vbarid
      integer(4) :: common_index
      integer(4) :: V2DTimeindex
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: SSH
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: DV_avg
     &3
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dVinterp
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(-1:Mm+2+padd_E) :: DV_west6
      real, dimension(-1:Mm+2+padd_E) :: DV_east6
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DV_west4
      real, dimension(-1:Mm+2+padd_E,0:NWEIGHT) :: DV_east4
      real, dimension(-1:Lm+2+padd_X) :: DV_south6
      real, dimension(-1:Lm+2+padd_X) :: DV_north6
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DV_south4
      real, dimension(-1:Lm+2+padd_X,0:NWEIGHT) :: DV_north4

                     
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                         
      integer(4) :: i
      integer(4) :: j
                                                               
      real :: t1
      real :: t2
      real :: t3
      real :: t4
      real :: t5
      real :: t6
      real :: dv1
      real :: dv1np1
      real :: dv2
      real :: tfin
      real :: c1
      real :: c2
      real :: c3
      real :: cff1
                            
      real :: t7
      real :: t8
      real :: t9
      real :: t10
      real :: t11
      external :: v2dinterp
                                       
      integer(4) :: irhox
      integer(4) :: irhoy
      integer(4) :: irhot
                                  
      real :: rrhox
      real :: rrhoy
      real :: rrhot
                                   
      real :: tinterp
      real :: onemtinterp
                        
      integer(4) :: iter
                                
      integer(4) :: parentnbstep
                   
      real :: cffy
                                                        
      real, dimension(Jstr-2:Jend+2) :: DV_west
      real, dimension(Jstr-2:Jend+2) :: DV_east
                                                          
                                                                     
                                                         
      real, dimension(Istr-2:Iend+2) :: DV_south
      real, dimension(Istr-2:Iend+2) :: DV_north
                                                            
                                                                      
                      
      real :: lastcff
                                                   
      real, allocatable, dimension(:,:) :: tabtemp2d
!$AGRIF_DO_NOT_TREAT
      integer*4 :: indinterp,nbgrid
      common/interp2d/indinterp,nbgrid
!$AGRIF_END_DO_NOT_TREAT
                        
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      irhox=Agrif_Irhox()
      irhoy=Agrif_Irhoy()
      irhot=Agrif_Irhot()
      rrhox = real(irhox)
      rrhoy = real(irhoy)
      rrhot = real(irhot)
      parentnbstep=
     &  Agrif_Parent(iif)
      if (V2DTimeindex .NE. parentnbstep) then
C$OMP BARRIER
C$OMP MASTER
        dVinterp = 0.D0
        tinterp=1.D0
        Agrif_UseSpecialValue = .true.
        Agrif_SpecialValue = 0.D0
        nbgrid = Agrif_Fixed()
        common_index = 1
        Call Agrif_Bc_variable(vbarid,calledweight=tinterp,
     &   procname = v2dinterp)
        Agrif_UseSpecialvalue=.false.
C$OMP END MASTER
C$OMP BARRIER
        V2DTimeindex = parentnbstep
        V2DTimeindex2 = agrif_nb_step()
      endif
      if (agrif_nb_step() .EQ. V2DTimeindex2) then
          lastcff=0.D0
          do iter=iif,iif+irhot-1
          lastcff=lastcff+((iter+1-iif)/rrhot)*
     &        weight2(iif+irhot-1,iter)
          enddo
          lastcff=1.D0/lastcff
      if (.not.SOUTH_INTER) then
          j=Jstr
          do i=Istr,Iend
          DV_south4(i,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DV_south6(Istr:Iend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do i=Istr,Iend
             DV_south6(i) = DV_south6(i) +cff1*DV_south4(i,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do i=Istr,Iend
          DV_south6(i)=DV_south6(i)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DV_south4(i,iif-1)
          enddo
          enddo
          do i=Istr,Iend
          DV_south6(i)=lastcff*((dVinterp(i,j)/rrhox)-DV_south6(i))
     &                               * vmask(i,j)
          enddo
          do i=Istr,Iend
          DV_south6(i)=(DV_south6(i)-DV_south4(i,iif-1))/rrhot
          enddo
          endif
      endif
      if (.not.NORTH_INTER) then
          j=Jend+1
          do i=Istr,Iend
          DV_north4(i,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DV_north6(Istr:Iend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do i=Istr,Iend
             DV_north6(i) = DV_north6(i) +cff1*DV_north4(i,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do i=Istr,Iend
          DV_north6(i)=DV_north6(i)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DV_north4(i,iif-1)
          enddo
          enddo
          do i=Istr,Iend
          DV_north6(i)=lastcff*((dVinterp(i,j)/rrhox)-DV_north6(i))
     &                               * vmask(i,j)
          enddo
          do i=Istr,Iend
          DV_north6(i)=(DV_north6(i)-DV_north4(i,iif-1))/rrhot
          enddo
          endif
      endif
      if (.not.WEST_INTER) then
          i = Istr-1
          do j=Jstr,Jend
          DV_west4(j,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DV_west6(Jstr:Jend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=Jstr,Jend
             DV_west6(j) = DV_west6(j) +cff1*DV_west4(j,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=Jstr,Jend
          DV_west6(j)=DV_west6(j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DV_west4(j,iif-1)
          enddo
          enddo
          do j=Jstr,Jend
          DV_west6(j)=lastcff*((dVinterp(i,j)/rrhoy)-DV_west6(j))
     &                               * vmask(i,j)
          enddo
          do j=Jstr,Jend
          DV_west6(j)=(DV_west6(j)-DV_west4(j,iif-1))/rrhot
          enddo
          endif
      endif
      if (.not.EAST_INTER) then
          i=Iend+1
          do j=Jstr,Jend
          DV_east4(j,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          if (iif+irhot-1<=nfast) then
           DV_east6(Jstr:Jend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=Jstr,Jend
             DV_east6(j) = DV_east6(j) +cff1*DV_east4(j,iter)
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=Jstr,Jend
          DV_east6(j)=DV_east6(j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*DV_east4(j,iif-1)
          enddo
          enddo
          do j=Jstr,Jend
          DV_east6(j)=lastcff*((dVinterp(i,j)/rrhoy)-DV_east6(j))
     &                               * vmask(i,j)
          enddo
          do j=Jstr,Jend
          DV_east6(j)=(DV_east6(j)-DV_east4(j,iif-1))/rrhot
          enddo
          endif
      endif
      endif
      if (.not.SOUTH_INTER) then
          j=Jstr
          do i=Istr,Iend
           DV_south4(i,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          do i=Istr,Iend
            DV_south(i)=DV_south4(i,iif-1)+DV_south6(i)
          enddo
      endif
      if (.not.NORTH_INTER) then
          j=Jend+1
          do i=Istr,Iend
           DV_north4(i,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          do i=Istr,Iend
            DV_north(i)=DV_north4(i,iif-1)+DV_north6(i)
          enddo
      endif
      if (.not.WEST_INTER) then
          i = Istr-1
          do j=Jstr,Jend
           DV_west4(j,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          do j=Jstr,Jend
            DV_west(j)=DV_west4(j,iif-1)+DV_west6(j)
          enddo
      endif
      if (.not.EAST_INTER) then
          i=Iend+1
          do j=Jstr,Jend
           DV_east4(j,iif-1) = DV_avg3(i,j,iif-1)
          enddo
          do j=Jstr,Jend
            DV_east(j)=DV_east4(j,iif-1)+DV_east6(j)
          enddo
      endif
      cffy = g*dtfast*2.D0/(1.D0+rrhoy)
      if (.not.SOUTH_INTER) then
         do i=Istr,Iend
           vbclm(i,Jstr)=
     &                   (cffy/on_v(i,Jstr))*
     &                   (SSH(i,Jstr)-zeta(i,Jstr,knew))   +
     &              (2.D0*DV_south(i)/((h(i,Jstr-1)+zeta(i,Jstr-1,knew)
     &                                  +h(i,Jstr)+zeta(i,Jstr,knew))
     &                                                 *om_v(i,Jstr)))
     &                  *vmask(i,Jstr)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=Istr,Iend
          vbclm(i,Jend+1)=
     &                    -(cffy/on_v(i,Jend+1))*
     &                     (SSH(i,Jend)-zeta(i,Jend,knew))  +
     &                  (2.D0*DV_north(i)/(( h(i,Jend)+zeta(i,Jend,knew)
     &                               +h(i,Jend+1)+zeta(i,Jend+1,knew))
     &                                               *om_v(i,Jend+1)))
     &                  *vmask(i,Jend+1)
         enddo
      endif
      if (.not.EAST_INTER) then
        do j=Jstr,Jend
          vbclm(Iend+1,j)=
     &             (2.D0*DV_east(j)/(( h(Iend+1,j)+zeta(Iend+1,j,knew)
     &                           +h(Iend+1,j-1)+zeta(Iend+1,j-1,knew))
     &                                             *om_v(Iend+1,j)))
     &                 *vmask(Iend+1,j)
         enddo
      endif
      if (.not.WEST_INTER) then
        do j=Jstr,Jend
          vbclm(Istr-1,j)=
     &                (2.D0*DV_west(j)/((h(Istr-1,j)+zeta(Istr-1,j,knew)
     &                           +h(Istr-1,j-1)+zeta(Istr-1,j-1,knew))
     &                                                *om_v(Istr-1,j)))
     &                  *vmask(Istr-1,j)
         enddo
      endif
      return
       
      

      end subroutine Sub_Loop_v2dbc_interp_tile

      subroutine v2Dinterp(tabres,i1,i2,j1,j2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_v2Dinterp(tabres,i1,i2,j1,j2,before,padd_E,M
     &m,padd_X,Lm,dVinterp,common_index,weight2,knew,vbar,om_v,kstp,zeta
     &,h,nfast,coarsevaluesinterp,iif,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: common_index
      integer(4) :: knew
      integer(4) :: kstp
      integer(4) :: nfast
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dVinterp
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, allocatable, dimension(:,:) :: coarsevaluesinterp
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
        end subroutine Sub_Loop_v2Dinterp

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      

        call Sub_Loop_v2Dinterp(tabres,i1,i2,j1,j2,before, Agrif_tabvars
     &_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189
     &)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(26)%array2,
     & Agrif_tabvars_i(24)%iarray0, Agrif_tabvars(34)%array2, Agrif_tabv
     &ars_i(179)%iarray0, Agrif_tabvars(96)%array3, Agrif_tabvars(68)%ar
     &ray2, Agrif_tabvars_i(181)%iarray0, Agrif_tabvars(98)%array3, Agri
     &f_tabvars(85)%array2, Agrif_tabvars_i(168)%iarray0, Agrif_tabvars(
     &1)%array2, Agrif_tabvars_i(177)%iarray0, Agrif_tabvars_i(194)%iarr
     &ay0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_v2Dinterp(tabres,i1,i2,j1,j2,before,padd_E,Mm,
     &padd_X,Lm,dVinterp,common_index,weight2,knew,vbar,om_v,kstp,zeta,h
     &,nfast,coarsevaluesinterp,iif,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: common_index
      integer(4) :: knew
      integer(4) :: kstp
      integer(4) :: nfast
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dVinterp
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, allocatable, dimension(:,:) :: coarsevaluesinterp
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                             
                                
                      
                                
      integer(4) :: i
      integer(4) :: j
      integer(4) :: iter
      integer(4) :: isize
                    
      real :: cff1
!$AGRIF_DO_NOT_TREAT
      integer*4 :: indinterp,nbgrid
      common/interp2d/indinterp,nbgrid
!$AGRIF_END_DO_NOT_TREAT
                                 
      integer(4) :: oldindinterp
       if (before) then
       isize = (j2-j1+1)*(i2-i1+1)
       IF (iif == 1) THEN
       IF (.NOT.allocated(coarsevaluesinterp)) THEN
       Allocate(coarsevaluesinterp(isize,0:nfast))
       ELSE
       CALL checksizeinterp(indinterp+isize)
       ENDIF
       oldindinterp = indinterp
       do j=j1,j2
       do i=i1,i2
       oldindinterp=oldindinterp+1
       coarsevaluesinterp(oldindinterp,0) =
     &  0.5D0*(h(i,j-1)+zeta(i,j-1,kstp)+h(i,j)+
     &  zeta(i,j,kstp))*om_v(i,j)*vbar(i,j,kstp)
       enddo
       enddo
       ENDIF
       oldindinterp = indinterp
       do j=j1,j2
       do i=i1,i2
       oldindinterp=oldindinterp+1
       coarsevaluesinterp(oldindinterp,iif) =
     &   0.5D0*(h(i,j-1)+zeta(i,j-1,knew)+h(i,j)+
     &  zeta(i,j,knew))*om_v(i,j)*vbar(i,j,knew)
       enddo
       enddo
       do iter=0,iif
          cff1 = weight2(iif,iter)
          call copy1d(tabres,coarsevaluesinterp(indinterp+1,iter),
     &                cff1,isize)
       enddo
       indinterp = oldindinterp
       else
       if (common_index == 1) then
         dVinterp(i1:i2,j1:j2) = tabres
        endif
        endif
      return
       
      

      end subroutine Sub_Loop_v2Dinterp

      subroutine copy1d(tab1,tab2,cff,isize)
      use Agrif_Types, only : Agrif_tabvars


      integer(4) :: isize
      real :: cff
      real, dimension(1:isize) :: tab1
      real, dimension(1:isize) :: tab2

                           
      integer(4) :: i
                  
                                         
      do i=1,isize
      tab1(i)=tab1(i)+cff*tab2(i)
      enddo
      return
      end subroutine copy1d
      subroutine checksizeinterp(isize)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_checksizeinterp(isize,nfast,coarsevaluesinte
     &rp,Mmmpi,Lmmpi,padd_E,Mm,padd_X,Lm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: nfast
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real, allocatable, dimension(:,:) :: coarsevaluesinterp
      integer(4) :: isize
        end subroutine Sub_Loop_checksizeinterp

      end interface
      integer(4) :: isize
      

        call Sub_Loop_checksizeinterp(isize, Agrif_tabvars_i(168)%iarray
     &0, Agrif_tabvars(1)%array2, Agrif_tabvars_i(194)%iarray0, Agrif_ta
     &bvars_i(195)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_
     &i(197)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)
     &%iarray0)

      end


      subroutine Sub_Loop_checksizeinterp(isize,nfast,coarsevaluesinterp
     &,Mmmpi,Lmmpi,padd_E,Mm,padd_X,Lm)

      use Agrif_Types, only : Agrif_tabvars


      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: nfast
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real, allocatable, dimension(:,:) :: coarsevaluesinterp
      integer(4) :: isize

                                                    
      real, allocatable, dimension(:,:) :: tempvalues
                            
      integer(4) :: n1
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
!$AGRIF_DO_NOT_TREAT
      integer*4 :: indinterp,nbgrid
      common/interp2d/indinterp,nbgrid
!$AGRIF_END_DO_NOT_TREAT
       IF (size(coarsevaluesinterp,1).LT.(isize)) THEN
       n1 = size(coarsevaluesinterp,1)
       allocate(tempvalues(n1,0:nfast))
       tempvalues=coarsevaluesinterp(1:n1,0:nfast)
       deallocate(coarsevaluesinterp)
       allocate(coarsevaluesinterp(isize,0:nfast))
       coarsevaluesinterp(1:n1,0:nfast) = tempvalues
       deallocate(tempvalues)
       ENDIF
      return
       
      

      end subroutine Sub_Loop_checksizeinterp

      subroutine zetabc_interp_tile(Istr,Iend,Jstr,Jend
     &        ,Zeta_west4,Zeta_east4,Zeta_west6,Zeta_east6
     &        ,Zeta_south4,Zeta_north4,Zeta_south6,Zeta_north6)      

      use AGRIF_Util
      integer(4), parameter :: NWEIGHT = 1000
      interface
        subroutine Sub_Loop_zetabc_interp_tile(Istr,Iend,Jstr,Jend,Zeta_
     &west4,Zeta_east4,Zeta_west6,Zeta_east6,Zeta_south4,Zeta_north4,Zet
     &a_south6,Zeta_north6,padd_E,Mm,padd_X,Lm,SSH,nfast,Zt_avg3,weight2
     &,ZetaTimeindex2,zetaid,dZtinterp,ZetaTimeindex,iif,NORTH_INTER,SOU
     &TH_INTER,EAST_INTER,WEST_INTER,Mmmpi,Lmmpi)
          use AGRIF_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nfast
      integer(4) :: ZetaTimeindex2
      integer(4) :: zetaid
      integer(4) :: ZetaTimeindex
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: SSH
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: Zt_avg
     &3
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dZtinterp
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-1:Istr,-1:Mm+2+padd_E) :: Zeta_west6
      real, dimension(Iend:Iend+1,-1:Mm+2+padd_E) :: Zeta_east6
      real, dimension(-1:Lm+2+padd_X,Jstr-1:Jstr) :: Zeta_south6
      real, dimension(-1:Lm+2+padd_X,Jend:Jend+1) :: Zeta_north6
      real, dimension(Istr-1:Istr,-1:Mm+2+padd_E,0:NWEIGHT) :: Zeta_west
     &4
      real, dimension(Iend:Iend+1,-1:Mm+2+padd_E,0:NWEIGHT) :: Zeta_east
     &4
      real, dimension(-1:Lm+2+padd_X,Jstr-1:Jstr,0:NWEIGHT) :: Zeta_sout
     &h4
      real, dimension(-1:Lm+2+padd_X,Jend:Jend+1,0:NWEIGHT) :: Zeta_nort
     &h4
        end subroutine Sub_Loop_zetabc_interp_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-1:Istr,-1: Agrif_tabvars_i(197)%iarray0+2+ Ag
     &rif_tabvars_i(188)%iarray0) :: Zeta_west6
      real, dimension(Iend:Iend+1,-1: Agrif_tabvars_i(197)%iarray0+2+ Ag
     &rif_tabvars_i(188)%iarray0) :: Zeta_east6
      real, dimension(-1: Agrif_tabvars_i(200)%iarray0+2+ Agrif_tabvars_
     &i(189)%iarray0,Jstr-1:Jstr) :: Zeta_south6
      real, dimension(-1: Agrif_tabvars_i(200)%iarray0+2+ Agrif_tabvars_
     &i(189)%iarray0,Jend:Jend+1) :: Zeta_north6
      real, dimension(Istr-1:Istr,-1: Agrif_tabvars_i(197)%iarray0+2+ Ag
     &rif_tabvars_i(188)%iarray0,0:NWEIGHT) :: Zeta_west4
      real, dimension(Iend:Iend+1,-1: Agrif_tabvars_i(197)%iarray0+2+ Ag
     &rif_tabvars_i(188)%iarray0,0:NWEIGHT) :: Zeta_east4
      real, dimension(-1: Agrif_tabvars_i(200)%iarray0+2+ Agrif_tabvars_
     &i(189)%iarray0,Jstr-1:Jstr,0:NWEIGHT) :: Zeta_south4
      real, dimension(-1: Agrif_tabvars_i(200)%iarray0+2+ Agrif_tabvars_
     &i(189)%iarray0,Jend:Jend+1,0:NWEIGHT) :: Zeta_north4
      

        call Sub_Loop_zetabc_interp_tile(Istr,Iend,Jstr,Jend,Zeta_west4,
     &Zeta_east4,Zeta_west6,Zeta_east6,Zeta_south4,Zeta_north4,Zeta_sout
     &h6,Zeta_north6, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)
     &%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarra
     &y0, Agrif_tabvars(142)%array2, Agrif_tabvars_i(168)%iarray0, Agrif
     &_tabvars(20)%array3, Agrif_tabvars(34)%array2, Agrif_tabvars_i(48)
     &%iarray0, Agrif_tabvars_i(22)%iarray0, Agrif_tabvars(28)%array2, A
     &grif_tabvars_i(49)%iarray0, Agrif_tabvars_i(177)%iarray0, Agrif_ta
     &bvars_l(5)%larray0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l(7)
     &%larray0, Agrif_tabvars_l(6)%larray0, Agrif_tabvars_i(194)%iarray0
     &, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_zetabc_interp_tile(Istr,Iend,Jstr,Jend,Zeta_we
     &st4,Zeta_east4,Zeta_west6,Zeta_east6,Zeta_south4,Zeta_north4,Zeta_
     &south6,Zeta_north6,padd_E,Mm,padd_X,Lm,SSH,nfast,Zt_avg3,weight2,Z
     &etaTimeindex2,zetaid,dZtinterp,ZetaTimeindex,iif,NORTH_INTER,SOUTH
     &_INTER,EAST_INTER,WEST_INTER,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nfast
      integer(4) :: ZetaTimeindex2
      integer(4) :: zetaid
      integer(4) :: ZetaTimeindex
      integer(4) :: iif
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: SSH
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: Zt_avg
     &3
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dZtinterp
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-1:Istr,-1:Mm+2+padd_E) :: Zeta_west6
      real, dimension(Iend:Iend+1,-1:Mm+2+padd_E) :: Zeta_east6
      real, dimension(-1:Lm+2+padd_X,Jstr-1:Jstr) :: Zeta_south6
      real, dimension(-1:Lm+2+padd_X,Jend:Jend+1) :: Zeta_north6
      real, dimension(Istr-1:Istr,-1:Mm+2+padd_E,0:NWEIGHT) :: Zeta_west
     &4
      real, dimension(Iend:Iend+1,-1:Mm+2+padd_E,0:NWEIGHT) :: Zeta_east
     &4
      real, dimension(-1:Lm+2+padd_X,Jstr-1:Jstr,0:NWEIGHT) :: Zeta_sout
     &h4
      real, dimension(-1:Lm+2+padd_X,Jend:Jend+1,0:NWEIGHT) :: Zeta_nort
     &h4

                     
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                 
      integer(4) :: i
      integer(4) :: j
      integer(4) :: i1
      integer(4) :: j1
                   
      real :: tinterp
                        
      Integer(4) :: itrcind
                                                         
      INTEGER(4) :: parentknew
      INTEGER(4) :: parentkstp
      INTEGER(4) :: nbstep3dparent
                                           
      real :: t1
      real :: t2
      real :: t3
      real :: t4
      real :: t5
      real :: t6
      real :: t9
      real :: t10
      real :: t11
      real :: t7
                           
      real, dimension(1:2) :: tin
      real, dimension(1:2) :: tout
                
      real :: cff1
                       
      real :: zeta1
      real :: zeta2
      external zetainterp
                                       
      integer(4) :: irhot
      integer(4) :: irhox
      integer(4) :: irhoy
                    
      real :: rrhot
                        
      integer(4) :: iter
                          
      real :: onemtinterp
                                
      integer(4) :: parentnbstep
                          
      integer(4) :: pngrid
                                                               
                                                               
                                                                
                                                                
                                                              
      real, dimension(Istr-1:Istr,Jstr-1:Jend+1) :: Zeta_west7
                                                              
      real, dimension(Iend:Iend+1,Jstr-1:Jend+1) :: Zeta_east7
                                                               
      real, dimension(Istr-1:Iend+1,Jstr-1:Jstr) :: Zeta_south7
                                                               
      real, dimension(Istr-1:Iend+1,Jend:Jend+1) :: Zeta_north7
                                                                       
                                                                 
  
                                                                       
                                                                 
  
                                                                       
                                                                 
                                                                 
 
                                                                       
                                                                 
                                                                 
 
                   
      real :: lastcff
!$AGRIF_DO_NOT_TREAT
      integer*4 :: nbgrid, indinterp
      common/interp2d/indinterp,nbgrid
!$AGRIF_END_DO_NOT_TREAT
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
       irhot = Agrif_Irhot()
       irhox = Agrif_Irhox()
       irhoy = Agrif_Irhoy()
       rrhot = real(irhot)
      parentnbstep=
     &  Agrif_Parent(iif)
      if (ZetaTimeindex .NE. parentnbstep) then
C$OMP BARRIER
C$OMP MASTER
        dZtinterp = 0.D0
            tinterp=1.D0
            Agrif_UseSpecialvalue=.true.
            Agrif_Specialvalue=0.D0
        nbgrid = Agrif_Fixed()
        pngrid = Agrif_Parent_Fixed()
        if (nbgrid == grids_at_level(pngrid,0)) indinterp = 0
            Call Agrif_Bc_variable(zetaid,
     &                                        calledweight=tinterp,
     &                                        procname=zetainterp)
            Agrif_UseSpecialvalue=.false.
C$OMP END MASTER
C$OMP BARRIER
            ZetaTimeindex = parentnbstep
            ZetaTimeindex2 = agrif_nb_step()
          endif
          if (agrif_nb_step() .EQ. ZetaTimeindex2) then
          lastcff=0.D0
          do iter=iif,iif+irhot-1
          lastcff=lastcff+((iter+1-iif)/rrhot)*
     &        weight2(iif+irhot-1,iter)
          enddo
          lastcff=1.D0/lastcff
      if (.not.SOUTH_INTER) then
          do j=Jstr-1,Jstr
          do i=IstrU-1,Iend
          Zeta_south4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          if (iif+irhot-1<=nfast) then
           Zeta_south7(IstrU-1:Iend,Jstr-1:Jstr)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=Jstr-1,Jstr
            do i=IstrU-1,Iend
             Zeta_south7(i,j) = Zeta_south7(i,j)
     &                         +cff1*Zeta_south4(i,j,iter)
            enddo
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=Jstr-1,Jstr
          do i=IstrU-1,Iend
          Zeta_south7(i,j)=Zeta_south7(i,j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*Zeta_south4(i,j,iif-1)
          enddo
          enddo
          enddo
          do j=Jstr-1,Jstr
          do i=IstrU-1,Iend
          Zeta_south7(i,j)=lastcff*(dZtinterp(i,j)-Zeta_south7(i,j))
          enddo
          enddo
          do j=Jstr-1,Jstr
          do i=IstrU-1,Iend
          Zeta_south6(i,j)=(Zeta_south7(i,j)-Zeta_south4(i,j,iif-1))
     &                           /rrhot
          enddo
          enddo
          endif
      endif
      if (.not.NORTH_INTER) then
          do j=Jend,Jend+1
          do i=IstrU-1,Iend
          Zeta_north4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          if (iif+irhot-1<=nfast) then
           Zeta_north7(IstrU-1:Iend,Jend:Jend+1)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=Jend,Jend+1
            do i=IstrU-1,Iend
             Zeta_north7(i,j) = Zeta_north7(i,j)
     &                         +cff1*Zeta_north4(i,j,iter)
            enddo
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=Jend,Jend+1
          do i=IstrU-1,Iend
          Zeta_north7(i,j)=Zeta_north7(i,j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*Zeta_north4(i,j,iif-1)
          enddo
          enddo
          enddo
          do j=Jend,Jend+1
          do i=IstrU-1,Iend
          Zeta_north7(i,j)=lastcff*(dZtinterp(i,j)-Zeta_north7(i,j))
          enddo
          enddo
          do j=Jend,Jend+1
          do i=IstrU-1,Iend
          Zeta_north6(i,j)=(Zeta_north7(i,j)-Zeta_north4(i,j,iif-1))
     &                           /rrhot
          enddo
          enddo
          endif
      endif
      if (.not.WEST_INTER) then
          do j=JstrV-1,Jend
          do i=Istr-1,Istr
          Zeta_west4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          if (iif+irhot-1<=nfast) then
           Zeta_west7(Istr-1:Istr,JstrV-1:Jend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=JstrV-1,Jend
            do i=Istr-1,Istr
             Zeta_west7(i,j) = Zeta_west7(i,j)
     &                         +cff1*Zeta_west4(i,j,iter)
            enddo
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=JstrV-1,Jend
          do i=Istr-1,Istr
          Zeta_west7(i,j)=Zeta_west7(i,j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*Zeta_west4(i,j,iif-1)
          enddo
          enddo
          enddo
          do j=JstrV-1,Jend
          do i=Istr-1,Istr
          Zeta_west7(i,j)=lastcff*(dZtinterp(i,j)-Zeta_west7(i,j))
          enddo
          enddo
          do j=JstrV-1,Jend
          do i=Istr-1,Istr
          Zeta_west6(i,j)=(Zeta_west7(i,j)-Zeta_west4(i,j,iif-1))
     &                           /rrhot
          enddo
          enddo
          endif
      endif
      if (.not.EAST_INTER) then
          do j=JstrV-1,Jend
          do i=Iend,Iend+1
          Zeta_east4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          if (iif+irhot-1<=nfast) then
           Zeta_east7(Iend:Iend+1,JstrV-1:Jend)=0.D0
          do iter=0,iif-1
          cff1=weight2(iif+irhot-1,iter)
            do j=JstrV-1,Jend
            do i=Iend,Iend+1
             Zeta_east7(i,j) = Zeta_east7(i,j)
     &                         +cff1*Zeta_east4(i,j,iter)
            enddo
            enddo
          enddo
          do iter=iif,iif+irhot-2
          do j=JstrV-1,Jend
          do i=Iend,Iend+1
          Zeta_east7(i,j)=Zeta_east7(i,j)+((iif+irhot-1-iter)/rrhot)*
     &        weight2(iif+irhot-1,iter)*Zeta_east4(i,j,iif-1)
          enddo
          enddo
          enddo
          do j=JstrV-1,Jend
          do i=Iend,Iend+1
          Zeta_east7(i,j)=lastcff*(dZtinterp(i,j)-Zeta_east7(i,j))
          enddo
          enddo
          do j=JstrV-1,Jend
          do i=Iend,Iend+1
          Zeta_east6(i,j)=(Zeta_east7(i,j)-Zeta_east4(i,j,iif-1))
     &                           /rrhot
          enddo
          enddo
          endif
      endif
          endif
          if (.not.SOUTH_INTER) then
          do j=Jstr-1,Jstr
          do i=IstrU-1,Iend
          Zeta_south4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          do j=Jstr-1,Jstr
          do i=IstrU-1,Iend
          SSH(i,j)=Zeta_south4(i,j,iif-1)+Zeta_south6(i,j)
           enddo
          enddo
          endif
          if (.not.NORTH_INTER) then
          do j=Jend,Jend+1
          do i=IstrU-1,Iend
          Zeta_north4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          do j=Jend,Jend+1
          do i=IstrU-1,Iend
          SSH(i,j)=Zeta_north4(i,j,iif-1)+Zeta_north6(i,j)
          enddo
          enddo
          endif
          if (.not.WEST_INTER) then
          do j=JstrV-1,Jend
          do i=Istr-1,Istr
          Zeta_west4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          do j=JstrV-1,Jend
          do i=Istr-1,Istr
          SSH(i,j)=Zeta_west4(i,j,iif-1)+Zeta_west6(i,j)
          enddo
          enddo
          endif
          if (.not.EAST_INTER) then
          do j=JstrV-1,Jend
          do i=Iend,Iend+1
          Zeta_east4(i,j,iif-1) = Zt_avg3(i,j,iif-1)
          enddo
          enddo
          do j=JstrV-1,Jend
          do i=Iend,Iend+1
          SSH(i,j)=Zeta_east4(i,j,iif-1)+Zeta_east6(i,j)
          enddo
          enddo
          endif
      return
       
      

      end subroutine Sub_Loop_zetabc_interp_tile

      subroutine zetainterp(tabres,i1,i2,j1,j2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_zetainterp(tabres,i1,i2,j1,j2,before,padd_E,
     &Mm,padd_X,Lm,dZtinterp,weight2,knew,kstp,zeta,nfast,coarsevaluesin
     &terp,iif,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: kstp
      integer(4) :: nfast
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dZtinterp
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, allocatable, dimension(:,:) :: coarsevaluesinterp
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
        end subroutine Sub_Loop_zetainterp

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      

        call Sub_Loop_zetainterp(tabres,i1,i2,j1,j2,before, Agrif_tabvar
     &s_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(18
     &9)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(28)%array2
     &, Agrif_tabvars(34)%array2, Agrif_tabvars_i(179)%iarray0, Agrif_ta
     &bvars_i(181)%iarray0, Agrif_tabvars(98)%array3, Agrif_tabvars_i(16
     &8)%iarray0, Agrif_tabvars(1)%array2, Agrif_tabvars_i(177)%iarray0,
     & Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_zetainterp(tabres,i1,i2,j1,j2,before,padd_E,Mm
     &,padd_X,Lm,dZtinterp,weight2,knew,kstp,zeta,nfast,coarsevaluesinte
     &rp,iif,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: kstp
      integer(4) :: nfast
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: dZtinterp
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, allocatable, dimension(:,:) :: coarsevaluesinterp
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                             
                                
                      
                                
      integer(4) :: i
      integer(4) :: j
      integer(4) :: iter
      integer(4) :: isize
                    
      real :: cff1
!$AGRIF_DO_NOT_TREAT
      integer*4 :: indinterp,nbgrid
      common/interp2d/indinterp,nbgrid
!$AGRIF_END_DO_NOT_TREAT
                                 
      integer(4) :: oldindinterp
       if (before) then
       isize = (j2-j1+1)*(i2-i1+1)
       IF (iif == 1) THEN
       IF (.NOT.allocated(coarsevaluesinterp)) THEN
       Allocate(coarsevaluesinterp(isize,0:nfast))
       ELSE
       CALL checksizeinterp(indinterp+isize)
       ENDIF
       oldindinterp = indinterp
       do j=j1,j2
       do i=i1,i2
       oldindinterp=oldindinterp+1
       coarsevaluesinterp(oldindinterp,0) =
     &  zeta(i,j,kstp)
       enddo
       enddo
       ENDIF
       oldindinterp = indinterp
       do j=j1,j2
       do i=i1,i2
       oldindinterp=oldindinterp+1
       coarsevaluesinterp(oldindinterp,iif) =
     &   zeta(i,j,knew)
       enddo
       enddo
       do iter=0,iif
          cff1 = weight2(iif,iter)
          call copy1d(tabres,coarsevaluesinterp(indinterp+1,iter),
     &                cff1,isize)
       enddo
       indinterp = oldindinterp
        else
         dZtinterp(i1:i2,j1:j2) = tabres
       endif
      return
       
      

      end subroutine Sub_Loop_zetainterp

