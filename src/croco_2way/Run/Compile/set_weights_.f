












      subroutine set_weights
      


      use Agrif_Util
      interface
        subroutine Sub_Loop_set_weights(mynode,weight2,weight,ndtfast,nf
     &ast,Mmmpi,padd_X,Lm,Lmmpi,padd_E,Mm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: mynode
      integer(4) :: ndtfast
      integer(4) :: nfast
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(1:6,0:NWEIGHT) :: weight
        end subroutine Sub_Loop_set_weights

      end interface
      

        call Sub_Loop_set_weights( Agrif_tabvars_i(156)%iarray0, Agrif_t
     &abvars(34)%array2, Agrif_tabvars(87)%array2, Agrif_tabvars_i(183)%
     &iarray0, Agrif_tabvars_i(168)%iarray0, Agrif_tabvars_i(194)%iarray
     &0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agr
     &if_tabvars_i(195)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tab
     &vars_i(197)%iarray0)

      end


      subroutine Sub_Loop_set_weights(mynode,weight2,weight,ndtfast,nfas
     &t,Mmmpi,padd_X,Lm,Lmmpi,padd_E,Mm)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: mynode
      integer(4) :: ndtfast
      integer(4) :: nfast
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(1:6,0:NWEIGHT) :: weight
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                          
      integer(4) :: i
      integer(4) :: j
      integer(4) :: iter
                        
      real :: p
      real :: q
      real :: r
      real :: scale
                            
      real(8) :: sum
      real(8) :: shft
      real(8) :: cff
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                               
      real :: t1
      real :: t2
      real :: t3
      real :: t4
      real :: t11
      real :: t12
                       
      real :: cff10
      real :: cff1m
      nfast=0
      do i=0,2*ndtfast
        weight(1,i)=0.D0
        weight(2,i)=0.D0
      enddo
      p=2.0D0
      q=4.0D0
      r=0.25D0
      scale=(p+1.D0)*(p+q+1.D0)/((p+2.D0)*(p+q+2.D0)*float(ndtfast))
      do iter=1,16
        nfast=0
        do i=1,2*ndtfast
          cff=scale*float(i)
          weight(1,i)=cff**p - cff**(p+q) - r*cff
          if (i.gt.ndtfast .and. weight(1,i).lt.0.D0) then
            weight(1,i)=0.D0
          else
            nfast=i
          endif
        enddo
        sum=0.D0
        shft=0.D0
        do i=1,nfast
          sum=sum+weight(1,i)
          shft=shft+weight(1,i)*float(i)
        enddo
        scale=scale * shft/(sum*float(ndtfast))
      enddo
      do iter=1,ndtfast
        sum=0.D0
        shft=0.D0
        do i=1,nfast
          sum=sum+weight(1,i)
          shft=shft+float(i)*weight(1,i)
        enddo
        shft=shft/sum
        cff=float(ndtfast)-shft
        if (cff .gt. 1.D0) then
          nfast=nfast+1
          do i=nfast,2,-1
            weight(1,i)=weight(1,i-1)
          enddo
          weight(1,1)=0.D0
        elseif (cff .gt. 0.D0) then
          sum=1.D0-cff
          do i=nfast,2,-1
            weight(1,i)=sum*weight(1,i)+cff*weight(1,i-1)
          enddo
          weight(1,1)=sum*weight(1,1)
        elseif (cff .lt. -1.D0) then
          nfast=nfast-1
          do i=1,nfast,+1
            weight(1,i)=weight(1,i+1)
          enddo
          weight(1,nfast+1)=0.D0
        elseif (cff .lt. 0.D0) then
          sum=1.D0+cff
          do i=1,nfast-1,+1
            weight(1,i)=sum*weight(1,i)-cff*weight(1,i+1)
          enddo
          weight(1,nfast)=sum*weight(1,nfast)
        endif
      enddo
      do j=1,nfast
        cff=weight(1,j)
        do i=1,j
          weight(2,i)=weight(2,i)+cff
        enddo
      enddo
      sum=0.D0
      cff=0.D0
      do i=1,nfast
        sum=sum+weight(1,i)
        cff=cff+weight(2,i)
      enddo
      sum=1.D0/sum
      cff=1.D0/cff
      do i=1,nfast
        weight(1,i)=sum*weight(1,i)
        weight(2,i)=cff*weight(2,i)
      enddo
      weight2 = 1.D0
      weight2(nfast,:) = weight(1,:)
      do j=nfast-1,1,-1
      cff10 = weight2(j+1,0)
      cff1m = weight2(j+1,j+1)
      t1 = (1+cff10*(-1.D0+(j+1)))*ndtfast
     &        +(-1+cff10)*nfast
      t2 = (cff10-cff1m)*(j+1)*ndtfast-
     & (-1.D0+cff10)*(-1.D0+cff1m+cff1m*(j+1))*nfast
      if (t2==0.D0) then
      t3=0.D0
      t4=1.D0/(1.D0-cff10)
      else
      t3 = t1/t2
      t4 = (1.D0-t3*(1-cff1m))/(1.D0-cff10)
      endif
      do i=0,j
        weight2(j,i) = t3*weight2(j+1,i)+t4*weight2(j+1,i+1)
      enddo
      weight2(j,j+1:) = 0.D0
      enddo
      if (mynode.eq.0) write(*,'(/1x,A,I3,4x,A,I3/)')
     &   'Time splitting: ndtfast =', ndtfast, 'nfast =', nfast
      return
       
      

      end subroutine Sub_Loop_set_weights

