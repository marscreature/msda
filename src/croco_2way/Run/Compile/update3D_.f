












      subroutine ResetAlready()      


      use Agrif_Util
      interface
        subroutine Sub_Loop_ResetAlready(padd_E,Mm,padd_X,Lm,Alreadyupda
     &ted)
          implicit none
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      logical, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:3) :: Alreadyup
     &dated
        end subroutine Sub_Loop_ResetAlready

      end interface
      

        call Sub_Loop_ResetAlready( Agrif_tabvars_i(188)%iarray0, Agrif_
     &tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvar
     &s_i(200)%iarray0, Agrif_tabvars_l(1)%larray3)

      end


      subroutine Sub_Loop_ResetAlready(padd_E,Mm,padd_X,Lm,Alreadyupdate
     &d)

      use Agrif_Types, only : Agrif_tabvars


      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      logical, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:3) :: Alreadyup
     &dated

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
      Alreadyupdated = .FALSE.
             

      end subroutine Sub_Loop_ResetAlready


      subroutine Agrif_update_np1
      

      use Agrif_Util
      interface
        subroutine Sub_Loop_Agrif_update_np1(updatetid,Mmmpi,Lmmpi,padd_
     &E,Mm,padd_X,Lm)
          use Agrif_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      integer(4) :: updatetid
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
        end subroutine Sub_Loop_Agrif_update_np1

      end interface
      

        call Sub_Loop_Agrif_update_np1( Agrif_tabvars_i(7)%iarray0, Agri
     &f_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabv
     &ars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(
     &189)%iarray0, Agrif_tabvars_i(200)%iarray0)

      end


      subroutine Sub_Loop_Agrif_update_np1(updatetid,Mmmpi,Lmmpi,padd_E,
     &Mm,padd_X,Lm)

      use Agrif_Types, only : Agrif_tabvars

      use Agrif_Util
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      integer(4) :: updatetid
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
                     
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                            
      integer(4) :: NNO
      integer(4) :: NNOX
      integer(4) :: NNOY
      integer(4) :: ich
      integer(4) :: jch
      integer(4) :: k
      integer(4) :: itrc
      integer(4) :: ipr
      integer(4) :: jpr
      integer(4) :: nnewpr
                                                                
      integer(4) :: irmin
      integer(4) :: irmax
      integer(4) :: jrmin
      integer(4) :: jrmax
      integer(4) :: iumin
      integer(4) :: iumax
      integer(4) :: jvmin
      integer(4) :: jvmax
                                
      integer(4) :: iint
      integer(4) :: jint
                             
      real :: cff
      real :: fxc
      real :: surfc
                            

                                                       
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tabtemp
      External UpdateTranp1, Updateunp1, Updatevnp1
                                                                       
                                                                 
 
      real, pointer, dimension(:,:,:,:) :: myfxparent
      real, pointer, dimension(:,:,:,:) :: myfyparent
      real, pointer, dimension(:,:,:,:) :: uparent
                                                                       
                                                                 
                                                 
      real, allocatable, dimension(:,:,:,:) :: myfxoldparent
      real, allocatable, dimension(:,:,:,:) :: myfyoldparent
                                                  
      real, pointer, dimension(:,:,:,:) :: vparent
                                                                   
      real, pointer, dimension(:,:) :: pmparent
      real, pointer, dimension(:,:) :: pnparent
      real, pointer, dimension(:,:) :: rmaskparent
                                                
      real, pointer, dimension(:,:,:) :: Hzparent
                                                  
      real, pointer, dimension(:,:,:,:,:) :: tparent
                                
      integer(4) :: ipu
      integer(4) :: jpu
      integer(4) :: ipv
      integer(4) :: jpv
                                                  
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: HTEMP
                    
      integer(4) :: j
      integer(4) :: i
                                  
      real :: t1
      real :: t2
      real :: t3
      real :: t4
      real :: t5
      real :: t6
      real :: t7
                    
      real :: dtparent
                                      
      integer(4) :: nnewparent
      integer(4) :: parentnnew
                   
      real, dimension(1:5) :: tind
                   
      integer(4) :: j1
                      
      integer(4) :: irhot
                       
      integer(4) :: idecal
      external :: updatemyfx, updatemyfy
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      irhot=Agrif_Irhot()
      IF (.TRUE.) THEN
      Call Agrif_Update_Variable(updatetid,
     &         procname = updateTranp1,locupdate=(/1,0/))
      Agrif_UseSpecialValueInUpdate = .FALSE.
      ELSE
      Call Agrif_Update_Variable(updatetid,
     &         locupdate=(/1,3/),procname = updateTranp1)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      ENDIF
       RETURN
      return
       
      

      end subroutine Sub_Loop_Agrif_update_np1

      subroutine ExchangeParentValuesTracers()      


      use Agrif_Util
      interface
        subroutine Sub_Loop_ExchangeParentValuesTracers(padd_E,Mm,padd_X
     &,Lm,nnew,t)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
        end subroutine Sub_Loop_ExchangeParentValuesTracers

      end interface
      

        call Sub_Loop_ExchangeParentValuesTracers( Agrif_tabvars_i(188)%
     &iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%iarray
     &0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(174)%iarray0, Agr
     &if_tabvars(109)%array5)

      end


      subroutine Sub_Loop_ExchangeParentValuesTracers(padd_E,Mm,padd_X,L
     &m,nnew,t)

      use Agrif_Types, only : Agrif_tabvars


      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                     
      integer(4) :: itrc
      do itrc = 1,NT
        call exchange_r3d_tile (1,Lm,1,Mm,
     &                          t(-1,-1,1,nnew,itrc))
      enddo
             

      end subroutine Sub_Loop_ExchangeParentValuesTracers


      subroutine Agrif_update_uv_np1
      

      use Agrif_Util
      interface
        subroutine Sub_Loop_Agrif_update_uv_np1(updatevid,updateuid,nbco
     &arse,Mmmpi,Lmmpi,padd_E,Mm,padd_X,Lm)
          use Agrif_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      integer(4) :: updatevid
      integer(4) :: updateuid
      integer(4) :: nbcoarse
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
        end subroutine Sub_Loop_Agrif_update_uv_np1

      end interface
      

        call Sub_Loop_Agrif_update_uv_np1( Agrif_tabvars_i(5)%iarray0, A
     &grif_tabvars_i(6)%iarray0, Agrif_tabvars_i(39)%iarray0, Agrif_tabv
     &ars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars_i(
     &188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%i
     &array0, Agrif_tabvars_i(200)%iarray0)

      end


      subroutine Sub_Loop_Agrif_update_uv_np1(updatevid,updateuid,nbcoar
     &se,Mmmpi,Lmmpi,padd_E,Mm,padd_X,Lm)

      use Agrif_Types, only : Agrif_tabvars

      use Agrif_Util
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      integer(4) :: updatevid
      integer(4) :: updateuid
      integer(4) :: nbcoarse
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
                     
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                            
      integer(4) :: NNO
      integer(4) :: NNOX
      integer(4) :: NNOY
      integer(4) :: ich
      integer(4) :: jch
      integer(4) :: k
      integer(4) :: itrc
      integer(4) :: ipr
      integer(4) :: jpr
      integer(4) :: nnewpr
                                                                
      integer(4) :: irmin
      integer(4) :: irmax
      integer(4) :: jrmin
      integer(4) :: jrmax
      integer(4) :: iumin
      integer(4) :: iumax
      integer(4) :: jvmin
      integer(4) :: jvmax
                                
      integer(4) :: iint
      integer(4) :: jint
                             
      real :: cff
      real :: fxc
      real :: surfc
                            

                                                       
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tabtemp
      External UpdateTranp1, Updateunp1, Updatevnp1
                                                                       
                                                                 
 
      real, pointer, dimension(:,:,:,:) :: myfxparent
      real, pointer, dimension(:,:,:,:) :: myfyparent
      real, pointer, dimension(:,:,:,:) :: uparent
                                                                       
                                                                 
                                                 
      real, allocatable, dimension(:,:,:,:) :: myfxoldparent
      real, allocatable, dimension(:,:,:,:) :: myfyoldparent
                                                  
      real, pointer, dimension(:,:,:,:) :: vparent
                                                                   
      real, pointer, dimension(:,:) :: pmparent
      real, pointer, dimension(:,:) :: pnparent
      real, pointer, dimension(:,:) :: rmaskparent
                                                
      real, pointer, dimension(:,:,:) :: Hzparent
                                                  
      real, pointer, dimension(:,:,:,:,:) :: tparent
                                
      integer(4) :: ipu
      integer(4) :: jpu
      integer(4) :: ipv
      integer(4) :: jpv
                                                  
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: HTEMP
                    
      integer(4) :: j
      integer(4) :: i
                                  
      real :: t1
      real :: t2
      real :: t3
      real :: t4
      real :: t5
      real :: t6
      real :: t7
                    
      real :: dtparent
                                      
      integer(4) :: nnewparent
      integer(4) :: parentnnew
                   
      real, dimension(1:5) :: tind
                   
      integer(4) :: j1
                      
      integer(4) :: irhot
                       
      integer(4) :: idecal
      external :: updatemyfx, updatemyfy
      external :: updateunp1pre
      external :: updatevnp1pre
!$AGRIF_DO_NOT_TREAT
      integer*4 :: nbcoarsechild
      common/updateprestepint/nbcoarsechild
!$AGRIF_END_DO_NOT_TREAT
      nbcoarsechild = nbcoarse
      Call Agrif_ChildGrid_To_ParentGrid()
      Call ResetAlready()
      Call Agrif_ParentGrid_To_ChildGrid()
      Agrif_UseSpecialValueInUpdate = .FALSE.
      Agrif_SpecialValueFineGrid = 0.D0
      Agrif_UseSpecialValueInUpdate = .FALSE.
      IF (nbcoarse .NE. Agrif_Irhot()) return
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      irhot=Agrif_Irhot()
      IF (.TRUE.) THEN
      Agrif_UseSpecialValueInUpdate = .FALSE.
      Call Agrif_Update_Variable(updateuid,
     &     locupdate1=(/0,-1/),locupdate2=(/1,-2/),
     &     procname = updateunp1)
      Call Agrif_Update_Variable(updatevid,
     &     locupdate1=(/1,-2/),locupdate2=(/0,-1/),
     &     procname = updatevnp1)
      ELSE
      Agrif_UseSpecialValueInUpdate = .FALSE.
      Call Agrif_Update_Variable(updateuid,
     &     locupdate1=(/0,2/),locupdate2=(/1,3/),
     &     procname = updateunp1)
      Call Agrif_Update_Variable(updatevid,
     &     locupdate1=(/1,3/),locupdate2=(/0,2/),
     &     procname = updatevnp1)
      ENDIF
      return
       
      

      end subroutine Sub_Loop_Agrif_update_uv_np1

      Subroutine UpdateTranp1pre(tabres,i1,i2,j1,j2,k1,k2,l1,l2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_UpdateTranp1pre(tabres,i1,i2,j1,j2,k1,k2,l1,
     &l2,before,padd_E,Mm,padd_X,Lm,Alreadyupdated,rmask,nrhs,t,Mmmpi,Lm
     &mpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nrhs
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      logical, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:3) :: Alreadyup
     &dated
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: l1
      integer(4) :: l2
      real, dimension(i1:i2,j1:j2,k1:k2,l1:l2) :: tabres
      logical :: before
        end subroutine Sub_Loop_UpdateTranp1pre

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: l1
      integer(4) :: l2
      real, dimension(i1:i2,j1:j2,k1:k2,l1:l2) :: tabres
      logical :: before
      

        call Sub_Loop_UpdateTranp1pre(tabres,i1,i2,j1,j2,k1,k2,l1,l2,bef
     &ore, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, A
     &grif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_t
     &abvars_l(1)%larray3, Agrif_tabvars(51)%array2, Agrif_tabvars_i(175
     &)%iarray0, Agrif_tabvars(109)%array5, Agrif_tabvars_i(194)%iarray0
     &, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_UpdateTranp1pre(tabres,i1,i2,j1,j2,k1,k2,l1,l2
     &,before,padd_E,Mm,padd_X,Lm,Alreadyupdated,rmask,nrhs,t,Mmmpi,Lmmp
     &i)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nrhs
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      logical, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:3) :: Alreadyup
     &dated
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: l1
      integer(4) :: l2
      real, dimension(i1:i2,j1:j2,k1:k2,l1:l2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
!$AGRIF_DO_NOT_TREAT
      integer*4 :: nbcoarsechild
      common/updateprestepint/nbcoarsechild
!$AGRIF_END_DO_NOT_TREAT
                                        
                                           
                     
                        
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: l
                       
      real :: invrrhot
      IF (before) THEN
        invrrhot = 1.D0/real(Agrif_Irhot())
        DO l=l1,l2
        DO k=k1,k2
        DO j=j1,j2
        DO i=i1,i2
          tabres(i,j,k,l) = invrrhot * t(i,j,k,nrhs,l)
     &                       *rmask(i,j)
        ENDDO
        ENDDO
        ENDDO
        ENDDO
       ELSE
         IF (nbcoarsechild == 1) THEN
           DO l=l1,l2
           DO k=k1,k2
           DO j=j1,j2
           DO i=i1,i2
             t(i,j,k,nrhs,l) = tabres(i,j,k,l)
     &                         * rmask(i,j)
           ENDDO
           ENDDO
           ENDDO
           ENDDO
         ELSE
           DO j=j1,j2
           DO i=i1,i2
             IF (.Not.Alreadyupdated(i,j,1)) THEN
               DO l=l1,l2
               DO k=k1,k2
                 t(i,j,k,nrhs,l) = (t(i,j,k,nrhs,l)+tabres(i,j,k,l))
     &                              * rmask(i,j)
               ENDDO
               ENDDO
               Alreadyupdated(i,j,1) = .TRUE.
             ENDIF
           ENDDO
           ENDDO
          ENDIF
        ENDIF
      return
       
      

      end subroutine Sub_Loop_UpdateTranp1pre

      Subroutine Updateunp1pre(tabres,i1,i2,j1,j2,k1,k2,before,nb,ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updateunp1pre(tabres,i1,i2,j1,j2,k1,k2,befor
     &e,nb,ndir,padd_E,Mm,padd_X,Lm,Alreadyupdated,Huonagrif,umask,Huon,
     &Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      logical, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:3) :: Alreadyup
     &dated
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Huonagrif
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Huon
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: nb
      integer(4) :: ndir
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
        end subroutine Sub_Loop_Updateunp1pre

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: nb
      integer(4) :: ndir
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      

        call Sub_Loop_Updateunp1pre(tabres,i1,i2,j1,j2,k1,k2,before,nb,n
     &dir, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, A
     &grif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_t
     &abvars_l(1)%larray3, Agrif_tabvars(22)%array3, Agrif_tabvars(49)%a
     &rray2, Agrif_tabvars(106)%array3, Agrif_tabvars_i(194)%iarray0, Ag
     &rif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_Updateunp1pre(tabres,i1,i2,j1,j2,k1,k2,before,
     &nb,ndir,padd_E,Mm,padd_X,Lm,Alreadyupdated,Huonagrif,umask,Huon,Mm
     &mpi,Lmmpi)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      logical, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:3) :: Alreadyup
     &dated
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Huonagrif
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Huon
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: nb
      integer(4) :: ndir
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                          
                                     
                     
!$AGRIF_DO_NOT_TREAT
      integer*4 :: nbcoarsechild
      common/updateprestepint/nbcoarsechild
!$AGRIF_END_DO_NOT_TREAT
                      
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                       
      real :: invrrhot
                    
      real :: t1
      real :: t2
      real :: t3
      IF (before) THEN
        invrrhot = 1.D0/real(Agrif_Irhot())
        DO k=k1,k2
        DO j=j1,j2
        DO i=i1,i2
          tabres(i,j,k) = invrrhot * Huon(i,j,k)
     &                       *umask(i,j)
        ENDDO
        ENDDO
        ENDDO
       ELSE
         IF (nbcoarsechild == 1) THEN
           DO k=k1,k2
           DO j=j1,j2
           DO i=i1,i2
             Huonagrif(i,j,k) = 3.D0*tabres(i,j,k)
     &                         * umask(i,j)
           ENDDO
           ENDDO
           ENDDO
         ELSE
           DO j=j1,j2
           DO i=i1,i2
             IF (.Not.Alreadyupdated(i,j,2)) THEN
               DO k=k1,k2
         Huonagrif(i,j,k) = (Huonagrif(i,j,k)+3.D0*tabres(i,j,k))
     &                              * umask(i,j)
          Huon(i,j,k) = Huonagrif(i,j,k)
               ENDDO
               Alreadyupdated(i,j,2) = .TRUE.
             ENDIF
           ENDDO
           ENDDO
          ENDIF
        ENDIF
      return
       
      

      end subroutine Sub_Loop_Updateunp1pre

      Subroutine Updatevnp1pre(tabres,i1,i2,j1,j2,k1,k2,before,nb,ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updatevnp1pre(tabres,i1,i2,j1,j2,k1,k2,befor
     &e,nb,ndir,padd_E,Mm,padd_X,Lm,Alreadyupdated,Hvomagrif,vmask,Hvom,
     &Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      logical, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:3) :: Alreadyup
     &dated
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hvomagrif
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hvom
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: nb
      integer(4) :: ndir
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
        end subroutine Sub_Loop_Updatevnp1pre

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: nb
      integer(4) :: ndir
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      

        call Sub_Loop_Updatevnp1pre(tabres,i1,i2,j1,j2,k1,k2,before,nb,n
     &dir, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, A
     &grif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_t
     &abvars_l(1)%larray3, Agrif_tabvars(21)%array3, Agrif_tabvars(48)%a
     &rray2, Agrif_tabvars(105)%array3, Agrif_tabvars_i(194)%iarray0, Ag
     &rif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_Updatevnp1pre(tabres,i1,i2,j1,j2,k1,k2,before,
     &nb,ndir,padd_E,Mm,padd_X,Lm,Alreadyupdated,Hvomagrif,vmask,Hvom,Mm
     &mpi,Lmmpi)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      logical, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:3) :: Alreadyup
     &dated
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hvomagrif
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hvom
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: nb
      integer(4) :: ndir
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                          
                                     
                     
!$AGRIF_DO_NOT_TREAT
      integer*4 :: nbcoarsechild
      common/updateprestepint/nbcoarsechild
!$AGRIF_END_DO_NOT_TREAT
                      
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                       
      real :: invrrhot
      IF (before) THEN
        invrrhot = 1.D0/real(Agrif_Irhot())
        DO k=k1,k2
        DO j=j1,j2
        DO i=i1,i2
          tabres(i,j,k) = invrrhot * Hvom(i,j,k)
     &                       *vmask(i,j)
        ENDDO
        ENDDO
        ENDDO
       ELSE
         IF (nbcoarsechild == 1) THEN
           DO k=k1,k2
           DO j=j1,j2
           DO i=i1,i2
             Hvomagrif(i,j,k) = 3.D0*tabres(i,j,k)
     &                         * vmask(i,j)
           ENDDO
           ENDDO
           ENDDO
         ELSE
           DO j=j1,j2
           DO i=i1,i2
             IF (.Not.Alreadyupdated(i,j,3)) THEN
               DO k=k1,k2
           Hvomagrif(i,j,k)= (Hvomagrif(i,j,k)+3.D0*tabres(i,j,k))
     &                              * vmask(i,j)
              Hvom(i,j,k) = Hvomagrif(i,j,k)
               ENDDO
               Alreadyupdated(i,j,3) = .TRUE.
             ENDIF
           ENDDO
           ENDDO
          ENDIF
        ENDIF
      return
       
      

      end subroutine Sub_Loop_Updatevnp1pre

      Subroutine UpdateTranp1(tabres,i1,i2,j1,j2,k1,k2,l1,l2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_UpdateTranp1(tabres,i1,i2,j1,j2,k1,k2,l1,l2,
     &before,padd_E,Mm,padd_X,Lm,rmask,nnew,t)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: l1
      integer(4) :: l2
      real, dimension(i1:i2,j1:j2,k1:k2,l1:l2) :: tabres
      logical :: before
        end subroutine Sub_Loop_UpdateTranp1

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: l1
      integer(4) :: l2
      real, dimension(i1:i2,j1:j2,k1:k2,l1:l2) :: tabres
      logical :: before
      

        call Sub_Loop_UpdateTranp1(tabres,i1,i2,j1,j2,k1,k2,l1,l2,before
     &, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agri
     &f_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabv
     &ars(51)%array2, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars(109)%a
     &rray5)

      end


      subroutine Sub_Loop_UpdateTranp1(tabres,i1,i2,j1,j2,k1,k2,l1,l2,be
     &fore,padd_E,Mm,padd_X,Lm,rmask,nnew,t)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: l1
      integer(4) :: l2
      real, dimension(i1:i2,j1:j2,k1:k2,l1:l2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                        
                                           
                     
                        
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: l
      IF (before) THEN
      DO l=l1,l2
        DO k=k1,k2
         DO j=j1,j2
           DO i=i1,i2
	    tabres(i,j,k,l) = t(i,j,k,nnew,l)
            ENDDO
         ENDDO
        ENDDO
       ENDDO
       ELSE
      DO l=l1,l2
        DO k=k1,k2
         DO j=j1,j2
          DO i=i1,i2
	      t(i,j,k,nnew,l) = tabres(i,j,k,l)
     &                         *rmask(i,j)
          ENDDO
         ENDDO
       ENDDO
        ENDDO
       ENDIF
      return
       
      

      end subroutine Sub_Loop_UpdateTranp1

      Subroutine Updatemyfx(tabres,i1,i2,j1,j2,k1,k2,l1,l2,before,
     &   nb,ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updatemyfx(tabres,i1,i2,j1,j2,k1,k2,l1,l2,be
     &fore,nb,ndir,padd_E,Mm,padd_X,Lm,Hz,pn,pm,nnew,t,myfx,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: myfx
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: l1
      integer(4) :: l2
      real, dimension(i1:i2,j1:j2,k1:k2,l1:l2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
        end subroutine Sub_Loop_Updatemyfx

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: l1
      integer(4) :: l2
      real, dimension(i1:i2,j1:j2,k1:k2,l1:l2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
      

        call Sub_Loop_Updatemyfx(tabres,i1,i2,j1,j2,k1,k2,l1,l2,before,n
     &b,ndir, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0
     &, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agri
     &f_tabvars(104)%array3, Agrif_tabvars(73)%array2, Agrif_tabvars(74)
     &%array2, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars(109)%array5, 
     &Agrif_tabvars(31)%array4, Agrif_tabvars_i(194)%iarray0, Agrif_tabv
     &ars_i(195)%iarray0)

      end


      subroutine Sub_Loop_Updatemyfx(tabres,i1,i2,j1,j2,k1,k2,l1,l2,befo
     &re,nb,ndir,padd_E,Mm,padd_X,Lm,Hz,pn,pm,nnew,t,myfx,Mmmpi,Lmmpi)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: myfx
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: l1
      integer(4) :: l2
      real, dimension(i1:i2,j1:j2,k1:k2,l1:l2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                        
                                           
                     
                        
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: l
                        
                        
      real :: t1
      real :: rrhoy
                                            
      logical :: western_side
      logical :: eastern_side
      IF (before) THEN
         rrhoy = real(Agrif_Irhoy())
      DO l=l1,l2
        DO k=k1,k2
         DO j=j1,j2
           DO i=i1,i2
	    tabres(i,j,k,l) = rrhoy*myfx(i,j,k,l)
            ENDDO
         ENDDO
        ENDDO
       ENDDO
       ELSE
         western_side  = (nb == 1).AND.(ndir == 1)
         eastern_side  = (nb == 1).AND.(ndir == 2)
      if (western_side) then
      i=i1
      DO l=l1,l2
        DO k=k1,k2
         DO j=j1,j2
         t(i-1,j,k,nnew,l)=t(i-1,j,k,nnew,l)-
     &  pm(i-1,j)*pn(i-1,j)*(tabres(i,j,k,l)-myfx(i,j,k,l))
     &  /Hz(i-1,j,k)
         ENDDO
       ENDDO
       ENDDO
       endif
      if (eastern_side) then
      i=i2
      DO l=l1,l2
        DO k=k1,k2
         DO j=j1,j2
         t(i,j,k,nnew,l)=t(i,j,k,nnew,l)+
     &  pm(i,j)*pn(i,j)*(tabres(i,j,k,l)-myfx(i,j,k,l))
     &  /Hz(i,j,k)
         ENDDO
       ENDDO
       ENDDO
       endif
       ENDIF
      return
       
      

      end subroutine Sub_Loop_Updatemyfx

      Subroutine Updatemyfy(tabres,i1,i2,j1,j2,k1,k2,l1,l2,before,
     &   nb,ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updatemyfy(tabres,i1,i2,j1,j2,k1,k2,l1,l2,be
     &fore,nb,ndir,padd_E,Mm,padd_X,Lm,Hz,pn,pm,nnew,t,myfy,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: myfy
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: l1
      integer(4) :: l2
      real, dimension(i1:i2,j1:j2,k1:k2,l1:l2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
        end subroutine Sub_Loop_Updatemyfy

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: l1
      integer(4) :: l2
      real, dimension(i1:i2,j1:j2,k1:k2,l1:l2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
      

        call Sub_Loop_Updatemyfy(tabres,i1,i2,j1,j2,k1,k2,l1,l2,before,n
     &b,ndir, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0
     &, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agri
     &f_tabvars(104)%array3, Agrif_tabvars(73)%array2, Agrif_tabvars(74)
     &%array2, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars(109)%array5, 
     &Agrif_tabvars(30)%array4, Agrif_tabvars_i(194)%iarray0, Agrif_tabv
     &ars_i(195)%iarray0)

      end


      subroutine Sub_Loop_Updatemyfy(tabres,i1,i2,j1,j2,k1,k2,l1,l2,befo
     &re,nb,ndir,padd_E,Mm,padd_X,Lm,Hz,pn,pm,nnew,t,myfy,Mmmpi,Lmmpi)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: myfy
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: l1
      integer(4) :: l2
      real, dimension(i1:i2,j1:j2,k1:k2,l1:l2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                        
                                           
                     
                        
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: l
                        
                        
      real :: t1
      real :: rrhox
                                             
      logical :: northern_side
      logical :: southern_side
      IF (before) THEN
         rrhox = real(Agrif_Irhox())
      DO l=l1,l2
        DO k=k1,k2
         DO j=j1,j2
           DO i=i1,i2
	    tabres(i,j,k,l) = rrhox * myfy(i,j,k,l)
            ENDDO
         ENDDO
        ENDDO
       ENDDO
       ELSE
         southern_side = (nb == 2).AND.(ndir == 1)
         northern_side = (nb == 2).AND.(ndir == 2)
      if (southern_side) then
      j=j1
      DO l=l1,l2
        DO k=k1,k2
         DO i=i1,i2
         t(i,j-1,k,nnew,l)=t(i,j-1,k,nnew,l)-
     &  pm(i,j-1)*pn(i,j-1)*(tabres(i,j,k,l)-myfy(i,j,k,l))
     &  /Hz(i,j-1,k)
         ENDDO
       ENDDO
       ENDDO
       endif
      if (northern_side) then
      j=j2
      DO l=l1,l2
        DO k=k1,k2
         DO i=i1,i2
         t(i,j,k,nnew,l)=t(i,j,k,nnew,l)+
     &  pm(i,j)*pn(i,j)*(tabres(i,j,k,l)-myfy(i,j,k,l))
     &  /Hz(i,j,k)
         ENDDO
       ENDDO
       ENDDO
       endif
       ENDIF
      return
       
      

      end subroutine Sub_Loop_Updatemyfy

      Subroutine Updateunp1(tabres,i1,i2,j1,j2,k1,k2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updateunp1(tabres,i1,i2,j1,j2,k1,k2,before,p
     &add_E,Mm,padd_X,Lm,umask,nnew,u)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
        end subroutine Sub_Loop_Updateunp1

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      

        call Sub_Loop_Updateunp1(tabres,i1,i2,j1,j2,k1,k2,before, Agrif_
     &tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvar
     &s_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(49)%
     &array2, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars(111)%array4)

      end


      subroutine Sub_Loop_Updateunp1(tabres,i1,i2,j1,j2,k1,k2,before,pad
     &d_E,Mm,padd_X,Lm,umask,nnew,u)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                  
                                     
                     
                  
      real :: hzu
                      
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      IF (before) THEN
        DO k=k1,k2
         DO j=j1,j2
           DO i=i1,i2
            tabres(i,j,k) = u(i,j,k,nnew)
            ENDDO
         ENDDO
        ENDDO
       ELSE
        DO k=k1,k2
         DO j=j1,j2
           DO i=i1,i2
          u(i,j,k,nnew) = tabres(i,j,k)
     &           * umask(i,j)
          ENDDO
         ENDDO
       ENDDO
       ENDIF
      return
       
      

      end subroutine Sub_Loop_Updateunp1

      Subroutine Updatevnp1(tabres,i1,i2,j1,j2,k1,k2,before)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updatevnp1(tabres,i1,i2,j1,j2,k1,k2,before,p
     &add_E,Mm,padd_X,Lm,vmask,nnew,v)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
        end subroutine Sub_Loop_Updatevnp1

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      

        call Sub_Loop_Updatevnp1(tabres,i1,i2,j1,j2,k1,k2,before, Agrif_
     &tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvar
     &s_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(48)%
     &array2, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars(110)%array4)

      end


      subroutine Sub_Loop_Updatevnp1(tabres,i1,i2,j1,j2,k1,k2,before,pad
     &d_E,Mm,padd_X,Lm,vmask,nnew,v)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                  
                                     
                     
               
      real :: hzv
                      
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      IF (before) THEN
        DO k=k1,k2
         DO j=j1,j2
           DO i=i1,i2
             tabres(i,j,k) = v(i,j,k,nnew)
            ENDDO
         ENDDO
        ENDDO
       ELSE
        DO k=k1,k2
         DO j=j1,j2
          DO i=i1,i2
          v(i,j,k,nnew) = tabres(i,j,k)
     &              * vmask(i,j)
          ENDDO
         ENDDO
       ENDDO
       ENDIF
      return
       
      

      end subroutine Sub_Loop_Updatevnp1

