












      subroutine v2dbc_tile(Istr,Iend,Jstr,Jend,grad)
      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util

      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad

                     
                                    
                                             
      if (AGRIF_Root()) then
        call v2dbc_parent_tile(Istr,Iend,Jstr,Jend,grad)
      else
        call v2dbc_child_tile(Istr,Iend,Jstr,Jend,grad)
      endif
      return
      end
      subroutine v2dbc_parent_tile(Istr,Iend,Jstr,Jend,grad)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_v2dbc_parent_tile(Istr,Iend,Jstr,Jend,grad,p
     &add_E,Mm,padd_X,Lm,pm,vmask,vbclm,ssh,knew,vbar,kstp,zeta,h,tauM_o
     &ut,tauM_in,dtfast,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,Mm
     &mpi,Lmmpi,N3d,N2d)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: eps = 1.D-20
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: kstp
      real :: tauM_out
      real :: tauM_in
      real :: dtfast
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      integer(4) :: N2d
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ssh
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad
        end subroutine Sub_Loop_v2dbc_parent_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad
      

        call Sub_Loop_v2dbc_parent_tile(Istr,Iend,Jstr,Jend,grad, Agrif_
     &tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvar
     &s_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(74)%
     &array2, Agrif_tabvars(48)%array2, Agrif_tabvars(132)%array2, Agrif
     &_tabvars(142)%array2, Agrif_tabvars_i(179)%iarray0, Agrif_tabvars(
     &96)%array3, Agrif_tabvars_i(181)%iarray0, Agrif_tabvars(98)%array3
     &, Agrif_tabvars(85)%array2, Agrif_tabvars_r(16)%array0, Agrif_tabv
     &ars_r(17)%array0, Agrif_tabvars_r(45)%array0, Agrif_tabvars_l(5)%l
     &array0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Ag
     &rif_tabvars_l(6)%larray0, Agrif_tabvars_i(194)%iarray0, Agrif_tabv
     &ars_i(195)%iarray0, Agrif_tabvars_i(186)%iarray0, Agrif_tabvars_i(
     &187)%iarray0)

      end


      subroutine Sub_Loop_v2dbc_parent_tile(Istr,Iend,Jstr,Jend,grad,pad
     &d_E,Mm,padd_X,Lm,pm,vmask,vbclm,ssh,knew,vbar,kstp,zeta,h,tauM_out
     &,tauM_in,dtfast,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,Mmmp
     &i,Lmmpi,N3d,N2d)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: eps = 1.D-20
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: kstp
      real :: tauM_out
      real :: tauM_in
      real :: dtfast
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      integer(4) :: N2d
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ssh
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                         
      integer(4) :: i
      integer(4) :: j
                                                
                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                    
      integer(4) :: trd
C$    integer*4 omp_get_thread_num
                                                                       
                                                                 
           
      real :: cff
      real :: cx
      real :: cy
      real :: dft
      real :: dfx
      real :: dfy
      real :: tau
      real :: tau_in
      real :: tau_out
      real :: hx
      real :: zx
                            

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      tau_in=dtfast*tauM_in
      tau_out=dtfast*tauM_out
      grad = 1.D0
      if (.not.WEST_INTER .and. .not.SOUTH_INTER) then
        grad(Istr,Jstr) = 0.5D0
      endif
      if (.not.EAST_INTER .and. .not.SOUTH_INTER) then
        grad(Iend,Jstr) = 0.5D0
      endif
      if (.not.WEST_INTER .and. .not.NORTH_INTER) then
        grad(Istr,Jend+1) = 0.5D0
      endif
      if (.not.EAST_INTER .and. .not.NORTH_INTER) then
        grad(Iend,Jend+1) = 0.5D0
      endif
      if (.not.SOUTH_INTER) then
        do i=Istr,Iend
          cff=-sqrt(2.D0*g/(h(i,Jstr)+h(i,Jstr-1)+
     &                    zeta(i,Jstr,kstp)+zeta(i,Jstr-1,kstp)))
          vbar(i,Jstr,knew)=cff
     &               *(0.5D0*(zeta(i,Jstr-1,knew)+zeta(i,Jstr,knew))
     &                            -0.5D0*(ssh(i,Jstr-1)+ssh(i,Jstr))
     &             )*grad(i,Jstr)
     &                                              +vbclm(i,Jstr)
          vbar(i,Jstr,knew)=vbar(i,Jstr,knew)*vmask(i,Jstr)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=Istr,Iend
        cff=sqrt(2.D0*g/(h(i,Jend)+h(i,Jend+1)
     &                +zeta(i,Jend,kstp)+zeta(i,Jend+1,kstp)))
          vbar(i,Jend+1,knew)=cff
     &               *(0.5D0*( zeta(i,Jend,knew)+zeta(i,Jend+1,knew))
     &                             -0.5D0*(ssh(i,Jend)+ssh(i,Jend+1))
     &               )*grad(i,Jend+1)
     &                                             +vbclm(i,Jend+1)
          vbar(i,Jend+1,knew)=vbar(i,Jend+1,knew)*vmask(i,Jend+1)
        enddo
      endif
      if (.not.WEST_INTER) then
        do j=JstrV,Jend
          cff=sqrt(0.5D0*g*(h(Istr-1,j-1)+h(Istr-1,j)+
     &                    zeta(Istr-1,j-1,kstp)+zeta(Istr-1,j,kstp)))
          cx=dtfast*cff*0.5D0*(pm(Istr-1,j-1)+pm(Istr-1,j))
          vbar(Istr-1,j,knew)=( vbar(Istr-1,j,kstp)
     &                         +cx*vbar(Istr,j,knew) )/(1.D0+cx)
     &                        *vmask(Istr-1,j)
        enddo
      endif
      if (.not.EAST_INTER) then
        do j=JstrV,Jend
          cff=sqrt(0.5D0*g*(h(Iend+1,j-1)+h(Iend+1,j)+
     &                    zeta(Iend+1,j-1,kstp)+zeta(Iend+1,j,kstp)))
          cx=dtfast*cff*0.5D0*(pm(Iend+1,j-1)+pm(Iend+1,j))
          vbar(Iend+1,j,knew)=( vbar(Iend+1,j,kstp)
     &                         +cx*vbar(Iend,j,knew) )/(1.D0+cx)
     &                        *vmask(Iend+1,j)
        enddo
      endif
      if (.not.WEST_INTER .and. .not.SOUTH_INTER) then
        vbar(Istr-1,Jstr,knew)=0.5D0*( vbar(Istr-1,Jstr+1,knew)
     &                              +vbar(Istr  ,Jstr  ,knew))
     &                         *vmask(Istr-1,Jstr)
      endif
      if (.not.EAST_INTER .and. .not.SOUTH_INTER) then
        vbar(Iend+1,Jstr,knew)=0.5D0*( vbar(Iend+1,Jstr+1,knew)
     &                              +vbar(Iend  ,Jstr  ,knew))
     &                         *vmask(Iend+1,Jstr)
      endif
      if (.not.WEST_INTER .and. .not.NORTH_INTER) then
        vbar(Istr-1,Jend+1,knew)=0.5D0*( vbar(Istr-1,Jend,knew)
     &                                +vbar(Istr,Jend+1,knew))
     &                         *vmask(Istr-1,Jend+1)
      endif
      if (.not.EAST_INTER .and. .not.NORTH_INTER) then
        vbar(Iend+1,Jend+1,knew)=0.5D0*( vbar(Iend+1,Jend,knew)
     &                                +vbar(Iend,Jend+1,knew))
     &                         *vmask(Iend+1,Jend+1)
      endif
      return
       
      

      end subroutine Sub_Loop_v2dbc_parent_tile

      subroutine v2dbc_child_tile (Istr,Iend,Jstr,Jend,grad)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_v2dbc_child_tile(Istr,Iend,Jstr,Jend,grad,pa
     &dd_E,Mm,padd_X,Lm,vmask,vbclm,knew,vbar,tauM_out,tauM_in,dtfast,A1
     &dXI,A1dETA,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,Mmmpi,Lmm
     &pi,N3d,N2d)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: eps = 1.D-20
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      real :: tauM_out
      real :: tauM_in
      real :: dtfast
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      integer(4) :: N2d
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,1:10*NWEIGHT) :: A1dXI
      real, dimension(-1:Mm+2+padd_E,1:10*NWEIGHT) :: A1dETA
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad
        end subroutine Sub_Loop_v2dbc_child_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad
      

        call Sub_Loop_v2dbc_child_tile(Istr,Iend,Jstr,Jend,grad, Agrif_t
     &abvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars
     &_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(48)%a
     &rray2, Agrif_tabvars(132)%array2, Agrif_tabvars_i(179)%iarray0, Ag
     &rif_tabvars(96)%array3, Agrif_tabvars_r(16)%array0, Agrif_tabvars_
     &r(17)%array0, Agrif_tabvars_r(45)%array0, Agrif_tabvars(5)%array2,
     & Agrif_tabvars(4)%array2, Agrif_tabvars_l(5)%larray0, Agrif_tabvar
     &s_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Agrif_tabvars_l(6)%lar
     &ray0, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0, 
     &Agrif_tabvars_i(186)%iarray0, Agrif_tabvars_i(187)%iarray0)

      end


      subroutine Sub_Loop_v2dbc_child_tile(Istr,Iend,Jstr,Jend,grad,padd
     &_E,Mm,padd_X,Lm,vmask,vbclm,knew,vbar,tauM_out,tauM_in,dtfast,A1dX
     &I,A1dETA,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER,Mmmpi,Lmmpi
     &,N3d,N2d)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: eps = 1.D-20
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      real :: tauM_out
      real :: tauM_in
      real :: dtfast
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      integer(4) :: N2d
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,1:10*NWEIGHT) :: A1dXI
      real, dimension(-1:Mm+2+padd_E,1:10*NWEIGHT) :: A1dETA
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                         
      integer(4) :: i
      integer(4) :: j
                                                
                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                    
      integer(4) :: trd
C$    integer*4 omp_get_thread_num
                                                                       
                                                                 
           
      real :: cff
      real :: cx
      real :: cy
      real :: dft
      real :: dfx
      real :: dfy
      real :: tau
      real :: tau_in
      real :: tau_out
      real :: hx
      real :: zx
                            

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      trd=0
C$    trd=omp_get_thread_num()
      call v2dbc_interp_tile(Istr,Iend,Jstr,Jend
     &                    ,A1dETA(1,1)
     &                    ,A1dETA(1,2+NWEIGHT)
     &                    ,A1dETA(1,3+2*NWEIGHT)
     &                    ,A1dETA(1,4+2*NWEIGHT)
     &                    ,A1dXI(1,1)
     &                    ,A1dXI(1,2+NWEIGHT)
     &                    ,A1dXI(1,3+2*NWEIGHT)
     &                    ,A1dXI(1,4+2*NWEIGHT))
      tau_in=dtfast*tauM_in
      tau_out=dtfast*tauM_out
      if (.not.SOUTH_INTER) then
        do i=Istr,Iend
          vbar(i,Jstr,knew)=vbclm(i,Jstr)
     &                      *vmask(i,Jstr)
        enddo
      endif
      if (.not.NORTH_INTER) then
        do i=Istr,Iend
          vbar(i,Jend+1,knew)=vbclm(i,Jend+1)
     &                        *vmask(i,Jend+1)
        enddo
      endif
      if (.not.WEST_INTER) then
       do j=JstrV,Jend
          vbar(Istr-1,j,knew)=vbclm(Istr-1,j)
     &                        *vmask(Istr-1,j)
        enddo
      endif
      if (.not.EAST_INTER) then
        do j=JstrV,Jend
          vbar(Iend+1,j,knew)=vbclm(Iend+1,j)
     &                        *vmask(Iend+1,j)
        enddo
      endif
      if (.not.WEST_INTER .and. .not.SOUTH_INTER) then
        vbar(Istr-1,Jstr,knew)=0.5D0*( vbar(Istr-1,Jstr+1,knew)
     &                              +vbar(Istr  ,Jstr  ,knew))
     &                         *vmask(Istr-1,Jstr)
      endif
      if (.not.EAST_INTER .and. .not.SOUTH_INTER) then
        vbar(Iend+1,Jstr,knew)=0.5D0*( vbar(Iend+1,Jstr+1,knew)
     &                              +vbar(Iend  ,Jstr  ,knew))
     &                         *vmask(Iend+1,Jstr)
      endif
      if (.not.WEST_INTER .and. .not.NORTH_INTER) then
        vbar(Istr-1,Jend+1,knew)=0.5D0*( vbar(Istr-1,Jend,knew)
     &                                +vbar(Istr,Jend+1,knew))
     &                         *vmask(Istr-1,Jend+1)
      endif
      if (.not.EAST_INTER .and. .not.NORTH_INTER) then
        vbar(Iend+1,Jend+1,knew)=0.5D0*( vbar(Iend+1,Jend,knew)
     &                                +vbar(Iend,Jend+1,knew))
     &                         *vmask(Iend+1,Jend+1)
      endif
      return
       
      

      end subroutine Sub_Loop_v2dbc_child_tile

