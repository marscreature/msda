












      subroutine get_date (date_str)
      use Agrif_Types, only : Agrif_tabvars

      implicit none

      character*(*) :: date_str

                   

                             
                                                                       
                                                                 
                                                                 
 
      integer(4) :: year
      integer(4) :: hour
      integer(4) :: minute
      integer(4) :: sec
      integer(4) :: half
      integer(4) :: iday
      integer(4) :: imon
      integer(4) :: dstat
      integer(4) :: tstat
      integer(4) :: nday
      integer(4), dimension(1:12) :: lmonth
      integer(4), dimension(1:31) :: lday
      integer(4) :: len1
      integer(4) :: len2
      integer(4) :: len3
      integer(4) :: lenstr
                             
      character(3), dimension(0:1) :: ampm
                                      
      character(9), dimension(0:6) :: day
      character(9), dimension(1:12) :: month
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                     
      data lmonth/7,8,5,5,3,4,4,6,9,7,8,8/
      data ampm/' AM',' PM'/
      data lday/9*1,22*2/
      data day/'Sunday   ','Monday   ','Tuesday  ','Wednesday','Thursday
     & ','Friday   ','Saturday '/
      data month/'January  ','February ','March    ','April    ','May   
     &   ','June     ','July     ','August   ','September','October  ','
     &November ','December '/
                                                        
      character(11) :: ctime
      character(18) :: today
      character(20) :: fmt
      character(44) :: wkday
      hour=0
      minute=0
      sec=0
      nday=1
      dstat=1
      tstat=1
      wkday=' '
      today=' '
      ctime=' '
      if (tstat.eq.0) then
        half=hour/12
        hour=hour-half*12
        if (hour.eq.0) hour=12
        if (half.eq.2) half=0
      endif
      if (dstat.eq.0) then
        write(fmt,10) lmonth(imon), lday(nday)
  10    format('(a',i1,',1x,i',i1,',1h,,1x,i4)')
        write(today,fmt) month(imon),nday,year
        wkday=day(iday)
      endif
      if(tstat.eq.0) then
        write(ctime,20) hour, minute, sec, ampm(half)
  20    format(i2,':',i2.2,':',i2.2,a3)
      endif
      len1=lenstr(wkday)
      len2=lenstr(today)
      len3=lenstr(ctime)
      date_str=wkday(1:len1)
      if (len2.gt.0) then
        len1=lenstr(date_str)
        date_str=date_str(1:len1)/ /' - '/ /today(1:len2)
      endif
      if (len3.gt.0) then
        len1=lenstr(date_str)
        date_str=date_str(1:len1)/ /' - '/ /ctime(1:len3)
      endif
      return
      end
