












      subroutine set_avg (tile)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_set_avg(tile,padd_E,Mm,padd_X,Lm,workr,wrtav
     &g,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: workr
      logical, dimension(1:500+NT) :: wrtavg
      integer(4) :: tile
        end subroutine Sub_Loop_set_avg

      end interface
      integer(4) :: tile
      

        call Sub_Loop_set_avg(tile, Agrif_tabvars_i(188)%iarray0, Agrif_
     &tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvar
     &s_i(200)%iarray0, Agrif_tabvars(254)%array3, Agrif_tabvars_l(2)%la
     &rray1, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_set_avg(tile,padd_E,Mm,padd_X,Lm,workr,wrtavg,
     &Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: workr
      logical, dimension(1:500+NT) :: wrtavg
      integer(4) :: tile

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                   
                           
                                                    
                              
                                                 
                             
                                                  
                               
                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                          
      integer(4) :: trd
C$    integer*4 omp_get_thread_num
                                                            
      integer(4) :: chunk_size_X
      integer(4) :: margin_X
      integer(4) :: chunk_size_E
      integer(4) :: margin_E
                                             
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: i_X
      integer(4) :: j_E
      chunk_size_X=(Lmmpi+NSUB_X-1)/NSUB_X
      margin_X=(NSUB_X*chunk_size_X-Lmmpi)/2
      chunk_size_E=(Mmmpi+NSUB_E-1)/NSUB_E
      margin_E=(NSUB_E*chunk_size_E-Mmmpi)/2
      j_E=tile/NSUB_X
      i_X=tile-j_E*NSUB_X
      Istr=1+i_X*chunk_size_X-margin_X
      Iend=Istr+chunk_size_X-1
      Istr=max(Istr,1)
      Iend=min(Iend,Lmmpi)
      Jstr=1+j_E*chunk_size_E-margin_E
      Jend=Jstr+chunk_size_E-1
      Jstr=max(Jstr,1)
      Jend=min(Jend,Mmmpi)
      if (wrtavg(indxW)) then
        call Wvlcty (tile, workr)
      endif
      call set_avg_tile (Istr,Iend,Jstr,Jend)
      return
       
      

      end subroutine Sub_Loop_set_avg

      subroutine set_avg_tile (Istr,Iend,Jstr,Jend)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_set_avg_tile(Istr,Iend,Jstr,Jend,padd_E,Mm,p
     &add_X,Lm,Akt,Akt_avg,Akv,Akv_avg,diff3d_v,diff3d_u,diff4,diff3d_av
     &g,shflx_sen,shflx_sen_avg,shflx_lat,shflx_lat_avg,shflx_rlw,shflx_
     &rlw_avg,shflx_rsw,shflx_rsw_avg,stflx,stflx_avg,hbbl,hbbl_avg,hbls
     &,hbl_avg,workr,w_avg,pn,pm,We,omega_avg,rho,rho_avg,t,t_avg,v,v_av
     &g,nstp,u,u_avg,svstr_avg,sustr_avg,svstr,sustr,wstr_avg,rho0,bvstr
     &,bustr,bostr_avg,vbar,vbar_avg,ubar,ubar_avg,knew,zeta,zeta_avg,wr
     &tavg,dt,time_avg,navg,ntsavg,ntstart,iic,Mmmpi,jj,Lmmpi,ii)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: eps = 1.D-20
      real, parameter :: stf_cff = 86400/0.01D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      real :: rho0
      integer(4) :: knew
      real :: dt
      real :: time_avg
      integer(4) :: navg
      integer(4) :: ntsavg
      integer(4) :: ntstart
      integer(4) :: iic
      integer(4) :: Mmmpi
      integer(4) :: jj
      integer(4) :: Lmmpi
      integer(4) :: ii
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N,1:2) :: Akt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N,1:2) :: Akt_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: diff4
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: hbls
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbl_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: workr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: w_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: We
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: omega_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: t_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: v_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: u_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: wstr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bostr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbar_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubar_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: zeta_avg
      logical, dimension(1:500+NT) :: wrtavg
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
        end subroutine Sub_Loop_set_avg_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      

        call Sub_Loop_set_avg_tile(Istr,Iend,Jstr,Jend, Agrif_tabvars_i(
     &188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%i
     &array0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(149)%array4, A
     &grif_tabvars(226)%array4, Agrif_tabvars(150)%array3, Agrif_tabvars
     &(227)%array3, Agrif_tabvars(154)%array3, Agrif_tabvars(155)%array3
     &, Agrif_tabvars(156)%array3, Agrif_tabvars(228)%array3, Agrif_tabv
     &ars(206)%array2, Agrif_tabvars(229)%array2, Agrif_tabvars(207)%arr
     &ay2, Agrif_tabvars(230)%array2, Agrif_tabvars(208)%array2, Agrif_t
     &abvars(231)%array2, Agrif_tabvars(209)%array2, Agrif_tabvars(232)%
     &array2, Agrif_tabvars(210)%array3, Agrif_tabvars(235)%array3, Agri
     &f_tabvars(147)%array2, Agrif_tabvars(233)%array2, Agrif_tabvars(14
     &6)%array3, Agrif_tabvars(234)%array2, Agrif_tabvars(254)%array3, A
     &grif_tabvars(236)%array3, Agrif_tabvars(73)%array2, Agrif_tabvars(
     &74)%array2, Agrif_tabvars(102)%array3, Agrif_tabvars(237)%array3, 
     &Agrif_tabvars(100)%array3, Agrif_tabvars(238)%array3, Agrif_tabvar
     &s(109)%array5, Agrif_tabvars(239)%array4, Agrif_tabvars(110)%array
     &4, Agrif_tabvars(240)%array3, Agrif_tabvars_i(176)%iarray0, Agrif_
     &tabvars(111)%array4, Agrif_tabvars(241)%array3, Agrif_tabvars(243)
     &%array2, Agrif_tabvars(244)%array2, Agrif_tabvars(224)%array2, Agr
     &if_tabvars(225)%array2, Agrif_tabvars(245)%array2, Agrif_tabvars_r
     &(38)%array0, Agrif_tabvars(217)%array2, Agrif_tabvars(218)%array2,
     & Agrif_tabvars(246)%array2, Agrif_tabvars(96)%array3, Agrif_tabvar
     &s(247)%array2, Agrif_tabvars(97)%array3, Agrif_tabvars(248)%array2
     &, Agrif_tabvars_i(179)%iarray0, Agrif_tabvars(98)%array3, Agrif_ta
     &bvars(249)%array2, Agrif_tabvars_l(2)%larray1, Agrif_tabvars_r(46)
     &%array0, Agrif_tabvars_r(40)%array0, Agrif_tabvars_i(163)%iarray0,
     & Agrif_tabvars_i(164)%iarray0, Agrif_tabvars_i(171)%iarray0, Agrif
     &_tabvars_i(182)%iarray0, Agrif_tabvars_i(194)%iarray0, Agrif_tabva
     &rs_i(154)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars_i(1
     &55)%iarray0)

      end


      subroutine Sub_Loop_set_avg_tile(Istr,Iend,Jstr,Jend,padd_E,Mm,pad
     &d_X,Lm,Akt,Akt_avg,Akv,Akv_avg,diff3d_v,diff3d_u,diff4,diff3d_avg,
     &shflx_sen,shflx_sen_avg,shflx_lat,shflx_lat_avg,shflx_rlw,shflx_rl
     &w_avg,shflx_rsw,shflx_rsw_avg,stflx,stflx_avg,hbbl,hbbl_avg,hbls,h
     &bl_avg,workr,w_avg,pn,pm,We,omega_avg,rho,rho_avg,t,t_avg,v,v_avg,
     &nstp,u,u_avg,svstr_avg,sustr_avg,svstr,sustr,wstr_avg,rho0,bvstr,b
     &ustr,bostr_avg,vbar,vbar_avg,ubar,ubar_avg,knew,zeta,zeta_avg,wrta
     &vg,dt,time_avg,navg,ntsavg,ntstart,iic,Mmmpi,jj,Lmmpi,ii)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: eps = 1.D-20
      real, parameter :: stf_cff = 86400/0.01D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nstp
      real :: rho0
      integer(4) :: knew
      real :: dt
      real :: time_avg
      integer(4) :: navg
      integer(4) :: ntsavg
      integer(4) :: ntstart
      integer(4) :: iic
      integer(4) :: Mmmpi
      integer(4) :: jj
      integer(4) :: Lmmpi
      integer(4) :: ii
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N,1:2) :: Akt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N,1:2) :: Akt_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: diff4
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: hbls
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbl_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: workr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: w_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: We
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: omega_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: t_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: v_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: u_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: wstr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bostr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbar_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubar_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: zeta_avg
      logical, dimension(1:500+NT) :: wrtavg
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                             
      integer(4) :: i
      integer(4) :: j
      integer(4) :: ilc
      integer(4) :: iout
      integer(4) :: indxWrk
                                   
      real :: cff
      real :: cff1
                            

                                     

                       
      integer(4) :: itrc
      integer(4) :: k
                                                   
                           
                                                    
                              
                                                 
                             
                                                  
                               
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                   
                                                   
                                                   
                                                                       
                                                                 
                         
                                                    
                                  
                                                   
                                
                                                    
                                  
                                                    
                                  
                                                    
                                  
                                                  
                                                  
                                                     
                                                    
                                                        
                                                  
                                                                       
                                                                 
                                                          
                                                       
                                  
                                                  
                              
                                                   
                                
                                                        
                                                        
                                                        
                                                        
                                          
                                          
                                          
                                          
                                                       
                                    
                                                      
                                                        
                                               
                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
      if (Istr.eq.1 .and. ii.eq.0) then
        IstrR=Istr-1
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lmmpi.and. ii.eq.NP_XI-1) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (Jstr.eq.1 .and. jj.eq.0) then
        JstrR=Jstr-1
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mmmpi .and. jj.eq.NP_ETA-1) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      ilc=1+iic-ntstart
      if (ilc.gt.ntsavg) then
        if (mod(ilc-1,navg).eq.1) then
          cff =1.0D0
          cff1=0.0D0
        elseif (mod(ilc-1,navg).gt.1) then
          cff =1.0D0
          cff1=1.0D0
        elseif (mod(ilc-1,navg).eq.0) then
          cff=1.D0/float(navg)
          cff1=1.0D0
          if (Istr+Jstr.eq.2) time_avg=time_avg+float(navg)*dt
        endif
        if (wrtavg(indxZ)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              zeta_avg(i,j)=cff*( cff1*zeta_avg(i,j)
     &                                +zeta(i,j,knew))
            enddo
          enddo
        endif
        if (wrtavg(indxUb)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              ubar_avg(i,j)=cff*( cff1*ubar_avg(i,j)
     &                                +ubar(i,j,knew))
            enddo
          enddo
        endif
        if (wrtavg(indxVb)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              vbar_avg(i,j)=cff*( cff1*vbar_avg(i,j)
     &                                +vbar(i,j,knew))
            enddo
          enddo
        endif
        if (wrtavg(indxBostr)) then
          do j=Jstr,Jend
            do i=Istr,Iend
              bostr_avg(i,j)=cff*( cff1*bostr_avg(i,j)+
     &                         0.5D0*sqrt((bustr(i,j)+bustr(i+1,j))**2
     &                                 +(bvstr(i,j)+bvstr(i,j+1))**2)
     &                                                          *rho0)
            enddo
          enddo
        endif
        if (wrtavg(indxWstr)) then
          do j=Jstr,Jend
            do i=Istr,Iend
              wstr_avg(i,j)=cff*( cff1*wstr_avg(i,j)+
     &                         0.5D0*sqrt((sustr(i,j)+sustr(i+1,j))**2
     &                                 +(svstr(i,j)+svstr(i,j+1))**2)
     &                                                          *rho0)
            enddo
          enddo
        endif
        if (wrtavg(indxUWstr)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              sustr_avg(i,j)=cff*( cff1*sustr_avg(i,j)+
     &                                  sustr(i,j)*rho0)
            enddo
          enddo
        endif
        if (wrtavg(indxVWstr)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              svstr_avg(i,j)=cff*( cff1*svstr_avg(i,j)+
     &                                  svstr(i,j)*rho0)
            enddo
          enddo
        endif
        if (wrtavg(indxU)) then
          do k=1,N
            do j=JstrR,JendR
              do i=IstrR,IendR
                u_avg(i,j,k)=cff*(cff1*u_avg(i,j,k)+u(i,j,k,nstp))
              enddo
            enddo
          enddo
        endif
        if (wrtavg(indxV)) then
          do k=1,N
            do j=JstrR,JendR
              do i=IstrR,IendR
                v_avg(i,j,k)=cff*(cff1*v_avg(i,j,k)+v(i,j,k,nstp))
              enddo
            enddo
          enddo
        endif
        do itrc=1,NT
          if (wrtavg(indxT+itrc-1)) then
            do k=1,N
              do j=JstrR,JendR
                do i=IstrR,IendR
                  t_avg(i,j,k,itrc)=cff*( cff1*t_avg(i,j,k,itrc)
     &                                        +t(i,j,k,nstp,itrc))
                enddo
              enddo
            enddo
          endif
        enddo
        if (wrtavg(indxR)) then
          do k=1,N
            do j=JstrR,JendR
              do i=IstrR,IendR
                rho_avg(i,j,k)=cff*(cff1*rho_avg(i,j,k)+rho(i,j,k))
              enddo
            enddo
          enddo
        endif
        if (wrtavg(indxO)) then
          do k=0,N
            do j=JstrR,JendR
              do i=IstrR,IendR
                omega_avg(i,j,k)=cff*( cff1*omega_avg(i,j,k)
     &                                 +We(i,j,k)*pm(i,j)*pn(i,j)
     &                                                          )
              enddo
            enddo
          enddo
        endif
        if (wrtavg(indxW)) then
          do k=1,N
            do j=JstrR,JendR
              do i=IstrR,IendR
                w_avg(i,j,k)=cff*(cff1*w_avg(i,j,k)+workr(i,j,k))
              enddo
            enddo
          enddo
        endif
        if (wrtavg(indxHbl)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              hbl_avg(i,j)=cff*( cff1*hbl_avg(i,j)
     &                               +hbls(i,j,nstp))
            enddo
          enddo
        endif
        if (wrtavg(indxHbbl)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              hbbl_avg(i,j)=cff*( cff1*hbbl_avg(i,j)
     &                                +hbbl(i,j))
            enddo
          enddo
        endif
        if (wrtavg(indxShflx)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              stflx_avg(i,j,itemp)=cff*(cff1*stflx_avg(i,j,itemp)+
     &                                       stflx(i,j,itemp)*(rho0*Cp))
            enddo
          enddo
        endif
        if (wrtavg(indxSwflx)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              stflx_avg(i,j,isalt)=cff*(cff1*stflx_avg(i,j,isalt)+
     &                               stf_cff*stflx(i,j,isalt) /
     &               ( max(eps,t(i,j,N,nstp,isalt)) ) )
            enddo
          enddo
        endif
        if (wrtavg(indxShflx_rsw)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              shflx_rsw_avg(i,j)=cff*( cff1*shflx_rsw_avg(i,j)+
     &                                 shflx_rsw(i,j)*rho0*Cp)
            enddo
          enddo
        endif
        if (wrtavg(indxShflx_rlw)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              shflx_rlw_avg(i,j)=cff*( cff1*shflx_rlw_avg(i,j)+
     &                                 shflx_rlw(i,j)*rho0*Cp)
            enddo
          enddo
        endif
        if (wrtavg(indxShflx_lat)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              shflx_lat_avg(i,j)=cff*( cff1*shflx_lat_avg(i,j)+
     &                                 shflx_lat(i,j)*rho0*Cp)
            enddo
          enddo
        endif
        if (wrtavg(indxShflx_sen)) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              shflx_sen_avg(i,j)=cff*( cff1*shflx_sen_avg(i,j)+
     &                                 shflx_sen(i,j)*rho0*Cp)
            enddo
          enddo
        endif
        if (wrtavg(indxDiff)) then
          do k=1,N
            do j=Jstr,Jend
              do i=Istr,Iend
                diff3d_avg(i,j,k)=cff*(cff1*diff3d_avg(i,j,k)+
     &                      diff4(i,j,itemp)
     &                     +0.25D0*(diff3d_u(i,j,k)+diff3d_u(i+1,j,k)
     &                           +diff3d_v(i,j,k)+diff3d_v(i,j+1,k))
     &                                  )
              enddo
            enddo
          enddo
        endif
        if (wrtavg(indxAkv)) then
          do k=0,N
            do j=JstrR,JendR
              do i=IstrR,IendR
                Akv_avg(i,j,k)=cff*(cff1*Akv_avg(i,j,k)+Akv(i,j,k))
              enddo
            enddo
          enddo
        endif
        if (wrtavg(indxAkt)) then
          do k=0,N
            do j=JstrR,JendR
              do i=IstrR,IendR
                Akt_avg(i,j,k,itemp)=cff*(cff1*Akt_avg(i,j,k,itemp)
     &                                        +Akt(i,j,k,itemp))
              enddo
            enddo
          enddo
        endif
        if (wrtavg(indxAks)) then
          do k=0,N
            do j=JstrR,JendR
              do i=IstrR,IendR
                Akt_avg(i,j,k,isalt)=cff*(cff1*Akt_avg(i,j,k,isalt)
     &                                        +Akt(i,j,k,isalt))
              enddo
            enddo
          enddo
        endif
      endif
      return
       
      

      end subroutine Sub_Loop_set_avg_tile

