












      subroutine rho_eos (tile)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_rho_eos(tile,N2d,A2d,Mmmpi,Lmmpi,N3d)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: N2d
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      real, dimension(1:N2d,1:NSA,0:NPP-1) :: A2d
      integer(4) :: tile
        end subroutine Sub_Loop_rho_eos

      end interface
      integer(4) :: tile
      

        call Sub_Loop_rho_eos(tile, Agrif_tabvars_i(187)%iarray0, Agrif_
     &tabvars(95)%array3, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(
     &195)%iarray0, Agrif_tabvars_i(186)%iarray0)

      end


      subroutine Sub_Loop_rho_eos(tile,N2d,A2d,Mmmpi,Lmmpi,N3d)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: N2d
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      real, dimension(1:N2d,1:NSA,0:NPP-1) :: A2d
      integer(4) :: tile

                   

                                              
      integer(4) :: trd
      integer(4) :: omp_get_thread_num
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                                                            
      integer(4) :: chunk_size_X
      integer(4) :: margin_X
      integer(4) :: chunk_size_E
      integer(4) :: margin_E
                                             
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: i_X
      integer(4) :: j_E
      chunk_size_X=(Lmmpi+NSUB_X-1)/NSUB_X
      margin_X=(NSUB_X*chunk_size_X-Lmmpi)/2
      chunk_size_E=(Mmmpi+NSUB_E-1)/NSUB_E
      margin_E=(NSUB_E*chunk_size_E-Mmmpi)/2
      j_E=tile/NSUB_X
      i_X=tile-j_E*NSUB_X
      Istr=1+i_X*chunk_size_X-margin_X
      Iend=Istr+chunk_size_X-1
      Istr=max(Istr,1)
      Iend=min(Iend,Lmmpi)
      Jstr=1+j_E*chunk_size_E-margin_E
      Jend=Jstr+chunk_size_E-1
      Jstr=max(Jstr,1)
      Jend=min(Jend,Mmmpi)
      trd=omp_get_thread_num()
      call rho_eos_tile (Istr,Iend,Jstr,Jend, A2d(1,1,trd),
     &                                        A2d(1,2,trd))
      return
       
      

      end subroutine Sub_Loop_rho_eos

      subroutine rho_eos_tile (Istr,Iend,Jstr,Jend, K_up,K_dw)      


      use Agrif_Util
      integer(4), parameter :: N = 66
      interface
        subroutine Sub_Loop_rho_eos_tile(Istr,Iend,Jstr,Jend,K_up,K_dw,p
     &add_E,Mm,padd_X,Lm,rhoA,rhoS,Hz,bvf,rho,z_r,z_w,qp1,rmask,rho1,nrh
     &s,t,rho0,NORTH_INTER,Mmmpi,SOUTH_INTER,EAST_INTER,Lmmpi,WEST_INTER
     &)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: r00 = 999.842594D0
      real, parameter :: r01 = 6.793952D-2
      real, parameter :: r02 = -9.095290D-3
      real, parameter :: r03 = 1.001685D-4
      real, parameter :: r04 = -1.120083D-6
      real, parameter :: r05 = 6.536332D-9
      real, parameter :: r10 = 0.824493D0
      real, parameter :: r11 = -4.08990D-3
      real, parameter :: r12 = 7.64380D-5
      real, parameter :: r13 = -8.24670D-7
      real, parameter :: r14 = 5.38750D-9
      real, parameter :: rS0 = -5.72466D-3
      real, parameter :: rS1 = 1.02270D-4
      real, parameter :: rS2 = -1.65460D-6
      real, parameter :: r20 = 4.8314D-4
      real, parameter :: K00 = 19092.56D0
      real, parameter :: K01 = 209.8925D0
      real, parameter :: K02 = -3.041638D0
      real, parameter :: K03 = -1.852732D-3
      real, parameter :: K04 = -1.361629D-5
      real, parameter :: K10 = 104.4077D0
      real, parameter :: K11 = -6.500517D0
      real, parameter :: K12 = 0.1553190D0
      real, parameter :: K13 = 2.326469D-4
      real, parameter :: KS0 = -5.587545D0
      real, parameter :: KS1 = +0.7390729D0
      real, parameter :: KS2 = -1.909078D-2
      real, parameter :: B00 = 0.4721788D0
      real, parameter :: B01 = 0.01028859D0
      real, parameter :: B02 = -2.512549D-4
      real, parameter :: B03 = -5.939910D-7
      real, parameter :: B10 = -0.01571896D0
      real, parameter :: B11 = -2.598241D-4
      real, parameter :: B12 = 7.267926D-6
      real, parameter :: BS1 = 2.042967D-3
      real, parameter :: E00 = +1.045941D-5
      real, parameter :: E01 = -5.782165D-10
      real, parameter :: E02 = +1.296821D-7
      real, parameter :: E10 = -2.595994D-7
      real, parameter :: E11 = -1.248266D-9
      real, parameter :: E12 = -3.508914D-9
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nrhs
      real :: rho0
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhoA
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhoS
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: z_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: qp1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,0:N) :: K_up
      real, dimension(Istr-2:Iend+2,0:N) :: K_dw
        end subroutine Sub_Loop_rho_eos_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,0:N) :: K_up
      real, dimension(Istr-2:Iend+2,0:N) :: K_dw
      

        call Sub_Loop_rho_eos_tile(Istr,Iend,Jstr,Jend,K_up,K_dw, Agrif_
     &tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvar
     &s_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(122)
     &%array2, Agrif_tabvars(121)%array2, Agrif_tabvars(104)%array3, Agr
     &if_tabvars(148)%array3, Agrif_tabvars(100)%array3, Agrif_tabvars(1
     &03)%array3, Agrif_tabvars(107)%array3, Agrif_tabvars(99)%array3, A
     &grif_tabvars(51)%array2, Agrif_tabvars(101)%array3, Agrif_tabvars_
     &i(175)%iarray0, Agrif_tabvars(109)%array5, Agrif_tabvars_r(38)%arr
     &ay0, Agrif_tabvars_l(5)%larray0, Agrif_tabvars_i(194)%iarray0, Agr
     &if_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Agrif_tabvars
     &_i(195)%iarray0, Agrif_tabvars_l(6)%larray0)

      end


      subroutine Sub_Loop_rho_eos_tile(Istr,Iend,Jstr,Jend,K_up,K_dw,pad
     &d_E,Mm,padd_X,Lm,rhoA,rhoS,Hz,bvf,rho,z_r,z_w,qp1,rmask,rho1,nrhs,
     &t,rho0,NORTH_INTER,Mmmpi,SOUTH_INTER,EAST_INTER,Lmmpi,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: r00 = 999.842594D0
      real, parameter :: r01 = 6.793952D-2
      real, parameter :: r02 = -9.095290D-3
      real, parameter :: r03 = 1.001685D-4
      real, parameter :: r04 = -1.120083D-6
      real, parameter :: r05 = 6.536332D-9
      real, parameter :: r10 = 0.824493D0
      real, parameter :: r11 = -4.08990D-3
      real, parameter :: r12 = 7.64380D-5
      real, parameter :: r13 = -8.24670D-7
      real, parameter :: r14 = 5.38750D-9
      real, parameter :: rS0 = -5.72466D-3
      real, parameter :: rS1 = 1.02270D-4
      real, parameter :: rS2 = -1.65460D-6
      real, parameter :: r20 = 4.8314D-4
      real, parameter :: K00 = 19092.56D0
      real, parameter :: K01 = 209.8925D0
      real, parameter :: K02 = -3.041638D0
      real, parameter :: K03 = -1.852732D-3
      real, parameter :: K04 = -1.361629D-5
      real, parameter :: K10 = 104.4077D0
      real, parameter :: K11 = -6.500517D0
      real, parameter :: K12 = 0.1553190D0
      real, parameter :: K13 = 2.326469D-4
      real, parameter :: KS0 = -5.587545D0
      real, parameter :: KS1 = +0.7390729D0
      real, parameter :: KS2 = -1.909078D-2
      real, parameter :: B00 = 0.4721788D0
      real, parameter :: B01 = 0.01028859D0
      real, parameter :: B02 = -2.512549D-4
      real, parameter :: B03 = -5.939910D-7
      real, parameter :: B10 = -0.01571896D0
      real, parameter :: B11 = -2.598241D-4
      real, parameter :: B12 = 7.267926D-6
      real, parameter :: BS1 = 2.042967D-3
      real, parameter :: E00 = +1.045941D-5
      real, parameter :: E01 = -5.782165D-10
      real, parameter :: E02 = +1.296821D-7
      real, parameter :: E10 = -2.595994D-7
      real, parameter :: E11 = -1.248266D-9
      real, parameter :: E12 = -3.508914D-9
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nrhs
      real :: rho0
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhoA
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhoS
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: z_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: z_w
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: qp1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,0:N) :: K_up
      real, dimension(Istr-2:Iend+2,0:N) :: K_dw

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
            
      real :: K0
      real :: K1
      real :: K2
      real :: dr00
      real :: Ts
      real :: Tt
      real :: sqrtTs
      real :: dpth
      real :: cff
      real :: cff1
      real :: cff2
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                   

                  
      real :: K0_Duk
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
      if (Istr.eq.1) then
        if (WEST_INTER) then
          IstrR=Istr-2
        else
          IstrR=Istr-1
        endif
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lmmpi) then
        if (EAST_INTER) then
          IendR=Iend+2
        else
          IendR=Iend+1
        endif
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        if (SOUTH_INTER) then
          JstrR=Jstr-2
        else
          JstrR=Jstr-1
        endif
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mmmpi) then
        if (NORTH_INTER) then
          JendR=Jend+2
        else
          JendR=Jend+1
        endif
      else
        JendR=Jend
      endif
      Tt=3.8D0
      Ts=34.5D0
      sqrtTs=sqrt(Ts)
      K0_Duk= Tt*( K01+Tt*( K02+Tt*( K03+Tt*K04 )))
     &       +Ts*( K10+Tt*( K11+Tt*( K12+Tt*K13 ))
     &            +sqrtTs*( KS0+Tt*( KS1+Tt*KS2 )))
      dr00=r00-rho0
      do j=JstrR,JendR
        do k=1,N
          do i=IstrR,IendR
            Tt=t(i,j,k,nrhs,itemp)
            Ts=max(t(i,j,k,nrhs,isalt), 0.D0)
            sqrtTs=sqrt(Ts)
            rho1(i,j,k)=( dr00 +Tt*( r01+Tt*( r02+Tt*( r03+Tt*(
     &                                            r04+Tt*r05 ))))
     &                         +Ts*( r10+Tt*( r11+Tt*( r12+Tt*(
     &                                            r13+Tt*r14 )))
     &                              +sqrtTs*(rS0+Tt*(
     &                                    rS1+Tt*rS2 ))+Ts*r20 ))
     &                                                *rmask(i,j)
            K0= Tt*( K01+Tt*( K02+Tt*( K03+Tt*K04 )))
     &         +Ts*( K10+Tt*( K11+Tt*( K12+Tt*K13 ))
     &              +sqrtTs*( KS0+Tt*( KS1+Tt*KS2 )))
            qp1(i,j,k)= 0.1D0*(rho0+rho1(i,j,k))*(K0_Duk-K0)
     &                               /((K00+K0)*(K00+K0_Duk))
     &                                                  *rmask(i,j)
            dpth=z_w(i,j,N)-z_r(i,j,k)
            rho(i,j,k)=rho1(i,j,k) +qp1(i,j,k)*dpth*(1.D0-qp2*dpth)
            rho(i,j,k)=rho(i,j,k)*rmask(i,j)
          enddo
        enddo
        cff=g/rho0
        do k=1,N-1
          do i=IstrR,IendR
            dpth=z_w(i,j,N)-0.5D0*(z_r(i,j,k+1)+z_r(i,j,k))
            cff2=( rho1(i,j,k+1)-rho1(i,j,k)
     &                        +(qp1(i,j,k+1)-qp1(i,j,k))
     &                            *dpth*(1.D0-2.D0*qp2*dpth)
     &                       )
            bvf(i,j,k)=-cff*cff2 / (z_r(i,j,k+1)-z_r(i,j,k))
            bvf (i,j,k)= bvf (i,j,k)*rmask(i,j)
          enddo
        enddo
        do i=istrR,iendR
          bvf(i,j,N)=bvf(i,j,N-1)
          bvf(i,j,0)=bvf(i,j,  1)
        enddo
        do i=IstrR,IendR
          dpth=z_w(i,j,N)-z_r(i,j,N)
          cff=Hz(i,j,N)*(rho1(i,j,N)+qp1(i,j,N)*dpth*(1.D0-qp2*dpth))
          rhoS(i,j)=0.5D0*cff*Hz(i,j,N)
          rhoA(i,j)=cff
        enddo
        do k=N-1,1,-1
          do i=IstrR,IendR
            dpth=z_w(i,j,N)-z_r(i,j,k)
            cff=Hz(i,j,k)*(rho1(i,j,k)+qp1(i,j,k)*dpth*(1.D0-qp2*dpth))
            rhoS(i,j)=rhoS(i,j)+Hz(i,j,k)*(rhoA(i,j)+0.5D0*cff)
            rhoA(i,j)=rhoA(i,j)+cff
          enddo
        enddo
        cff1=1.D0/rho0
        do i=IstrR,IendR
          cff=1.D0/(z_w(i,j,N)-z_w(i,j,0))
          rhoA(i,j)=cff*cff1*rhoA(i,j)
          rhoS(i,j)=2.D0*cff*cff*cff1*rhoS(i,j)
        enddo
      enddo
      return
       
      

      end subroutine Sub_Loop_rho_eos_tile

