












      subroutine def_his (ncid, total_rec, ierr)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_def_his(ncid,total_rec,ierr,hisShflx_sen,his
     &Shflx_lat,hisShflx_rlw,hisShflx_rsw,hisSwflx,hisShflx,hisHbbl,hisH
     &bl,hisAks,hisAkt,hisAkv,hisDiff,hisVWstr,hisUWstr,hisWstr,hisBostr
     &,hisW,hisO,hisR,hisT,hisV,hisU,hisVb,hisUb,hisZ,wrthis,hisTime2,hi
     &sTime,vname,hisTstep,eta_v,eta_rho,xi_u,xi_rho,mynode,ldefhis,nrpf
     &his,hisname)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4), parameter :: nf_byte = 1
      integer(4), parameter :: nf_int1 = nf_byte
      integer(4), parameter :: nf_char = 2
      integer(4), parameter :: nf_short = 3
      integer(4), parameter :: nf_int2 = nf_short
      integer(4), parameter :: nf_int = 4
      integer(4), parameter :: nf_float = 5
      integer(4), parameter :: nf_real = nf_float
      integer(4), parameter :: nf_double = 6
      integer(4), parameter :: nf_fill_byte = -127
      integer(4), parameter :: nf_fill_int1 = nf_fill_byte
      integer(4), parameter :: nf_fill_char = 0
      integer(4), parameter :: nf_fill_short = -32767
      integer(4), parameter :: nf_fill_int2 = nf_fill_short
      integer(4), parameter :: nf_fill_int = -2147483647
      real, parameter :: nf_fill_float = 9.9692099683868690D+36
      real, parameter :: nf_fill_real = nf_fill_float
      real(8), parameter :: nf_fill_double = 9.9692099683868690D+36
      integer(4), parameter :: nf_nowrite = 0
      integer(4), parameter :: nf_write = 1
      integer(4), parameter :: nf_clobber = 0
      integer(4), parameter :: nf_noclobber = 4
      integer(4), parameter :: nf_fill = 0
      integer(4), parameter :: nf_nofill = 256
      integer(4), parameter :: nf_lock = 1024
      integer(4), parameter :: nf_share = 2048
      integer(4), parameter :: nf_64bit_offset = 512
      integer(4), parameter :: nf_sizehint_default = 0
      integer(4), parameter :: nf_align_chunk = -1
      integer(4), parameter :: nf_format_classic = 1
      integer(4), parameter :: nf_format_64bit = 2
      integer(4), parameter :: nf_diskless = 8
      integer(4), parameter :: nf_mmap = 16
      integer(4), parameter :: nf_unlimited = 0
      integer(4), parameter :: nf_global = 0
      integer(4), parameter :: nf_max_dims = 1024
      integer(4), parameter :: nf_max_attrs = 8192
      integer(4), parameter :: nf_max_vars = 8192
      integer(4), parameter :: nf_max_name = 256
      integer(4), parameter :: nf_max_var_dims = nf_max_dims
      integer(4), parameter :: nf_noerr = 0
      integer(4), parameter :: nf_ebadid = -33
      integer(4), parameter :: nf_eexist = -35
      integer(4), parameter :: nf_einval = -36
      integer(4), parameter :: nf_eperm = -37
      integer(4), parameter :: nf_enotindefine = -38
      integer(4), parameter :: nf_eindefine = -39
      integer(4), parameter :: nf_einvalcoords = -40
      integer(4), parameter :: nf_emaxdims = -41
      integer(4), parameter :: nf_enameinuse = -42
      integer(4), parameter :: nf_enotatt = -43
      integer(4), parameter :: nf_emaxatts = -44
      integer(4), parameter :: nf_ebadtype = -45
      integer(4), parameter :: nf_ebaddim = -46
      integer(4), parameter :: nf_eunlimpos = -47
      integer(4), parameter :: nf_emaxvars = -48
      integer(4), parameter :: nf_enotvar = -49
      integer(4), parameter :: nf_eglobal = -50
      integer(4), parameter :: nf_enotnc = -51
      integer(4), parameter :: nf_ests = -52
      integer(4), parameter :: nf_emaxname = -53
      integer(4), parameter :: nf_eunlimit = -54
      integer(4), parameter :: nf_enorecvars = -55
      integer(4), parameter :: nf_echar = -56
      integer(4), parameter :: nf_eedge = -57
      integer(4), parameter :: nf_estride = -58
      integer(4), parameter :: nf_ebadname = -59
      integer(4), parameter :: nf_erange = -60
      integer(4), parameter :: nf_enomem = -61
      integer(4), parameter :: nf_evarsize = -62
      integer(4), parameter :: nf_edimsize = -63
      integer(4), parameter :: nf_etrunc = -64
      integer(4), parameter :: nf_fatal = 1
      integer(4), parameter :: nf_verbose = 2
      integer(4), parameter :: nf_ubyte = 7
      integer(4), parameter :: nf_ushort = 8
      integer(4), parameter :: nf_uint = 9
      integer(4), parameter :: nf_int64 = 10
      integer(4), parameter :: nf_uint64 = 11
      integer(4), parameter :: nf_string = 12
      integer(4), parameter :: nf_vlen = 13
      integer(4), parameter :: nf_opaque = 14
      integer(4), parameter :: nf_enum = 15
      integer(4), parameter :: nf_compound = 16
      integer(4), parameter :: nf_fill_ubyte = 255
      integer(4), parameter :: nf_fill_ushort = 65535
      integer(4), parameter :: nf_format_netcdf4 = 3
      integer(4), parameter :: nf_format_netcdf4_classic = 4
      integer(4), parameter :: nf_netcdf4 = 4096
      integer(4), parameter :: nf_classic_model = 256
      integer(4), parameter :: nf_chunk_seq = 0
      integer(4), parameter :: nf_chunk_sub = 1
      integer(4), parameter :: nf_chunk_sizes = 2
      integer(4), parameter :: nf_endian_native = 0
      integer(4), parameter :: nf_endian_little = 1
      integer(4), parameter :: nf_endian_big = 2
      integer(4), parameter :: nf_chunked = 0
      integer(4), parameter :: nf_contiguous = 1
      integer(4), parameter :: nf_nochecksum = 0
      integer(4), parameter :: nf_fletcher32 = 1
      integer(4), parameter :: nf_noshuffle = 0
      integer(4), parameter :: nf_shuffle = 1
      integer(4), parameter :: nf_szip_ec_option_mask = 4
      integer(4), parameter :: nf_szip_nn_option_mask = 32
      integer(4), parameter :: nf_mpiio = 8192
      integer(4), parameter :: nf_mpiposix = 16384
      integer(4), parameter :: nf_pnetcdf = 32768
      integer(4), parameter :: nf_independent = 0
      integer(4), parameter :: nf_collective = 1
      integer(4), parameter :: nf_ehdferr = -101
      integer(4), parameter :: nf_ecantread = -102
      integer(4), parameter :: nf_ecantwrite = -103
      integer(4), parameter :: nf_ecantcreate = -104
      integer(4), parameter :: nf_efilemeta = -105
      integer(4), parameter :: nf_edimmeta = -106
      integer(4), parameter :: nf_eattmeta = -107
      integer(4), parameter :: nf_evarmeta = -108
      integer(4), parameter :: nf_enocompound = -109
      integer(4), parameter :: nf_eattexists = -110
      integer(4), parameter :: nf_enotnc4 = -111
      integer(4), parameter :: nf_estrictnc3 = -112
      integer(4), parameter :: nf_enotnc3 = -113
      integer(4), parameter :: nf_enopar = -114
      integer(4), parameter :: nf_eparinit = -115
      integer(4), parameter :: nf_ebadgrpid = -116
      integer(4), parameter :: nf_ebadtypid = -117
      integer(4), parameter :: nf_etypdefined = -118
      integer(4), parameter :: nf_ebadfield = -119
      integer(4), parameter :: nf_ebadclass = -120
      integer(4), parameter :: nf_emaptype = -121
      integer(4), parameter :: nf_elatefill = -122
      integer(4), parameter :: nf_elatedef = -123
      integer(4), parameter :: nf_edimscale = -124
      integer(4), parameter :: nf_enogrp = -125
      integer(4), parameter :: ncbyte = 1
      integer(4), parameter :: ncchar = 2
      integer(4), parameter :: ncshort = 3
      integer(4), parameter :: nclong = 4
      integer(4), parameter :: ncfloat = 5
      integer(4), parameter :: ncdouble = 6
      integer(4), parameter :: ncrdwr = 1
      integer(4), parameter :: nccreat = 2
      integer(4), parameter :: ncexcl = 4
      integer(4), parameter :: ncindef = 8
      integer(4), parameter :: ncnsync = 16
      integer(4), parameter :: nchsync = 32
      integer(4), parameter :: ncndirty = 64
      integer(4), parameter :: nchdirty = 128
      integer(4), parameter :: ncfill = 0
      integer(4), parameter :: ncnofill = 256
      integer(4), parameter :: nclink = 32768
      integer(4), parameter :: ncnowrit = 0
      integer(4), parameter :: ncwrite = ncrdwr
      integer(4), parameter :: ncclob = nf_clobber
      integer(4), parameter :: ncnoclob = nf_noclobber
      integer(4), parameter :: ncunlim = 0
      integer(4), parameter :: ncglobal = 0
      integer(4), parameter :: maxncop = 64
      integer(4), parameter :: maxncdim = 1024
      integer(4), parameter :: maxncatt = 8192
      integer(4), parameter :: maxncvar = 8192
      integer(4), parameter :: maxncnam = 256
      integer(4), parameter :: maxvdims = maxncdim
      integer(4), parameter :: ncnoerr = nf_noerr
      integer(4), parameter :: ncebadid = nf_ebadid
      integer(4), parameter :: ncenfile = -31
      integer(4), parameter :: nceexist = nf_eexist
      integer(4), parameter :: nceinval = nf_einval
      integer(4), parameter :: nceperm = nf_eperm
      integer(4), parameter :: ncenotin = nf_enotindefine
      integer(4), parameter :: nceindef = nf_eindefine
      integer(4), parameter :: ncecoord = nf_einvalcoords
      integer(4), parameter :: ncemaxds = nf_emaxdims
      integer(4), parameter :: ncename = nf_enameinuse
      integer(4), parameter :: ncenoatt = nf_enotatt
      integer(4), parameter :: ncemaxat = nf_emaxatts
      integer(4), parameter :: ncebadty = nf_ebadtype
      integer(4), parameter :: ncebadd = nf_ebaddim
      integer(4), parameter :: nceunlim = nf_eunlimpos
      integer(4), parameter :: ncemaxvs = nf_emaxvars
      integer(4), parameter :: ncenotvr = nf_enotvar
      integer(4), parameter :: nceglob = nf_eglobal
      integer(4), parameter :: ncenotnc = nf_enotnc
      integer(4), parameter :: ncests = nf_ests
      integer(4), parameter :: ncentool = nf_emaxname
      integer(4), parameter :: ncfoobar = 32
      integer(4), parameter :: ncsyserr = -31
      integer(4), parameter :: ncfatal = 1
      integer(4), parameter :: ncverbos = 2
      integer(4), parameter :: filbyte = -127
      integer(4), parameter :: filchar = 0
      integer(4), parameter :: filshort = -32767
      integer(4), parameter :: fillong = -2147483647
      real, parameter :: filfloat = 9.9692099683868690D+36
      real(8), parameter :: fildoub = 9.9692099683868690D+36
      integer(4) :: hisShflx_sen
      integer(4) :: hisShflx_lat
      integer(4) :: hisShflx_rlw
      integer(4) :: hisShflx_rsw
      integer(4) :: hisSwflx
      integer(4) :: hisShflx
      integer(4) :: hisHbbl
      integer(4) :: hisHbl
      integer(4) :: hisAks
      integer(4) :: hisAkt
      integer(4) :: hisAkv
      integer(4) :: hisDiff
      integer(4) :: hisVWstr
      integer(4) :: hisUWstr
      integer(4) :: hisWstr
      integer(4) :: hisBostr
      integer(4) :: hisW
      integer(4) :: hisO
      integer(4) :: hisR
      integer(4) :: hisV
      integer(4) :: hisU
      integer(4) :: hisVb
      integer(4) :: hisUb
      integer(4) :: hisZ
      integer(4) :: hisTime2
      integer(4) :: hisTime
      integer(4) :: hisTstep
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: mynode
      logical :: ldefhis
      integer(4) :: nrpfhis
      character(128) :: hisname
      integer(4), dimension(1:NT) :: hisT
      logical, dimension(1:500+NT) :: wrthis
      character(75), dimension(1:20,1:500) :: vname
      integer(4) :: ncid
      integer(4) :: total_rec
      integer(4) :: ierr
        end subroutine Sub_Loop_def_his

      end interface
      integer(4) :: ncid
      integer(4) :: total_rec
      integer(4) :: ierr
      

        call Sub_Loop_def_his(ncid,total_rec,ierr, Agrif_tabvars_i(82)%i
     &array0, Agrif_tabvars_i(83)%iarray0, Agrif_tabvars_i(84)%iarray0, 
     &Agrif_tabvars_i(98)%iarray0, Agrif_tabvars_i(99)%iarray0, Agrif_ta
     &bvars_i(100)%iarray0, Agrif_tabvars_i(85)%iarray0, Agrif_tabvars_i
     &(86)%iarray0, Agrif_tabvars_i(87)%iarray0, Agrif_tabvars_i(88)%iar
     &ray0, Agrif_tabvars_i(89)%iarray0, Agrif_tabvars_i(90)%iarray0, Ag
     &rif_tabvars_i(101)%iarray0, Agrif_tabvars_i(102)%iarray0, Agrif_ta
     &bvars_i(103)%iarray0, Agrif_tabvars_i(104)%iarray0, Agrif_tabvars_
     &i(92)%iarray0, Agrif_tabvars_i(93)%iarray0, Agrif_tabvars_i(94)%ia
     &rray0, Agrif_tabvars_i(95)%iarray1, Agrif_tabvars_i(96)%iarray0, A
     &grif_tabvars_i(97)%iarray0, Agrif_tabvars_i(105)%iarray0, Agrif_ta
     &bvars_i(106)%iarray0, Agrif_tabvars_i(107)%iarray0, Agrif_tabvars_
     &l(3)%larray1, Agrif_tabvars_i(109)%iarray0, Agrif_tabvars_i(110)%i
     &array0, Agrif_tabvars_c(1)%carray2, Agrif_tabvars_i(108)%iarray0, 
     &Agrif_tabvars_i(142)%iarray0, Agrif_tabvars_i(143)%iarray0, Agrif_
     &tabvars_i(144)%iarray0, Agrif_tabvars_i(145)%iarray0, Agrif_tabvar
     &s_i(156)%iarray0, Agrif_tabvars_l(9)%larray0, Agrif_tabvars_i(111)
     &%iarray0, Agrif_tabvars_c(10)%carray0)

      end


      subroutine Sub_Loop_def_his(ncid,total_rec,ierr,hisShflx_sen,hisSh
     &flx_lat,hisShflx_rlw,hisShflx_rsw,hisSwflx,hisShflx,hisHbbl,hisHbl
     &,hisAks,hisAkt,hisAkv,hisDiff,hisVWstr,hisUWstr,hisWstr,hisBostr,h
     &isW,hisO,hisR,hisT,hisV,hisU,hisVb,hisUb,hisZ,wrthis,hisTime2,hisT
     &ime,vname,hisTstep,eta_v,eta_rho,xi_u,xi_rho,mynode,ldefhis,nrpfhi
     &s,hisname)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4), parameter :: nf_byte = 1
      integer(4), parameter :: nf_int1 = nf_byte
      integer(4), parameter :: nf_char = 2
      integer(4), parameter :: nf_short = 3
      integer(4), parameter :: nf_int2 = nf_short
      integer(4), parameter :: nf_int = 4
      integer(4), parameter :: nf_float = 5
      integer(4), parameter :: nf_real = nf_float
      integer(4), parameter :: nf_double = 6
      integer(4), parameter :: nf_fill_byte = -127
      integer(4), parameter :: nf_fill_int1 = nf_fill_byte
      integer(4), parameter :: nf_fill_char = 0
      integer(4), parameter :: nf_fill_short = -32767
      integer(4), parameter :: nf_fill_int2 = nf_fill_short
      integer(4), parameter :: nf_fill_int = -2147483647
      real, parameter :: nf_fill_float = 9.9692099683868690D+36
      real, parameter :: nf_fill_real = nf_fill_float
      real(8), parameter :: nf_fill_double = 9.9692099683868690D+36
      integer(4), parameter :: nf_nowrite = 0
      integer(4), parameter :: nf_write = 1
      integer(4), parameter :: nf_clobber = 0
      integer(4), parameter :: nf_noclobber = 4
      integer(4), parameter :: nf_fill = 0
      integer(4), parameter :: nf_nofill = 256
      integer(4), parameter :: nf_lock = 1024
      integer(4), parameter :: nf_share = 2048
      integer(4), parameter :: nf_64bit_offset = 512
      integer(4), parameter :: nf_sizehint_default = 0
      integer(4), parameter :: nf_align_chunk = -1
      integer(4), parameter :: nf_format_classic = 1
      integer(4), parameter :: nf_format_64bit = 2
      integer(4), parameter :: nf_diskless = 8
      integer(4), parameter :: nf_mmap = 16
      integer(4), parameter :: nf_unlimited = 0
      integer(4), parameter :: nf_global = 0
      integer(4), parameter :: nf_max_dims = 1024
      integer(4), parameter :: nf_max_attrs = 8192
      integer(4), parameter :: nf_max_vars = 8192
      integer(4), parameter :: nf_max_name = 256
      integer(4), parameter :: nf_max_var_dims = nf_max_dims
      integer(4), parameter :: nf_noerr = 0
      integer(4), parameter :: nf_ebadid = -33
      integer(4), parameter :: nf_eexist = -35
      integer(4), parameter :: nf_einval = -36
      integer(4), parameter :: nf_eperm = -37
      integer(4), parameter :: nf_enotindefine = -38
      integer(4), parameter :: nf_eindefine = -39
      integer(4), parameter :: nf_einvalcoords = -40
      integer(4), parameter :: nf_emaxdims = -41
      integer(4), parameter :: nf_enameinuse = -42
      integer(4), parameter :: nf_enotatt = -43
      integer(4), parameter :: nf_emaxatts = -44
      integer(4), parameter :: nf_ebadtype = -45
      integer(4), parameter :: nf_ebaddim = -46
      integer(4), parameter :: nf_eunlimpos = -47
      integer(4), parameter :: nf_emaxvars = -48
      integer(4), parameter :: nf_enotvar = -49
      integer(4), parameter :: nf_eglobal = -50
      integer(4), parameter :: nf_enotnc = -51
      integer(4), parameter :: nf_ests = -52
      integer(4), parameter :: nf_emaxname = -53
      integer(4), parameter :: nf_eunlimit = -54
      integer(4), parameter :: nf_enorecvars = -55
      integer(4), parameter :: nf_echar = -56
      integer(4), parameter :: nf_eedge = -57
      integer(4), parameter :: nf_estride = -58
      integer(4), parameter :: nf_ebadname = -59
      integer(4), parameter :: nf_erange = -60
      integer(4), parameter :: nf_enomem = -61
      integer(4), parameter :: nf_evarsize = -62
      integer(4), parameter :: nf_edimsize = -63
      integer(4), parameter :: nf_etrunc = -64
      integer(4), parameter :: nf_fatal = 1
      integer(4), parameter :: nf_verbose = 2
      integer(4), parameter :: nf_ubyte = 7
      integer(4), parameter :: nf_ushort = 8
      integer(4), parameter :: nf_uint = 9
      integer(4), parameter :: nf_int64 = 10
      integer(4), parameter :: nf_uint64 = 11
      integer(4), parameter :: nf_string = 12
      integer(4), parameter :: nf_vlen = 13
      integer(4), parameter :: nf_opaque = 14
      integer(4), parameter :: nf_enum = 15
      integer(4), parameter :: nf_compound = 16
      integer(4), parameter :: nf_fill_ubyte = 255
      integer(4), parameter :: nf_fill_ushort = 65535
      integer(4), parameter :: nf_format_netcdf4 = 3
      integer(4), parameter :: nf_format_netcdf4_classic = 4
      integer(4), parameter :: nf_netcdf4 = 4096
      integer(4), parameter :: nf_classic_model = 256
      integer(4), parameter :: nf_chunk_seq = 0
      integer(4), parameter :: nf_chunk_sub = 1
      integer(4), parameter :: nf_chunk_sizes = 2
      integer(4), parameter :: nf_endian_native = 0
      integer(4), parameter :: nf_endian_little = 1
      integer(4), parameter :: nf_endian_big = 2
      integer(4), parameter :: nf_chunked = 0
      integer(4), parameter :: nf_contiguous = 1
      integer(4), parameter :: nf_nochecksum = 0
      integer(4), parameter :: nf_fletcher32 = 1
      integer(4), parameter :: nf_noshuffle = 0
      integer(4), parameter :: nf_shuffle = 1
      integer(4), parameter :: nf_szip_ec_option_mask = 4
      integer(4), parameter :: nf_szip_nn_option_mask = 32
      integer(4), parameter :: nf_mpiio = 8192
      integer(4), parameter :: nf_mpiposix = 16384
      integer(4), parameter :: nf_pnetcdf = 32768
      integer(4), parameter :: nf_independent = 0
      integer(4), parameter :: nf_collective = 1
      integer(4), parameter :: nf_ehdferr = -101
      integer(4), parameter :: nf_ecantread = -102
      integer(4), parameter :: nf_ecantwrite = -103
      integer(4), parameter :: nf_ecantcreate = -104
      integer(4), parameter :: nf_efilemeta = -105
      integer(4), parameter :: nf_edimmeta = -106
      integer(4), parameter :: nf_eattmeta = -107
      integer(4), parameter :: nf_evarmeta = -108
      integer(4), parameter :: nf_enocompound = -109
      integer(4), parameter :: nf_eattexists = -110
      integer(4), parameter :: nf_enotnc4 = -111
      integer(4), parameter :: nf_estrictnc3 = -112
      integer(4), parameter :: nf_enotnc3 = -113
      integer(4), parameter :: nf_enopar = -114
      integer(4), parameter :: nf_eparinit = -115
      integer(4), parameter :: nf_ebadgrpid = -116
      integer(4), parameter :: nf_ebadtypid = -117
      integer(4), parameter :: nf_etypdefined = -118
      integer(4), parameter :: nf_ebadfield = -119
      integer(4), parameter :: nf_ebadclass = -120
      integer(4), parameter :: nf_emaptype = -121
      integer(4), parameter :: nf_elatefill = -122
      integer(4), parameter :: nf_elatedef = -123
      integer(4), parameter :: nf_edimscale = -124
      integer(4), parameter :: nf_enogrp = -125
      integer(4), parameter :: ncbyte = 1
      integer(4), parameter :: ncchar = 2
      integer(4), parameter :: ncshort = 3
      integer(4), parameter :: nclong = 4
      integer(4), parameter :: ncfloat = 5
      integer(4), parameter :: ncdouble = 6
      integer(4), parameter :: ncrdwr = 1
      integer(4), parameter :: nccreat = 2
      integer(4), parameter :: ncexcl = 4
      integer(4), parameter :: ncindef = 8
      integer(4), parameter :: ncnsync = 16
      integer(4), parameter :: nchsync = 32
      integer(4), parameter :: ncndirty = 64
      integer(4), parameter :: nchdirty = 128
      integer(4), parameter :: ncfill = 0
      integer(4), parameter :: ncnofill = 256
      integer(4), parameter :: nclink = 32768
      integer(4), parameter :: ncnowrit = 0
      integer(4), parameter :: ncwrite = ncrdwr
      integer(4), parameter :: ncclob = nf_clobber
      integer(4), parameter :: ncnoclob = nf_noclobber
      integer(4), parameter :: ncunlim = 0
      integer(4), parameter :: ncglobal = 0
      integer(4), parameter :: maxncop = 64
      integer(4), parameter :: maxncdim = 1024
      integer(4), parameter :: maxncatt = 8192
      integer(4), parameter :: maxncvar = 8192
      integer(4), parameter :: maxncnam = 256
      integer(4), parameter :: maxvdims = maxncdim
      integer(4), parameter :: ncnoerr = nf_noerr
      integer(4), parameter :: ncebadid = nf_ebadid
      integer(4), parameter :: ncenfile = -31
      integer(4), parameter :: nceexist = nf_eexist
      integer(4), parameter :: nceinval = nf_einval
      integer(4), parameter :: nceperm = nf_eperm
      integer(4), parameter :: ncenotin = nf_enotindefine
      integer(4), parameter :: nceindef = nf_eindefine
      integer(4), parameter :: ncecoord = nf_einvalcoords
      integer(4), parameter :: ncemaxds = nf_emaxdims
      integer(4), parameter :: ncename = nf_enameinuse
      integer(4), parameter :: ncenoatt = nf_enotatt
      integer(4), parameter :: ncemaxat = nf_emaxatts
      integer(4), parameter :: ncebadty = nf_ebadtype
      integer(4), parameter :: ncebadd = nf_ebaddim
      integer(4), parameter :: nceunlim = nf_eunlimpos
      integer(4), parameter :: ncemaxvs = nf_emaxvars
      integer(4), parameter :: ncenotvr = nf_enotvar
      integer(4), parameter :: nceglob = nf_eglobal
      integer(4), parameter :: ncenotnc = nf_enotnc
      integer(4), parameter :: ncests = nf_ests
      integer(4), parameter :: ncentool = nf_emaxname
      integer(4), parameter :: ncfoobar = 32
      integer(4), parameter :: ncsyserr = -31
      integer(4), parameter :: ncfatal = 1
      integer(4), parameter :: ncverbos = 2
      integer(4), parameter :: filbyte = -127
      integer(4), parameter :: filchar = 0
      integer(4), parameter :: filshort = -32767
      integer(4), parameter :: fillong = -2147483647
      real, parameter :: filfloat = 9.9692099683868690D+36
      real(8), parameter :: fildoub = 9.9692099683868690D+36
      integer(4) :: hisShflx_sen
      integer(4) :: hisShflx_lat
      integer(4) :: hisShflx_rlw
      integer(4) :: hisShflx_rsw
      integer(4) :: hisSwflx
      integer(4) :: hisShflx
      integer(4) :: hisHbbl
      integer(4) :: hisHbl
      integer(4) :: hisAks
      integer(4) :: hisAkt
      integer(4) :: hisAkv
      integer(4) :: hisDiff
      integer(4) :: hisVWstr
      integer(4) :: hisUWstr
      integer(4) :: hisWstr
      integer(4) :: hisBostr
      integer(4) :: hisW
      integer(4) :: hisO
      integer(4) :: hisR
      integer(4) :: hisV
      integer(4) :: hisU
      integer(4) :: hisVb
      integer(4) :: hisUb
      integer(4) :: hisZ
      integer(4) :: hisTime2
      integer(4) :: hisTime
      integer(4) :: hisTstep
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: mynode
      logical :: ldefhis
      integer(4) :: nrpfhis
      character(128) :: hisname
      integer(4), dimension(1:NT) :: hisT
      logical, dimension(1:500+NT) :: wrthis
      character(75), dimension(1:20,1:500) :: vname
      integer(4) :: ncid
      integer(4) :: total_rec
      integer(4) :: ierr

                   

                              
      logical :: create_new_file
                                                                       
                                                                 
                                                                 
                                                                 
                         
      integer(4) :: rec
      integer(4) :: lstr
      integer(4) :: lvar
      integer(4) :: lenstr
      integer(4) :: timedim
      integer(4), dimension(1:3) :: r2dgrd
      integer(4), dimension(1:3) :: u2dgrd
      integer(4), dimension(1:3) :: v2dgrd
      integer(4), dimension(1:2) :: auxil
      integer(4) :: checkdims
      integer(4), dimension(1:4) :: b3dgrd
      integer(4), dimension(1:4) :: r3dgrd
      integer(4), dimension(1:4) :: u3dgrd
      integer(4), dimension(1:4) :: v3dgrd
      integer(4), dimension(1:4) :: w3dgrd
      integer(4) :: itrc
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                        
                        
                        
                         
                        
                       
                         
                        
                          
                             

                                   

                             

                              

                                    

                            

                              

                                    

                               

                                       
                                       
                                       
                                        
                                       
                                      
                                      
                                     
                                       
                                     

                                             

                                  

                                        

                                              

                                           

                                                        

                                              

                                                         

                           
                         
                           
                             
                        
                          
                        
                         
                                
                                    
                               
                                  
                                
                            
                        
                                

                              

                                

                                  

                             

                                 

                                

                                 

                                       

                                         

                                     

                                       

                                     

                                 

                              

                             
                                  

                          
                               

                            
                             
                            
                            
                                
                                    

                                     

                                    

                                   

                                               

                         
                          
                          
                          
                         
                                
                             
                                
                            
                              
                           
                            
                            
                           
                             
                            
                           
                           
                          
                        
                            
                            
                              
                         
                         
                           
                            
                          
                          
                            
                            
                          
                              

                                 

                                 

                                 

                                

                                       

                                    

                                       

                                   

                                     

                                  

                                   

                                   

                                  

                                    

                                   

                                  

                                  

                                 

                               

                                   

                                   

                                     

                                

                                

                                  

                                   

                                 

                                 

                                   

                                   

                                 

                          
                           
                              

                                

                                    
      character(80) :: nf_inq_libvers
      external       nf_inq_libvers
                                 
      character(80) :: nf_strerror
      external       nf_strerror
                                 
      logical :: nf_issyserr
      external       nf_issyserr
                                       
      integer(4) :: nf_inq_base_pe
      external        nf_inq_base_pe
                                       
      integer(4) :: nf_set_base_pe
      external        nf_set_base_pe
                                  
      integer(4) :: nf_create
      external        nf_create
                                   
      integer(4) :: nf__create
      external        nf__create
                                      
      integer(4) :: nf__create_mp
      external        nf__create_mp
                                
      integer(4) :: nf_open
      external        nf_open
                                 
      integer(4) :: nf__open
      external        nf__open
                                    
      integer(4) :: nf__open_mp
      external        nf__open_mp
                                    
      integer(4) :: nf_set_fill
      external        nf_set_fill
                                              
      integer(4) :: nf_set_default_format
      external        nf_set_default_format
                                 
      integer(4) :: nf_redef
      external        nf_redef
                                  
      integer(4) :: nf_enddef
      external        nf_enddef
                                   
      integer(4) :: nf__enddef
      external        nf__enddef
                                
      integer(4) :: nf_sync
      external        nf_sync
                                 
      integer(4) :: nf_abort
      external        nf_abort
                                 
      integer(4) :: nf_close
      external        nf_close
                                  
      integer(4) :: nf_delete
      external        nf_delete
                               
      integer(4) :: nf_inq
      external        nf_inq
                            
      integer(4) :: nf_inq_path
      external nf_inq_path
                                     
      integer(4) :: nf_inq_ndims
      external        nf_inq_ndims
                                     
      integer(4) :: nf_inq_nvars
      external        nf_inq_nvars
                                     
      integer(4) :: nf_inq_natts
      external        nf_inq_natts
                                        
      integer(4) :: nf_inq_unlimdim
      external        nf_inq_unlimdim
                                      
      integer(4) :: nf_inq_format
      external        nf_inq_format
                                   
      integer(4) :: nf_def_dim
      external        nf_def_dim
                                     
      integer(4) :: nf_inq_dimid
      external        nf_inq_dimid
                                   
      integer(4) :: nf_inq_dim
      external        nf_inq_dim
                                       
      integer(4) :: nf_inq_dimname
      external        nf_inq_dimname
                                      
      integer(4) :: nf_inq_dimlen
      external        nf_inq_dimlen
                                      
      integer(4) :: nf_rename_dim
      external        nf_rename_dim
                                   
      integer(4) :: nf_inq_att
      external        nf_inq_att
                                     
      integer(4) :: nf_inq_attid
      external        nf_inq_attid
                                       
      integer(4) :: nf_inq_atttype
      external        nf_inq_atttype
                                      
      integer(4) :: nf_inq_attlen
      external        nf_inq_attlen
                                       
      integer(4) :: nf_inq_attname
      external        nf_inq_attname
                                    
      integer(4) :: nf_copy_att
      external        nf_copy_att
                                      
      integer(4) :: nf_rename_att
      external        nf_rename_att
                                   
      integer(4) :: nf_del_att
      external        nf_del_att
                                        
      integer(4) :: nf_put_att_text
      external        nf_put_att_text
                                        
      integer(4) :: nf_get_att_text
      external        nf_get_att_text
                                        
      integer(4) :: nf_put_att_int1
      external        nf_put_att_int1
                                        
      integer(4) :: nf_get_att_int1
      external        nf_get_att_int1
                                        
      integer(4) :: nf_put_att_int2
      external        nf_put_att_int2
                                        
      integer(4) :: nf_get_att_int2
      external        nf_get_att_int2
                                       
      integer(4) :: nf_put_att_int
      external        nf_put_att_int
                                       
      integer(4) :: nf_get_att_int
      external        nf_get_att_int
                                        
      integer(4) :: nf_put_att_real
      external        nf_put_att_real
                                        
      integer(4) :: nf_get_att_real
      external        nf_get_att_real
                                          
      integer(4) :: nf_put_att_double
      external        nf_put_att_double
                                          
      integer(4) :: nf_get_att_double
      external        nf_get_att_double
                                   
      integer(4) :: nf_def_var
      external        nf_def_var
                                   
      integer(4) :: nf_inq_var
      external        nf_inq_var
                                     
      integer(4) :: nf_inq_varid
      external        nf_inq_varid
                                       
      integer(4) :: nf_inq_varname
      external        nf_inq_varname
                                       
      integer(4) :: nf_inq_vartype
      external        nf_inq_vartype
                                        
      integer(4) :: nf_inq_varndims
      external        nf_inq_varndims
                                        
      integer(4) :: nf_inq_vardimid
      external        nf_inq_vardimid
                                        
      integer(4) :: nf_inq_varnatts
      external        nf_inq_varnatts
                                      
      integer(4) :: nf_rename_var
      external        nf_rename_var
                                    
      integer(4) :: nf_copy_var
      external        nf_copy_var
                                        
      integer(4) :: nf_put_var_text
      external        nf_put_var_text
                                        
      integer(4) :: nf_get_var_text
      external        nf_get_var_text
                                        
      integer(4) :: nf_put_var_int1
      external        nf_put_var_int1
                                        
      integer(4) :: nf_get_var_int1
      external        nf_get_var_int1
                                        
      integer(4) :: nf_put_var_int2
      external        nf_put_var_int2
                                        
      integer(4) :: nf_get_var_int2
      external        nf_get_var_int2
                                       
      integer(4) :: nf_put_var_int
      external        nf_put_var_int
                                       
      integer(4) :: nf_get_var_int
      external        nf_get_var_int
                                        
      integer(4) :: nf_put_var_real
      external        nf_put_var_real
                                        
      integer(4) :: nf_get_var_real
      external        nf_get_var_real
                                          
      integer(4) :: nf_put_var_double
      external        nf_put_var_double
                                          
      integer(4) :: nf_get_var_double
      external        nf_get_var_double
                                         
      integer(4) :: nf_put_var1_text
      external        nf_put_var1_text
                                         
      integer(4) :: nf_get_var1_text
      external        nf_get_var1_text
                                         
      integer(4) :: nf_put_var1_int1
      external        nf_put_var1_int1
                                         
      integer(4) :: nf_get_var1_int1
      external        nf_get_var1_int1
                                         
      integer(4) :: nf_put_var1_int2
      external        nf_put_var1_int2
                                         
      integer(4) :: nf_get_var1_int2
      external        nf_get_var1_int2
                                        
      integer(4) :: nf_put_var1_int
      external        nf_put_var1_int
                                        
      integer(4) :: nf_get_var1_int
      external        nf_get_var1_int
                                         
      integer(4) :: nf_put_var1_real
      external        nf_put_var1_real
                                         
      integer(4) :: nf_get_var1_real
      external        nf_get_var1_real
                                           
      integer(4) :: nf_put_var1_double
      external        nf_put_var1_double
                                           
      integer(4) :: nf_get_var1_double
      external        nf_get_var1_double
                                         
      integer(4) :: nf_put_vara_text
      external        nf_put_vara_text
                                         
      integer(4) :: nf_get_vara_text
      external        nf_get_vara_text
                                         
      integer(4) :: nf_put_vara_int1
      external        nf_put_vara_int1
                                         
      integer(4) :: nf_get_vara_int1
      external        nf_get_vara_int1
                                         
      integer(4) :: nf_put_vara_int2
      external        nf_put_vara_int2
                                         
      integer(4) :: nf_get_vara_int2
      external        nf_get_vara_int2
                                        
      integer(4) :: nf_put_vara_int
      external        nf_put_vara_int
                                        
      integer(4) :: nf_get_vara_int
      external        nf_get_vara_int
                                         
      integer(4) :: nf_put_vara_real
      external        nf_put_vara_real
                                         
      integer(4) :: nf_get_vara_real
      external        nf_get_vara_real
                                           
      integer(4) :: nf_put_vara_double
      external        nf_put_vara_double
                                           
      integer(4) :: nf_get_vara_double
      external        nf_get_vara_double
                                         
      integer(4) :: nf_put_vars_text
      external        nf_put_vars_text
                                         
      integer(4) :: nf_get_vars_text
      external        nf_get_vars_text
                                         
      integer(4) :: nf_put_vars_int1
      external        nf_put_vars_int1
                                         
      integer(4) :: nf_get_vars_int1
      external        nf_get_vars_int1
                                         
      integer(4) :: nf_put_vars_int2
      external        nf_put_vars_int2
                                         
      integer(4) :: nf_get_vars_int2
      external        nf_get_vars_int2
                                        
      integer(4) :: nf_put_vars_int
      external        nf_put_vars_int
                                        
      integer(4) :: nf_get_vars_int
      external        nf_get_vars_int
                                         
      integer(4) :: nf_put_vars_real
      external        nf_put_vars_real
                                         
      integer(4) :: nf_get_vars_real
      external        nf_get_vars_real
                                           
      integer(4) :: nf_put_vars_double
      external        nf_put_vars_double
                                           
      integer(4) :: nf_get_vars_double
      external        nf_get_vars_double
                                         
      integer(4) :: nf_put_varm_text
      external        nf_put_varm_text
                                         
      integer(4) :: nf_get_varm_text
      external        nf_get_varm_text
                                         
      integer(4) :: nf_put_varm_int1
      external        nf_put_varm_int1
                                         
      integer(4) :: nf_get_varm_int1
      external        nf_get_varm_int1
                                         
      integer(4) :: nf_put_varm_int2
      external        nf_put_varm_int2
                                         
      integer(4) :: nf_get_varm_int2
      external        nf_get_varm_int2
                                        
      integer(4) :: nf_put_varm_int
      external        nf_put_varm_int
                                        
      integer(4) :: nf_get_varm_int
      external        nf_get_varm_int
                                         
      integer(4) :: nf_put_varm_real
      external        nf_put_varm_real
                                         
      integer(4) :: nf_get_varm_real
      external        nf_get_varm_real
                                           
      integer(4) :: nf_put_varm_double
      external        nf_put_varm_double
                                           
      integer(4) :: nf_get_varm_double
      external        nf_get_varm_double
                         
                          
                        
                         
                          
                          
                        
                          
                        
                            
                              

                               

                             

                               

                                

                                

                              

                                

                              

                                  

                                        
                                         
                                     

                                        

                                  
                                       

                                          
                                               

                           
                                   

                                 
                                        

                             
                                  

                             
                                  

                               
                                    

                                 
                                      

                                 
                                      

                              
                                   

                           
                                

                              
                                   

                              
                                   

                              
                                   

                             
                                  

                           
                                

                                       
                                            

                                       
                                             

                         
                                 

                            
                                     

                           
                                    

                               
                                    

                              
                                   

                           
                                   

                             
                                     

                              
                                      

                               
                                       

                             
                                     

                            
                                    

                            
                                    

                            
                                    

                               
                                       

                              
                                      

                           
                                   

                              
                                      

                           
                                   

                          
                                  

                            
                                    

                             
                                     

                             
                                     

                               
                                       

                             
                                     

                             
                                     

                            
                                    

                             
                                     

                            
                                    

                             
                                     

                          
                                  

                              
      integer(4) :: nf_create_par
      external nf_create_par
                            
      integer(4) :: nf_open_par
      external nf_open_par
                                  
      integer(4) :: nf_var_par_access
      external nf_var_par_access
                            
      integer(4) :: nf_inq_ncid
      external nf_inq_ncid
                            
      integer(4) :: nf_inq_grps
      external nf_inq_grps
                               
      integer(4) :: nf_inq_grpname
      external nf_inq_grpname
                                    
      integer(4) :: nf_inq_grpname_full
      external nf_inq_grpname_full
                                   
      integer(4) :: nf_inq_grpname_len
      external nf_inq_grpname_len
                                  
      integer(4) :: nf_inq_grp_parent
      external nf_inq_grp_parent
                                
      integer(4) :: nf_inq_grp_ncid
      external nf_inq_grp_ncid
                                     
      integer(4) :: nf_inq_grp_full_ncid
      external nf_inq_grp_full_ncid
                              
      integer(4) :: nf_inq_varids
      external nf_inq_varids
                              
      integer(4) :: nf_inq_dimids
      external nf_inq_dimids
                           
      integer(4) :: nf_def_grp
      external nf_def_grp
                              
      integer(4) :: nf_rename_grp
      external nf_rename_grp
                                   
      integer(4) :: nf_def_var_deflate
      external nf_def_var_deflate
                                   
      integer(4) :: nf_inq_var_deflate
      external nf_inq_var_deflate
                                      
      integer(4) :: nf_def_var_fletcher32
      external nf_def_var_fletcher32
                                      
      integer(4) :: nf_inq_var_fletcher32
      external nf_inq_var_fletcher32
                                    
      integer(4) :: nf_def_var_chunking
      external nf_def_var_chunking
                                    
      integer(4) :: nf_inq_var_chunking
      external nf_inq_var_chunking
                                
      integer(4) :: nf_def_var_fill
      external nf_def_var_fill
                                
      integer(4) :: nf_inq_var_fill
      external nf_inq_var_fill
                                  
      integer(4) :: nf_def_var_endian
      external nf_def_var_endian
                                  
      integer(4) :: nf_inq_var_endian
      external nf_inq_var_endian
                               
      integer(4) :: nf_inq_typeids
      external nf_inq_typeids
                              
      integer(4) :: nf_inq_typeid
      external nf_inq_typeid
                            
      integer(4) :: nf_inq_type
      external nf_inq_type
                                 
      integer(4) :: nf_inq_user_type
      external nf_inq_user_type
                                
      integer(4) :: nf_def_compound
      external nf_def_compound
                                   
      integer(4) :: nf_insert_compound
      external nf_insert_compound
                                         
      integer(4) :: nf_insert_array_compound
      external nf_insert_array_compound
                                
      integer(4) :: nf_inq_compound
      external nf_inq_compound
                                     
      integer(4) :: nf_inq_compound_name
      external nf_inq_compound_name
                                     
      integer(4) :: nf_inq_compound_size
      external nf_inq_compound_size
                                        
      integer(4) :: nf_inq_compound_nfields
      external nf_inq_compound_nfields
                                      
      integer(4) :: nf_inq_compound_field
      external nf_inq_compound_field
                                          
      integer(4) :: nf_inq_compound_fieldname
      external nf_inq_compound_fieldname
                                           
      integer(4) :: nf_inq_compound_fieldindex
      external nf_inq_compound_fieldindex
                                            
      integer(4) :: nf_inq_compound_fieldoffset
      external nf_inq_compound_fieldoffset
                                          
      integer(4) :: nf_inq_compound_fieldtype
      external nf_inq_compound_fieldtype
                                           
      integer(4) :: nf_inq_compound_fieldndims
      external nf_inq_compound_fieldndims
                                               
      integer(4) :: nf_inq_compound_fielddim_sizes
      external nf_inq_compound_fielddim_sizes
                            
      integer(4) :: nf_def_vlen
      external nf_def_vlen
                            
      integer(4) :: nf_inq_vlen
      external nf_inq_vlen
                             
      integer(4) :: nf_free_vlen
      external nf_free_vlen
                            
      integer(4) :: nf_def_enum
      external nf_def_enum
                               
      integer(4) :: nf_insert_enum
      external nf_insert_enum
                            
      integer(4) :: nf_inq_enum
      external nf_inq_enum
                                   
      integer(4) :: nf_inq_enum_member
      external nf_inq_enum_member
                                  
      integer(4) :: nf_inq_enum_ident
      external nf_inq_enum_ident
                              
      integer(4) :: nf_def_opaque
      external nf_def_opaque
                              
      integer(4) :: nf_inq_opaque
      external nf_inq_opaque
                           
      integer(4) :: nf_put_att
      external nf_put_att
                           
      integer(4) :: nf_get_att
      external nf_get_att
                           
      integer(4) :: nf_put_var
      external nf_put_var
                            
      integer(4) :: nf_put_var1
      external nf_put_var1
                            
      integer(4) :: nf_put_vara
      external nf_put_vara
                            
      integer(4) :: nf_put_vars
      external nf_put_vars
                           
      integer(4) :: nf_get_var
      external nf_get_var
                            
      integer(4) :: nf_get_var1
      external nf_get_var1
                            
      integer(4) :: nf_get_vara
      external nf_get_vara
                            
      integer(4) :: nf_get_vars
      external nf_get_vars
                                  
      integer(4) :: nf_put_var1_int64
      external nf_put_var1_int64
                                  
      integer(4) :: nf_put_vara_int64
      external nf_put_vara_int64
                                  
      integer(4) :: nf_put_vars_int64
      external nf_put_vars_int64
                                  
      integer(4) :: nf_put_varm_int64
      external nf_put_varm_int64
                                 
      integer(4) :: nf_put_var_int64
      external nf_put_var_int64
                                  
      integer(4) :: nf_get_var1_int64
      external nf_get_var1_int64
                                  
      integer(4) :: nf_get_vara_int64
      external nf_get_vara_int64
                                  
      integer(4) :: nf_get_vars_int64
      external nf_get_vars_int64
                                  
      integer(4) :: nf_get_varm_int64
      external nf_get_varm_int64
                                 
      integer(4) :: nf_get_var_int64
      external nf_get_var_int64
                                    
      integer(4) :: nf_get_vlen_element
      external nf_get_vlen_element
                                    
      integer(4) :: nf_put_vlen_element
      external nf_put_vlen_element
                                   
      integer(4) :: nf_set_chunk_cache
      external nf_set_chunk_cache
                                   
      integer(4) :: nf_get_chunk_cache
      external nf_get_chunk_cache
                                       
      integer(4) :: nf_set_var_chunk_cache
      external nf_set_var_chunk_cache
                                       
      integer(4) :: nf_get_var_chunk_cache
      external nf_get_var_chunk_cache
                      
      integer(4) :: nccre
                      
      integer(4) :: ncopn
                       
      integer(4) :: ncddef
                      
      integer(4) :: ncdid
                       
      integer(4) :: ncvdef
                      
      integer(4) :: ncvid
                       
      integer(4) :: nctlen
                       
      integer(4) :: ncsfil
      external nccre
      external ncopn
      external ncddef
      external ncdid
      external ncvdef
      external ncvid
      external nctlen
      external ncsfil
                       
                        
                       
                        
                        
                        
                         
                         
                       
                         
                        
                       
                         
                         
                       
                         
                        
                         
                         
                         
                         
                         
                        
                         
                         
                         
                         
                        
                         
                         
                         
                         
                        
                         
                         
                         
                        
                       
                         
                         
                         
                        
                         
                         
                         
                        
                         
                         
                       
                       
                        
                       
                        
                         
                           

                           

                            

                           

                            

                             

                           

                            

                           

                            

                             

                             

                              

                               

                           

                               

                               

                             

                                 

                                    

                                        

                        
                            

                              

                             

                                

                                

                                

                               

                                    

                                   

                                     

                               

                                     

                                     

                                   

                                            

                                        

                                           

                                       

                                        

                                      

                                       

                                       

                                     

                                        

                                       

                                      

                                     

                                     

                                 

                                        

                              

                               

                            

                             

                        
                        
                         
                        
                    
                              
                                

                             

                                   

                                       

                                                   

                                                  

                        
      character(70) :: text
      ierr=0
      lstr=lenstr(hisname)
      if (nrpfhis.gt.0) then
        lvar=total_rec-(1+mod(total_rec-1, nrpfhis))
        call insert_time_index (hisname, lstr, lvar, ierr)
        if (ierr .ne. 0) goto 99
      endif
      create_new_file=ldefhis
      if (ncid.ne.-1) create_new_file=.false.
      if (mynode.gt.0) create_new_file=.false.
  10  if (create_new_file) then
        ierr=nf_create(hisname(1:lstr),nf_clobber, ncid)
        if (ierr .ne. nf_noerr) then
          write(stdout,'(/3(1x,A)/)') 'ERROR in def_his/avg:',
     &           'Cannot create netCDF file:', hisname(1:lstr)
          goto 99
        endif
        if (nrpfhis.eq.0) total_rec=0
        call put_global_atts (ncid, ierr)
        ierr=nf_def_dim (ncid, 'xi_rho',   xi_rho,   r2dgrd(1))
        ierr=nf_def_dim (ncid, 'xi_u',     xi_u,     u2dgrd(1))
        ierr=nf_def_dim (ncid, 'eta_rho',  eta_rho,  r2dgrd(2))
        ierr=nf_def_dim (ncid, 'eta_v',    eta_v,    v2dgrd(2))
        ierr=nf_def_dim (ncid, 's_rho',    N,        r3dgrd(3))
        ierr=nf_def_dim (ncid, 's_w',      N+1,      w3dgrd(3))
        ierr=nf_def_dim (ncid, 'time', nf_unlimited, timedim)
        ierr=nf_def_dim (ncid, 'auxil',    4,        auxil(1))
        auxil(2)=timedim
        r2dgrd(3)=timedim
        u2dgrd(2)=r2dgrd(2)
        u2dgrd(3)=timedim
        v2dgrd(1)=r2dgrd(1)
        v2dgrd(3)=timedim
        b3dgrd(1)=r2dgrd(1)
        b3dgrd(2)=r2dgrd(2)
        b3dgrd(4)=timedim
        r3dgrd(1)=r2dgrd(1)
        r3dgrd(2)=r2dgrd(2)
        r3dgrd(4)=timedim
        u3dgrd(1)=u2dgrd(1)
        u3dgrd(2)=r2dgrd(2)
        u3dgrd(3)=r3dgrd(3)
        u3dgrd(4)=timedim
        v3dgrd(1)=r2dgrd(1)
        v3dgrd(2)=v2dgrd(2)
        v3dgrd(3)=r3dgrd(3)
        v3dgrd(4)=timedim
        w3dgrd(1)=r2dgrd(1)
        w3dgrd(2)=r2dgrd(2)
        w3dgrd(4)=timedim
      if (total_rec.le.1) then
         call def_grid_3d(ncid, r2dgrd, u2dgrd, v2dgrd
     &                   ,r3dgrd, w3dgrd)
      endif
        ierr=nf_def_var (ncid, 'time_step', nf_int, 2, auxil,
     &                                                 hisTstep)
        ierr=nf_put_att_text (ncid, hisTstep, 'long_name', 48,
     &       'time step and record numbers from initialization')
        lvar=lenstr(vname(1,indxTime))
        ierr=nf_def_var (ncid, vname(1,indxTime)(1:lvar),
     &                            NF_REAL, 1, timedim, hisTime)
        lvar=lenstr(vname(2,indxTime))
        ierr=nf_put_att_text (ncid, hisTime, 'long_name', lvar,
     &                                vname(2,indxTime)(1:lvar))
        lvar=lenstr(vname(3,indxTime))
        ierr=nf_put_att_text (ncid, hisTime, 'units',  lvar,
     &                                vname(3,indxTime)(1:lvar))
        lvar=lenstr(vname(4,indxTime))
        ierr=nf_put_att_text (ncid, hisTime, 'field',  lvar,
     &                                vname(4,indxTime)(1:lvar))
        call nf_add_attribute(ncid, hisTime, indxTime, 5,
     &       NF_REAL, ierr)
        lvar=lenstr(vname(1,indxTime2))
        ierr=nf_def_var (ncid, vname(1,indxTime2)(1:lvar),
     &                            NF_REAL, 1, timedim, hisTime2)
        lvar=lenstr(vname(2,indxTime2))
        ierr=nf_put_att_text (ncid, hisTime2, 'long_name', lvar,
     &                                vname(2,indxTime2)(1:lvar))
        lvar=lenstr(vname(3,indxTime2))
        ierr=nf_put_att_text (ncid, hisTime2, 'units',  lvar,
     &                                vname(3,indxTime2)(1:lvar))
        lvar=lenstr(vname(4,indxTime2))
        ierr=nf_put_att_text (ncid, hisTime2, 'field',  lvar,
     &                                vname(4,indxTime2)(1:lvar))
        call nf_add_attribute(ncid, hisTime2, indxTime2, 5,
     &       NF_REAL, ierr)
        if (wrthis(indxZ)) then
          lvar=lenstr(vname(1,indxZ))
          ierr=nf_def_var (ncid, vname(1,indxZ)(1:lvar),
     &                              NF_REAL, 3, r2dgrd, hisZ)
          lvar=lenstr(vname(2,indxZ))
          ierr=nf_put_att_text (ncid, hisZ, 'long_name', lvar,
     &                                  vname(2,indxZ)(1:lvar))
          lvar=lenstr(vname(3,indxZ))
          ierr=nf_put_att_text (ncid, hisZ, 'units',     lvar,
     &                                  vname(3,indxZ)(1:lvar))
          lvar=lenstr(vname(4,indxZ))
          ierr=nf_put_att_text (ncid, hisZ, 'field',     lvar,
     &                                  vname(4,indxZ)(1:lvar))
          call nf_add_attribute(ncid, hisZ, indxZ, 5, NF_REAL, ierr)
       endif
        if (wrthis(indxUb)) then
          lvar=lenstr(vname(1,indxUb))
          ierr=nf_def_var (ncid, vname(1,indxUb)(1:lvar),
     &                             NF_REAL, 3, u2dgrd, hisUb)
          lvar=lenstr(vname(2,indxUb))
          ierr=nf_put_att_text (ncid, hisUb, 'long_name', lvar,
     &                                  vname(2,indxUb)(1:lvar))
          lvar=lenstr(vname(3,indxUb))
          ierr=nf_put_att_text (ncid, hisUb, 'units',     lvar,
     &                                  vname(3,indxUb)(1:lvar))
          lvar=lenstr(vname(4,indxUb))
          ierr=nf_put_att_text (ncid, hisUb, 'field',    lvar,
     &                                  vname(4,indxUb)(1:lvar))
          call nf_add_attribute(ncid, hisUb, indxUb, 5, NF_REAL, ierr)
        endif
        if (wrthis(indxVb)) then
          lvar=lenstr(vname(1,indxVb))
          ierr=nf_def_var (ncid, vname(1,indxVb)(1:lvar),
     &                              NF_REAL, 3, v2dgrd, hisVb)
          lvar=lenstr(vname(2,indxVb))
          ierr=nf_put_att_text (ncid, hisVb, 'long_name', lvar,
     &                                  vname(2,indxVb)(1:lvar))
          lvar=lenstr(vname(3,indxVb))
          ierr=nf_put_att_text (ncid, hisVb, 'units',     lvar,
     &                                  vname(3,indxVb)(1:lvar))
          lvar=lenstr(vname(4,indxVb))
          ierr=nf_put_att_text (ncid, hisVb, 'field',     lvar,
     &                                  vname(4,indxVb)(1:lvar))
          call nf_add_attribute(ncid, hisVb, indxVb, 5, NF_REAL, ierr)
        endif
        if (wrthis(indxU)) then
          lvar=lenstr(vname(1,indxU))
          ierr=nf_def_var (ncid, vname(1,indxU)(1:lvar),
     &                             NF_REAL, 4, u3dgrd, hisU)
          lvar=lenstr(vname(2,indxU))
          ierr=nf_put_att_text (ncid, hisU, 'long_name', lvar,
     &                                  vname(2,indxU)(1:lvar))
          lvar=lenstr(vname(3,indxU))
          ierr=nf_put_att_text (ncid, hisU, 'units',     lvar,
     &                                  vname(3,indxU)(1:lvar))
          lvar=lenstr(vname(4,indxU))
          ierr=nf_put_att_text (ncid, hisU, 'field',     lvar,
     &                                  vname(4,indxU)(1:lvar))
          call nf_add_attribute(ncid, hisU, indxU, 5, NF_REAL, ierr)
        endif
        if (wrthis(indxV)) then
          lvar=lenstr(vname(1,indxV))
          ierr=nf_def_var (ncid, vname(1,indxV)(1:lvar),
     &                             NF_REAL, 4, v3dgrd, hisV)
          lvar=lenstr(vname(2,indxV))
          ierr=nf_put_att_text (ncid, hisV, 'long_name', lvar,
     &                                  vname(2,indxV)(1:lvar))
          lvar=lenstr(vname(3,indxV))
          ierr=nf_put_att_text (ncid, hisV, 'units',     lvar,
     &                                  vname(3,indxV)(1:lvar))
          lvar=lenstr(vname(4,indxV))
          ierr=nf_put_att_text (ncid, hisV, 'field',     lvar,
     &                                  vname(4,indxV)(1:lvar))
          call nf_add_attribute(ncid, hisV, indxV, 5, NF_REAL, ierr)
        endif
        do itrc=1,NT
          if (wrthis(indxT+itrc-1)) then
            lvar=lenstr(vname(1,indxT+itrc-1))
            ierr=nf_def_var (ncid, vname(1,indxT+itrc-1)(1:lvar),
     &                             NF_REAL, 4, r3dgrd, hisT(itrc))
            lvar=lenstr(vname(2,indxT+itrc-1))
            ierr=nf_put_att_text (ncid, hisT(itrc), 'long_name',
     &                         lvar, vname(2,indxT+itrc-1)(1:lvar))
            lvar=lenstr(vname(3,indxT+itrc-1))
            ierr=nf_put_att_text (ncid, hisT(itrc), 'units', lvar,
     &                               vname(3,indxT+itrc-1)(1:lvar))
            lvar=lenstr(vname(4,indxT+itrc-1))
            ierr=nf_put_att_text (ncid, hisT(itrc), 'field', lvar,
     &                               vname(4,indxT+itrc-1)(1:lvar))
            call nf_add_attribute(ncid,hisT(itrc),indxT+itrc-1,5,
     &           NF_REAL, ierr)
            endif
      enddo
        if (wrthis(indxR)) then
          lvar=lenstr(vname(1,indxR))
          ierr=nf_def_var (ncid, vname(1,indxR)(1:lvar),
     &                             NF_REAL, 4, r3dgrd, hisR)
          lvar=lenstr(vname(2,indxR))
          ierr=nf_put_att_text (ncid, hisR, 'long_name', lvar,
     &                                  vname(2,indxR)(1:lvar))
          lvar=lenstr(vname(3,indxR))
          ierr=nf_put_att_text (ncid, hisR, 'units',     lvar,
     &                                  vname(3,indxR)(1:lvar))
          lvar=lenstr(vname(4,indxR))
          ierr=nf_put_att_text (ncid, hisR, 'field',     lvar,
     &                                  vname(4,indxR)(1:lvar))
          call nf_add_attribute(ncid, hisR, indxR, 5, NF_REAL, ierr)
        endif
        if (wrthis(indxO)) then
          lvar=lenstr(vname(1,indxO))
          ierr=nf_def_var (ncid, vname(1,indxO)(1:lvar),
     &                             NF_REAL, 4, w3dgrd, hisO)
          lvar=lenstr(vname(2,indxO))
          ierr=nf_put_att_text (ncid, hisO, 'long_name', lvar,
     &                                  vname(2,indxO)(1:lvar))
          lvar=lenstr(vname(3,indxO))
          ierr=nf_put_att_text (ncid, hisO, 'units',     lvar,
     &                                  vname(3,indxO)(1:lvar))
          lvar=lenstr(vname(4,indxO))
          ierr=nf_put_att_text (ncid, hisO, 'field',     lvar,
     &                                  vname(4,indxO)(1:lvar))
          call nf_add_attribute(ncid, hisO, indxO, 5, NF_REAL, ierr)
        endif
        if (wrthis(indxW)) then
          lvar=lenstr(vname(1,indxW))
          ierr=nf_def_var (ncid, vname(1,indxW)(1:lvar),
     &                               NF_REAL, 4, r3dgrd, hisW)
          lvar=lenstr(vname(2,indxW))
          ierr=nf_put_att_text (ncid, hisW, 'long_name', lvar,
     &                                  vname(2,indxW)(1:lvar))
          lvar=lenstr(vname(3,indxW))
          ierr=nf_put_att_text (ncid, hisW, 'units',     lvar,
     &                                  vname(3,indxW)(1:lvar))
          lvar=lenstr(vname(4,indxW))
          ierr=nf_put_att_text (ncid, hisW, 'field',     lvar,
     &                                  vname(4,indxW)(1:lvar))
          call nf_add_attribute(ncid, hisW, indxW, 5, NF_REAL, ierr)
        endif
        if (wrthis(indxBostr)) then
          lvar=lenstr(vname(1,indxBostr))
          ierr=nf_def_var (ncid, vname(1,indxBostr)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, hisBostr)
          lvar=lenstr(vname(2,indxBostr))
          ierr=nf_put_att_text (ncid, hisBostr, 'long_name', lvar,
     &                                 vname(2,indxBostr)(1:lvar))
          lvar=lenstr(vname(3,indxBostr))
          ierr=nf_put_att_text (ncid, hisBostr, 'units',     lvar,
     &                                 vname(3,indxBostr)(1:lvar))
          call nf_add_attribute(ncid, hisBostr,indxBostr,5,
     &                          NF_REAL, ierr)
        endif
        if (wrthis(indxWstr)) then
          lvar=lenstr(vname(1,indxWstr))
          ierr=nf_def_var (ncid, vname(1,indxWstr)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, hisWstr)
          lvar=lenstr(vname(2,indxWstr))
          ierr=nf_put_att_text (ncid, hisWstr, 'long_name', lvar,
     &                                 vname(2,indxWstr)(1:lvar))
          lvar=lenstr(vname(3,indxWstr))
          ierr=nf_put_att_text (ncid, hisWstr, 'units',     lvar,
     &                                 vname(3,indxWstr)(1:lvar))
          call nf_add_attribute(ncid, hisWstr, indxWstr, 5,
     &                          NF_REAL, ierr)
        endif
        if (wrthis(indxUWstr)) then
          lvar=lenstr(vname(1,indxUWstr))
          ierr=nf_def_var (ncid, vname(1,indxUWstr)(1:lvar),
     &                             NF_REAL, 3, u2dgrd, hisUWstr)
          lvar=lenstr(vname(2,indxUWstr))
          ierr=nf_put_att_text (ncid, hisUWstr, 'long_name', lvar,
     &                                 vname(2,indxUWstr)(1:lvar))
          lvar=lenstr(vname(3,indxUWstr))
          ierr=nf_put_att_text (ncid, hisUWstr, 'units',     lvar,
     &                                 vname(3,indxUWstr)(1:lvar))
          call nf_add_attribute(ncid, hisUWstr, indxUWstr, 5,
     &                          NF_REAL, ierr)
        endif
        if (wrthis(indxVWstr)) then
          lvar=lenstr(vname(1,indxVWstr))
          ierr=nf_def_var (ncid, vname(1,indxVWstr)(1:lvar),
     &                             NF_REAL, 3, v2dgrd, hisVWstr)
          lvar=lenstr(vname(2,indxVWstr))
          ierr=nf_put_att_text (ncid, hisVWstr, 'long_name', lvar,
     &                                 vname(2,indxVWstr)(1:lvar))
          lvar=lenstr(vname(3,indxVWstr))
          ierr=nf_put_att_text (ncid, hisVWstr, 'units',     lvar,
     &                                 vname(3,indxVWstr)(1:lvar))
          call nf_add_attribute(ncid, hisVWstr, indxVWstr, 5,
     &                          NF_REAL, ierr)
        endif
        if (wrthis(indxDiff)) then
          lvar=lenstr(vname(1,indxDiff))
          ierr=nf_def_var (ncid, vname(1,indxDiff)(1:lvar),
     &                             NF_REAL, 4, r3dgrd, hisDiff)
          lvar=lenstr(vname(2,indxDiff))
          ierr=nf_put_att_text (ncid, hisDiff, 'long_name', lvar,
     &                                  vname(2,indxDiff)(1:lvar))
          lvar=lenstr(vname(4,indxDiff))
          ierr=nf_put_att_text (ncid, hisDiff, 'field',     lvar,
     &                                  vname(4,indxDiff)(1:lvar))
          call nf_add_attribute(ncid, hisDiff, indxDiff, 5,
     &                          NF_REAL, ierr)
       endif
        if (wrthis(indxAkv)) then
          lvar=lenstr(vname(1,indxAkv))
          ierr=nf_def_var (ncid, vname(1,indxAkv)(1:lvar),
     &                             NF_REAL, 4, w3dgrd, hisAkv)
          lvar=lenstr(vname(2,indxAkv))
          ierr=nf_put_att_text (ncid, hisAkv, 'long_name', lvar,
     &                                  vname(2,indxAkv)(1:lvar))
          lvar=lenstr(vname(3,indxAkv))
          ierr=nf_put_att_text (ncid, hisAkv, 'units',     lvar,
     &                                  vname(3,indxAkv)(1:lvar))
          lvar=lenstr(vname(4,indxAkv))
          ierr=nf_put_att_text (ncid, hisAkv, 'field',     lvar,
     &                                  vname(4,indxAkv)(1:lvar))
          call nf_add_attribute(ncid, hisAkv, indxAkv, 5, NF_REAL,
     &                                                          ierr)
        endif
        if (wrthis(indxAkt)) then
          lvar=lenstr(vname(1,indxAkt))
          ierr=nf_def_var (ncid, vname(1,indxAkt)(1:lvar),
     &                              NF_REAL, 4, w3dgrd, hisAkt)
          lvar=lenstr(vname(2,indxAkt))
          ierr=nf_put_att_text (ncid, hisAkt, 'long_name', lvar,
     &                                  vname(2,indxAkt)(1:lvar))
          lvar=lenstr(vname(3,indxAkt))
          ierr=nf_put_att_text (ncid, hisAkt, 'units',     lvar,
     &                                  vname(3,indxAkt)(1:lvar))
          lvar=lenstr(vname(4,indxAkt))
          ierr=nf_put_att_text (ncid, hisAkt, 'field',     lvar,
     &                                  vname(4,indxAkt)(1:lvar))
          call nf_add_attribute(ncid, hisAkt, indxAkt, 5, NF_REAL,
     &                                                           ierr)
        endif
        if (wrthis(indxAks)) then
          lvar=lenstr(vname(1,indxAks))
          ierr=nf_def_var (ncid, vname(1,indxAks)(1:lvar),
     &                              NF_REAL, 4, w3dgrd, hisAks)
          lvar=lenstr(vname(2,indxAks))
          ierr=nf_put_att_text (ncid, hisAks, 'long_name', lvar,
     &                                  vname(2,indxAks)(1:lvar))
          lvar=lenstr(vname(3,indxAks))
          ierr=nf_put_att_text (ncid, hisAks, 'units',     lvar,
     &                                  vname(3,indxAks)(1:lvar))
          lvar=lenstr(vname(4,indxAks))
          ierr=nf_put_att_text (ncid, hisAks, 'field',     lvar,
     &                                  vname(4,indxAks)(1:lvar))
          call nf_add_attribute(ncid, hisAks, indxAks, 5, NF_REAL,
     &                                                           ierr)
        endif
        if (wrthis(indxHbl)) then
          lvar=lenstr(vname(1,indxHbl))
          ierr=nf_def_var (ncid, vname(1,indxHbl)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, hisHbl)
          lvar=lenstr(vname(2,indxHbl))
          ierr=nf_put_att_text (ncid, hisHbl, 'long_name', lvar,
     &                                  vname(2,indxHbl)(1:lvar))
          lvar=lenstr(vname(3,indxHbl))
          ierr=nf_put_att_text (ncid, hisHbl, 'units',     lvar,
     &                                  vname(3,indxHbl)(1:lvar))
          lvar=lenstr(vname(4,indxHbl))
          ierr=nf_put_att_text (ncid, hisHbl, 'field',     lvar,
     &                                  vname(4,indxHbl)(1:lvar))
          call nf_add_attribute(ncid, hisHbl, indxHbl, 5, NF_REAL,
     &                                                            ierr)
        endif
        if (wrthis(indxHbbl)) then
          lvar=lenstr(vname(1,indxHbbl))
          ierr=nf_def_var (ncid, vname(1,indxHbbl)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, hisHbbl)
          lvar=lenstr(vname(2,indxHbbl))
          ierr=nf_put_att_text (ncid, hisHbbl, 'long_name', lvar,
     &                                  vname(2,indxHbbl)(1:lvar))
          lvar=lenstr(vname(3,indxHbbl))
          ierr=nf_put_att_text (ncid, hisHbbl, 'units',     lvar,
     &                                  vname(3,indxHbbl)(1:lvar))
          lvar=lenstr(vname(4,indxHbbl))
          ierr=nf_put_att_text (ncid, hisHbbl, 'field',     lvar,
     &                                  vname(4,indxHbbl)(1:lvar))
          call nf_add_attribute(ncid, hisHbbl, indxHbbl, 5,
     &                          NF_REAL, ierr)
        endif
        if (wrthis(indxShflx)) then
          lvar=lenstr(vname(1,indxShflx))
          ierr=nf_def_var (ncid, vname(1,indxShflx)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, hisShflx)
          lvar=lenstr(vname(2,indxShflx))
          ierr=nf_put_att_text (ncid, hisShflx, 'long_name', lvar,
     &                                 vname(2,indxShflx)(1:lvar))
          lvar=lenstr(vname(3,indxShflx))
          ierr=nf_put_att_text (ncid, hisShflx, 'units',     lvar,
     &                                 vname(3,indxShflx)(1:lvar))
          call nf_add_attribute(ncid, hisShflx, indxShflx, 5,
     &                          NF_REAL, ierr)
        endif
        if (wrthis(indxSwflx)) then
          lvar=lenstr(vname(1,indxSwflx))
          ierr=nf_def_var (ncid, vname(1,indxSwflx)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, hisSwflx)
          lvar=lenstr(vname(2,indxSwflx))
          ierr=nf_put_att_text (ncid, hisSwflx, 'long_name', lvar,
     &                                 vname(2,indxSwflx)(1:lvar))
          lvar=lenstr(vname(3,indxSwflx))
          ierr=nf_put_att_text (ncid, hisSwflx, 'units',     lvar,
     &                                 vname(3,indxSwflx)(1:lvar))
          call nf_add_attribute(ncid, hisSwflx, indxSwflx, 5,
     &                          NF_REAL, ierr)
        endif
      if (wrthis(indxShflx_rsw)) then
        lvar=lenstr(vname(1,indxShflx_rsw))
        ierr=nf_def_var (ncid, vname(1,indxShflx_rsw)(1:lvar),
     &                           NF_REAL, 3, r2dgrd, hisShflx_rsw)
        lvar=lenstr(vname(2,indxShflx_rsw))
        ierr=nf_put_att_text (ncid, hisShflx_rsw, 'long_name', lvar,
     &                               vname(2,indxShflx_rsw)(1:lvar))
        lvar=lenstr(vname(3,indxShflx_rsw))
        ierr=nf_put_att_text (ncid, hisShflx_rsw, 'units',     lvar,
     &                               vname(3,indxShflx_rsw)(1:lvar))
        call nf_add_attribute(ncid, hisShflx_rsw, indxShflx_rsw,5,
     &                                                   NF_REAL, ierr)
      endif
      if (wrthis(indxShflx_rlw)) then
        lvar=lenstr(vname(1,indxShflx_rlw))
        ierr=nf_def_var (ncid, vname(1,indxShflx_rlw)(1:lvar),
     &                           NF_REAL, 3, r2dgrd, hisShflx_rlw)
        lvar=lenstr(vname(2,indxShflx_rlw))
        ierr=nf_put_att_text (ncid, hisShflx_rlw, 'long_name', lvar,
     &                               vname(2,indxShflx_rlw)(1:lvar))
        lvar=lenstr(vname(3,indxShflx_rlw))
        ierr=nf_put_att_text (ncid, hisShflx_rlw, 'units',     lvar,
     &                               vname(3,indxShflx_rlw)(1:lvar))
        call nf_add_attribute(ncid, hisShflx_rlw, indxShflx_rlw,5,
     &       NF_REAL, ierr)
      endif
      if (wrthis(indxShflx_lat)) then
        lvar=lenstr(vname(1,indxShflx_lat))
        ierr=nf_def_var (ncid, vname(1,indxShflx_lat)(1:lvar),
     &                           NF_REAL, 3, r2dgrd, hisShflx_lat)
        lvar=lenstr(vname(2,indxShflx_lat))
        ierr=nf_put_att_text (ncid, hisShflx_lat, 'long_name', lvar,
     &                               vname(2,indxShflx_lat)(1:lvar))
        lvar=lenstr(vname(3,indxShflx_lat))
        ierr=nf_put_att_text (ncid, hisShflx_lat, 'units',     lvar,
     &                               vname(3,indxShflx_lat)(1:lvar))
        call nf_add_attribute(ncid, hisShflx_lat, indxShflx_lat, 5,
     &       NF_REAL, ierr)
      endif
      if (wrthis(indxShflx_sen)) then
        lvar=lenstr(vname(1,indxShflx_sen))
        ierr=nf_def_var (ncid, vname(1,indxShflx_sen)(1:lvar),
     &                           NF_REAL, 3, r2dgrd, hisShflx_sen)
        lvar=lenstr(vname(2,indxShflx_sen))
        ierr=nf_put_att_text (ncid, hisShflx_sen, 'long_name', lvar,
     &                               vname(2,indxShflx_sen)(1:lvar))
        lvar=lenstr(vname(3,indxShflx_sen))
        ierr=nf_put_att_text (ncid, hisShflx_sen, 'units',     lvar,
     &                               vname(3,indxShflx_sen)(1:lvar))
        call nf_add_attribute(ncid, hisShflx_sen, indxShflx_sen, 5,
     &       NF_REAL, ierr)
      endif
        ierr=nf_enddef(ncid)
        write(stdout,'(6x,4A,1x,A,i4)') 'DEF_HIS/AVG - Created ',
     &                'new netCDF file ''', hisname(1:lstr), '''.'
     &                 ,' mynode =', mynode
      elseif (ncid.eq.-1) then
        ierr=nf_open (hisname(1:lstr), nf_write, ncid)
        if (ierr. eq. nf_noerr) then
          ierr=checkdims (ncid, hisname, lstr, rec)
          if (ierr .eq. nf_noerr) then
            if (nrpfhis.eq.0) then
              ierr=rec+1 - total_rec
            else
              ierr=rec+1 - (1+mod(total_rec-1, nrpfhis))
            endif
            if (ierr.gt.0) then
              if (mynode.eq.0) write( stdout,
     &                 '(/1x,A,I5,1x,A/8x,3A,I5,/8x,A,I5,1x,A/)'
     &            ) 'DEF_HIS/AVG WARNING: Actual number of records',
     &               rec,  'in netCDF file',  '''',  hisname(1:lstr),
     &             ''' exceeds the record number from restart data',
     &             rec+1-ierr,'/', total_rec,', restart is assumed.'
              rec=rec-ierr
            elseif (nrpfhis.eq.0) then
              total_rec=rec+1
              if (mynode.gt.0) total_rec=total_rec-1
            endif
            ierr=nf_noerr
          endif
        endif
        if (ierr. ne. nf_noerr) then
          if (mynode.eq.0) then
            create_new_file=.true.
            goto 10
          else
            write(stdout,'(/1x,4A,2x,A,I4/)') 'DEF_HIS/AVG ERROR: ',
     &                  'Cannot open file ''', hisname(1:lstr), '''.'
     &                   ,' mynode =', mynode
            goto 99
          endif
        endif
        ierr=nf_inq_varid (ncid, 'time_step', hisTstep)
        if (ierr .ne. nf_noerr) then
          write(stdout,1) 'time_step', hisname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxTime))
        ierr=nf_inq_varid (ncid,vname(1,indxTime)(1:lvar),hisTime)
        if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxTime)(1:lvar), hisname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxTime2))
        ierr=nf_inq_varid (ncid,vname(1,indxTime2)(1:lvar),hisTime2)
        if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxTime2)(1:lvar), hisname(1:lstr)
          goto 99
        endif
        if (wrthis(indxZ)) then
          lvar=lenstr(vname(1,indxZ))
          ierr=nf_inq_varid (ncid, vname(1,indxZ)(1:lvar), hisZ)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxZ)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxUb)) then
          lvar=lenstr(vname(1,indxUb))
          ierr=nf_inq_varid (ncid, vname(1,indxUb)(1:lvar), hisUb)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxUb)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxVb)) then
          lvar=lenstr(vname(1,indxVb))
          ierr=nf_inq_varid (ncid, vname(1,indxVb)(1:lvar), hisVb)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxVb)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxBostr)) then
          lvar=lenstr(vname(1,indxBostr))
          ierr=nf_inq_varid (ncid,vname(1,indxBostr)(1:lvar),
     &                                                   hisBostr)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxBostr)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxWstr)) then
          lvar=lenstr(vname(1,indxWstr))
          ierr=nf_inq_varid (ncid,vname(1,indxWstr)(1:lvar),
     &                                                   hisWstr)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxWstr)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxUWstr)) then
          lvar=lenstr(vname(1,indxUWstr))
          ierr=nf_inq_varid (ncid,vname(1,indxUWstr)(1:lvar),
     &                                                   hisUWstr)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxUWstr)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxVWstr)) then
          lvar=lenstr(vname(1,indxVWstr))
          ierr=nf_inq_varid (ncid,vname(1,indxVWstr)(1:lvar),
     &                                                   hisVWstr)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxVWstr)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxU)) then
          lvar=lenstr(vname(1,indxU))
          ierr=nf_inq_varid (ncid, vname(1,indxU)(1:lvar), hisU)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxU)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxV)) then
          lvar=lenstr(vname(1,indxV))
          ierr=nf_inq_varid (ncid, vname(1,indxV)(1:lvar), hisV)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxV)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        do itrc=1,NT
          if (wrthis(indxT+itrc-1)) then
            lvar=lenstr(vname(1,indxT+itrc-1))
            ierr=nf_inq_varid (ncid, vname(1,indxT+itrc-1)(1:lvar),
     &                                                 hisT(itrc))
            if (ierr .ne. nf_noerr) then
              write(stdout,1) vname(1,indxT+itrc-1)(1:lvar),
     &                                       hisname(1:lstr)
              goto 99
            endif
          endif
        enddo
        if (wrthis(indxR)) then
          lvar=lenstr(vname(1,indxR))
          ierr=nf_inq_varid (ncid, vname(1,indxR)(1:lvar), hisR)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxR)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxO)) then
          lvar=lenstr(vname(1,indxO))
          ierr=nf_inq_varid (ncid, vname(1,indxO)(1:lvar), hisO)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxO)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxW)) then
          lvar=lenstr(vname(1,indxW))
          ierr=nf_inq_varid (ncid, vname(1,indxW)(1:lvar), hisW)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxW)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxDiff)) then
          lvar=lenstr(vname(1,indxDiff))
          ierr=nf_inq_varid (ncid, vname(1,indxDiff)(1:lvar), hisDiff)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxDiff)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxAkv)) then
          lvar=lenstr(vname(1,indxAkv))
          ierr=nf_inq_varid (ncid, vname(1,indxAkv)(1:lvar), hisAkv)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxAkv)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxAkt)) then
          lvar=lenstr(vname(1,indxAkt))
          ierr=nf_inq_varid (ncid,vname(1,indxAkt)(1:lvar), hisAkt)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxAkt)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxAks)) then
          lvar=lenstr(vname(1,indxAks))
          ierr=nf_inq_varid (ncid,vname(1,indxAks)(1:lvar), hisAks)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxAks)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxHbl)) then
          lvar=lenstr(vname(1,indxHbl))
          ierr=nf_inq_varid (ncid,vname(1,indxHbl)(1:lvar), hisHbl)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxHbl)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxHbbl)) then
          lvar=lenstr(vname(1,indxHbbl))
          ierr=nf_inq_varid (ncid,vname(1,indxHbbl)(1:lvar), hisHbbl)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxHbbl)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxShflx)) then
          lvar=lenstr(vname(1,indxShflx))
          ierr=nf_inq_varid (ncid,vname(1,indxShflx)(1:lvar),
     &                                                   hisShflx)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxShflx)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxSwflx)) then
          lvar=lenstr(vname(1,indxSwflx))
          ierr=nf_inq_varid (ncid,vname(1,indxSwflx)(1:lvar),
     &                                                   hisSwflx)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxSwflx)(1:lvar), hisname(1:lstr)
            goto 99
          endif
        endif
        if (wrthis(indxShflx_rsw)) then
         lvar=lenstr(vname(1,indxShflx_rsw))
         ierr=nf_inq_varid (ncid,vname(1,indxShflx_rsw)(1:lvar),
     &                                               hisShflx_rsw)
         if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxShflx_rsw)(1:lvar), 
     &                                                   hisname(1:lstr)
          goto 99
         endif
        endif
        if (wrthis(indxShflx_rlw)) then
         lvar=lenstr(vname(1,indxShflx_rlw))
         ierr=nf_inq_varid (ncid,vname(1,indxShflx_rlw)(1:lvar),
     &                                               hisShflx_rlw)
         if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxShflx_rlw)(1:lvar), 
     &                                                   hisname(1:lstr)
          goto 99
         endif
        endif
        if (wrthis(indxShflx_lat)) then
         lvar=lenstr(vname(1,indxShflx_lat))
         ierr=nf_inq_varid (ncid,vname(1,indxShflx_lat)(1:lvar),
     &                                               hisShflx_lat)
         if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxShflx_lat)(1:lvar), 
     &                                                   hisname(1:lstr)
          goto 99
         endif
        endif
        if (wrthis(indxShflx_sen)) then
         lvar=lenstr(vname(1,indxShflx_sen))
         ierr=nf_inq_varid (ncid,vname(1,indxShflx_sen)(1:lvar),
     &                                               hisShflx_sen)
         if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxShflx_sen)(1:lvar), 
     &                                                   hisname(1:lstr)
          goto 99
         endif
        endif
      if (mynode.eq.0) write(*,'(6x,2A,i4,1x,A,i4)')
     &                     'DEF_HIS/AVG -- Opened ',
     &                     'existing file  from record =', rec
     &                      ,' mynode =', mynode
      else
        ierr=nf_open (hisname(1:lstr), nf_write, ncid)
        if (ierr .ne. nf_noerr) then
          if (mynode.eq.0) write(stdout,'(/1x,4A,2x,A,I4/)')
     &                'DEF_HIS/AVG ERROR: ',
     &                'Cannot open file ''', hisname(1:lstr), '''.'
     &                 ,' mynode =', mynode
          goto 99
        endif
      endif
      ierr=nf_set_fill (ncid, nf_nofill, lvar)
      if (ierr .ne. nf_noerr) then
        write(*,'(6x,2A,i4,1x,A,i4)') 'DEF_HIS/AVG ERROR: Cannot ',
     &    'switch to ''nf_nofill'' more; netCDF error code =', ierr
      endif
   1  format(/1x,'DEF_HIS/AVG ERROR: Cannot find variable ''',
     &                   A, ''' in netCDF file ''', A, '''.'/)
        if (total_rec.le.1) call wrt_grid (ncid, hisname, lstr)
  99  return
       
      

      end subroutine Sub_Loop_def_his

      subroutine def_avg (ncid, total_rec, ierr)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_def_avg(ncid,total_rec,ierr,dt,navg,time,tim
     &e_avg,ntsavg,avgShflx_sen,avgShflx_lat,avgShflx_rlw,avgShflx_rsw,a
     &vgSwflx,avgShflx,avgHbbl,avgHbl,avgAks,avgAkt,avgAkv,avgDiff,avgVW
     &str,avgUWstr,avgWstr,avgBostr,avgW,avgO,avgR,avgT,avgV,avgU,avgVb,
     &avgUb,avgZ,wrtavg,avgTime2,avgTime,vname,avgTstep,eta_v,eta_rho,xi
     &_u,xi_rho,mynode,ldefhis,nrpfavg,avgname)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4), parameter :: nf_byte = 1
      integer(4), parameter :: nf_int1 = nf_byte
      integer(4), parameter :: nf_char = 2
      integer(4), parameter :: nf_short = 3
      integer(4), parameter :: nf_int2 = nf_short
      integer(4), parameter :: nf_int = 4
      integer(4), parameter :: nf_float = 5
      integer(4), parameter :: nf_real = nf_float
      integer(4), parameter :: nf_double = 6
      integer(4), parameter :: nf_fill_byte = -127
      integer(4), parameter :: nf_fill_int1 = nf_fill_byte
      integer(4), parameter :: nf_fill_char = 0
      integer(4), parameter :: nf_fill_short = -32767
      integer(4), parameter :: nf_fill_int2 = nf_fill_short
      integer(4), parameter :: nf_fill_int = -2147483647
      real, parameter :: nf_fill_float = 9.9692099683868690D+36
      real, parameter :: nf_fill_real = nf_fill_float
      real(8), parameter :: nf_fill_double = 9.9692099683868690D+36
      integer(4), parameter :: nf_nowrite = 0
      integer(4), parameter :: nf_write = 1
      integer(4), parameter :: nf_clobber = 0
      integer(4), parameter :: nf_noclobber = 4
      integer(4), parameter :: nf_fill = 0
      integer(4), parameter :: nf_nofill = 256
      integer(4), parameter :: nf_lock = 1024
      integer(4), parameter :: nf_share = 2048
      integer(4), parameter :: nf_64bit_offset = 512
      integer(4), parameter :: nf_sizehint_default = 0
      integer(4), parameter :: nf_align_chunk = -1
      integer(4), parameter :: nf_format_classic = 1
      integer(4), parameter :: nf_format_64bit = 2
      integer(4), parameter :: nf_diskless = 8
      integer(4), parameter :: nf_mmap = 16
      integer(4), parameter :: nf_unlimited = 0
      integer(4), parameter :: nf_global = 0
      integer(4), parameter :: nf_max_dims = 1024
      integer(4), parameter :: nf_max_attrs = 8192
      integer(4), parameter :: nf_max_vars = 8192
      integer(4), parameter :: nf_max_name = 256
      integer(4), parameter :: nf_max_var_dims = nf_max_dims
      integer(4), parameter :: nf_noerr = 0
      integer(4), parameter :: nf_ebadid = -33
      integer(4), parameter :: nf_eexist = -35
      integer(4), parameter :: nf_einval = -36
      integer(4), parameter :: nf_eperm = -37
      integer(4), parameter :: nf_enotindefine = -38
      integer(4), parameter :: nf_eindefine = -39
      integer(4), parameter :: nf_einvalcoords = -40
      integer(4), parameter :: nf_emaxdims = -41
      integer(4), parameter :: nf_enameinuse = -42
      integer(4), parameter :: nf_enotatt = -43
      integer(4), parameter :: nf_emaxatts = -44
      integer(4), parameter :: nf_ebadtype = -45
      integer(4), parameter :: nf_ebaddim = -46
      integer(4), parameter :: nf_eunlimpos = -47
      integer(4), parameter :: nf_emaxvars = -48
      integer(4), parameter :: nf_enotvar = -49
      integer(4), parameter :: nf_eglobal = -50
      integer(4), parameter :: nf_enotnc = -51
      integer(4), parameter :: nf_ests = -52
      integer(4), parameter :: nf_emaxname = -53
      integer(4), parameter :: nf_eunlimit = -54
      integer(4), parameter :: nf_enorecvars = -55
      integer(4), parameter :: nf_echar = -56
      integer(4), parameter :: nf_eedge = -57
      integer(4), parameter :: nf_estride = -58
      integer(4), parameter :: nf_ebadname = -59
      integer(4), parameter :: nf_erange = -60
      integer(4), parameter :: nf_enomem = -61
      integer(4), parameter :: nf_evarsize = -62
      integer(4), parameter :: nf_edimsize = -63
      integer(4), parameter :: nf_etrunc = -64
      integer(4), parameter :: nf_fatal = 1
      integer(4), parameter :: nf_verbose = 2
      integer(4), parameter :: nf_ubyte = 7
      integer(4), parameter :: nf_ushort = 8
      integer(4), parameter :: nf_uint = 9
      integer(4), parameter :: nf_int64 = 10
      integer(4), parameter :: nf_uint64 = 11
      integer(4), parameter :: nf_string = 12
      integer(4), parameter :: nf_vlen = 13
      integer(4), parameter :: nf_opaque = 14
      integer(4), parameter :: nf_enum = 15
      integer(4), parameter :: nf_compound = 16
      integer(4), parameter :: nf_fill_ubyte = 255
      integer(4), parameter :: nf_fill_ushort = 65535
      integer(4), parameter :: nf_format_netcdf4 = 3
      integer(4), parameter :: nf_format_netcdf4_classic = 4
      integer(4), parameter :: nf_netcdf4 = 4096
      integer(4), parameter :: nf_classic_model = 256
      integer(4), parameter :: nf_chunk_seq = 0
      integer(4), parameter :: nf_chunk_sub = 1
      integer(4), parameter :: nf_chunk_sizes = 2
      integer(4), parameter :: nf_endian_native = 0
      integer(4), parameter :: nf_endian_little = 1
      integer(4), parameter :: nf_endian_big = 2
      integer(4), parameter :: nf_chunked = 0
      integer(4), parameter :: nf_contiguous = 1
      integer(4), parameter :: nf_nochecksum = 0
      integer(4), parameter :: nf_fletcher32 = 1
      integer(4), parameter :: nf_noshuffle = 0
      integer(4), parameter :: nf_shuffle = 1
      integer(4), parameter :: nf_szip_ec_option_mask = 4
      integer(4), parameter :: nf_szip_nn_option_mask = 32
      integer(4), parameter :: nf_mpiio = 8192
      integer(4), parameter :: nf_mpiposix = 16384
      integer(4), parameter :: nf_pnetcdf = 32768
      integer(4), parameter :: nf_independent = 0
      integer(4), parameter :: nf_collective = 1
      integer(4), parameter :: nf_ehdferr = -101
      integer(4), parameter :: nf_ecantread = -102
      integer(4), parameter :: nf_ecantwrite = -103
      integer(4), parameter :: nf_ecantcreate = -104
      integer(4), parameter :: nf_efilemeta = -105
      integer(4), parameter :: nf_edimmeta = -106
      integer(4), parameter :: nf_eattmeta = -107
      integer(4), parameter :: nf_evarmeta = -108
      integer(4), parameter :: nf_enocompound = -109
      integer(4), parameter :: nf_eattexists = -110
      integer(4), parameter :: nf_enotnc4 = -111
      integer(4), parameter :: nf_estrictnc3 = -112
      integer(4), parameter :: nf_enotnc3 = -113
      integer(4), parameter :: nf_enopar = -114
      integer(4), parameter :: nf_eparinit = -115
      integer(4), parameter :: nf_ebadgrpid = -116
      integer(4), parameter :: nf_ebadtypid = -117
      integer(4), parameter :: nf_etypdefined = -118
      integer(4), parameter :: nf_ebadfield = -119
      integer(4), parameter :: nf_ebadclass = -120
      integer(4), parameter :: nf_emaptype = -121
      integer(4), parameter :: nf_elatefill = -122
      integer(4), parameter :: nf_elatedef = -123
      integer(4), parameter :: nf_edimscale = -124
      integer(4), parameter :: nf_enogrp = -125
      integer(4), parameter :: ncbyte = 1
      integer(4), parameter :: ncchar = 2
      integer(4), parameter :: ncshort = 3
      integer(4), parameter :: nclong = 4
      integer(4), parameter :: ncfloat = 5
      integer(4), parameter :: ncdouble = 6
      integer(4), parameter :: ncrdwr = 1
      integer(4), parameter :: nccreat = 2
      integer(4), parameter :: ncexcl = 4
      integer(4), parameter :: ncindef = 8
      integer(4), parameter :: ncnsync = 16
      integer(4), parameter :: nchsync = 32
      integer(4), parameter :: ncndirty = 64
      integer(4), parameter :: nchdirty = 128
      integer(4), parameter :: ncfill = 0
      integer(4), parameter :: ncnofill = 256
      integer(4), parameter :: nclink = 32768
      integer(4), parameter :: ncnowrit = 0
      integer(4), parameter :: ncwrite = ncrdwr
      integer(4), parameter :: ncclob = nf_clobber
      integer(4), parameter :: ncnoclob = nf_noclobber
      integer(4), parameter :: ncunlim = 0
      integer(4), parameter :: ncglobal = 0
      integer(4), parameter :: maxncop = 64
      integer(4), parameter :: maxncdim = 1024
      integer(4), parameter :: maxncatt = 8192
      integer(4), parameter :: maxncvar = 8192
      integer(4), parameter :: maxncnam = 256
      integer(4), parameter :: maxvdims = maxncdim
      integer(4), parameter :: ncnoerr = nf_noerr
      integer(4), parameter :: ncebadid = nf_ebadid
      integer(4), parameter :: ncenfile = -31
      integer(4), parameter :: nceexist = nf_eexist
      integer(4), parameter :: nceinval = nf_einval
      integer(4), parameter :: nceperm = nf_eperm
      integer(4), parameter :: ncenotin = nf_enotindefine
      integer(4), parameter :: nceindef = nf_eindefine
      integer(4), parameter :: ncecoord = nf_einvalcoords
      integer(4), parameter :: ncemaxds = nf_emaxdims
      integer(4), parameter :: ncename = nf_enameinuse
      integer(4), parameter :: ncenoatt = nf_enotatt
      integer(4), parameter :: ncemaxat = nf_emaxatts
      integer(4), parameter :: ncebadty = nf_ebadtype
      integer(4), parameter :: ncebadd = nf_ebaddim
      integer(4), parameter :: nceunlim = nf_eunlimpos
      integer(4), parameter :: ncemaxvs = nf_emaxvars
      integer(4), parameter :: ncenotvr = nf_enotvar
      integer(4), parameter :: nceglob = nf_eglobal
      integer(4), parameter :: ncenotnc = nf_enotnc
      integer(4), parameter :: ncests = nf_ests
      integer(4), parameter :: ncentool = nf_emaxname
      integer(4), parameter :: ncfoobar = 32
      integer(4), parameter :: ncsyserr = -31
      integer(4), parameter :: ncfatal = 1
      integer(4), parameter :: ncverbos = 2
      integer(4), parameter :: filbyte = -127
      integer(4), parameter :: filchar = 0
      integer(4), parameter :: filshort = -32767
      integer(4), parameter :: fillong = -2147483647
      real, parameter :: filfloat = 9.9692099683868690D+36
      real(8), parameter :: fildoub = 9.9692099683868690D+36
      real :: dt
      integer(4) :: navg
      real :: time
      real :: time_avg
      integer(4) :: ntsavg
      integer(4) :: avgShflx_sen
      integer(4) :: avgShflx_lat
      integer(4) :: avgShflx_rlw
      integer(4) :: avgShflx_rsw
      integer(4) :: avgSwflx
      integer(4) :: avgShflx
      integer(4) :: avgHbbl
      integer(4) :: avgHbl
      integer(4) :: avgAks
      integer(4) :: avgAkt
      integer(4) :: avgAkv
      integer(4) :: avgDiff
      integer(4) :: avgVWstr
      integer(4) :: avgUWstr
      integer(4) :: avgWstr
      integer(4) :: avgBostr
      integer(4) :: avgW
      integer(4) :: avgO
      integer(4) :: avgR
      integer(4) :: avgV
      integer(4) :: avgU
      integer(4) :: avgVb
      integer(4) :: avgUb
      integer(4) :: avgZ
      integer(4) :: avgTime2
      integer(4) :: avgTime
      integer(4) :: avgTstep
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: mynode
      logical :: ldefhis
      integer(4) :: nrpfavg
      character(128) :: avgname
      integer(4), dimension(1:NT) :: avgT
      logical, dimension(1:500+NT) :: wrtavg
      character(75), dimension(1:20,1:500) :: vname
      integer(4) :: ncid
      integer(4) :: total_rec
      integer(4) :: ierr
        end subroutine Sub_Loop_def_avg

      end interface
      integer(4) :: ncid
      integer(4) :: total_rec
      integer(4) :: ierr
      

        call Sub_Loop_def_avg(ncid,total_rec,ierr, Agrif_tabvars_r(46)%a
     &rray0, Agrif_tabvars_i(163)%iarray0, Agrif_tabvars_r(44)%array0, A
     &grif_tabvars_r(40)%array0, Agrif_tabvars_i(164)%iarray0, Agrif_tab
     &vars_i(50)%iarray0, Agrif_tabvars_i(51)%iarray0, Agrif_tabvars_i(5
     &2)%iarray0, Agrif_tabvars_i(66)%iarray0, Agrif_tabvars_i(67)%iarra
     &y0, Agrif_tabvars_i(68)%iarray0, Agrif_tabvars_i(53)%iarray0, Agri
     &f_tabvars_i(54)%iarray0, Agrif_tabvars_i(55)%iarray0, Agrif_tabvar
     &s_i(56)%iarray0, Agrif_tabvars_i(57)%iarray0, Agrif_tabvars_i(58)%
     &iarray0, Agrif_tabvars_i(69)%iarray0, Agrif_tabvars_i(70)%iarray0,
     & Agrif_tabvars_i(71)%iarray0, Agrif_tabvars_i(72)%iarray0, Agrif_t
     &abvars_i(60)%iarray0, Agrif_tabvars_i(61)%iarray0, Agrif_tabvars_i
     &(62)%iarray0, Agrif_tabvars_i(63)%iarray1, Agrif_tabvars_i(64)%iar
     &ray0, Agrif_tabvars_i(65)%iarray0, Agrif_tabvars_i(73)%iarray0, Ag
     &rif_tabvars_i(74)%iarray0, Agrif_tabvars_i(75)%iarray0, Agrif_tabv
     &ars_l(2)%larray1, Agrif_tabvars_i(77)%iarray0, Agrif_tabvars_i(78)
     &%iarray0, Agrif_tabvars_c(1)%carray2, Agrif_tabvars_i(76)%iarray0,
     & Agrif_tabvars_i(142)%iarray0, Agrif_tabvars_i(143)%iarray0, Agrif
     &_tabvars_i(144)%iarray0, Agrif_tabvars_i(145)%iarray0, Agrif_tabva
     &rs_i(156)%iarray0, Agrif_tabvars_l(9)%larray0, Agrif_tabvars_i(79)
     &%iarray0, Agrif_tabvars_c(3)%carray0)

      end


      subroutine Sub_Loop_def_avg(ncid,total_rec,ierr,dt,navg,time,time_
     &avg,ntsavg,avgShflx_sen,avgShflx_lat,avgShflx_rlw,avgShflx_rsw,avg
     &Swflx,avgShflx,avgHbbl,avgHbl,avgAks,avgAkt,avgAkv,avgDiff,avgVWst
     &r,avgUWstr,avgWstr,avgBostr,avgW,avgO,avgR,avgT,avgV,avgU,avgVb,av
     &gUb,avgZ,wrtavg,avgTime2,avgTime,vname,avgTstep,eta_v,eta_rho,xi_u
     &,xi_rho,mynode,ldefhis,nrpfavg,avgname)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4), parameter :: nf_byte = 1
      integer(4), parameter :: nf_int1 = nf_byte
      integer(4), parameter :: nf_char = 2
      integer(4), parameter :: nf_short = 3
      integer(4), parameter :: nf_int2 = nf_short
      integer(4), parameter :: nf_int = 4
      integer(4), parameter :: nf_float = 5
      integer(4), parameter :: nf_real = nf_float
      integer(4), parameter :: nf_double = 6
      integer(4), parameter :: nf_fill_byte = -127
      integer(4), parameter :: nf_fill_int1 = nf_fill_byte
      integer(4), parameter :: nf_fill_char = 0
      integer(4), parameter :: nf_fill_short = -32767
      integer(4), parameter :: nf_fill_int2 = nf_fill_short
      integer(4), parameter :: nf_fill_int = -2147483647
      real, parameter :: nf_fill_float = 9.9692099683868690D+36
      real, parameter :: nf_fill_real = nf_fill_float
      real(8), parameter :: nf_fill_double = 9.9692099683868690D+36
      integer(4), parameter :: nf_nowrite = 0
      integer(4), parameter :: nf_write = 1
      integer(4), parameter :: nf_clobber = 0
      integer(4), parameter :: nf_noclobber = 4
      integer(4), parameter :: nf_fill = 0
      integer(4), parameter :: nf_nofill = 256
      integer(4), parameter :: nf_lock = 1024
      integer(4), parameter :: nf_share = 2048
      integer(4), parameter :: nf_64bit_offset = 512
      integer(4), parameter :: nf_sizehint_default = 0
      integer(4), parameter :: nf_align_chunk = -1
      integer(4), parameter :: nf_format_classic = 1
      integer(4), parameter :: nf_format_64bit = 2
      integer(4), parameter :: nf_diskless = 8
      integer(4), parameter :: nf_mmap = 16
      integer(4), parameter :: nf_unlimited = 0
      integer(4), parameter :: nf_global = 0
      integer(4), parameter :: nf_max_dims = 1024
      integer(4), parameter :: nf_max_attrs = 8192
      integer(4), parameter :: nf_max_vars = 8192
      integer(4), parameter :: nf_max_name = 256
      integer(4), parameter :: nf_max_var_dims = nf_max_dims
      integer(4), parameter :: nf_noerr = 0
      integer(4), parameter :: nf_ebadid = -33
      integer(4), parameter :: nf_eexist = -35
      integer(4), parameter :: nf_einval = -36
      integer(4), parameter :: nf_eperm = -37
      integer(4), parameter :: nf_enotindefine = -38
      integer(4), parameter :: nf_eindefine = -39
      integer(4), parameter :: nf_einvalcoords = -40
      integer(4), parameter :: nf_emaxdims = -41
      integer(4), parameter :: nf_enameinuse = -42
      integer(4), parameter :: nf_enotatt = -43
      integer(4), parameter :: nf_emaxatts = -44
      integer(4), parameter :: nf_ebadtype = -45
      integer(4), parameter :: nf_ebaddim = -46
      integer(4), parameter :: nf_eunlimpos = -47
      integer(4), parameter :: nf_emaxvars = -48
      integer(4), parameter :: nf_enotvar = -49
      integer(4), parameter :: nf_eglobal = -50
      integer(4), parameter :: nf_enotnc = -51
      integer(4), parameter :: nf_ests = -52
      integer(4), parameter :: nf_emaxname = -53
      integer(4), parameter :: nf_eunlimit = -54
      integer(4), parameter :: nf_enorecvars = -55
      integer(4), parameter :: nf_echar = -56
      integer(4), parameter :: nf_eedge = -57
      integer(4), parameter :: nf_estride = -58
      integer(4), parameter :: nf_ebadname = -59
      integer(4), parameter :: nf_erange = -60
      integer(4), parameter :: nf_enomem = -61
      integer(4), parameter :: nf_evarsize = -62
      integer(4), parameter :: nf_edimsize = -63
      integer(4), parameter :: nf_etrunc = -64
      integer(4), parameter :: nf_fatal = 1
      integer(4), parameter :: nf_verbose = 2
      integer(4), parameter :: nf_ubyte = 7
      integer(4), parameter :: nf_ushort = 8
      integer(4), parameter :: nf_uint = 9
      integer(4), parameter :: nf_int64 = 10
      integer(4), parameter :: nf_uint64 = 11
      integer(4), parameter :: nf_string = 12
      integer(4), parameter :: nf_vlen = 13
      integer(4), parameter :: nf_opaque = 14
      integer(4), parameter :: nf_enum = 15
      integer(4), parameter :: nf_compound = 16
      integer(4), parameter :: nf_fill_ubyte = 255
      integer(4), parameter :: nf_fill_ushort = 65535
      integer(4), parameter :: nf_format_netcdf4 = 3
      integer(4), parameter :: nf_format_netcdf4_classic = 4
      integer(4), parameter :: nf_netcdf4 = 4096
      integer(4), parameter :: nf_classic_model = 256
      integer(4), parameter :: nf_chunk_seq = 0
      integer(4), parameter :: nf_chunk_sub = 1
      integer(4), parameter :: nf_chunk_sizes = 2
      integer(4), parameter :: nf_endian_native = 0
      integer(4), parameter :: nf_endian_little = 1
      integer(4), parameter :: nf_endian_big = 2
      integer(4), parameter :: nf_chunked = 0
      integer(4), parameter :: nf_contiguous = 1
      integer(4), parameter :: nf_nochecksum = 0
      integer(4), parameter :: nf_fletcher32 = 1
      integer(4), parameter :: nf_noshuffle = 0
      integer(4), parameter :: nf_shuffle = 1
      integer(4), parameter :: nf_szip_ec_option_mask = 4
      integer(4), parameter :: nf_szip_nn_option_mask = 32
      integer(4), parameter :: nf_mpiio = 8192
      integer(4), parameter :: nf_mpiposix = 16384
      integer(4), parameter :: nf_pnetcdf = 32768
      integer(4), parameter :: nf_independent = 0
      integer(4), parameter :: nf_collective = 1
      integer(4), parameter :: nf_ehdferr = -101
      integer(4), parameter :: nf_ecantread = -102
      integer(4), parameter :: nf_ecantwrite = -103
      integer(4), parameter :: nf_ecantcreate = -104
      integer(4), parameter :: nf_efilemeta = -105
      integer(4), parameter :: nf_edimmeta = -106
      integer(4), parameter :: nf_eattmeta = -107
      integer(4), parameter :: nf_evarmeta = -108
      integer(4), parameter :: nf_enocompound = -109
      integer(4), parameter :: nf_eattexists = -110
      integer(4), parameter :: nf_enotnc4 = -111
      integer(4), parameter :: nf_estrictnc3 = -112
      integer(4), parameter :: nf_enotnc3 = -113
      integer(4), parameter :: nf_enopar = -114
      integer(4), parameter :: nf_eparinit = -115
      integer(4), parameter :: nf_ebadgrpid = -116
      integer(4), parameter :: nf_ebadtypid = -117
      integer(4), parameter :: nf_etypdefined = -118
      integer(4), parameter :: nf_ebadfield = -119
      integer(4), parameter :: nf_ebadclass = -120
      integer(4), parameter :: nf_emaptype = -121
      integer(4), parameter :: nf_elatefill = -122
      integer(4), parameter :: nf_elatedef = -123
      integer(4), parameter :: nf_edimscale = -124
      integer(4), parameter :: nf_enogrp = -125
      integer(4), parameter :: ncbyte = 1
      integer(4), parameter :: ncchar = 2
      integer(4), parameter :: ncshort = 3
      integer(4), parameter :: nclong = 4
      integer(4), parameter :: ncfloat = 5
      integer(4), parameter :: ncdouble = 6
      integer(4), parameter :: ncrdwr = 1
      integer(4), parameter :: nccreat = 2
      integer(4), parameter :: ncexcl = 4
      integer(4), parameter :: ncindef = 8
      integer(4), parameter :: ncnsync = 16
      integer(4), parameter :: nchsync = 32
      integer(4), parameter :: ncndirty = 64
      integer(4), parameter :: nchdirty = 128
      integer(4), parameter :: ncfill = 0
      integer(4), parameter :: ncnofill = 256
      integer(4), parameter :: nclink = 32768
      integer(4), parameter :: ncnowrit = 0
      integer(4), parameter :: ncwrite = ncrdwr
      integer(4), parameter :: ncclob = nf_clobber
      integer(4), parameter :: ncnoclob = nf_noclobber
      integer(4), parameter :: ncunlim = 0
      integer(4), parameter :: ncglobal = 0
      integer(4), parameter :: maxncop = 64
      integer(4), parameter :: maxncdim = 1024
      integer(4), parameter :: maxncatt = 8192
      integer(4), parameter :: maxncvar = 8192
      integer(4), parameter :: maxncnam = 256
      integer(4), parameter :: maxvdims = maxncdim
      integer(4), parameter :: ncnoerr = nf_noerr
      integer(4), parameter :: ncebadid = nf_ebadid
      integer(4), parameter :: ncenfile = -31
      integer(4), parameter :: nceexist = nf_eexist
      integer(4), parameter :: nceinval = nf_einval
      integer(4), parameter :: nceperm = nf_eperm
      integer(4), parameter :: ncenotin = nf_enotindefine
      integer(4), parameter :: nceindef = nf_eindefine
      integer(4), parameter :: ncecoord = nf_einvalcoords
      integer(4), parameter :: ncemaxds = nf_emaxdims
      integer(4), parameter :: ncename = nf_enameinuse
      integer(4), parameter :: ncenoatt = nf_enotatt
      integer(4), parameter :: ncemaxat = nf_emaxatts
      integer(4), parameter :: ncebadty = nf_ebadtype
      integer(4), parameter :: ncebadd = nf_ebaddim
      integer(4), parameter :: nceunlim = nf_eunlimpos
      integer(4), parameter :: ncemaxvs = nf_emaxvars
      integer(4), parameter :: ncenotvr = nf_enotvar
      integer(4), parameter :: nceglob = nf_eglobal
      integer(4), parameter :: ncenotnc = nf_enotnc
      integer(4), parameter :: ncests = nf_ests
      integer(4), parameter :: ncentool = nf_emaxname
      integer(4), parameter :: ncfoobar = 32
      integer(4), parameter :: ncsyserr = -31
      integer(4), parameter :: ncfatal = 1
      integer(4), parameter :: ncverbos = 2
      integer(4), parameter :: filbyte = -127
      integer(4), parameter :: filchar = 0
      integer(4), parameter :: filshort = -32767
      integer(4), parameter :: fillong = -2147483647
      real, parameter :: filfloat = 9.9692099683868690D+36
      real(8), parameter :: fildoub = 9.9692099683868690D+36
      real :: dt
      integer(4) :: navg
      real :: time
      real :: time_avg
      integer(4) :: ntsavg
      integer(4) :: avgShflx_sen
      integer(4) :: avgShflx_lat
      integer(4) :: avgShflx_rlw
      integer(4) :: avgShflx_rsw
      integer(4) :: avgSwflx
      integer(4) :: avgShflx
      integer(4) :: avgHbbl
      integer(4) :: avgHbl
      integer(4) :: avgAks
      integer(4) :: avgAkt
      integer(4) :: avgAkv
      integer(4) :: avgDiff
      integer(4) :: avgVWstr
      integer(4) :: avgUWstr
      integer(4) :: avgWstr
      integer(4) :: avgBostr
      integer(4) :: avgW
      integer(4) :: avgO
      integer(4) :: avgR
      integer(4) :: avgV
      integer(4) :: avgU
      integer(4) :: avgVb
      integer(4) :: avgUb
      integer(4) :: avgZ
      integer(4) :: avgTime2
      integer(4) :: avgTime
      integer(4) :: avgTstep
      integer(4) :: eta_v
      integer(4) :: eta_rho
      integer(4) :: xi_u
      integer(4) :: xi_rho
      integer(4) :: mynode
      logical :: ldefhis
      integer(4) :: nrpfavg
      character(128) :: avgname
      integer(4), dimension(1:NT) :: avgT
      logical, dimension(1:500+NT) :: wrtavg
      character(75), dimension(1:20,1:500) :: vname
      integer(4) :: ncid
      integer(4) :: total_rec
      integer(4) :: ierr

                   

                              
      logical :: create_new_file
                                                                       
                                                                 
                                                                 
                                                                 
                         
      integer(4) :: rec
      integer(4) :: lstr
      integer(4) :: lvar
      integer(4) :: lenstr
      integer(4) :: timedim
      integer(4), dimension(1:3) :: r2dgrd
      integer(4), dimension(1:3) :: u2dgrd
      integer(4), dimension(1:3) :: v2dgrd
      integer(4), dimension(1:2) :: auxil
      integer(4) :: checkdims
      integer(4), dimension(1:4) :: b3dgrd
      integer(4), dimension(1:4) :: r3dgrd
      integer(4), dimension(1:4) :: u3dgrd
      integer(4), dimension(1:4) :: v3dgrd
      integer(4), dimension(1:4) :: w3dgrd
      integer(4) :: itrc
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                        
                        
                        
                         
                        
                       
                         
                        
                          
                             

                                   

                             

                              

                                    

                            

                              

                                    

                               

                                       
                                       
                                       
                                        
                                       
                                      
                                      
                                     
                                       
                                     

                                             

                                  

                                        

                                              

                                           

                                                        

                                              

                                                         

                           
                         
                           
                             
                        
                          
                        
                         
                                
                                    
                               
                                  
                                
                            
                        
                                

                              

                                

                                  

                             

                                 

                                

                                 

                                       

                                         

                                     

                                       

                                     

                                 

                              

                             
                                  

                          
                               

                            
                             
                            
                            
                                
                                    

                                     

                                    

                                   

                                               

                         
                          
                          
                          
                         
                                
                             
                                
                            
                              
                           
                            
                            
                           
                             
                            
                           
                           
                          
                        
                            
                            
                              
                         
                         
                           
                            
                          
                          
                            
                            
                          
                              

                                 

                                 

                                 

                                

                                       

                                    

                                       

                                   

                                     

                                  

                                   

                                   

                                  

                                    

                                   

                                  

                                  

                                 

                               

                                   

                                   

                                     

                                

                                

                                  

                                   

                                 

                                 

                                   

                                   

                                 

                          
                           
                              

                                

                                    
      character(80) :: nf_inq_libvers
      external       nf_inq_libvers
                                 
      character(80) :: nf_strerror
      external       nf_strerror
                                 
      logical :: nf_issyserr
      external       nf_issyserr
                                       
      integer(4) :: nf_inq_base_pe
      external        nf_inq_base_pe
                                       
      integer(4) :: nf_set_base_pe
      external        nf_set_base_pe
                                  
      integer(4) :: nf_create
      external        nf_create
                                   
      integer(4) :: nf__create
      external        nf__create
                                      
      integer(4) :: nf__create_mp
      external        nf__create_mp
                                
      integer(4) :: nf_open
      external        nf_open
                                 
      integer(4) :: nf__open
      external        nf__open
                                    
      integer(4) :: nf__open_mp
      external        nf__open_mp
                                    
      integer(4) :: nf_set_fill
      external        nf_set_fill
                                              
      integer(4) :: nf_set_default_format
      external        nf_set_default_format
                                 
      integer(4) :: nf_redef
      external        nf_redef
                                  
      integer(4) :: nf_enddef
      external        nf_enddef
                                   
      integer(4) :: nf__enddef
      external        nf__enddef
                                
      integer(4) :: nf_sync
      external        nf_sync
                                 
      integer(4) :: nf_abort
      external        nf_abort
                                 
      integer(4) :: nf_close
      external        nf_close
                                  
      integer(4) :: nf_delete
      external        nf_delete
                               
      integer(4) :: nf_inq
      external        nf_inq
                            
      integer(4) :: nf_inq_path
      external nf_inq_path
                                     
      integer(4) :: nf_inq_ndims
      external        nf_inq_ndims
                                     
      integer(4) :: nf_inq_nvars
      external        nf_inq_nvars
                                     
      integer(4) :: nf_inq_natts
      external        nf_inq_natts
                                        
      integer(4) :: nf_inq_unlimdim
      external        nf_inq_unlimdim
                                      
      integer(4) :: nf_inq_format
      external        nf_inq_format
                                   
      integer(4) :: nf_def_dim
      external        nf_def_dim
                                     
      integer(4) :: nf_inq_dimid
      external        nf_inq_dimid
                                   
      integer(4) :: nf_inq_dim
      external        nf_inq_dim
                                       
      integer(4) :: nf_inq_dimname
      external        nf_inq_dimname
                                      
      integer(4) :: nf_inq_dimlen
      external        nf_inq_dimlen
                                      
      integer(4) :: nf_rename_dim
      external        nf_rename_dim
                                   
      integer(4) :: nf_inq_att
      external        nf_inq_att
                                     
      integer(4) :: nf_inq_attid
      external        nf_inq_attid
                                       
      integer(4) :: nf_inq_atttype
      external        nf_inq_atttype
                                      
      integer(4) :: nf_inq_attlen
      external        nf_inq_attlen
                                       
      integer(4) :: nf_inq_attname
      external        nf_inq_attname
                                    
      integer(4) :: nf_copy_att
      external        nf_copy_att
                                      
      integer(4) :: nf_rename_att
      external        nf_rename_att
                                   
      integer(4) :: nf_del_att
      external        nf_del_att
                                        
      integer(4) :: nf_put_att_text
      external        nf_put_att_text
                                        
      integer(4) :: nf_get_att_text
      external        nf_get_att_text
                                        
      integer(4) :: nf_put_att_int1
      external        nf_put_att_int1
                                        
      integer(4) :: nf_get_att_int1
      external        nf_get_att_int1
                                        
      integer(4) :: nf_put_att_int2
      external        nf_put_att_int2
                                        
      integer(4) :: nf_get_att_int2
      external        nf_get_att_int2
                                       
      integer(4) :: nf_put_att_int
      external        nf_put_att_int
                                       
      integer(4) :: nf_get_att_int
      external        nf_get_att_int
                                        
      integer(4) :: nf_put_att_real
      external        nf_put_att_real
                                        
      integer(4) :: nf_get_att_real
      external        nf_get_att_real
                                          
      integer(4) :: nf_put_att_double
      external        nf_put_att_double
                                          
      integer(4) :: nf_get_att_double
      external        nf_get_att_double
                                   
      integer(4) :: nf_def_var
      external        nf_def_var
                                   
      integer(4) :: nf_inq_var
      external        nf_inq_var
                                     
      integer(4) :: nf_inq_varid
      external        nf_inq_varid
                                       
      integer(4) :: nf_inq_varname
      external        nf_inq_varname
                                       
      integer(4) :: nf_inq_vartype
      external        nf_inq_vartype
                                        
      integer(4) :: nf_inq_varndims
      external        nf_inq_varndims
                                        
      integer(4) :: nf_inq_vardimid
      external        nf_inq_vardimid
                                        
      integer(4) :: nf_inq_varnatts
      external        nf_inq_varnatts
                                      
      integer(4) :: nf_rename_var
      external        nf_rename_var
                                    
      integer(4) :: nf_copy_var
      external        nf_copy_var
                                        
      integer(4) :: nf_put_var_text
      external        nf_put_var_text
                                        
      integer(4) :: nf_get_var_text
      external        nf_get_var_text
                                        
      integer(4) :: nf_put_var_int1
      external        nf_put_var_int1
                                        
      integer(4) :: nf_get_var_int1
      external        nf_get_var_int1
                                        
      integer(4) :: nf_put_var_int2
      external        nf_put_var_int2
                                        
      integer(4) :: nf_get_var_int2
      external        nf_get_var_int2
                                       
      integer(4) :: nf_put_var_int
      external        nf_put_var_int
                                       
      integer(4) :: nf_get_var_int
      external        nf_get_var_int
                                        
      integer(4) :: nf_put_var_real
      external        nf_put_var_real
                                        
      integer(4) :: nf_get_var_real
      external        nf_get_var_real
                                          
      integer(4) :: nf_put_var_double
      external        nf_put_var_double
                                          
      integer(4) :: nf_get_var_double
      external        nf_get_var_double
                                         
      integer(4) :: nf_put_var1_text
      external        nf_put_var1_text
                                         
      integer(4) :: nf_get_var1_text
      external        nf_get_var1_text
                                         
      integer(4) :: nf_put_var1_int1
      external        nf_put_var1_int1
                                         
      integer(4) :: nf_get_var1_int1
      external        nf_get_var1_int1
                                         
      integer(4) :: nf_put_var1_int2
      external        nf_put_var1_int2
                                         
      integer(4) :: nf_get_var1_int2
      external        nf_get_var1_int2
                                        
      integer(4) :: nf_put_var1_int
      external        nf_put_var1_int
                                        
      integer(4) :: nf_get_var1_int
      external        nf_get_var1_int
                                         
      integer(4) :: nf_put_var1_real
      external        nf_put_var1_real
                                         
      integer(4) :: nf_get_var1_real
      external        nf_get_var1_real
                                           
      integer(4) :: nf_put_var1_double
      external        nf_put_var1_double
                                           
      integer(4) :: nf_get_var1_double
      external        nf_get_var1_double
                                         
      integer(4) :: nf_put_vara_text
      external        nf_put_vara_text
                                         
      integer(4) :: nf_get_vara_text
      external        nf_get_vara_text
                                         
      integer(4) :: nf_put_vara_int1
      external        nf_put_vara_int1
                                         
      integer(4) :: nf_get_vara_int1
      external        nf_get_vara_int1
                                         
      integer(4) :: nf_put_vara_int2
      external        nf_put_vara_int2
                                         
      integer(4) :: nf_get_vara_int2
      external        nf_get_vara_int2
                                        
      integer(4) :: nf_put_vara_int
      external        nf_put_vara_int
                                        
      integer(4) :: nf_get_vara_int
      external        nf_get_vara_int
                                         
      integer(4) :: nf_put_vara_real
      external        nf_put_vara_real
                                         
      integer(4) :: nf_get_vara_real
      external        nf_get_vara_real
                                           
      integer(4) :: nf_put_vara_double
      external        nf_put_vara_double
                                           
      integer(4) :: nf_get_vara_double
      external        nf_get_vara_double
                                         
      integer(4) :: nf_put_vars_text
      external        nf_put_vars_text
                                         
      integer(4) :: nf_get_vars_text
      external        nf_get_vars_text
                                         
      integer(4) :: nf_put_vars_int1
      external        nf_put_vars_int1
                                         
      integer(4) :: nf_get_vars_int1
      external        nf_get_vars_int1
                                         
      integer(4) :: nf_put_vars_int2
      external        nf_put_vars_int2
                                         
      integer(4) :: nf_get_vars_int2
      external        nf_get_vars_int2
                                        
      integer(4) :: nf_put_vars_int
      external        nf_put_vars_int
                                        
      integer(4) :: nf_get_vars_int
      external        nf_get_vars_int
                                         
      integer(4) :: nf_put_vars_real
      external        nf_put_vars_real
                                         
      integer(4) :: nf_get_vars_real
      external        nf_get_vars_real
                                           
      integer(4) :: nf_put_vars_double
      external        nf_put_vars_double
                                           
      integer(4) :: nf_get_vars_double
      external        nf_get_vars_double
                                         
      integer(4) :: nf_put_varm_text
      external        nf_put_varm_text
                                         
      integer(4) :: nf_get_varm_text
      external        nf_get_varm_text
                                         
      integer(4) :: nf_put_varm_int1
      external        nf_put_varm_int1
                                         
      integer(4) :: nf_get_varm_int1
      external        nf_get_varm_int1
                                         
      integer(4) :: nf_put_varm_int2
      external        nf_put_varm_int2
                                         
      integer(4) :: nf_get_varm_int2
      external        nf_get_varm_int2
                                        
      integer(4) :: nf_put_varm_int
      external        nf_put_varm_int
                                        
      integer(4) :: nf_get_varm_int
      external        nf_get_varm_int
                                         
      integer(4) :: nf_put_varm_real
      external        nf_put_varm_real
                                         
      integer(4) :: nf_get_varm_real
      external        nf_get_varm_real
                                           
      integer(4) :: nf_put_varm_double
      external        nf_put_varm_double
                                           
      integer(4) :: nf_get_varm_double
      external        nf_get_varm_double
                         
                          
                        
                         
                          
                          
                        
                          
                        
                            
                              

                               

                             

                               

                                

                                

                              

                                

                              

                                  

                                        
                                         
                                     

                                        

                                  
                                       

                                          
                                               

                           
                                   

                                 
                                        

                             
                                  

                             
                                  

                               
                                    

                                 
                                      

                                 
                                      

                              
                                   

                           
                                

                              
                                   

                              
                                   

                              
                                   

                             
                                  

                           
                                

                                       
                                            

                                       
                                             

                         
                                 

                            
                                     

                           
                                    

                               
                                    

                              
                                   

                           
                                   

                             
                                     

                              
                                      

                               
                                       

                             
                                     

                            
                                    

                            
                                    

                            
                                    

                               
                                       

                              
                                      

                           
                                   

                              
                                      

                           
                                   

                          
                                  

                            
                                    

                             
                                     

                             
                                     

                               
                                       

                             
                                     

                             
                                     

                            
                                    

                             
                                     

                            
                                    

                             
                                     

                          
                                  

                              
      integer(4) :: nf_create_par
      external nf_create_par
                            
      integer(4) :: nf_open_par
      external nf_open_par
                                  
      integer(4) :: nf_var_par_access
      external nf_var_par_access
                            
      integer(4) :: nf_inq_ncid
      external nf_inq_ncid
                            
      integer(4) :: nf_inq_grps
      external nf_inq_grps
                               
      integer(4) :: nf_inq_grpname
      external nf_inq_grpname
                                    
      integer(4) :: nf_inq_grpname_full
      external nf_inq_grpname_full
                                   
      integer(4) :: nf_inq_grpname_len
      external nf_inq_grpname_len
                                  
      integer(4) :: nf_inq_grp_parent
      external nf_inq_grp_parent
                                
      integer(4) :: nf_inq_grp_ncid
      external nf_inq_grp_ncid
                                     
      integer(4) :: nf_inq_grp_full_ncid
      external nf_inq_grp_full_ncid
                              
      integer(4) :: nf_inq_varids
      external nf_inq_varids
                              
      integer(4) :: nf_inq_dimids
      external nf_inq_dimids
                           
      integer(4) :: nf_def_grp
      external nf_def_grp
                              
      integer(4) :: nf_rename_grp
      external nf_rename_grp
                                   
      integer(4) :: nf_def_var_deflate
      external nf_def_var_deflate
                                   
      integer(4) :: nf_inq_var_deflate
      external nf_inq_var_deflate
                                      
      integer(4) :: nf_def_var_fletcher32
      external nf_def_var_fletcher32
                                      
      integer(4) :: nf_inq_var_fletcher32
      external nf_inq_var_fletcher32
                                    
      integer(4) :: nf_def_var_chunking
      external nf_def_var_chunking
                                    
      integer(4) :: nf_inq_var_chunking
      external nf_inq_var_chunking
                                
      integer(4) :: nf_def_var_fill
      external nf_def_var_fill
                                
      integer(4) :: nf_inq_var_fill
      external nf_inq_var_fill
                                  
      integer(4) :: nf_def_var_endian
      external nf_def_var_endian
                                  
      integer(4) :: nf_inq_var_endian
      external nf_inq_var_endian
                               
      integer(4) :: nf_inq_typeids
      external nf_inq_typeids
                              
      integer(4) :: nf_inq_typeid
      external nf_inq_typeid
                            
      integer(4) :: nf_inq_type
      external nf_inq_type
                                 
      integer(4) :: nf_inq_user_type
      external nf_inq_user_type
                                
      integer(4) :: nf_def_compound
      external nf_def_compound
                                   
      integer(4) :: nf_insert_compound
      external nf_insert_compound
                                         
      integer(4) :: nf_insert_array_compound
      external nf_insert_array_compound
                                
      integer(4) :: nf_inq_compound
      external nf_inq_compound
                                     
      integer(4) :: nf_inq_compound_name
      external nf_inq_compound_name
                                     
      integer(4) :: nf_inq_compound_size
      external nf_inq_compound_size
                                        
      integer(4) :: nf_inq_compound_nfields
      external nf_inq_compound_nfields
                                      
      integer(4) :: nf_inq_compound_field
      external nf_inq_compound_field
                                          
      integer(4) :: nf_inq_compound_fieldname
      external nf_inq_compound_fieldname
                                           
      integer(4) :: nf_inq_compound_fieldindex
      external nf_inq_compound_fieldindex
                                            
      integer(4) :: nf_inq_compound_fieldoffset
      external nf_inq_compound_fieldoffset
                                          
      integer(4) :: nf_inq_compound_fieldtype
      external nf_inq_compound_fieldtype
                                           
      integer(4) :: nf_inq_compound_fieldndims
      external nf_inq_compound_fieldndims
                                               
      integer(4) :: nf_inq_compound_fielddim_sizes
      external nf_inq_compound_fielddim_sizes
                            
      integer(4) :: nf_def_vlen
      external nf_def_vlen
                            
      integer(4) :: nf_inq_vlen
      external nf_inq_vlen
                             
      integer(4) :: nf_free_vlen
      external nf_free_vlen
                            
      integer(4) :: nf_def_enum
      external nf_def_enum
                               
      integer(4) :: nf_insert_enum
      external nf_insert_enum
                            
      integer(4) :: nf_inq_enum
      external nf_inq_enum
                                   
      integer(4) :: nf_inq_enum_member
      external nf_inq_enum_member
                                  
      integer(4) :: nf_inq_enum_ident
      external nf_inq_enum_ident
                              
      integer(4) :: nf_def_opaque
      external nf_def_opaque
                              
      integer(4) :: nf_inq_opaque
      external nf_inq_opaque
                           
      integer(4) :: nf_put_att
      external nf_put_att
                           
      integer(4) :: nf_get_att
      external nf_get_att
                           
      integer(4) :: nf_put_var
      external nf_put_var
                            
      integer(4) :: nf_put_var1
      external nf_put_var1
                            
      integer(4) :: nf_put_vara
      external nf_put_vara
                            
      integer(4) :: nf_put_vars
      external nf_put_vars
                           
      integer(4) :: nf_get_var
      external nf_get_var
                            
      integer(4) :: nf_get_var1
      external nf_get_var1
                            
      integer(4) :: nf_get_vara
      external nf_get_vara
                            
      integer(4) :: nf_get_vars
      external nf_get_vars
                                  
      integer(4) :: nf_put_var1_int64
      external nf_put_var1_int64
                                  
      integer(4) :: nf_put_vara_int64
      external nf_put_vara_int64
                                  
      integer(4) :: nf_put_vars_int64
      external nf_put_vars_int64
                                  
      integer(4) :: nf_put_varm_int64
      external nf_put_varm_int64
                                 
      integer(4) :: nf_put_var_int64
      external nf_put_var_int64
                                  
      integer(4) :: nf_get_var1_int64
      external nf_get_var1_int64
                                  
      integer(4) :: nf_get_vara_int64
      external nf_get_vara_int64
                                  
      integer(4) :: nf_get_vars_int64
      external nf_get_vars_int64
                                  
      integer(4) :: nf_get_varm_int64
      external nf_get_varm_int64
                                 
      integer(4) :: nf_get_var_int64
      external nf_get_var_int64
                                    
      integer(4) :: nf_get_vlen_element
      external nf_get_vlen_element
                                    
      integer(4) :: nf_put_vlen_element
      external nf_put_vlen_element
                                   
      integer(4) :: nf_set_chunk_cache
      external nf_set_chunk_cache
                                   
      integer(4) :: nf_get_chunk_cache
      external nf_get_chunk_cache
                                       
      integer(4) :: nf_set_var_chunk_cache
      external nf_set_var_chunk_cache
                                       
      integer(4) :: nf_get_var_chunk_cache
      external nf_get_var_chunk_cache
                      
      integer(4) :: nccre
                      
      integer(4) :: ncopn
                       
      integer(4) :: ncddef
                      
      integer(4) :: ncdid
                       
      integer(4) :: ncvdef
                      
      integer(4) :: ncvid
                       
      integer(4) :: nctlen
                       
      integer(4) :: ncsfil
      external nccre
      external ncopn
      external ncddef
      external ncdid
      external ncvdef
      external ncvid
      external nctlen
      external ncsfil
                       
                        
                       
                        
                        
                        
                         
                         
                       
                         
                        
                       
                         
                         
                       
                         
                        
                         
                         
                         
                         
                         
                        
                         
                         
                         
                         
                        
                         
                         
                         
                         
                        
                         
                         
                         
                        
                       
                         
                         
                         
                        
                         
                         
                         
                        
                         
                         
                       
                       
                        
                       
                        
                         
                           

                           

                            

                           

                            

                             

                           

                            

                           

                            

                             

                             

                              

                               

                           

                               

                               

                             

                                 

                                    

                                        

                        
                            

                              

                             

                                

                                

                                

                               

                                    

                                   

                                     

                               

                                     

                                     

                                   

                                            

                                        

                                           

                                       

                                        

                                      

                                       

                                       

                                     

                                        

                                       

                                      

                                     

                                     

                                 

                                        

                              

                               

                            

                             

                        
                        
                         
                        
                    
                              
                                

                             

                                   

                                       

                                                   

                                                  

                        
      character(70) :: text
      ierr=0
      lstr=lenstr(avgname)
      if (nrpfavg.gt.0) then
        lvar=total_rec-(1+mod(total_rec-1, nrpfavg))
        call insert_time_index (avgname, lstr, lvar, ierr)
        if (ierr .ne. 0) goto 99
      endif
      create_new_file=ldefhis
      if (ncid.ne.-1) create_new_file=.false.
      if (mynode.gt.0) create_new_file=.false.
  10  if (create_new_file) then
        ierr=nf_create(avgname(1:lstr),nf_clobber, ncid)
        if (ierr .ne. nf_noerr) then
          write(stdout,'(/3(1x,A)/)') 'ERROR in def_his/avg:',
     &           'Cannot create netCDF file:', avgname(1:lstr)
          goto 99
        endif
        if (nrpfavg.eq.0) total_rec=0
        call put_global_atts (ncid, ierr)
        ierr=nf_def_dim (ncid, 'xi_rho',   xi_rho,   r2dgrd(1))
        ierr=nf_def_dim (ncid, 'xi_u',     xi_u,     u2dgrd(1))
        ierr=nf_def_dim (ncid, 'eta_rho',  eta_rho,  r2dgrd(2))
        ierr=nf_def_dim (ncid, 'eta_v',    eta_v,    v2dgrd(2))
        ierr=nf_def_dim (ncid, 's_rho',    N,        r3dgrd(3))
        ierr=nf_def_dim (ncid, 's_w',      N+1,      w3dgrd(3))
        ierr=nf_def_dim (ncid, 'time', nf_unlimited, timedim)
        ierr=nf_def_dim (ncid, 'auxil',    4,        auxil(1))
        auxil(2)=timedim
        r2dgrd(3)=timedim
        u2dgrd(2)=r2dgrd(2)
        u2dgrd(3)=timedim
        v2dgrd(1)=r2dgrd(1)
        v2dgrd(3)=timedim
        b3dgrd(1)=r2dgrd(1)
        b3dgrd(2)=r2dgrd(2)
        b3dgrd(4)=timedim
        r3dgrd(1)=r2dgrd(1)
        r3dgrd(2)=r2dgrd(2)
        r3dgrd(4)=timedim
        u3dgrd(1)=u2dgrd(1)
        u3dgrd(2)=r2dgrd(2)
        u3dgrd(3)=r3dgrd(3)
        u3dgrd(4)=timedim
        v3dgrd(1)=r2dgrd(1)
        v3dgrd(2)=v2dgrd(2)
        v3dgrd(3)=r3dgrd(3)
        v3dgrd(4)=timedim
        w3dgrd(1)=r2dgrd(1)
        w3dgrd(2)=r2dgrd(2)
        w3dgrd(4)=timedim
      if (total_rec.le.1) then
         call def_grid_3d(ncid, r2dgrd, u2dgrd, v2dgrd
     &                   ,r3dgrd, w3dgrd)
      endif
        ierr=nf_def_var (ncid, 'time_step', nf_int, 2, auxil,
     &                                                 avgTstep)
        ierr=nf_put_att_text (ncid, avgTstep, 'long_name', 48,
     &       'time step and record numbers from initialization')
        lvar=lenstr(vname(1,indxTime))
        ierr=nf_def_var (ncid, vname(1,indxTime)(1:lvar),
     &                            NF_REAL, 1, timedim, avgTime)
        text='averaged '/ /vname(2,indxTime)
        lvar=lenstr(text)
        ierr=nf_put_att_text (ncid, avgTime, 'long_name', lvar,
     &                                             text(1:lvar))
        lvar=lenstr(vname(3,indxTime))
        ierr=nf_put_att_text (ncid, avgTime, 'units',  lvar,
     &                                vname(3,indxTime)(1:lvar))
        lvar=lenstr(vname(4,indxTime))
        ierr=nf_put_att_text (ncid, avgTime, 'field',  lvar,
     &                                vname(4,indxTime)(1:lvar))
        call nf_add_attribute(ncid, avgTime, indxTime, 5,
     &       NF_REAL, ierr)
        lvar=lenstr(vname(1,indxTime2))
        ierr=nf_def_var (ncid, vname(1,indxTime2)(1:lvar),
     &                            NF_REAL, 1, timedim, avgTime2)
        text='averaged '/ /vname(2,indxTime2)
        lvar=lenstr(text)
        ierr=nf_put_att_text (ncid, avgTime2, 'long_name', lvar,
     &                                             text(1:lvar))
        lvar=lenstr(vname(3,indxTime2))
        ierr=nf_put_att_text (ncid, avgTime2, 'units',  lvar,
     &                                vname(3,indxTime2)(1:lvar))
        lvar=lenstr(vname(4,indxTime2))
        ierr=nf_put_att_text (ncid, avgTime2, 'field',  lvar,
     &                                vname(4,indxTime2)(1:lvar))
        call nf_add_attribute(ncid, avgTime2, indxTime2, 5,
     &       NF_REAL, ierr)
        if (wrtavg(indxZ)) then
          lvar=lenstr(vname(1,indxZ))
          ierr=nf_def_var (ncid, vname(1,indxZ)(1:lvar),
     &                              NF_REAL, 3, r2dgrd, avgZ)
          text='averaged '/ /vname(2,indxZ)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgZ, 'long_name', lvar,
     &                                            text(1:lvar))
          lvar=lenstr(vname(3,indxZ))
          ierr=nf_put_att_text (ncid, avgZ, 'units',     lvar,
     &                                  vname(3,indxZ)(1:lvar))
          lvar=lenstr(vname(4,indxZ))
          ierr=nf_put_att_text (ncid, avgZ, 'field',     lvar,
     &                                  vname(4,indxZ)(1:lvar))
          call nf_add_attribute(ncid, avgZ, indxZ, 5, NF_REAL, ierr)
       endif
        if (wrtavg(indxUb)) then
          lvar=lenstr(vname(1,indxUb))
          ierr=nf_def_var (ncid, vname(1,indxUb)(1:lvar),
     &                             NF_REAL, 3, u2dgrd, avgUb)
          text='averaged '/ /vname(2,indxUb)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgUb, 'long_name', lvar,
     &                                             text(1:lvar))
          lvar=lenstr(vname(3,indxUb))
          ierr=nf_put_att_text (ncid, avgUb, 'units',     lvar,
     &                                  vname(3,indxUb)(1:lvar))
          lvar=lenstr(vname(4,indxUb))
          ierr=nf_put_att_text (ncid, avgUb, 'field',    lvar,
     &                                  vname(4,indxUb)(1:lvar))
          call nf_add_attribute(ncid, avgUb, indxUb, 5, NF_REAL, ierr)
        endif
        if (wrtavg(indxVb)) then
          lvar=lenstr(vname(1,indxVb))
          ierr=nf_def_var (ncid, vname(1,indxVb)(1:lvar),
     &                              NF_REAL, 3, v2dgrd, avgVb)
          text='averaged '/ /vname(2,indxVb)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgVb, 'long_name', lvar,
     &                                             text(1:lvar))
          lvar=lenstr(vname(3,indxVb))
          ierr=nf_put_att_text (ncid, avgVb, 'units',     lvar,
     &                                  vname(3,indxVb)(1:lvar))
          lvar=lenstr(vname(4,indxVb))
          ierr=nf_put_att_text (ncid, avgVb, 'field',     lvar,
     &                                  vname(4,indxVb)(1:lvar))
          call nf_add_attribute(ncid, avgVb, indxVb, 5, NF_REAL, ierr)
        endif
        if (wrtavg(indxU)) then
          lvar=lenstr(vname(1,indxU))
          ierr=nf_def_var (ncid, vname(1,indxU)(1:lvar),
     &                             NF_REAL, 4, u3dgrd, avgU)
          text='averaged '/ /vname(2,indxU)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgU, 'long_name', lvar,
     &                                            text(1:lvar))
          lvar=lenstr(vname(3,indxU))
          ierr=nf_put_att_text (ncid, avgU, 'units',     lvar,
     &                                  vname(3,indxU)(1:lvar))
          lvar=lenstr(vname(4,indxU))
          ierr=nf_put_att_text (ncid, avgU, 'field',     lvar,
     &                                  vname(4,indxU)(1:lvar))
          call nf_add_attribute(ncid, avgU, indxU, 5, NF_REAL, ierr)
        endif
        if (wrtavg(indxV)) then
          lvar=lenstr(vname(1,indxV))
          ierr=nf_def_var (ncid, vname(1,indxV)(1:lvar),
     &                             NF_REAL, 4, v3dgrd, avgV)
          text='averaged '/ /vname(2,indxV)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgV, 'long_name', lvar,
     &                                            text(1:lvar))
          lvar=lenstr(vname(3,indxV))
          ierr=nf_put_att_text (ncid, avgV, 'units',     lvar,
     &                                  vname(3,indxV)(1:lvar))
          lvar=lenstr(vname(4,indxV))
          ierr=nf_put_att_text (ncid, avgV, 'field',     lvar,
     &                                  vname(4,indxV)(1:lvar))
          call nf_add_attribute(ncid, avgV, indxV, 5, NF_REAL, ierr)
        endif
        do itrc=1,NT
          if (wrtavg(indxT+itrc-1)) then
            lvar=lenstr(vname(1,indxT+itrc-1))
            ierr=nf_def_var (ncid, vname(1,indxT+itrc-1)(1:lvar),
     &                             NF_REAL, 4, r3dgrd, avgT(itrc))
            text='averaged '/ /vname(2,indxT+itrc-1)
            lvar=lenstr(text)
            ierr=nf_put_att_text (ncid, avgT(itrc), 'long_name',
     &                                          lvar, text(1:lvar))
            lvar=lenstr(vname(3,indxT+itrc-1))
            ierr=nf_put_att_text (ncid, avgT(itrc), 'units', lvar,
     &                               vname(3,indxT+itrc-1)(1:lvar))
            lvar=lenstr(vname(4,indxT+itrc-1))
            ierr=nf_put_att_text (ncid, avgT(itrc), 'field', lvar,
     &                               vname(4,indxT+itrc-1)(1:lvar))
            call nf_add_attribute(ncid,avgT(itrc),indxT+itrc-1,5,
     &           NF_REAL, ierr)
            endif
      enddo
        if (wrtavg(indxR)) then
          lvar=lenstr(vname(1,indxR))
          ierr=nf_def_var (ncid, vname(1,indxR)(1:lvar),
     &                             NF_REAL, 4, r3dgrd, avgR)
          text='averaged '/ /vname(2,indxR)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgR, 'long_name', lvar,
     &                                            text(1:lvar))
          lvar=lenstr(vname(3,indxR))
          ierr=nf_put_att_text (ncid, avgR, 'units',     lvar,
     &                                  vname(3,indxR)(1:lvar))
          lvar=lenstr(vname(4,indxR))
          ierr=nf_put_att_text (ncid, avgR, 'field',     lvar,
     &                                  vname(4,indxR)(1:lvar))
          call nf_add_attribute(ncid, avgR, indxR, 5, NF_REAL, ierr)
        endif
        if (wrtavg(indxO)) then
          lvar=lenstr(vname(1,indxO))
          ierr=nf_def_var (ncid, vname(1,indxO)(1:lvar),
     &                             NF_REAL, 4, w3dgrd, avgO)
          text='averaged '/ /vname(2,indxO)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgO, 'long_name', lvar,
     &                                            text(1:lvar))
          lvar=lenstr(vname(3,indxO))
          ierr=nf_put_att_text (ncid, avgO, 'units',     lvar,
     &                                  vname(3,indxO)(1:lvar))
          lvar=lenstr(vname(4,indxO))
          ierr=nf_put_att_text (ncid, avgO, 'field',     lvar,
     &                                  vname(4,indxO)(1:lvar))
          call nf_add_attribute(ncid, avgO, indxO, 5, NF_REAL, ierr)
        endif
        if (wrtavg(indxW)) then
          lvar=lenstr(vname(1,indxW))
          ierr=nf_def_var (ncid, vname(1,indxW)(1:lvar),
     &                               NF_REAL, 4, r3dgrd, avgW)
          text='averaged '/ /vname(2,indxW)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgW, 'long_name', lvar,
     &                                            text(1:lvar))
          lvar=lenstr(vname(3,indxW))
          ierr=nf_put_att_text (ncid, avgW, 'units',     lvar,
     &                                  vname(3,indxW)(1:lvar))
          lvar=lenstr(vname(4,indxW))
          ierr=nf_put_att_text (ncid, avgW, 'field',     lvar,
     &                                  vname(4,indxW)(1:lvar))
          call nf_add_attribute(ncid, avgW, indxW, 5, NF_REAL, ierr)
        endif
        if (wrtavg(indxBostr)) then
          lvar=lenstr(vname(1,indxBostr))
          ierr=nf_def_var (ncid, vname(1,indxBostr)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, avgBostr)
          text='averaged '/ /vname(2,indxBostr)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgBostr, 'long_name', lvar,
     &                                               text(1:lvar))
          lvar=lenstr(vname(3,indxBostr))
          ierr=nf_put_att_text (ncid, avgBostr, 'units',     lvar,
     &                                 vname(3,indxBostr)(1:lvar))
          call nf_add_attribute(ncid, avgBostr,indxBostr,5,
     &                          NF_REAL, ierr)
        endif
        if (wrtavg(indxWstr)) then
          lvar=lenstr(vname(1,indxWstr))
          ierr=nf_def_var (ncid, vname(1,indxWstr)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, avgWstr)
          text='averaged '/ /vname(2,indxWstr)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgWstr, 'long_name', lvar,
     &                                               text(1:lvar))
          lvar=lenstr(vname(3,indxWstr))
          ierr=nf_put_att_text (ncid, avgWstr, 'units',     lvar,
     &                                 vname(3,indxWstr)(1:lvar))
          call nf_add_attribute(ncid, avgWstr, indxWstr, 5,
     &                          NF_REAL, ierr)
        endif
        if (wrtavg(indxUWstr)) then
          lvar=lenstr(vname(1,indxUWstr))
          ierr=nf_def_var (ncid, vname(1,indxUWstr)(1:lvar),
     &                             NF_REAL, 3, u2dgrd, avgUWstr)
          text='averaged '/ /vname(2,indxUWstr)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgUWstr, 'long_name', lvar,
     &                                               text(1:lvar))
          lvar=lenstr(vname(3,indxUWstr))
          ierr=nf_put_att_text (ncid, avgUWstr, 'units',     lvar,
     &                                 vname(3,indxUWstr)(1:lvar))
          call nf_add_attribute(ncid, avgUWstr, indxUWstr, 5,
     &                          NF_REAL, ierr)
        endif
        if (wrtavg(indxVWstr)) then
          lvar=lenstr(vname(1,indxVWstr))
          ierr=nf_def_var (ncid, vname(1,indxVWstr)(1:lvar),
     &                             NF_REAL, 3, v2dgrd, avgVWstr)
          text='averaged '/ /vname(2,indxVWstr)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgVWstr, 'long_name', lvar,
     &                                               text(1:lvar))
          lvar=lenstr(vname(3,indxVWstr))
          ierr=nf_put_att_text (ncid, avgVWstr, 'units',     lvar,
     &                                 vname(3,indxVWstr)(1:lvar))
          call nf_add_attribute(ncid, avgVWstr, indxVWstr, 5,
     &                          NF_REAL, ierr)
        endif
        if (wrtavg(indxDiff)) then
          lvar=lenstr(vname(1,indxDiff))
          ierr=nf_def_var (ncid, vname(1,indxDiff)(1:lvar),
     &                             NF_REAL, 4, r3dgrd, avgDiff)
          text='averaged '/ /vname(2,indxDiff)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgDiff, 'long_name', lvar,
     &                                              text(1:lvar))
          lvar=lenstr(vname(4,indxDiff))
          ierr=nf_put_att_text (ncid, avgDiff, 'field',     lvar,
     &                                  vname(4,indxDiff)(1:lvar))
          call nf_add_attribute(ncid, avgDiff, indxDiff, 5,
     &                          NF_REAL, ierr)
       endif
        if (wrtavg(indxAkv)) then
          lvar=lenstr(vname(1,indxAkv))
          ierr=nf_def_var (ncid, vname(1,indxAkv)(1:lvar),
     &                             NF_REAL, 4, w3dgrd, avgAkv)
          text='averaged '/ /vname(2,indxAkv)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgAkv, 'long_name', lvar,
     &                                              text(1:lvar))
          lvar=lenstr(vname(3,indxAkv))
          ierr=nf_put_att_text (ncid, avgAkv, 'units',     lvar,
     &                                  vname(3,indxAkv)(1:lvar))
          lvar=lenstr(vname(4,indxAkv))
          ierr=nf_put_att_text (ncid, avgAkv, 'field',     lvar,
     &                                  vname(4,indxAkv)(1:lvar))
          call nf_add_attribute(ncid, avgAkv, indxAkv, 5, NF_REAL,
     &                                                          ierr)
        endif
        if (wrtavg(indxAkt)) then
          lvar=lenstr(vname(1,indxAkt))
          ierr=nf_def_var (ncid, vname(1,indxAkt)(1:lvar),
     &                              NF_REAL, 4, w3dgrd, avgAkt)
          text='averaged '/ /vname(2,indxAkt)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgAkt, 'long_name', lvar,
     &                                              text(1:lvar))
          lvar=lenstr(vname(3,indxAkt))
          ierr=nf_put_att_text (ncid, avgAkt, 'units',     lvar,
     &                                  vname(3,indxAkt)(1:lvar))
          lvar=lenstr(vname(4,indxAkt))
          ierr=nf_put_att_text (ncid, avgAkt, 'field',     lvar,
     &                                  vname(4,indxAkt)(1:lvar))
          call nf_add_attribute(ncid, avgAkt, indxAkt, 5, NF_REAL,
     &                                                           ierr)
        endif
        if (wrtavg(indxAks)) then
          lvar=lenstr(vname(1,indxAks))
          ierr=nf_def_var (ncid, vname(1,indxAks)(1:lvar),
     &                              NF_REAL, 4, w3dgrd, avgAks)
          text='averaged '/ /vname(2,indxAks)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgAks, 'long_name', lvar,
     &                                              text(1:lvar))
          lvar=lenstr(vname(3,indxAks))
          ierr=nf_put_att_text (ncid, avgAks, 'units',     lvar,
     &                                  vname(3,indxAks)(1:lvar))
          lvar=lenstr(vname(4,indxAks))
          ierr=nf_put_att_text (ncid, avgAks, 'field',     lvar,
     &                                  vname(4,indxAks)(1:lvar))
          call nf_add_attribute(ncid, avgAks, indxAks, 5, NF_REAL,
     &                                                           ierr)
        endif
        if (wrtavg(indxHbl)) then
          lvar=lenstr(vname(1,indxHbl))
          ierr=nf_def_var (ncid, vname(1,indxHbl)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, avgHbl)
          text='averaged '/ /vname(2,indxHbl)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgHbl, 'long_name', lvar,
     &                                              text(1:lvar))
          lvar=lenstr(vname(3,indxHbl))
          ierr=nf_put_att_text (ncid, avgHbl, 'units',     lvar,
     &                                  vname(3,indxHbl)(1:lvar))
          lvar=lenstr(vname(4,indxHbl))
          ierr=nf_put_att_text (ncid, avgHbl, 'field',     lvar,
     &                                  vname(4,indxHbl)(1:lvar))
          call nf_add_attribute(ncid, avgHbl, indxHbl, 5, NF_REAL,
     &                                                            ierr)
        endif
        if (wrtavg(indxHbbl)) then
          lvar=lenstr(vname(1,indxHbbl))
          ierr=nf_def_var (ncid, vname(1,indxHbbl)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, avgHbbl)
          text='averaged '/ /vname(2,indxHbbl)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgHbbl, 'long_name', lvar,
     &                                              text(1:lvar))
          lvar=lenstr(vname(3,indxHbbl))
          ierr=nf_put_att_text (ncid, avgHbbl, 'units',     lvar,
     &                                  vname(3,indxHbbl)(1:lvar))
          lvar=lenstr(vname(4,indxHbbl))
          ierr=nf_put_att_text (ncid, avgHbbl, 'field',     lvar,
     &                                  vname(4,indxHbbl)(1:lvar))
          call nf_add_attribute(ncid, avgHbbl, indxHbbl, 5,
     &                          NF_REAL, ierr)
        endif
        if (wrtavg(indxShflx)) then
          lvar=lenstr(vname(1,indxShflx))
          ierr=nf_def_var (ncid, vname(1,indxShflx)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, avgShflx)
          text='averaged '/ /vname(2,indxShflx)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgShflx, 'long_name', lvar,
     &                                               text(1:lvar))
          lvar=lenstr(vname(3,indxShflx))
          ierr=nf_put_att_text (ncid, avgShflx, 'units',     lvar,
     &                                 vname(3,indxShflx)(1:lvar))
          call nf_add_attribute(ncid, avgShflx, indxShflx, 5,
     &                          NF_REAL, ierr)
        endif
        if (wrtavg(indxSwflx)) then
          lvar=lenstr(vname(1,indxSwflx))
          ierr=nf_def_var (ncid, vname(1,indxSwflx)(1:lvar),
     &                             NF_REAL, 3, r2dgrd, avgSwflx)
          text='averaged '/ /vname(2,indxSwflx)
          lvar=lenstr(text)
          ierr=nf_put_att_text (ncid, avgSwflx, 'long_name', lvar,
     &                                               text(1:lvar))
          lvar=lenstr(vname(3,indxSwflx))
          ierr=nf_put_att_text (ncid, avgSwflx, 'units',     lvar,
     &                                 vname(3,indxSwflx)(1:lvar))
          call nf_add_attribute(ncid, avgSwflx, indxSwflx, 5,
     &                          NF_REAL, ierr)
        endif
      if (wrtavg(indxShflx_rsw)) then
        lvar=lenstr(vname(1,indxShflx_rsw))
        ierr=nf_def_var (ncid, vname(1,indxShflx_rsw)(1:lvar),
     &                           NF_REAL, 3, r2dgrd, avgShflx_rsw)
        text='averaged '/ /vname(2,indxShflx_rsw)
        lvar=lenstr(text)
        ierr=nf_put_att_text (ncid, avgShflx_rsw, 'long_name', lvar,
     &                                               text(1:lvar))
        lvar=lenstr(vname(3,indxShflx_rsw))
        ierr=nf_put_att_text (ncid, avgShflx_rsw, 'units',     lvar,
     &                               vname(3,indxShflx_rsw)(1:lvar))
        call nf_add_attribute(ncid, avgShflx_rsw, indxShflx_rsw,5,
     &                                                   NF_REAL, ierr)
      endif
      if (wrtavg(indxShflx_rlw)) then
        lvar=lenstr(vname(1,indxShflx_rlw))
        ierr=nf_def_var (ncid, vname(1,indxShflx_rlw)(1:lvar),
     &                           NF_REAL, 3, r2dgrd, avgShflx_rlw)
        text='averaged '/ /vname(2,indxShflx_rlw)
        lvar=lenstr(text)
        ierr=nf_put_att_text (ncid, avgShflx_rlw, 'long_name', lvar,
     &                                             text(1:lvar))
        lvar=lenstr(vname(3,indxShflx_rlw))
        ierr=nf_put_att_text (ncid, avgShflx_rlw, 'units',     lvar,
     &                               vname(3,indxShflx_rlw)(1:lvar))
        call nf_add_attribute(ncid, avgShflx_rlw, indxShflx_rlw,5,
     &       NF_REAL, ierr)
      endif
      if (wrtavg(indxShflx_lat)) then
        lvar=lenstr(vname(1,indxShflx_lat))
        ierr=nf_def_var (ncid, vname(1,indxShflx_lat)(1:lvar),
     &                           NF_REAL, 3, r2dgrd, avgShflx_lat)
        text='averaged '/ /vname(2,indxShflx_lat)
        lvar=lenstr(text)
        ierr=nf_put_att_text (ncid, avgShflx_lat, 'long_name', lvar,
     &                                             text(1:lvar))
        lvar=lenstr(vname(3,indxShflx_lat))
        ierr=nf_put_att_text (ncid, avgShflx_lat, 'units',     lvar,
     &                               vname(3,indxShflx_lat)(1:lvar))
        call nf_add_attribute(ncid, avgShflx_lat, indxShflx_lat, 5,
     &       NF_REAL, ierr)
      endif
      if (wrtavg(indxShflx_sen)) then
        lvar=lenstr(vname(1,indxShflx_sen))
        ierr=nf_def_var (ncid, vname(1,indxShflx_sen)(1:lvar),
     &                           NF_REAL, 3, r2dgrd, avgShflx_sen)
        text='averaged '/ /vname(2,indxShflx_sen)
        lvar=lenstr(text)
        ierr=nf_put_att_text (ncid, avgShflx_sen, 'long_name', lvar,
     &                                             text(1:lvar))
        lvar=lenstr(vname(3,indxShflx_sen))
        ierr=nf_put_att_text (ncid, avgShflx_sen, 'units',     lvar,
     &                               vname(3,indxShflx_sen)(1:lvar))
        call nf_add_attribute(ncid, avgShflx_sen, indxShflx_sen, 5,
     &       NF_REAL, ierr)
      endif
        ierr=nf_enddef(ncid)
        write(stdout,'(6x,4A,1x,A,i4)') 'DEF_HIS/AVG - Created ',
     &                'new netCDF file ''', avgname(1:lstr), '''.'
     &                 ,' mynode =', mynode
      elseif (ncid.eq.-1) then
        ierr=nf_open (avgname(1:lstr), nf_write, ncid)
        if (ierr. eq. nf_noerr) then
          ierr=checkdims (ncid, avgname, lstr, rec)
          if (ierr .eq. nf_noerr) then
            if (nrpfavg.eq.0) then
              ierr=rec+1 - total_rec
            else
              ierr=rec+1 - (1+mod(total_rec-1, nrpfavg))
            endif
            if (ierr.gt.0) then
              if (mynode.eq.0) write( stdout,
     &                 '(/1x,A,I5,1x,A/8x,3A,I5,/8x,A,I5,1x,A/)'
     &            ) 'DEF_HIS/AVG WARNING: Actual number of records',
     &               rec,  'in netCDF file',  '''',  avgname(1:lstr),
     &             ''' exceeds the record number from restart data',
     &             rec+1-ierr,'/', total_rec,', restart is assumed.'
              rec=rec-ierr
            elseif (nrpfavg.eq.0) then
              total_rec=rec+1
              if (mynode.gt.0) total_rec=total_rec-1
            endif
            ierr=nf_noerr
          endif
        endif
        if (ierr. ne. nf_noerr) then
          if (mynode.eq.0) then
            create_new_file=.true.
            goto 10
          else
            write(stdout,'(/1x,4A,2x,A,I4/)') 'DEF_HIS/AVG ERROR: ',
     &                  'Cannot open file ''', avgname(1:lstr), '''.'
     &                   ,' mynode =', mynode
            goto 99
          endif
        endif
        ierr=nf_inq_varid (ncid, 'time_step', avgTstep)
        if (ierr .ne. nf_noerr) then
          write(stdout,1) 'time_step', avgname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxTime))
        ierr=nf_inq_varid (ncid,vname(1,indxTime)(1:lvar),avgTime)
        if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxTime)(1:lvar), avgname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxTime2))
        ierr=nf_inq_varid (ncid,vname(1,indxTime2)(1:lvar),avgTime2)
        if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxTime2)(1:lvar), avgname(1:lstr)
          goto 99
        endif
        if (wrtavg(indxZ)) then
          lvar=lenstr(vname(1,indxZ))
          ierr=nf_inq_varid (ncid, vname(1,indxZ)(1:lvar), avgZ)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxZ)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxUb)) then
          lvar=lenstr(vname(1,indxUb))
          ierr=nf_inq_varid (ncid, vname(1,indxUb)(1:lvar), avgUb)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxUb)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxVb)) then
          lvar=lenstr(vname(1,indxVb))
          ierr=nf_inq_varid (ncid, vname(1,indxVb)(1:lvar), avgVb)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxVb)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxBostr)) then
          lvar=lenstr(vname(1,indxBostr))
          ierr=nf_inq_varid (ncid,vname(1,indxBostr)(1:lvar),
     &                                                   avgBostr)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxBostr)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxWstr)) then
          lvar=lenstr(vname(1,indxWstr))
          ierr=nf_inq_varid (ncid,vname(1,indxWstr)(1:lvar),
     &                                                   avgWstr)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxWstr)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxUWstr)) then
          lvar=lenstr(vname(1,indxUWstr))
          ierr=nf_inq_varid (ncid,vname(1,indxUWstr)(1:lvar),
     &                                                   avgUWstr)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxUWstr)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxVWstr)) then
          lvar=lenstr(vname(1,indxVWstr))
          ierr=nf_inq_varid (ncid,vname(1,indxVWstr)(1:lvar),
     &                                                   avgVWstr)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxVWstr)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxU)) then
          lvar=lenstr(vname(1,indxU))
          ierr=nf_inq_varid (ncid, vname(1,indxU)(1:lvar), avgU)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxU)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxV)) then
          lvar=lenstr(vname(1,indxV))
          ierr=nf_inq_varid (ncid, vname(1,indxV)(1:lvar), avgV)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxV)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        do itrc=1,NT
          if (wrtavg(indxT+itrc-1)) then
            lvar=lenstr(vname(1,indxT+itrc-1))
            ierr=nf_inq_varid (ncid, vname(1,indxT+itrc-1)(1:lvar),
     &                                                 avgT(itrc))
            if (ierr .ne. nf_noerr) then
              write(stdout,1) vname(1,indxT+itrc-1)(1:lvar),
     &                                       avgname(1:lstr)
              goto 99
            endif
          endif
        enddo
        if (wrtavg(indxR)) then
          lvar=lenstr(vname(1,indxR))
          ierr=nf_inq_varid (ncid, vname(1,indxR)(1:lvar), avgR)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxR)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxO)) then
          lvar=lenstr(vname(1,indxO))
          ierr=nf_inq_varid (ncid, vname(1,indxO)(1:lvar), avgO)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxO)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxW)) then
          lvar=lenstr(vname(1,indxW))
          ierr=nf_inq_varid (ncid, vname(1,indxW)(1:lvar), avgW)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxW)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxDiff)) then
          lvar=lenstr(vname(1,indxDiff))
          ierr=nf_inq_varid (ncid, vname(1,indxDiff)(1:lvar), avgDiff)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxDiff)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxAkv)) then
          lvar=lenstr(vname(1,indxAkv))
          ierr=nf_inq_varid (ncid, vname(1,indxAkv)(1:lvar), avgAkv)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxAkv)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxAkt)) then
          lvar=lenstr(vname(1,indxAkt))
          ierr=nf_inq_varid (ncid,vname(1,indxAkt)(1:lvar), avgAkt)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxAkt)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxAks)) then
          lvar=lenstr(vname(1,indxAks))
          ierr=nf_inq_varid (ncid,vname(1,indxAks)(1:lvar), avgAks)
          if (ierr .ne. nf_noerr) then
            write(stdout,1) vname(1,indxAks)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxHbl)) then
          lvar=lenstr(vname(1,indxHbl))
          ierr=nf_inq_varid (ncid,vname(1,indxHbl)(1:lvar), avgHbl)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxHbl)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxHbbl)) then
          lvar=lenstr(vname(1,indxHbbl))
          ierr=nf_inq_varid (ncid,vname(1,indxHbbl)(1:lvar), avgHbbl)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxHbbl)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxShflx)) then
          lvar=lenstr(vname(1,indxShflx))
          ierr=nf_inq_varid (ncid,vname(1,indxShflx)(1:lvar),
     &                                                   avgShflx)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxShflx)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxSwflx)) then
          lvar=lenstr(vname(1,indxSwflx))
          ierr=nf_inq_varid (ncid,vname(1,indxSwflx)(1:lvar),
     &                                                   avgSwflx)
          if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxSwflx)(1:lvar), avgname(1:lstr)
            goto 99
          endif
        endif
        if (wrtavg(indxShflx_rsw)) then
         lvar=lenstr(vname(1,indxShflx_rsw))
         ierr=nf_inq_varid (ncid,vname(1,indxShflx_rsw)(1:lvar),
     &                                               avgShflx_rsw)
         if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxShflx_rsw)(1:lvar), 
     &                                                   avgname(1:lstr)
          goto 99
         endif
        endif
        if (wrtavg(indxShflx_rlw)) then
         lvar=lenstr(vname(1,indxShflx_rlw))
         ierr=nf_inq_varid (ncid,vname(1,indxShflx_rlw)(1:lvar),
     &                                               avgShflx_rlw)
         if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxShflx_rlw)(1:lvar), 
     &                                                   avgname(1:lstr)
          goto 99
         endif
        endif
        if (wrtavg(indxShflx_lat)) then
         lvar=lenstr(vname(1,indxShflx_lat))
         ierr=nf_inq_varid (ncid,vname(1,indxShflx_lat)(1:lvar),
     &                                               avgShflx_lat)
         if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxShflx_lat)(1:lvar), 
     &                                                   avgname(1:lstr)
          goto 99
         endif
        endif
        if (wrtavg(indxShflx_sen)) then
         lvar=lenstr(vname(1,indxShflx_sen))
         ierr=nf_inq_varid (ncid,vname(1,indxShflx_sen)(1:lvar),
     &                                               avgShflx_sen)
         if (ierr .ne. nf_noerr) then
          write(stdout,1) vname(1,indxShflx_sen)(1:lvar), 
     &                                                   avgname(1:lstr)
          goto 99
         endif
        endif
      if (mynode.eq.0) write(*,'(6x,2A,i4,1x,A,i4)')
     &                     'DEF_HIS/AVG -- Opened ',
     &                     'existing file  from record =', rec
     &                      ,' mynode =', mynode
      else
        ierr=nf_open (avgname(1:lstr), nf_write, ncid)
        if (ierr .ne. nf_noerr) then
          if (mynode.eq.0) write(stdout,'(/1x,4A,2x,A,I4/)')
     &                'DEF_HIS/AVG ERROR: ',
     &                'Cannot open file ''', avgname(1:lstr), '''.'
     &                 ,' mynode =', mynode
          goto 99
        endif
      endif
      ierr=nf_set_fill (ncid, nf_nofill, lvar)
      if (ierr .ne. nf_noerr) then
        write(*,'(6x,2A,i4,1x,A,i4)') 'DEF_HIS/AVG ERROR: Cannot ',
     &    'switch to ''nf_nofill'' more; netCDF error code =', ierr
      endif
   1  format(/1x,'DEF_HIS/AVG ERROR: Cannot find variable ''',
     &                   A, ''' in netCDF file ''', A, '''.'/)
        if (total_rec.le.1) call wrt_grid (ncid, avgname, lstr)
      if (ntsavg.eq.1) then
        time_avg=time-0.5D0*float(navg)*dt
      else
        time_avg=time-0.5D0*float(navg)*dt+float(ntsavg)*dt
      endif
  99  return
       
      

      end subroutine Sub_Loop_def_avg

