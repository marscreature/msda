












      subroutine get_uclima
      


      use Agrif_Util
      interface
        subroutine Sub_Loop_get_uclima(padd_E,Mm,padd_X,Lm,mynode,vclima
     &,uclima,vbclima,ubclima,tdays,dt,time,uclm_time,ituclm,uclm_rec,uc
     &lm_ncycle,uclm_cycle,ntuclm,vclm_id,uclm_id,vbclm_id,ubclm_id,vnam
     &e,uclm_tid,ncidclm,clmname,iic,may_day_flag)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4), parameter :: nf_byte = 1
      integer(4), parameter :: nf_int1 = nf_byte
      integer(4), parameter :: nf_char = 2
      integer(4), parameter :: nf_short = 3
      integer(4), parameter :: nf_int2 = nf_short
      integer(4), parameter :: nf_int = 4
      integer(4), parameter :: nf_float = 5
      integer(4), parameter :: nf_real = nf_float
      integer(4), parameter :: nf_double = 6
      integer(4), parameter :: nf_fill_byte = -127
      integer(4), parameter :: nf_fill_int1 = nf_fill_byte
      integer(4), parameter :: nf_fill_char = 0
      integer(4), parameter :: nf_fill_short = -32767
      integer(4), parameter :: nf_fill_int2 = nf_fill_short
      integer(4), parameter :: nf_fill_int = -2147483647
      real, parameter :: nf_fill_float = 9.9692099683868690D+36
      real, parameter :: nf_fill_real = nf_fill_float
      real(8), parameter :: nf_fill_double = 9.9692099683868690D+36
      integer(4), parameter :: nf_nowrite = 0
      integer(4), parameter :: nf_write = 1
      integer(4), parameter :: nf_clobber = 0
      integer(4), parameter :: nf_noclobber = 4
      integer(4), parameter :: nf_fill = 0
      integer(4), parameter :: nf_nofill = 256
      integer(4), parameter :: nf_lock = 1024
      integer(4), parameter :: nf_share = 2048
      integer(4), parameter :: nf_64bit_offset = 512
      integer(4), parameter :: nf_sizehint_default = 0
      integer(4), parameter :: nf_align_chunk = -1
      integer(4), parameter :: nf_format_classic = 1
      integer(4), parameter :: nf_format_64bit = 2
      integer(4), parameter :: nf_diskless = 8
      integer(4), parameter :: nf_mmap = 16
      integer(4), parameter :: nf_unlimited = 0
      integer(4), parameter :: nf_global = 0
      integer(4), parameter :: nf_max_dims = 1024
      integer(4), parameter :: nf_max_attrs = 8192
      integer(4), parameter :: nf_max_vars = 8192
      integer(4), parameter :: nf_max_name = 256
      integer(4), parameter :: nf_max_var_dims = nf_max_dims
      integer(4), parameter :: nf_noerr = 0
      integer(4), parameter :: nf_ebadid = -33
      integer(4), parameter :: nf_eexist = -35
      integer(4), parameter :: nf_einval = -36
      integer(4), parameter :: nf_eperm = -37
      integer(4), parameter :: nf_enotindefine = -38
      integer(4), parameter :: nf_eindefine = -39
      integer(4), parameter :: nf_einvalcoords = -40
      integer(4), parameter :: nf_emaxdims = -41
      integer(4), parameter :: nf_enameinuse = -42
      integer(4), parameter :: nf_enotatt = -43
      integer(4), parameter :: nf_emaxatts = -44
      integer(4), parameter :: nf_ebadtype = -45
      integer(4), parameter :: nf_ebaddim = -46
      integer(4), parameter :: nf_eunlimpos = -47
      integer(4), parameter :: nf_emaxvars = -48
      integer(4), parameter :: nf_enotvar = -49
      integer(4), parameter :: nf_eglobal = -50
      integer(4), parameter :: nf_enotnc = -51
      integer(4), parameter :: nf_ests = -52
      integer(4), parameter :: nf_emaxname = -53
      integer(4), parameter :: nf_eunlimit = -54
      integer(4), parameter :: nf_enorecvars = -55
      integer(4), parameter :: nf_echar = -56
      integer(4), parameter :: nf_eedge = -57
      integer(4), parameter :: nf_estride = -58
      integer(4), parameter :: nf_ebadname = -59
      integer(4), parameter :: nf_erange = -60
      integer(4), parameter :: nf_enomem = -61
      integer(4), parameter :: nf_evarsize = -62
      integer(4), parameter :: nf_edimsize = -63
      integer(4), parameter :: nf_etrunc = -64
      integer(4), parameter :: nf_fatal = 1
      integer(4), parameter :: nf_verbose = 2
      integer(4), parameter :: nf_ubyte = 7
      integer(4), parameter :: nf_ushort = 8
      integer(4), parameter :: nf_uint = 9
      integer(4), parameter :: nf_int64 = 10
      integer(4), parameter :: nf_uint64 = 11
      integer(4), parameter :: nf_string = 12
      integer(4), parameter :: nf_vlen = 13
      integer(4), parameter :: nf_opaque = 14
      integer(4), parameter :: nf_enum = 15
      integer(4), parameter :: nf_compound = 16
      integer(4), parameter :: nf_fill_ubyte = 255
      integer(4), parameter :: nf_fill_ushort = 65535
      integer(4), parameter :: nf_format_netcdf4 = 3
      integer(4), parameter :: nf_format_netcdf4_classic = 4
      integer(4), parameter :: nf_netcdf4 = 4096
      integer(4), parameter :: nf_classic_model = 256
      integer(4), parameter :: nf_chunk_seq = 0
      integer(4), parameter :: nf_chunk_sub = 1
      integer(4), parameter :: nf_chunk_sizes = 2
      integer(4), parameter :: nf_endian_native = 0
      integer(4), parameter :: nf_endian_little = 1
      integer(4), parameter :: nf_endian_big = 2
      integer(4), parameter :: nf_chunked = 0
      integer(4), parameter :: nf_contiguous = 1
      integer(4), parameter :: nf_nochecksum = 0
      integer(4), parameter :: nf_fletcher32 = 1
      integer(4), parameter :: nf_noshuffle = 0
      integer(4), parameter :: nf_shuffle = 1
      integer(4), parameter :: nf_szip_ec_option_mask = 4
      integer(4), parameter :: nf_szip_nn_option_mask = 32
      integer(4), parameter :: nf_mpiio = 8192
      integer(4), parameter :: nf_mpiposix = 16384
      integer(4), parameter :: nf_pnetcdf = 32768
      integer(4), parameter :: nf_independent = 0
      integer(4), parameter :: nf_collective = 1
      integer(4), parameter :: nf_ehdferr = -101
      integer(4), parameter :: nf_ecantread = -102
      integer(4), parameter :: nf_ecantwrite = -103
      integer(4), parameter :: nf_ecantcreate = -104
      integer(4), parameter :: nf_efilemeta = -105
      integer(4), parameter :: nf_edimmeta = -106
      integer(4), parameter :: nf_eattmeta = -107
      integer(4), parameter :: nf_evarmeta = -108
      integer(4), parameter :: nf_enocompound = -109
      integer(4), parameter :: nf_eattexists = -110
      integer(4), parameter :: nf_enotnc4 = -111
      integer(4), parameter :: nf_estrictnc3 = -112
      integer(4), parameter :: nf_enotnc3 = -113
      integer(4), parameter :: nf_enopar = -114
      integer(4), parameter :: nf_eparinit = -115
      integer(4), parameter :: nf_ebadgrpid = -116
      integer(4), parameter :: nf_ebadtypid = -117
      integer(4), parameter :: nf_etypdefined = -118
      integer(4), parameter :: nf_ebadfield = -119
      integer(4), parameter :: nf_ebadclass = -120
      integer(4), parameter :: nf_emaptype = -121
      integer(4), parameter :: nf_elatefill = -122
      integer(4), parameter :: nf_elatedef = -123
      integer(4), parameter :: nf_edimscale = -124
      integer(4), parameter :: nf_enogrp = -125
      integer(4), parameter :: ncbyte = 1
      integer(4), parameter :: ncchar = 2
      integer(4), parameter :: ncshort = 3
      integer(4), parameter :: nclong = 4
      integer(4), parameter :: ncfloat = 5
      integer(4), parameter :: ncdouble = 6
      integer(4), parameter :: ncrdwr = 1
      integer(4), parameter :: nccreat = 2
      integer(4), parameter :: ncexcl = 4
      integer(4), parameter :: ncindef = 8
      integer(4), parameter :: ncnsync = 16
      integer(4), parameter :: nchsync = 32
      integer(4), parameter :: ncndirty = 64
      integer(4), parameter :: nchdirty = 128
      integer(4), parameter :: ncfill = 0
      integer(4), parameter :: ncnofill = 256
      integer(4), parameter :: nclink = 32768
      integer(4), parameter :: ncnowrit = 0
      integer(4), parameter :: ncwrite = ncrdwr
      integer(4), parameter :: ncclob = nf_clobber
      integer(4), parameter :: ncnoclob = nf_noclobber
      integer(4), parameter :: ncunlim = 0
      integer(4), parameter :: ncglobal = 0
      integer(4), parameter :: maxncop = 64
      integer(4), parameter :: maxncdim = 1024
      integer(4), parameter :: maxncatt = 8192
      integer(4), parameter :: maxncvar = 8192
      integer(4), parameter :: maxncnam = 256
      integer(4), parameter :: maxvdims = maxncdim
      integer(4), parameter :: ncnoerr = nf_noerr
      integer(4), parameter :: ncebadid = nf_ebadid
      integer(4), parameter :: ncenfile = -31
      integer(4), parameter :: nceexist = nf_eexist
      integer(4), parameter :: nceinval = nf_einval
      integer(4), parameter :: nceperm = nf_eperm
      integer(4), parameter :: ncenotin = nf_enotindefine
      integer(4), parameter :: nceindef = nf_eindefine
      integer(4), parameter :: ncecoord = nf_einvalcoords
      integer(4), parameter :: ncemaxds = nf_emaxdims
      integer(4), parameter :: ncename = nf_enameinuse
      integer(4), parameter :: ncenoatt = nf_enotatt
      integer(4), parameter :: ncemaxat = nf_emaxatts
      integer(4), parameter :: ncebadty = nf_ebadtype
      integer(4), parameter :: ncebadd = nf_ebaddim
      integer(4), parameter :: nceunlim = nf_eunlimpos
      integer(4), parameter :: ncemaxvs = nf_emaxvars
      integer(4), parameter :: ncenotvr = nf_enotvar
      integer(4), parameter :: nceglob = nf_eglobal
      integer(4), parameter :: ncenotnc = nf_enotnc
      integer(4), parameter :: ncests = nf_ests
      integer(4), parameter :: ncentool = nf_emaxname
      integer(4), parameter :: ncfoobar = 32
      integer(4), parameter :: ncsyserr = -31
      integer(4), parameter :: ncfatal = 1
      integer(4), parameter :: ncverbos = 2
      integer(4), parameter :: filbyte = -127
      integer(4), parameter :: filchar = 0
      integer(4), parameter :: filshort = -32767
      integer(4), parameter :: fillong = -2147483647
      real, parameter :: filfloat = 9.9692099683868690D+36
      real(8), parameter :: fildoub = 9.9692099683868690D+36
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: mynode
      real :: tdays
      real :: dt
      real :: time
      integer(4) :: ituclm
      integer(4) :: uclm_rec
      integer(4) :: uclm_ncycle
      real :: uclm_cycle
      integer(4) :: ntuclm
      integer(4) :: vclm_id
      integer(4) :: uclm_id
      integer(4) :: vbclm_id
      integer(4) :: ubclm_id
      integer(4) :: uclm_tid
      integer(4) :: ncidclm
      character(128) :: clmname
      integer(4) :: iic
      integer(4) :: may_day_flag
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: vclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: uclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vbclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: ubclima
      real, dimension(1:2) :: uclm_time
      character(75), dimension(1:20,1:500) :: vname
        end subroutine Sub_Loop_get_uclima

      end interface
      

        call Sub_Loop_get_uclima( Agrif_tabvars_i(188)%iarray0, Agrif_ta
     &bvars_i(197)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_
     &i(200)%iarray0, Agrif_tabvars_i(156)%iarray0, Agrif_tabvars(124)%a
     &rray4, Agrif_tabvars(125)%array4, Agrif_tabvars(127)%array3, Agrif
     &_tabvars(128)%array3, Agrif_tabvars_r(41)%array0, Agrif_tabvars_r(
     &46)%array0, Agrif_tabvars_r(44)%array0, Agrif_tabvars(123)%array1,
     & Agrif_tabvars_i(209)%iarray0, Agrif_tabvars_i(207)%iarray0, Agrif
     &_tabvars_i(208)%iarray0, Agrif_tabvars_r(47)%array0, Agrif_tabvars
     &_i(134)%iarray0, Agrif_tabvars_i(202)%iarray0, Agrif_tabvars_i(203
     &)%iarray0, Agrif_tabvars_i(204)%iarray0, Agrif_tabvars_i(205)%iarr
     &ay0, Agrif_tabvars_c(1)%carray2, Agrif_tabvars_i(206)%iarray0, Agr
     &if_tabvars_i(139)%iarray0, Agrif_tabvars_c(2)%carray0, Agrif_tabva
     &rs_i(182)%iarray0, Agrif_tabvars_i(162)%iarray0)

      end


      subroutine Sub_Loop_get_uclima(padd_E,Mm,padd_X,Lm,mynode,vclima,u
     &clima,vbclima,ubclima,tdays,dt,time,uclm_time,ituclm,uclm_rec,uclm
     &_ncycle,uclm_cycle,ntuclm,vclm_id,uclm_id,vbclm_id,ubclm_id,vname,
     &uclm_tid,ncidclm,clmname,iic,may_day_flag)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4), parameter :: nf_byte = 1
      integer(4), parameter :: nf_int1 = nf_byte
      integer(4), parameter :: nf_char = 2
      integer(4), parameter :: nf_short = 3
      integer(4), parameter :: nf_int2 = nf_short
      integer(4), parameter :: nf_int = 4
      integer(4), parameter :: nf_float = 5
      integer(4), parameter :: nf_real = nf_float
      integer(4), parameter :: nf_double = 6
      integer(4), parameter :: nf_fill_byte = -127
      integer(4), parameter :: nf_fill_int1 = nf_fill_byte
      integer(4), parameter :: nf_fill_char = 0
      integer(4), parameter :: nf_fill_short = -32767
      integer(4), parameter :: nf_fill_int2 = nf_fill_short
      integer(4), parameter :: nf_fill_int = -2147483647
      real, parameter :: nf_fill_float = 9.9692099683868690D+36
      real, parameter :: nf_fill_real = nf_fill_float
      real(8), parameter :: nf_fill_double = 9.9692099683868690D+36
      integer(4), parameter :: nf_nowrite = 0
      integer(4), parameter :: nf_write = 1
      integer(4), parameter :: nf_clobber = 0
      integer(4), parameter :: nf_noclobber = 4
      integer(4), parameter :: nf_fill = 0
      integer(4), parameter :: nf_nofill = 256
      integer(4), parameter :: nf_lock = 1024
      integer(4), parameter :: nf_share = 2048
      integer(4), parameter :: nf_64bit_offset = 512
      integer(4), parameter :: nf_sizehint_default = 0
      integer(4), parameter :: nf_align_chunk = -1
      integer(4), parameter :: nf_format_classic = 1
      integer(4), parameter :: nf_format_64bit = 2
      integer(4), parameter :: nf_diskless = 8
      integer(4), parameter :: nf_mmap = 16
      integer(4), parameter :: nf_unlimited = 0
      integer(4), parameter :: nf_global = 0
      integer(4), parameter :: nf_max_dims = 1024
      integer(4), parameter :: nf_max_attrs = 8192
      integer(4), parameter :: nf_max_vars = 8192
      integer(4), parameter :: nf_max_name = 256
      integer(4), parameter :: nf_max_var_dims = nf_max_dims
      integer(4), parameter :: nf_noerr = 0
      integer(4), parameter :: nf_ebadid = -33
      integer(4), parameter :: nf_eexist = -35
      integer(4), parameter :: nf_einval = -36
      integer(4), parameter :: nf_eperm = -37
      integer(4), parameter :: nf_enotindefine = -38
      integer(4), parameter :: nf_eindefine = -39
      integer(4), parameter :: nf_einvalcoords = -40
      integer(4), parameter :: nf_emaxdims = -41
      integer(4), parameter :: nf_enameinuse = -42
      integer(4), parameter :: nf_enotatt = -43
      integer(4), parameter :: nf_emaxatts = -44
      integer(4), parameter :: nf_ebadtype = -45
      integer(4), parameter :: nf_ebaddim = -46
      integer(4), parameter :: nf_eunlimpos = -47
      integer(4), parameter :: nf_emaxvars = -48
      integer(4), parameter :: nf_enotvar = -49
      integer(4), parameter :: nf_eglobal = -50
      integer(4), parameter :: nf_enotnc = -51
      integer(4), parameter :: nf_ests = -52
      integer(4), parameter :: nf_emaxname = -53
      integer(4), parameter :: nf_eunlimit = -54
      integer(4), parameter :: nf_enorecvars = -55
      integer(4), parameter :: nf_echar = -56
      integer(4), parameter :: nf_eedge = -57
      integer(4), parameter :: nf_estride = -58
      integer(4), parameter :: nf_ebadname = -59
      integer(4), parameter :: nf_erange = -60
      integer(4), parameter :: nf_enomem = -61
      integer(4), parameter :: nf_evarsize = -62
      integer(4), parameter :: nf_edimsize = -63
      integer(4), parameter :: nf_etrunc = -64
      integer(4), parameter :: nf_fatal = 1
      integer(4), parameter :: nf_verbose = 2
      integer(4), parameter :: nf_ubyte = 7
      integer(4), parameter :: nf_ushort = 8
      integer(4), parameter :: nf_uint = 9
      integer(4), parameter :: nf_int64 = 10
      integer(4), parameter :: nf_uint64 = 11
      integer(4), parameter :: nf_string = 12
      integer(4), parameter :: nf_vlen = 13
      integer(4), parameter :: nf_opaque = 14
      integer(4), parameter :: nf_enum = 15
      integer(4), parameter :: nf_compound = 16
      integer(4), parameter :: nf_fill_ubyte = 255
      integer(4), parameter :: nf_fill_ushort = 65535
      integer(4), parameter :: nf_format_netcdf4 = 3
      integer(4), parameter :: nf_format_netcdf4_classic = 4
      integer(4), parameter :: nf_netcdf4 = 4096
      integer(4), parameter :: nf_classic_model = 256
      integer(4), parameter :: nf_chunk_seq = 0
      integer(4), parameter :: nf_chunk_sub = 1
      integer(4), parameter :: nf_chunk_sizes = 2
      integer(4), parameter :: nf_endian_native = 0
      integer(4), parameter :: nf_endian_little = 1
      integer(4), parameter :: nf_endian_big = 2
      integer(4), parameter :: nf_chunked = 0
      integer(4), parameter :: nf_contiguous = 1
      integer(4), parameter :: nf_nochecksum = 0
      integer(4), parameter :: nf_fletcher32 = 1
      integer(4), parameter :: nf_noshuffle = 0
      integer(4), parameter :: nf_shuffle = 1
      integer(4), parameter :: nf_szip_ec_option_mask = 4
      integer(4), parameter :: nf_szip_nn_option_mask = 32
      integer(4), parameter :: nf_mpiio = 8192
      integer(4), parameter :: nf_mpiposix = 16384
      integer(4), parameter :: nf_pnetcdf = 32768
      integer(4), parameter :: nf_independent = 0
      integer(4), parameter :: nf_collective = 1
      integer(4), parameter :: nf_ehdferr = -101
      integer(4), parameter :: nf_ecantread = -102
      integer(4), parameter :: nf_ecantwrite = -103
      integer(4), parameter :: nf_ecantcreate = -104
      integer(4), parameter :: nf_efilemeta = -105
      integer(4), parameter :: nf_edimmeta = -106
      integer(4), parameter :: nf_eattmeta = -107
      integer(4), parameter :: nf_evarmeta = -108
      integer(4), parameter :: nf_enocompound = -109
      integer(4), parameter :: nf_eattexists = -110
      integer(4), parameter :: nf_enotnc4 = -111
      integer(4), parameter :: nf_estrictnc3 = -112
      integer(4), parameter :: nf_enotnc3 = -113
      integer(4), parameter :: nf_enopar = -114
      integer(4), parameter :: nf_eparinit = -115
      integer(4), parameter :: nf_ebadgrpid = -116
      integer(4), parameter :: nf_ebadtypid = -117
      integer(4), parameter :: nf_etypdefined = -118
      integer(4), parameter :: nf_ebadfield = -119
      integer(4), parameter :: nf_ebadclass = -120
      integer(4), parameter :: nf_emaptype = -121
      integer(4), parameter :: nf_elatefill = -122
      integer(4), parameter :: nf_elatedef = -123
      integer(4), parameter :: nf_edimscale = -124
      integer(4), parameter :: nf_enogrp = -125
      integer(4), parameter :: ncbyte = 1
      integer(4), parameter :: ncchar = 2
      integer(4), parameter :: ncshort = 3
      integer(4), parameter :: nclong = 4
      integer(4), parameter :: ncfloat = 5
      integer(4), parameter :: ncdouble = 6
      integer(4), parameter :: ncrdwr = 1
      integer(4), parameter :: nccreat = 2
      integer(4), parameter :: ncexcl = 4
      integer(4), parameter :: ncindef = 8
      integer(4), parameter :: ncnsync = 16
      integer(4), parameter :: nchsync = 32
      integer(4), parameter :: ncndirty = 64
      integer(4), parameter :: nchdirty = 128
      integer(4), parameter :: ncfill = 0
      integer(4), parameter :: ncnofill = 256
      integer(4), parameter :: nclink = 32768
      integer(4), parameter :: ncnowrit = 0
      integer(4), parameter :: ncwrite = ncrdwr
      integer(4), parameter :: ncclob = nf_clobber
      integer(4), parameter :: ncnoclob = nf_noclobber
      integer(4), parameter :: ncunlim = 0
      integer(4), parameter :: ncglobal = 0
      integer(4), parameter :: maxncop = 64
      integer(4), parameter :: maxncdim = 1024
      integer(4), parameter :: maxncatt = 8192
      integer(4), parameter :: maxncvar = 8192
      integer(4), parameter :: maxncnam = 256
      integer(4), parameter :: maxvdims = maxncdim
      integer(4), parameter :: ncnoerr = nf_noerr
      integer(4), parameter :: ncebadid = nf_ebadid
      integer(4), parameter :: ncenfile = -31
      integer(4), parameter :: nceexist = nf_eexist
      integer(4), parameter :: nceinval = nf_einval
      integer(4), parameter :: nceperm = nf_eperm
      integer(4), parameter :: ncenotin = nf_enotindefine
      integer(4), parameter :: nceindef = nf_eindefine
      integer(4), parameter :: ncecoord = nf_einvalcoords
      integer(4), parameter :: ncemaxds = nf_emaxdims
      integer(4), parameter :: ncename = nf_enameinuse
      integer(4), parameter :: ncenoatt = nf_enotatt
      integer(4), parameter :: ncemaxat = nf_emaxatts
      integer(4), parameter :: ncebadty = nf_ebadtype
      integer(4), parameter :: ncebadd = nf_ebaddim
      integer(4), parameter :: nceunlim = nf_eunlimpos
      integer(4), parameter :: ncemaxvs = nf_emaxvars
      integer(4), parameter :: ncenotvr = nf_enotvar
      integer(4), parameter :: nceglob = nf_eglobal
      integer(4), parameter :: ncenotnc = nf_enotnc
      integer(4), parameter :: ncests = nf_ests
      integer(4), parameter :: ncentool = nf_emaxname
      integer(4), parameter :: ncfoobar = 32
      integer(4), parameter :: ncsyserr = -31
      integer(4), parameter :: ncfatal = 1
      integer(4), parameter :: ncverbos = 2
      integer(4), parameter :: filbyte = -127
      integer(4), parameter :: filchar = 0
      integer(4), parameter :: filshort = -32767
      integer(4), parameter :: fillong = -2147483647
      real, parameter :: filfloat = 9.9692099683868690D+36
      real(8), parameter :: fildoub = 9.9692099683868690D+36
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: mynode
      real :: tdays
      real :: dt
      real :: time
      integer(4) :: ituclm
      integer(4) :: uclm_rec
      integer(4) :: uclm_ncycle
      real :: uclm_cycle
      integer(4) :: ntuclm
      integer(4) :: vclm_id
      integer(4) :: uclm_id
      integer(4) :: vbclm_id
      integer(4) :: ubclm_id
      integer(4) :: uclm_tid
      integer(4) :: ncidclm
      character(128) :: clmname
      integer(4) :: iic
      integer(4) :: may_day_flag
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: vclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: uclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vbclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: ubclima
      real, dimension(1:2) :: uclm_time
      character(75), dimension(1:20,1:500) :: vname
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
               
      real :: cff
                                                                   
      integer(4) :: i
      integer(4) :: lstr
      integer(4) :: lvar
      integer(4) :: lenstr
      integer(4) :: ierr
      integer(4) :: nf_fread
      integer(4) :: advance_cycle
                        
                        
                        
                         
                        
                       
                         
                        
                          
                             

                                   

                             

                              

                                    

                            

                              

                                    

                               

                                       
                                       
                                       
                                        
                                       
                                      
                                      
                                     
                                       
                                     

                                             

                                  

                                        

                                              

                                           

                                                        

                                              

                                                         

                           
                         
                           
                             
                        
                          
                        
                         
                                
                                    
                               
                                  
                                
                            
                        
                                

                              

                                

                                  

                             

                                 

                                

                                 

                                       

                                         

                                     

                                       

                                     

                                 

                              

                             
                                  

                          
                               

                            
                             
                            
                            
                                
                                    

                                     

                                    

                                   

                                               

                         
                          
                          
                          
                         
                                
                             
                                
                            
                              
                           
                            
                            
                           
                             
                            
                           
                           
                          
                        
                            
                            
                              
                         
                         
                           
                            
                          
                          
                            
                            
                          
                              

                                 

                                 

                                 

                                

                                       

                                    

                                       

                                   

                                     

                                  

                                   

                                   

                                  

                                    

                                   

                                  

                                  

                                 

                               

                                   

                                   

                                     

                                

                                

                                  

                                   

                                 

                                 

                                   

                                   

                                 

                          
                           
                              

                                

                                    
      character(80) :: nf_inq_libvers
      external       nf_inq_libvers
                                 
      character(80) :: nf_strerror
      external       nf_strerror
                                 
      logical :: nf_issyserr
      external       nf_issyserr
                                       
      integer(4) :: nf_inq_base_pe
      external        nf_inq_base_pe
                                       
      integer(4) :: nf_set_base_pe
      external        nf_set_base_pe
                                  
      integer(4) :: nf_create
      external        nf_create
                                   
      integer(4) :: nf__create
      external        nf__create
                                      
      integer(4) :: nf__create_mp
      external        nf__create_mp
                                
      integer(4) :: nf_open
      external        nf_open
                                 
      integer(4) :: nf__open
      external        nf__open
                                    
      integer(4) :: nf__open_mp
      external        nf__open_mp
                                    
      integer(4) :: nf_set_fill
      external        nf_set_fill
                                              
      integer(4) :: nf_set_default_format
      external        nf_set_default_format
                                 
      integer(4) :: nf_redef
      external        nf_redef
                                  
      integer(4) :: nf_enddef
      external        nf_enddef
                                   
      integer(4) :: nf__enddef
      external        nf__enddef
                                
      integer(4) :: nf_sync
      external        nf_sync
                                 
      integer(4) :: nf_abort
      external        nf_abort
                                 
      integer(4) :: nf_close
      external        nf_close
                                  
      integer(4) :: nf_delete
      external        nf_delete
                               
      integer(4) :: nf_inq
      external        nf_inq
                            
      integer(4) :: nf_inq_path
      external nf_inq_path
                                     
      integer(4) :: nf_inq_ndims
      external        nf_inq_ndims
                                     
      integer(4) :: nf_inq_nvars
      external        nf_inq_nvars
                                     
      integer(4) :: nf_inq_natts
      external        nf_inq_natts
                                        
      integer(4) :: nf_inq_unlimdim
      external        nf_inq_unlimdim
                                      
      integer(4) :: nf_inq_format
      external        nf_inq_format
                                   
      integer(4) :: nf_def_dim
      external        nf_def_dim
                                     
      integer(4) :: nf_inq_dimid
      external        nf_inq_dimid
                                   
      integer(4) :: nf_inq_dim
      external        nf_inq_dim
                                       
      integer(4) :: nf_inq_dimname
      external        nf_inq_dimname
                                      
      integer(4) :: nf_inq_dimlen
      external        nf_inq_dimlen
                                      
      integer(4) :: nf_rename_dim
      external        nf_rename_dim
                                   
      integer(4) :: nf_inq_att
      external        nf_inq_att
                                     
      integer(4) :: nf_inq_attid
      external        nf_inq_attid
                                       
      integer(4) :: nf_inq_atttype
      external        nf_inq_atttype
                                      
      integer(4) :: nf_inq_attlen
      external        nf_inq_attlen
                                       
      integer(4) :: nf_inq_attname
      external        nf_inq_attname
                                    
      integer(4) :: nf_copy_att
      external        nf_copy_att
                                      
      integer(4) :: nf_rename_att
      external        nf_rename_att
                                   
      integer(4) :: nf_del_att
      external        nf_del_att
                                        
      integer(4) :: nf_put_att_text
      external        nf_put_att_text
                                        
      integer(4) :: nf_get_att_text
      external        nf_get_att_text
                                        
      integer(4) :: nf_put_att_int1
      external        nf_put_att_int1
                                        
      integer(4) :: nf_get_att_int1
      external        nf_get_att_int1
                                        
      integer(4) :: nf_put_att_int2
      external        nf_put_att_int2
                                        
      integer(4) :: nf_get_att_int2
      external        nf_get_att_int2
                                       
      integer(4) :: nf_put_att_int
      external        nf_put_att_int
                                       
      integer(4) :: nf_get_att_int
      external        nf_get_att_int
                                        
      integer(4) :: nf_put_att_real
      external        nf_put_att_real
                                        
      integer(4) :: nf_get_att_real
      external        nf_get_att_real
                                          
      integer(4) :: nf_put_att_double
      external        nf_put_att_double
                                          
      integer(4) :: nf_get_att_double
      external        nf_get_att_double
                                   
      integer(4) :: nf_def_var
      external        nf_def_var
                                   
      integer(4) :: nf_inq_var
      external        nf_inq_var
                                     
      integer(4) :: nf_inq_varid
      external        nf_inq_varid
                                       
      integer(4) :: nf_inq_varname
      external        nf_inq_varname
                                       
      integer(4) :: nf_inq_vartype
      external        nf_inq_vartype
                                        
      integer(4) :: nf_inq_varndims
      external        nf_inq_varndims
                                        
      integer(4) :: nf_inq_vardimid
      external        nf_inq_vardimid
                                        
      integer(4) :: nf_inq_varnatts
      external        nf_inq_varnatts
                                      
      integer(4) :: nf_rename_var
      external        nf_rename_var
                                    
      integer(4) :: nf_copy_var
      external        nf_copy_var
                                        
      integer(4) :: nf_put_var_text
      external        nf_put_var_text
                                        
      integer(4) :: nf_get_var_text
      external        nf_get_var_text
                                        
      integer(4) :: nf_put_var_int1
      external        nf_put_var_int1
                                        
      integer(4) :: nf_get_var_int1
      external        nf_get_var_int1
                                        
      integer(4) :: nf_put_var_int2
      external        nf_put_var_int2
                                        
      integer(4) :: nf_get_var_int2
      external        nf_get_var_int2
                                       
      integer(4) :: nf_put_var_int
      external        nf_put_var_int
                                       
      integer(4) :: nf_get_var_int
      external        nf_get_var_int
                                        
      integer(4) :: nf_put_var_real
      external        nf_put_var_real
                                        
      integer(4) :: nf_get_var_real
      external        nf_get_var_real
                                          
      integer(4) :: nf_put_var_double
      external        nf_put_var_double
                                          
      integer(4) :: nf_get_var_double
      external        nf_get_var_double
                                         
      integer(4) :: nf_put_var1_text
      external        nf_put_var1_text
                                         
      integer(4) :: nf_get_var1_text
      external        nf_get_var1_text
                                         
      integer(4) :: nf_put_var1_int1
      external        nf_put_var1_int1
                                         
      integer(4) :: nf_get_var1_int1
      external        nf_get_var1_int1
                                         
      integer(4) :: nf_put_var1_int2
      external        nf_put_var1_int2
                                         
      integer(4) :: nf_get_var1_int2
      external        nf_get_var1_int2
                                        
      integer(4) :: nf_put_var1_int
      external        nf_put_var1_int
                                        
      integer(4) :: nf_get_var1_int
      external        nf_get_var1_int
                                         
      integer(4) :: nf_put_var1_real
      external        nf_put_var1_real
                                         
      integer(4) :: nf_get_var1_real
      external        nf_get_var1_real
                                           
      integer(4) :: nf_put_var1_double
      external        nf_put_var1_double
                                           
      integer(4) :: nf_get_var1_double
      external        nf_get_var1_double
                                         
      integer(4) :: nf_put_vara_text
      external        nf_put_vara_text
                                         
      integer(4) :: nf_get_vara_text
      external        nf_get_vara_text
                                         
      integer(4) :: nf_put_vara_int1
      external        nf_put_vara_int1
                                         
      integer(4) :: nf_get_vara_int1
      external        nf_get_vara_int1
                                         
      integer(4) :: nf_put_vara_int2
      external        nf_put_vara_int2
                                         
      integer(4) :: nf_get_vara_int2
      external        nf_get_vara_int2
                                        
      integer(4) :: nf_put_vara_int
      external        nf_put_vara_int
                                        
      integer(4) :: nf_get_vara_int
      external        nf_get_vara_int
                                         
      integer(4) :: nf_put_vara_real
      external        nf_put_vara_real
                                         
      integer(4) :: nf_get_vara_real
      external        nf_get_vara_real
                                           
      integer(4) :: nf_put_vara_double
      external        nf_put_vara_double
                                           
      integer(4) :: nf_get_vara_double
      external        nf_get_vara_double
                                         
      integer(4) :: nf_put_vars_text
      external        nf_put_vars_text
                                         
      integer(4) :: nf_get_vars_text
      external        nf_get_vars_text
                                         
      integer(4) :: nf_put_vars_int1
      external        nf_put_vars_int1
                                         
      integer(4) :: nf_get_vars_int1
      external        nf_get_vars_int1
                                         
      integer(4) :: nf_put_vars_int2
      external        nf_put_vars_int2
                                         
      integer(4) :: nf_get_vars_int2
      external        nf_get_vars_int2
                                        
      integer(4) :: nf_put_vars_int
      external        nf_put_vars_int
                                        
      integer(4) :: nf_get_vars_int
      external        nf_get_vars_int
                                         
      integer(4) :: nf_put_vars_real
      external        nf_put_vars_real
                                         
      integer(4) :: nf_get_vars_real
      external        nf_get_vars_real
                                           
      integer(4) :: nf_put_vars_double
      external        nf_put_vars_double
                                           
      integer(4) :: nf_get_vars_double
      external        nf_get_vars_double
                                         
      integer(4) :: nf_put_varm_text
      external        nf_put_varm_text
                                         
      integer(4) :: nf_get_varm_text
      external        nf_get_varm_text
                                         
      integer(4) :: nf_put_varm_int1
      external        nf_put_varm_int1
                                         
      integer(4) :: nf_get_varm_int1
      external        nf_get_varm_int1
                                         
      integer(4) :: nf_put_varm_int2
      external        nf_put_varm_int2
                                         
      integer(4) :: nf_get_varm_int2
      external        nf_get_varm_int2
                                        
      integer(4) :: nf_put_varm_int
      external        nf_put_varm_int
                                        
      integer(4) :: nf_get_varm_int
      external        nf_get_varm_int
                                         
      integer(4) :: nf_put_varm_real
      external        nf_put_varm_real
                                         
      integer(4) :: nf_get_varm_real
      external        nf_get_varm_real
                                           
      integer(4) :: nf_put_varm_double
      external        nf_put_varm_double
                                           
      integer(4) :: nf_get_varm_double
      external        nf_get_varm_double
                         
                          
                        
                         
                          
                          
                        
                          
                        
                            
                              

                               

                             

                               

                                

                                

                              

                                

                              

                                  

                                        
                                         
                                     

                                        

                                  
                                       

                                          
                                               

                           
                                   

                                 
                                        

                             
                                  

                             
                                  

                               
                                    

                                 
                                      

                                 
                                      

                              
                                   

                           
                                

                              
                                   

                              
                                   

                              
                                   

                             
                                  

                           
                                

                                       
                                            

                                       
                                             

                         
                                 

                            
                                     

                           
                                    

                               
                                    

                              
                                   

                           
                                   

                             
                                     

                              
                                      

                               
                                       

                             
                                     

                            
                                    

                            
                                    

                            
                                    

                               
                                       

                              
                                      

                           
                                   

                              
                                      

                           
                                   

                          
                                  

                            
                                    

                             
                                     

                             
                                     

                               
                                       

                             
                                     

                             
                                     

                            
                                    

                             
                                     

                            
                                    

                             
                                     

                          
                                  

                              
      integer(4) :: nf_create_par
      external nf_create_par
                            
      integer(4) :: nf_open_par
      external nf_open_par
                                  
      integer(4) :: nf_var_par_access
      external nf_var_par_access
                            
      integer(4) :: nf_inq_ncid
      external nf_inq_ncid
                            
      integer(4) :: nf_inq_grps
      external nf_inq_grps
                               
      integer(4) :: nf_inq_grpname
      external nf_inq_grpname
                                    
      integer(4) :: nf_inq_grpname_full
      external nf_inq_grpname_full
                                   
      integer(4) :: nf_inq_grpname_len
      external nf_inq_grpname_len
                                  
      integer(4) :: nf_inq_grp_parent
      external nf_inq_grp_parent
                                
      integer(4) :: nf_inq_grp_ncid
      external nf_inq_grp_ncid
                                     
      integer(4) :: nf_inq_grp_full_ncid
      external nf_inq_grp_full_ncid
                              
      integer(4) :: nf_inq_varids
      external nf_inq_varids
                              
      integer(4) :: nf_inq_dimids
      external nf_inq_dimids
                           
      integer(4) :: nf_def_grp
      external nf_def_grp
                              
      integer(4) :: nf_rename_grp
      external nf_rename_grp
                                   
      integer(4) :: nf_def_var_deflate
      external nf_def_var_deflate
                                   
      integer(4) :: nf_inq_var_deflate
      external nf_inq_var_deflate
                                      
      integer(4) :: nf_def_var_fletcher32
      external nf_def_var_fletcher32
                                      
      integer(4) :: nf_inq_var_fletcher32
      external nf_inq_var_fletcher32
                                    
      integer(4) :: nf_def_var_chunking
      external nf_def_var_chunking
                                    
      integer(4) :: nf_inq_var_chunking
      external nf_inq_var_chunking
                                
      integer(4) :: nf_def_var_fill
      external nf_def_var_fill
                                
      integer(4) :: nf_inq_var_fill
      external nf_inq_var_fill
                                  
      integer(4) :: nf_def_var_endian
      external nf_def_var_endian
                                  
      integer(4) :: nf_inq_var_endian
      external nf_inq_var_endian
                               
      integer(4) :: nf_inq_typeids
      external nf_inq_typeids
                              
      integer(4) :: nf_inq_typeid
      external nf_inq_typeid
                            
      integer(4) :: nf_inq_type
      external nf_inq_type
                                 
      integer(4) :: nf_inq_user_type
      external nf_inq_user_type
                                
      integer(4) :: nf_def_compound
      external nf_def_compound
                                   
      integer(4) :: nf_insert_compound
      external nf_insert_compound
                                         
      integer(4) :: nf_insert_array_compound
      external nf_insert_array_compound
                                
      integer(4) :: nf_inq_compound
      external nf_inq_compound
                                     
      integer(4) :: nf_inq_compound_name
      external nf_inq_compound_name
                                     
      integer(4) :: nf_inq_compound_size
      external nf_inq_compound_size
                                        
      integer(4) :: nf_inq_compound_nfields
      external nf_inq_compound_nfields
                                      
      integer(4) :: nf_inq_compound_field
      external nf_inq_compound_field
                                          
      integer(4) :: nf_inq_compound_fieldname
      external nf_inq_compound_fieldname
                                           
      integer(4) :: nf_inq_compound_fieldindex
      external nf_inq_compound_fieldindex
                                            
      integer(4) :: nf_inq_compound_fieldoffset
      external nf_inq_compound_fieldoffset
                                          
      integer(4) :: nf_inq_compound_fieldtype
      external nf_inq_compound_fieldtype
                                           
      integer(4) :: nf_inq_compound_fieldndims
      external nf_inq_compound_fieldndims
                                               
      integer(4) :: nf_inq_compound_fielddim_sizes
      external nf_inq_compound_fielddim_sizes
                            
      integer(4) :: nf_def_vlen
      external nf_def_vlen
                            
      integer(4) :: nf_inq_vlen
      external nf_inq_vlen
                             
      integer(4) :: nf_free_vlen
      external nf_free_vlen
                            
      integer(4) :: nf_def_enum
      external nf_def_enum
                               
      integer(4) :: nf_insert_enum
      external nf_insert_enum
                            
      integer(4) :: nf_inq_enum
      external nf_inq_enum
                                   
      integer(4) :: nf_inq_enum_member
      external nf_inq_enum_member
                                  
      integer(4) :: nf_inq_enum_ident
      external nf_inq_enum_ident
                              
      integer(4) :: nf_def_opaque
      external nf_def_opaque
                              
      integer(4) :: nf_inq_opaque
      external nf_inq_opaque
                           
      integer(4) :: nf_put_att
      external nf_put_att
                           
      integer(4) :: nf_get_att
      external nf_get_att
                           
      integer(4) :: nf_put_var
      external nf_put_var
                            
      integer(4) :: nf_put_var1
      external nf_put_var1
                            
      integer(4) :: nf_put_vara
      external nf_put_vara
                            
      integer(4) :: nf_put_vars
      external nf_put_vars
                           
      integer(4) :: nf_get_var
      external nf_get_var
                            
      integer(4) :: nf_get_var1
      external nf_get_var1
                            
      integer(4) :: nf_get_vara
      external nf_get_vara
                            
      integer(4) :: nf_get_vars
      external nf_get_vars
                                  
      integer(4) :: nf_put_var1_int64
      external nf_put_var1_int64
                                  
      integer(4) :: nf_put_vara_int64
      external nf_put_vara_int64
                                  
      integer(4) :: nf_put_vars_int64
      external nf_put_vars_int64
                                  
      integer(4) :: nf_put_varm_int64
      external nf_put_varm_int64
                                 
      integer(4) :: nf_put_var_int64
      external nf_put_var_int64
                                  
      integer(4) :: nf_get_var1_int64
      external nf_get_var1_int64
                                  
      integer(4) :: nf_get_vara_int64
      external nf_get_vara_int64
                                  
      integer(4) :: nf_get_vars_int64
      external nf_get_vars_int64
                                  
      integer(4) :: nf_get_varm_int64
      external nf_get_varm_int64
                                 
      integer(4) :: nf_get_var_int64
      external nf_get_var_int64
                                    
      integer(4) :: nf_get_vlen_element
      external nf_get_vlen_element
                                    
      integer(4) :: nf_put_vlen_element
      external nf_put_vlen_element
                                   
      integer(4) :: nf_set_chunk_cache
      external nf_set_chunk_cache
                                   
      integer(4) :: nf_get_chunk_cache
      external nf_get_chunk_cache
                                       
      integer(4) :: nf_set_var_chunk_cache
      external nf_set_var_chunk_cache
                                       
      integer(4) :: nf_get_var_chunk_cache
      external nf_get_var_chunk_cache
                      
      integer(4) :: nccre
                      
      integer(4) :: ncopn
                       
      integer(4) :: ncddef
                      
      integer(4) :: ncdid
                       
      integer(4) :: ncvdef
                      
      integer(4) :: ncvid
                       
      integer(4) :: nctlen
                       
      integer(4) :: ncsfil
      external nccre
      external ncopn
      external ncddef
      external ncdid
      external ncvdef
      external ncvid
      external nctlen
      external ncsfil
                       
                        
                       
                        
                        
                        
                         
                         
                       
                         
                        
                       
                         
                         
                       
                         
                        
                         
                         
                         
                         
                         
                        
                         
                         
                         
                         
                        
                         
                         
                         
                         
                        
                         
                         
                         
                        
                       
                         
                         
                         
                        
                         
                         
                         
                        
                         
                         
                       
                       
                        
                       
                        
                         
                           

                           

                            

                           

                            

                             

                           

                            

                           

                            

                             

                             

                              

                               

                           

                               

                               

                             

                                 

                                    

                                        

                        
                            

                              

                             

                                

                                

                                

                               

                                    

                                   

                                     

                               

                                     

                                     

                                   

                                            

                                        

                                           

                                       

                                        

                                      

                                       

                                       

                                     

                                        

                                       

                                      

                                     

                                     

                                 

                                        

                              

                               

                            

                             

                        
                        
                         
                        
                    
                              
                                

                             

                                   

                                       

                                                   

                                                  

      if (may_day_flag.ne.0) return
      if (iic.eq.0 ) then
        lstr=lenstr(clmname)
        if (ncidclm.eq.-1) then
          ierr=nf_open (clmname(1:lstr), nf_nowrite, ncidclm)
          if (ierr .ne. nf_noerr) goto 4
        endif
        ierr=nf_inq_varid (ncidclm, 'uclm_time', uclm_tid)
        if (ierr .ne. nf_noerr) then
          write(stdout,3) 'uclm_time', clmname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxUb))
        ierr=nf_inq_varid (ncidclm, vname(1,indxUb)(1:lvar), ubclm_id)
        if (ierr .ne. nf_noerr) then
          write(stdout,3) vname(1,indxUb)(1:lvar), clmname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxVb))
        ierr=nf_inq_varid (ncidclm, vname(1,indxVb)(1:lvar), vbclm_id)
        if (ierr .ne. nf_noerr) then
          write(stdout,3) vname(1,indxVb)(1:lvar), clmname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxU))
        ierr=nf_inq_varid (ncidclm, vname(1,indxU)(1:lvar), uclm_id)
        if (ierr .ne. nf_noerr) then
          write(stdout,3) vname(1,indxU)(1:lvar), clmname(1:lstr)
          goto 99
        endif
        lvar=lenstr(vname(1,indxV))
        ierr=nf_inq_varid (ncidclm, vname(1,indxV)(1:lvar), vclm_id)
        if (ierr .ne. nf_noerr) then
          write(stdout,3) vname(1,indxV)(1:lvar), clmname(1:lstr)
          goto 99
        endif
          call set_cycle (ncidclm, uclm_tid, ntuclm,
     &                    uclm_cycle, uclm_ncycle, uclm_rec)
          if (may_day_flag.ne.0) return
          ituclm=2
          uclm_time(1)=-1.D+20
          uclm_time(2)=-1.D+20
      endif
 10   i=3-ituclm
      cff=time+0.5D0*dt
      if (uclm_time(i).le.cff .and.
     &  cff.lt.uclm_time(ituclm)) goto 1
      ierr=advance_cycle (uclm_cycle,  ntuclm,
     &                    uclm_ncycle, uclm_rec)
      if (ierr.ne.0) then
        write(stdout,7) uclm_rec, ntuclm,
     &                  clmname(1:lstr), tdays,
     &                  uclm_time(ituclm)*sec2day
        goto 99
      endif
      ierr=nf_get_var1_double(ncidclm, uclm_tid,
     &                             uclm_rec, cff)
      if (ierr.ne.NF_NOERR) then
        write(stdout,6) 'Xclm_time', uclm_rec
        goto 99
      endif
      uclm_time(i)=cff*day2sec+uclm_cycle
     &                             *uclm_ncycle
      if (uclm_time(ituclm).eq.-1.D+20)
     &    uclm_time(ituclm)=uclm_time(i)
      ierr=nf_fread (ubclima(-1,-1,i),
     &                           ncidclm, ubclm_id,
     &                           uclm_rec, u2dvar)
      if (ierr.ne.NF_NOERR) then
        lvar=lenstr(vname(1,indxUb))
        write(stdout,6) vname(1,indxUb)(1:lvar), uclm_rec
        goto 99
      endif
      ierr=nf_fread (vbclima(-1,-1,i),
     &                           ncidclm, vbclm_id,
     &                           uclm_rec, v2dvar)
      if (ierr.ne.NF_NOERR) then
        lvar=lenstr(vname(1,indxVb))
        write(stdout,6) vname(1,indxVb)(1:lvar), uclm_rec
        goto 99
      endif
      ierr=nf_fread (uclima(-1,-1,1,i),
     &                           ncidclm, uclm_id,
     &                           uclm_rec, u3dvar)
      if (ierr.ne.NF_NOERR) then
        lvar=lenstr(vname(1,indxU))
        write(stdout,6) vname(1,indxU)(1:lvar), uclm_rec
        goto 99
      endif
      ierr=nf_fread (vclima(-1,-1,1,i),
     &                           ncidclm, vclm_id,
     &                           uclm_rec, v3dvar)
      if (ierr.ne.NF_NOERR) then
        lvar=lenstr(vname(1,indxV))
        write(stdout,6) vname(1,indxV)(1:lvar), uclm_rec
        goto 99
      endif
      ituclm=i
      if (mynode.eq.0) write(stdout,'(6x,A,1x,g12.4,1x,I4)')
     &'GET_UCLIMA -- Read momentum climatology      for time =', cff
     &                                                      , mynode
        if (ntuclm.gt.1) goto 10
  1    continue
      return
  3   format(/,' GET_UCLIMA - unable to find climatology variable: ',
     &       a,/,15x,'in climatology NetCDF file: ',a)
  4   write(stdout,5) clmname(1:lstr)
  5   format(/,' GET_UCLIMA - unable to open climatology',
     &         1x,'NetCDF file: ',a)
      goto 99
  6   format(/,' GET_UCLIMA - ERROR while reading variable: ',a,2x,
     &       ' at TIME index = ',i4)
  7   format(/,' GET_UCLIMA - ERROR: requested time record ',I4,
     &       1x,'exeeds the last available', /,14x,'record ',I4,
     &       1x,'in climatology file: ',a, /,14x,'TDAYS = ',
     &       g12.4,2x,'last available UCLM_TIME = ',g12.4)
  99  may_day_flag=2
      return
       
      

      end subroutine Sub_Loop_get_uclima

      subroutine set_uclima (tile)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_set_uclima(tile,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: tile
        end subroutine Sub_Loop_set_uclima

      end interface
      integer(4) :: tile
      

        call Sub_Loop_set_uclima(tile, Agrif_tabvars_i(194)%iarray0, Agr
     &if_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_set_uclima(tile,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: tile

                   

                     
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                            
      integer(4) :: chunk_size_X
      integer(4) :: margin_X
      integer(4) :: chunk_size_E
      integer(4) :: margin_E
                                             
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: i_X
      integer(4) :: j_E
      chunk_size_X=(Lmmpi+NSUB_X-1)/NSUB_X
      margin_X=(NSUB_X*chunk_size_X-Lmmpi)/2
      chunk_size_E=(Mmmpi+NSUB_E-1)/NSUB_E
      margin_E=(NSUB_E*chunk_size_E-Mmmpi)/2
      j_E=tile/NSUB_X
      i_X=tile-j_E*NSUB_X
      Istr=1+i_X*chunk_size_X-margin_X
      Iend=Istr+chunk_size_X-1
      Istr=max(Istr,1)
      Iend=min(Iend,Lmmpi)
      Jstr=1+j_E*chunk_size_E-margin_E
      Jend=Jstr+chunk_size_E-1
      Jstr=max(Jstr,1)
      Jend=min(Jend,Mmmpi)
      call set_uclima_tile (Istr,Iend,Jstr,Jend)
      return
       
      

      end subroutine Sub_Loop_set_uclima

      subroutine set_uclima_tile (Istr,Iend,Jstr,Jend)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_set_uclima_tile(Istr,Iend,Jstr,Jend,padd_E,M
     &m,padd_X,Lm,may_day_flag,tdays,vclima,vclm,uclima,uclm,vbclima,vbc
     &lm,ubclima,ubclm,iic,uclm_cycle,synchro_flag,uclm_time,dt,time,itu
     &clm,NORTH_INTER,Mmmpi,SOUTH_INTER,EAST_INTER,Lmmpi,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: may_day_flag
      real :: tdays
      integer(4) :: iic
      real :: uclm_cycle
      logical :: synchro_flag
      real :: dt
      real :: time
      integer(4) :: ituclm
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: vclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: uclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vbclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: ubclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubclm
      real, dimension(1:2) :: uclm_time
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
        end subroutine Sub_Loop_set_uclima_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      

        call Sub_Loop_set_uclima_tile(Istr,Iend,Jstr,Jend, Agrif_tabvars
     &_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189
     &)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(162)%iarr
     &ay0, Agrif_tabvars_r(41)%array0, Agrif_tabvars(124)%array4, Agrif_
     &tabvars(130)%array3, Agrif_tabvars(125)%array4, Agrif_tabvars(131)
     &%array3, Agrif_tabvars(127)%array3, Agrif_tabvars(132)%array2, Agr
     &if_tabvars(128)%array3, Agrif_tabvars(133)%array2, Agrif_tabvars_i
     &(182)%iarray0, Agrif_tabvars_r(47)%array0, Agrif_tabvars_l(8)%larr
     &ay0, Agrif_tabvars(123)%array1, Agrif_tabvars_r(46)%array0, Agrif_
     &tabvars_r(44)%array0, Agrif_tabvars_i(209)%iarray0, Agrif_tabvars_
     &l(5)%larray0, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_l(4)%lar
     &ray0, Agrif_tabvars_l(7)%larray0, Agrif_tabvars_i(195)%iarray0, Ag
     &rif_tabvars_l(6)%larray0)

      end


      subroutine Sub_Loop_set_uclima_tile(Istr,Iend,Jstr,Jend,padd_E,Mm,
     &padd_X,Lm,may_day_flag,tdays,vclima,vclm,uclima,uclm,vbclima,vbclm
     &,ubclima,ubclm,iic,uclm_cycle,synchro_flag,uclm_time,dt,time,itucl
     &m,NORTH_INTER,Mmmpi,SOUTH_INTER,EAST_INTER,Lmmpi,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: may_day_flag
      real :: tdays
      integer(4) :: iic
      real :: uclm_cycle
      logical :: synchro_flag
      real :: dt
      real :: time
      integer(4) :: ituclm
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: vclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: uclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vbclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: ubclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubclm
      real, dimension(1:2) :: uclm_time
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                                    
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: it1
      integer(4) :: it2
                           
      real :: cff
      real :: cff1
      real :: cff2
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
      if (Istr.eq.1) then
        if (WEST_INTER) then
          IstrR=Istr-2
        else
          IstrR=Istr-1
        endif
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lmmpi) then
        if (EAST_INTER) then
          IendR=Iend+2
        else
          IendR=Iend+1
        endif
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        if (SOUTH_INTER) then
          JstrR=Jstr-2
        else
          JstrR=Jstr-1
        endif
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mmmpi) then
        if (NORTH_INTER) then
          JendR=Jend+2
        else
          JendR=Jend+1
        endif
      else
        JendR=Jend
      endif
      it1=3-ituclm
      it2=ituclm
      cff=time+0.5D0*dt
      cff1=uclm_time(it2)-cff
      cff2=cff-uclm_time(it1)
      if (Istr+Jstr.eq.2 .and. cff1.lt.dt) synchro_flag=.TRUE.
      if (uclm_cycle.lt.0.D0) then
        if (iic.eq.0) then
          do j=JstrR,JendR
            do i=IstrR,IendR
              ubclm(i,j)=ubclima(i,j,ituclm)
              vbclm(i,j)=vbclima(i,j,ituclm)
            enddo
          enddo
          do k=1,N
            do j=JstrR,JendR
              do i=IstrR,IendR
                uclm(i,j,k)=uclima(i,j,k,ituclm)
                vclm(i,j,k)=vclima(i,j,k,ituclm)
              enddo
            enddo
          enddo
        endif
      elseif (cff1.ge.0.D0 .and. cff2.ge.0.D0) then
        cff=1.D0/(cff1+cff2)
        cff1=cff1*cff
        cff2=cff2*cff
        do j=JstrR,JendR
          do i=IstrR,IendR
            ubclm(i,j)=cff1*ubclima(i,j,it1)
     &                +cff2*ubclima(i,j,it2)
            vbclm(i,j)=cff1*vbclima(i,j,it1)
     &                +cff2*vbclima(i,j,it2)
          enddo
        enddo
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              uclm(i,j,k)=cff1*uclima(i,j,k,it1)
     &                   +cff2*uclima(i,j,k,it2)
              vclm(i,j,k)=cff1*vclima(i,j,k,it1)
     &                   +cff2*vclima(i,j,k,it2)
            enddo
          enddo
        enddo
      elseif (Istr+Jstr.eq.2) then
          write(stdout,'(/1x,2A/3(1x,A,F16.10)/)')
     &            'SET_UCLIMA_TILE - current model time is outside ',
     &            'bounds of ''uclm_time''.', 'UCLM_TSTART=',
     &             uclm_time(it1)*sec2day,     'TDAYS=',  tdays,
     &            'UCLM_TEND=',   uclm_time(it2)*sec2day
          may_day_flag=2
      endif
      return
       
      

      end subroutine Sub_Loop_set_uclima_tile

