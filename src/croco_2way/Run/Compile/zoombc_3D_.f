












      subroutine u3dbc_interp_tile(Istr,Iend,Jstr,Jend)      

      use AGRIF_Util
      interface
        subroutine Sub_Loop_u3dbc_interp_tile(Istr,Iend,Jstr,Jend,Mmmpi,
     &padd_X,Lm,padd_E,Mm,Lmmpi,U_east,U_west,U_north,umask,nstp,u,U_sou
     &th,uclm,nbstep3d,uid,UTimeindex,nnew,nbcoarse,NORTH_INTER,SOUTH_IN
     &TER,EAST_INTER,WEST_INTER)
          use AGRIF_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      integer(4) :: nstp
      integer(4) :: nbstep3d
      integer(4) :: uid
      integer(4) :: UTimeindex
      integer(4) :: nnew
      integer(4) :: nbcoarse
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: U_east
      real, dimension(1:1,-1:Mm+2+padd_E,1:N,1:4) :: U_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: U_north
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,0:0,1:N,1:4) :: U_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
        end subroutine Sub_Loop_u3dbc_interp_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      

        call Sub_Loop_u3dbc_interp_tile(Istr,Iend,Jstr,Jend, Agrif_tabva
     &rs_i(194)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(2
     &00)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%ia
     &rray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars(42)%array4, Agr
     &if_tabvars(45)%array4, Agrif_tabvars(36)%array4, Agrif_tabvars(49)
     &%array2, Agrif_tabvars_i(176)%iarray0, Agrif_tabvars(111)%array4, 
     &Agrif_tabvars(39)%array4, Agrif_tabvars(131)%array3, Agrif_tabvars
     &_i(173)%iarray0, Agrif_tabvars_i(19)%iarray0, Agrif_tabvars_i(42)%
     &iarray0, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars_i(39)%iarray0
     &, Agrif_tabvars_l(5)%larray0, Agrif_tabvars_l(4)%larray0, Agrif_ta
     &bvars_l(7)%larray0, Agrif_tabvars_l(6)%larray0)

      end


      subroutine Sub_Loop_u3dbc_interp_tile(Istr,Iend,Jstr,Jend,Mmmpi,pa
     &dd_X,Lm,padd_E,Mm,Lmmpi,U_east,U_west,U_north,umask,nstp,u,U_south
     &,uclm,nbstep3d,uid,UTimeindex,nnew,nbcoarse,NORTH_INTER,SOUTH_INTE
     &R,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      integer(4) :: nstp
      integer(4) :: nbstep3d
      integer(4) :: uid
      integer(4) :: UTimeindex
      integer(4) :: nnew
      integer(4) :: nbcoarse
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: U_east
      real, dimension(1:1,-1:Mm+2+padd_E,1:N,1:4) :: U_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: U_north
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,0:0,1:N,1:4) :: U_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                     
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                        
      integer(4) :: itrcind
                   
      real :: tinterp
                                  
      INTEGER(4) :: nbstep3dparent
      External :: u3dinterp
                         
      real, dimension(1:6) :: ainterp
                         
      integer(4) :: irhot
                    
      real :: rrhot
                                  
      integer(4) :: nsource
      integer(4) :: ndest
                                
      integer(4) :: parentnbstep
                    
      real :: gamma
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      irhot=Agrif_Irhot()
      rrhot=real(irhot)
C$OMP BARRIER
C$OMP MASTER
      parentnbstep=Agrif_Parent_Nb_Step()
       IF ((((nbcoarse == 1).AND.(nnew == 3)).OR.
     &   ((nbcoarse == irhot).AND.(nnew /=3))).AND.
     &    (UTimeindex.NE.parentnbstep)) THEN
        tinterp=1.D0
        Agrif_UseSpecialvalue=.true.
        Agrif_Specialvalue=0.D0
        Call Agrif_Set_bc(uid,(/0,0/),InterpolationShouldbemade=.TRUE.)
        Call Agrif_Bc_variable(uid,calledweight=tinterp,
     &procname=u3dinterp)
        Agrif_UseSpecialValue=.false.
        UTimeindex = parentnbstep
      endif
C$OMP END MASTER
C$OMP BARRIER
       nbstep3dparent=
     &Agrif_Parent(nbstep3d)
       tinterp = real(nbcoarse)/rrhot
       IF (nnew == 3) tinterp = tinterp - 1.D0/(2.D0*rrhot)
       gamma = 1.D0/12.D0
       ainterp(3)=tinterp*4*(1+tinterp)
       ainterp(2)=-3.D0*(-1+tinterp+2*tinterp**2)
       ainterp(1)=tinterp*(-1+2*tinterp)
       ainterp = ainterp/3.D0
       ainterp(1) = 0.D0
       ainterp(2) = 1.D0-2*tinterp
       ainterp(3) = 2.D0*tinterp
       ainterp = 0.D0
       IF (nnew == 3) then
         ainterp(1) = 0.5D0-tinterp
         ainterp(2) = 0.5D0+tinterp
       else
         ainterp(3) = -tinterp
         ainterp(4) = 1.D0+tinterp
       endif
       IF (nbstep3dparent .LE. 1) THEN
       ainterp = 0.D0
       ainterp(2) = 1.D0
       ENDIF
       if (nbstep3d .LE. (Agrif_irhot()-1)) then
        ainterp = 0.D0
        ainterp(2) = 1.D0
       endif
      IF ((nbstep3dparent .NE. 0) .AND. (nnew ==3)) THEN
      ENDIF
      IF ((nbstep3dparent .NE. 0) .AND. (nnew /=3)) THEN
      ENDIF
       IF ((nnew/=3).AND.(nbcoarse == irhot)) THEN
         ainterp    = 0.D0
         ainterp(4) = 1.D0
       ENDIF
      if (.not.SOUTH_INTER) then
        do k=1,N
          do i=Istr,IendR
            uclm(i,Jstr-1,k)=
     &  (ainterp(1)*U_south(i,Jstr-1,k,1)+
     &   ainterp(2)*U_south(i,Jstr-1,k,2)+
     &   ainterp(3)*U_south(i,Jstr-1,k,3)+
     &   ainterp(4)*U_south(i,Jstr-1,k,4)+
     &   ainterp(5)*u(i,Jstr-1,k,3-nstp)+
     &   ainterp(6)*u(i,Jstr-1,k,nstp))
     &    *umask(i,Jstr-1)
          enddo
        enddo
      endif
      if (.not.NORTH_INTER) then
        do k=1,N
          do i=Istr,IendR
            uclm(i,Jend+1,k)=
     &  (ainterp(1)*U_north(i,Jend+1,k,1)+
     &   ainterp(2)*U_north(i,Jend+1,k,2)+
     &   ainterp(3)*U_north(i,Jend+1,k,3)+
     &   ainterp(4)*U_north(i,Jend+1,k,4)+
     &   ainterp(5)*u(i,Jend+1,k,3-nstp)+
     &   ainterp(6)*u(i,Jend+1,k,nstp))
     &    *umask(i,Jend+1)
          enddo
        enddo
      endif
      if (.not.WEST_INTER) then
        do k=1,N
          do j=JstrR,JendR
            uclm(Istr,j,k)=
     &  (ainterp(1)*U_west(Istr,j,k,1)+
     &   ainterp(2)*U_west(Istr,j,k,2)+
     &   ainterp(3)*U_west(Istr,j,k,3)+
     &   ainterp(4)*U_west(Istr,j,k,4)+
     &   ainterp(5)*u(Istr,j,k,3-nstp)+
     &   ainterp(6)*u(Istr,j,k,nstp))
     &    *umask(Istr,j)
          enddo
        enddo
      endif
      if (.not.EAST_INTER) then
        do k=1,N
          do j=JstrR,JendR
            uclm(Iend+1,j,k)=
     &  (ainterp(1)*U_east(Iend+1,j,k,1)+
     &   ainterp(2)*U_east(Iend+1,j,k,2)+
     &   ainterp(3)*U_east(Iend+1,j,k,3)+
     &   ainterp(4)*U_east(Iend+1,j,k,4)+
     &   ainterp(5)*u(Iend+1,j,k,3-nstp)+
     &   ainterp(6)*u(Iend+1,j,k,nstp))
     &    *umask(Iend+1,j)
          enddo
        enddo
      endif
      return
       
      

      end subroutine Sub_Loop_u3dbc_interp_tile

      subroutine u3Dinterp(tabres,i1,i2,j1,j2,k1,k2,before,nb,ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_u3Dinterp(tabres,i1,i2,j1,j2,k1,k2,before,nb
     &,ndir,Mmmpi,padd_X,Lm,padd_E,Mm,Lmmpi,U_east,EAST_INTER,U_west,WES
     &T_INTER,U_north,NORTH_INTER,U_south,SOUTH_INTER,u,nnew,iif)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      logical :: EAST_INTER
      logical :: WEST_INTER
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      integer(4) :: nnew
      integer(4) :: iif
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: U_east
      real, dimension(1:1,-1:Mm+2+padd_E,1:N,1:4) :: U_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: U_north
      real, dimension(-1:Lm+2+padd_X,0:0,1:N,1:4) :: U_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
        end subroutine Sub_Loop_u3Dinterp

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
      

        call Sub_Loop_u3Dinterp(tabres,i1,i2,j1,j2,k1,k2,before,nb,ndir,
     & Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif
     &_tabvars_i(200)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabva
     &rs_i(197)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars(42)
     &%array4, Agrif_tabvars_l(7)%larray0, Agrif_tabvars(45)%array4, Agr
     &if_tabvars_l(6)%larray0, Agrif_tabvars(36)%array4, Agrif_tabvars_l
     &(5)%larray0, Agrif_tabvars(39)%array4, Agrif_tabvars_l(4)%larray0,
     & Agrif_tabvars(111)%array4, Agrif_tabvars_i(174)%iarray0, Agrif_ta
     &bvars_i(177)%iarray0)

      end


      subroutine Sub_Loop_u3Dinterp(tabres,i1,i2,j1,j2,k1,k2,before,nb,n
     &dir,Mmmpi,padd_X,Lm,padd_E,Mm,Lmmpi,U_east,EAST_INTER,U_west,WEST_
     &INTER,U_north,NORTH_INTER,U_south,SOUTH_INTER,u,nnew,iif)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      logical :: EAST_INTER
      logical :: WEST_INTER
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      integer(4) :: nnew
      integer(4) :: iif
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: U_east
      real, dimension(1:1,-1:Mm+2+padd_E,1:N,1:4) :: U_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: U_north
      real, dimension(-1:Lm+2+padd_X,0:0,1:N,1:4) :: U_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                   
                                      
                      
                         
                         
      integer(4) :: nparent
                                
      integer(4) :: nsource
      integer(4) :: ndest
                                             
      logical :: western_side
      logical :: eastern_side
                                              
      logical :: northern_side
      logical :: southern_side
       if (before) then
         IF ((iif == 0)) THEN
           nparent = 3
         ELSE
           nparent = nnew
         ENDIF
         tabres(i1:i2,j1:j2,k1:k2) = u(i1:i2,j1:j2,k1:k2,nparent)
       else
         western_side  = (nb == 1).AND.(ndir == 1)
         eastern_side  = (nb == 1).AND.(ndir == 2)
         southern_side = (nb == 2).AND.(ndir == 1)
         northern_side = (nb == 2).AND.(ndir == 2)
         IF (nnew == 3) THEN
           nsource = 2
         ELSE
           nsource = 4
         ENDIF
         ndest = nsource - 1
        if (southern_side) then
            if (.not.SOUTH_INTER) then
                U_south(i1:i2,j1:j2,1:N,ndest)=
     &          U_south(i1:i2,j1:j2,1:N,nsource)
                U_south(i1:i2,j1:j2,1:N,nsource) = tabres
            endif
        endif
        if (northern_side) then
            if (.not.NORTH_INTER) then
                U_north(i1:i2,j1:j2,1:N,ndest)=
     &          U_north(i1:i2,j1:j2,1:N,nsource)
                U_north(i1:i2,j1:j2,1:N,nsource) = tabres
            endif
        endif
        if (western_side) then
            if (.not.WEST_INTER) then
                U_west(i1:i2,j1:j2,1:N,ndest)=
     &          U_west(i1:i2,j1:j2,1:N,nsource)
                U_west(i1:i2,j1:j2,1:N,nsource) = tabres
            endif
        endif
        if (eastern_side) then
            if (.not.EAST_INTER) then
                U_east(i1:i2,j1:j2,1:N,ndest)=
     &          U_east(i1:i2,j1:j2,1:N,nsource)
                U_east(i1:i2,j1:j2,1:N,nsource) = tabres
            endif
        endif
       endif
      return
       
      

      end subroutine Sub_Loop_u3Dinterp

      subroutine u3Dinterp_old(tabres,i1,i2,j1,j2,k1,k2)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_u3Dinterp_old(tabres,i1,i2,j1,j2,k1,k2,padd_
     &E,Mm,padd_X,Lm,u,nnew,iif)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: iif
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
        end subroutine Sub_Loop_u3Dinterp_old

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      

        call Sub_Loop_u3Dinterp_old(tabres,i1,i2,j1,j2,k1,k2, Agrif_tabv
     &ars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(
     &189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(111)%arr
     &ay4, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars_i(177)%iarray0)

      end


      subroutine Sub_Loop_u3Dinterp_old(tabres,i1,i2,j1,j2,k1,k2,padd_E,
     &Mm,padd_X,Lm,u,nnew,iif)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: iif
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                   
                                      
                         
      integer(4) :: nparent
       IF ((iif == 0)) THEN
         nparent = 3
       ELSE
         nparent = nnew
       ENDIF
       tabres(i1:i2,j1:j2,k1:k2) = u(i1:i2,j1:j2,k1:k2,nparent)
      return
       
      

      end subroutine Sub_Loop_u3Dinterp_old

      subroutine v3Dinterp(tabres,i1,i2,j1,j2,k1,k2,before,nb,ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_v3Dinterp(tabres,i1,i2,j1,j2,k1,k2,before,nb
     &,ndir,Mmmpi,padd_X,Lm,padd_E,Mm,Lmmpi,V_east,EAST_INTER,V_west,WES
     &T_INTER,V_north,NORTH_INTER,V_south,SOUTH_INTER,v,nnew,iif)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      logical :: EAST_INTER
      logical :: WEST_INTER
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      integer(4) :: nnew
      integer(4) :: iif
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: V_east
      real, dimension(0:0,-1:Mm+2+padd_E,1:N,1:4) :: V_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: V_north
      real, dimension(-1:Lm+2+padd_X,1:1,1:N,1:4) :: V_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
        end subroutine Sub_Loop_v3Dinterp

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
      

        call Sub_Loop_v3Dinterp(tabres,i1,i2,j1,j2,k1,k2,before,nb,ndir,
     & Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif
     &_tabvars_i(200)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabva
     &rs_i(197)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars(41)
     &%array4, Agrif_tabvars_l(7)%larray0, Agrif_tabvars(44)%array4, Agr
     &if_tabvars_l(6)%larray0, Agrif_tabvars(35)%array4, Agrif_tabvars_l
     &(5)%larray0, Agrif_tabvars(38)%array4, Agrif_tabvars_l(4)%larray0,
     & Agrif_tabvars(110)%array4, Agrif_tabvars_i(174)%iarray0, Agrif_ta
     &bvars_i(177)%iarray0)

      end


      subroutine Sub_Loop_v3Dinterp(tabres,i1,i2,j1,j2,k1,k2,before,nb,n
     &dir,Mmmpi,padd_X,Lm,padd_E,Mm,Lmmpi,V_east,EAST_INTER,V_west,WEST_
     &INTER,V_north,NORTH_INTER,V_south,SOUTH_INTER,v,nnew,iif)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      logical :: EAST_INTER
      logical :: WEST_INTER
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      integer(4) :: nnew
      integer(4) :: iif
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: V_east
      real, dimension(0:0,-1:Mm+2+padd_E,1:N,1:4) :: V_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: V_north
      real, dimension(-1:Lm+2+padd_X,1:1,1:N,1:4) :: V_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                   
                                      
                      
                         
                         
      integer(4) :: nparent
                                
      integer(4) :: nsource
      integer(4) :: ndest
                                             
      logical :: western_side
      logical :: eastern_side
                                              
      logical :: northern_side
      logical :: southern_side
       if (before) then
         IF ((iif == 0)) THEN
           nparent = 3
         ELSE
           nparent = nnew
         ENDIF
         tabres(i1:i2,j1:j2,k1:k2) = v(i1:i2,j1:j2,k1:k2,nparent)
       else
         western_side  = (nb == 1).AND.(ndir == 1)
         eastern_side  = (nb == 1).AND.(ndir == 2)
         southern_side = (nb == 2).AND.(ndir == 1)
         northern_side = (nb == 2).AND.(ndir == 2)
         IF (nnew == 3) THEN
           nsource = 2
         ELSE
           nsource = 4
         ENDIF
         ndest = nsource - 1
        if (southern_side) then
            if (.not.SOUTH_INTER) then
                V_south(i1:i2,j1:j2,1:N,ndest)=
     &          V_south(i1:i2,j1:j2,1:N,nsource)
                V_south(i1:i2,j1:j2,1:N,nsource) = tabres
            endif
        endif
        if (northern_side) then
            if (.not.NORTH_INTER) then
                V_north(i1:i2,j1:j2,1:N,ndest)=
     &          V_north(i1:i2,j1:j2,1:N,nsource)
                V_north(i1:i2,j1:j2,1:N,nsource) = tabres
            endif
        endif
        if (western_side) then
            if (.not.WEST_INTER) then
                V_west(i1:i2,j1:j2,1:N,ndest)=
     &          V_west(i1:i2,j1:j2,1:N,nsource)
                V_west(i1:i2,j1:j2,1:N,nsource) = tabres
            endif
        endif
        if (eastern_side) then
            if (.not.EAST_INTER) then
                V_east(i1:i2,j1:j2,1:N,ndest)=
     &          V_east(i1:i2,j1:j2,1:N,nsource)
                V_east(i1:i2,j1:j2,1:N,nsource) = tabres
            endif
        endif
       endif
      return
       
      

      end subroutine Sub_Loop_v3Dinterp

      subroutine v3Dinterp_old(tabres,i1,i2,j1,j2,k1,k2)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_v3Dinterp_old(tabres,i1,i2,j1,j2,k1,k2,padd_
     &E,Mm,padd_X,Lm,v,nnew,iif)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: iif
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
        end subroutine Sub_Loop_v3Dinterp_old

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      

        call Sub_Loop_v3Dinterp_old(tabres,i1,i2,j1,j2,k1,k2, Agrif_tabv
     &ars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(
     &189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(110)%arr
     &ay4, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars_i(177)%iarray0)

      end


      subroutine Sub_Loop_v3Dinterp_old(tabres,i1,i2,j1,j2,k1,k2,padd_E,
     &Mm,padd_X,Lm,v,nnew,iif)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: iif
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                   
                                      
                         
      integer(4) :: nparent
       IF ((iif == 0)) THEN
         nparent = 3
       ELSE
         nparent = nnew
       ENDIF
       tabres(i1:i2,j1:j2,k1:k2) = v(i1:i2,j1:j2,k1:k2,nparent)
      return
       
      

      end subroutine Sub_Loop_v3Dinterp_old

      subroutine v3dbc_interp_tile(Istr,Iend,Jstr,Jend)      

      use AGRIF_Util
      interface
        subroutine Sub_Loop_v3dbc_interp_tile(Istr,Iend,Jstr,Jend,Mmmpi,
     &padd_X,Lm,padd_E,Mm,Lmmpi,V_east,V_west,V_north,vmask,nstp,v,V_sou
     &th,vclm,nbstep3d,vid,VTimeindex,nnew,nbcoarse,NORTH_INTER,SOUTH_IN
     &TER,EAST_INTER,WEST_INTER)
          use AGRIF_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      integer(4) :: nstp
      integer(4) :: nbstep3d
      integer(4) :: vid
      integer(4) :: VTimeindex
      integer(4) :: nnew
      integer(4) :: nbcoarse
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: V_east
      real, dimension(0:0,-1:Mm+2+padd_E,1:N,1:4) :: V_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: V_north
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,1:1,1:N,1:4) :: V_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
        end subroutine Sub_Loop_v3dbc_interp_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      

        call Sub_Loop_v3dbc_interp_tile(Istr,Iend,Jstr,Jend, Agrif_tabva
     &rs_i(194)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(2
     &00)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%ia
     &rray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars(41)%array4, Agr
     &if_tabvars(44)%array4, Agrif_tabvars(35)%array4, Agrif_tabvars(48)
     &%array2, Agrif_tabvars_i(176)%iarray0, Agrif_tabvars(110)%array4, 
     &Agrif_tabvars(38)%array4, Agrif_tabvars(130)%array3, Agrif_tabvars
     &_i(173)%iarray0, Agrif_tabvars_i(18)%iarray0, Agrif_tabvars_i(41)%
     &iarray0, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars_i(39)%iarray0
     &, Agrif_tabvars_l(5)%larray0, Agrif_tabvars_l(4)%larray0, Agrif_ta
     &bvars_l(7)%larray0, Agrif_tabvars_l(6)%larray0)

      end


      subroutine Sub_Loop_v3dbc_interp_tile(Istr,Iend,Jstr,Jend,Mmmpi,pa
     &dd_X,Lm,padd_E,Mm,Lmmpi,V_east,V_west,V_north,vmask,nstp,v,V_south
     &,vclm,nbstep3d,vid,VTimeindex,nnew,nbcoarse,NORTH_INTER,SOUTH_INTE
     &R,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      integer(4) :: nstp
      integer(4) :: nbstep3d
      integer(4) :: vid
      integer(4) :: VTimeindex
      integer(4) :: nnew
      integer(4) :: nbcoarse
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(Lmmpi+1:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4) :: V_east
      real, dimension(0:0,-1:Mm+2+padd_E,1:N,1:4) :: V_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi+1:Mmmpi+1,1:N,1:4) :: V_north
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,1:1,1:N,1:4) :: V_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                     
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                        
      integer(4) :: itrcind
                   
      real :: tinterp
                                  
      INTEGER(4) :: nbstep3dparent
      external :: v3dinterp
                      
      real, dimension(1:6) :: ainterp
                         
      integer(4) :: irhot
                    
      real :: rrhot
                                  
      integer(4) :: nsource
      integer(4) :: ndest
                    
      real :: gamma
                                
      integer(4) :: parentnbstep
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      irhot=Agrif_Irhot()
      rrhot=real(irhot)
C$OMP BARRIER
C$OMP MASTER
      parentnbstep=Agrif_Parent_Nb_Step()
       IF ((((nbcoarse == 1).AND.(nnew == 3)).OR.
     &   ((nbcoarse == irhot).AND.(nnew /=3))).AND.
     &    (VTimeindex.NE.parentnbstep)) THEN
        tinterp=1.D0
        AGRIF_UseSpecialvalue=.true.
        AGRIF_Specialvalue=0.D0
        Call Agrif_Set_bc(vid,(/0,0/),InterpolationShouldbemade=.TRUE.)
        Call Agrif_Bc_variable(vid,calledweight=tinterp,
     &procname=v3dinterp)
        AGRIF_UseSpecialValue=.false.
        VTimeindex=parentnbstep
      endif
C$OMP END MASTER
C$OMP BARRIER
       nbstep3dparent=
     &Agrif_Parent(nbstep3d)
       gamma = 1.D0/12.D0
       tinterp = real(nbcoarse)/rrhot
       IF (nnew == 3) tinterp = tinterp - 1.D0/(2.D0*rrhot)
       ainterp(3)=tinterp*4*(1+tinterp)
       ainterp(2)=-3.D0*(-1+tinterp+2*tinterp**2)
       ainterp(1)=tinterp*(-1+2*tinterp)
       ainterp = ainterp/3.D0
       ainterp(1) = 0.D0
       ainterp(2) = 1.D0-2*tinterp
       ainterp(3) = 2.D0*tinterp
       ainterp = 0.D0
       IF (nnew == 3) then
         ainterp(1) = 0.5D0-tinterp
         ainterp(2) = 0.5D0+tinterp
       else
         ainterp(3) = -tinterp
         ainterp(4) = 1.D0+tinterp
       endif
       IF (nbstep3dparent .LE. 1) THEN
       ainterp = 0.D0
       ainterp(2) = 1.D0
       ENDIF
       if (nbstep3d .LE. (Agrif_irhot()-1)) then
        ainterp = 0.D0
        ainterp(2) = 1.D0
       endif
      IF ((nbstep3dparent .NE. 0) .AND. (nnew ==3)) THEN
      ENDIF
      IF ((nbstep3dparent .NE. 0) .AND. (nnew /=3)) THEN
      ENDIF
       IF ((nnew/=3).AND.(nbcoarse == irhot)) THEN
         ainterp    = 0.D0
         ainterp(4) = 1.D0
       ENDIF
      if (.not.SOUTH_INTER) then
        do k=1,N
          do i=IstrR,IendR
            vclm(i,Jstr,k)=
     &    (ainterp(1)*V_south(i,Jstr,k,1)+
     &     ainterp(2)*V_south(i,Jstr,k,2)+
     &     ainterp(3)*V_south(i,Jstr,k,3)+
     &     ainterp(4)*V_south(i,Jstr,k,4)+
     &     ainterp(5)*v(i,Jstr,k,3-nstp)+
     &     ainterp(6)*v(i,Jstr,k,nstp))
     &    *vmask(i,Jstr)
          enddo
        enddo
      endif
      if (.not.NORTH_INTER) then
        do k=1,N
          do i=IstrR,IendR
            vclm(i,Jend+1,k)=
     &    (ainterp(1)*V_north(i,Jend+1,k,1)+
     &     ainterp(2)*V_north(i,Jend+1,k,2)+
     &     ainterp(3)*V_north(i,Jend+1,k,3)+
     &     ainterp(4)*V_north(i,Jend+1,k,4)+
     &     ainterp(5)*v(i,Jend+1,k,3-nstp)+
     &     ainterp(6)*v(i,Jend+1,k,nstp))
     &    *vmask(i,Jend+1)
          enddo
        enddo
      endif
      if (.not.WEST_INTER) then
        do k=1,N
          do j=Jstr,JendR
            vclm(Istr-1,j,k)=
     &    (ainterp(1)*V_west(Istr-1,j,k,1)+
     &     ainterp(2)*V_west(Istr-1,j,k,2)+
     &     ainterp(3)*V_west(Istr-1,j,k,3)+
     &     ainterp(4)*V_west(Istr-1,j,k,4)+
     &     ainterp(5)*v(Istr-1,j,k,3-nstp)+
     &     ainterp(6)*v(Istr-1,j,k,nstp))
     &    *vmask(Istr-1,j)
          enddo
        enddo
      endif
      if (.not.EAST_INTER) then
        do k=1,N
          do j=Jstr,JendR
            vclm(Iend+1,j,k)=
     &    (ainterp(1)*V_east(Iend+1,j,k,1)+
     &     ainterp(2)*V_east(Iend+1,j,k,2)+
     &     ainterp(3)*V_east(Iend+1,j,k,3)+
     &     ainterp(4)*V_east(Iend+1,j,k,4)+
     &     ainterp(5)*v(Iend+1,j,k,3-nstp)+
     &     ainterp(6)*v(Iend+1,j,k,nstp))
     &    *vmask(Iend+1,j)
          enddo
        enddo
      endif
      return
       
      

      end subroutine Sub_Loop_v3dbc_interp_tile

      subroutine t3dbc_interp_tile(Istr,Iend,Jstr,Jend,indx,itrc)      

      use AGRIF_Util
      interface
        subroutine Sub_Loop_t3dbc_interp_tile(Istr,Iend,Jstr,Jend,indx,i
     &trc,Mmmpi,padd_X,Lm,padd_E,Mm,Lmmpi,T_east,T_west,T_north,rmask,T_
     &south,tclm,tid,TTimeindex,nnew,nbcoarse,nbstep3d,NORTH_INTER,SOUTH
     &_INTER,EAST_INTER,WEST_INTER)
          use AGRIF_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      integer(4) :: tid
      integer(4) :: TTimeindex
      integer(4) :: nnew
      integer(4) :: nbcoarse
      integer(4) :: nbstep3d
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(Lmmpi:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_ea
     &st
      real, dimension(0:1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi:Mmmpi+1,1:N,1:4,1:NT) :: T_no
     &rth
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,0:1,1:N,1:4,1:NT) :: T_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tclm
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: itrc
      integer(4) :: indx
        end subroutine Sub_Loop_t3dbc_interp_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: itrc
      integer(4) :: indx
      

        call Sub_Loop_t3dbc_interp_tile(Istr,Iend,Jstr,Jend,indx,itrc, A
     &grif_tabvars_i(194)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_t
     &abvars_i(200)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars
     &_i(197)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars(43)%a
     &rray5, Agrif_tabvars(46)%array5, Agrif_tabvars(37)%array5, Agrif_t
     &abvars(51)%array2, Agrif_tabvars(40)%array5, Agrif_tabvars(138)%ar
     &ray4, Agrif_tabvars_i(17)%iarray0, Agrif_tabvars_i(43)%iarray0, Ag
     &rif_tabvars_i(174)%iarray0, Agrif_tabvars_i(39)%iarray0, Agrif_tab
     &vars_i(173)%iarray0, Agrif_tabvars_l(5)%larray0, Agrif_tabvars_l(4
     &)%larray0, Agrif_tabvars_l(7)%larray0, Agrif_tabvars_l(6)%larray0)

      end


      subroutine Sub_Loop_t3dbc_interp_tile(Istr,Iend,Jstr,Jend,indx,itr
     &c,Mmmpi,padd_X,Lm,padd_E,Mm,Lmmpi,T_east,T_west,T_north,rmask,T_so
     &uth,tclm,tid,TTimeindex,nnew,nbcoarse,nbstep3d,NORTH_INTER,SOUTH_I
     &NTER,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      integer(4) :: tid
      integer(4) :: TTimeindex
      integer(4) :: nnew
      integer(4) :: nbcoarse
      integer(4) :: nbstep3d
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(Lmmpi:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_ea
     &st
      real, dimension(0:1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi:Mmmpi+1,1:N,1:4,1:NT) :: T_no
     &rth
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,0:1,1:N,1:4,1:NT) :: T_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tclm
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: itrc
      integer(4) :: indx

                     
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                     
      integer(4) :: itrcind
      integer(4) :: ntind
                   
      real :: tinterp
                                  
      INTEGER(4) :: nbstep3dparent
      external t3dinterp
                      
      real, dimension(1:4) :: ainterp
                         
      integer(4) :: irhot
                    
      real :: rrhot
                                  
      integer(4) :: nsource
      integer(4) :: ndest
                                
      integer(4) :: parentnbstep
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      irhot = Agrif_Irhot()
      rrhot = real(irhot)
       nbstep3dparent=
     &Agrif_Parent(nbstep3d)
C$OMP BARRIER
C$OMP MASTER
      parentnbstep=Agrif_Parent_Nb_Step()
       IF ((((nbcoarse == 1).AND.(nnew == 3)).OR.
     &   ((nbcoarse == irhot).AND.(nnew /=3))).AND.
     &    (TTimeindex.NE.parentnbstep)) THEN
        tinterp=1.D0
        Agrif_UseSpecialvalue=.true.
        Agrif_Specialvalue=0.D0
        Call Agrif_Set_bc(tid,(/-1,0/),
     &     InterpolationShouldbemade=.TRUE.)
        Call Agrif_Bc_variable(tid,calledweight=tinterp,
     & procname = t3dinterp)
        Agrif_UseSpecialValue=.false.
      endif
C$OMP END MASTER
C$OMP BARRIER
       tinterp = real(nbcoarse)/rrhot
       IF (nnew == 3) tinterp = tinterp - 1.D0/(2.D0*rrhot)
       ainterp(3)=tinterp*4*(1+tinterp)
       ainterp(2)=-3.D0*(-1+tinterp+2*tinterp**2)
       ainterp(1)=tinterp*(-1+2*tinterp)
       ainterp = ainterp/3.D0
       ainterp(1) = 0.D0
       ainterp(2) = 1.D0-2*tinterp
       ainterp(3) = 2.D0*tinterp
       ainterp = 0.D0
       IF (nnew == 3) then
         ainterp(1) = 0.5D0-tinterp
         ainterp(2) = 0.5D0+tinterp
       else
         ainterp(3) = -tinterp
         ainterp(4) = 1.D0+tinterp
       endif
       IF (nbstep3dparent .LE. 1) THEN
       ainterp = 0.D0
       ainterp(2) = 1.D0
       ENDIF
       if (nbstep3d .LE. (Agrif_irhot()-1)) then
        ainterp = 0.D0
        ainterp(2) = 1.D0
       endif
       IF ((nnew/=3).AND.(nbcoarse == irhot)) THEN
         ainterp    = 0.D0
         ainterp(4) = 1.D0
       ENDIF
       if (.not.SOUTH_INTER) then
         do k=1,N
           do i=IstrR,IendR
            tclm(i,Jstr-1,k,itrc)=
     &     (ainterp(1)*T_south(i,Jstr-1,k,1,itrc)+
     &      ainterp(2)*T_south(i,Jstr-1,k,2,itrc)+
     &      ainterp(3)*T_south(i,Jstr-1,k,3,itrc)+
     &      ainterp(4)*T_south(i,Jstr-1,k,4,itrc))
     &        *rmask(i,Jstr-1)
          enddo
        enddo
      endif
      if (.not.NORTH_INTER) then
        do k=1,N
          do i=IstrR,IendR
            tclm(i,Jend+1,k,itrc)=
     &     (ainterp(1)*T_north(i,Jend+1,k,1,itrc)+
     &      ainterp(2)*T_north(i,Jend+1,k,2,itrc)+
     &      ainterp(3)*T_north(i,Jend+1,k,3,itrc)+
     &      ainterp(4)*T_north(i,Jend+1,k,4,itrc))
     &          *rmask(i,Jend+1)
          enddo
        enddo
      endif
      if (.not.WEST_INTER) then
        do k=1,N
          do j=JstrR,JendR
            tclm(Istr-1,j,k,itrc)=
     &     (ainterp(1)*T_west(Istr-1,j,k,1,itrc)+
     &      ainterp(2)*T_west(Istr-1,j,k,2,itrc)+
     &      ainterp(3)*T_west(Istr-1,j,k,3,itrc)+
     &      ainterp(4)*T_west(Istr-1,j,k,4,itrc))
     &         *rmask(Istr-1,j)
          enddo
        enddo
      endif
      if (.not.EAST_INTER) then
        do k=1,N
          do j=JstrR,JendR
            tclm(Iend+1,j,k,itrc)=
     &     (ainterp(1)*T_east(Iend+1,j,k,1,itrc)+
     &      ainterp(2)*T_east(Iend+1,j,k,2,itrc)+
     &      ainterp(3)*T_east(Iend+1,j,k,3,itrc)+
     &      ainterp(4)*T_east(Iend+1,j,k,4,itrc))
     &        *rmask(Iend+1,j)
           enddo
        enddo
      endif
      return
       
      

      end subroutine Sub_Loop_t3dbc_interp_tile

      subroutine t3Dinterp(tabres,i1,i2,j1,j2,k1,k2,m1,m2,before,nb,
     &                                                             ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_t3Dinterp(tabres,i1,i2,j1,j2,k1,k2,m1,m2,bef
     &ore,nb,ndir,Mmmpi,padd_X,Lm,padd_E,Mm,Lmmpi,T_east,EAST_INTER,T_we
     &st,WEST_INTER,T_north,NORTH_INTER,T_south,SOUTH_INTER,t,nnew,iif)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      logical :: EAST_INTER
      logical :: WEST_INTER
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      integer(4) :: nnew
      integer(4) :: iif
      real, dimension(Lmmpi:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_ea
     &st
      real, dimension(0:1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi:Mmmpi+1,1:N,1:4,1:NT) :: T_no
     &rth
      real, dimension(-1:Lm+2+padd_X,0:1,1:N,1:4,1:NT) :: T_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
        end subroutine Sub_Loop_t3Dinterp

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
      

        call Sub_Loop_t3Dinterp(tabres,i1,i2,j1,j2,k1,k2,m1,m2,before,nb
     &,ndir, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(189)%iarray0,
     & Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_i(188)%iarray0, Agrif
     &_tabvars_i(197)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabva
     &rs(43)%array5, Agrif_tabvars_l(7)%larray0, Agrif_tabvars(46)%array
     &5, Agrif_tabvars_l(6)%larray0, Agrif_tabvars(37)%array5, Agrif_tab
     &vars_l(5)%larray0, Agrif_tabvars(40)%array5, Agrif_tabvars_l(4)%la
     &rray0, Agrif_tabvars(109)%array5, Agrif_tabvars_i(174)%iarray0, Ag
     &rif_tabvars_i(177)%iarray0)

      end


      subroutine Sub_Loop_t3Dinterp(tabres,i1,i2,j1,j2,k1,k2,m1,m2,befor
     &e,nb,ndir,Mmmpi,padd_X,Lm,padd_E,Mm,Lmmpi,T_east,EAST_INTER,T_west
     &,WEST_INTER,T_north,NORTH_INTER,T_south,SOUTH_INTER,t,nnew,iif)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: Mmmpi
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: Lmmpi
      logical :: EAST_INTER
      logical :: WEST_INTER
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      integer(4) :: nnew
      integer(4) :: iif
      real, dimension(Lmmpi:Lmmpi+1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_ea
     &st
      real, dimension(0:1,-1:Mm+2+padd_E,1:N,1:4,1:NT) :: T_west
      real, dimension(-1:Lm+2+padd_X,Mmmpi:Mmmpi+1,1:N,1:4,1:NT) :: T_no
     &rth
      real, dimension(-1:Lm+2+padd_X,0:1,1:N,1:4,1:NT) :: T_south
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                         
                                            
                      
                         
                         
      integer(4) :: nparent
                                
      integer(4) :: nsource
      integer(4) :: ndest
                                             
      logical :: western_side
      logical :: eastern_side
                                              
      logical :: northern_side
      logical :: southern_side
       if (before) then
         IF ((iif == 0)) THEN
           nparent = 3
         ELSE
           nparent = nnew
         ENDIF
         tabres(i1:i2,j1:j2,k1:k2,m1:m2) =
     &               t(i1:i2,j1:j2,k1:k2,nparent,m1:m2)
        else
          western_side  = (nb == 1).AND.(ndir == 1)
          eastern_side  = (nb == 1).AND.(ndir == 2)
          southern_side = (nb == 2).AND.(ndir == 1)
          northern_side = (nb == 2).AND.(ndir == 2)
          IF (nnew == 3) THEN
            nsource = 2
          ELSE
            nsource = 4
          ENDIF
          ndest = nsource - 1
        if (southern_side) then
              if (.not.SOUTH_INTER) Then
                T_south(i1:i2,j1:j2,1:N,ndest,1:NT)=
     &          T_south(i1:i2,j1:j2,1:N,nsource,1:NT)
                T_south(i1:i2,j1:j2,1:N,nsource,1:NT) = tabres
              endif
        endif
        if (northern_side) then
              if (.not.NORTH_INTER) then
                T_north(i1:i2,j1:j2,1:N,ndest,1:NT)=
     &          T_north(i1:i2,j1:j2,1:N,nsource,1:NT)
                T_north(i1:i2,j1:j2,1:N,nsource,1:NT) = tabres
              endif
        endif
        if (western_side) then
              if (.not.WEST_INTER) then
                  T_west(i1:i2,j1:j2,1:N,ndest,1:NT)=
     &            T_west(i1:i2,j1:j2,1:N,nsource,1:NT)
                  T_west(i1:i2,j1:j2,1:N,nsource,1:NT) = tabres
              endif
        endif
        if (eastern_side) then
              if (.not.EAST_INTER) then
                  T_east(i1:i2,j1:j2,1:N,ndest,1:NT)=
     &            T_east(i1:i2,j1:j2,1:N,nsource,1:NT)
                  T_east(i1:i2,j1:j2,1:N,nsource,1:NT) = tabres
              endif
        endif
        endif
      return
       
      

      end subroutine Sub_Loop_t3Dinterp

      subroutine t3Dinterp_old(tabres,i1,i2,j1,j2,k1,k2,m1,m2)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_t3Dinterp_old(tabres,i1,i2,j1,j2,k1,k2,m1,m2
     &,padd_E,Mm,padd_X,Lm,t,nnew,iif)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: iif
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres
        end subroutine Sub_Loop_t3Dinterp_old

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres
      

        call Sub_Loop_t3Dinterp_old(tabres,i1,i2,j1,j2,k1,k2,m1,m2, Agri
     &f_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabv
     &ars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(10
     &9)%array5, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars_i(177)%iarr
     &ay0)

      end


      subroutine Sub_Loop_t3Dinterp_old(tabres,i1,i2,j1,j2,k1,k2,m1,m2,p
     &add_E,Mm,padd_X,Lm,t,nnew,iif)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: iif
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: m1
      integer(4) :: m2
      real, dimension(i1:i2,j1:j2,k1:k2,m1:m2) :: tabres

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                         
                                            
                         
      integer(4) :: nparent
       IF ((iif == 0)) THEN
         nparent = 3
       ELSE
         nparent = nnew
       ENDIF
       tabres(i1:i2,j1:j2,k1:k2,m1:m2) =
     &               t(i1:i2,j1:j2,k1:k2,nparent,m1:m2)
      return
       
      

      end subroutine Sub_Loop_t3Dinterp_old

