












      subroutine ExchangeParentValues()      


      use Agrif_Util
      interface
        subroutine Sub_Loop_ExchangeParentValues(padd_E,Mm,padd_X,Lm,Zt_
     &avg1)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
        end subroutine Sub_Loop_ExchangeParentValues

      end interface
      

        call Sub_Loop_ExchangeParentValues( Agrif_tabvars_i(188)%iarray0
     &, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%iarray0, Agri
     &f_tabvars_i(200)%iarray0, Agrif_tabvars(116)%array2)

      end


      subroutine Sub_Loop_ExchangeParentValues(padd_E,Mm,padd_X,Lm,Zt_av
     &g1)

      use Agrif_Types, only : Agrif_tabvars


      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
      call exchange_r2d_tile (1,Lm,1,Mm,
     &                   Zt_avg1(-1,-1))
             

      end subroutine Sub_Loop_ExchangeParentValues


      subroutine update2d
      

      use Agrif_Util
      interface
        subroutine Sub_Loop_update2d(padd_E,Mm,padd_X,Lm,DV_avg1,DU_avg1
     &,updatedvavg2id,updateduavg2id,updatevbarid,updateubarid,updatezet
     &aid,indupdate,nfast,nbcoarse,iif,Mmmpi,Lmmpi)
          use Agrif_Util
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: updatedvavg2id
      integer(4) :: updateduavg2id
      integer(4) :: updatevbarid
      integer(4) :: updateubarid
      integer(4) :: updatezetaid
      integer(4) :: indupdate
      integer(4) :: nfast
      integer(4) :: nbcoarse
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
        end subroutine Sub_Loop_update2d

      end interface
      

        call Sub_Loop_update2d( Agrif_tabvars_i(188)%iarray0, Agrif_tabv
     &ars_i(197)%iarray0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(
     &200)%iarray0, Agrif_tabvars(114)%array3, Agrif_tabvars(115)%array3
     &, Agrif_tabvars_i(8)%iarray0, Agrif_tabvars_i(9)%iarray0, Agrif_ta
     &bvars_i(10)%iarray0, Agrif_tabvars_i(11)%iarray0, Agrif_tabvars_i(
     &12)%iarray0, Agrif_tabvars_i(40)%iarray0, Agrif_tabvars_i(168)%iar
     &ray0, Agrif_tabvars_i(39)%iarray0, Agrif_tabvars_i(177)%iarray0, A
     &grif_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_update2d(padd_E,Mm,padd_X,Lm,DV_avg1,DU_avg1,u
     &pdatedvavg2id,updateduavg2id,updatevbarid,updateubarid,updatezetai
     &d,indupdate,nfast,nbcoarse,iif,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      use Agrif_Util
      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: updatedvavg2id
      integer(4) :: updateduavg2id
      integer(4) :: updatevbarid
      integer(4) :: updateubarid
      integer(4) :: updatezetaid
      integer(4) :: indupdate
      integer(4) :: nfast
      integer(4) :: nbcoarse
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
                     
                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                                 
                                                 
                                                 
                              
                              
                              
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
      external updateubar,updatevbar,updatezeta
      external updateduavg2, updatedvavg2
                                                                
      real, pointer, dimension(:,:) :: ztavg1_parent
      real, pointer, dimension(:,:) :: duavg2parent
                                                                   
      real, pointer, dimension(:,:) :: parenth
      real, pointer, dimension(:,:) :: parenton_u
      real, pointer, dimension(:,:) :: parentom_v
                                                 
      real, pointer, dimension(:,:) :: dvavg2parent
                                            
      integer(4) :: i
      integer(4) :: j
      integer(4) :: ipu
      integer(4) :: jpu
      integer(4) :: ipv
      integer(4) :: jpv
      integer(4) :: ipr
      integer(4) :: jpr
                       
      real :: dtparent
                                                                   
      real, pointer, dimension(:,:) :: pmparent
      real, pointer, dimension(:,:) :: pnparent
      real, pointer, dimension(:,:) :: rmaskparent
                                                             
      real, pointer, dimension(:,:,:) :: zetaparent
      real, pointer, dimension(:,:,:) :: parentzeta
                                                                
      real, pointer, dimension(:,:,:) :: parentubar
      real, pointer, dimension(:,:,:) :: parentdu_avg1
                                                                
      real, pointer, dimension(:,:,:) :: parentdv_avg1
      real, pointer, dimension(:,:,:) :: parentvbar
                                        
      real :: t1
      real :: t2
      real :: t3
      real :: t4
      real :: t5
      real :: t6
      real, dimension(1:5) :: tind
                   
      integer(4) :: j1
                                                              
      integer(4) :: parentknew
      integer(4) :: parentiif
      integer(4) :: parentnnew
      integer(4) :: parentkstp
                                             
      integer(4) :: iter
      integer(4) :: irhox
      integer(4) :: irhoy
      integer(4) :: irhot
                  
      real :: cff
                       
      real :: invrrhot
                      
      integer(4) :: pp
                          
      integer(4) :: idecal
                                                          
      real, allocatable, dimension(:,:) :: duavg2parentold
                                                          
      real, allocatable, dimension(:,:) :: dvavg2parentold
                          
      integer(4) :: pngrid
      external :: updatermask_child
!$AGRIF_DO_NOT_TREAT
      logical global_update_2d
      common/globalupdate/global_update_2d
      integer*4 :: iiffine, irhotfine, nbgrid
      common/updateubar2val/iiffine,irhotfine,
     &   nbgrid
!$AGRIF_END_DO_NOT_TREAT
      irhot = Agrif_Irhot()
      IF ((mod(iif+(nbcoarse-1)*mod(nfast,irhot),irhot).NE.0)) RETURN
         iiffine = iif
         irhotfine = Agrif_Irhot()
         nbgrid = Agrif_Fixed()
C$OMP BARRIER
C$OMP MASTER
        irhox = Agrif_Irhox()
        irhoy = Agrif_Irhoy()
        pngrid = Agrif_Parent_Fixed()
        if (nbgrid == grids_at_level(pngrid,0)) then
           Call Agrif_Set_Parent(indupdate,0)
        endif
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      indupdate = 0
      global_update_2d = .FALSE.
       Call Agrif_Update_Variable(updatezetaid,
     &         locupdate=(/1,4/),procname = updatezeta)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      Call Agrif_Update_Variable(updateubarid,
     &     locupdate1=(/0,3/),locupdate2=(/1,4/),
     &     procname = updateubar)
      Call Agrif_Update_Variable(updatevbarid,
     &     locupdate1=(/1,4/),locupdate2=(/0,3/),
     &     procname = updatevbar)
      IF (iif == nfast) THEN
      IF (nbcoarse == irhot) THEN
      global_update_2d = .TRUE.
      Agrif_UseSpecialValueInUpdate = .TRUE.
      Agrif_SpecialValueFineGrid = 0.D0
      Call Agrif_Update_Variable(updatezetaid,
     &         locupdate=(/1,0/),procname = updatezeta)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      Call Agrif_Update_Variable(updateubarid,
     &     locupdate1=(/0,-1/),locupdate2=(/1,-2/),
     &     procname = updateubar)
      Call Agrif_Update_Variable(updatevbarid,
     &     locupdate1=(/1,-2/),locupdate2=(/0,-1/),
     &     procname = updatevbar)
      Agrif_UseSpecialValueInUpdate = .FALSE.
      Call Agrif_Update_Variable(updateduavg2id,
     &     locupdate1=(/0,0/),locupdate2=(/1,1/),
     &         procname = updateduavg2)
      Call Agrif_Update_Variable(updatedvavg2id,
     &     locupdate1=(/1,1/),locupdate2=(/0,0/),
     &         procname = updatedvavg2)
      Call Agrif_ChildGrid_To_ParentGrid()
      Call ExchangeParentValues()
      Call Agrif_ParentGrid_To_ChildGrid()
      DU_avg1(:,:,4) = 0.D0
      DV_avg1(:,:,4) = 0.D0
      ENDIF
      ENDIF
C$OMP END MASTER
      return
       
      

      end subroutine Sub_Loop_update2d

      Subroutine Updateubar(tabres,i1,i2,j1,j2,k1,k2,before,nb,ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updateubar(tabres,i1,i2,j1,j2,k1,k2,before,n
     &b,ndir,padd_E,Mm,padd_X,Lm,ubar,on_u,h,knew,zeta,weight2,nstp,indu
     &pdate,coarsevalues,nfast,finevalues,nbstep3d,umask,du_avg3,iif,nne
     &w,DU_avg1,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: nstp
      integer(4) :: indupdate
      integer(4) :: nfast
      integer(4) :: nbstep3d
      integer(4) :: iif
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, allocatable, dimension(:,:) :: coarsevalues
      real, allocatable, dimension(:,:) :: finevalues
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: du_avg
     &3
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
        end subroutine Sub_Loop_Updateubar

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
      

        call Sub_Loop_Updateubar(tabres,i1,i2,j1,j2,k1,k2,before,nb,ndir
     &, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agri
     &f_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabv
     &ars(97)%array3, Agrif_tabvars(69)%array2, Agrif_tabvars(85)%array2
     &, Agrif_tabvars_i(179)%iarray0, Agrif_tabvars(98)%array3, Agrif_ta
     &bvars(34)%array2, Agrif_tabvars_i(176)%iarray0, Agrif_tabvars_i(40
     &)%iarray0, Agrif_tabvars(2)%array2, Agrif_tabvars_i(168)%iarray0, 
     &Agrif_tabvars(3)%array2, Agrif_tabvars_i(173)%iarray0, Agrif_tabva
     &rs(49)%array2, Agrif_tabvars(19)%array3, Agrif_tabvars_i(177)%iarr
     &ay0, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars(115)%array3, Agri
     &f_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_Updateubar(tabres,i1,i2,j1,j2,k1,k2,before,nb,
     &ndir,padd_E,Mm,padd_X,Lm,ubar,on_u,h,knew,zeta,weight2,nstp,indupd
     &ate,coarsevalues,nfast,finevalues,nbstep3d,umask,du_avg3,iif,nnew,
     &DU_avg1,Mmmpi,Lmmpi)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: nstp
      integer(4) :: indupdate
      integer(4) :: nfast
      integer(4) :: nbstep3d
      integer(4) :: iif
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, allocatable, dimension(:,:) :: coarsevalues
      real, allocatable, dimension(:,:) :: finevalues
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: du_avg
     &3
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                  
                                     
                     
                         
               
      real :: hzu
                    
      real :: t1
      real :: t2
      real :: t3
                    
      integer(4) :: i
      integer(4) :: j
                  
      real :: t4
      real :: t8
                                   
      integer(4) :: iter
      integer(4) :: iifparent
                                
      integer(4) :: oldindupdate
                    
      real :: rrhoy
                              
      real :: cff
      real :: cff1
      real :: cff2
                                                                       
                                                                 
 
      logical :: western_side
      logical :: eastern_side
      logical :: northern_side
      logical :: southern_side
!$AGRIF_DO_NOT_TREAT
      logical global_update_2d
      common/globalupdate/global_update_2d
      integer*4 :: iiffine, irhotfine, nbgrid
      common/updateubar2val/iiffine,irhotfine,
     &   nbgrid
!$AGRIF_END_DO_NOT_TREAT
                                     
      integer(4) :: irhot
      integer(4) :: ibegin
      integer(4) :: isize
                                 
      real, dimension(i1:i2,j1:j2) :: tabtemp
                             
      integer(4) :: n1
      integer(4) :: n2
      integer(4) :: n3
      IF (before) THEN
         rrhoy = real(Agrif_Irhoy())
         IF (global_update_2d) THEN
           tabres(i1:i2,j1:j2,1) = rrhoy * DU_avg1(i1:i2,j1:j2,nnew)
           RETURN
         ENDIF
         irhot=Agrif_Irhot()
         western_side  = (nb == 1).AND.(ndir == 1)
         eastern_side  = (nb == 1).AND.(ndir == 2)
         southern_side = (nb == 2).AND.(ndir == 1)
         northern_side = (nb == 2).AND.(ndir == 2)
         ibegin = min(irhot,iif+1)
          IF (iif .LE. irhot) THEN
          ibegin = iif + 1
          ENDIF
          do iter=1,ibegin
          do j=j1,j2
          do i=i1,i2
          tabres(i,j,iter) = du_avg3(i,j,iif-iter+1)
          enddo
          enddo
          enddo
         tabres = rrhoy * tabres
       ELSE
       IF (global_update_2d) THEN
         DU_avg1(i1:i2,j1:j2,nnew) = tabres(i1:i2,j1:j2,1)
     &             *umask(i1:i2,j1:j2)
         RETURN
       ENDIF
       ibegin = min(irhotfine,iiffine+1)
        IF (iiffine .LE. irhotfine) THEN
         ibegin = iiffine + 1
        ENDIF
       isize = (j2-j1+1)*(i2-i1+1)
       IF ((nbstep3d == 0).AND.(iif == 1)) THEN
       IF (.NOT.allocated(finevalues)) THEN
       Allocate(finevalues(isize,0:nfast))
       Allocate(coarsevalues(isize,0:nfast))
       ELSE
       CALL checksize(indupdate+isize)
       ENDIF
       ENDIF
          do iter=1,ibegin
          oldindupdate = indupdate
          do j=j1,j2
          do i=i1,i2
          oldindupdate = oldindupdate + 1
          finevalues(oldindupdate,iiffine-iter+1)
     &         = tabres(i,j,iter)
          enddo
          enddo
          enddo
          IF (iif == 1) THEN
          oldindupdate = indupdate
          do j=j1,j2
          do i=i1,i2
          oldindupdate = oldindupdate + 1
          coarsevalues(oldindupdate,0) = DU_avg1(i,j,nstp)
          enddo
          enddo
          ENDIF
         tabtemp = 0.D0
         do iter=0,iif-1
         cff = -weight2(iif,iter)
         call copy1d(tabtemp,coarsevalues(indupdate+1,iter),
     &               cff,isize)
         enddo
         do iter=0,iiffine
         cff=weight2(iiffine,iter)
         call copy1d(tabtemp,finevalues(indupdate+1,iter),
     &               cff,isize)
         enddo
         tabtemp = tabtemp/weight2(iif,iif)
         DO j=j1,j2
           DO i=i1,i2
        t1 = 0.5D0*(zeta(i,j,knew)+h(i,j)+
     &  zeta(i-1,j,knew)+h(i-1,j))*on_u(i,j)
           t2 = tabtemp(i,j)
     &           * umask(i,j)
          ubar(i,j,knew) = t2/t1
         indupdate = indupdate + 1
          coarsevalues(indupdate,iif) = tabtemp(i,j)
          ENDDO
         ENDDO
       ENDIF
      return
       
      

      end subroutine Sub_Loop_Updateubar

      Subroutine Updatezeta(tabres,i1,i2,j1,j2,k1,k2,before,nb,ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updatezeta(tabres,i1,i2,j1,j2,k1,k2,before,n
     &b,ndir,padd_E,Mm,padd_X,Lm,knew,weight2,kstp,zeta,indupdate,coarse
     &values,nfast,finevalues,nbstep3d,rmask,Zt_avg3,iif,Zt_avg1,Mmmpi,L
     &mmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: kstp
      integer(4) :: indupdate
      integer(4) :: nfast
      integer(4) :: nbstep3d
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, allocatable, dimension(:,:) :: coarsevalues
      real, allocatable, dimension(:,:) :: finevalues
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: Zt_avg
     &3
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
        end subroutine Sub_Loop_Updatezeta

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
      

        call Sub_Loop_Updatezeta(tabres,i1,i2,j1,j2,k1,k2,before,nb,ndir
     &, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agri
     &f_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabv
     &ars_i(179)%iarray0, Agrif_tabvars(34)%array2, Agrif_tabvars_i(181)
     &%iarray0, Agrif_tabvars(98)%array3, Agrif_tabvars_i(40)%iarray0, A
     &grif_tabvars(2)%array2, Agrif_tabvars_i(168)%iarray0, Agrif_tabvar
     &s(3)%array2, Agrif_tabvars_i(173)%iarray0, Agrif_tabvars(51)%array
     &2, Agrif_tabvars(20)%array3, Agrif_tabvars_i(177)%iarray0, Agrif_t
     &abvars(116)%array2, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(
     &195)%iarray0)

      end


      subroutine Sub_Loop_Updatezeta(tabres,i1,i2,j1,j2,k1,k2,before,nb,
     &ndir,padd_E,Mm,padd_X,Lm,knew,weight2,kstp,zeta,indupdate,coarseva
     &lues,nfast,finevalues,nbstep3d,rmask,Zt_avg3,iif,Zt_avg1,Mmmpi,Lmm
     &pi)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: kstp
      integer(4) :: indupdate
      integer(4) :: nfast
      integer(4) :: nbstep3d
      integer(4) :: iif
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, allocatable, dimension(:,:) :: coarsevalues
      real, allocatable, dimension(:,:) :: finevalues
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: Zt_avg
     &3
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                  
                                     
                     
                         
               
      real :: hzu
                    
      real :: t1
      real :: t2
      real :: t3
                    
      integer(4) :: i
      integer(4) :: j
                  
      real :: t4
      real :: t8
                                   
      integer(4) :: iter
      integer(4) :: iifparent
                                
      integer(4) :: oldindupdate
                              
      real :: cff
      real :: cff1
      real :: cff2
                                                                       
                                                                 
 
      logical :: western_side
      logical :: eastern_side
      logical :: northern_side
      logical :: southern_side
!$AGRIF_DO_NOT_TREAT
      logical global_update_2d
      common/globalupdate/global_update_2d
      integer*4 :: iiffine, irhotfine, nbgrid
      common/updateubar2val/iiffine,irhotfine,
     &   nbgrid
!$AGRIF_END_DO_NOT_TREAT
                                     
      integer(4) :: irhot
      integer(4) :: ibegin
      integer(4) :: isize
                                 
      real, dimension(i1:i2,j1:j2) :: tabtemp
                             
      integer(4) :: n1
      integer(4) :: n2
      integer(4) :: n3
      IF (before) THEN
         IF (global_update_2d) THEN
           tabres(i1:i2,j1:j2,1) = Zt_avg1(i1:i2,j1:j2)
           RETURN
         ENDIF
         irhot=Agrif_Irhot()
         western_side  = (nb == 1).AND.(ndir == 1)
         eastern_side  = (nb == 1).AND.(ndir == 2)
         southern_side = (nb == 2).AND.(ndir == 1)
         northern_side = (nb == 2).AND.(ndir == 2)
         ibegin = min(irhot,iif+1)
         IF (iif .LE. irhot) THEN
            ibegin = iif + 1
         ENDIF
          do iter=1,ibegin
          do j=j1,j2
          do i=i1,i2
          tabres(i,j,iter) = Zt_avg3(i,j,iif-iter+1)
          enddo
          enddo
          enddo
       ELSE
       IF (global_update_2d) THEN
         Zt_avg1(i1:i2,j1:j2) = tabres(i1:i2,j1:j2,1)
     &             *rmask(i1:i2,j1:j2)
         RETURN
       ENDIF
       ibegin = min(irhotfine,iiffine+1)
       IF (iiffine .LE. irhotfine) THEN
        ibegin = iiffine + 1
       ENDIF
       isize = (j2-j1+1)*(i2-i1+1)
       IF ((nbstep3d == 0).AND.(iif == 1)) THEN
       IF (.NOT.allocated(finevalues)) THEN
       Allocate(finevalues(isize,0:nfast))
       Allocate(coarsevalues(isize,0:nfast))
       ELSE
       CALL checksize(indupdate+isize)
       ENDIF
       ENDIF
          do iter=1,ibegin
          oldindupdate = indupdate
          do j=j1,j2
          do i=i1,i2
          oldindupdate = oldindupdate + 1
          finevalues(oldindupdate,iiffine-iter+1)
     &         = tabres(i,j,iter)
          enddo
          enddo
          enddo
          IF (iif == 1) THEN
          oldindupdate = indupdate
          do j=j1,j2
          do i=i1,i2
          oldindupdate = oldindupdate + 1
          coarsevalues(oldindupdate,0) = zeta(i,j,kstp)
          enddo
          enddo
          ENDIF
         tabtemp = 0.D0
         do iter=0,iif-1
         cff = -weight2(iif,iter)
         call copy1d(tabtemp,coarsevalues(indupdate+1,iter),
     &               cff,isize)
         enddo
         do iter=0,iiffine
         cff=weight2(iiffine,iter)
         call copy1d(tabtemp,finevalues(indupdate+1,iter),
     &               cff,isize)
         enddo
         tabtemp = tabtemp/weight2(iif,iif)
         DO j=j1,j2
           DO i=i1,i2
           t2 = tabtemp(i,j)
     &           * rmask(i,j)
          zeta(i,j,knew) = t2
          indupdate = indupdate + 1
          coarsevalues(indupdate,iif) = tabtemp(i,j)
          ENDDO
         ENDDO
       ENDIF
      return
       
      

      end subroutine Sub_Loop_Updatezeta

      Subroutine Updatevbar(tabres,i1,i2,j1,j2,k1,k2,before,nb,ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updatevbar(tabres,i1,i2,j1,j2,k1,k2,before,n
     &b,ndir,padd_E,Mm,padd_X,Lm,vbar,om_v,h,knew,zeta,weight2,nstp,indu
     &pdate,coarsevalues,nfast,finevalues,nbstep3d,vmask,dv_avg3,iif,nne
     &w,DV_avg1,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: nstp
      integer(4) :: indupdate
      integer(4) :: nfast
      integer(4) :: nbstep3d
      integer(4) :: iif
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, allocatable, dimension(:,:) :: coarsevalues
      real, allocatable, dimension(:,:) :: finevalues
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: dv_avg
     &3
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
        end subroutine Sub_Loop_Updatevbar

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
      

        call Sub_Loop_Updatevbar(tabres,i1,i2,j1,j2,k1,k2,before,nb,ndir
     &, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agri
     &f_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabv
     &ars(96)%array3, Agrif_tabvars(68)%array2, Agrif_tabvars(85)%array2
     &, Agrif_tabvars_i(179)%iarray0, Agrif_tabvars(98)%array3, Agrif_ta
     &bvars(34)%array2, Agrif_tabvars_i(176)%iarray0, Agrif_tabvars_i(40
     &)%iarray0, Agrif_tabvars(2)%array2, Agrif_tabvars_i(168)%iarray0, 
     &Agrif_tabvars(3)%array2, Agrif_tabvars_i(173)%iarray0, Agrif_tabva
     &rs(48)%array2, Agrif_tabvars(18)%array3, Agrif_tabvars_i(177)%iarr
     &ay0, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars(114)%array3, Agri
     &f_tabvars_i(194)%iarray0, Agrif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_Updatevbar(tabres,i1,i2,j1,j2,k1,k2,before,nb,
     &ndir,padd_E,Mm,padd_X,Lm,vbar,om_v,h,knew,zeta,weight2,nstp,indupd
     &ate,coarsevalues,nfast,finevalues,nbstep3d,vmask,dv_avg3,iif,nnew,
     &DV_avg1,Mmmpi,Lmmpi)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: knew
      integer(4) :: nstp
      integer(4) :: indupdate
      integer(4) :: nfast
      integer(4) :: nbstep3d
      integer(4) :: iif
      integer(4) :: nnew
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: h
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      real, dimension(0:NWEIGHT,0:NWEIGHT) :: weight2
      real, allocatable, dimension(:,:) :: coarsevalues
      real, allocatable, dimension(:,:) :: finevalues
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:NWEIGHT) :: dv_avg
     &3
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      integer(4) :: k1
      integer(4) :: k2
      real, dimension(i1:i2,j1:j2,k1:k2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                  
                                     
                     
                         
               
      real :: hzu
                    
      real :: t1
      real :: t2
      real :: t3
                    
      integer(4) :: i
      integer(4) :: j
                  
      real :: t4
      real :: t8
                                   
      integer(4) :: iter
      integer(4) :: iifparent
                                
      integer(4) :: oldindupdate
                    
      real :: rrhox
                              
      real :: cff
      real :: cff1
      real :: cff2
                                                                       
                                                                 
 
      logical :: western_side
      logical :: eastern_side
      logical :: northern_side
      logical :: southern_side
!$AGRIF_DO_NOT_TREAT
      logical global_update_2d
      common/globalupdate/global_update_2d
      integer*4 :: iiffine, irhotfine, nbgrid
      common/updateubar2val/iiffine,irhotfine,
     &   nbgrid
!$AGRIF_END_DO_NOT_TREAT
                                     
      integer(4) :: irhot
      integer(4) :: ibegin
      integer(4) :: isize
                                 
      real, dimension(i1:i2,j1:j2) :: tabtemp
                             
      integer(4) :: n1
      integer(4) :: n2
      integer(4) :: n3
      IF (before) THEN
         rrhox = real(Agrif_Irhox())
         IF (global_update_2d) THEN
           tabres(i1:i2,j1:j2,1) = rrhox * DV_avg1(i1:i2,j1:j2,nnew)
           RETURN
         ENDIF
         irhot=Agrif_Irhot()
         western_side  = (nb == 1).AND.(ndir == 1)
         eastern_side  = (nb == 1).AND.(ndir == 2)
         southern_side = (nb == 2).AND.(ndir == 1)
         northern_side = (nb == 2).AND.(ndir == 2)
         ibegin = min(irhot,iif+1)
         IF (iif .LE. irhot) THEN
           ibegin = iif + 1
         ENDIF
          do iter=1,ibegin
          do j=j1,j2
          do i=i1,i2
          tabres(i,j,iter) = dv_avg3(i,j,iif-iter+1)
          enddo
          enddo
          enddo
         tabres = rrhox * tabres
       ELSE
       IF (global_update_2d) THEN
         DV_avg1(i1:i2,j1:j2,nnew) = tabres(i1:i2,j1:j2,1)
     &             *vmask(i1:i2,j1:j2)
         RETURN
       ENDIF
       ibegin = min(irhotfine,iiffine+1)
       IF (iiffine .LE. irhotfine) THEN
         ibegin = iiffine + 1
       ENDIF
       isize = (j2-j1+1)*(i2-i1+1)
       IF ((nbstep3d == 0).AND.(iif == 1)) THEN
       IF (.NOT.allocated(finevalues)) THEN
       Allocate(finevalues(isize,0:nfast))
       Allocate(coarsevalues(isize,0:nfast))
       ELSE
       CALL checksize(indupdate+isize)
       ENDIF
       ENDIF
          do iter=1,ibegin
          oldindupdate = indupdate
          do j=j1,j2
          do i=i1,i2
          oldindupdate = oldindupdate + 1
          finevalues(oldindupdate,iiffine-iter+1)
     &         = tabres(i,j,iter)
          enddo
          enddo
          enddo
          IF (iif == 1) THEN
          oldindupdate = indupdate
          do j=j1,j2
          do i=i1,i2
          oldindupdate = oldindupdate + 1
          coarsevalues(oldindupdate,0) = DV_avg1(i,j,nstp)
          enddo
          enddo
          ENDIF
         tabtemp = 0.D0
         do iter=0,iif-1
         cff = -weight2(iif,iter)
         call copy1d(tabtemp,coarsevalues(indupdate+1,iter),
     &               cff,isize)
         enddo
         do iter=0,iiffine
         cff=weight2(iiffine,iter)
         call copy1d(tabtemp,finevalues(indupdate+1,iter),
     &               cff,isize)
         enddo
         tabtemp = tabtemp/weight2(iif,iif)
         DO j=j1,j2
           DO i=i1,i2
         t1 = 0.5D0*(zeta(i,j,knew)+h(i,j)+
     &  zeta(i,j-1,knew)+h(i,j-1))*om_v(i,j)
           t2 = tabtemp(i,j)
     &           * vmask(i,j)
          vbar(i,j,knew) = t2/t1
         indupdate = indupdate + 1
          coarsevalues(indupdate,iif) = tabtemp(i,j)
          ENDDO
         ENDDO
       ENDIF
      return
       
      

      end subroutine Sub_Loop_Updatevbar

      Subroutine Updateduavg2(tabres,i1,i2,j1,j2,before,nb,ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updateduavg2(tabres,i1,i2,j1,j2,before,nb,nd
     &ir,padd_E,Mm,padd_X,Lm,dt,DU_avg2,umask,pn,pm,Zt_avg1,DU_avg1)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: dt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DU_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
        end subroutine Sub_Loop_Updateduavg2

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
      

        call Sub_Loop_Updateduavg2(tabres,i1,i2,j1,j2,before,nb,ndir, Ag
     &rif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_ta
     &bvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_
     &r(46)%array0, Agrif_tabvars(113)%array2, Agrif_tabvars(49)%array2,
     & Agrif_tabvars(73)%array2, Agrif_tabvars(74)%array2, Agrif_tabvars
     &(116)%array2, Agrif_tabvars(115)%array3)

      end


      subroutine Sub_Loop_Updateduavg2(tabres,i1,i2,j1,j2,before,nb,ndir
     &,padd_E,Mm,padd_X,Lm,dt,DU_avg2,umask,pn,pm,Zt_avg1,DU_avg1)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: dt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DU_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: umask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                            
                               
                     
                    
      real :: rrhoy
                       
      integer(4) :: i
      integer(4) :: j
                           
                 
      real :: t1
                                            
      logical :: western_side
      logical :: eastern_side
      IF (before) THEN
         rrhoy = real(Agrif_Irhoy())
         tabres(i1:i2,j1:j2) = rrhoy * DU_avg1(i1:i2,j1:j2,4)
       ELSE
         western_side  = (nb == 1).AND.(ndir == 1)
         eastern_side  = (nb == 1).AND.(ndir == 2)
         if (western_side) then
         i = i1
         do j=j1,j2
         Zt_avg1(i-1,j) = Zt_avg1(i-1,j)-pm(i-1,j)*pn(i-1,j)*
     &  (tabres(i,j) - DU_avg1(i,j,5))
     &                * umask(i,j)
         enddo
         endif
         if (eastern_side) then
         i = i2
         do j=j1,j2
         Zt_avg1(i,j) = Zt_avg1(i,j)+pm(i,j)*pn(i,j)*
     &  (tabres(i,j) - DU_avg1(i,j,5))
     &                * umask(i,j)
         enddo
         endif
          DU_avg2(i1:i2,j1:j2) = (tabres(i1:i2,j1:j2)/dt)
     &                * umask(i1:i2,j1:j2)
       ENDIF
      return
       
      

      end subroutine Sub_Loop_Updateduavg2

      Subroutine Updatedvavg2(tabres,i1,i2,j1,j2,before,nb,ndir)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_Updatedvavg2(tabres,i1,i2,j1,j2,before,nb,nd
     &ir,padd_E,Mm,padd_X,Lm,dt,DV_avg2,vmask,pn,pm,Zt_avg1,DV_avg1)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: dt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DV_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
        end subroutine Sub_Loop_Updatedvavg2

      end interface
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir
      

        call Sub_Loop_Updatedvavg2(tabres,i1,i2,j1,j2,before,nb,ndir, Ag
     &rif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_ta
     &bvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars_
     &r(46)%array0, Agrif_tabvars(112)%array2, Agrif_tabvars(48)%array2,
     & Agrif_tabvars(73)%array2, Agrif_tabvars(74)%array2, Agrif_tabvars
     &(116)%array2, Agrif_tabvars(114)%array3)

      end


      subroutine Sub_Loop_Updatedvavg2(tabres,i1,i2,j1,j2,before,nb,ndir
     &,padd_E,Mm,padd_X,Lm,dt,DV_avg2,vmask,pn,pm,Zt_avg1,DV_avg1)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: dt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DV_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      integer(4) :: i1
      integer(4) :: i2
      integer(4) :: j1
      integer(4) :: j2
      real, dimension(i1:i2,j1:j2) :: tabres
      logical :: before
      integer(4) :: nb
      integer(4) :: ndir

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                            
                               
                     
                    
      real :: rrhox
                       
      integer(4) :: i
      integer(4) :: j
                           
                 
      real :: t1
                                             
      logical :: northern_side
      logical :: southern_side
      IF (before) THEN
         rrhox = real(Agrif_Irhox())
         tabres(i1:i2,j1:j2) = rrhox * DV_avg1(i1:i2,j1:j2,4)
       ELSE
         southern_side = (nb == 2).AND.(ndir == 1)
         northern_side = (nb == 2).AND.(ndir == 2)
         if (southern_side) then
         j = j1
         do i=i1,i2
         Zt_avg1(i,j-1) = Zt_avg1(i,j-1)-pm(i,j-1)*pn(i,j-1)*
     &  (tabres(i,j) - DV_avg1(i,j,5))
     &                * vmask(i,j)
         enddo
         endif
         if (northern_side) then
         j = j1
         do i=i1,i2
         Zt_avg1(i,j) = Zt_avg1(i,j)+pm(i,j)*pn(i,j)*
     &  (tabres(i,j) - DV_avg1(i,j,5))
     &                * vmask(i,j)
         enddo
         endif
          DV_avg2(i1:i2,j1:j2) = (tabres(i1:i2,j1:j2)/dt)
     &                * vmask(i1:i2,j1:j2)
       ENDIF
      return
       
      

      end subroutine Sub_Loop_Updatedvavg2

      subroutine checksize(isize)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_checksize(isize,coarsevalues,nfast,finevalue
     &s,Mmmpi,Lmmpi,padd_E,Mm,padd_X,Lm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: nfast
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real, allocatable, dimension(:,:) :: coarsevalues
      real, allocatable, dimension(:,:) :: finevalues
      integer(4) :: isize
        end subroutine Sub_Loop_checksize

      end interface
      integer(4) :: isize
      

        call Sub_Loop_checksize(isize, Agrif_tabvars(2)%array2, Agrif_ta
     &bvars_i(168)%iarray0, Agrif_tabvars(3)%array2, Agrif_tabvars_i(194
     &)%iarray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars_i(188)%iarr
     &ay0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(189)%iarray0, A
     &grif_tabvars_i(200)%iarray0)

      end


      subroutine Sub_Loop_checksize(isize,coarsevalues,nfast,finevalues,
     &Mmmpi,Lmmpi,padd_E,Mm,padd_X,Lm)

      use Agrif_Types, only : Agrif_tabvars


      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4) :: nfast
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real, allocatable, dimension(:,:) :: coarsevalues
      real, allocatable, dimension(:,:) :: finevalues
      integer(4) :: isize

                         
                                                    
      real, allocatable, dimension(:,:) :: tempvalues
                         
      integer(4) :: n1
      integer(4) :: n3
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
!$AGRIF_DO_NOT_TREAT
      logical global_update_2d
      common/globalupdate/global_update_2d
      integer*4 :: iiffine, irhotfine, nbgrid
      common/updateubar2val/iiffine,irhotfine,
     &   nbgrid
!$AGRIF_END_DO_NOT_TREAT
       IF (size(finevalues,1).LT.(isize)) THEN
       n1 = size(finevalues,1)
       allocate(tempvalues(n1,0:nfast))
       tempvalues=finevalues(1:n1,0:nfast)
       deallocate(finevalues)
       allocate(finevalues(isize,0:nfast))
       finevalues(1:n1,0:nfast) = tempvalues
       tempvalues=coarsevalues(1:n1,0:nfast)
       deallocate(coarsevalues)
       allocate(coarsevalues(isize,0:nfast))
       coarsevalues(1:n1,0:nfast) = tempvalues
       deallocate(tempvalues)
       ENDIF
      return
       
      

      end subroutine Sub_Loop_checksize

