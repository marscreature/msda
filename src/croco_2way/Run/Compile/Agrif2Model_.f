












!$AGRIF_DO_NOT_TREAT
      Subroutine Agrif_Set_numberofcells(Agrif_Gr)
      USE Agrif_Grids
      Implicit none
      Type(Agrif_Grid), Pointer :: Agrif_Gr
      if ( associated(Agrif_Curgrid) )then
      Agrif_Gr % nb(1) = Agrif_Curgrid % tabvars_i(201) % iarray0
      Agrif_Gr % nb(2) = Agrif_Curgrid % tabvars_i(198) % iarray0
      endif
      End Subroutine Agrif_Set_numberofcells
      Subroutine Agrif_Get_numberofcells(Agrif_Gr)
      USE Agrif_Grids
      Implicit none
      Type(Agrif_Grid), Pointer :: Agrif_Gr
      Agrif_Curgrid % tabvars_i(201) % iarray0 = Agrif_Gr % nb(1)
      Agrif_Curgrid % tabvars_i(198) % iarray0 = Agrif_Gr % nb(2)
      End Subroutine Agrif_Get_numberofcells
      Subroutine Agrif_Allocationcalls(Agrif_Gr)
      USE Agrif_Grids
      use gridinter2, only: Alloc_agrif_gridinter2
      use gridinter, only: Alloc_agrif_gridinter
      use work_agrif, only: Alloc_agrif_work_agrif
      use zoom3D_sponge_VN, only: Alloc_agrif_zoom3D_sponge_VN
      use zoom3D_sponge_UN, only: Alloc_agrif_zoom3D_sponge_UN
      use zoom3D_sponge_TN, only: Alloc_agrif_zoom3D_sponge_TN
      use zoom3D_sponge_VS, only: Alloc_agrif_zoom3D_sponge_VS
      use zoom3D_sponge_US, only: Alloc_agrif_zoom3D_sponge_US
      use zoom3D_sponge_TS, only: Alloc_agrif_zoom3D_sponge_TS
      use zoom3D_sponge_VE, only: Alloc_agrif_zoom3D_sponge_VE
      use zoom3D_sponge_UE, only: Alloc_agrif_zoom3D_sponge_UE
      use zoom3D_sponge_TE, only: Alloc_agrif_zoom3D_sponge_TE
      use zoom3D_sponge_VW, only: Alloc_agrif_zoom3D_sponge_VW
      use zoom3D_sponge_UW, only: Alloc_agrif_zoom3D_sponge_UW
      use zoom3D_sponge_TW, only: Alloc_agrif_zoom3D_sponge_TW
      use zoom2D_V2, only: Alloc_agrif_zoom2D_V2
      use zoom2D_U2, only: Alloc_agrif_zoom2D_U2
      use zoom2D_Zeta2, only: Alloc_agrif_zoom2D_Zeta2
      use huvagrif, only: Alloc_agrif_huvagrif
      use sponge_com, only: Alloc_agrif_sponge_com
      use updateprestep, only: Alloc_agrif_updateprestep
      use zoombc2D, only: Alloc_agrif_zoombc2D
      use averagebaro, only: Alloc_agrif_averagebaro
      use myfluxes, only: Alloc_agrif_myfluxes
      use updatevalues, only: Alloc_agrif_updatevalues
      use updateTprofile, only: Alloc_agrif_updateTprofile
      use weighting, only: Alloc_agrif_weighting
      use zoom3D_VN, only: Alloc_agrif_zoom3D_VN
      use zoom3D_UN, only: Alloc_agrif_zoom3D_UN
      use zoom3D_TN, only: Alloc_agrif_zoom3D_TN
      use zoom3D_VS, only: Alloc_agrif_zoom3D_VS
      use zoom3D_US, only: Alloc_agrif_zoom3D_US
      use zoom3D_TS, only: Alloc_agrif_zoom3D_TS
      use zoom3D_VE, only: Alloc_agrif_zoom3D_VE
      use zoom3D_UE, only: Alloc_agrif_zoom3D_UE
      use zoom3D_TE, only: Alloc_agrif_zoom3D_TE
      use zoom3D_VW, only: Alloc_agrif_zoom3D_VW
      use zoom3D_UW, only: Alloc_agrif_zoom3D_UW
      use zoom3D_TW, only: Alloc_agrif_zoom3D_TW
      use mask_p2, only: Alloc_agrif_mask_p2
      use mask_v, only: Alloc_agrif_mask_v
      use mask_u, only: Alloc_agrif_mask_u
      use mask_p, only: Alloc_agrif_mask_p
      use mask_r, only: Alloc_agrif_mask_r
      use metrics_grdscl, only: Alloc_agrif_metrics_grdscl
      use metrics_pnom_v, only: Alloc_agrif_metrics_pnom_v
      use metrics_pmon_u, only: Alloc_agrif_metrics_pmon_u
      use metrics_pnom_r, only: Alloc_agrif_metrics_pnom_r
      use metrics_pmon_r, only: Alloc_agrif_metrics_pmon_r
      use metrics_pnom_p, only: Alloc_agrif_metrics_pnom_p
      use metrics_pmon_p, only: Alloc_agrif_metrics_pmon_p
      use metrics_dndx, only: Alloc_agrif_metrics_dndx
      use metrics_dmde, only: Alloc_agrif_metrics_dmde
      use metrics_pnv, only: Alloc_agrif_metrics_pnv
      use metrics_pmu, only: Alloc_agrif_metrics_pmu
      use metrics_pmv, only: Alloc_agrif_metrics_pmv
      use metrics_pnu, only: Alloc_agrif_metrics_pnu
      use metrics_on_p, only: Alloc_agrif_metrics_on_p
      use metrics_omp, only: Alloc_agrif_metrics_omp
      use metrics_on_v, only: Alloc_agrif_metrics_on_v
      use metrics_omv, only: Alloc_agrif_metrics_omv
      use metrics_on_u, only: Alloc_agrif_metrics_on_u
      use metrics_omu, only: Alloc_agrif_metrics_omu
      use metrics_on_r, only: Alloc_agrif_metrics_on_r
      use metrics_omr, only: Alloc_agrif_metrics_omr
      use metrics_pn, only: Alloc_agrif_metrics_pn
      use metrics_pm, only: Alloc_agrif_metrics_pm
      use grid_lonv, only: Alloc_agrif_grid_lonv
      use grid_latv, only: Alloc_agrif_grid_latv
      use grid_lonu, only: Alloc_agrif_grid_lonu
      use grid_latu, only: Alloc_agrif_grid_latu
      use grid_lonr, only: Alloc_agrif_grid_lonr
      use grid_latr, only: Alloc_agrif_grid_latr
      use grid_angler, only: Alloc_agrif_grid_angler
      use grid_fomn, only: Alloc_agrif_grid_fomn
      use grid_f, only: Alloc_agrif_grid_f
      use grid_hinv, only: Alloc_agrif_grid_hinv
      use grid_h, only: Alloc_agrif_grid_h
      use cncscrum, only: Alloc_agrif_cncscrum
      use incscrum, only: Alloc_agrif_incscrum
      use timers_roms, only: Alloc_agrif_timers_roms
      use scalars_main, only: Alloc_agrif_scalars_main
      use private_scratch, only: Alloc_agrif_private_scratch
      use ocean_vbar, only: Alloc_agrif_ocean_vbar
      use ocean_ubar, only: Alloc_agrif_ocean_ubar
      use ocean_zeta, only: Alloc_agrif_ocean_zeta
      use ocean_qp1, only: Alloc_agrif_ocean_qp1
      use ocean_rho, only: Alloc_agrif_ocean_rho
      use ocean_rho1, only: Alloc_agrif_ocean_rho1
      use grid_We, only: Alloc_agrif_grid_We
      use grid_zr, only: Alloc_agrif_grid_zr
      use grid_Hz, only: Alloc_agrif_grid_Hz
      use grid_Hvom, only: Alloc_agrif_grid_Hvom
      use grid_Huon, only: Alloc_agrif_grid_Huon
      use grid_zw, only: Alloc_agrif_grid_zw
      use grid_Hz_bak, only: Alloc_agrif_grid_Hz_bak
      use ocean_t, only: Alloc_agrif_ocean_t
      use ocean_v, only: Alloc_agrif_ocean_v
      use ocean_u, only: Alloc_agrif_ocean_u
      use coup_DV_avg2, only: Alloc_agrif_coup_DV_avg2
      use coup_DU_avg2, only: Alloc_agrif_coup_DU_avg2
      use coup_DV_avg1, only: Alloc_agrif_coup_DV_avg1
      use coup_DU_avg1, only: Alloc_agrif_coup_DU_avg1
      use ocean_Zt_avg1, only: Alloc_agrif_ocean_Zt_avg1
      use coup_rvfrc_bak, only: Alloc_agrif_coup_rvfrc_bak
      use coup_rufrc_bak, only: Alloc_agrif_coup_rufrc_bak
      use coup_rvfrc, only: Alloc_agrif_coup_rvfrc
      use coup_rufrc, only: Alloc_agrif_coup_rufrc
      use coup_rhoS, only: Alloc_agrif_coup_rhoS
      use coup_rhoA, only: Alloc_agrif_coup_rhoA
      use climat_udat1, only: Alloc_agrif_climat_udat1
      use climat_vclima, only: Alloc_agrif_climat_vclima
      use climat_uclima, only: Alloc_agrif_climat_uclima
      use climat_M3nudgcof, only: Alloc_agrif_climat_M3nudgcof
      use climat_vbclima, only: Alloc_agrif_climat_vbclima
      use climat_ubclima, only: Alloc_agrif_climat_ubclima
      use climat_M2nudgcof, only: Alloc_agrif_climat_M2nudgcof
      use climat_vclm, only: Alloc_agrif_climat_vclm
      use climat_uclm, only: Alloc_agrif_climat_uclm
      use climat_vbclm, only: Alloc_agrif_climat_vbclm
      use climat_ubclm, only: Alloc_agrif_climat_ubclm
      use climat_tdat, only: Alloc_agrif_climat_tdat
      use climat_tclima, only: Alloc_agrif_climat_tclima
      use climat_Tnudgcof, only: Alloc_agrif_climat_Tnudgcof
      use climat_tclm, only: Alloc_agrif_climat_tclm
      use climat_zdat1, only: Alloc_agrif_climat_zdat1
      use climat_sshg, only: Alloc_agrif_climat_sshg
      use climat_Znudgcof, only: Alloc_agrif_climat_Znudgcof
      use climat_ssh, only: Alloc_agrif_climat_ssh
      use bry_indices_array, only: Alloc_agrif_bry_indices_array
      use lmd_kpp_ustar, only: Alloc_agrif_lmd_kpp_ustar
      use lmd_kpp_ghats, only: Alloc_agrif_lmd_kpp_ghats
      use lmd_kpp_hbl, only: Alloc_agrif_lmd_kpp_hbl
      use lmd_kpp_kbbl, only: Alloc_agrif_lmd_kpp_kbbl
      use lmd_kpp_hbbl, only: Alloc_agrif_lmd_kpp_hbbl
      use lmd_kpp_kbl, only: Alloc_agrif_lmd_kpp_kbl
      use mixing_bvf, only: Alloc_agrif_mixing_bvf
      use mixing_Akt, only: Alloc_agrif_mixing_Akt
      use mixing_Akv, only: Alloc_agrif_mixing_Akv
      use mixing_idRz, only: Alloc_agrif_mixing_idRz
      use mixing_dRde, only: Alloc_agrif_mixing_dRde
      use mixing_dRdx, only: Alloc_agrif_mixing_dRdx
      use mixing_diff3d_v, only: Alloc_agrif_mixing_diff3d_v
      use mixing_diff3d_u, only: Alloc_agrif_mixing_diff3d_u
      use mixing_diff4, only: Alloc_agrif_mixing_diff4
      use mixing_diff4_sponge, only: Alloc_agrif_mixing_diff4_sponge
      use mixing_diff2, only: Alloc_agrif_mixing_diff2
      use mixing_diff2_sponge, only: Alloc_agrif_mixing_diff2_sponge
      use mixing_visc2_sponge_p, only: Alloc_agrif_mixing_visc2_sponge_p
      use mixing_visc2_sponge_r, only: Alloc_agrif_mixing_visc2_sponge_r
      use mixing_visc2_p, only: Alloc_agrif_mixing_visc2_p
      use mixing_visc2_r, only: Alloc_agrif_mixing_visc2_r
      use tides_UV_Tphase, only: Alloc_agrif_tides_UV_Tphase
      use tides_UV_Tminor, only: Alloc_agrif_tides_UV_Tminor
      use tides_UV_Tmajor, only: Alloc_agrif_tides_UV_Tmajor
      use tides_UV_Tangle, only: Alloc_agrif_tides_UV_Tangle
      use tides_SSH_Tphase, only: Alloc_agrif_tides_SSH_Tphase
      use tides_SSH_Tamp, only: Alloc_agrif_tides_SSH_Tamp
      use tides_Tperiod, only: Alloc_agrif_tides_Tperiod
      use srfdat1, only: Alloc_agrif_srfdat1
      use srfdat_srflxg, only: Alloc_agrif_srfdat_srflxg
      use forces_srflx, only: Alloc_agrif_forces_srflx
      use bulkdat2_wspd, only: Alloc_agrif_bulkdat2_wspd
      use bulkdat2_wnd, only: Alloc_agrif_bulkdat2_wnd
      use bulkdat2_tim, only: Alloc_agrif_bulkdat2_tim
      use bulkdat2_for, only: Alloc_agrif_bulkdat2_for
      use bulk_vwndg, only: Alloc_agrif_bulk_vwndg
      use bulk_uwndg, only: Alloc_agrif_bulk_uwndg
      use bulkdat_wspdg, only: Alloc_agrif_bulkdat_wspdg
      use bulkdat_radswg, only: Alloc_agrif_bulkdat_radswg
      use bulkdat_radlwg, only: Alloc_agrif_bulkdat_radlwg
      use bulkdat_prateg, only: Alloc_agrif_bulkdat_prateg
      use bulkdat_rhumg, only: Alloc_agrif_bulkdat_rhumg
      use bulkdat_tairg, only: Alloc_agrif_bulkdat_tairg
      use bulk_vwnd, only: Alloc_agrif_bulk_vwnd
      use bulk_uwnd, only: Alloc_agrif_bulk_uwnd
      use bulk_wspd, only: Alloc_agrif_bulk_wspd
      use bulk_radsw, only: Alloc_agrif_bulk_radsw
      use bulk_radlw, only: Alloc_agrif_bulk_radlw
      use bulk_prate, only: Alloc_agrif_bulk_prate
      use bulk_rhum, only: Alloc_agrif_bulk_rhum
      use bulk_tair, only: Alloc_agrif_bulk_tair
      use forces_btflx, only: Alloc_agrif_forces_btflx
      use stfdat3, only: Alloc_agrif_stfdat3
      use stfdat2, only: Alloc_agrif_stfdat2
      use stfdat1, only: Alloc_agrif_stfdat1
      use stfdat_stflxg, only: Alloc_agrif_stfdat_stflxg
      use frc_shflx_sen, only: Alloc_agrif_frc_shflx_sen
      use frc_shflx_lat, only: Alloc_agrif_frc_shflx_lat
      use frc_shflx_rlw, only: Alloc_agrif_frc_shflx_rlw
      use frc_shflx_rsw, only: Alloc_agrif_frc_shflx_rsw
      use forces_stflx, only: Alloc_agrif_forces_stflx
      use bmsdat1, only: Alloc_agrif_bmsdat1
      use bmsdat_bvstrg, only: Alloc_agrif_bmsdat_bvstrg
      use bmsdat_bustrg, only: Alloc_agrif_bmsdat_bustrg
      use forces_bvstr, only: Alloc_agrif_forces_bvstr
      use forces_bustr, only: Alloc_agrif_forces_bustr
      use smsdat1, only: Alloc_agrif_smsdat1
      use smsdat_svstrg, only: Alloc_agrif_smsdat_svstrg
      use smsdat_sustrg, only: Alloc_agrif_smsdat_sustrg
      use forces_svstr, only: Alloc_agrif_forces_svstr
      use forces_sustr, only: Alloc_agrif_forces_sustr
      use avg_Akt, only: Alloc_agrif_avg_Akt
      use avg_Akv, only: Alloc_agrif_avg_Akv
      use avg_diff3d, only: Alloc_agrif_avg_diff3d
      use avg_shflx_sen, only: Alloc_agrif_avg_shflx_sen
      use avg_shflx_lat, only: Alloc_agrif_avg_shflx_lat
      use avg_shflx_rlw, only: Alloc_agrif_avg_shflx_rlw
      use avg_shflx_rsw, only: Alloc_agrif_avg_shflx_rsw
      use avg_hbbl, only: Alloc_agrif_avg_hbbl
      use avg_hbl, only: Alloc_agrif_avg_hbl
      use avg_stflx, only: Alloc_agrif_avg_stflx
      use avg_w, only: Alloc_agrif_avg_w
      use avg_omega, only: Alloc_agrif_avg_omega
      use avg_rho, only: Alloc_agrif_avg_rho
      use avg_t, only: Alloc_agrif_avg_t
      use avg_v, only: Alloc_agrif_avg_v
      use avg_u, only: Alloc_agrif_avg_u
      use avg_srflx, only: Alloc_agrif_avg_srflx
      use avg_svstr, only: Alloc_agrif_avg_svstr
      use avg_sustr, only: Alloc_agrif_avg_sustr
      use avg_wstr, only: Alloc_agrif_avg_wstr
      use avg_bostr, only: Alloc_agrif_avg_bostr
      use avg_vbar, only: Alloc_agrif_avg_vbar
      use avg_ubar, only: Alloc_agrif_avg_ubar
      use avg_zeta, only: Alloc_agrif_avg_zeta
      use nils_jerlov, only: Alloc_agrif_nils_jerlov
      use xyz, only: Alloc_agrif_xyz
      use zzz, only: Alloc_agrif_zzz
      use work2d2, only: Alloc_agrif_work2d2
      use work2d, only: Alloc_agrif_work2d
      use work3d_r, only: Alloc_agrif_work3d_r
      use work3d, only: Alloc_agrif_work3d
      Implicit none
      Type(Agrif_Grid), Pointer :: Agrif_Gr
      call Alloc_agrif_gridinter2(Agrif_Gr)
      call Alloc_agrif_gridinter(Agrif_Gr)
      call Alloc_agrif_work_agrif(Agrif_Gr)
      call Alloc_agrif_zoom3D_sponge_VN(Agrif_Gr)
      call Alloc_agrif_zoom3D_sponge_UN(Agrif_Gr)
      call Alloc_agrif_zoom3D_sponge_TN(Agrif_Gr)
      call Alloc_agrif_zoom3D_sponge_VS(Agrif_Gr)
      call Alloc_agrif_zoom3D_sponge_US(Agrif_Gr)
      call Alloc_agrif_zoom3D_sponge_TS(Agrif_Gr)
      call Alloc_agrif_zoom3D_sponge_VE(Agrif_Gr)
      call Alloc_agrif_zoom3D_sponge_UE(Agrif_Gr)
      call Alloc_agrif_zoom3D_sponge_TE(Agrif_Gr)
      call Alloc_agrif_zoom3D_sponge_VW(Agrif_Gr)
      call Alloc_agrif_zoom3D_sponge_UW(Agrif_Gr)
      call Alloc_agrif_zoom3D_sponge_TW(Agrif_Gr)
      call Alloc_agrif_zoom2D_V2(Agrif_Gr)
      call Alloc_agrif_zoom2D_U2(Agrif_Gr)
      call Alloc_agrif_zoom2D_Zeta2(Agrif_Gr)
      call Alloc_agrif_huvagrif(Agrif_Gr)
      call Alloc_agrif_sponge_com(Agrif_Gr)
      call Alloc_agrif_updateprestep(Agrif_Gr)
      call Alloc_agrif_zoombc2D(Agrif_Gr)
      call Alloc_agrif_averagebaro(Agrif_Gr)
      call Alloc_agrif_myfluxes(Agrif_Gr)
      call Alloc_agrif_updatevalues(Agrif_Gr)
      call Alloc_agrif_updateTprofile(Agrif_Gr)
      call Alloc_agrif_weighting(Agrif_Gr)
      call Alloc_agrif_zoom3D_VN(Agrif_Gr)
      call Alloc_agrif_zoom3D_UN(Agrif_Gr)
      call Alloc_agrif_zoom3D_TN(Agrif_Gr)
      call Alloc_agrif_zoom3D_VS(Agrif_Gr)
      call Alloc_agrif_zoom3D_US(Agrif_Gr)
      call Alloc_agrif_zoom3D_TS(Agrif_Gr)
      call Alloc_agrif_zoom3D_VE(Agrif_Gr)
      call Alloc_agrif_zoom3D_UE(Agrif_Gr)
      call Alloc_agrif_zoom3D_TE(Agrif_Gr)
      call Alloc_agrif_zoom3D_VW(Agrif_Gr)
      call Alloc_agrif_zoom3D_UW(Agrif_Gr)
      call Alloc_agrif_zoom3D_TW(Agrif_Gr)
      call Alloc_agrif_mask_p2(Agrif_Gr)
      call Alloc_agrif_mask_v(Agrif_Gr)
      call Alloc_agrif_mask_u(Agrif_Gr)
      call Alloc_agrif_mask_p(Agrif_Gr)
      call Alloc_agrif_mask_r(Agrif_Gr)
      call Alloc_agrif_metrics_grdscl(Agrif_Gr)
      call Alloc_agrif_metrics_pnom_v(Agrif_Gr)
      call Alloc_agrif_metrics_pmon_u(Agrif_Gr)
      call Alloc_agrif_metrics_pnom_r(Agrif_Gr)
      call Alloc_agrif_metrics_pmon_r(Agrif_Gr)
      call Alloc_agrif_metrics_pnom_p(Agrif_Gr)
      call Alloc_agrif_metrics_pmon_p(Agrif_Gr)
      call Alloc_agrif_metrics_dndx(Agrif_Gr)
      call Alloc_agrif_metrics_dmde(Agrif_Gr)
      call Alloc_agrif_metrics_pnv(Agrif_Gr)
      call Alloc_agrif_metrics_pmu(Agrif_Gr)
      call Alloc_agrif_metrics_pmv(Agrif_Gr)
      call Alloc_agrif_metrics_pnu(Agrif_Gr)
      call Alloc_agrif_metrics_on_p(Agrif_Gr)
      call Alloc_agrif_metrics_omp(Agrif_Gr)
      call Alloc_agrif_metrics_on_v(Agrif_Gr)
      call Alloc_agrif_metrics_omv(Agrif_Gr)
      call Alloc_agrif_metrics_on_u(Agrif_Gr)
      call Alloc_agrif_metrics_omu(Agrif_Gr)
      call Alloc_agrif_metrics_on_r(Agrif_Gr)
      call Alloc_agrif_metrics_omr(Agrif_Gr)
      call Alloc_agrif_metrics_pn(Agrif_Gr)
      call Alloc_agrif_metrics_pm(Agrif_Gr)
      call Alloc_agrif_grid_lonv(Agrif_Gr)
      call Alloc_agrif_grid_latv(Agrif_Gr)
      call Alloc_agrif_grid_lonu(Agrif_Gr)
      call Alloc_agrif_grid_latu(Agrif_Gr)
      call Alloc_agrif_grid_lonr(Agrif_Gr)
      call Alloc_agrif_grid_latr(Agrif_Gr)
      call Alloc_agrif_grid_angler(Agrif_Gr)
      call Alloc_agrif_grid_fomn(Agrif_Gr)
      call Alloc_agrif_grid_f(Agrif_Gr)
      call Alloc_agrif_grid_hinv(Agrif_Gr)
      call Alloc_agrif_grid_h(Agrif_Gr)
      call Alloc_agrif_cncscrum(Agrif_Gr)
      call Alloc_agrif_incscrum(Agrif_Gr)
      call Alloc_agrif_timers_roms(Agrif_Gr)
      call Alloc_agrif_scalars_main(Agrif_Gr)
      call Alloc_agrif_private_scratch(Agrif_Gr)
      call Alloc_agrif_ocean_vbar(Agrif_Gr)
      call Alloc_agrif_ocean_ubar(Agrif_Gr)
      call Alloc_agrif_ocean_zeta(Agrif_Gr)
      call Alloc_agrif_ocean_qp1(Agrif_Gr)
      call Alloc_agrif_ocean_rho(Agrif_Gr)
      call Alloc_agrif_ocean_rho1(Agrif_Gr)
      call Alloc_agrif_grid_We(Agrif_Gr)
      call Alloc_agrif_grid_zr(Agrif_Gr)
      call Alloc_agrif_grid_Hz(Agrif_Gr)
      call Alloc_agrif_grid_Hvom(Agrif_Gr)
      call Alloc_agrif_grid_Huon(Agrif_Gr)
      call Alloc_agrif_grid_zw(Agrif_Gr)
      call Alloc_agrif_grid_Hz_bak(Agrif_Gr)
      call Alloc_agrif_ocean_t(Agrif_Gr)
      call Alloc_agrif_ocean_v(Agrif_Gr)
      call Alloc_agrif_ocean_u(Agrif_Gr)
      call Alloc_agrif_coup_DV_avg2(Agrif_Gr)
      call Alloc_agrif_coup_DU_avg2(Agrif_Gr)
      call Alloc_agrif_coup_DV_avg1(Agrif_Gr)
      call Alloc_agrif_coup_DU_avg1(Agrif_Gr)
      call Alloc_agrif_ocean_Zt_avg1(Agrif_Gr)
      call Alloc_agrif_coup_rvfrc_bak(Agrif_Gr)
      call Alloc_agrif_coup_rufrc_bak(Agrif_Gr)
      call Alloc_agrif_coup_rvfrc(Agrif_Gr)
      call Alloc_agrif_coup_rufrc(Agrif_Gr)
      call Alloc_agrif_coup_rhoS(Agrif_Gr)
      call Alloc_agrif_coup_rhoA(Agrif_Gr)
      call Alloc_agrif_climat_udat1(Agrif_Gr)
      call Alloc_agrif_climat_vclima(Agrif_Gr)
      call Alloc_agrif_climat_uclima(Agrif_Gr)
      call Alloc_agrif_climat_M3nudgcof(Agrif_Gr)
      call Alloc_agrif_climat_vbclima(Agrif_Gr)
      call Alloc_agrif_climat_ubclima(Agrif_Gr)
      call Alloc_agrif_climat_M2nudgcof(Agrif_Gr)
      call Alloc_agrif_climat_vclm(Agrif_Gr)
      call Alloc_agrif_climat_uclm(Agrif_Gr)
      call Alloc_agrif_climat_vbclm(Agrif_Gr)
      call Alloc_agrif_climat_ubclm(Agrif_Gr)
      call Alloc_agrif_climat_tdat(Agrif_Gr)
      call Alloc_agrif_climat_tclima(Agrif_Gr)
      call Alloc_agrif_climat_Tnudgcof(Agrif_Gr)
      call Alloc_agrif_climat_tclm(Agrif_Gr)
      call Alloc_agrif_climat_zdat1(Agrif_Gr)
      call Alloc_agrif_climat_sshg(Agrif_Gr)
      call Alloc_agrif_climat_Znudgcof(Agrif_Gr)
      call Alloc_agrif_climat_ssh(Agrif_Gr)
      call Alloc_agrif_bry_indices_array(Agrif_Gr)
      call Alloc_agrif_lmd_kpp_ustar(Agrif_Gr)
      call Alloc_agrif_lmd_kpp_ghats(Agrif_Gr)
      call Alloc_agrif_lmd_kpp_hbl(Agrif_Gr)
      call Alloc_agrif_lmd_kpp_kbbl(Agrif_Gr)
      call Alloc_agrif_lmd_kpp_hbbl(Agrif_Gr)
      call Alloc_agrif_lmd_kpp_kbl(Agrif_Gr)
      call Alloc_agrif_mixing_bvf(Agrif_Gr)
      call Alloc_agrif_mixing_Akt(Agrif_Gr)
      call Alloc_agrif_mixing_Akv(Agrif_Gr)
      call Alloc_agrif_mixing_idRz(Agrif_Gr)
      call Alloc_agrif_mixing_dRde(Agrif_Gr)
      call Alloc_agrif_mixing_dRdx(Agrif_Gr)
      call Alloc_agrif_mixing_diff3d_v(Agrif_Gr)
      call Alloc_agrif_mixing_diff3d_u(Agrif_Gr)
      call Alloc_agrif_mixing_diff4(Agrif_Gr)
      call Alloc_agrif_mixing_diff4_sponge(Agrif_Gr)
      call Alloc_agrif_mixing_diff2(Agrif_Gr)
      call Alloc_agrif_mixing_diff2_sponge(Agrif_Gr)
      call Alloc_agrif_mixing_visc2_sponge_p(Agrif_Gr)
      call Alloc_agrif_mixing_visc2_sponge_r(Agrif_Gr)
      call Alloc_agrif_mixing_visc2_p(Agrif_Gr)
      call Alloc_agrif_mixing_visc2_r(Agrif_Gr)
      call Alloc_agrif_tides_UV_Tphase(Agrif_Gr)
      call Alloc_agrif_tides_UV_Tminor(Agrif_Gr)
      call Alloc_agrif_tides_UV_Tmajor(Agrif_Gr)
      call Alloc_agrif_tides_UV_Tangle(Agrif_Gr)
      call Alloc_agrif_tides_SSH_Tphase(Agrif_Gr)
      call Alloc_agrif_tides_SSH_Tamp(Agrif_Gr)
      call Alloc_agrif_tides_Tperiod(Agrif_Gr)
      call Alloc_agrif_srfdat1(Agrif_Gr)
      call Alloc_agrif_srfdat_srflxg(Agrif_Gr)
      call Alloc_agrif_forces_srflx(Agrif_Gr)
      call Alloc_agrif_bulkdat2_wspd(Agrif_Gr)
      call Alloc_agrif_bulkdat2_wnd(Agrif_Gr)
      call Alloc_agrif_bulkdat2_tim(Agrif_Gr)
      call Alloc_agrif_bulkdat2_for(Agrif_Gr)
      call Alloc_agrif_bulk_vwndg(Agrif_Gr)
      call Alloc_agrif_bulk_uwndg(Agrif_Gr)
      call Alloc_agrif_bulkdat_wspdg(Agrif_Gr)
      call Alloc_agrif_bulkdat_radswg(Agrif_Gr)
      call Alloc_agrif_bulkdat_radlwg(Agrif_Gr)
      call Alloc_agrif_bulkdat_prateg(Agrif_Gr)
      call Alloc_agrif_bulkdat_rhumg(Agrif_Gr)
      call Alloc_agrif_bulkdat_tairg(Agrif_Gr)
      call Alloc_agrif_bulk_vwnd(Agrif_Gr)
      call Alloc_agrif_bulk_uwnd(Agrif_Gr)
      call Alloc_agrif_bulk_wspd(Agrif_Gr)
      call Alloc_agrif_bulk_radsw(Agrif_Gr)
      call Alloc_agrif_bulk_radlw(Agrif_Gr)
      call Alloc_agrif_bulk_prate(Agrif_Gr)
      call Alloc_agrif_bulk_rhum(Agrif_Gr)
      call Alloc_agrif_bulk_tair(Agrif_Gr)
      call Alloc_agrif_forces_btflx(Agrif_Gr)
      call Alloc_agrif_stfdat3(Agrif_Gr)
      call Alloc_agrif_stfdat2(Agrif_Gr)
      call Alloc_agrif_stfdat1(Agrif_Gr)
      call Alloc_agrif_stfdat_stflxg(Agrif_Gr)
      call Alloc_agrif_frc_shflx_sen(Agrif_Gr)
      call Alloc_agrif_frc_shflx_lat(Agrif_Gr)
      call Alloc_agrif_frc_shflx_rlw(Agrif_Gr)
      call Alloc_agrif_frc_shflx_rsw(Agrif_Gr)
      call Alloc_agrif_forces_stflx(Agrif_Gr)
      call Alloc_agrif_bmsdat1(Agrif_Gr)
      call Alloc_agrif_bmsdat_bvstrg(Agrif_Gr)
      call Alloc_agrif_bmsdat_bustrg(Agrif_Gr)
      call Alloc_agrif_forces_bvstr(Agrif_Gr)
      call Alloc_agrif_forces_bustr(Agrif_Gr)
      call Alloc_agrif_smsdat1(Agrif_Gr)
      call Alloc_agrif_smsdat_svstrg(Agrif_Gr)
      call Alloc_agrif_smsdat_sustrg(Agrif_Gr)
      call Alloc_agrif_forces_svstr(Agrif_Gr)
      call Alloc_agrif_forces_sustr(Agrif_Gr)
      call Alloc_agrif_avg_Akt(Agrif_Gr)
      call Alloc_agrif_avg_Akv(Agrif_Gr)
      call Alloc_agrif_avg_diff3d(Agrif_Gr)
      call Alloc_agrif_avg_shflx_sen(Agrif_Gr)
      call Alloc_agrif_avg_shflx_lat(Agrif_Gr)
      call Alloc_agrif_avg_shflx_rlw(Agrif_Gr)
      call Alloc_agrif_avg_shflx_rsw(Agrif_Gr)
      call Alloc_agrif_avg_hbbl(Agrif_Gr)
      call Alloc_agrif_avg_hbl(Agrif_Gr)
      call Alloc_agrif_avg_stflx(Agrif_Gr)
      call Alloc_agrif_avg_w(Agrif_Gr)
      call Alloc_agrif_avg_omega(Agrif_Gr)
      call Alloc_agrif_avg_rho(Agrif_Gr)
      call Alloc_agrif_avg_t(Agrif_Gr)
      call Alloc_agrif_avg_v(Agrif_Gr)
      call Alloc_agrif_avg_u(Agrif_Gr)
      call Alloc_agrif_avg_srflx(Agrif_Gr)
      call Alloc_agrif_avg_svstr(Agrif_Gr)
      call Alloc_agrif_avg_sustr(Agrif_Gr)
      call Alloc_agrif_avg_wstr(Agrif_Gr)
      call Alloc_agrif_avg_bostr(Agrif_Gr)
      call Alloc_agrif_avg_vbar(Agrif_Gr)
      call Alloc_agrif_avg_ubar(Agrif_Gr)
      call Alloc_agrif_avg_zeta(Agrif_Gr)
      call Alloc_agrif_nils_jerlov(Agrif_Gr)
      call Alloc_agrif_xyz(Agrif_Gr)
      call Alloc_agrif_zzz(Agrif_Gr)
      call Alloc_agrif_work2d2(Agrif_Gr)
      call Alloc_agrif_work2d(Agrif_Gr)
      call Alloc_agrif_work3d_r(Agrif_Gr)
      call Alloc_agrif_work3d(Agrif_Gr)
      End Subroutine Agrif_Allocationcalls
      Subroutine Agrif_probdim_modtype_def()
      Use Agrif_Grids
      Implicit none
      Agrif_NbVariables(0) = 255
      Agrif_NbVariables(1) = 17
      Agrif_NbVariables(2) = 60
      Agrif_NbVariables(3) = 16
      Agrif_NbVariables(4) = 275
      Agrif_Probdim = 2
      AGRIF_USE_FIXED_GRIDS = 0
      AGRIF_USE_ONLY_FIXED_GRIDS = 1
      Return
      End Subroutine Agrif_probdim_modtype_def
      Subroutine Agrif_clustering_def()
      Use Agrif_Grids
      Implicit none
      Return
      End Subroutine Agrif_clustering_def
!$AGRIF_END_DO_NOT_TREAT
