

























! $Id: cppdefs.h 1628 2015-01-10 13:53:00Z marchesiello $
!
!======================================================================
! CROCO is a branch of ROMS developped at IRD and INRIA, in France
! The two other branches from UCLA (Shchepetkin et al) 
! and Rutgers University (Arango et al) are under MIT/X style license.
! CROCO specific routines (nesting) are under CeCILL-C license.
! 
! CROCO website : http://www.croco-ocean.org
!======================================================================
!




                      
                      
                      
                      
                      
                      
                      

                      
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      

                      
                      


                      
                      
                      
                      

                      
                      
                      
                      
                      


! $Id: set_global_definitions.h 1618 2014-12-18 14:39:51Z rblod $
!
!======================================================================
! CROCO is a branch of ROMS developped at IRD and INRIA, in France
! The two other branches from UCLA (Shchepetkin et al) 
! and Rutgers University (Arango et al) are under MIT/X style license.
! CROCO specific routines (nesting) are under CeCILL-C license.
! 
! CROCO website : http://www.croco-ocean.org
!======================================================================
!


 







 



 
  


































































 

 

 



! $Id: set_global_definitions.h 1618 2014-12-18 14:39:51Z rblod $
!
!======================================================================
! CROCO is a branch of ROMS developped at IRD and INRIA, in France
! The two other branches from UCLA (Shchepetkin et al) 
! and Rutgers University (Arango et al) are under MIT/X style license.
! CROCO specific routines (nesting) are under CeCILL-C license.
! 
! CROCO website : http://www.croco-ocean.org
!======================================================================
!



























!-# define float dfloat
!-# define FLoaT dfloat
!-# define FLOAT dfloat
!-# define sqrt dsqrt
!-# define SQRT dsqrt
!-# define exp dexp
!-# define EXP dexp
!-# define dtanh dtanh
!-# define TANH dtanh



 




MODULE par_pisces
   !!======================================================================
   !!                        ***  par_pisces  ***
   !! TOP :   set the PISCES parameters
   !!======================================================================
   !! History :   2.0  !  2007-12  (C. Ethe, G. Madec)  revised architecture
   !!----------------------------------------------------------------------
   IMPLICIT NONE
   PUBLIC

   !!---------------------------------------------------------------------
   !!   Default                                   No CFC geochemical model
   !!---------------------------------------------------------------------
                                                                    
                                                                 
     

      LOGICAL, parameter, public :: lk_pisces = .FALSE.
      LOGICAL, parameter, public :: lk_kriest = .FALSE.
      INTEGER, parameter, public :: jp_pisces = 0
      INTEGER, parameter, public :: jp_pisces_2d = 0
      INTEGER, parameter, public :: jp_pisces_3d = 0
      INTEGER, parameter, public :: jptra = jp_pisces
      INTEGER, parameter, public :: jp_pcs0 = 1
      INTEGER, parameter, public :: jp_pcs1 = jp_pisces
                                                                    
                                                                 
        
                                                                    
                                                                 
          
                                                                    
                                                                 
                               
                                                                    
                                                                 
                               

   ! Starting/ending PISCES do-loop indices (N.B. no PISCES : jpl_pcs < jpf_pcs the do-loop are never done)
                                                                    
                                                                 
                                         
                                                                    
                                                                 
                                 
                                                                    
                                                                 
                                 

   !!======================================================================
 
      end module par_pisces

