












      subroutine init_scalars (ierr)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_init_scalars(ierr,got_tclm,vname,ismooth,dt,
     &csmooth,date_str,bry_id,ncidqbar,ncidclm,ncidbulk,ncidfrc,ncidavg,
     &ncidhis,ncidrst,rx1,rx0,bc_crss,latmax,latmin,lonmax,lonmin,Cu_max
     &,Cu_min,grdmax,grdmin,hmax,hmin,volume,avgkp,avgpe,avgke,bc_count,
     &tile_count,nrecavg,nrechis,nrecrst,trd_count,CPU_time,proc,may_day
     &_flag,first_time,synchro_flag,nfast,nnew,nrhs,nstp,ntstart,knew,kr
     &hs,kstp,iic,PREDICTOR_2D_STEP,tdays,time,mynode,numthreads,padd_E,
     &Mm,padd_X,Lm)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4) :: ismooth
      real :: dt
      real :: csmooth
      character(80) :: date_str
      integer(4) :: bry_id
      integer(4) :: ncidqbar
      integer(4) :: ncidclm
      integer(4) :: ncidbulk
      integer(4) :: ncidfrc
      integer(4) :: ncidavg
      integer(4) :: ncidhis
      integer(4) :: ncidrst
      real :: rx1
      real :: rx0
      real(8) :: bc_crss
      real :: latmax
      real :: latmin
      real :: lonmax
      real :: lonmin
      real :: Cu_max
      real :: Cu_min
      real :: grdmax
      real :: grdmin
      real :: hmax
      real :: hmin
      real(8) :: volume
      real(8) :: avgkp
      real(8) :: avgpe
      real(8) :: avgke
      integer(4) :: bc_count
      integer(4) :: tile_count
      integer(4) :: nrecavg
      integer(4) :: nrechis
      integer(4) :: nrecrst
      integer(4) :: trd_count
      integer(4) :: may_day_flag
      integer(4) :: first_time
      logical :: synchro_flag
      integer(4) :: nfast
      integer(4) :: nnew
      integer(4) :: nrhs
      integer(4) :: nstp
      integer(4) :: ntstart
      integer(4) :: knew
      integer(4) :: krhs
      integer(4) :: kstp
      integer(4) :: iic
      logical :: PREDICTOR_2D_STEP
      real :: tdays
      real :: time
      integer(4) :: mynode
      integer(4) :: numthreads
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      logical, dimension(1:NT) :: got_tclm
      character(75), dimension(1:20,1:500) :: vname
      real(4), dimension(0:31,0:NPP) :: CPU_time
      integer(4), dimension(0:31,0:NPP) :: proc
      integer(4) :: ierr
        end subroutine Sub_Loop_init_scalars

      end interface
      integer(4) :: ierr
      

        call Sub_Loop_init_scalars(ierr, Agrif_tabvars_l(12)%larray1, Ag
     &rif_tabvars_c(1)%carray2, Agrif_tabvars_i(228)%iarray0, Agrif_tabv
     &ars_r(46)%array0, Agrif_tabvars_r(50)%array0, Agrif_tabvars_c(15)%
     &carray0, Agrif_tabvars_i(225)%iarray0, Agrif_tabvars_i(131)%iarray
     &0, Agrif_tabvars_i(139)%iarray0, Agrif_tabvars_i(140)%iarray0, Agr
     &if_tabvars_i(141)%iarray0, Agrif_tabvars_i(81)%iarray0, Agrif_tabv
     &ars_i(113)%iarray0, Agrif_tabvars_i(125)%iarray0, Agrif_tabvars_r(
     &22)%array0, Agrif_tabvars_r(23)%array0, Agrif_tabvars_r(1)%darray0
     &, Agrif_tabvars_r(6)%array0, Agrif_tabvars_r(7)%array0, Agrif_tabv
     &ars_r(8)%array0, Agrif_tabvars_r(9)%array0, Agrif_tabvars_r(10)%ar
     &ray0, Agrif_tabvars_r(11)%array0, Agrif_tabvars_r(12)%array0, Agri
     &f_tabvars_r(13)%array0, Agrif_tabvars_r(14)%array0, Agrif_tabvars_
     &r(15)%array0, Agrif_tabvars_r(5)%darray0, Agrif_tabvars_r(2)%darra
     &y0, Agrif_tabvars_r(3)%darray0, Agrif_tabvars_r(4)%darray0, Agrif_
     &tabvars_i(159)%iarray0, Agrif_tabvars_i(161)%iarray0, Agrif_tabvar
     &s_i(80)%iarray0, Agrif_tabvars_i(112)%iarray0, Agrif_tabvars_i(124
     &)%iarray0, Agrif_tabvars_i(157)%iarray0, Agrif_tabvars(86)%sarray2
     &, Agrif_tabvars_i(158)%iarray2, Agrif_tabvars_i(162)%iarray0, Agri
     &f_tabvars_i(160)%iarray0, Agrif_tabvars_l(8)%larray0, Agrif_tabvar
     &s_i(168)%iarray0, Agrif_tabvars_i(174)%iarray0, Agrif_tabvars_i(17
     &5)%iarray0, Agrif_tabvars_i(176)%iarray0, Agrif_tabvars_i(171)%iar
     &ray0, Agrif_tabvars_i(179)%iarray0, Agrif_tabvars_i(180)%iarray0, 
     &Agrif_tabvars_i(181)%iarray0, Agrif_tabvars_i(182)%iarray0, Agrif_
     &tabvars_l(11)%larray0, Agrif_tabvars_r(41)%array0, Agrif_tabvars_r
     &(44)%array0, Agrif_tabvars_i(156)%iarray0, Agrif_tabvars_i(172)%ia
     &rray0, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0,
     & Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0)

      end


      subroutine Sub_Loop_init_scalars(ierr,got_tclm,vname,ismooth,dt,cs
     &mooth,date_str,bry_id,ncidqbar,ncidclm,ncidbulk,ncidfrc,ncidavg,nc
     &idhis,ncidrst,rx1,rx0,bc_crss,latmax,latmin,lonmax,lonmin,Cu_max,C
     &u_min,grdmax,grdmin,hmax,hmin,volume,avgkp,avgpe,avgke,bc_count,ti
     &le_count,nrecavg,nrechis,nrecrst,trd_count,CPU_time,proc,may_day_f
     &lag,first_time,synchro_flag,nfast,nnew,nrhs,nstp,ntstart,knew,krhs
     &,kstp,iic,PREDICTOR_2D_STEP,tdays,time,mynode,numthreads,padd_E,Mm
     &,padd_X,Lm)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4) :: ismooth
      real :: dt
      real :: csmooth
      character(80) :: date_str
      integer(4) :: bry_id
      integer(4) :: ncidqbar
      integer(4) :: ncidclm
      integer(4) :: ncidbulk
      integer(4) :: ncidfrc
      integer(4) :: ncidavg
      integer(4) :: ncidhis
      integer(4) :: ncidrst
      real :: rx1
      real :: rx0
      real(8) :: bc_crss
      real :: latmax
      real :: latmin
      real :: lonmax
      real :: lonmin
      real :: Cu_max
      real :: Cu_min
      real :: grdmax
      real :: grdmin
      real :: hmax
      real :: hmin
      real(8) :: volume
      real(8) :: avgkp
      real(8) :: avgpe
      real(8) :: avgke
      integer(4) :: bc_count
      integer(4) :: tile_count
      integer(4) :: nrecavg
      integer(4) :: nrechis
      integer(4) :: nrecrst
      integer(4) :: trd_count
      integer(4) :: may_day_flag
      integer(4) :: first_time
      logical :: synchro_flag
      integer(4) :: nfast
      integer(4) :: nnew
      integer(4) :: nrhs
      integer(4) :: nstp
      integer(4) :: ntstart
      integer(4) :: knew
      integer(4) :: krhs
      integer(4) :: kstp
      integer(4) :: iic
      logical :: PREDICTOR_2D_STEP
      real :: tdays
      real :: time
      integer(4) :: mynode
      integer(4) :: numthreads
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      logical, dimension(1:NT) :: got_tclm
      character(75), dimension(1:20,1:500) :: vname
      real(4), dimension(0:31,0:NPP) :: CPU_time
      integer(4), dimension(0:31,0:NPP) :: proc
      integer(4) :: ierr

                   

                                             
      integer(4) :: i
      integer(4) :: j
      integer(4) :: itrc
      integer(4) :: lvar
      integer(4) :: lenstr
                                  
      character(20) :: nametrc
      character(20) :: unitt
                                  
      character(60) :: vname1
      character(60) :: vname3
                                    
      integer(4) :: omp_get_num_threads
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
C$OMP PARALLEL
C$OMP CRITICAL (isca_cr_rgn)
      numthreads=omp_get_num_threads()
C$OMP END CRITICAL (isca_cr_rgn)
C$OMP END PARALLEL
      if (mynode.eq.0) write(stdout,'(1x,A,3(1x,A,I3),A)') 'NUMBER',
     &    'OF THREADS:',numthreads,'BLOCKING:',NSUB_X,'x',NSUB_E,'.'
      if (numthreads.gt.NPP) then
        if (mynode.eq.0) write(stdout,'(/1x,A,I3/)')
     &    'ERROR: Requested number of threads exceeds NPP =', NPP
        ierr=ierr+1
      elseif (mod(NSUB_X*NSUB_E,numthreads).ne.0) then
        if (mynode.eq.0) write(stdout,
     &                '(/1x,A,1x,A,I3,4x,A,I3,4x,A,I4,A)') 'ERROR:',
     &                'wrong choice of numthreads =', numthreads,
     &                'NSUB_X =', NSUB_X, 'NSUB_E =', NSUB_E, '.'
        ierr=ierr+1
      endif
      time=0.D0
      tdays=0.D0
      PREDICTOR_2D_STEP=.FALSE.
      iic=0
      kstp=1
      krhs=1
      knew=1
      ntstart=1
      nstp=1
      nrhs=1
      nnew=1
      nfast=1
      synchro_flag=.true.
      first_time=0
      may_day_flag=0
      do j=0,NPP
        do i=0,31
          proc(i,j)=0
          CPU_time(i,j)=0.D0
        enddo
      enddo
      trd_count=0
      nrecrst=0
      nrechis=0
      nrecavg=0
      tile_count=0
      bc_count=0
      avgke=0.D0
      avgpe=0.D0
      avgkp=0.D0
      volume=0.D0
      hmin=+1.D+20
      hmax=-1.D+20
      grdmin=+1.D+20
      grdmax=-1.D+20
      Cu_min=+1.D+20
      Cu_max=-1.D+20
      lonmin=+1.D+20
      lonmax=-1.D+20
      latmin=+1.D+20
      latmax=-1.D+20
      bc_crss=0.D0
      rx0=-1.D+20
      rx1=-1.D+20
      ncidrst=-1
      ncidhis=-1
      ncidavg=-1
      ncidfrc=-1
      ncidbulk=-1
      ncidclm=-1
      ncidqbar=-1
       bry_id=-1
      call get_date (date_str)
      csmooth=dt/(86400.D0)
      ismooth=int(1.D0/csmooth)
      vname(:,:)='  '
      vname(1,indxTime)='scrum_time'
      vname(2,indxTime)='time since initialization'
      vname(3,indxTime)='second'
      vname(4,indxTime)='time, scalar, series'
      vname(5,indxTime)='time'
      vname(6,indxTime)='    '
      vname(7,indxTime)='T'
      vname(8,indxTime)='.NO.'
      vname(9,indxTime)=' '
      vname(1,indxTime2)='time'
      vname(2,indxTime2)='time since initialization'
      vname(3,indxTime2)='second'
      vname(4,indxTime2)='time, scalar, series'
      vname(5,indxTime2)='time'
      vname(6,indxTime2)='  '
      vname(7,indxTime2)='T '
      vname(8,indxTime2)='.NO.'
      vname(9,indxTime2)=' '
      vname(1,indxZ)='zeta                                        '
      vname(2,indxZ)='free-surface                                '
      vname(3,indxZ)='meter                                       '
      vname(4,indxZ)='free-surface, scalar, series                '
      vname(5,indxZ)='sea_surface_height                          '
      vname(6,indxZ)='lat_rho lon_rho                             '
      vname(7,indxZ)='                                            '
      vname(1,indxUb)='ubar                                       '
      vname(2,indxUb)='vertically integrated u-momentum component '
      vname(3,indxUb)='meter second-1                             '
      vname(4,indxUb)='ubar-velocity, scalar, series              '
      vname(5,indxUb)='barotropic_sea_water_x_'
     &               // 'velocity_at_u_location'
      vname(6,indxUb)='lat_u lon_u                                '
      vname(7,indxUb)='                                           '
      vname(1,indxVb)='vbar                                       '
      vname(2,indxVb)='vertically integrated v-momentum component '
      vname(3,indxVb)='meter second-1                             '
      vname(4,indxVb)='vbar-velocity, scalar, series              '
      vname(5,indxVb)='barotropic_sea_water_y_velocity_at_'
     &                // 'v_location'
      vname(6,indxVb)='lat_v lon_v                                '
      vname(7,indxVb)='                                           '
      vname(11,indxVb)=' '
      vname(1,indxBostr)='bostr                                   '
      vname(2,indxBostr)='Kinematic bottom stress                 '
      vname(3,indxBostr)='N/m2                                    '
      vname(4,indxBostr)='                                        '
      vname(5,indxBostr)='                                        '
      vname(6,indxBostr)='lat_rho lon_rho                         '
      vname(7,indxBostr)='                                        '
      vname(1,indxWstr)='wstr                                     '
      vname(2,indxWstr)='Kinematic wind stress                    '
      vname(3,indxWstr)='N/m2                                     '
      vname(4,indxWstr)='                                         '
      vname(5,indxWstr)='magnitude_of_surface_downward_stress     '
      vname(6,indxWstr)='lat_rho lon_rho                          '
      vname(7,indxWstr)='                                         '
      vname(1,indxUWstr)='sustr                                   '
      vname(2,indxUWstr)='Kinematic u wind stress component       '
      vname(3,indxUWstr)='N/m2                                    '
      vname(4,indxUWstr)='                                        '
      vname(5,indxUWstr)='surface_downward_eastward_stress        '
      vname(6,indxUWstr)='lat_u lon_u                             '
      vname(7,indxUWstr)='                                        '
      vname(8,indxUWstr)='                                        '
      vname(1,indxVWstr)='svstr                                   '
      vname(2,indxVWstr)='Kinematic v wind stress component       '
      vname(3,indxVWstr)='N/m2                                    '
      vname(4,indxVWstr)='                                        '
      vname(5,indxVWstr)='surface_downward_northward_stress       '
      vname(6,indxVWstr)='lat_v lon_v                             '
      vname(7,indxVWstr)='                                        '
      vname(1,indxU)='u                                           '
      vname(2,indxU)='u-momentum component                        '
      vname(3,indxU)='meter second-1                              '
      vname(4,indxU)='u-velocity, scalar, series                  '
      vname(5,indxU)='sea_water_x_velocity_at_u_location          '
      vname(6,indxU)='lat_u lon_u                                 '
      vname(7,indxU)='                                            '
      vname(1,indxV)='v                                           '
      vname(2,indxV)='v-momentum component                        '
      vname(3,indxV)='meter second-1                              '
      vname(4,indxV)='v-velocity, scalar, series                  '
      vname(5,indxV)='sea_water_y_velocity_at_v_location          '
      vname(6,indxV)='lat_v lon_v                                 '
      vname(7,indxV)='                                            '
      vname(1,indxT)='temp                                        '
      vname(2,indxT)='potential temperature                       '
      vname(3,indxT)='Celsius                                     '
      vname(4,indxT)='temperature, scalar, series                 '
      vname(5,indxT)='sea_water_potential_temperature             '
      vname(6,indxT)='lat_rho lon_rho                             '
      vname(7,indxT)='                                            '
      vname(1,indxS)='salt                                        '
      vname(2,indxS)='salinity                                    '
      vname(3,indxS)='PSU                                         '
      vname(4,indxS)='salinity, scalar, series                    '
      vname(5,indxS)='sea_water_salinity                          '
      vname(6,indxS)='lat_rho lon_rho                             '
      vname(7,indxS)='                                            '
      vname(1,indxShflx)='shflux                                  '
      vname(2,indxShflx)='surface net heat flux                   '
      vname(3,indxShflx)='Watts meter-2                           '
      vname(4,indxShflx)='surface heat flux, scalar, series       '
      vname(6,indxShflx)='lat_rho lon_rho                         '
      vname(7,indxShflx)='                                        '
      vname(1,indxSwflx)='swflux                                  '
      vname(2,indxSwflx)='surface freshwater flux (E-P)           '
      vname(3,indxSwflx)='centimeter day-1                        '
      vname(4,indxSwflx)='surface freshwater flux, scalar, series '
      vname(5,indxSwflx)='                                        '
      vname(6,indxSwflx)='lat_rho lon_rho                         '
      vname(7,indxSwflx)='                                        '
      vname(8,indxSwflx)='                                        '
      vname(1,indxShflx_rsw)='radsw                               '
      vname(2,indxShflx_rsw)='Short-wave surface radiation        '
      vname(3,indxShflx_rsw)='Watts meter-2                       '
      vname(4,indxShflx_rsw)='                                    '
      vname(5,indxShflx_rsw)='                                    '
      vname(6,indxShflx_rsw)='lat_rho lon_rho                     '
      vname(7,indxShflx_rsw)='                                    '
      vname(1,indxShflx_rlw)='shflx_rlw                           '
      vname(2,indxShflx_rlw)='Long-wave surface radiation         '
      vname(3,indxShflx_rlw)='Watts meter-2                       '
      vname(5,indxShflx_rlw)='                                    '
      vname(6,indxShflx_rlw)='lat_rho lon_rho                     '
      vname(7,indxShflx_rlw)='                                    '
      vname(1,indxShflx_lat)='shflx_lat                           '
      vname(2,indxShflx_lat)='Latent surface heat flux            '
      vname(3,indxShflx_lat)='Watts meter-2                       '
      vname(4,indxShflx_sen)='                                    '
      vname(5,indxShflx_lat)='                                    '
      vname(6,indxShflx_lat)='lat_rho lon_rho                     '
      vname(7,indxShflx_lat)='                                    '
      vname(1,indxShflx_sen)='shflx_sen                           '
      vname(2,indxShflx_sen)='Sensible surface heat flux          '
      vname(3,indxShflx_sen)='Watts meter-2                       '
      vname(4,indxShflx_sen)='                                    '
      vname(4,indxShflx_sen)='                                    '
      vname(6,indxShflx_sen)='lat_rho lon_rho                     '
      vname(7,indxShflx_sen)='                                    '
      vname(7,indxShflx_sen)='  '
      vname(1,indxO)='omega                                       '
      vname(2,indxO)='S-coordinate vertical momentum component    '
      vname(3,indxO)='meter second-1                              '
      vname(4,indxO)='omega, scalar, series                       '
      vname(5,indxO)='                                            '
      vname(6,indxO)='lat_rho lon_rho                             '
      vname(7,indxO)='                                            '
      vname(1,indxW)='w                                           '
      vname(2,indxW)='vertical momentum component                 '
      vname(3,indxW)='meter second-1                              '
      vname(4,indxW)='w-velocity, scalar, series                  '
      vname(5,indxW)='upward_sea_water_velocity                   '
      vname(6,indxW)='lat_rho lon_rho                             '
      vname(1,indxR)='rho                                         '
      vname(2,indxR)='density anomaly                             '
      vname(3,indxR)='kilogram meter-3                            '
      vname(4,indxR)='density, scalar, series                     '
      vname(5,indxR)='sea_water_sigma_t                           '
      vname(7,indxR)='                                            '
      vname(1,indxDiff)='diff3d                                   '
      vname(2,indxDiff)='horizontal diffusivity coefficient       '
      vname(3,indxDiff)='meter4 second-1                          '
      vname(5,indxDiff)='ocean_tracer_xy_biharmonic_diffusivity   '
      vname(4,indxDiff)='diff3d, scalar, series                   '
      vname(6,indxDiff)='lat_rho lon_rho                          '
      vname(7,indxDiff)='                                         '
      vname(1,indxAkv)='AKv                                       '
      vname(2,indxAkv)='vertical viscosity coefficient            '
      vname(3,indxAkv)='meter2 second-1                           '
      vname(4,indxAkv)='AKv, scalar, series                       '
      vname(5,indxAkv)='ocean_vertical_momentum_diffusivity_'
     &                 / / 'at_w_location                         '
      vname(6,indxAkv)='lat_rho lon_rho                           '
      vname(7,indxAkv)='                                          '
      vname(1,indxAkt)='AKt                                       '
      vname(2,indxAkt)='temperature vertical diffusion coefficient'
      vname(3,indxAkt)='meter2 second-1                           '
      vname(4,indxAkt)='AKt, scalar, series                       '
      vname(5,indxAkt)='ocean_vertical_heat_diffusivity_'
     &                               / /  'at_w_location          '
      vname(6,indxAkt)='lat_rho lon_rho                           '
      vname(7,indxAkt)='                                          '
      vname(1,indxAks)='AKs                                       '
      vname(2,indxAks)='salinity vertical diffusion coefficient   '
      vname(3,indxAks)='meter2 second-1                           '
      vname(4,indxAks)='AKs, scalar, series                       '
      vname(5,indxAks)='ocean_vertical_salt_diffusivity_'
     &               / /  'at_w_location                          '
      vname(6,indxAks)='lat_rho lon_rho                           '
      vname(7,indxAks)='                                          '
      vname(1,indxHbl)='hbl                                       '
      vname(2,indxHbl)='depth of planetary boundary layer         '
      vname(3,indxHbl)='meter                                     '
      vname(4,indxHbl)='hbl, scalar, series                       '
      vname(5,indxHbl)='ocean_mixed_layer_thickness_defined_'
     &                / / 'by_mixing_scheme                       '
      vname(6,indxHbl)='lat_rho lon_rho                           '
      vname(7,indxHbl)='                                          '
      vname(1,indxHbbl)='hbbl                                     '
      vname(2,indxHbbl)='depth of bottom boundary layer           '
      vname(3,indxHbbl)='meter                                    '
      vname(4,indxHbbl)='hbbl, scalar, series                     '
      vname(5,indxHbbl)='                                         '
      vname(6,indxHbbl)='lat_rho lon_rho                          '
      vname(7,indxHbbl)='                                         '
      vname(1,indxSSH)='SSH                                       '
      vname(2,indxSSH)='sea surface height                        '
      vname(3,indxSSH)='meter                                     '
      vname(4,indxSSH)='SSH, scalar, series                       '
      vname(5,indxSSH)='sea_surface_height_above_sea_level        '
      vname(6,indxSSH)='lat_rho lon_rho                           '
      vname(7,indxSSH)='                                          '
      vname(1,indxSUSTR)='sustr                                   '
      vname(2,indxSUSTR)='surface u-momentum stress               '
      vname(3,indxSUSTR)='Newton meter-2                          '
      vname(4,indxSUSTR)='surface u-mom. stress, scalar, series   '
      vname(5,indxSUSTR)='surface_downward_x_stress               '
      vname(6,indxSUSTR)='lat_u lon_u                             '
      vname(7,indxSUSTR)='                                        '
      vname(1,indxSVSTR)='svstr                                   '
      vname(2,indxSVSTR)='surface v-momentum stress               '
      vname(3,indxSVSTR)='Newton meter-2                          '
      vname(4,indxSVSTR)='surface v-mom. stress, scalar, series   '
      vname(5,indxSVSTR)='surface_downward_y_stress               '
      vname(6,indxSVSTR)='lat_v lon_v                             '
      vname(7,indxSVSTR)='                                        '
      vname(1,indxWSPD)='wspd                                     '
      vname(2,indxWSPD)='surface wind speed 10 m                  '
      vname(3,indxWSPD)='meter second-1                           '
      vname(4,indxWSPD)='surface wind speed, scalar, series       '
      vname(5,indxWSPD)='wind_speed                               '
      vname(6,indxWSPD)='lat_rho lon_rho                          '
      vname(7,indxWSPD)='                                         '
      vname(1,indxTAIR)='tair                                     '
      vname(2,indxTAIR)='surface air temperature 2m               '
      vname(3,indxTAIR)='Celsius                                  '
      vname(4,indxTAIR)='surface air temperature, scalar, series  '
      vname(5,indxTAIR)='air_temperature_at_2m                    '
      vname(6,indxTAIR)='lat_rho lon_rho                          '
      vname(7,indxTAIR)='                                         '
      vname(1,indxRHUM)='rhum                                     '
      vname(2,indxRHUM)='surface air relative humidity 2m         '
      vname(3,indxRHUM)='fraction                                 '
      vname(4,indxRHUM)='surface relative humidity, scalar, series'
      vname(5,indxRHUM)='relative_humidity_at_2m                  '
      vname(6,indxRHUM)='lat_rho lon_rho                          '
      vname(7,indxRHUM)='                                         '
      vname(1,indxRADLW)='radlw                                   '
      vname(2,indxRADLW)='net terrestrial longwave radiation      '
      vname(3,indxRADLW)='Watts meter-2                           '
      vname(4,indxRADLW)='terrestrial longwave, scalar, series    '
      vname(5,indxRADLW)='surface_net_downward_longwave_flux      '
      vname(6,indxRADLW)='                                        '
      vname(7,indxRADLW)='                                        '
      vname(1,indxPRATE)='prate                                   '
      vname(2,indxPRATE)='surface precipitation rate              '
      vname(3,indxPRATE)='Kg meter-2 second-1                     '
      vname(4,indxPRATE)='precipitation rate, scalar, series      '
      vname(5,indxPRATE)='                                        '
      vname(6,indxPRATE)='lat_rho lon_rho                         '
      vname(7,indxPRATE)='                                        '
      vname(1,indxUWND)='uwnd                                     '
      vname(2,indxUWND)='surface u-wind speed 10 m                '
      vname(3,indxUWND)='meter second-1                           '
      vname(4,indxUWND)='surface wind speed, scalar, series       '
      vname(5,indxUWND)='x_wind                                   '
      vname(6,indxUWND)='lat_u lon_u                              '
      vname(7,indxUWND)='                                         '
      vname(1,indxVWND)='vwnd                                     '
      vname(2,indxVWND)='surface v-wind speed 10 m                '
      vname(3,indxVWND)='meter second-1                           '
      vname(4,indxVWND)='surface wind speed, scalar, series       '
      vname(5,indxVWND)='y_wind                                   '
      vname(6,indxVWND)='lat_v lon_v                              '
      vname(7,indxVWND)='                                         '
      do itrc=1,NT
         got_tclm(itrc)=.false.
      enddo
      return
       
      

      end subroutine Sub_Loop_init_scalars

      module climat_udat1


        implicit none
        public :: Alloc_agrif_climat_udat1
      contains
      subroutine Alloc_agrif_climat_udat1(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : uclm_time
          if (.not. allocated(Agrif_Gr % tabvars(123)% array1)) then
          allocate(Agrif_Gr % tabvars(123)% array1(1 : 2))
          Agrif_Gr % tabvars(123)% array1 = 0
      endif
      end subroutine Alloc_agrif_climat_udat1
      end module climat_udat1
      module climat_vclima


        implicit none
        public :: Alloc_agrif_climat_vclima
      contains
      subroutine Alloc_agrif_climat_vclima(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : vclima
          if (.not. allocated(Agrif_Gr % tabvars(124)% array4)) then
          allocate(Agrif_Gr % tabvars(124)% array4(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 2))
          Agrif_Gr % tabvars(124)% array4 = 0
      endif
      end subroutine Alloc_agrif_climat_vclima
      end module climat_vclima
      module climat_uclima


        implicit none
        public :: Alloc_agrif_climat_uclima
      contains
      subroutine Alloc_agrif_climat_uclima(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : uclima
          if (.not. allocated(Agrif_Gr % tabvars(125)% array4)) then
          allocate(Agrif_Gr % tabvars(125)% array4(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 2))
          Agrif_Gr % tabvars(125)% array4 = 0
      endif
      end subroutine Alloc_agrif_climat_uclima
      end module climat_uclima
      module climat_M3nudgcof


        implicit none
        public :: Alloc_agrif_climat_M3nudgcof
      contains
      subroutine Alloc_agrif_climat_M3nudgcof(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : M3nudgcof
          if (.not. allocated(Agrif_Gr % tabvars(126)% array2)) then
          allocate(Agrif_Gr % tabvars(126)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(126)% array2 = 0
      endif
      end subroutine Alloc_agrif_climat_M3nudgcof
      end module climat_M3nudgcof
      module climat_vbclima


        implicit none
        public :: Alloc_agrif_climat_vbclima
      contains
      subroutine Alloc_agrif_climat_vbclima(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : vbclima
          if (.not. allocated(Agrif_Gr % tabvars(127)% array3)) then
          allocate(Agrif_Gr % tabvars(127)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(127)% array3 = 0
      endif
      end subroutine Alloc_agrif_climat_vbclima
      end module climat_vbclima
      module climat_ubclima


        implicit none
        public :: Alloc_agrif_climat_ubclima
      contains
      subroutine Alloc_agrif_climat_ubclima(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : ubclima
          if (.not. allocated(Agrif_Gr % tabvars(128)% array3)) then
          allocate(Agrif_Gr % tabvars(128)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(128)% array3 = 0
      endif
      end subroutine Alloc_agrif_climat_ubclima
      end module climat_ubclima
      module climat_M2nudgcof


        implicit none
        public :: Alloc_agrif_climat_M2nudgcof
      contains
      subroutine Alloc_agrif_climat_M2nudgcof(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : M2nudgcof
          if (.not. allocated(Agrif_Gr % tabvars(129)% array2)) then
          allocate(Agrif_Gr % tabvars(129)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(129)% array2 = 0
      endif
      end subroutine Alloc_agrif_climat_M2nudgcof
      end module climat_M2nudgcof
      module climat_vclm


        implicit none
        public :: Alloc_agrif_climat_vclm
      contains
      subroutine Alloc_agrif_climat_vclm(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : vclm
          if (.not. allocated(Agrif_Gr % tabvars(130)% array3)) then
          allocate(Agrif_Gr % tabvars(130)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(130)% array3 = 0
      endif
      end subroutine Alloc_agrif_climat_vclm
      end module climat_vclm
      module climat_uclm


        implicit none
        public :: Alloc_agrif_climat_uclm
      contains
      subroutine Alloc_agrif_climat_uclm(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : uclm
          if (.not. allocated(Agrif_Gr % tabvars(131)% array3)) then
          allocate(Agrif_Gr % tabvars(131)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(131)% array3 = 0
      endif
      end subroutine Alloc_agrif_climat_uclm
      end module climat_uclm
      module climat_vbclm


        implicit none
        public :: Alloc_agrif_climat_vbclm
      contains
      subroutine Alloc_agrif_climat_vbclm(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : vbclm
          if (.not. allocated(Agrif_Gr % tabvars(132)% array2)) then
          allocate(Agrif_Gr % tabvars(132)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(132)% array2 = 0
      endif
      end subroutine Alloc_agrif_climat_vbclm
      end module climat_vbclm
      module climat_ubclm


        implicit none
        public :: Alloc_agrif_climat_ubclm
      contains
      subroutine Alloc_agrif_climat_ubclm(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : ubclm
          if (.not. allocated(Agrif_Gr % tabvars(133)% array2)) then
          allocate(Agrif_Gr % tabvars(133)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(133)% array2 = 0
      endif
      end subroutine Alloc_agrif_climat_ubclm
      end module climat_ubclm
      module climat_tdat


        implicit none
        public :: Alloc_agrif_climat_tdat
      contains
      subroutine Alloc_agrif_climat_tdat(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      !! ALLOCATION OF VARIABLE : got_tclm
          if (.not. allocated(Agrif_Gr % tabvars_l(12)% larray1)) then
          allocate(Agrif_Gr % tabvars_l(12)% larray1(1 : NT))
      endif
      !! ALLOCATION OF VARIABLE : tclm_id
      do i = 210,214
          if (.not. allocated(Agrif_Gr % tabvars_i(i)% iarray1)) then
          allocate(Agrif_Gr % tabvars_i(i)% iarray1(1 : NT))
          Agrif_Gr % tabvars_i(i)% iarray1 = 0
      endif
      enddo
      !! ALLOCATION OF VARIABLE : tclm_cycle
          if (.not. allocated(Agrif_Gr % tabvars(134)% array1)) then
          allocate(Agrif_Gr % tabvars(134)% array1(1 : NT))
          Agrif_Gr % tabvars(134)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : tclm_time
          if (.not. allocated(Agrif_Gr % tabvars(135)% array2)) then
          allocate(Agrif_Gr % tabvars(135)% array2(1 : 2,1 : NT))
          Agrif_Gr % tabvars(135)% array2 = 0
      endif
      end subroutine Alloc_agrif_climat_tdat
      end module climat_tdat
      module climat_tclima


        implicit none
        public :: Alloc_agrif_climat_tclima
      contains
      subroutine Alloc_agrif_climat_tclima(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : tclima
          if (.not. allocated(Agrif_Gr % tabvars(136)% array5)) then
          allocate(Agrif_Gr % tabvars(136)% array5(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : 2,1 : NT)
     &)
          Agrif_Gr % tabvars(136)% array5 = 0
      endif
      end subroutine Alloc_agrif_climat_tclima
      end module climat_tclima
      module climat_Tnudgcof


        implicit none
        public :: Alloc_agrif_climat_Tnudgcof
      contains
      subroutine Alloc_agrif_climat_Tnudgcof(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : Tnudgcof
          if (.not. allocated(Agrif_Gr % tabvars(137)% array4)) then
          allocate(Agrif_Gr % tabvars(137)% array4(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : NT))
          Agrif_Gr % tabvars(137)% array4 = 0
      endif
      end subroutine Alloc_agrif_climat_Tnudgcof
      end module climat_Tnudgcof
      module climat_tclm


        implicit none
        public :: Alloc_agrif_climat_tclm
      contains
      subroutine Alloc_agrif_climat_tclm(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : tclm
          if (.not. allocated(Agrif_Gr % tabvars(138)% array4)) then
          allocate(Agrif_Gr % tabvars(138)% array4(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : NT))
          Agrif_Gr % tabvars(138)% array4 = 0
      endif
      end subroutine Alloc_agrif_climat_tclm
      end module climat_tclm
      module climat_zdat1


        implicit none
        public :: Alloc_agrif_climat_zdat1
      contains
      subroutine Alloc_agrif_climat_zdat1(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : ssh_time
          if (.not. allocated(Agrif_Gr % tabvars(139)% array1)) then
          allocate(Agrif_Gr % tabvars(139)% array1(1 : 2))
          Agrif_Gr % tabvars(139)% array1 = 0
      endif
      end subroutine Alloc_agrif_climat_zdat1
      end module climat_zdat1
      module climat_sshg


        implicit none
        public :: Alloc_agrif_climat_sshg
      contains
      subroutine Alloc_agrif_climat_sshg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : sshg
          if (.not. allocated(Agrif_Gr % tabvars(140)% array3)) then
          allocate(Agrif_Gr % tabvars(140)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(140)% array3 = 0
      endif
      end subroutine Alloc_agrif_climat_sshg
      end module climat_sshg
      module climat_Znudgcof


        implicit none
        public :: Alloc_agrif_climat_Znudgcof
      contains
      subroutine Alloc_agrif_climat_Znudgcof(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : Znudgcof
          if (.not. allocated(Agrif_Gr % tabvars(141)% array2)) then
          allocate(Agrif_Gr % tabvars(141)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(141)% array2 = 0
      endif
      end subroutine Alloc_agrif_climat_Znudgcof
      end module climat_Znudgcof
      module climat_ssh


        implicit none
        public :: Alloc_agrif_climat_ssh
      contains
      subroutine Alloc_agrif_climat_ssh(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : ssh
          if (.not. allocated(Agrif_Gr % tabvars(142)% array2)) then
          allocate(Agrif_Gr % tabvars(142)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(142)% array2 = 0
      endif
      end subroutine Alloc_agrif_climat_ssh
      end module climat_ssh
      module bry_indices_array


        implicit none
        public :: Alloc_agrif_bry_indices_array
      contains
      subroutine Alloc_agrif_bry_indices_array(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : bry_time
          if (.not. allocated(Agrif_Gr % tabvars(143)% array1)) then
          allocate(Agrif_Gr % tabvars(143)% array1(1 : 2))
          Agrif_Gr % tabvars(143)% array1 = 0
      endif
      end subroutine Alloc_agrif_bry_indices_array
      end module bry_indices_array
      module lmd_kpp_ustar


        implicit none
        public :: Alloc_agrif_lmd_kpp_ustar
      contains
      subroutine Alloc_agrif_lmd_kpp_ustar(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : ustar
          if (.not. allocated(Agrif_Gr % tabvars(144)% array2)) then
          allocate(Agrif_Gr % tabvars(144)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(144)% array2 = 0
      endif
      end subroutine Alloc_agrif_lmd_kpp_ustar
      end module lmd_kpp_ustar
      module lmd_kpp_ghats


        implicit none
        public :: Alloc_agrif_lmd_kpp_ghats
      contains
      subroutine Alloc_agrif_lmd_kpp_ghats(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : ghats
          if (.not. allocated(Agrif_Gr % tabvars(145)% array3)) then
          allocate(Agrif_Gr % tabvars(145)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,0 : N))
          Agrif_Gr % tabvars(145)% array3 = 0
      endif
      end subroutine Alloc_agrif_lmd_kpp_ghats
      end module lmd_kpp_ghats
      module lmd_kpp_hbl


        implicit none
        public :: Alloc_agrif_lmd_kpp_hbl
      contains
      subroutine Alloc_agrif_lmd_kpp_hbl(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : hbls
          if (.not. allocated(Agrif_Gr % tabvars(146)% array3)) then
          allocate(Agrif_Gr % tabvars(146)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(146)% array3 = 0
      endif
      end subroutine Alloc_agrif_lmd_kpp_hbl
      end module lmd_kpp_hbl
      module lmd_kpp_kbbl


        implicit none
        public :: Alloc_agrif_lmd_kpp_kbbl
      contains
      subroutine Alloc_agrif_lmd_kpp_kbbl(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : kbbl
          if (.not. allocated(Agrif_Gr % tabvars_i(226)% iarray2)) then
          allocate(Agrif_Gr % tabvars_i(226)% iarray2(-1 :  Agrif_tabvar
     &s_i(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabva
     &rs_i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars_i(226)% iarray2 = 0
      endif
      end subroutine Alloc_agrif_lmd_kpp_kbbl
      end module lmd_kpp_kbbl
      module lmd_kpp_hbbl


        implicit none
        public :: Alloc_agrif_lmd_kpp_hbbl
      contains
      subroutine Alloc_agrif_lmd_kpp_hbbl(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : hbbl
          if (.not. allocated(Agrif_Gr % tabvars(147)% array2)) then
          allocate(Agrif_Gr % tabvars(147)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(147)% array2 = 0
      endif
      end subroutine Alloc_agrif_lmd_kpp_hbbl
      end module lmd_kpp_hbbl
      module lmd_kpp_kbl


        implicit none
        public :: Alloc_agrif_lmd_kpp_kbl
      contains
      subroutine Alloc_agrif_lmd_kpp_kbl(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : kbl
          if (.not. allocated(Agrif_Gr % tabvars_i(227)% iarray2)) then
          allocate(Agrif_Gr % tabvars_i(227)% iarray2(-1 :  Agrif_tabvar
     &s_i(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabva
     &rs_i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars_i(227)% iarray2 = 0
      endif
      end subroutine Alloc_agrif_lmd_kpp_kbl
      end module lmd_kpp_kbl
      module mixing_bvf


        implicit none
        public :: Alloc_agrif_mixing_bvf
      contains
      subroutine Alloc_agrif_mixing_bvf(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : bvf
          if (.not. allocated(Agrif_Gr % tabvars(148)% array3)) then
          allocate(Agrif_Gr % tabvars(148)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,0 : N))
          Agrif_Gr % tabvars(148)% array3 = 0
      endif
      end subroutine Alloc_agrif_mixing_bvf
      end module mixing_bvf
      module mixing_Akt


        implicit none
        public :: Alloc_agrif_mixing_Akt
      contains
      subroutine Alloc_agrif_mixing_Akt(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : Akt
          if (.not. allocated(Agrif_Gr % tabvars(149)% array4)) then
          allocate(Agrif_Gr % tabvars(149)% array4(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,0 : N,1 : 2))
          Agrif_Gr % tabvars(149)% array4 = 0
      endif
      end subroutine Alloc_agrif_mixing_Akt
      end module mixing_Akt
      module mixing_Akv


        implicit none
        public :: Alloc_agrif_mixing_Akv
      contains
      subroutine Alloc_agrif_mixing_Akv(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : Akv
          if (.not. allocated(Agrif_Gr % tabvars(150)% array3)) then
          allocate(Agrif_Gr % tabvars(150)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,0 : N))
          Agrif_Gr % tabvars(150)% array3 = 0
      endif
      end subroutine Alloc_agrif_mixing_Akv
      end module mixing_Akv
      module mixing_idRz


        implicit none
        public :: Alloc_agrif_mixing_idRz
      contains
      subroutine Alloc_agrif_mixing_idRz(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : idRz
          if (.not. allocated(Agrif_Gr % tabvars(151)% array3)) then
          allocate(Agrif_Gr % tabvars(151)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,0 : N))
          Agrif_Gr % tabvars(151)% array3 = 0
      endif
      end subroutine Alloc_agrif_mixing_idRz
      end module mixing_idRz
      module mixing_dRde


        implicit none
        public :: Alloc_agrif_mixing_dRde
      contains
      subroutine Alloc_agrif_mixing_dRde(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : dRde
          if (.not. allocated(Agrif_Gr % tabvars(152)% array3)) then
          allocate(Agrif_Gr % tabvars(152)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(152)% array3 = 0
      endif
      end subroutine Alloc_agrif_mixing_dRde
      end module mixing_dRde
      module mixing_dRdx


        implicit none
        public :: Alloc_agrif_mixing_dRdx
      contains
      subroutine Alloc_agrif_mixing_dRdx(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : dRdx
          if (.not. allocated(Agrif_Gr % tabvars(153)% array3)) then
          allocate(Agrif_Gr % tabvars(153)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(153)% array3 = 0
      endif
      end subroutine Alloc_agrif_mixing_dRdx
      end module mixing_dRdx
      module mixing_diff3d_v


        implicit none
        public :: Alloc_agrif_mixing_diff3d_v
      contains
      subroutine Alloc_agrif_mixing_diff3d_v(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : diff3d_v
          if (.not. allocated(Agrif_Gr % tabvars(154)% array3)) then
          allocate(Agrif_Gr % tabvars(154)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(154)% array3 = 0
      endif
      end subroutine Alloc_agrif_mixing_diff3d_v
      end module mixing_diff3d_v
      module mixing_diff3d_u


        implicit none
        public :: Alloc_agrif_mixing_diff3d_u
      contains
      subroutine Alloc_agrif_mixing_diff3d_u(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : diff3d_u
          if (.not. allocated(Agrif_Gr % tabvars(155)% array3)) then
          allocate(Agrif_Gr % tabvars(155)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(155)% array3 = 0
      endif
      end subroutine Alloc_agrif_mixing_diff3d_u
      end module mixing_diff3d_u
      module mixing_diff4


        implicit none
        public :: Alloc_agrif_mixing_diff4
      contains
      subroutine Alloc_agrif_mixing_diff4(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      !! ALLOCATION OF VARIABLE : diff4
          if (.not. allocated(Agrif_Gr % tabvars(156)% array3)) then
          allocate(Agrif_Gr % tabvars(156)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : NT))
          Agrif_Gr % tabvars(156)% array3 = 0
      endif
      end subroutine Alloc_agrif_mixing_diff4
      end module mixing_diff4
      module mixing_diff4_sponge


        implicit none
        public :: Alloc_agrif_mixing_diff4_sponge
      contains
      subroutine Alloc_agrif_mixing_diff4_sponge(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : diff4_sponge
          if (.not. allocated(Agrif_Gr % tabvars(157)% array2)) then
          allocate(Agrif_Gr % tabvars(157)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(157)% array2 = 0
      endif
      end subroutine Alloc_agrif_mixing_diff4_sponge
      end module mixing_diff4_sponge
      module mixing_diff2


        implicit none
        public :: Alloc_agrif_mixing_diff2
      contains
      subroutine Alloc_agrif_mixing_diff2(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      !! ALLOCATION OF VARIABLE : diff2
          if (.not. allocated(Agrif_Gr % tabvars(158)% array3)) then
          allocate(Agrif_Gr % tabvars(158)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : NT))
          Agrif_Gr % tabvars(158)% array3 = 0
      endif
      end subroutine Alloc_agrif_mixing_diff2
      end module mixing_diff2
      module mixing_diff2_sponge


        implicit none
        public :: Alloc_agrif_mixing_diff2_sponge
      contains
      subroutine Alloc_agrif_mixing_diff2_sponge(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : diff2_sponge
          if (.not. allocated(Agrif_Gr % tabvars(159)% array2)) then
          allocate(Agrif_Gr % tabvars(159)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(159)% array2 = 0
      endif
      end subroutine Alloc_agrif_mixing_diff2_sponge
      end module mixing_diff2_sponge
      module mixing_visc2_sponge_p


        implicit none
        public :: Alloc_agrif_mixing_visc2_sponge_p
      contains
      subroutine Alloc_agrif_mixing_visc2_sponge_p(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : visc2_sponge_p
          if (.not. allocated(Agrif_Gr % tabvars(160)% array2)) then
          allocate(Agrif_Gr % tabvars(160)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(160)% array2 = 0
      endif
      end subroutine Alloc_agrif_mixing_visc2_sponge_p
      end module mixing_visc2_sponge_p
      module mixing_visc2_sponge_r


        implicit none
        public :: Alloc_agrif_mixing_visc2_sponge_r
      contains
      subroutine Alloc_agrif_mixing_visc2_sponge_r(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : visc2_sponge_r
          if (.not. allocated(Agrif_Gr % tabvars(161)% array2)) then
          allocate(Agrif_Gr % tabvars(161)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(161)% array2 = 0
      endif
      end subroutine Alloc_agrif_mixing_visc2_sponge_r
      end module mixing_visc2_sponge_r
      module mixing_visc2_p


        implicit none
        public :: Alloc_agrif_mixing_visc2_p
      contains
      subroutine Alloc_agrif_mixing_visc2_p(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : visc2_p
          if (.not. allocated(Agrif_Gr % tabvars(162)% array2)) then
          allocate(Agrif_Gr % tabvars(162)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(162)% array2 = 0
      endif
      end subroutine Alloc_agrif_mixing_visc2_p
      end module mixing_visc2_p
      module mixing_visc2_r


        implicit none
        public :: Alloc_agrif_mixing_visc2_r
      contains
      subroutine Alloc_agrif_mixing_visc2_r(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : visc2_r
          if (.not. allocated(Agrif_Gr % tabvars(163)% array2)) then
          allocate(Agrif_Gr % tabvars(163)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(163)% array2 = 0
      endif
      end subroutine Alloc_agrif_mixing_visc2_r
      end module mixing_visc2_r
