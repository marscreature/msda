












      subroutine uv3dmix_spg (tile)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_uv3dmix_spg(tile,N2d,A2d,Mmmpi,Lmmpi,N3d)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: N2d
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      real, dimension(1:N2d,1:NSA,0:NPP-1) :: A2d
      integer(4) :: tile
        end subroutine Sub_Loop_uv3dmix_spg

      end interface
      integer(4) :: tile
      

        call Sub_Loop_uv3dmix_spg(tile, Agrif_tabvars_i(187)%iarray0, Ag
     &rif_tabvars(95)%array3, Agrif_tabvars_i(194)%iarray0, Agrif_tabvar
     &s_i(195)%iarray0, Agrif_tabvars_i(186)%iarray0)

      end


      subroutine Sub_Loop_uv3dmix_spg(tile,N2d,A2d,Mmmpi,Lmmpi,N3d)

      use Agrif_Util

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: N2d
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: N3d
      real, dimension(1:N2d,1:NSA,0:NPP-1) :: A2d
      integer(4) :: tile

                   

                                              
      integer(4) :: trd
      integer(4) :: omp_get_thread_num
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                    
                                 
      integer(4), dimension(1:N2d,0:NPP-1) :: B2d
                                       
                                                            
      integer(4) :: chunk_size_X
      integer(4) :: margin_X
      integer(4) :: chunk_size_E
      integer(4) :: margin_E
                                             
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: i_X
      integer(4) :: j_E
      chunk_size_X=(Lmmpi+NSUB_X-1)/NSUB_X
      margin_X=(NSUB_X*chunk_size_X-Lmmpi)/2
      chunk_size_E=(Mmmpi+NSUB_E-1)/NSUB_E
      margin_E=(NSUB_E*chunk_size_E-Mmmpi)/2
      j_E=tile/NSUB_X
      i_X=tile-j_E*NSUB_X
      Istr=1+i_X*chunk_size_X-margin_X
      Iend=Istr+chunk_size_X-1
      Istr=max(Istr,1)
      Iend=min(Iend,Lmmpi)
      Jstr=1+j_E*chunk_size_E-margin_E
      Jend=Jstr+chunk_size_E-1
      Jstr=max(Jstr,1)
      Jend=min(Jend,Mmmpi)
      trd=omp_get_thread_num()
      if (AGRIF_Root()) then
      call uv3dmix_spg_tile (Istr,Iend,Jstr,Jend,
     &                        A2d(1,1,trd), A2d(1,2,trd),
     &                        A2d(1,3,trd), A2d(1,4,trd),
     &                        A2d(1, 5,trd), A2d(1, 7,trd),
     &                        A2d(1, 9,trd), A2d(1,11,trd),
     &                        A2d(1,13,trd), A2d(1,15,trd),
     &                        A2d(1,17,trd), A2d(1,19,trd),
     &                        A2d(1,21,trd), A2d(1,23,trd),
     &                        A2d(1,25,trd), A2d(1,27,trd))
      else
      call uv3dmix_spg_child_tile (Istr,Iend,Jstr,Jend,
     &                        A2d(1,1,trd), A2d(1,2,trd),
     &                        A2d(1,3,trd), A2d(1,4,trd),
     &                        A2d(1, 5,trd), A2d(1, 7,trd),
     &                        A2d(1, 9,trd), A2d(1,11,trd),
     &                        A2d(1,13,trd), A2d(1,15,trd),
     &                        A2d(1,17,trd), A2d(1,19,trd),
     &                        A2d(1,21,trd), A2d(1,23,trd),
     &                        A2d(1,25,trd), A2d(1,27,trd))
       endif
      return
       
      

      end subroutine Sub_Loop_uv3dmix_spg

      subroutine uv3dmix_spg_tile (Istr,Iend,Jstr,Jend,
     &                             UFx,UFe,VFx,VFe,
     &                             UFs,VFs, dnUdx,  dmUde,
     &                             dUdz,    dnVdx,   dmVde,  dVdz,
     &                             dZdx_r,  dZdx_p,  dZde_r, dZde_p)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_uv3dmix_spg_tile(Istr,Iend,Jstr,Jend,UFx,UFe
     &,VFx,VFe,UFs,VFs,dnUdx,dmUde,dUdz,dnVdx,dmVde,dVdz,dZdx_r,dZdx_p,d
     &Zde_r,dZde_p,padd_E,Mm,padd_X,Lm,rvfrc,dt,rufrc,on_p,om_p,pmask,pm
     &_u,pnom_p,pn_v,pmon_p,visc2_sponge_p,om_r,on_r,vclm,v,pm_v,pnom_r,
     &uclm,u,pn_u,pmon_r,visc2_sponge_r,Hz,nstp,NORTH_INTER,SOUTH_INTER,
     &EAST_INTER,WEST_INTER,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: dt
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rvfrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rufrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pnom_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmon_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pnom_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmon_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: UFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: VFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmUde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmVde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnUdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnVdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dUdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dVdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_r
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_r
        end subroutine Sub_Loop_uv3dmix_spg_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: UFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: VFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmUde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmVde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnUdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnVdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dUdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dVdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_r
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_r
      

        call Sub_Loop_uv3dmix_spg_tile(Istr,Iend,Jstr,Jend,UFx,UFe,VFx,V
     &Fe,UFs,VFs,dnUdx,dmUde,dUdz,dnVdx,dmVde,dVdz,dZdx_r,dZdx_p,dZde_r,
     &dZde_p, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0
     &, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agri
     &f_tabvars(119)%array2, Agrif_tabvars_r(46)%array0, Agrif_tabvars(1
     &20)%array2, Agrif_tabvars(65)%array2, Agrif_tabvars(66)%array2, Ag
     &rif_tabvars(50)%array2, Agrif_tabvars(62)%array2, Agrif_tabvars(57
     &)%array2, Agrif_tabvars(61)%array2, Agrif_tabvars(58)%array2, Agri
     &f_tabvars(160)%array2, Agrif_tabvars(72)%array2, Agrif_tabvars(71)
     &%array2, Agrif_tabvars(130)%array3, Agrif_tabvars(110)%array4, Agr
     &if_tabvars(63)%array2, Agrif_tabvars(55)%array2, Agrif_tabvars(131
     &)%array3, Agrif_tabvars(111)%array4, Agrif_tabvars(64)%array2, Agr
     &if_tabvars(56)%array2, Agrif_tabvars(161)%array2, Agrif_tabvars(10
     &4)%array3, Agrif_tabvars_i(176)%iarray0, Agrif_tabvars_l(5)%larray
     &0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Agrif_t
     &abvars_l(6)%larray0, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i
     &(195)%iarray0)

      end


      subroutine Sub_Loop_uv3dmix_spg_tile(Istr,Iend,Jstr,Jend,UFx,UFe,V
     &Fx,VFe,UFs,VFs,dnUdx,dmUde,dUdz,dnVdx,dmVde,dVdz,dZdx_r,dZdx_p,dZd
     &e_r,dZde_p,padd_E,Mm,padd_X,Lm,rvfrc,dt,rufrc,on_p,om_p,pmask,pm_u
     &,pnom_p,pn_v,pmon_p,visc2_sponge_p,om_r,on_r,vclm,v,pm_v,pnom_r,uc
     &lm,u,pn_u,pmon_r,visc2_sponge_r,Hz,nstp,NORTH_INTER,SOUTH_INTER,EA
     &ST_INTER,WEST_INTER,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: dt
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rvfrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rufrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pnom_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmon_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pnom_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmon_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: UFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: VFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmUde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmVde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnUdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnVdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dUdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dVdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_r
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_r

                   

                                                        
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: indx
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
     
      real :: cff
      real :: cff1
      real :: cff2
      real :: cff3
      real :: cff4
      real :: cff5
      real :: cff6
      real :: cff7
      real :: cff8
      real :: dmUdz
      real :: dnUdz
      real :: dmVdz
      real :: dnVdz
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      indx=3-nstp
      do k=1,N
        do j=JstrV-1,Jend
          do i=IstrU-1,Iend
            cff=Hz(i,j,k)*visc2_sponge_r(i,j)*
     &    ( pmon_r(i,j)*
     &        ( pn_u(i+1,j)*(u(i+1,j,k,nstp)-uclm(i+1,j,k))
     &         -pn_u(i  ,j)*(u(i  ,j,k,nstp)-uclm(i  ,j,k)) )
     &     -pnom_r(i,j)*
     &        ( pm_v(i,j+1)*(v(i,j+1,k,nstp)-vclm(i,j+1,k))
     &         -pm_v(i,j  )*(v(i,j  ,k,nstp)-vclm(i,j  ,k)) ) )
            UFx(i,j)=on_r(i,j)*on_r(i,j)*cff
            VFe(i,j)=om_r(i,j)*om_r(i,j)*cff
          enddo
        enddo
        do j=Jstr,Jend+1
          do i=Istr,Iend+1
            cff=0.25D0*visc2_sponge_p(i,j)*
     &              (Hz(i-1,j,k)+Hz(i,j,k)+Hz(i-1,j-1,k)+Hz(i,j-1,k))*
     &      (  pmon_p(i,j)
     &              *( pn_v(i  ,j)*(v(i  ,j,k,nstp)-vclm(i  ,j,k))
     &                -pn_v(i-1,j)*(v(i-1,j,k,nstp)-vclm(i-1,j,k)) )
     &        +pnom_p(i,j)
     &              *( pm_u(i,j  )*(u(i,j  ,k,nstp)-uclm(i,j  ,k))
     &                -pm_u(i,j-1)*(u(i,j-1,k,nstp)-uclm(i,j-1,k)) ) )
     &                                               * pmask(i,j)
            UFe(i,j)=om_p(i,j)*om_p(i,j)*cff
            VFx(i,j)=on_p(i,j)*on_p(i,j)*cff
          enddo
        enddo
        do j=Jstr,Jend
          do i=IstrU,Iend
            cff=pn_u(i,j)*(UFx(i,j)-UFx(i-1,j))
     &         +pm_u(i,j)*(UFe(i,j+1)-UFe(i,j))
            cff1=pm_u(i,j)*pn_u(i,j)*cff
            rufrc(i,j)=rufrc(i,j) + cff
            u(i,j,k,indx)=u(i,j,k,indx) + dt*cff1
          enddo
        enddo
        do j=JstrV,Jend
          do i=Istr,Iend
            cff=pn_v(i,j)*(VFx(i+1,j)-VFx(i,j))
     &         -pm_v(i,j)*(VFe(i,j)-VFe(i,j-1))
            cff1=pm_v(i,j)*pn_v(i,j)*cff
            rvfrc(i,j)=rvfrc(i,j) + cff
            v(i,j,k,indx)=v(i,j,k,indx) + dt*cff1
          enddo
        enddo
      enddo
      return
       
      

      end subroutine Sub_Loop_uv3dmix_spg_tile

      subroutine uv3dmix_spg_child_tile(Istr,Iend,Jstr,Jend,
     &                             UFx,UFe,VFx,VFe,
     &                             UFs,VFs, dnUdx,  dmUde,
     &                             dUdz,    dnVdx,   dmVde,  dVdz,
     &                             dZdx_r,  dZdx_p,  dZde_r, dZde_p)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_uv3dmix_spg_child_tile(Istr,Iend,Jstr,Jend,U
     &Fx,UFe,VFx,VFe,UFs,VFs,dnUdx,dmUde,dUdz,dnVdx,dmVde,dVdz,dZdx_r,dZ
     &dx_p,dZde_r,dZde_p,padd_E,Mm,padd_X,Lm,rvfrc,dt,rufrc,on_p,om_p,pm
     &ask,pm_u,pnom_p,pn_v,pmon_p,visc2_sponge_p,om_r,on_r,vsponge,v,pm_
     &v,pnom_r,usponge,u,pn_u,pmon_r,visc2_sponge_r,Hz,nstp,NORTH_INTER,
     &SOUTH_INTER,EAST_INTER,WEST_INTER,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: dt
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rvfrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rufrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pnom_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmon_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vsponge
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pnom_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: usponge
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmon_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: UFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: VFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmUde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmVde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnUdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnVdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dUdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dVdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_r
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_r
        end subroutine Sub_Loop_uv3dmix_spg_child_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: UFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: VFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmUde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmVde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnUdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnVdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dUdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dVdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_r
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_r
      

        call Sub_Loop_uv3dmix_spg_child_tile(Istr,Iend,Jstr,Jend,UFx,UFe
     &,VFx,VFe,UFs,VFs,dnUdx,dmUde,dUdz,dnVdx,dmVde,dVdz,dZdx_r,dZdx_p,d
     &Zde_r,dZde_p, Agrif_tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%i
     &array0, Agrif_tabvars_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0
     &, Agrif_tabvars(119)%array2, Agrif_tabvars_r(46)%array0, Agrif_tab
     &vars(120)%array2, Agrif_tabvars(65)%array2, Agrif_tabvars(66)%arra
     &y2, Agrif_tabvars(50)%array2, Agrif_tabvars(62)%array2, Agrif_tabv
     &ars(57)%array2, Agrif_tabvars(61)%array2, Agrif_tabvars(58)%array2
     &, Agrif_tabvars(160)%array2, Agrif_tabvars(72)%array2, Agrif_tabva
     &rs(71)%array2, Agrif_tabvars(24)%array3, Agrif_tabvars(110)%array4
     &, Agrif_tabvars(63)%array2, Agrif_tabvars(55)%array2, Agrif_tabvar
     &s(25)%array3, Agrif_tabvars(111)%array4, Agrif_tabvars(64)%array2,
     & Agrif_tabvars(56)%array2, Agrif_tabvars(161)%array2, Agrif_tabvar
     &s(104)%array3, Agrif_tabvars_i(176)%iarray0, Agrif_tabvars_l(5)%la
     &rray0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Agr
     &if_tabvars_l(6)%larray0, Agrif_tabvars_i(194)%iarray0, Agrif_tabva
     &rs_i(195)%iarray0)

      end


      subroutine Sub_Loop_uv3dmix_spg_child_tile(Istr,Iend,Jstr,Jend,UFx
     &,UFe,VFx,VFe,UFs,VFs,dnUdx,dmUde,dUdz,dnVdx,dmVde,dVdz,dZdx_r,dZdx
     &_p,dZde_r,dZde_p,padd_E,Mm,padd_X,Lm,rvfrc,dt,rufrc,on_p,om_p,pmas
     &k,pm_u,pnom_p,pn_v,pmon_p,visc2_sponge_p,om_r,on_r,vsponge,v,pm_v,
     &pnom_r,usponge,u,pn_u,pmon_r,visc2_sponge_r,Hz,nstp,NORTH_INTER,SO
     &UTH_INTER,EAST_INTER,WEST_INTER,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: dt
      integer(4) :: nstp
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rvfrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rufrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pnom_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmon_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: om_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: on_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vsponge
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pm_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pnom_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: usponge
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pn_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmon_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: Hz
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: UFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFe
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: VFx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: UFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: VFs
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmUde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dmVde
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnUdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dnVdx
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dUdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dVdz
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZde_r
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_p
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2,1:2) :: dZdx_r

                   

                                                        
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: k1
      integer(4) :: k2
      integer(4) :: indx
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
     
      real :: cff
      real :: cff1
      real :: cff2
      real :: cff3
      real :: cff4
      real :: cff5
      real :: cff6
      real :: cff7
      real :: cff8
      real :: dmUdz
      real :: dnUdz
      real :: dmVdz
      real :: dnVdz
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                             
                              
                                          
                              
                                          
                              
                                                       
                              
                                                      
                              
                                                      
                              
                                              
                               
                                           
                               
                                           
                               
                                                        
                               
                                                       
                               
                                                       
                               
                                              
                                                        
                                            
                                                   
                                            
                                                   
                           
                                  
                           
                                  
                           
                                  
                                        
                               
                                                           
                                        
                          
                                                       
                                             
                         
                                   
                                                    
                                                    
                                
                                                    
                                 
                                                    
                                                   
                                                   
                                                  
                                                               
                                           
                                                     
                                                     
                                                        
                                                   
                                                       
                                                       
                                           
                                                            
                                  
                                                            
                               
                                                            
                               
                                                     
                                            
                                                  
                                            
                                                  
                                            
                                                                
                                            
                                                             
                                            
                                                             
                                            
                                                      
                                             
                                                   
                                             
                                                   
                                             
                                                                 
                                             
                                                              
                                             
                                                              
                                             
                                          
                                                           
                                            
                                             
                                     
                                                    
                                                      
                                                                       
                                                                 
                                           
                                                    
                                                      
                                               
                                                            
                                           
                                
                                
                             
                                                                       
                                                                 
             
                                                      
                        
                                                
                                                                       
                                                                 
                                    
                                                         
                                               
                                                
                                           
                                           
                                                                       
                                                                 
                                                                 
                                                                 
                         
!$AGRIF_DO_NOT_TREAT
      integer*4 :: iind
      integer*4 :: sortedint(0:10000)
      integer*4 :: whichstep(0:10000)
      integer*4 :: grids_at_level(0:20,0:100)
      integer*4 :: parent_grid(0:20)
      integer*4 :: coeff_ref_time(0:20)
      integer*4 :: nbtimes, nbmaxtimes
      common/rootintegrate/nbtimes, nbmaxtimes,
     &    iind,sortedint,whichstep,
     &    grids_at_level,parent_grid,coeff_ref_time
!$AGRIF_END_DO_NOT_TREAT
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      indx=3-nstp
      do k=1,N
        do j=JstrV-1,Jend
          do i=IstrU-1,Iend
            cff=Hz(i,j,k)*visc2_sponge_r(i,j)*
     &    ( pmon_r(i,j)*
     &        ( pn_u(i+1,j)*(u(i+1,j,k,nstp)-usponge(i+1,j,k))
     &         -pn_u(i  ,j)*(u(i  ,j,k,nstp)-usponge(i  ,j,k)) )
     &     -pnom_r(i,j)*
     &        ( pm_v(i,j+1)*(v(i,j+1,k,nstp)-vsponge(i,j+1,k))
     &         -pm_v(i,j  )*(v(i,j  ,k,nstp)-vsponge(i,j  ,k)) ) )
            UFx(i,j)=on_r(i,j)*on_r(i,j)*cff
            VFe(i,j)=om_r(i,j)*om_r(i,j)*cff
          enddo
        enddo
        do j=Jstr,Jend+1
          do i=Istr,Iend+1
            cff=0.25D0*visc2_sponge_p(i,j)*
     &              (Hz(i-1,j,k)+Hz(i,j,k)+Hz(i-1,j-1,k)+Hz(i,j-1,k))*
     &      (  pmon_p(i,j)
     &              *( pn_v(i  ,j)*(v(i  ,j,k,nstp)-vsponge(i  ,j,k))
     &                -pn_v(i-1,j)*(v(i-1,j,k,nstp)-vsponge(i-1,j,k)) )
     &        +pnom_p(i,j)
     &              *( pm_u(i,j  )*(u(i,j  ,k,nstp)-usponge(i,j  ,k))
     &                -pm_u(i,j-1)*(u(i,j-1,k,nstp)-usponge(i,j-1,k)) ) 
     &                                                                 )
     &                                               * pmask(i,j)
            UFe(i,j)=om_p(i,j)*om_p(i,j)*cff
            VFx(i,j)=on_p(i,j)*on_p(i,j)*cff
          enddo
        enddo
        do j=Jstr,Jend
          do i=IstrU,Iend
            cff=pn_u(i,j)*(UFx(i,j)-UFx(i-1,j))
     &         +pm_u(i,j)*(UFe(i,j+1)-UFe(i,j))
            cff1=pm_u(i,j)*pn_u(i,j)*cff
            rufrc(i,j)=rufrc(i,j) + cff
            u(i,j,k,indx)=u(i,j,k,indx) + dt*cff1
          enddo
        enddo
        do j=JstrV,Jend
          do i=Istr,Iend
            cff=pn_v(i,j)*(VFx(i+1,j)-VFx(i,j))
     &         -pm_v(i,j)*(VFe(i,j)-VFe(i,j-1))
            cff1=pm_v(i,j)*pn_v(i,j)*cff
            rvfrc(i,j)=rvfrc(i,j) + cff
            v(i,j,k,indx)=v(i,j,k,indx) + dt*cff1
          enddo
        enddo
      enddo
      return
       
      

      end subroutine Sub_Loop_uv3dmix_spg_child_tile

