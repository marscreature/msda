












      subroutine MPI_Setup (ierr)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_MPI_Setup(ierr,jmaxmpi,jminmpi,imaxmpi,iminm
     &pi,Mmmpi,Lmmpi,MMm,LLm,p_SE,p_NE,p_SW,p_NW,p_N,p_S,p_E,p_W,NORTH_I
     &NTER,SOUTH_INTER,EAST_INTER,WEST_INTER,jj,ii,mynode)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4) :: jmaxmpi
      integer(4) :: jminmpi
      integer(4) :: imaxmpi
      integer(4) :: iminmpi
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: MMm
      integer(4) :: LLm
      integer(4) :: p_SE
      integer(4) :: p_NE
      integer(4) :: p_SW
      integer(4) :: p_NW
      integer(4) :: p_N
      integer(4) :: p_S
      integer(4) :: p_E
      integer(4) :: p_W
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: jj
      integer(4) :: ii
      integer(4) :: mynode
      integer(4) :: ierr
        end subroutine Sub_Loop_MPI_Setup

      end interface
      integer(4) :: ierr
      

        call Sub_Loop_MPI_Setup(ierr, Agrif_tabvars_i(190)%iarray0, Agri
     &f_tabvars_i(191)%iarray0, Agrif_tabvars_i(192)%iarray0, Agrif_tabv
     &ars_i(193)%iarray0, Agrif_tabvars_i(194)%iarray0, Agrif_tabvars_i(
     &195)%iarray0, Agrif_tabvars_i(198)%iarray0, Agrif_tabvars_i(201)%i
     &array0, Agrif_tabvars_i(148)%iarray0, Agrif_tabvars_i(146)%iarray0
     &, Agrif_tabvars_i(149)%iarray0, Agrif_tabvars_i(147)%iarray0, Agri
     &f_tabvars_i(150)%iarray0, Agrif_tabvars_i(151)%iarray0, Agrif_tabv
     &ars_i(152)%iarray0, Agrif_tabvars_i(153)%iarray0, Agrif_tabvars_l(
     &5)%larray0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0
     &, Agrif_tabvars_l(6)%larray0, Agrif_tabvars_i(154)%iarray0, Agrif_
     &tabvars_i(155)%iarray0, Agrif_tabvars_i(156)%iarray0)

      end


      subroutine Sub_Loop_MPI_Setup(ierr,jmaxmpi,jminmpi,imaxmpi,iminmpi
     &,Mmmpi,Lmmpi,MMm,LLm,p_SE,p_NE,p_SW,p_NW,p_N,p_S,p_E,p_W,NORTH_INT
     &ER,SOUTH_INTER,EAST_INTER,WEST_INTER,jj,ii,mynode)

      use Agrif_Types, only : Agrif_tabvars


      include 'mpif.h'
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      integer(4), parameter :: filetype_his = 1
      integer(4), parameter :: filetype_avg = 2
      integer(4), parameter :: filetype_dia = 3
      integer(4), parameter :: filetype_dia_avg = 4
      integer(4), parameter :: filetype_diaM = 5
      integer(4), parameter :: filetype_diaM_avg = 6
      integer(4), parameter :: filetype_diabio = 7
      integer(4), parameter :: filetype_diabio_avg = 8
      integer(4), parameter :: indxTime = 1
      integer(4), parameter :: indxZ = 2
      integer(4), parameter :: indxUb = 3
      integer(4), parameter :: indxVb = 4
      integer(4), parameter :: indxU = 6
      integer(4), parameter :: indxV = 7
      integer(4), parameter :: indxT = 8
      integer(4), parameter :: indxS = indxT+1
      integer(4), parameter :: indxBSD = indxT+ntrc_salt+ntrc_pas+ntrc_b
     &io+1
      integer(4), parameter :: indxBSS = 101
      integer(4), parameter :: indxO = indxT+ntrc_salt+ntrc_pas+ntrc_bio
     &+ntrc_sed+ntrc_diats+ntrc_diauv+ntrc_diabio+1
      integer(4), parameter :: indxW = indxO+1
      integer(4), parameter :: indxR = indxO+2
      integer(4), parameter :: indxVisc = indxO+3
      integer(4), parameter :: indxDiff = indxO+4
      integer(4), parameter :: indxAkv = indxO+5
      integer(4), parameter :: indxAkt = indxO+6
      integer(4), parameter :: indxAks = indxAkt+4
      integer(4), parameter :: indxHbl = indxAkt+5
      integer(4), parameter :: indxHbbl = indxAkt+6
      integer(4), parameter :: indxSSH = indxAkt+12
      integer(4), parameter :: indxSUSTR = indxSSH+1
      integer(4), parameter :: indxSVSTR = indxSSH+2
      integer(4), parameter :: indxTime2 = indxSSH+3
      integer(4), parameter :: indxShflx = indxSSH+4
      integer(4), parameter :: indxSwflx = indxShflx+1
      integer(4), parameter :: indxShflx_rsw = indxShflx+2
      integer(4), parameter :: indxSST = indxShflx_rsw+1
      integer(4), parameter :: indxdQdSST = indxShflx_rsw+2
      integer(4), parameter :: indxWSPD = indxSST+3
      integer(4), parameter :: indxTAIR = indxSST+4
      integer(4), parameter :: indxRHUM = indxSST+5
      integer(4), parameter :: indxRADLW = indxSST+6
      integer(4), parameter :: indxRADSW = indxSST+7
      integer(4), parameter :: indxPRATE = indxSST+8
      integer(4), parameter :: indxUWND = indxSST+9
      integer(4), parameter :: indxVWND = indxSST+10
      integer(4), parameter :: indxShflx_rlw = indxSST+12
      integer(4), parameter :: indxShflx_lat = indxSST+13
      integer(4), parameter :: indxShflx_sen = indxSST+14
      integer(4), parameter :: indxWstr = indxSUSTR+21
      integer(4), parameter :: indxUWstr = indxSUSTR+22
      integer(4), parameter :: indxVWstr = indxSUSTR+23
      integer(4), parameter :: indxBostr = indxSUSTR+24
      integer(4), parameter :: indxWWA = indxSUSTR+32
      integer(4), parameter :: indxWWD = indxWWA+1
      integer(4), parameter :: indxWWP = indxWWA+2
      integer(4), parameter :: r2dvar = 0
      integer(4), parameter :: u2dvar = 1
      integer(4), parameter :: v2dvar = 2
      integer(4), parameter :: p2dvar = 3
      integer(4), parameter :: r3dvar = 4
      integer(4), parameter :: u3dvar = 5
      integer(4), parameter :: v3dvar = 6
      integer(4), parameter :: p3dvar = 7
      integer(4), parameter :: w3dvar = 8
      integer(4), parameter :: b3dvar = 12
      integer(4) :: jmaxmpi
      integer(4) :: jminmpi
      integer(4) :: imaxmpi
      integer(4) :: iminmpi
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: MMm
      integer(4) :: LLm
      integer(4) :: p_SE
      integer(4) :: p_NE
      integer(4) :: p_SW
      integer(4) :: p_NW
      integer(4) :: p_N
      integer(4) :: p_S
      integer(4) :: p_E
      integer(4) :: p_W
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      integer(4) :: jj
      integer(4) :: ii
      integer(4) :: mynode
      integer(4) :: ierr

                                                    
      integer(4) :: nsize
      integer(4) :: ii_W
      integer(4) :: ii_E
      integer(4) :: jj_S
      integer(4) :: jj_N
                                                            
      integer(4) :: chunk_size_X
      integer(4) :: margin_X
      integer(4) :: chunk_size_E
      integer(4) :: margin_E
                                                         
      integer(4) :: Istrmpi
      integer(4) :: Iendmpi
      integer(4) :: Jstrmpi
      integer(4) :: Jendmpi
      integer(4) :: i_X
      integer(4) :: j_E
                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                                                                       
                                                                 
                                                                 
                                                  
                                                                       
                                                                 
                                                                 
                                                                 
             

                                 
      integer(4) :: iloop
      integer(4) :: indextemp
                                                
                                                         

                                    
                                           

                      
                               

                                 
                                                                       
                                                                 
                    

                                                                       
                                                                 
                                                                 
    
                                                                       
                                                                 
                                                                 
                                                                 
                                                               

                        
                                   

                        
                                   

                         
                                    

                        
                                    

                                     
                                                          

                          
                                     

                                         
                                     

                          
                                                                  

                                    
                                                                     

                                                                       
                                                                 
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                 

                                                          
                                                                       
                                                                 
                                        

                         
                                       

                          
                                        

                          
                                        

                          
                                        

                                                                
      integer(4) :: indxWEB
      integer(4) :: indxWED
      integer(4) :: indxWER
                                                                       
                                                                 
                                                          

                                                                       
                                                                 
                                              
                                                                       
                                                                 
                                                    

                                           
                                                       
                                                                       
                                                                 
                                                                 
              
                                                  
                                                                       
                                                                 
                                                                 
                    
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                      
                         
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
             
                         
                                                                       
                                                                 
 
                                                          
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                        
                                               
                                                                       
                                                                 
                                                                 
                                                                 
                                           
                                   
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
       
!$AGRIF_DO_NOT_TREAT
      INTEGER*4 :: ocean_grid_comm
      common /cpl_comm/ ocean_grid_comm
!$AGRIF_END_DO_NOT_TREAT
                       
      call MPI_Comm_size (MPI_COMM_WORLD, nsize,  ierr)
      call MPI_Comm_rank (MPI_COMM_WORLD, mynode, ierr)
      if (nsize.ne.NNODES) then
        if (mynode.eq.0) write(stdout,'(/1x,A,I4,1x,A,I3,A/)')
     &     'ERROR in MPI_Setup: number of MPI-nodes should be',
     &                         NNODES, 'instead of', nsize, '.'
        ierr=99
        return
      endif
      ii=mod(mynode,NP_XI)
      jj=mynode/NP_XI
      if (NP_XI.eq.1) then
        WEST_INTER=.false.
        EAST_INTER=.false.
      else
        if (ii.eq.0) then
          WEST_INTER=.false.
        else
          WEST_INTER=.true.
        endif
        if (ii.eq.NP_XI-1) then
          EAST_INTER=.false.
        else
          EAST_INTER=.true.
        endif
      endif
      if (NP_ETA.eq.1) then
        SOUTH_INTER=.false.
        NORTH_INTER=.false.
      else
        if (jj.eq.0) then
          SOUTH_INTER=.false.
        else
          SOUTH_INTER=.true.
        endif
        if (jj.eq.NP_ETA-1) then
          NORTH_INTER=.false.
        else
          NORTH_INTER=.true.
        endif
      endif
      ii_W=mod(ii-1+NP_XI,NP_XI)
      ii_E=mod(ii+1       ,NP_XI)
      jj_S=mod(jj-1+NP_ETA,NP_ETA)
      jj_N=mod(jj+1       ,NP_ETA)
      p_W=ii_W +NP_XI*jj
      p_E=ii_E +NP_XI*jj
      p_S=ii   +NP_XI*jj_S
      p_N=ii   +NP_XI*jj_N
      p_NW=ii_W+NP_XI*jj_N
      p_SW=ii_W+NP_XI*jj_S
      p_NE=ii_E+NP_XI*jj_N
      p_SE=ii_E+NP_XI*jj_S
      chunk_size_X=(LLm+NP_XI-1)/NP_XI
      margin_X=(NP_XI*chunk_size_X-LLm)/2
      chunk_size_E=(MMm+NP_ETA-1)/NP_ETA
      margin_E=(NP_ETA*chunk_size_E-MMm)/2
      j_E=mynode/NP_XI
      i_X=mynode-j_E*NP_XI
      istrmpi=1+i_X*chunk_size_X-margin_X
      iendmpi=istrmpi+chunk_size_X-1
      istrmpi=max(istrmpi,1)
      iendmpi=min(iendmpi,LLm)
      jstrmpi=1+j_E*chunk_size_E-margin_E
      jendmpi=jstrmpi+chunk_size_E-1
      jstrmpi=max(jstrmpi,1)
      jendmpi=min(jendmpi,MMm)
      Lmmpi=iendmpi-istrmpi+1
      Mmmpi=jendmpi-jstrmpi+1
      iminmpi=istrmpi
      imaxmpi=iendmpi
      jminmpi=jstrmpi
      jmaxmpi=jendmpi
      return
       
      

      end subroutine Sub_Loop_MPI_Setup

