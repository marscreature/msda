












      subroutine init_arrays (tile)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_init_arrays(tile,Mmmpi,Lmmpi)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: tile
        end subroutine Sub_Loop_init_arrays

      end interface
      integer(4) :: tile
      

        call Sub_Loop_init_arrays(tile, Agrif_tabvars_i(194)%iarray0, Ag
     &rif_tabvars_i(195)%iarray0)

      end


      subroutine Sub_Loop_init_arrays(tile,Mmmpi,Lmmpi)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      integer(4) :: Mmmpi
      integer(4) :: Lmmpi
      integer(4) :: tile

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                          
      integer(4) :: trd
C$    integer*4 omp_get_thread_num
                                                            
      integer(4) :: chunk_size_X
      integer(4) :: margin_X
      integer(4) :: chunk_size_E
      integer(4) :: margin_E
                                             
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      integer(4) :: i_X
      integer(4) :: j_E
      chunk_size_X=(Lmmpi+NSUB_X-1)/NSUB_X
      margin_X=(NSUB_X*chunk_size_X-Lmmpi)/2
      chunk_size_E=(Mmmpi+NSUB_E-1)/NSUB_E
      margin_E=(NSUB_E*chunk_size_E-Mmmpi)/2
      j_E=tile/NSUB_X
      i_X=tile-j_E*NSUB_X
      Istr=1+i_X*chunk_size_X-margin_X
      Iend=Istr+chunk_size_X-1
      Istr=max(Istr,1)
      Iend=min(Iend,Lmmpi)
      Jstr=1+j_E*chunk_size_E-margin_E
      Jend=Jstr+chunk_size_E-1
      Jstr=max(Jstr,1)
      Jend=min(Jend,Mmmpi)
      call init_arrays_tile (Istr,Iend,Jstr,Jend)
      return
       
      

      end subroutine Sub_Loop_init_arrays

      subroutine init_arrays_tile (Istr,Iend,Jstr,Jend)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_init_arrays_tile(Istr,Iend,Jstr,Jend,padd_E,
     &Mm,padd_X,Lm,hbbl_avg,hbbl,hbl_avg,kbl,hbls,ghats,Akt,bvf,Akv,dRde
     &,dRdx,diff3d_v,diff3d_u,diff4_sponge,tnu4,diff4,diff2_sponge,tnu2,
     &diff2,visc2_sponge_p,visc2_sponge_r,visc2_p,visc2,visc2_r,UV_Tmino
     &r,UV_Tmajor,UV_Tphase,UV_Tangle,SSH_Tphase,SSH_Tamp,Tperiod,vclm,u
     &clm,vclima,uclima,vbclima,ubclima,vbclm,ubclm,tclima,tclm,Tnudgcof
     &,sshg,ssh,Znudgcof,srflxg,srflx_avg,srflx,shflx_sen_avg,shflx_lat_
     &avg,shflx_rlw_avg,shflx_rsw_avg,shflx_sen,shflx_lat,shflx_rlw,shfl
     &x_rsw,vwndg,uwndg,wspdg,radswg,radlwg,prateg,rhumg,tairg,vwnd,uwnd
     &,wspd,radsw,radlw,prate,rhum,tair,btflx,stflxg,stflx_avg,stflx,bvs
     &trg,bustrg,svstr_avg,sustr_avg,wstr_avg,bostr_avg,bvstr,bustr,svst
     &rg,sustrg,svstr,sustr,t_avg,t,got_tini,omega_avg,We,w_avg,v_avg,u_
     &avg,rho_avg,rho,v,u,DV_avg2,DU_avg2,DV_avg1,DU_avg1,Zt_avg1,rufrc,
     &vbar_avg,ubar_avg,zeta_avg,vbar,ubar,zeta,NORTH_INTER,Mmmpi,SOUTH_
     &INTER,EAST_INTER,Lmmpi,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: init = 0.D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: visc2
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbl_avg
      integer(4), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: kbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: hbls
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: ghats
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N,1:2) :: Akt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: dRde
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: dRdx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: diff4_sponge
      real, dimension(1:NT) :: tnu4
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: diff4
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: diff2_sponge
      real, dimension(1:NT) :: tnu2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: diff2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tmin
     &or
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tmaj
     &or
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tpha
     &se
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tang
     &le
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: SSH_Tph
     &ase
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: SSH_Tam
     &p
      real, dimension(1:Ntides) :: Tperiod
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: vclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: uclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vbclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: ubclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2,1:NT) :: tcl
     &ima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: Tnudgco
     &f
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: sshg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ssh
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Znudgcof
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: srflxg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vwndg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: uwndg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: wspdg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: radswg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: radlwg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: prateg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: rhumg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: tairg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vwnd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: uwnd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: wspd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: radsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: radlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: prate
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhum
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: tair
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: btflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2,1:NT) :: stflxg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: bvstrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: bustrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: wstr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bostr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: svstrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: sustrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: t_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      logical, dimension(1:NT) :: got_tini
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: omega_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: We
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: w_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: v_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: u_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DV_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DU_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rufrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbar_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubar_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: zeta_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
        end subroutine Sub_Loop_init_arrays_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      

        call Sub_Loop_init_arrays_tile(Istr,Iend,Jstr,Jend, Agrif_tabvar
     &s_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars_i(18
     &9)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(233)%array
     &2, Agrif_tabvars(147)%array2, Agrif_tabvars(234)%array2, Agrif_tab
     &vars_i(227)%iarray2, Agrif_tabvars(146)%array3, Agrif_tabvars(145)
     &%array3, Agrif_tabvars(149)%array4, Agrif_tabvars(148)%array3, Agr
     &if_tabvars(150)%array3, Agrif_tabvars(152)%array3, Agrif_tabvars(1
     &53)%array3, Agrif_tabvars(154)%array3, Agrif_tabvars(155)%array3, 
     &Agrif_tabvars(157)%array2, Agrif_tabvars(88)%array1, Agrif_tabvars
     &(156)%array3, Agrif_tabvars(159)%array2, Agrif_tabvars(89)%array1,
     & Agrif_tabvars(158)%array3, Agrif_tabvars(160)%array2, Agrif_tabva
     &rs(161)%array2, Agrif_tabvars(162)%array2, Agrif_tabvars_r(30)%arr
     &ay0, Agrif_tabvars(163)%array2, Agrif_tabvars(165)%array3, Agrif_t
     &abvars(166)%array3, Agrif_tabvars(164)%array3, Agrif_tabvars(167)%
     &array3, Agrif_tabvars(168)%array3, Agrif_tabvars(169)%array3, Agri
     &f_tabvars(170)%array1, Agrif_tabvars(130)%array3, Agrif_tabvars(13
     &1)%array3, Agrif_tabvars(124)%array4, Agrif_tabvars(125)%array4, A
     &grif_tabvars(127)%array3, Agrif_tabvars(128)%array3, Agrif_tabvars
     &(132)%array2, Agrif_tabvars(133)%array2, Agrif_tabvars(136)%array5
     &, Agrif_tabvars(138)%array4, Agrif_tabvars(137)%array4, Agrif_tabv
     &ars(140)%array3, Agrif_tabvars(142)%array2, Agrif_tabvars(141)%arr
     &ay2, Agrif_tabvars(173)%array3, Agrif_tabvars(242)%array2, Agrif_t
     &abvars(174)%array2, Agrif_tabvars(229)%array2, Agrif_tabvars(230)%
     &array2, Agrif_tabvars(231)%array2, Agrif_tabvars(232)%array2, Agri
     &f_tabvars(206)%array2, Agrif_tabvars(207)%array2, Agrif_tabvars(20
     &8)%array2, Agrif_tabvars(209)%array2, Agrif_tabvars(184)%array3, A
     &grif_tabvars(185)%array3, Agrif_tabvars(186)%array3, Agrif_tabvars
     &(187)%array3, Agrif_tabvars(188)%array3, Agrif_tabvars(189)%array3
     &, Agrif_tabvars(190)%array3, Agrif_tabvars(191)%array3, Agrif_tabv
     &ars(192)%array2, Agrif_tabvars(193)%array2, Agrif_tabvars(194)%arr
     &ay2, Agrif_tabvars(195)%array2, Agrif_tabvars(196)%array2, Agrif_t
     &abvars(197)%array2, Agrif_tabvars(198)%array2, Agrif_tabvars(199)%
     &array2, Agrif_tabvars(200)%array3, Agrif_tabvars(205)%array4, Agri
     &f_tabvars(235)%array3, Agrif_tabvars(210)%array3, Agrif_tabvars(21
     &5)%array3, Agrif_tabvars(216)%array3, Agrif_tabvars(243)%array2, A
     &grif_tabvars(244)%array2, Agrif_tabvars(245)%array2, Agrif_tabvars
     &(246)%array2, Agrif_tabvars(217)%array2, Agrif_tabvars(218)%array2
     &, Agrif_tabvars(222)%array3, Agrif_tabvars(223)%array3, Agrif_tabv
     &ars(224)%array2, Agrif_tabvars(225)%array2, Agrif_tabvars(239)%arr
     &ay4, Agrif_tabvars(109)%array5, Agrif_tabvars_l(10)%larray1, Agrif
     &_tabvars(237)%array3, Agrif_tabvars(102)%array3, Agrif_tabvars(236
     &)%array3, Agrif_tabvars(240)%array3, Agrif_tabvars(241)%array3, Ag
     &rif_tabvars(238)%array3, Agrif_tabvars(100)%array3, Agrif_tabvars(
     &110)%array4, Agrif_tabvars(111)%array4, Agrif_tabvars(112)%array2,
     & Agrif_tabvars(113)%array2, Agrif_tabvars(114)%array3, Agrif_tabva
     &rs(115)%array3, Agrif_tabvars(116)%array2, Agrif_tabvars(120)%arra
     &y2, Agrif_tabvars(247)%array2, Agrif_tabvars(248)%array2, Agrif_ta
     &bvars(249)%array2, Agrif_tabvars(96)%array3, Agrif_tabvars(97)%arr
     &ay3, Agrif_tabvars(98)%array3, Agrif_tabvars_l(5)%larray0, Agrif_t
     &abvars_i(194)%iarray0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l
     &(7)%larray0, Agrif_tabvars_i(195)%iarray0, Agrif_tabvars_l(6)%larr
     &ay0)

      end


      subroutine Sub_Loop_init_arrays_tile(Istr,Iend,Jstr,Jend,padd_E,Mm
     &,padd_X,Lm,hbbl_avg,hbbl,hbl_avg,kbl,hbls,ghats,Akt,bvf,Akv,dRde,d
     &Rdx,diff3d_v,diff3d_u,diff4_sponge,tnu4,diff4,diff2_sponge,tnu2,di
     &ff2,visc2_sponge_p,visc2_sponge_r,visc2_p,visc2,visc2_r,UV_Tminor,
     &UV_Tmajor,UV_Tphase,UV_Tangle,SSH_Tphase,SSH_Tamp,Tperiod,vclm,ucl
     &m,vclima,uclima,vbclima,ubclima,vbclm,ubclm,tclima,tclm,Tnudgcof,s
     &shg,ssh,Znudgcof,srflxg,srflx_avg,srflx,shflx_sen_avg,shflx_lat_av
     &g,shflx_rlw_avg,shflx_rsw_avg,shflx_sen,shflx_lat,shflx_rlw,shflx_
     &rsw,vwndg,uwndg,wspdg,radswg,radlwg,prateg,rhumg,tairg,vwnd,uwnd,w
     &spd,radsw,radlw,prate,rhum,tair,btflx,stflxg,stflx_avg,stflx,bvstr
     &g,bustrg,svstr_avg,sustr_avg,wstr_avg,bostr_avg,bvstr,bustr,svstrg
     &,sustrg,svstr,sustr,t_avg,t,got_tini,omega_avg,We,w_avg,v_avg,u_av
     &g,rho_avg,rho,v,u,DV_avg2,DU_avg2,DV_avg1,DU_avg1,Zt_avg1,rufrc,vb
     &ar_avg,ubar_avg,zeta_avg,vbar,ubar,zeta,NORTH_INTER,Mmmpi,SOUTH_IN
     &TER,EAST_INTER,Lmmpi,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars


      include 'mpif.h'
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: Gslope_max = 5.D0
      real, parameter :: Rslope_max = 0.05D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: init = 0.D0
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      real :: visc2
      logical :: NORTH_INTER
      integer(4) :: Mmmpi
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      integer(4) :: Lmmpi
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: hbl_avg
      integer(4), dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: kbl
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: hbls
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: ghats
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N,1:2) :: Akt
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: bvf
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: Akv
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: dRde
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: dRdx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: diff3d_u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: diff4_sponge
      real, dimension(1:NT) :: tnu4
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: diff4
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: diff2_sponge
      real, dimension(1:NT) :: tnu2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: diff2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_sponge_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_p
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: visc2_r
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tmin
     &or
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tmaj
     &or
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tpha
     &se
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: UV_Tang
     &le
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: SSH_Tph
     &ase
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:Ntides) :: SSH_Tam
     &p
      real, dimension(1:Ntides) :: Tperiod
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: uclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: vclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2) :: uclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vbclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: ubclima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:2,1:NT) :: tcl
     &ima
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: tclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: Tnudgco
     &f
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: sshg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ssh
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Znudgcof
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: srflxg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: srflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_sen
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_lat
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: shflx_rsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: vwndg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: uwndg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: wspdg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: radswg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: radlwg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: prateg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: rhumg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: tairg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vwnd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: uwnd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: wspd
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: radsw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: radlw
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: prate
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rhum
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: tair
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: btflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2,1:NT) :: stflxg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:NT) :: stflx
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: bvstrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: bustrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: wstr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bostr_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bvstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: bustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: svstrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:2) :: sustrg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: svstr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: sustr
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:NT) :: t_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3,1:NT) :: t
      logical, dimension(1:NT) :: got_tini
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: omega_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,0:N) :: We
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: w_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: v_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: u_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: rho
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: u
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DV_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: DU_avg2
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DV_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:5) :: DU_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: Zt_avg1
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: rufrc
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vbar_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: ubar_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: zeta_avg
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: vbar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: ubar
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:4) :: zeta
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                 
                                                 
                                                 
                              
                              
                              
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                               
                                               
                                                       
                                                
                                                
                                                      
                                                      
                               
                               
                                       
                                       
                                                  
                                                    
                                                    
                                                  
                                                  
                                    
                                   
                                   
                                   
                                   
                                                   
                                                   
                                                   
                                                                       
                                                                 
                         
                                                    
                                  
                                                   
                                
                                                    
                                  
                                                    
                                  
                                                    
                                  
                                                  
                                                  
                                                     
                                                    
                                                        
                                                  
                                                                       
                                                                 
                                                          
                                                       
                                  
                                                  
                              
                                                   
                                
                                                        
                                                        
                                                        
                                                        
                                          
                                          
                                          
                                          
                                                       
                                    
                                                      
                                                        
                                               
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                
                                   
                                                    
                                              
                                                
                                            
                                                           
                                                     
                                                
                                                
                                                     
                                                   
                                                   
                                                         
                                                           
                                                         
                                                              
                                                           
                                                                
                                                                       
                                                                 
                                                                 
      
                                                                    
                                                                   
                                                   
                                 
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                    
                                      
                                                      
                                   
                                        
                                        
                                                       
                                                     
                                                               
                                                           
                                        
                                                   
                                 
                                               
                                               
                                                
                                                
                                                
                                               
                                               
                                               
                              
                              
                                
                                
                                
                              
                              
                              
                                                  
                                                  
                                                   
                                                   
                                                   
                                                  
                                                  
                                                  
                                  
                                  
                                    
                                    
                                    
                                  
                               
                               
                                                              
                       
                                
                                       
                                                           
                                                                
                                 
                                                  
                                                     
                           
                                                                       
                                                                       
                                                                 
                             
                                                                    
                                       
                                                              
                                              
                                                             
                                                  
                                        
                                   
                                                
                                 
                                                   
                                   
                                 
                                
                                           
                                         
                                                              
                                                                       
                                                                 
                                                                 
       
                                                  
                                                  
                                                         
                                                         
                                                             
                                                   
                                                   
                                                       
                                                   
                                               
                                 
                                                       
                                                   
                                               
                                 
                                                     
                                                     
                                       
                                       
                                                 
                                                 
                                                   
                               
                               
                               
                                 
                                                    

                        
                   
                                      
                                      
                                                  
                                                    
                                             
                                                  
                              
                                                   
                                                    
                                               
                               
                                 
                                 
                                                 
                                
                                                    
                                  
                                                
                                  
                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                           
                                     
                                                          
                                       
                                                            
                                           
                                                           
                                         
                                                           
                                         
                                                           
                                         
                                                           
                                         
                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                                                       
                                                                 
           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
      integer(4) :: itrc
      integer(4) :: itide
                
                           

                       
                     
      integer(4) :: ierr
                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
      if (Istr.eq.1) then
        if (WEST_INTER) then
          IstrR=Istr-2
        else
          IstrR=Istr-1
        endif
      else
        IstrR=Istr
      endif
      if (Iend.eq.Lmmpi) then
        if (EAST_INTER) then
          IendR=Iend+2
        else
          IendR=Iend+1
        endif
      else
        IendR=Iend
      endif
      if (Jstr.eq.1) then
        if (SOUTH_INTER) then
          JstrR=Jstr-2
        else
          JstrR=Jstr-1
        endif
      else
        JstrR=Jstr
      endif
      if (Jend.eq.Mmmpi) then
        if (NORTH_INTER) then
          JendR=Jend+2
        else
          JendR=Jend+1
        endif
      else
        JendR=Jend
      endif
      do j=JstrR,JendR
        do i=IstrR,IendR
          zeta(i,j,1)=0.D0
          zeta(i,j,2)=init
          zeta(i,j,3)=init
          zeta(i,j,4)=init
          ubar(i,j,1)=init
          ubar(i,j,2)=init
          ubar(i,j,3)=init
          ubar(i,j,4)=init
          vbar(i,j,1)=init
          vbar(i,j,2)=init
          vbar(i,j,3)=init
          vbar(i,j,4)=init
          zeta_avg(i,j)=init
          ubar_avg(i,j)=init
          vbar_avg(i,j)=init
          rufrc(i,j)=init
          rufrc(i,j)=init
          Zt_avg1(i,j)=0.D0
          DU_avg1(i,j,1)=0.D0
          DV_avg1(i,j,1)=0.D0
          DU_avg1(i,j,2)=0.D0
          DV_avg1(i,j,2)=0.D0
          DU_avg2(i,j)=0.D0
          DV_avg2(i,j)=0.D0
        enddo
      enddo
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            u(i,j,k,1)=init
            u(i,j,k,2)=init
            v(i,j,k,1)=init
            v(i,j,k,2)=init
            rho(i,j,k) =init
            rho_avg(i,j,k)=init
            u_avg(i,j,k)=init
            v_avg(i,j,k)=init
            w_avg(i,j,k)=init
          enddo
        enddo
      enddo
      do k=0,N
        do j=JstrR,JendR
          do i=IstrR,IendR
             We(i,j,k)=init
            omega_avg(i,j,k)=init
          enddo
        enddo
      enddo
      do itrc=1,NT
        got_tini(itrc)=.false.
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              t(i,j,k,1,itrc)=init
              t(i,j,k,2,itrc)=init
              t_avg(i,j,k,itrc)=init
            enddo
          enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          sustr(i,j)=init
          svstr(i,j)=init
          sustrg(i,j,1)=init
          svstrg(i,j,1)=init
          sustrg(i,j,2)=init
          svstrg(i,j,2)=init
          bustr(i,j)=init
          bvstr(i,j)=init
          bostr_avg(i,j)=init
          wstr_avg(i,j)=init
          sustr_avg(i,j)=init
          svstr_avg(i,j)=init
          bustrg(i,j,1)=init
          bvstrg(i,j,1)=init
          bustrg(i,j,2)=init
          bvstrg(i,j,2)=init
        enddo
      enddo
      do itrc=1,NT
        do j=JstrR,JendR
          do i=IstrR,IendR
            stflx(i,j,itrc)=init
            stflx_avg(i,j,itrc)=init
            stflxg(i,j,1,itrc)=init
            stflxg(i,j,2,itrc)=init
            btflx(i,j,itrc)=init
          enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          tair (i,j)=init
          rhum (i,j)=init
          prate (i,j)=init
          radlw (i,j)=init
          radsw (i,j)=init
          wspd(i,j)=init
          uwnd(i,j)=init
          vwnd(i,j)=init
          tairg(i,j,1)=init
          rhumg (i,j,1)=init
          prateg (i,j,1)=init
          radlwg (i,j,1)=init
          radswg (i,j,1)=init
          tairg(i,j,2)=init
          rhumg (i,j,2)=init
          prateg (i,j,2)=init
          radlwg (i,j,2)=init
          radswg (i,j,2)=init
          wspdg(i,j,1)=init
          wspdg(i,j,2)=init
          uwndg(i,j,1)=init
          vwndg(i,j,1)=init
          uwndg(i,j,2)=init
          vwndg(i,j,2)=init
          shflx_rsw(i,j)=init
          shflx_rlw(i,j)=init
          shflx_lat(i,j)=init
          shflx_sen(i,j)=init
          shflx_rsw_avg(i,j)=init
          shflx_rlw_avg(i,j)=init
          shflx_lat_avg(i,j)=init
          shflx_sen_avg(i,j)=init
          srflx(i,j)=init
          srflx_avg(i,j)=init
          srflxg(i,j,1)=init
          srflxg(i,j,2)=init
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          Znudgcof(i,j)=init
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          ssh(i,j)=init
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          sshg(i,j,1)=init
          sshg(i,j,2)=init
        enddo
      enddo
      do itrc=1,NT
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              Tnudgcof(i,j,k,itrc)=init
            enddo
          enddo
        enddo
      enddo
      do itrc=1,NT
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              tclm(i,j,k,itrc)=init
            enddo
          enddo
        enddo
      enddo
      do itrc=1,NT
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              tclima(i,j,k,1,itrc)=init
              tclima(i,j,k,2,itrc)=init
            enddo
          enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          ubclm(i,j)=init
          vbclm(i,j)=init
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          ubclima(i,j,1)=init
          ubclima(i,j,2)=init
          vbclima(i,j,1)=init
          vbclima(i,j,2)=init
        enddo
      enddo
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            uclima(i,j,k,1)=init
            uclima(i,j,k,2)=init
            vclima(i,j,k,1)=init
            vclima(i,j,k,2)=init
          enddo
        enddo
      enddo
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            uclm(i,j,k)=init
            vclm(i,j,k)=init
          enddo
        enddo
      enddo
      do itide=1,Ntides
        Tperiod(itide)=init
        do j=JstrR,JendR
          do i=IstrR,IendR
            SSH_Tamp(i,j,itide)=init
            SSH_Tphase(i,j,itide)=init
            UV_Tangle(i,j,itide)=init
            UV_Tphase(i,j,itide)=init
            UV_Tmajor(i,j,itide)=init
            UV_Tminor(i,j,itide)=init
          enddo
        enddo
      enddo
        do j=JstrR,JendR
          do i=IstrR,IendR
            visc2_r(i,j)=visc2
            visc2_p(i,j)=visc2
            visc2_sponge_r(i,j)=init
            visc2_sponge_p(i,j)=init
          enddo
        enddo
        do itrc=1,NT
          do j=JstrR,JendR
            do i=IstrR,IendR
              diff2(i,j,itrc)=tnu2(itrc)
            enddo
          enddo
        enddo
          do j=JstrR,JendR
            do i=IstrR,IendR
              diff2_sponge(i,j)=init
            enddo
          enddo
        do itrc=1,NT
          do j=JstrR,JendR
            do i=IstrR,IendR
              diff4(i,j,itrc)=tnu4(itrc)
            enddo
          enddo
        enddo
          do j=JstrR,JendR
            do i=IstrR,IendR
              diff4_sponge(i,j)=init
            enddo
          enddo
        do k=1,N
          do j=JstrR,JendR
            do i=IstrR,IendR
              diff3d_u(i,j,k)=init
              diff3d_v(i,j,k)=init
            enddo
          enddo
        enddo
        do k=1,N-1
          do j=JstrR,JendR
            do i=IstrR,IendR
              dRdx(i,j,k)=0.D0
              dRde(i,j,k)=0.D0
            enddo
          enddo
        enddo
      do k=0,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            Akv(i,j,k)=init
            bvf(i,j,k)=init
          enddo
        enddo
        do j=JstrR,JendR
          do i=IstrR,IendR
              Akt(i,j,k,itemp)=init
              Akt(i,j,k,isalt)=init
          enddo
        enddo
      enddo
      do k=1,N
        do j=JstrR,JendR
          do i=IstrR,IendR
            ghats(i,j,k)=init
          enddo
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          hbls(i,j,1)=init
          hbls(i,j,2)=init
          kbl(i,j)=init
          hbl_avg(i,j)=init
        enddo
      enddo
      do j=JstrR,JendR
        do i=IstrR,IendR
          hbbl(i,j)=init
          hbbl_avg(i,j)=init
        enddo
      enddo
      return
       
      

      end subroutine Sub_Loop_init_arrays_tile

      module tides_UV_Tphase


        implicit none
        public :: Alloc_agrif_tides_UV_Tphase
      contains
      subroutine Alloc_agrif_tides_UV_Tphase(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: Ntides = 10
      !! ALLOCATION OF VARIABLE : UV_Tphase
          if (.not. allocated(Agrif_Gr % tabvars(164)% array3)) then
          allocate(Agrif_Gr % tabvars(164)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : Ntides))
          Agrif_Gr % tabvars(164)% array3 = 0
      endif
      end subroutine Alloc_agrif_tides_UV_Tphase
      end module tides_UV_Tphase
      module tides_UV_Tminor


        implicit none
        public :: Alloc_agrif_tides_UV_Tminor
      contains
      subroutine Alloc_agrif_tides_UV_Tminor(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: Ntides = 10
      !! ALLOCATION OF VARIABLE : UV_Tminor
          if (.not. allocated(Agrif_Gr % tabvars(165)% array3)) then
          allocate(Agrif_Gr % tabvars(165)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : Ntides))
          Agrif_Gr % tabvars(165)% array3 = 0
      endif
      end subroutine Alloc_agrif_tides_UV_Tminor
      end module tides_UV_Tminor
      module tides_UV_Tmajor


        implicit none
        public :: Alloc_agrif_tides_UV_Tmajor
      contains
      subroutine Alloc_agrif_tides_UV_Tmajor(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: Ntides = 10
      !! ALLOCATION OF VARIABLE : UV_Tmajor
          if (.not. allocated(Agrif_Gr % tabvars(166)% array3)) then
          allocate(Agrif_Gr % tabvars(166)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : Ntides))
          Agrif_Gr % tabvars(166)% array3 = 0
      endif
      end subroutine Alloc_agrif_tides_UV_Tmajor
      end module tides_UV_Tmajor
      module tides_UV_Tangle


        implicit none
        public :: Alloc_agrif_tides_UV_Tangle
      contains
      subroutine Alloc_agrif_tides_UV_Tangle(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: Ntides = 10
      !! ALLOCATION OF VARIABLE : UV_Tangle
          if (.not. allocated(Agrif_Gr % tabvars(167)% array3)) then
          allocate(Agrif_Gr % tabvars(167)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : Ntides))
          Agrif_Gr % tabvars(167)% array3 = 0
      endif
      end subroutine Alloc_agrif_tides_UV_Tangle
      end module tides_UV_Tangle
      module tides_SSH_Tphase


        implicit none
        public :: Alloc_agrif_tides_SSH_Tphase
      contains
      subroutine Alloc_agrif_tides_SSH_Tphase(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: Ntides = 10
      !! ALLOCATION OF VARIABLE : SSH_Tphase
          if (.not. allocated(Agrif_Gr % tabvars(168)% array3)) then
          allocate(Agrif_Gr % tabvars(168)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : Ntides))
          Agrif_Gr % tabvars(168)% array3 = 0
      endif
      end subroutine Alloc_agrif_tides_SSH_Tphase
      end module tides_SSH_Tphase
      module tides_SSH_Tamp


        implicit none
        public :: Alloc_agrif_tides_SSH_Tamp
      contains
      subroutine Alloc_agrif_tides_SSH_Tamp(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: Ntides = 10
      !! ALLOCATION OF VARIABLE : SSH_Tamp
          if (.not. allocated(Agrif_Gr % tabvars(169)% array3)) then
          allocate(Agrif_Gr % tabvars(169)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : Ntides))
          Agrif_Gr % tabvars(169)% array3 = 0
      endif
      end subroutine Alloc_agrif_tides_SSH_Tamp
      end module tides_SSH_Tamp
      module tides_Tperiod


        implicit none
        public :: Alloc_agrif_tides_Tperiod
      contains
      subroutine Alloc_agrif_tides_Tperiod(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: Ntides = 10
      !! ALLOCATION OF VARIABLE : Tperiod
          if (.not. allocated(Agrif_Gr % tabvars(170)% array1)) then
          allocate(Agrif_Gr % tabvars(170)% array1(1 : Ntides))
          Agrif_Gr % tabvars(170)% array1 = 0
      endif
      end subroutine Alloc_agrif_tides_Tperiod
      end module tides_Tperiod
      module srfdat1


        implicit none
        public :: Alloc_agrif_srfdat1
      contains
      subroutine Alloc_agrif_srfdat1(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : srf_time
          if (.not. allocated(Agrif_Gr % tabvars(171)% array1)) then
          allocate(Agrif_Gr % tabvars(171)% array1(1 : 2))
          Agrif_Gr % tabvars(171)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : srflxp
          if (.not. allocated(Agrif_Gr % tabvars(172)% array1)) then
          allocate(Agrif_Gr % tabvars(172)% array1(1 : 2))
          Agrif_Gr % tabvars(172)% array1 = 0
      endif
      end subroutine Alloc_agrif_srfdat1
      end module srfdat1
      module srfdat_srflxg


        implicit none
        public :: Alloc_agrif_srfdat_srflxg
      contains
      subroutine Alloc_agrif_srfdat_srflxg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : srflxg
          if (.not. allocated(Agrif_Gr % tabvars(173)% array3)) then
          allocate(Agrif_Gr % tabvars(173)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(173)% array3 = 0
      endif
      end subroutine Alloc_agrif_srfdat_srflxg
      end module srfdat_srflxg
      module forces_srflx


        implicit none
        public :: Alloc_agrif_forces_srflx
      contains
      subroutine Alloc_agrif_forces_srflx(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : srflx
          if (.not. allocated(Agrif_Gr % tabvars(174)% array2)) then
          allocate(Agrif_Gr % tabvars(174)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(174)% array2 = 0
      endif
      end subroutine Alloc_agrif_forces_srflx
      end module forces_srflx
      module bulkdat2_wspd


        implicit none
        public :: Alloc_agrif_bulkdat2_wspd
      contains
      subroutine Alloc_agrif_bulkdat2_wspd(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : wspdp
          if (.not. allocated(Agrif_Gr % tabvars(175)% array1)) then
          allocate(Agrif_Gr % tabvars(175)% array1(1 : 2))
          Agrif_Gr % tabvars(175)% array1 = 0
      endif
      end subroutine Alloc_agrif_bulkdat2_wspd
      end module bulkdat2_wspd
      module bulkdat2_wnd


        implicit none
        public :: Alloc_agrif_bulkdat2_wnd
      contains
      subroutine Alloc_agrif_bulkdat2_wnd(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : vwndp
          if (.not. allocated(Agrif_Gr % tabvars(176)% array1)) then
          allocate(Agrif_Gr % tabvars(176)% array1(1 : 2))
          Agrif_Gr % tabvars(176)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : uwndp
          if (.not. allocated(Agrif_Gr % tabvars(177)% array1)) then
          allocate(Agrif_Gr % tabvars(177)% array1(1 : 2))
          Agrif_Gr % tabvars(177)% array1 = 0
      endif
      end subroutine Alloc_agrif_bulkdat2_wnd
      end module bulkdat2_wnd
      module bulkdat2_tim


        implicit none
        public :: Alloc_agrif_bulkdat2_tim
      contains
      subroutine Alloc_agrif_bulkdat2_tim(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : bulk_time
          if (.not. allocated(Agrif_Gr % tabvars(178)% array1)) then
          allocate(Agrif_Gr % tabvars(178)% array1(1 : 2))
          Agrif_Gr % tabvars(178)% array1 = 0
      endif
      end subroutine Alloc_agrif_bulkdat2_tim
      end module bulkdat2_tim
      module bulkdat2_for


        implicit none
        public :: Alloc_agrif_bulkdat2_for
      contains
      subroutine Alloc_agrif_bulkdat2_for(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : radswp
      do i = 179,183
          if (.not. allocated(Agrif_Gr % tabvars(i)% array1)) then
          allocate(Agrif_Gr % tabvars(i)% array1(1 : 2))
          Agrif_Gr % tabvars(i)% array1 = 0
      endif
      enddo
      end subroutine Alloc_agrif_bulkdat2_for
      end module bulkdat2_for
      module bulk_vwndg


        implicit none
        public :: Alloc_agrif_bulk_vwndg
      contains
      subroutine Alloc_agrif_bulk_vwndg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : vwndg
          if (.not. allocated(Agrif_Gr % tabvars(184)% array3)) then
          allocate(Agrif_Gr % tabvars(184)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(184)% array3 = 0
      endif
      end subroutine Alloc_agrif_bulk_vwndg
      end module bulk_vwndg
      module bulk_uwndg


        implicit none
        public :: Alloc_agrif_bulk_uwndg
      contains
      subroutine Alloc_agrif_bulk_uwndg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : uwndg
          if (.not. allocated(Agrif_Gr % tabvars(185)% array3)) then
          allocate(Agrif_Gr % tabvars(185)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(185)% array3 = 0
      endif
      end subroutine Alloc_agrif_bulk_uwndg
      end module bulk_uwndg
      module bulkdat_wspdg


        implicit none
        public :: Alloc_agrif_bulkdat_wspdg
      contains
      subroutine Alloc_agrif_bulkdat_wspdg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : wspdg
          if (.not. allocated(Agrif_Gr % tabvars(186)% array3)) then
          allocate(Agrif_Gr % tabvars(186)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(186)% array3 = 0
      endif
      end subroutine Alloc_agrif_bulkdat_wspdg
      end module bulkdat_wspdg
      module bulkdat_radswg


        implicit none
        public :: Alloc_agrif_bulkdat_radswg
      contains
      subroutine Alloc_agrif_bulkdat_radswg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : radswg
          if (.not. allocated(Agrif_Gr % tabvars(187)% array3)) then
          allocate(Agrif_Gr % tabvars(187)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(187)% array3 = 0
      endif
      end subroutine Alloc_agrif_bulkdat_radswg
      end module bulkdat_radswg
      module bulkdat_radlwg


        implicit none
        public :: Alloc_agrif_bulkdat_radlwg
      contains
      subroutine Alloc_agrif_bulkdat_radlwg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : radlwg
          if (.not. allocated(Agrif_Gr % tabvars(188)% array3)) then
          allocate(Agrif_Gr % tabvars(188)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(188)% array3 = 0
      endif
      end subroutine Alloc_agrif_bulkdat_radlwg
      end module bulkdat_radlwg
      module bulkdat_prateg


        implicit none
        public :: Alloc_agrif_bulkdat_prateg
      contains
      subroutine Alloc_agrif_bulkdat_prateg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : prateg
          if (.not. allocated(Agrif_Gr % tabvars(189)% array3)) then
          allocate(Agrif_Gr % tabvars(189)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(189)% array3 = 0
      endif
      end subroutine Alloc_agrif_bulkdat_prateg
      end module bulkdat_prateg
      module bulkdat_rhumg


        implicit none
        public :: Alloc_agrif_bulkdat_rhumg
      contains
      subroutine Alloc_agrif_bulkdat_rhumg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : rhumg
          if (.not. allocated(Agrif_Gr % tabvars(190)% array3)) then
          allocate(Agrif_Gr % tabvars(190)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(190)% array3 = 0
      endif
      end subroutine Alloc_agrif_bulkdat_rhumg
      end module bulkdat_rhumg
      module bulkdat_tairg


        implicit none
        public :: Alloc_agrif_bulkdat_tairg
      contains
      subroutine Alloc_agrif_bulkdat_tairg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : tairg
          if (.not. allocated(Agrif_Gr % tabvars(191)% array3)) then
          allocate(Agrif_Gr % tabvars(191)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(191)% array3 = 0
      endif
      end subroutine Alloc_agrif_bulkdat_tairg
      end module bulkdat_tairg
      module bulk_vwnd


        implicit none
        public :: Alloc_agrif_bulk_vwnd
      contains
      subroutine Alloc_agrif_bulk_vwnd(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : vwnd
          if (.not. allocated(Agrif_Gr % tabvars(192)% array2)) then
          allocate(Agrif_Gr % tabvars(192)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(192)% array2 = 0
      endif
      end subroutine Alloc_agrif_bulk_vwnd
      end module bulk_vwnd
      module bulk_uwnd


        implicit none
        public :: Alloc_agrif_bulk_uwnd
      contains
      subroutine Alloc_agrif_bulk_uwnd(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : uwnd
          if (.not. allocated(Agrif_Gr % tabvars(193)% array2)) then
          allocate(Agrif_Gr % tabvars(193)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(193)% array2 = 0
      endif
      end subroutine Alloc_agrif_bulk_uwnd
      end module bulk_uwnd
      module bulk_wspd


        implicit none
        public :: Alloc_agrif_bulk_wspd
      contains
      subroutine Alloc_agrif_bulk_wspd(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : wspd
          if (.not. allocated(Agrif_Gr % tabvars(194)% array2)) then
          allocate(Agrif_Gr % tabvars(194)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(194)% array2 = 0
      endif
      end subroutine Alloc_agrif_bulk_wspd
      end module bulk_wspd
      module bulk_radsw


        implicit none
        public :: Alloc_agrif_bulk_radsw
      contains
      subroutine Alloc_agrif_bulk_radsw(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : radsw
          if (.not. allocated(Agrif_Gr % tabvars(195)% array2)) then
          allocate(Agrif_Gr % tabvars(195)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(195)% array2 = 0
      endif
      end subroutine Alloc_agrif_bulk_radsw
      end module bulk_radsw
      module bulk_radlw


        implicit none
        public :: Alloc_agrif_bulk_radlw
      contains
      subroutine Alloc_agrif_bulk_radlw(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : radlw
          if (.not. allocated(Agrif_Gr % tabvars(196)% array2)) then
          allocate(Agrif_Gr % tabvars(196)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(196)% array2 = 0
      endif
      end subroutine Alloc_agrif_bulk_radlw
      end module bulk_radlw
      module bulk_prate


        implicit none
        public :: Alloc_agrif_bulk_prate
      contains
      subroutine Alloc_agrif_bulk_prate(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : prate
          if (.not. allocated(Agrif_Gr % tabvars(197)% array2)) then
          allocate(Agrif_Gr % tabvars(197)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(197)% array2 = 0
      endif
      end subroutine Alloc_agrif_bulk_prate
      end module bulk_prate
      module bulk_rhum


        implicit none
        public :: Alloc_agrif_bulk_rhum
      contains
      subroutine Alloc_agrif_bulk_rhum(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : rhum
          if (.not. allocated(Agrif_Gr % tabvars(198)% array2)) then
          allocate(Agrif_Gr % tabvars(198)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(198)% array2 = 0
      endif
      end subroutine Alloc_agrif_bulk_rhum
      end module bulk_rhum
      module bulk_tair


        implicit none
        public :: Alloc_agrif_bulk_tair
      contains
      subroutine Alloc_agrif_bulk_tair(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : tair
          if (.not. allocated(Agrif_Gr % tabvars(199)% array2)) then
          allocate(Agrif_Gr % tabvars(199)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(199)% array2 = 0
      endif
      end subroutine Alloc_agrif_bulk_tair
      end module bulk_tair
      module forces_btflx


        implicit none
        public :: Alloc_agrif_forces_btflx
      contains
      subroutine Alloc_agrif_forces_btflx(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      !! ALLOCATION OF VARIABLE : btflx
          if (.not. allocated(Agrif_Gr % tabvars(200)% array3)) then
          allocate(Agrif_Gr % tabvars(200)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : NT))
          Agrif_Gr % tabvars(200)% array3 = 0
      endif
      end subroutine Alloc_agrif_forces_btflx
      end module forces_btflx
      module stfdat3


        implicit none
        public :: Alloc_agrif_stfdat3
      contains
      subroutine Alloc_agrif_stfdat3(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      !! ALLOCATION OF VARIABLE : stf_id
          if (.not. allocated(Agrif_Gr % tabvars_i(256)% iarray1)) then
          allocate(Agrif_Gr % tabvars_i(256)% iarray1(1 : NT))
          Agrif_Gr % tabvars_i(256)% iarray1 = 0
      endif
      !! ALLOCATION OF VARIABLE : stf_tid
          if (.not. allocated(Agrif_Gr % tabvars_i(257)% iarray1)) then
          allocate(Agrif_Gr % tabvars_i(257)% iarray1(1 : NT))
          Agrif_Gr % tabvars_i(257)% iarray1 = 0
      endif
      end subroutine Alloc_agrif_stfdat3
      end module stfdat3
      module stfdat2


        implicit none
        public :: Alloc_agrif_stfdat2
      contains
      subroutine Alloc_agrif_stfdat2(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      !! ALLOCATION OF VARIABLE : lstfgrd
      do i = 258,261
          if (.not. allocated(Agrif_Gr % tabvars_i(i)% iarray1)) then
          allocate(Agrif_Gr % tabvars_i(i)% iarray1(1 : NT))
          Agrif_Gr % tabvars_i(i)% iarray1 = 0
      endif
      enddo
      end subroutine Alloc_agrif_stfdat2
      end module stfdat2
      module stfdat1


        implicit none
        public :: Alloc_agrif_stfdat1
      contains
      subroutine Alloc_agrif_stfdat1(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      !! ALLOCATION OF VARIABLE : stf_scale
          if (.not. allocated(Agrif_Gr % tabvars(201)% array1)) then
          allocate(Agrif_Gr % tabvars(201)% array1(1 : NT))
          Agrif_Gr % tabvars(201)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : stf_cycle
          if (.not. allocated(Agrif_Gr % tabvars(202)% array1)) then
          allocate(Agrif_Gr % tabvars(202)% array1(1 : NT))
          Agrif_Gr % tabvars(202)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : stf_time
          if (.not. allocated(Agrif_Gr % tabvars(203)% array2)) then
          allocate(Agrif_Gr % tabvars(203)% array2(1 : 2,1 : NT))
          Agrif_Gr % tabvars(203)% array2 = 0
      endif
      !! ALLOCATION OF VARIABLE : stflxp
          if (.not. allocated(Agrif_Gr % tabvars(204)% array2)) then
          allocate(Agrif_Gr % tabvars(204)% array2(1 : 2,1 : NT))
          Agrif_Gr % tabvars(204)% array2 = 0
      endif
      end subroutine Alloc_agrif_stfdat1
      end module stfdat1
      module stfdat_stflxg


        implicit none
        public :: Alloc_agrif_stfdat_stflxg
      contains
      subroutine Alloc_agrif_stfdat_stflxg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      !! ALLOCATION OF VARIABLE : stflxg
          if (.not. allocated(Agrif_Gr % tabvars(205)% array4)) then
          allocate(Agrif_Gr % tabvars(205)% array4(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2,1 : NT))
          Agrif_Gr % tabvars(205)% array4 = 0
      endif
      end subroutine Alloc_agrif_stfdat_stflxg
      end module stfdat_stflxg
      module frc_shflx_sen


        implicit none
        public :: Alloc_agrif_frc_shflx_sen
      contains
      subroutine Alloc_agrif_frc_shflx_sen(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : shflx_sen
          if (.not. allocated(Agrif_Gr % tabvars(206)% array2)) then
          allocate(Agrif_Gr % tabvars(206)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(206)% array2 = 0
      endif
      end subroutine Alloc_agrif_frc_shflx_sen
      end module frc_shflx_sen
      module frc_shflx_lat


        implicit none
        public :: Alloc_agrif_frc_shflx_lat
      contains
      subroutine Alloc_agrif_frc_shflx_lat(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : shflx_lat
          if (.not. allocated(Agrif_Gr % tabvars(207)% array2)) then
          allocate(Agrif_Gr % tabvars(207)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(207)% array2 = 0
      endif
      end subroutine Alloc_agrif_frc_shflx_lat
      end module frc_shflx_lat
      module frc_shflx_rlw


        implicit none
        public :: Alloc_agrif_frc_shflx_rlw
      contains
      subroutine Alloc_agrif_frc_shflx_rlw(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : shflx_rlw
          if (.not. allocated(Agrif_Gr % tabvars(208)% array2)) then
          allocate(Agrif_Gr % tabvars(208)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(208)% array2 = 0
      endif
      end subroutine Alloc_agrif_frc_shflx_rlw
      end module frc_shflx_rlw
      module frc_shflx_rsw


        implicit none
        public :: Alloc_agrif_frc_shflx_rsw
      contains
      subroutine Alloc_agrif_frc_shflx_rsw(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : shflx_rsw
          if (.not. allocated(Agrif_Gr % tabvars(209)% array2)) then
          allocate(Agrif_Gr % tabvars(209)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(209)% array2 = 0
      endif
      end subroutine Alloc_agrif_frc_shflx_rsw
      end module frc_shflx_rsw
      module forces_stflx


        implicit none
        public :: Alloc_agrif_forces_stflx
      contains
      subroutine Alloc_agrif_forces_stflx(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      !! ALLOCATION OF VARIABLE : stflx
          if (.not. allocated(Agrif_Gr % tabvars(210)% array3)) then
          allocate(Agrif_Gr % tabvars(210)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : NT))
          Agrif_Gr % tabvars(210)% array3 = 0
      endif
      end subroutine Alloc_agrif_forces_stflx
      end module forces_stflx
      module bmsdat1


        implicit none
        public :: Alloc_agrif_bmsdat1
      contains
      subroutine Alloc_agrif_bmsdat1(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : tbms
      do i = 211,214
          if (.not. allocated(Agrif_Gr % tabvars(i)% array1)) then
          allocate(Agrif_Gr % tabvars(i)% array1(1 : 2))
          Agrif_Gr % tabvars(i)% array1 = 0
      endif
      enddo
      end subroutine Alloc_agrif_bmsdat1
      end module bmsdat1
      module bmsdat_bvstrg


        implicit none
        public :: Alloc_agrif_bmsdat_bvstrg
      contains
      subroutine Alloc_agrif_bmsdat_bvstrg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : bvstrg
          if (.not. allocated(Agrif_Gr % tabvars(215)% array3)) then
          allocate(Agrif_Gr % tabvars(215)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(215)% array3 = 0
      endif
      end subroutine Alloc_agrif_bmsdat_bvstrg
      end module bmsdat_bvstrg
      module bmsdat_bustrg


        implicit none
        public :: Alloc_agrif_bmsdat_bustrg
      contains
      subroutine Alloc_agrif_bmsdat_bustrg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : bustrg
          if (.not. allocated(Agrif_Gr % tabvars(216)% array3)) then
          allocate(Agrif_Gr % tabvars(216)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(216)% array3 = 0
      endif
      end subroutine Alloc_agrif_bmsdat_bustrg
      end module bmsdat_bustrg
      module forces_bvstr


        implicit none
        public :: Alloc_agrif_forces_bvstr
      contains
      subroutine Alloc_agrif_forces_bvstr(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : bvstr
          if (.not. allocated(Agrif_Gr % tabvars(217)% array2)) then
          allocate(Agrif_Gr % tabvars(217)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(217)% array2 = 0
      endif
      end subroutine Alloc_agrif_forces_bvstr
      end module forces_bvstr
      module forces_bustr


        implicit none
        public :: Alloc_agrif_forces_bustr
      contains
      subroutine Alloc_agrif_forces_bustr(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : bustr
          if (.not. allocated(Agrif_Gr % tabvars(218)% array2)) then
          allocate(Agrif_Gr % tabvars(218)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(218)% array2 = 0
      endif
      end subroutine Alloc_agrif_forces_bustr
      end module forces_bustr
      module smsdat1


        implicit none
        public :: Alloc_agrif_smsdat1
      contains
      subroutine Alloc_agrif_smsdat1(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : sms_time
          if (.not. allocated(Agrif_Gr % tabvars(219)% array1)) then
          allocate(Agrif_Gr % tabvars(219)% array1(1 : 2))
          Agrif_Gr % tabvars(219)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : svstrp
          if (.not. allocated(Agrif_Gr % tabvars(220)% array1)) then
          allocate(Agrif_Gr % tabvars(220)% array1(1 : 2))
          Agrif_Gr % tabvars(220)% array1 = 0
      endif
      !! ALLOCATION OF VARIABLE : sustrp
          if (.not. allocated(Agrif_Gr % tabvars(221)% array1)) then
          allocate(Agrif_Gr % tabvars(221)% array1(1 : 2))
          Agrif_Gr % tabvars(221)% array1 = 0
      endif
      end subroutine Alloc_agrif_smsdat1
      end module smsdat1
      module smsdat_svstrg


        implicit none
        public :: Alloc_agrif_smsdat_svstrg
      contains
      subroutine Alloc_agrif_smsdat_svstrg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : svstrg
          if (.not. allocated(Agrif_Gr % tabvars(222)% array3)) then
          allocate(Agrif_Gr % tabvars(222)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(222)% array3 = 0
      endif
      end subroutine Alloc_agrif_smsdat_svstrg
      end module smsdat_svstrg
      module smsdat_sustrg


        implicit none
        public :: Alloc_agrif_smsdat_sustrg
      contains
      subroutine Alloc_agrif_smsdat_sustrg(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : sustrg
          if (.not. allocated(Agrif_Gr % tabvars(223)% array3)) then
          allocate(Agrif_Gr % tabvars(223)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : 2))
          Agrif_Gr % tabvars(223)% array3 = 0
      endif
      end subroutine Alloc_agrif_smsdat_sustrg
      end module smsdat_sustrg
      module forces_svstr


        implicit none
        public :: Alloc_agrif_forces_svstr
      contains
      subroutine Alloc_agrif_forces_svstr(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : svstr
          if (.not. allocated(Agrif_Gr % tabvars(224)% array2)) then
          allocate(Agrif_Gr % tabvars(224)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(224)% array2 = 0
      endif
      end subroutine Alloc_agrif_forces_svstr
      end module forces_svstr
      module forces_sustr


        implicit none
        public :: Alloc_agrif_forces_sustr
      contains
      subroutine Alloc_agrif_forces_sustr(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : sustr
          if (.not. allocated(Agrif_Gr % tabvars(225)% array2)) then
          allocate(Agrif_Gr % tabvars(225)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(225)% array2 = 0
      endif
      end subroutine Alloc_agrif_forces_sustr
      end module forces_sustr
      module avg_Akt


        implicit none
        public :: Alloc_agrif_avg_Akt
      contains
      subroutine Alloc_agrif_avg_Akt(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : Akt_avg
          if (.not. allocated(Agrif_Gr % tabvars(226)% array4)) then
          allocate(Agrif_Gr % tabvars(226)% array4(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,0 : N,1 : 2))
          Agrif_Gr % tabvars(226)% array4 = 0
      endif
      end subroutine Alloc_agrif_avg_Akt
      end module avg_Akt
      module avg_Akv


        implicit none
        public :: Alloc_agrif_avg_Akv
      contains
      subroutine Alloc_agrif_avg_Akv(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : Akv_avg
          if (.not. allocated(Agrif_Gr % tabvars(227)% array3)) then
          allocate(Agrif_Gr % tabvars(227)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,0 : N))
          Agrif_Gr % tabvars(227)% array3 = 0
      endif
      end subroutine Alloc_agrif_avg_Akv
      end module avg_Akv
      module avg_diff3d


        implicit none
        public :: Alloc_agrif_avg_diff3d
      contains
      subroutine Alloc_agrif_avg_diff3d(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : diff3d_avg
          if (.not. allocated(Agrif_Gr % tabvars(228)% array3)) then
          allocate(Agrif_Gr % tabvars(228)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(228)% array3 = 0
      endif
      end subroutine Alloc_agrif_avg_diff3d
      end module avg_diff3d
      module avg_shflx_sen


        implicit none
        public :: Alloc_agrif_avg_shflx_sen
      contains
      subroutine Alloc_agrif_avg_shflx_sen(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : shflx_sen_avg
          if (.not. allocated(Agrif_Gr % tabvars(229)% array2)) then
          allocate(Agrif_Gr % tabvars(229)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(229)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_shflx_sen
      end module avg_shflx_sen
      module avg_shflx_lat


        implicit none
        public :: Alloc_agrif_avg_shflx_lat
      contains
      subroutine Alloc_agrif_avg_shflx_lat(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : shflx_lat_avg
          if (.not. allocated(Agrif_Gr % tabvars(230)% array2)) then
          allocate(Agrif_Gr % tabvars(230)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(230)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_shflx_lat
      end module avg_shflx_lat
      module avg_shflx_rlw


        implicit none
        public :: Alloc_agrif_avg_shflx_rlw
      contains
      subroutine Alloc_agrif_avg_shflx_rlw(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : shflx_rlw_avg
          if (.not. allocated(Agrif_Gr % tabvars(231)% array2)) then
          allocate(Agrif_Gr % tabvars(231)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(231)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_shflx_rlw
      end module avg_shflx_rlw
      module avg_shflx_rsw


        implicit none
        public :: Alloc_agrif_avg_shflx_rsw
      contains
      subroutine Alloc_agrif_avg_shflx_rsw(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : shflx_rsw_avg
          if (.not. allocated(Agrif_Gr % tabvars(232)% array2)) then
          allocate(Agrif_Gr % tabvars(232)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(232)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_shflx_rsw
      end module avg_shflx_rsw
      module avg_hbbl


        implicit none
        public :: Alloc_agrif_avg_hbbl
      contains
      subroutine Alloc_agrif_avg_hbbl(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : hbbl_avg
          if (.not. allocated(Agrif_Gr % tabvars(233)% array2)) then
          allocate(Agrif_Gr % tabvars(233)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(233)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_hbbl
      end module avg_hbbl
      module avg_hbl


        implicit none
        public :: Alloc_agrif_avg_hbl
      contains
      subroutine Alloc_agrif_avg_hbl(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : hbl_avg
          if (.not. allocated(Agrif_Gr % tabvars(234)% array2)) then
          allocate(Agrif_Gr % tabvars(234)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(234)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_hbl
      end module avg_hbl
      module avg_stflx


        implicit none
        public :: Alloc_agrif_avg_stflx
      contains
      subroutine Alloc_agrif_avg_stflx(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      !! ALLOCATION OF VARIABLE : stflx_avg
          if (.not. allocated(Agrif_Gr % tabvars(235)% array3)) then
          allocate(Agrif_Gr % tabvars(235)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : NT))
          Agrif_Gr % tabvars(235)% array3 = 0
      endif
      end subroutine Alloc_agrif_avg_stflx
      end module avg_stflx
      module avg_w


        implicit none
        public :: Alloc_agrif_avg_w
      contains
      subroutine Alloc_agrif_avg_w(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : w_avg
          if (.not. allocated(Agrif_Gr % tabvars(236)% array3)) then
          allocate(Agrif_Gr % tabvars(236)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(236)% array3 = 0
      endif
      end subroutine Alloc_agrif_avg_w
      end module avg_w
      module avg_omega


        implicit none
        public :: Alloc_agrif_avg_omega
      contains
      subroutine Alloc_agrif_avg_omega(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : omega_avg
          if (.not. allocated(Agrif_Gr % tabvars(237)% array3)) then
          allocate(Agrif_Gr % tabvars(237)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,0 : N))
          Agrif_Gr % tabvars(237)% array3 = 0
      endif
      end subroutine Alloc_agrif_avg_omega
      end module avg_omega
      module avg_rho


        implicit none
        public :: Alloc_agrif_avg_rho
      contains
      subroutine Alloc_agrif_avg_rho(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : rho_avg
          if (.not. allocated(Agrif_Gr % tabvars(238)% array3)) then
          allocate(Agrif_Gr % tabvars(238)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(238)% array3 = 0
      endif
      end subroutine Alloc_agrif_avg_rho
      end module avg_rho
      module avg_t


        implicit none
        public :: Alloc_agrif_avg_t
      contains
      subroutine Alloc_agrif_avg_t(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : t_avg
          if (.not. allocated(Agrif_Gr % tabvars(239)% array4)) then
          allocate(Agrif_Gr % tabvars(239)% array4(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N,1 : NT))
          Agrif_Gr % tabvars(239)% array4 = 0
      endif
      end subroutine Alloc_agrif_avg_t
      end module avg_t
      module avg_v


        implicit none
        public :: Alloc_agrif_avg_v
      contains
      subroutine Alloc_agrif_avg_v(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : v_avg
          if (.not. allocated(Agrif_Gr % tabvars(240)% array3)) then
          allocate(Agrif_Gr % tabvars(240)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(240)% array3 = 0
      endif
      end subroutine Alloc_agrif_avg_v
      end module avg_v
      module avg_u


        implicit none
        public :: Alloc_agrif_avg_u
      contains
      subroutine Alloc_agrif_avg_u(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      integer(4), parameter :: N = 66
      !! ALLOCATION OF VARIABLE : u_avg
          if (.not. allocated(Agrif_Gr % tabvars(241)% array3)) then
          allocate(Agrif_Gr % tabvars(241)% array3(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0,1 : N))
          Agrif_Gr % tabvars(241)% array3 = 0
      endif
      end subroutine Alloc_agrif_avg_u
      end module avg_u
      module avg_srflx


        implicit none
        public :: Alloc_agrif_avg_srflx
      contains
      subroutine Alloc_agrif_avg_srflx(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : srflx_avg
          if (.not. allocated(Agrif_Gr % tabvars(242)% array2)) then
          allocate(Agrif_Gr % tabvars(242)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(242)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_srflx
      end module avg_srflx
      module avg_svstr


        implicit none
        public :: Alloc_agrif_avg_svstr
      contains
      subroutine Alloc_agrif_avg_svstr(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : svstr_avg
          if (.not. allocated(Agrif_Gr % tabvars(243)% array2)) then
          allocate(Agrif_Gr % tabvars(243)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(243)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_svstr
      end module avg_svstr
      module avg_sustr


        implicit none
        public :: Alloc_agrif_avg_sustr
      contains
      subroutine Alloc_agrif_avg_sustr(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : sustr_avg
          if (.not. allocated(Agrif_Gr % tabvars(244)% array2)) then
          allocate(Agrif_Gr % tabvars(244)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(244)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_sustr
      end module avg_sustr
      module avg_wstr


        implicit none
        public :: Alloc_agrif_avg_wstr
      contains
      subroutine Alloc_agrif_avg_wstr(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : wstr_avg
          if (.not. allocated(Agrif_Gr % tabvars(245)% array2)) then
          allocate(Agrif_Gr % tabvars(245)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(245)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_wstr
      end module avg_wstr
      module avg_bostr


        implicit none
        public :: Alloc_agrif_avg_bostr
      contains
      subroutine Alloc_agrif_avg_bostr(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : bostr_avg
          if (.not. allocated(Agrif_Gr % tabvars(246)% array2)) then
          allocate(Agrif_Gr % tabvars(246)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(246)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_bostr
      end module avg_bostr
      module avg_vbar


        implicit none
        public :: Alloc_agrif_avg_vbar
      contains
      subroutine Alloc_agrif_avg_vbar(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : vbar_avg
          if (.not. allocated(Agrif_Gr % tabvars(247)% array2)) then
          allocate(Agrif_Gr % tabvars(247)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(247)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_vbar
      end module avg_vbar
      module avg_ubar


        implicit none
        public :: Alloc_agrif_avg_ubar
      contains
      subroutine Alloc_agrif_avg_ubar(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : ubar_avg
          if (.not. allocated(Agrif_Gr % tabvars(248)% array2)) then
          allocate(Agrif_Gr % tabvars(248)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(248)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_ubar
      end module avg_ubar
      module avg_zeta


        implicit none
        public :: Alloc_agrif_avg_zeta
      contains
      subroutine Alloc_agrif_avg_zeta(Agrif_Gr)
        use Agrif_Util
        type(Agrif_grid), pointer :: Agrif_Gr
        integer :: i
      !! ALLOCATION OF VARIABLE : zeta_avg
          if (.not. allocated(Agrif_Gr % tabvars(249)% array2)) then
          allocate(Agrif_Gr % tabvars(249)% array2(-1 :  Agrif_tabvars_i
     &(200)%iarray0+2+ Agrif_tabvars_i(189)%iarray0,-1 :  Agrif_tabvars_
     &i(197)%iarray0+2+ Agrif_tabvars_i(188)%iarray0))
          Agrif_Gr % tabvars(249)% array2 = 0
      endif
      end subroutine Alloc_agrif_avg_zeta
      end module avg_zeta
