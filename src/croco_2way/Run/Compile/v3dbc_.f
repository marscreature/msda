












      subroutine v3dbc_tile (Istr,Iend,Jstr,Jend,grad)
      use Agrif_Types, only : Agrif_tabvars

      use AGRIF_Util

      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad

                     
                                    
                                             
      if (AGRIF_Root()) then
        call v3dbc_parent_tile (Istr,Iend,Jstr,Jend,grad)
      else
        call v3dbc_child_tile (Istr,Iend,Jstr,Jend,grad)
      endif
      return
      end
      subroutine v3dbc_parent_tile (Istr,Iend,Jstr,Jend,grad)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_v3dbc_parent_tile(Istr,Iend,Jstr,Jend,grad,p
     &add_E,Mm,padd_X,Lm,vmask,vclm,nnew,pmask,nstp,v,tauM_out,tauM_in,d
     &t,NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: nstp
      real :: tauM_out
      real :: tauM_in
      real :: dt
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad
        end subroutine Sub_Loop_v3dbc_parent_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad
      

        call Sub_Loop_v3dbc_parent_tile(Istr,Iend,Jstr,Jend,grad, Agrif_
     &tabvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvar
     &s_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(48)%
     &array2, Agrif_tabvars(130)%array3, Agrif_tabvars_i(174)%iarray0, A
     &grif_tabvars(50)%array2, Agrif_tabvars_i(176)%iarray0, Agrif_tabva
     &rs(110)%array4, Agrif_tabvars_r(16)%array0, Agrif_tabvars_r(17)%ar
     &ray0, Agrif_tabvars_r(46)%array0, Agrif_tabvars_l(5)%larray0, Agri
     &f_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Agrif_tabvars_
     &l(6)%larray0)

      end


      subroutine Sub_Loop_v3dbc_parent_tile(Istr,Iend,Jstr,Jend,grad,pad
     &d_E,Mm,padd_X,Lm,vmask,vclm,nnew,pmask,nstp,v,tauM_out,tauM_in,dt,
     &NORTH_INTER,SOUTH_INTER,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      integer(4) :: nstp
      real :: tauM_out
      real :: tauM_in
      real :: dt
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: pmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                                                       
                                                                 
                                   
      real :: cff
      real :: cx
      real :: cy
      real :: dft
      real :: dfx
      real :: dfy
      real :: tau
      real :: tau_in
      real :: tau_out
                            

                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      tau_in=dt*tauM_in
      tau_out=dt*tauM_out
      if (.not.SOUTH_INTER) then
        do k=1,N
          do i=Istr,Iend+1
            grad(i,Jstr  )=(v(i,Jstr  ,k,nstp)-v(i-1,Jstr  ,k,nstp))
     &                                                *pmask(i,Jstr)
            grad(i,Jstr+1)=(v(i,Jstr+1,k,nstp)-v(i-1,Jstr+1,k,nstp))
     &                                              *pmask(i,Jstr+1)
          enddo
          do i=Istr,Iend
            dft=v(i,Jstr+1,k,nstp)-v(i,Jstr+1,k,nnew)
            dfx=v(i,Jstr+1,k,nnew)-v(i,Jstr+2,k,nnew)
            if (dfx*dft .lt. 0.D0) then
              dft=0.D0
              tau=tau_in
            else
              tau=tau_out
            endif
            if (dft*(grad(i,Jstr+1)+grad(i+1,Jstr+1)) .gt. 0.D0) then
              dfy=grad(i,Jstr+1)
            else
              dfy=grad(i+1,Jstr+1)
            endif
            cff=max(dfx*dfx+dfy*dfy, eps)
            cx=dft*dfx
            cy=min(cff,max(dft*dfy,-cff))
            v(i,Jstr,k,nnew)=( cff*v(i,Jstr,k,nstp)
     &                        +cx*v(i,Jstr+1,k,nnew)
     &                    -max(cy,0.D0)*grad(i  ,Jstr)
     &                    -min(cy,0.D0)*grad(i+1,Jstr)
     &                                   )/(cff+cx)
            v(i,Jstr,k,nnew)=(1.D0-tau)*v(i,Jstr,k,nnew)+tau*
     &                                     vclm(i,Jstr,k)
            v(i,Jstr,k,nnew)=v(i,Jstr,k,nnew)*vmask(i,Jstr)
          enddo
        enddo
      endif
      if (.not.NORTH_INTER) then
        do k=1,N
          do i=Istr,Iend+1
            grad(i,Jend  )=(v(i,Jend  ,k,nstp)-v(i-1,Jend  ,k,nstp))
     &                                                *pmask(i,Jend)
            grad(i,Jend+1)=(v(i,Jend+1,k,nstp)-v(i-1,Jend+1,k,nstp))
     &                                              *pmask(i,Jend+1)
          enddo
          do i=Istr,Iend
            dft=v(i,Jend,k,nstp)-v(i,Jend  ,k,nnew)
            dfx=v(i,Jend,k,nnew)-v(i,Jend-1,k,nnew)
            if (dfx*dft .lt. 0.D0) then
              dft=0.D0
              tau=tau_in
            else
              tau=tau_out
            endif
            if (dft*(grad(i,Jend)+grad(i+1,Jend)) .gt. 0.D0) then
              dfy=grad(i,Jend)
            else
              dfy=grad(i+1,Jend)
            endif
            cff=max(dfx*dfx+dfy*dfy, eps)
            cx=dft*dfx
            cy=min(cff,max(dft*dfy,-cff))
            v(i,Jend+1,k,nnew)=( cff*v(i,Jend+1,k,nstp)
     &                              +cx*v(i,Jend,k,nnew)
     &                      -max(cy,0.D0)*grad(i  ,Jend+1)
     &                      -min(cy,0.D0)*grad(i+1,Jend+1)
     &                                      )/(cff+cx)
            v(i,Jend+1,k,nnew)=(1.D0-tau)*v(i,Jend+1,k,nnew)+tau*
     &                                        vclm(i,Jend+1,k)
            v(i,Jend+1,k,nnew)=v(i,Jend+1,k,nnew)*vmask(i,Jend+1)
          enddo
        enddo
      endif
      if (.not.WEST_INTER) then
        do k=1,N
          do j=JstrV-1,Jend
            grad(Istr-1,j)=v(Istr-1,j+1,k,nstp)-v(Istr-1,j,k,nstp)
            grad(Istr  ,j)=v(Istr  ,j+1,k,nstp)-v(Istr  ,j,k,nstp)
          enddo
          do j=JstrV,Jend
            dft=v(Istr,j,k,nstp)-v(Istr  ,j,k,nnew)
            dfx=v(Istr,j,k,nnew)-v(Istr+1,j,k,nnew)
            if (dfx*dft .lt. 0.D0) then
              dft=0.D0
              tau=tau_in
            else
              tau=tau_out
            endif
            if (dft*(grad(Istr,j-1)+grad(Istr,j)) .gt. 0.D0) then
              dfy=grad(Istr,j-1)
            else
              dfy=grad(Istr,j  )
            endif
            cff=max(dfx*dfx+dfy*dfy, eps)
            cx=dft*dfx
            cy=min(cff,max(dft*dfy,-cff))
            v(Istr-1,j,k,nnew)=( cff*v(Istr-1,j,k,nstp)
     &                              +cx*v(Istr,j,k,nnew)
     &                      -max(cy,0.D0)*grad(Istr-1,j-1)
     &                      -min(cy,0.D0)*grad(Istr-1,j  )
     &                                       )/(cff+cx)
            v(Istr-1,j,k,nnew)=(1.D0-tau)*v(Istr-1,j,k,nnew)
     &                               +tau*vclm(Istr-1,j,k)
            v(Istr-1,j,k,nnew)=v(Istr-1,j,k,nnew)*vmask(Istr-1,j)
          enddo
        enddo
      endif
      if (.not.EAST_INTER) then
        do k=1,N
          do j=JstrV-1,Jend
            grad(Iend  ,j)=v(Iend  ,j+1,k,nstp)-v(Iend  ,j,k,nstp)
            grad(Iend+1,j)=v(Iend+1,j+1,k,nstp)-v(Iend+1,j,k,nstp)
          enddo
          do j=JstrV,Jend
            dft=v(Iend,j,k,nstp)-v(Iend  ,j,k,nnew)
            dfx=v(Iend,j,k,nnew)-v(Iend-1,j,k,nnew)
            if (dfx*dft .lt. 0.D0) then
              dft=0.D0
              tau=tau_in
            else
              tau=tau_out
            endif
            if (dft*(grad(Iend,j-1)+grad(Iend,j)) .gt. 0.D0) then
              dfy=grad(Iend,j-1)
            else
              dfy=grad(Iend,j  )
            endif
            cff=max(dfx*dfx+dfy*dfy, eps)
            cx=dft*dfx
            cy=min(cff,max(dft*dfy,-cff))
            v(Iend+1,j,k,nnew)=( cff*v(Iend+1,j,k,nstp)
     &                              +cx*v(Iend,j,k,nnew)
     &                      -max(cy,0.D0)*grad(Iend+1,j-1)
     &                      -min(cy,0.D0)*grad(Iend+1,j  )
     &                                       )/(cff+cx)
            v(Iend+1,j,k,nnew)=(1.D0-tau)*v(Iend+1,j,k,nnew)
     &                               +tau*vclm(Iend+1,j,k)
            v(Iend+1,j,k,nnew)=v(Iend+1,j,k,nnew)*vmask(Iend+1,j)
          enddo
        enddo
      endif
      if (.not.WEST_INTER .and. .not.SOUTH_INTER) then
        do k=1,N
          v(Istr-1,Jstr,k,nnew)=0.5D0*( v(Istr-1,Jstr+1,k,nnew)
     &                               +v(Istr  ,Jstr  ,k,nnew))
     &                          *vmask(Istr-1,Jstr)
        enddo
      endif
      if (.not.EAST_INTER .and. .not.SOUTH_INTER) then
        do k=1,N
          v(Iend+1,Jstr,k,nnew)=0.5D0*( v(Iend+1,Jstr+1,k,nnew)
     &                               +v(Iend  ,Jstr  ,k,nnew))
     &                          *vmask(Iend+1,Jstr)
        enddo
      endif
      if (.not.WEST_INTER .and. .not.NORTH_INTER) then
        do k=1,N
          v(Istr-1,Jend+1,k,nnew)=0.5D0*( v(Istr-1,Jend,k,nnew)
     &                                 +v(Istr,Jend+1,k,nnew))
     &                            *vmask(Istr-1,Jend+1)
        enddo
      endif
      if (.not.EAST_INTER .and. .not.NORTH_INTER) then
        do k=1,N
          v(Iend+1,Jend+1,k,nnew)=0.5D0*( v(Iend+1,Jend,k,nnew)
     &                                 +v(Iend,Jend+1,k,nnew))
     &                            *vmask(Iend+1,Jend+1)
        enddo
      endif
      return
       
      

      end subroutine Sub_Loop_v3dbc_parent_tile

      subroutine v3dbc_child_tile (Istr,Iend,Jstr,Jend,grad)      


      use Agrif_Util
      interface
        subroutine Sub_Loop_v3dbc_child_tile(Istr,Iend,Jstr,Jend,grad,pa
     &dd_E,Mm,padd_X,Lm,vmask,vclm,nnew,v,tauM_out,tauM_in,dt,NORTH_INTE
     &R,SOUTH_INTER,EAST_INTER,WEST_INTER)
          implicit none
      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      real :: tauM_out
      real :: tauM_in
      real :: dt
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad
        end subroutine Sub_Loop_v3dbc_child_tile

      end interface
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad
      

        call Sub_Loop_v3dbc_child_tile(Istr,Iend,Jstr,Jend,grad, Agrif_t
     &abvars_i(188)%iarray0, Agrif_tabvars_i(197)%iarray0, Agrif_tabvars
     &_i(189)%iarray0, Agrif_tabvars_i(200)%iarray0, Agrif_tabvars(48)%a
     &rray2, Agrif_tabvars(130)%array3, Agrif_tabvars_i(174)%iarray0, Ag
     &rif_tabvars(110)%array4, Agrif_tabvars_r(16)%array0, Agrif_tabvars
     &_r(17)%array0, Agrif_tabvars_r(46)%array0, Agrif_tabvars_l(5)%larr
     &ay0, Agrif_tabvars_l(4)%larray0, Agrif_tabvars_l(7)%larray0, Agrif
     &_tabvars_l(6)%larray0)

      end


      subroutine Sub_Loop_v3dbc_child_tile(Istr,Iend,Jstr,Jend,grad,padd
     &_E,Mm,padd_X,Lm,vmask,vclm,nnew,v,tauM_out,tauM_in,dt,NORTH_INTER,
     &SOUTH_INTER,EAST_INTER,WEST_INTER)

      use Agrif_Types, only : Agrif_tabvars

      implicit none

      integer(4), parameter :: LLm0 = 384
      integer(4), parameter :: MMm0 = 390
      integer(4), parameter :: N = 66
      integer(4), parameter :: NP_XI = 6
      integer(4), parameter :: NP_ETA = 30
      integer(4), parameter :: NNODES = NP_XI*NP_ETA
      integer(4), parameter :: NPP = 1
      integer(4), parameter :: NSUB_X = 1
      integer(4), parameter :: NSUB_E = 1
      integer(4), parameter :: NWEIGHT = 1000
      integer(4), parameter :: Ntides = 10
      integer(4), parameter :: stdout = 6
      integer(4), parameter :: Np = N+1
      integer(4), parameter :: NSA = 28
      real, parameter :: Vtransform = 1
      integer(4), parameter :: itemp = 1
      integer(4), parameter :: ntrc_salt = 1
      integer(4), parameter :: ntrc_pas = 0
      integer(4), parameter :: ntrc_bio = 0
      integer(4), parameter :: ntrc_sed = 0
      integer(4), parameter :: NT = itemp+ntrc_salt+ntrc_pas+ntrc_bio+nt
     &rc_sed
      integer(4), parameter :: isalt = itemp+1
      integer(4), parameter :: ntrc_diabio = 0
      integer(4), parameter :: ntrc_diats = 0
      integer(4), parameter :: ntrc_diauv = 0
      real, parameter :: qp2 = 0.0000172D0
      real, parameter :: pi = 3.14159265358979323846D0
      real, parameter :: deg2rad = pi/180.D0
      real, parameter :: rad2deg = 180.D0/pi
      real, parameter :: Eradius = 6371315.0D0
      real, parameter :: day2sec = 86400.D0
      real, parameter :: sec2day = 1.D0/86400.D0
      real, parameter :: jul_off = 2440000.D0
      real, parameter :: year2day = 365.25D0
      real, parameter :: day2year = 1.D0/365.25D0
      real, parameter :: g = 9.81D0
      real, parameter :: Cp = 3985.0D0
      real, parameter :: vonKar = 0.41D0
      real, parameter :: spval = -9999.0D0
      logical, parameter :: mask_val = .true.
      real, parameter :: eps = 1.D-20
      integer(4) :: padd_E
      integer(4) :: Mm
      integer(4) :: padd_X
      integer(4) :: Lm
      integer(4) :: nnew
      real :: tauM_out
      real :: tauM_in
      real :: dt
      logical :: NORTH_INTER
      logical :: SOUTH_INTER
      logical :: EAST_INTER
      logical :: WEST_INTER
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E) :: vmask
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N) :: vclm
      real, dimension(-1:Lm+2+padd_X,-1:Mm+2+padd_E,1:N,1:3) :: v
      integer(4) :: Istr
      integer(4) :: Iend
      integer(4) :: Jstr
      integer(4) :: Jend
      real, dimension(Istr-2:Iend+2,Jstr-2:Jend+2) :: grad

                   

                                            
                             
                                            

                                                             
                                                            
                                           
                                                               
                                    
                                      
                                                           

                       

                                    

                        
                              

                       
                           

                                          
                                             
                                  

                                          
                        

                                                        
                      
                              

                            
                                                          
                         

                             

                            

                            

                            

                                                               

                                                                       
                                                                 
       
                               

                               

                              

                              

                                            
                                               
                                            
                                               
                                                                 
                                                 
                                 
                                               
                                               
                                               
                                               
                                               
                                               
                                             
                                             
                                             
                                             
                                             
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                               
                                              
                                                  
                                                  
                                                  
                                                  
                                                 
                                                 
                                               
                                               
                                                      
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 
                                                           
                                                           
                                                           
                                    
                                                
                                                
                                                
                                                
                                                 
                           
                           
                           
                           
                             
                                                
                                                
                                                   
                                              
                                               
                                                   
                                                
                                                  
                                                 
                                                 
                                                              
                             
                                                 
                                                  
                                                 
                                                
                                             
                                                
                            
               
                                 

                                              
                             
                                                   
                                       
                                                 
                               
                          
                        
                                                            
                                     
                                      
                                                                       
                                                                 
               
                                                    
                               
                                                        
                                       
                                                        
                                   
                           
                          
                                                                       
                                                                 
                            
                           
                                                                       
                                                                 
                                                                 
                                                                 
                   
                                                
                                                
                                                     
                                                 
                                                 
                                                 
                                                    
                                         
                                                    
                                                    
                                                             
                                                    
                                         
                                                     
                                                     
                                                         
                            
                          
                                                                       
                                                                 
                                     
                                       
                                        
                                                                       
                                                                 
                                                                 
                                 
                                                      
                                                                       
                                                                 
                                 
                                
                                                                       
                                                                 
                                                                 
                                                                 
                                                   
                                                                       
                                                                 
                                                                 
           
                                              
                                                   
                     
                              
                               
                                 
                                                  
                                                                       
                                                                 
                                                                 
                        
                      
                           
                                                                       
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                                                 
                                 
                           
                                      
                             
                                                 
                                                                       
                                                                 
                     
                                                      
                                                                       
                                                                 
              
                                          
                                                                       
                                                                 
       
                                                  
                                                                       
                                                                 
              
                                  
                                           
                                                  
                                                               
                                                                     
                                                                       
                                                                 
                                                                 
 
                                
                                                                       
                                                                 
                                                        

                                                                       
                                                                 
       
                                                                       
                                                                 
                                                                 
                                        

                          

              
                             

                  
                               

                 
                                 

                       
                                   

                       
                                          
                     
                                          
                                                                       
                                                                       
                                                                 
                                                      
                                           
      integer(4) :: i
      integer(4) :: j
      integer(4) :: k
                                                                       
                                                                 
                                   
      real :: cff
      real :: cx
      real :: cy
      real :: dft
      real :: dfx
      real :: dfy
      real :: tau
      real :: tau_in
      real :: tau_out
                            

                                        
      integer(4) :: IstrR
      integer(4) :: IendR
      integer(4) :: JstrR
      integer(4) :: JendR
                      
      integer(4) :: IstrU
                      
      integer(4) :: JstrV
      if (.not.WEST_INTER) then
        IstrR=Istr-1
        IstrU=Istr+1
      else
        IstrR=Istr
        IstrU=Istr
      endif
      if (.not.EAST_INTER) then
        IendR=Iend+1
      else
        IendR=Iend
      endif
      if (.not.SOUTH_INTER) then
        JstrR=Jstr-1
        JstrV=Jstr+1
      else
        JstrR=Jstr
        JstrV=Jstr
      endif
      if (.not.NORTH_INTER) then
        JendR=Jend+1
      else
        JendR=Jend
      endif
      call v3dbc_interp_tile(Istr,Iend,Jstr,Jend)
      tau_in=dt*tauM_in
      tau_out=dt*tauM_out
      if (.not.SOUTH_INTER) then
        do k=1,N
          do i=Istr,Iend
            v(i,Jstr,k,nnew)=vclm(i,Jstr,k)
     &                       *vmask(i,Jstr)
          enddo
        enddo
      endif
      if (.not.NORTH_INTER) then
        do k=1,N
          do i=Istr,Iend
            v(i,Jend+1,k,nnew)=vclm(i,Jend+1,k)
     &                         *vmask(i,Jend+1)
          enddo
        enddo
      endif
      if (.not.WEST_INTER) then
        do k=1,N
          do j=JstrV,Jend
            v(Istr-1,j,k,nnew)=vclm(Istr-1,j,k)
     &                         *vmask(Istr-1,j)
          enddo
        enddo
      endif
      if (.not.EAST_INTER) then
        do k=1,N
          do j=Jstr,Jend
            v(Iend+1,j,k,nnew)=vclm(Iend+1,j,k)
     &                         *vmask(Iend+1,j)
          enddo
        enddo
      endif
      if (.not.WEST_INTER .and. .not.SOUTH_INTER) then
        do k=1,N
          v(Istr-1,Jstr,k,nnew)=0.5D0*( v(Istr-1,Jstr+1,k,nnew)
     &                               +v(Istr  ,Jstr  ,k,nnew))
     &                          *vmask(Istr-1,Jstr)
        enddo
      endif
      if (.not.EAST_INTER .and. .not.SOUTH_INTER) then
        do k=1,N
          v(Iend+1,Jstr,k,nnew)=0.5D0*( v(Iend+1,Jstr+1,k,nnew)
     &                               +v(Iend  ,Jstr  ,k,nnew))
     &                          *vmask(Iend+1,Jstr)
        enddo
      endif
      if (.not.WEST_INTER .and. .not.NORTH_INTER) then
        do k=1,N
          v(Istr-1,Jend+1,k,nnew)=0.5D0*( v(Istr-1,Jend,k,nnew)
     &                                 +v(Istr,Jend+1,k,nnew))
     &                            *vmask(Istr-1,Jend+1)
        enddo
      endif
      if (.not.EAST_INTER .and. .not.NORTH_INTER) then
        do k=1,N
          v(Iend+1,Jend+1,k,nnew)=0.5D0*( v(Iend+1,Jend,k,nnew)
     &                                 +v(Iend,Jend+1,k,nnew))
     &                            *vmask(Iend+1,Jend+1)
        enddo
      endif
      return
       
      

      end subroutine Sub_Loop_v3dbc_child_tile

