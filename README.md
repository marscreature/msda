# MSDA

Multiscale data assimilation system for SWOT product.

This system uses 3DVAR to assimilate satellite and in-situ observations in producing a gridded SSH product and the upper ocean state. 
